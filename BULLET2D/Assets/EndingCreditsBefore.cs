﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EndingCreditsBefore : MonoBehaviour
{
    [SerializeField] private GameObject OnObj;
    [SerializeField] private bool isSecond = false;
    private void Start()
    {
        if (isSecond) StartCoroutine(EndingStart2());
    }
    public void EndingStart()
    {
        OnObj.SetActive(true);
        gameObject.SetActive(false);
    }
    IEnumerator EndingStart2()
    {
        transform.Find("CHN_S").gameObject.SetActive(false);
        transform.Find("CHN_T").gameObject.SetActive(false);
        transform.Find("ENG").gameObject.SetActive(false);
        transform.Find("KOR").gameObject.SetActive(false);
        if (LocalizeManager.lang == LANGUAGE_SHIFTRICK.CHN_S)
            transform.Find("CHN_S").gameObject.SetActive(true);
        else if (LocalizeManager.lang == LANGUAGE_SHIFTRICK.CHN_T)
            transform.Find("CHN_T").gameObject.SetActive(true);
        else if (LocalizeManager.lang == LANGUAGE_SHIFTRICK.ENG)
            transform.Find("ENG").gameObject.SetActive(true);
        else if (LocalizeManager.lang == LANGUAGE_SHIFTRICK.KOR)
            transform.Find("KOR").gameObject.SetActive(true);
        yield return new WaitForSeconds(24);
        OnObj.SetActive(true);
        gameObject.SetActive(false);
        GameObject.Find("Canvas").transform.Find("Back").GetComponent<Image>().enabled = true;
    }
}
