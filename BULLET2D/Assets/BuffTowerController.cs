﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuffTowerController : MonoBehaviour
{
    [SerializeField] private GameObject controllerObj;
    [SerializeField] private SpriteRenderer controllerSR;
    [SerializeField] private bool isSelect = false;
    [SerializeField] private AudioSource audio;
    [SerializeField] private AudioSource audio2;
    [SerializeField] private AudioClip[] clips;
    bool isOpen = false;
    public bool isUsing = false;
    public bool isUse = false;
    Animator animator;
    private float delay = 0.0f;

    [SerializeField] private Vector3 vOffset;
    [SerializeField] private Vector3 vSize;
    [SerializeField] private string firstCommnet;
    [SerializeField] private string secondCommnet;
    [SerializeField] private string thirdCommnet;
    [SerializeField] private int nowType = 0;
    [SerializeField] private GameObject[] buffType;
    [SerializeField] private GameObject collisionObj;
    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
        nowType = Random.Range(0, buffType.Length);
        collisionObj.SetActive(true);
        buffType[nowType].SetActive(true);
        buffType[nowType].GetComponent<ParticleSystem>().Play();
    }

    // Update is called once per frame
    void Update()
    {
        delay -= Time.deltaTime;
        if (delay <= 0.0f) delay = 0.0f;

        RaycastHit2D hit = Physics2D.BoxCast(transform.position + vOffset, vSize, 0, Vector2.zero, 0, 1 << 9);
        if (hit && !isSelect && !isOpen && !isUse && !TextOpenController.instance.isOpen && !PDG.Dungeon.instance.newObjectSelected)
        {
            PDG.Dungeon.instance.newObjectSelected = true;
            PDG.Dungeon.instance.newObjectSelectedObj = gameObject;
            isSelect = true;
            controllerObj.SetActive(true);
            controllerObj.GetComponent<SpriteRenderer>().sprite = PDG.Dungeon.instance.isJoystrick ? PDG.Dungeon.GetJoyStrickKeySprite(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_ACTIVE]) : PDG.Dungeon.GetJoyStrickKeySprite(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ACTIVE]);
            TextOpenController.instance.ShowText(LocalizeManager.GetLocalize("bufftower_buff_" + nowType), Color.white, 24, controllerObj, 2.0f, true);
            //TextOpenController.instance.ShowText(LocalizeManager.GetLocalize(firstCommnet), Color.white, 24, controllerObj, 0.5f, true);
        }
        else if (!hit)
        {
            if (PDG.Dungeon.instance.newObjectSelectedObj == gameObject)
            {
                PDG.Dungeon.instance.newObjectSelectedObj = null;
                PDG.Dungeon.instance.newObjectSelected = false;
            }
            isSelect = false;
            isOpen = false;
            controllerObj.SetActive(false);
            if (TextOpenController.instance.targetObj == controllerObj) TextOpenController.instance.TextClose(Color.white);
        }
        if (isSelect)
        {
            if ((PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ACTIVE], true) || PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_ACTIVE], true)) && !isOpen && delay <= 0.0f)
            {
                SoundManager.instance.StartAudio(new string[1] { "Object/active_F" }, VOLUME_TYPE.EFFECT);
                delay = 0.1f;
                isUse = true;
                isSelect = false;
                if (PDG.Dungeon.instance.newObjectSelectedObj == gameObject)
                {
                    PDG.Dungeon.instance.newObjectSelectedObj = null;
                    PDG.Dungeon.instance.newObjectSelected = false;
                }
                PDG.Player.instance.nowBuffTowerBuff = nowType;
                //animator.enabled = false;
                if (nowType == 0)
                {
                    PDG.Player.instance.buffTowerBuffTime_0 = 20.0f + PDG.Player.instance.fBuffPlusTime;
                }
                else if (nowType == 1)
                {
                    if (PDG.Player.instance.buffTowerBuffTime_1 <= 0)
                    {
                        PDG.Player.instance.damagePlus += 10.0f;
                        PDG.Player.instance.damagePlusNear += 10.0f;
                        PDG.Player.instance.damageQPlus += 10.0f;
                        PDG.Player.instance.damageQPlusNear += 10.0f;
                        PDG.Player.instance.damageRightPlus += 10.0f;
                        PDG.Player.instance.damageRightPlusNear += 10.0f;
                        PDG.Player.instance.isBufftowerBuff1 = true;
                    }
                    PDG.Player.instance.buffTowerBuffTime_1 = 20.0f + PDG.Player.instance.fBuffPlusTime;
                }
                else if (nowType == 2)
                {
                    PDG.Player.instance.buffTowerBuffTime_2 = 20.0f + PDG.Player.instance.fBuffPlusTime;
                }
                else if (nowType == 3)
                {
                    if (PDG.Player.instance.buffTowerBuffTime_3 <= 0.0f)
                        PDG.Player.instance.plusAttackSpeed += 1;
                    PDG.Player.instance.buffTowerBuffTime_3 = 20.0f + PDG.Player.instance.fBuffPlusTime;
                    PDG.Player.instance.isBufftowerBuff3 = true;
                }
                for (int i = 0; i < buffType.Length; i++) PDG.Player.instance.transform.Find("buffTower_" + i).gameObject.SetActive(false);
                PDG.Player.instance.transform.Find("buffTower_" + nowType).gameObject.SetActive(true);
                buffType[nowType].SetActive(false);
                audio.clip = clips[nowType];
                audio.volume =  (SoundManager.isMute || (PDG.Dungeon.instance == null ? false : PDG.Dungeon.instance.isPause) || (PDG.Player.instance == null ? false : PDG.Player.instance.isDie)) ? 0 : SoundManager.EffectVolume;
                audio.Play();
                audio2.Stop();
                if (animator) animator.enabled = false;
                collisionObj.SetActive(false);
            }
        }
    }
}
