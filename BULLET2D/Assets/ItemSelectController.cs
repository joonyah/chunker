﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemSelectController : MonoBehaviour
{
    [SerializeField] private Sprite fSprite;
    [SerializeField] private GameObject controllerObj;
    [SerializeField] private SpriteRenderer controllerSR;
    [SerializeField] private bool isSelect = false;
    public int num;
    public string itemPath;
    public ItemsData iData;
    public bool isItem = false;

    [SerializeField] private GameObject GetParticle;
    // Update is called once per frame
    void Update()
    {
        RaycastHit2D hit = Physics2D.BoxCast(transform.position, new Vector2(2.5f, 3.5f), 0, Vector2.zero, 0, 1 << 9);

        if (hit && !isSelect && !PDG.Dungeon.instance.newObjectSelected)
        {
            isSelect = true;
            PDG.Dungeon.instance.newObjectSelected = true;
            PDG.Dungeon.instance.newObjectSelectedObj = gameObject;
            controllerObj.SetActive(true);
            controllerObj.GetComponent<SpriteRenderer>().sprite = PDG.Dungeon.instance.isJoystrick ? PDG.Dungeon.GetJoyStrickKeySprite(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_ACTIVE]) : PDG.Dungeon.GetJoyStrickKeySprite(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ACTIVE]);
            TextOpenController.instance.ShowText(LocalizeManager.GetLocalize(itemPath.Replace('/', '_')), Color.white, 18, controllerObj, 0.5f, true);
        }
        else if (!hit)
        {
            isSelect = false;
            if (PDG.Dungeon.instance.newObjectSelectedObj == gameObject)
            {
                PDG.Dungeon.instance.newObjectSelectedObj = null;
                PDG.Dungeon.instance.newObjectSelected = false;
            }
            controllerObj.SetActive(false);
            if (TextOpenController.instance.targetObj == controllerObj)
            {
                TextOpenController.instance.TextClose(Color.white);
            }
        }

        if (isSelect)
        {
            if ((PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ACTIVE], true) || PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_ACTIVE], true)) && PDG.Dungeon.instance.closeCoolTime > 0.2f)
            {
                PDG.Dungeon.instance.closeCoolTime = 0.0f;
                SoundManager.instance.StartAudio(new string[1] { "Object/active_F" }, VOLUME_TYPE.EFFECT);
                isSelect = false;
                if (PDG.Dungeon.instance.newObjectSelectedObj == gameObject)
                {
                    PDG.Dungeon.instance.newObjectSelectedObj = null;
                    PDG.Dungeon.instance.newObjectSelected = false;
                }
                if (!isItem)
                {
                    string[] s = itemPath.Split('/');
                    controllerObj.SetActive(false);
                    Inventorys.Instance.AddItem(new ItemsData
                    {
                        Type1 = "Material",
                        Type2 = s[0],
                        ID = s[1]
                    }, 1, true, false);
                    MailSaveData mData = SaveAndLoadManager.instance.LoadMail("hellfogee_item");
                    mData.isItems[num] = true;
                    SaveAndLoadManager.instance.SaveMail(mData);
                }
                else
                {
                    controllerObj.SetActive(false);
                    Inventorys.Instance.AddItem(iData, 1, true, false);
                    PDG.Player.instance.SettingItem(iData);
                    MailSaveData mData = SaveAndLoadManager.instance.LoadMail("doctor_item");
                    mData.isItems[num] = true;
                    SaveAndLoadManager.instance.SaveMail(mData);
                }
                SoundManager.instance.StartAudio(new string[1] { "Object/ajit_object" }, VOLUME_TYPE.EFFECT);
                GameObject obj = Instantiate(GetParticle, transform.position, Quaternion.identity, null);
                obj.transform.localScale = new Vector3(1, 1, 1);
                obj.transform.position = transform.position;
                obj.GetComponent<SpriteRenderer>().sortingLayerName = "Item";
                Destroy(obj, 1.0f);
                if (TextOpenController.instance.targetObj == controllerObj)
                {
                    TextOpenController.instance.TextClose(Color.white);
                }
                gameObject.SetActive(false);
            }
        }
    }
}
