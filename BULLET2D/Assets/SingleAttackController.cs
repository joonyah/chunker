﻿using PDG;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SingleAttackController : MonoBehaviour
{
    [SerializeField] private float radius = 4;
    [SerializeField] private GameObject HitEffect;
    [SerializeField] private GameObject HitEffect2;


    public void SettingDamage(float _damage, bool isRange)
    {
        RaycastHit2D[] hits = Physics2D.CircleCastAll(transform.position, radius, Vector2.zero, 0, 1 << 12 | 1 << 23 | 1 << 24);
        if (hits.Length > 0)
        {
            for (int i = 0; i < hits.Length; i++)
            {
                if (!isRange && i > 0) break;


                if (hits[i].collider.gameObject.layer.Equals(24))
                {
                    if (hits[i].collider.GetComponent<MOB.BossMonster_Six_Apear>()) hits[i].collider.GetComponent<MOB.BossMonster_Six_Apear>().SetDamage(_damage);
                    if (hits[i].collider.GetComponent<Boss_STG>()) hits[i].collider.GetComponent<Boss_STG>().SetDamage(_damage, hits[i].point);
                    if (hits[i].collider.GetComponent<Boss_NPC>()) hits[i].collider.GetComponent<Boss_NPC>().SetDamage(_damage, hits[i].point);
                    if (hits[i].collider.GetComponent<Boss_Golem>()) hits[i].collider.GetComponent<Boss_Golem>().SetDamage(_damage, hits[i].point);
                    if (hits[i].collider.GetComponent<Boss_Stone>()) hits[i].collider.GetComponent<Boss_Stone>().SetDamage(_damage, hits[i].point, gameObject.name.Contains("multiple_bombing") ? true : false);
                    if (Player.instance.isStrategic_02) Player.instance.isStrategic_02 = false;
                    if (HitEffect)
                    {
                        GameObject obj = Instantiate(HitEffect);
                        obj.transform.position = hits[i].point;
                        obj.transform.localScale = new Vector3(0.3f, 0.3f, 0.3f);
                        Destroy(obj, 2f);
                    }
                    if (HitEffect2)
                    {
                        GameObject obj = Instantiate(HitEffect2);
                        obj.transform.position = hits[i].point;
                        obj.transform.localScale = new Vector3(0.3f, 0.3f, 0.3f);
                        Destroy(obj, 2f);
                    }
                    if (PDG.Player.instance.isRightNear && PDG.Player.instance.isCriticalRight)
                    {
                        GameObject criEffect = ObjManager.Call().GetObject("CriticalHitEffect");
                        criEffect.transform.position = hits[i].point;
                        criEffect.SetActive(true);
                    }
                }
                else if (hits[i].collider.gameObject.layer.Equals(23))
                {
                    if (hits[i].collider.transform.parent && hits[i].collider.transform.parent.GetComponent<TentacleController>())
                    {
                        hits[i].collider.transform.parent.GetComponent<TentacleController>().SetDamage(1);
                        if (HitEffect)
                        {
                            GameObject obj = Instantiate(HitEffect);
                            obj.transform.position = hits[i].point;
                            obj.transform.localScale = new Vector3(0.3f, 0.3f, 0.3f);
                            Destroy(obj, 2f);
                        }
                        if (HitEffect2)
                        {
                            GameObject obj = Instantiate(HitEffect2);
                            obj.transform.position = hits[i].point;
                            obj.transform.localScale = new Vector3(0.3f, 0.3f, 0.3f);
                            Destroy(obj, 2f);
                        }
                    }
                    Vector3 dir = hits[i].collider.gameObject.transform.position - PDG.Player.instance.transform.position;
                    dir.Normalize();
                    if (hits[i].collider.gameObject.GetComponent<DestroyObjects>())
                    {
                        hits[i].collider.gameObject.GetComponent<DestroyObjects>().DestroyObject(dir);
                        if (HitEffect)
                        {
                            GameObject obj = Instantiate(HitEffect);
                            obj.transform.position = hits[i].point;
                            obj.transform.localScale = new Vector3(0.3f, 0.3f, 0.3f);
                            Destroy(obj, 2f);
                        }
                        if (HitEffect2)
                        {
                            GameObject obj = Instantiate(HitEffect2);
                            obj.transform.position = hits[i].point;
                            obj.transform.localScale = new Vector3(0.3f, 0.3f, 0.3f);
                            Destroy(obj, 2f);
                        }
                    }
                    if (hits[i].collider.gameObject.GetComponent<ChestHit>() && !hits[i].collider.gameObject.GetComponent<ChestHit>().isOpenBefore)
                    {
                        hits[i].collider.gameObject.GetComponent<ChestHit>().SetDamage(1);
                        if (HitEffect)
                        {
                            GameObject obj = Instantiate(HitEffect);
                            obj.transform.position = hits[i].point;
                            obj.transform.localScale = new Vector3(0.3f, 0.3f, 0.3f);
                            Destroy(obj, 2f);
                        }
                        if (HitEffect2)
                        {
                            GameObject obj = Instantiate(HitEffect2);
                            obj.transform.position = hits[i].point;
                            obj.transform.localScale = new Vector3(0.3f, 0.3f, 0.3f);
                            Destroy(obj, 2f);
                        }
                    }
                    if (hits[i].collider.gameObject.GetComponent<DrumHit>())
                    {
                        hits[i].collider.gameObject.GetComponent<DrumHit>().SetDamage(1);
                        if (HitEffect)
                        {
                            GameObject obj = Instantiate(HitEffect);
                            obj.transform.position = hits[i].point;
                            obj.transform.localScale = new Vector3(0.3f, 0.3f, 0.3f);
                            Destroy(obj, 2f);
                        }
                        if (HitEffect2)
                        {
                            GameObject obj = Instantiate(HitEffect2);
                            obj.transform.position = hits[i].point;
                            obj.transform.localScale = new Vector3(0.3f, 0.3f, 0.3f);
                            Destroy(obj, 2f);
                        }
                    }
                }
                else
                {
                    Vector3 dir = hits[i].collider.transform.position - transform.position;
                    dir.Normalize();

                    if (hits[i].collider.GetComponent<MOB.Monster>())
                    {
                        hits[i].collider.GetComponent<MOB.Monster>().SetDamage(_damage, -dir, 1, ELEMENTAL_TYPE.NONE, 0.0f);
                        if (HitEffect)
                        {
                            GameObject obj = Instantiate(HitEffect);
                            obj.transform.position = hits[i].point;
                            obj.transform.localScale = new Vector3(0.3f, 0.3f, 0.3f);
                            Destroy(obj, 2f);
                        }
                        if (HitEffect2)
                        {
                            GameObject obj = Instantiate(HitEffect2);
                            obj.transform.position = hits[i].point;
                            obj.transform.localScale = new Vector3(0.3f, 0.3f, 0.3f);
                            Destroy(obj, 2f);
                        }
                        if (PDG.Player.instance.isRightNear && PDG.Player.instance.isCriticalRight)
                        {
                            GameObject criEffect = ObjManager.Call().GetObject("CriticalHitEffect");
                            criEffect.transform.position = hits[i].point;
                            criEffect.SetActive(true);
                        }
                    }
                }
            }
        }
    }
}
