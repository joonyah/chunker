﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AjitObjectController : MonoBehaviour
{
    [SerializeField] private Sprite fSprite;
    [SerializeField] private GameObject controllerObj;
    [SerializeField] private SpriteRenderer controllerSR;
    [SerializeField] private bool isSelect = false;
    [SerializeField] private Vector2 hitSize;
    [SerializeField] private Vector2 controllerPos;
    [SerializeField] private string _openText;
    [SerializeField] private string _openNameText;
    // Start is called before the first frame update
    void Start()
    {
        controllerObj.transform.localPosition = controllerPos;
    }

    // Update is called once per frame
    void Update()
    {
        RaycastHit2D hit = Physics2D.BoxCast(transform.position, hitSize, 0, Vector2.zero, 0, 1 << 9);
        if (hit && !isSelect && !PDG.Dungeon.instance.newObjectSelected)
        {
            isSelect = true;
            PDG.Dungeon.instance.newObjectSelected = true;
            PDG.Dungeon.instance.newObjectSelectedObj = gameObject;
            TextOpenController.instance.ShowText(LocalizeManager.GetLocalize(_openNameText), Color.white, 24, controllerObj, 0.5f, true);
            controllerSR.sprite = PDG.Dungeon.instance.isJoystrick ? PDG.Dungeon.GetJoyStrickKeySprite(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_ACTIVE]) : PDG.Dungeon.GetJoyStrickKeySprite(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ACTIVE]);
            controllerObj.SetActive(true);
        }
        else if (!hit)
        {
            isSelect = false;
            if (PDG.Dungeon.instance.newObjectSelectedObj == gameObject)
            {
                PDG.Dungeon.instance.newObjectSelectedObj = null;
                PDG.Dungeon.instance.newObjectSelected = false;
            }
            controllerObj.SetActive(false);
            if (TextOpenController.instance.targetObj == controllerObj) TextOpenController.instance.TextClose(Color.white);
        }

        if (isSelect)
        {
            if (PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ACTIVE], true) || PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_ACTIVE], true) && PDG.Dungeon.instance.closeCoolTime > 0.2f) 
            {
                PDG.Dungeon.instance.closeCoolTime = 0.0f;
                SoundManager.instance.StartAudio(new string[1] { "Object/active_F" }, VOLUME_TYPE.EFFECT);
                isSelect = false;
                controllerObj.SetActive(false);
                TextOpenController.instance.ShowText(LocalizeManager.GetLocalize(_openText), Color.white, 24, controllerObj, 3, false);
            }
        }
    }
}
