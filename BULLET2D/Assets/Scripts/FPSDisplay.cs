﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public enum JoystickKeySet
{
    KEY_ATTACK,
    KEY_ATTACK2,
    KEY_SKILL,
    KEY_SLIDE,
    KEY_OPTION,
    KEY_RESTART,
    KEY_QUIT,
    KEY_MAP,
    KEY_RECYCLE,
    KEY_ACTIVE,
    KEY_CONFIRM,
    KEY_CANCLE,
    KEY_ARROW,
    KEY_ARROW_UP,
    KEY_ARROW_DOWN,
    KEY_ARROW_LEFT,
    KEY_ARROW_RIGHT,
    KEY_MAX
};

public class FPSDisplay : MonoBehaviour
{
    public static FPSDisplay instance;
    public Dictionary<JoystickKeySet, KeyCode> JoyStickKey = new Dictionary<JoystickKeySet, KeyCode>();
    public Dictionary<JoystickKeySet, KeyCode> KeyboardKey = new Dictionary<JoystickKeySet, KeyCode>();

    float deltaTime = 0.0f;
    GUIStyle style;
    Rect rect;
    float msec;
    float fps;
    float worstFps = 100f;
    string text;
    [SerializeField] private Font _font;
    void Awake()
    {
        if (instance == null) instance = this;

        int w = Screen.width, h = Screen.height;

        rect = new Rect(0, 0, w, h * 4 / 100);

        style = new GUIStyle();
        style.alignment = TextAnchor.UpperRight;
        style.fontSize = 18;
        style.font = _font;
        style.normal.textColor = Color.gray;

        StartCoroutine("WorstReset");

    }

    private void Start()
    {
        KeySettingData[] keyData = SaveAndLoadManager.instance.LoadKey(false);
        if (keyData == null)
        {
            JoyStickKey.Add(JoystickKeySet.KEY_ATTACK, KeyCode.JoystickButton5);
            JoyStickKey.Add(JoystickKeySet.KEY_ATTACK2, KeyCode.JoystickButton4);
            JoyStickKey.Add(JoystickKeySet.KEY_SKILL, KeyCode.JoystickButton3);
            JoyStickKey.Add(JoystickKeySet.KEY_SLIDE, KeyCode.JoystickButton2);

            JoyStickKey.Add(JoystickKeySet.KEY_OPTION, KeyCode.Joystick6Button0);
            JoyStickKey.Add(JoystickKeySet.KEY_RESTART, KeyCode.Joystick6Button1);
            JoyStickKey.Add(JoystickKeySet.KEY_QUIT, KeyCode.Joystick6Button3);
            JoyStickKey.Add(JoystickKeySet.KEY_MAP, KeyCode.Joystick6Button4);

            JoyStickKey.Add(JoystickKeySet.KEY_RECYCLE, KeyCode.Joystick7Button1);
            JoyStickKey.Add(JoystickKeySet.KEY_ACTIVE, KeyCode.Joystick7Button0);

            JoyStickKey.Add(JoystickKeySet.KEY_CONFIRM, KeyCode.JoystickButton0);
            JoyStickKey.Add(JoystickKeySet.KEY_CANCLE, KeyCode.JoystickButton1);

            JoyStickKey.Add(JoystickKeySet.KEY_ARROW, KeyCode.Joystick8Button0);

            SaveAndLoadManager.instance.SaveKey(KeyCode.JoystickButton5, 0, false);
            SaveAndLoadManager.instance.SaveKey(KeyCode.JoystickButton4, 1, false);
            SaveAndLoadManager.instance.SaveKey(KeyCode.JoystickButton3, 2, false);
            SaveAndLoadManager.instance.SaveKey(KeyCode.JoystickButton2, 3, false);

            SaveAndLoadManager.instance.SaveKey(KeyCode.Joystick6Button0, 4, false);
            SaveAndLoadManager.instance.SaveKey(KeyCode.Joystick6Button1, 5, false);
            SaveAndLoadManager.instance.SaveKey(KeyCode.Joystick6Button3, 6, false);
            SaveAndLoadManager.instance.SaveKey(KeyCode.Joystick6Button4, 7, false);

            SaveAndLoadManager.instance.SaveKey(KeyCode.Joystick7Button1, 8, false);
            SaveAndLoadManager.instance.SaveKey(KeyCode.Joystick7Button0, 9, false);

            SaveAndLoadManager.instance.SaveKey(KeyCode.JoystickButton0, 10, false);
            SaveAndLoadManager.instance.SaveKey(KeyCode.JoystickButton1, 11, false);

            SaveAndLoadManager.instance.SaveKey(KeyCode.Joystick8Button0, 12, false);
        }
        else
        {
            for (int i = 0; i < keyData.Length; i++)
            {
                JoyStickKey.Add((JoystickKeySet)keyData[i]._num, (KeyCode)keyData[i]._code);
            }
        }
        KeySettingData[] keyData_Keyboard = SaveAndLoadManager.instance.LoadKey(true);
        if (keyData_Keyboard == null)
        {
            KeyboardKey.Add(JoystickKeySet.KEY_ATTACK, KeyCode.Mouse0);
            KeyboardKey.Add(JoystickKeySet.KEY_ATTACK2, KeyCode.Mouse1);
            KeyboardKey.Add(JoystickKeySet.KEY_SKILL, KeyCode.Q);
            KeyboardKey.Add(JoystickKeySet.KEY_SLIDE, KeyCode.Space);

            KeyboardKey.Add(JoystickKeySet.KEY_OPTION, KeyCode.O);
            KeyboardKey.Add(JoystickKeySet.KEY_RESTART, KeyCode.X);
            KeyboardKey.Add(JoystickKeySet.KEY_QUIT, KeyCode.E);
            KeyboardKey.Add(JoystickKeySet.KEY_MAP, KeyCode.Tab);

            KeyboardKey.Add(JoystickKeySet.KEY_RECYCLE, KeyCode.R);
            KeyboardKey.Add(JoystickKeySet.KEY_ACTIVE, KeyCode.F);

            KeyboardKey.Add(JoystickKeySet.KEY_CANCLE, KeyCode.None);
            KeyboardKey.Add(JoystickKeySet.KEY_CONFIRM, KeyCode.None);
            KeyboardKey.Add(JoystickKeySet.KEY_ARROW, KeyCode.None);

            KeyboardKey.Add(JoystickKeySet.KEY_ARROW_UP, KeyCode.W);
            KeyboardKey.Add(JoystickKeySet.KEY_ARROW_DOWN, KeyCode.S);
            KeyboardKey.Add(JoystickKeySet.KEY_ARROW_LEFT, KeyCode.A);
            KeyboardKey.Add(JoystickKeySet.KEY_ARROW_RIGHT, KeyCode.D);

            SaveAndLoadManager.instance.SaveKey(KeyCode.Mouse0, 0, true);
            SaveAndLoadManager.instance.SaveKey(KeyCode.Mouse1, 1, true);
            SaveAndLoadManager.instance.SaveKey(KeyCode.Q, 2, true);
            SaveAndLoadManager.instance.SaveKey(KeyCode.Space, 3, true);

            SaveAndLoadManager.instance.SaveKey(KeyCode.O, 4, true);
            SaveAndLoadManager.instance.SaveKey(KeyCode.X, 5, true);
            SaveAndLoadManager.instance.SaveKey(KeyCode.E, 6, true);
            SaveAndLoadManager.instance.SaveKey(KeyCode.Tab, 7, true);

            SaveAndLoadManager.instance.SaveKey(KeyCode.R, 8, true);
            SaveAndLoadManager.instance.SaveKey(KeyCode.F, 9, true);

            SaveAndLoadManager.instance.SaveKey(KeyCode.None, 10, true);
            SaveAndLoadManager.instance.SaveKey(KeyCode.None, 11, true);
            SaveAndLoadManager.instance.SaveKey(KeyCode.None, 12, true);

            SaveAndLoadManager.instance.SaveKey(KeyCode.W, 13, true);
            SaveAndLoadManager.instance.SaveKey(KeyCode.S, 14, true);
            SaveAndLoadManager.instance.SaveKey(KeyCode.A, 15, true);
            SaveAndLoadManager.instance.SaveKey(KeyCode.D, 16, true);
        }
        else
        {
            for (int i = 0; i < keyData_Keyboard.Length; i++)
            {
                KeyboardKey.Add((JoystickKeySet)keyData_Keyboard[i]._num, (KeyCode)keyData_Keyboard[i]._code);
            }
        }

    }

    public void LoadKey(bool _isJoy)
    {
        if(_isJoy)
        {
            JoyStickKey.Clear();
            KeySettingData[] keyData = SaveAndLoadManager.instance.LoadKey(false);
            for (int i = 0; i < keyData.Length; i++)
            {
                JoyStickKey.Add((JoystickKeySet)keyData[i]._num, (KeyCode)keyData[i]._code);
            }
        }
        else
        {
            KeyboardKey.Clear();
            KeySettingData[] keyData_Keyboard = SaveAndLoadManager.instance.LoadKey(true);
            for (int i = 0; i < keyData_Keyboard.Length; i++)
            {
                KeyboardKey.Add((JoystickKeySet)keyData_Keyboard[i]._num, (KeyCode)keyData_Keyboard[i]._code);
            }
        }
    }

    public void ReSetKeyBoard()
    {
        KeyboardKey.Clear();

        KeyboardKey.Add(JoystickKeySet.KEY_ATTACK, KeyCode.Mouse0);
        KeyboardKey.Add(JoystickKeySet.KEY_ATTACK2, KeyCode.Mouse1);
        KeyboardKey.Add(JoystickKeySet.KEY_SKILL, KeyCode.Q);
        KeyboardKey.Add(JoystickKeySet.KEY_SLIDE, KeyCode.Space);

        KeyboardKey.Add(JoystickKeySet.KEY_OPTION, KeyCode.O);
        KeyboardKey.Add(JoystickKeySet.KEY_RESTART, KeyCode.X);
        KeyboardKey.Add(JoystickKeySet.KEY_QUIT, KeyCode.E);
        KeyboardKey.Add(JoystickKeySet.KEY_MAP, KeyCode.Tab);

        KeyboardKey.Add(JoystickKeySet.KEY_RECYCLE, KeyCode.R);
        KeyboardKey.Add(JoystickKeySet.KEY_ACTIVE, KeyCode.F);

        KeyboardKey.Add(JoystickKeySet.KEY_CANCLE, KeyCode.None);
        KeyboardKey.Add(JoystickKeySet.KEY_CONFIRM, KeyCode.None);
        KeyboardKey.Add(JoystickKeySet.KEY_ARROW, KeyCode.None);

        KeyboardKey.Add(JoystickKeySet.KEY_ARROW_UP, KeyCode.W);
        KeyboardKey.Add(JoystickKeySet.KEY_ARROW_DOWN, KeyCode.S);
        KeyboardKey.Add(JoystickKeySet.KEY_ARROW_LEFT, KeyCode.A);
        KeyboardKey.Add(JoystickKeySet.KEY_ARROW_RIGHT, KeyCode.D);

        SaveAndLoadManager.instance.SaveKey(KeyCode.Mouse0, 0, true);
        SaveAndLoadManager.instance.SaveKey(KeyCode.Mouse1, 1, true);
        SaveAndLoadManager.instance.SaveKey(KeyCode.Q, 2, true);
        SaveAndLoadManager.instance.SaveKey(KeyCode.Space, 3, true);

        SaveAndLoadManager.instance.SaveKey(KeyCode.O, 4, true);
        SaveAndLoadManager.instance.SaveKey(KeyCode.X, 5, true);
        SaveAndLoadManager.instance.SaveKey(KeyCode.E, 6, true);
        SaveAndLoadManager.instance.SaveKey(KeyCode.Tab, 7, true);

        SaveAndLoadManager.instance.SaveKey(KeyCode.R, 8, true);
        SaveAndLoadManager.instance.SaveKey(KeyCode.F, 9, true);

        SaveAndLoadManager.instance.SaveKey(KeyCode.None, 10, true);
        SaveAndLoadManager.instance.SaveKey(KeyCode.None, 11, true);
        SaveAndLoadManager.instance.SaveKey(KeyCode.None, 12, true);

        SaveAndLoadManager.instance.SaveKey(KeyCode.W, 13, true);
        SaveAndLoadManager.instance.SaveKey(KeyCode.S, 14, true);
        SaveAndLoadManager.instance.SaveKey(KeyCode.A, 15, true);
        SaveAndLoadManager.instance.SaveKey(KeyCode.D, 16, true);
    }

    IEnumerator WorstReset() //코루틴으로 15초 간격으로 최저 프레임 리셋해줌.
    {
        while (true)
        {
            yield return new WaitForSecondsRealtime(15f);
            worstFps = 100f;
        }
    }


    void Update()
    {
        deltaTime += (Time.unscaledDeltaTime - deltaTime) * 0.1f;
    }

    void OnGUI()//소스로 GUI 표시.
    {

        msec = deltaTime * 1000.0f;
        fps = 1.0f / deltaTime;  //초당 프레임 - 1초에
        if (fps < worstFps)  //새로운 최저 fps가 나왔다면 worstFps 바꿔줌.
            worstFps = fps;
        //text = msec.ToString("F1") + "ms (" + fps.ToString("F1") + ") //worst : " + worstFps.ToString("F1");
        text = fps.ToString("F0");
        GUI.Label(rect, text, style);
    }
}
