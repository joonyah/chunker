﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MouseAim : MonoBehaviour
{
    public GameObject player;


    // Update is called once per frame
    void Update()
    {

        if (player == null)
        {
            if(PDG.Player.instance != null)
            {
                player = (PDG.Player.instance?.gameObject);
            }
        }
        else
        {
            if (PDG.Dungeon.instance.isJoystrick)
            {
                float x = 0;
                float y = 0;
                if (!MemorialManager.instance.isOpen)
                {
                    if (PDG.Dungeon.instance.joystricks[0].Contains("DS4") || PDG.Dungeon.instance.joystricks[0].Contains("PC") || PDG.Dungeon.instance.joystricks[0].Contains("Wireless"))
                    {
                        x = Input.GetAxis("PS4_subX");
                        y = Input.GetAxis("PS4_subY");
                    }
                    else
                    {
                        x = Input.GetAxis("Horizontal_sub");
                        y = Input.GetAxis("Vertical_sub");
                    }
                }

                if ((x >= -0.1f && x <= 0.1f) && (y >= -0.1f && y <= 0.1f))
                {
                    GetComponent<Image>().enabled = false;
                }
                else
                {
                    GetComponent<Image>().enabled = true;
                    Vector3 pPos = PDG.Player.instance.transform.position;
                    Vector3 dir = pPos + new Vector3(x, y);
                    dir -= pPos;
                    dir.Normalize();
                    transform.position = Camera.main.WorldToScreenPoint(pPos + (dir * 5));
                }
            }
            else
            {
                if (!GetComponent<Image>().enabled) GetComponent<Image>().enabled = true;
                var pos = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
                //pos = Camera.main.ScreenToWorldPoint(pos);
                transform.position = pos;
            }
        }
    }
}
