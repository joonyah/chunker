﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

//마우스 방향에 따라 플레이어가 바라보고 있을 방향을 지정
public enum PlayerLookDirection
{
    Up,
    Down,
    UpLeft,
    UpRight,
    DownLeft,
    DownRight,
    Left,   //InputDirection전용
    Right   //InputDirection전용
}

public enum PlayerState
{
    Idle,
    Walk,
    Avoid,
    Fall,
    Health,
    Swing,
    Predators,
    DeepImpact,
    Die
}

public class PlayerCombat : MonoBehaviour
{
    public Coroutine tCoroutine = null;
    public static PlayerCombat instance;
    public static PlayerLookDirection pl = PlayerLookDirection.Down;       //다른곳에서도 플레이어가 바라보는곳에 접근 가능하도록 설정
    public static PlayerState ps = PlayerState.Idle;
    public static Vector3 fallPosition = Vector3.zero;
    public static bool isCompulsionMove = false;
    PlayerLookDirection InputDirection;

    public GameObject weapon;
    public BoxCollider2D coll;
    public CircleCollider2D cColl;

    Vector3 slideDir;
    Vector2 InputDir;

    public float moveSpeed;
    public float dodgeRollSpeed;
    public float dodgeRollCooltime;
    private float slideSpeed;

    public float testValue = 20.0f;

    public bool isFalling = false;

    public float fMoveSpeedTime_stategic_03 = 0.0f;
    private float avoidTimeNow = 0.5f;
    private float avoidTime = 0.5f;
    public MouseAim aim;

    private void Awake()
    {
        if (instance == null) instance = this;
        pl = PlayerLookDirection.Down;
        ps = PlayerState.Idle;
    }

    private void Update()
    {
        if (aim == null) aim = GameObject.Find("Canvas").transform.Find("CrossHair").GetComponent<MouseAim>();
        if (CustomManager.instance != null && CustomManager.instance.isOpen) return;
        if (fMoveSpeedTime_stategic_03 > 0.0f) fMoveSpeedTime_stategic_03 -= Time.deltaTime;
        else
        {
            if (PDG.Player.instance.transform.Find("GreenMiniZone").gameObject.activeSelf)
                PDG.Player.instance.transform.Find("GreenMiniZone").gameObject.SetActive(false);
        }
        avoidTimeNow += Time.deltaTime;
        switch (ps)
        {
            case PlayerState.Fall:
            case PlayerState.Health:
                PDG.Player.instance.hero.WeaponsSlot.SetActive(false);
                break;
            case PlayerState.Predators:
                PDG.Player.instance.hero.WeaponsSlot.SetActive(false);
                break;
            case PlayerState.DeepImpact:
                PDG.Player.instance.hero.WeaponsSlot.SetActive(false);
                break;
            case PlayerState.Idle:
            case PlayerState.Walk:
            case PlayerState.Swing:
                {
                    if (!PDG.Player.instance.hero.isBack) PlayerCombatMoving();
                    break;
                }
            case PlayerState.Avoid:
                PDG.Player.instance.hero.WeaponsSlot.SetActive(false);
                HandleDodgeRoll();
                break;
        }
        PlayerInputDirection();
    }
    //플레이어 이동
    private void PlayerCombatMoving()
    {
        if (PDG.Player.instance.isLock) return;
        if (PDG.Player.instance.isFreeze) return;
        if (PDG.Player.instance.isDie) return;
        if (PDG.Dungeon.instance.isOpenPortal) return;
        if (Dialogue.Instance.isCommnet) return;
        if (ps == PlayerState.Health) return;
        if (ps == PlayerState.Predators) return;
        if (ps == PlayerState.DeepImpact) return;
        if (PDG.Dungeon.instance.isPause) return;
        if (CustomManager.instance != null && CustomManager.instance.isOpen) return;
        if (BodyRemodelingManager.instance != null && BodyRemodelingManager.instance.isOpen) return;
        if (BossRushListManager.instance != null && BossRushListManager.instance.isOpen) return;
        if (MakeController.instance != null && MakeController.instance.isOpen) return;
        if (CollectionManager.instance != null && CollectionManager.instance.isOpen) return;
        if (StateGroup.instance != null && StateGroup.instance.isOpen) return;
        if (MemorialManager.instance != null && MemorialManager.instance.isOpen) return;
        if (EquipmentUpgradeManager.instance != null && EquipmentUpgradeManager.instance.isOpen) return;
        if (DifficultyManager.instance != null && DifficultyManager.instance.isOpen) return;

        if (ps == PlayerState.Idle || ps == PlayerState.Walk || ps == PlayerState.Swing)
        {
            float x = Input.GetAxisRaw("Horizontal_joy");
            float y = Input.GetAxisRaw("Vertical_joy");

            float x2 = 0;
            float y2 = 0;
            if (PDG.Dungeon.instance.joystricks.Count > 0 && (PDG.Dungeon.instance.joystricks[0].Contains("DS4") || PDG.Dungeon.instance.joystricks[0].Contains("PC") || PDG.Dungeon.instance.joystricks[0].Contains("Wireless")))
            {
                x2 = Input.GetAxis("PS4_subX");
                y2 = Input.GetAxis("PS4_subY");
            }
            else
            {
                x2 = Input.GetAxis("Horizontal_sub");
                y2 = Input.GetAxis("Vertical_sub");
            }


            if (isCompulsionMove)
            {
                float moveSpeedComplete = (fMoveSpeedTime_stategic_03 > 0.0f ? moveSpeed * testValue * 1.2f : moveSpeed * testValue);
                moveSpeedComplete = moveSpeedComplete + (moveSpeedComplete * PDG.Player.instance.plusMoveSpeed);
                if (PDG.Player.instance.volatileBuffType == 2) moveSpeedComplete += 5;
                if (PDG.Player.instance.isShield) moveSpeedComplete /= 2;
                if (moveSpeedComplete > 7) moveSpeedComplete = 7;

                if (PDG.Player.instance.isFreeze2)
                    moveSpeedComplete = 2;

                //Up
                if (PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ARROW_UP], false) || y > 0)
                {
                    if (tCoroutine != null)
                    {
                        if (PDG.Dungeon.instance.newObjectSelectedObj != null && PDG.Dungeon.instance.newObjectSelectedObj.GetComponent<HpRecoveryController>()) PDG.Dungeon.instance.newObjectSelectedObj.GetComponent<HpRecoveryController>().isUsing = false;
                        if (PDG.Dungeon.instance.newObjectSelectedObj != null && PDG.Dungeon.instance.newObjectSelectedObj.GetComponent<UpgradeController>()) PDG.Dungeon.instance.newObjectSelectedObj.GetComponent<UpgradeController>().isUsing = false;
                        if (PDG.Dungeon.instance.newObjectSelectedObj != null && PDG.Dungeon.instance.newObjectSelectedObj.GetComponent<DungeonSpecialObject>()) PDG.Dungeon.instance.newObjectSelectedObj.GetComponent<DungeonSpecialObject>().isUsing = false;
                        if (PDG.Dungeon.instance.newObjectSelectedObj != null && PDG.Dungeon.instance.newObjectSelectedObj.GetComponent<NPC>()) PDG.Dungeon.instance.newObjectSelectedObj.GetComponent<NPC>().isUsing = false;
                        if (PDG.Dungeon.instance.newObjectSelectedObj != null && PDG.Dungeon.instance.newObjectSelectedObj.GetComponent<NewShopController>()) PDG.Dungeon.instance.newObjectSelectedObj.GetComponent<NewShopController>().isUse = false;
                        if (PDG.Dungeon.instance.newObjectSelectedObj != null && PDG.Dungeon.instance.newObjectSelectedObj.GetComponent<MOB.Monster>()) PDG.Dungeon.instance.newObjectSelectedObj.GetComponent<MOB.Monster>().isUsing = false;
                    }
                    RaycastHit2D hit = Physics2D.Raycast(transform.position, Vector2.up, 1, 1 << 8 | 1 << 29);
                    if (!hit)
                    {
                        gameObject.transform.Translate(Vector2.up / 2 * moveSpeedComplete * Time.deltaTime * PDG.Hero.avoidSpeed);
                    }
                    if (ps != PlayerState.Swing) ps = PlayerState.Walk;
                }
                //Down
                if (PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ARROW_DOWN], false) || y < 0)
                {
                    if (tCoroutine != null)
                    {
                        if (PDG.Dungeon.instance.newObjectSelectedObj != null && PDG.Dungeon.instance.newObjectSelectedObj.GetComponent<HpRecoveryController>()) PDG.Dungeon.instance.newObjectSelectedObj.GetComponent<HpRecoveryController>().isUsing = false;
                        if (PDG.Dungeon.instance.newObjectSelectedObj != null && PDG.Dungeon.instance.newObjectSelectedObj.GetComponent<UpgradeController>()) PDG.Dungeon.instance.newObjectSelectedObj.GetComponent<UpgradeController>().isUsing = false;
                        if (PDG.Dungeon.instance.newObjectSelectedObj != null && PDG.Dungeon.instance.newObjectSelectedObj.GetComponent<DungeonSpecialObject>()) PDG.Dungeon.instance.newObjectSelectedObj.GetComponent<DungeonSpecialObject>().isUsing = false;
                        if (PDG.Dungeon.instance.newObjectSelectedObj != null && PDG.Dungeon.instance.newObjectSelectedObj.GetComponent<NPC>()) PDG.Dungeon.instance.newObjectSelectedObj.GetComponent<NPC>().isUsing = false;
                        if (PDG.Dungeon.instance.newObjectSelectedObj != null && PDG.Dungeon.instance.newObjectSelectedObj.GetComponent<NewShopController>()) PDG.Dungeon.instance.newObjectSelectedObj.GetComponent<NewShopController>().isUse = false;
                        if (PDG.Dungeon.instance.newObjectSelectedObj != null && PDG.Dungeon.instance.newObjectSelectedObj.GetComponent<MOB.Monster>()) PDG.Dungeon.instance.newObjectSelectedObj.GetComponent<MOB.Monster>().isUsing = false;
                    }
                    RaycastHit2D hit = Physics2D.Raycast(transform.position, Vector2.down, 1, 1 << 8 | 1 << 29);
                    if (!hit)
                    {
                        gameObject.transform.Translate(Vector2.down / 2 * moveSpeedComplete * Time.deltaTime * PDG.Hero.avoidSpeed);
                    }
                    if (ps != PlayerState.Swing) ps = PlayerState.Walk;
                }
                //Left
                if (PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ARROW_LEFT], false) || x < 0)
                {
                    if (tCoroutine != null)
                    {
                        if (PDG.Dungeon.instance.newObjectSelectedObj != null && PDG.Dungeon.instance.newObjectSelectedObj.GetComponent<HpRecoveryController>()) PDG.Dungeon.instance.newObjectSelectedObj.GetComponent<HpRecoveryController>().isUsing = false;
                        if (PDG.Dungeon.instance.newObjectSelectedObj != null && PDG.Dungeon.instance.newObjectSelectedObj.GetComponent<UpgradeController>()) PDG.Dungeon.instance.newObjectSelectedObj.GetComponent<UpgradeController>().isUsing = false;
                        if (PDG.Dungeon.instance.newObjectSelectedObj != null && PDG.Dungeon.instance.newObjectSelectedObj.GetComponent<DungeonSpecialObject>()) PDG.Dungeon.instance.newObjectSelectedObj.GetComponent<DungeonSpecialObject>().isUsing = false;
                        if (PDG.Dungeon.instance.newObjectSelectedObj != null && PDG.Dungeon.instance.newObjectSelectedObj.GetComponent<NPC>()) PDG.Dungeon.instance.newObjectSelectedObj.GetComponent<NPC>().isUsing = false;
                        if (PDG.Dungeon.instance.newObjectSelectedObj != null && PDG.Dungeon.instance.newObjectSelectedObj.GetComponent<NewShopController>()) PDG.Dungeon.instance.newObjectSelectedObj.GetComponent<NewShopController>().isUse = false;
                        if (PDG.Dungeon.instance.newObjectSelectedObj != null && PDG.Dungeon.instance.newObjectSelectedObj.GetComponent<MOB.Monster>()) PDG.Dungeon.instance.newObjectSelectedObj.GetComponent<MOB.Monster>().isUsing = false;
                    }
                    RaycastHit2D hit = Physics2D.Raycast(transform.position, Vector2.left, 1, 1 << 8 | 1 << 29);
                    if (!hit)
                    {
                        gameObject.transform.Translate(Vector2.left / 2 * moveSpeedComplete * Time.deltaTime * PDG.Hero.avoidSpeed);
                    }
                    if (ps != PlayerState.Swing) ps = PlayerState.Walk;
                }
                //Right
                if (PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ARROW_RIGHT], false) || x > 0)
                {
                    if (tCoroutine != null)
                    {
                        if (PDG.Dungeon.instance.newObjectSelectedObj != null && PDG.Dungeon.instance.newObjectSelectedObj.GetComponent<HpRecoveryController>()) PDG.Dungeon.instance.newObjectSelectedObj.GetComponent<HpRecoveryController>().isUsing = false;
                        if (PDG.Dungeon.instance.newObjectSelectedObj != null && PDG.Dungeon.instance.newObjectSelectedObj.GetComponent<UpgradeController>()) PDG.Dungeon.instance.newObjectSelectedObj.GetComponent<UpgradeController>().isUsing = false;
                        if (PDG.Dungeon.instance.newObjectSelectedObj != null && PDG.Dungeon.instance.newObjectSelectedObj.GetComponent<DungeonSpecialObject>()) PDG.Dungeon.instance.newObjectSelectedObj.GetComponent<DungeonSpecialObject>().isUsing = false;
                        if (PDG.Dungeon.instance.newObjectSelectedObj != null && PDG.Dungeon.instance.newObjectSelectedObj.GetComponent<NPC>()) PDG.Dungeon.instance.newObjectSelectedObj.GetComponent<NPC>().isUsing = false;
                        if (PDG.Dungeon.instance.newObjectSelectedObj != null && PDG.Dungeon.instance.newObjectSelectedObj.GetComponent<NewShopController>()) PDG.Dungeon.instance.newObjectSelectedObj.GetComponent<NewShopController>().isUse = false;
                        if (PDG.Dungeon.instance.newObjectSelectedObj != null && PDG.Dungeon.instance.newObjectSelectedObj.GetComponent<MOB.Monster>()) PDG.Dungeon.instance.newObjectSelectedObj.GetComponent<MOB.Monster>().isUsing = false;
                    }
                    RaycastHit2D hit = Physics2D.Raycast(transform.position, Vector2.right, 1, 1 << 8 | 1 << 29);
                    if (!hit)
                    {
                        gameObject.transform.Translate(Vector2.right / 2 * moveSpeedComplete * Time.deltaTime * PDG.Hero.avoidSpeed);
                    }
                    if (ps != PlayerState.Swing) ps = PlayerState.Walk;
                }
            }
            else
            {
                float moveSpeedComplete = (fMoveSpeedTime_stategic_03 > 0.0f ? moveSpeed * 1.2f : moveSpeed) + (PDG.Player.instance.isRollingTransform ? 0 : 0);
                moveSpeedComplete = moveSpeedComplete + (moveSpeedComplete * PDG.Player.instance.plusMoveSpeed);
                if (PDG.Player.instance.volatileBuffType == 2) moveSpeedComplete += 5;
                if (PDG.Player.instance.isShield) moveSpeedComplete /= 2;
                if (moveSpeedComplete > 7) moveSpeedComplete = 7;
                if (PDG.Player.instance.isFreeze2)
                    moveSpeedComplete = 2;
                //Up
                if (PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ARROW_UP], false) || y > 0)
                {
                    if (tCoroutine != null)
                    {
                        if (PDG.Dungeon.instance.newObjectSelectedObj != null && PDG.Dungeon.instance.newObjectSelectedObj.GetComponent<HpRecoveryController>()) PDG.Dungeon.instance.newObjectSelectedObj.GetComponent<HpRecoveryController>().isUsing = false;
                        if (PDG.Dungeon.instance.newObjectSelectedObj != null && PDG.Dungeon.instance.newObjectSelectedObj.GetComponent<UpgradeController>()) PDG.Dungeon.instance.newObjectSelectedObj.GetComponent<UpgradeController>().isUsing = false;
                        if (PDG.Dungeon.instance.newObjectSelectedObj != null && PDG.Dungeon.instance.newObjectSelectedObj.GetComponent<DungeonSpecialObject>()) PDG.Dungeon.instance.newObjectSelectedObj.GetComponent<DungeonSpecialObject>().isUsing = false;
                        if (PDG.Dungeon.instance.newObjectSelectedObj != null && PDG.Dungeon.instance.newObjectSelectedObj.GetComponent<NPC>()) PDG.Dungeon.instance.newObjectSelectedObj.GetComponent<NPC>().isUsing = false;
                        if (PDG.Dungeon.instance.newObjectSelectedObj != null && PDG.Dungeon.instance.newObjectSelectedObj.GetComponent<NewShopController>()) PDG.Dungeon.instance.newObjectSelectedObj.GetComponent<NewShopController>().isUse = false;
                        if (PDG.Dungeon.instance.newObjectSelectedObj != null && PDG.Dungeon.instance.newObjectSelectedObj.GetComponent<MOB.Monster>()) PDG.Dungeon.instance.newObjectSelectedObj.GetComponent<MOB.Monster>().isUsing = false;
                    }
                    RaycastHit2D hit = Physics2D.Raycast(transform.position, Vector2.up, moveSpeedComplete * Time.deltaTime * PDG.Hero.avoidSpeed , 1 << 8 | 1 << 29);
                    if (!hit)
                    {
                        gameObject.transform.Translate(Vector2.up * moveSpeedComplete * Time.deltaTime * PDG.Hero.avoidSpeed);
                    }
                    if (ps != PlayerState.Swing) ps = PlayerState.Walk;
                }
                //Down
                if (PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ARROW_DOWN], false) || y < 0)
                {
                    if (tCoroutine != null)
                    {
                        if (PDG.Dungeon.instance.newObjectSelectedObj != null && PDG.Dungeon.instance.newObjectSelectedObj.GetComponent<HpRecoveryController>()) PDG.Dungeon.instance.newObjectSelectedObj.GetComponent<HpRecoveryController>().isUsing = false;
                        if (PDG.Dungeon.instance.newObjectSelectedObj != null && PDG.Dungeon.instance.newObjectSelectedObj.GetComponent<UpgradeController>()) PDG.Dungeon.instance.newObjectSelectedObj.GetComponent<UpgradeController>().isUsing = false;
                        if (PDG.Dungeon.instance.newObjectSelectedObj != null && PDG.Dungeon.instance.newObjectSelectedObj.GetComponent<DungeonSpecialObject>()) PDG.Dungeon.instance.newObjectSelectedObj.GetComponent<DungeonSpecialObject>().isUsing = false;
                        if (PDG.Dungeon.instance.newObjectSelectedObj != null && PDG.Dungeon.instance.newObjectSelectedObj.GetComponent<NPC>()) PDG.Dungeon.instance.newObjectSelectedObj.GetComponent<NPC>().isUsing = false;
                        if (PDG.Dungeon.instance.newObjectSelectedObj != null && PDG.Dungeon.instance.newObjectSelectedObj.GetComponent<NewShopController>()) PDG.Dungeon.instance.newObjectSelectedObj.GetComponent<NewShopController>().isUse = false;
                        if (PDG.Dungeon.instance.newObjectSelectedObj != null && PDG.Dungeon.instance.newObjectSelectedObj.GetComponent<MOB.Monster>()) PDG.Dungeon.instance.newObjectSelectedObj.GetComponent<MOB.Monster>().isUsing = false;
                    }
                    RaycastHit2D hit = Physics2D.Raycast(transform.position, Vector2.down, moveSpeedComplete * Time.deltaTime * PDG.Hero.avoidSpeed , 1 << 8 | 1 << 29);
                    if (!hit)
                    {
                        gameObject.transform.Translate(Vector2.down * moveSpeedComplete * Time.deltaTime * PDG.Hero.avoidSpeed);
                    }
                    if (ps != PlayerState.Swing) ps = PlayerState.Walk;
                }
                //Left
                if (PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ARROW_LEFT], false) || x < 0)
                {
                    if (tCoroutine != null)
                    {
                        if (PDG.Dungeon.instance.newObjectSelectedObj != null && PDG.Dungeon.instance.newObjectSelectedObj.GetComponent<HpRecoveryController>()) PDG.Dungeon.instance.newObjectSelectedObj.GetComponent<HpRecoveryController>().isUsing = false;
                        if (PDG.Dungeon.instance.newObjectSelectedObj != null && PDG.Dungeon.instance.newObjectSelectedObj.GetComponent<UpgradeController>()) PDG.Dungeon.instance.newObjectSelectedObj.GetComponent<UpgradeController>().isUsing = false;
                        if (PDG.Dungeon.instance.newObjectSelectedObj != null && PDG.Dungeon.instance.newObjectSelectedObj.GetComponent<DungeonSpecialObject>()) PDG.Dungeon.instance.newObjectSelectedObj.GetComponent<DungeonSpecialObject>().isUsing = false;
                        if (PDG.Dungeon.instance.newObjectSelectedObj != null && PDG.Dungeon.instance.newObjectSelectedObj.GetComponent<NPC>()) PDG.Dungeon.instance.newObjectSelectedObj.GetComponent<NPC>().isUsing = false;
                        if (PDG.Dungeon.instance.newObjectSelectedObj != null && PDG.Dungeon.instance.newObjectSelectedObj.GetComponent<NewShopController>()) PDG.Dungeon.instance.newObjectSelectedObj.GetComponent<NewShopController>().isUse = false;
                        if (PDG.Dungeon.instance.newObjectSelectedObj != null && PDG.Dungeon.instance.newObjectSelectedObj.GetComponent<MOB.Monster>()) PDG.Dungeon.instance.newObjectSelectedObj.GetComponent<MOB.Monster>().isUsing = false;
                    }
                    RaycastHit2D hit = Physics2D.Raycast(transform.position, Vector2.left, moveSpeedComplete * Time.deltaTime * PDG.Hero.avoidSpeed , 1 << 8 | 1 << 29);
                    if (!hit)
                    {
                        gameObject.transform.Translate(Vector2.left * moveSpeedComplete * Time.deltaTime * PDG.Hero.avoidSpeed);
                    }
                    if (ps != PlayerState.Swing) ps = PlayerState.Walk;
                }
                //Right
                if (PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ARROW_RIGHT], false) || x > 0)
                {
                    if (tCoroutine != null)
                    {
                        if (PDG.Dungeon.instance.newObjectSelectedObj != null && PDG.Dungeon.instance.newObjectSelectedObj.GetComponent<HpRecoveryController>()) PDG.Dungeon.instance.newObjectSelectedObj.GetComponent<HpRecoveryController>().isUsing = false;
                        if (PDG.Dungeon.instance.newObjectSelectedObj != null && PDG.Dungeon.instance.newObjectSelectedObj.GetComponent<UpgradeController>()) PDG.Dungeon.instance.newObjectSelectedObj.GetComponent<UpgradeController>().isUsing = false;
                        if (PDG.Dungeon.instance.newObjectSelectedObj != null && PDG.Dungeon.instance.newObjectSelectedObj.GetComponent<DungeonSpecialObject>()) PDG.Dungeon.instance.newObjectSelectedObj.GetComponent<DungeonSpecialObject>().isUsing = false;
                        if (PDG.Dungeon.instance.newObjectSelectedObj != null && PDG.Dungeon.instance.newObjectSelectedObj.GetComponent<NPC>()) PDG.Dungeon.instance.newObjectSelectedObj.GetComponent<NPC>().isUsing = false;
                        if (PDG.Dungeon.instance.newObjectSelectedObj != null && PDG.Dungeon.instance.newObjectSelectedObj.GetComponent<NewShopController>()) PDG.Dungeon.instance.newObjectSelectedObj.GetComponent<NewShopController>().isUse = false;
                        if (PDG.Dungeon.instance.newObjectSelectedObj != null && PDG.Dungeon.instance.newObjectSelectedObj.GetComponent<MOB.Monster>()) PDG.Dungeon.instance.newObjectSelectedObj.GetComponent<MOB.Monster>().isUsing = false;
                    }
                    RaycastHit2D hit = Physics2D.Raycast(transform.position, Vector2.right, moveSpeedComplete * Time.deltaTime * PDG.Hero.avoidSpeed , 1 << 8 | 1 << 29);
                    if (!hit)
                    {
                        gameObject.transform.Translate(Vector2.right * moveSpeedComplete * Time.deltaTime * PDG.Hero.avoidSpeed);
                    }
                    if (ps != PlayerState.Swing) ps = PlayerState.Walk;
                }
            }

            if ((x2 >= -0.1f && x2 <= 0.1f) && (y2 >= -0.1f && y2 <= 0.1f) && (x != 0 || y != 0)) 
            {
                aim.GetComponent<Image>().enabled = false;
                Vector3 pPos = PDG.Player.instance.transform.position;
                Vector3 dir = pPos + new Vector3(x, y);
                dir -= pPos;
                dir.Normalize();
                aim.transform.position = Camera.main.WorldToScreenPoint(pPos + (dir * 5));
            }
        }

        if (((Input.GetAxisRaw("Horizontal") == 0 && Input.GetAxisRaw("Vertical") == 0) && (Input.GetAxisRaw("Horizontal_joy") == 0 && Input.GetAxisRaw("Vertical_joy") == 0)) && ps != PlayerState.Swing)
        {
            ps = PlayerState.Idle;
        }
        else if ((PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_SLIDE], true) || PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_SLIDE], true)) && !PDG.Hero.isTeleport && !Inventorys.Instance.QuickSlots[3].sprite.name.Equals("noItem") && !PDG.Player.instance.isRollingTransform && !PDG.Player.instance.isMiniGame
            && (!PDG.Dungeon.instance.nowRoomType.Equals("boss_waiting_2") || !PDG.Dungeon.instance.isStageBoss) && avoidTimeNow > avoidTime)     //Avoid
        {
            ps = PlayerState.Avoid;
            weapon.SetActive(false);
            coll.enabled = false;
            slideDir = InputDir;
             transform.Find("Gun").Find("Gun").Find("EneryCharge").gameObject.SetActive(false);
            slideSpeed = dodgeRollSpeed;
            SoundManager.instance.StartAudio(new string[1] { "Avoid" }, VOLUME_TYPE.EFFECT);
            if(PDG.Player.instance.isShield)
            {
                PDG.Player.instance.isShield = false;
            }
            PDG.Player.instance.ShieldObj.SetActive(false);
            avoidTimeNow = 0.0f;
        }
    }
    private void HandleDodgeRoll()
    {
        RaycastHit2D hit = Physics2D.Raycast(transform.position, slideDir, 1, 1 << 8 | 1 << 29);
        if (!hit) transform.Translate(slideDir * (slideSpeed + (slideSpeed * PDG.Player.instance.plusAvoidSpeed)) * Time.deltaTime);
        slideSpeed -= slideSpeed * 5f * Time.deltaTime;
        if(slideSpeed < 2)
        {
            if (Input.GetAxisRaw("Horizontal") == 0 && Input.GetAxisRaw("Vertical") == 0 && Input.GetAxisRaw("Horizontal_joy") == 0 && Input.GetAxisRaw("Vertical_joy") == 0)
            {
                ps = PlayerState.Idle;
                slideSpeed = 0;
            }
            else
            {
                ps = PlayerState.Walk;
                slideSpeed = 0;
            }
            weapon.SetActive(true);
            if (PDG.Player.instance.isSubWeapon) weapon.transform.Find("Gun").gameObject.SetActive(true);
            coll.enabled = true;
            if (PlayerBuffController.buffList.FindIndex(item => item.Equals("strategic_03")) != -1)
            {
                fMoveSpeedTime_stategic_03 = 3.0f;
                PDG.Player.instance.transform.Find("GreenMiniZone").gameObject.SetActive(true);
            }
            if (PlayerBuffController.buffList.FindIndex(item => item.Equals("strategic_04")) != -1)
            {
                PDG.Player.instance.fInvincibleTime_strategic_04 = 1.0f;
            }


        }
    }
    //키 입력에 따라 구르는 방향을 지정해야하는 함수
    private void PlayerInputDirection()
    {
        float x = Input.GetAxisRaw("Horizontal_joy");
        float y = Input.GetAxisRaw("Vertical_joy");

        //Up
        if (PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ARROW_UP], false) || y > 0)
        {
            InputDirection = PlayerLookDirection.Up;
            InputDir = new Vector2(0, 1).normalized;
        }

        //Down
        if (PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ARROW_DOWN], false) || y < 0)
        {
            InputDirection = PlayerLookDirection.Down;
            InputDir = new Vector2(0, -1).normalized;
        }

        //Left
        if (PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ARROW_LEFT], false) || x < 0)
        {
            InputDirection = PlayerLookDirection.Left;
            InputDir = new Vector2(-1, 0).normalized;
        }

        //Right
        if (PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ARROW_RIGHT], false) || x > 0)
        {
            InputDirection = PlayerLookDirection.Right;
            InputDir = new Vector2(1, 0);
        }

        //UpLeft
        if ((PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ARROW_UP], false) && PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ARROW_LEFT], false)) || (y > 0 && x < 0)) 
        {
            InputDirection = PlayerLookDirection.UpLeft;
            InputDir = new Vector2(-1, 1).normalized;
        }

        //DownLeft
        if ((PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ARROW_DOWN], false) && PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ARROW_LEFT], false)) || (y < 0 && x < 0))
        {
            InputDirection = PlayerLookDirection.DownLeft;
            InputDir = new Vector2(-1, -1).normalized;
        }

        //UpRight
        if ((PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ARROW_UP], false) && PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ARROW_RIGHT], false)) || (y > 0 && x > 0)) 
        {
            InputDirection = PlayerLookDirection.UpRight;
            InputDir = new Vector2(1, 1).normalized;
        }

        //DownRight
        if ((PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ARROW_DOWN], false) && PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ARROW_RIGHT], false)) || (y < 0 && x > 0)) 
        {
            InputDirection = PlayerLookDirection.DownRight;
            InputDir = new Vector2(1, -1).normalized;
        }
    }
    private Vector2 GetDir()
    {
        switch(pl)
        {
            case PlayerLookDirection.Down: return new Vector2(0, -1);
            case PlayerLookDirection.DownLeft: return new Vector2(-0.5f, -0.5f);
            case PlayerLookDirection.DownRight: return new Vector2(0.5f, -0.5f);
            case PlayerLookDirection.Left: return new Vector2(-1, 0);
            case PlayerLookDirection.Right: return new Vector2(1, 0);
            case PlayerLookDirection.Up: return new Vector2(0, 1);
            case PlayerLookDirection.UpLeft: return new Vector2(-0.5f, 0.5f);
            case PlayerLookDirection.UpRight: return new Vector2(0.5f, 0.5f);
        }
        return Vector2.zero;
    }
    //낙하
    public void FallAnimationEnd()
    {
        PDG.Player.instance.transform.position = fallPosition;
        ps = PlayerState.Idle;
        PDG.Player.instance.SetDamage(1, LocalizeManager.GetLocalize("killedby_trap_falling"), DEATH_TYPE.DEATH_FALL, string.Empty);
    }
}
