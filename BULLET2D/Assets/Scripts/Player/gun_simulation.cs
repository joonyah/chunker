﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class gun_simulation : MonoBehaviour
{
    public GameObject gun;
    public GameObject player;
    public SpriteRenderer helmet;
    public static float rot;
    private MouseAim aim;
    [SerializeField] private bool isSword = false;

    private void Start()
    {
        if (aim == null) aim = GameObject.Find("Canvas").transform.Find("CrossHair").GetComponent<MouseAim>();
    }
    // Update is called once per frame
    void Update()
    {
        if (aim == null) aim = GameObject.Find("Canvas").transform.Find("CrossHair").GetComponent<MouseAim>();
        if (!player.GetComponent<PDG.Player>().isRollingTransform) 
        {
            rot = gameObject.transform.rotation.eulerAngles.z;
            WeaponRotate();
            MouseInDirection();
        }
    }
    void WeaponRotate()
    {
        if (PDG.Player.instance.nowRightWeaponName.Contains("multi_canon"))
        {
            if (PDG.Player.instance.gunControl.transform.Find("living_arrow_weapon").gameObject.activeSelf || PDG.Player.instance.swordControl.transform.Find("living_arrow_weapon").gameObject.activeSelf)
            {
                gun.GetComponent<SpriteRenderer>().flipY = false;

                if (PDG.Player.instance.gunControl.transform.Find("living_arrow_weapon"))
                    PDG.Player.instance.gunControl.transform.Find("living_arrow_weapon").GetComponent<SpriteRenderer>().flipY = false;
                if (PDG.Player.instance.swordControl.transform.Find("living_arrow_weapon"))
                    PDG.Player.instance.swordControl.transform.Find("living_arrow_weapon").GetComponent<SpriteRenderer>().flipY = false;
                transform.rotation = Quaternion.Euler(0f, 0f, 0f);
                return;
            }
        }

        //먼저 계산을 위해 마우스와 게임 오브젝트의 현재의 좌표를 임시로 저장합니다.
        Vector3 mPosition = aim.transform.position;//Input.mousePosition; //마우스 좌표 저장
        Vector3 oPosition = transform.position; //게임 오브젝트 좌표 저장

        //카메라가 앞면에서 뒤로 보고 있기 때문에, 마우스 position의 z축 정보에 
        //게임 오브젝트와 카메라와의 z축의 차이를 입력시켜줘야 합니다.
        mPosition.z = oPosition.z - Camera.main.transform.position.z;

        //화면의 픽셀별로 변화되는 마우스의 좌표를 유니티의 좌표로 변화해 줘야 합니다.
        //그래야, 위치를 찾아갈 수 있겠습니다.
        Vector3 target = Camera.main.ScreenToWorldPoint(mPosition);

        //다음은 아크탄젠트(arctan, 역탄젠트)로 게임 오브젝트의 좌표와 마우스 포인트의 좌표를
        //이용하여 각도를 구한 후, 오일러(Euler)회전 함수를 사용하여 게임 오브젝트를 회전시키기
        //위해, 각 축의 거리차를 구한 후 오일러 회전함수에 적용시킵니다.

        //우선 각 축의 거리를 계산하여, dy, dx에 저장해 둡니다.
        float dy = target.y - oPosition.y;
        float dx = target.x - oPosition.x;

        //오릴러 회전 함수를 0에서 180 또는 0에서 -180의 각도를 입력 받는데 반하여
        //(물론 270과 같은 값의 입력도 전혀 문제없습니다.) 아크탄젠트 Atan2()함수의 결과 값은 
        //라디안 값(180도가 파이(3.141592654...)로)으로 출력되므로
        //라디안 값을 각도로 변화하기 위해 Rad2Deg를 곱해주어야 각도가 됩니다.
        float rotateDegree = Mathf.Atan2(dy, dx) * Mathf.Rad2Deg;

        //구해진 각도를 오일러 회전 함수에 적용하여 z축을 기준으로 게임 오브젝트를 회전시킵니다.
        transform.rotation = Quaternion.Euler(0f, 0f, rotateDegree);
        if (PlayerCombat.ps == PlayerState.Swing)
        {
            gun.GetComponent<SpriteRenderer>().flipY = false;
            player.GetComponent<SpriteRenderer>().flipX = false;
            return;
        }

        if (PlayerCombat.ps == PlayerState.Predators) return;
        if (PlayerCombat.ps == PlayerState.DeepImpact) return;

        if (gameObject.transform.rotation.eulerAngles.z >= 90f && gameObject.transform.rotation.eulerAngles.z <= 260f)
        {
            gun.GetComponent<SpriteRenderer>().flipY = true;
            player.GetComponent<SpriteRenderer>().flipX = true;

            if (PDG.Player.instance.gunControl.transform.Find("living_arrow_weapon"))
            {
                PDG.Player.instance.gunControl.transform.Find("living_arrow_weapon").GetComponent<SpriteRenderer>().flipY = true;
            }
            if (PDG.Player.instance.swordControl.transform.Find("living_arrow_weapon"))
            {
                PDG.Player.instance.swordControl.transform.Find("living_arrow_weapon").GetComponent<SpriteRenderer>().flipY = true;
            }
        }
        else
        {
            gun.GetComponent<SpriteRenderer>().flipY = false;
            player.GetComponent<SpriteRenderer>().flipX = false;

            if (PDG.Player.instance.gunControl.transform.Find("living_arrow_weapon"))
            {
                PDG.Player.instance.gunControl.transform.Find("living_arrow_weapon").GetComponent<SpriteRenderer>().flipY = false;
            }
            if (PDG.Player.instance.swordControl.transform.Find("living_arrow_weapon"))
            {
                PDG.Player.instance.swordControl.transform.Find("living_arrow_weapon").GetComponent<SpriteRenderer>().flipY = false;
            }
        }
        if (gun.GetComponent<GunChangeController>())
        {
            if (gun.GetComponent<GunChangeController>().FireEffectObj) 
            gun.GetComponent<GunChangeController>().FireEffectObj.transform.eulerAngles = new Vector3(-90.0f, 90.0f, 90.0f);
        }

        /*Direction Debug
        if (Input.GetMouseButtonDown(0))
        {
            Debug.Log(gameObject.transform.rotation.eulerAngles.z);
        }
        */
    }
    //마우스 위치에 따른 방향분위
    void MouseInDirection()
    {
        //오른쪽 중앙 가로선이 회전 절댓값이 0임
        //Up
        if(rot >= 76f && rot <= 115f)
        {
            PlayerCombat.pl = PlayerLookDirection.Up;
            if (!isSword) gun.GetComponent<SpriteRenderer>().sortingOrder = 2;
        }

        //UpRight
        if(rot >= 0 && rot <= 75f)
        {
            PlayerCombat.pl = PlayerLookDirection.UpRight;
            if (!isSword) gun.GetComponent<SpriteRenderer>().sortingOrder = 2;
        }

        //UpLeft
        if(rot >= 116f && rot <= 179f)
        {
            PlayerCombat.pl = PlayerLookDirection.UpLeft;
            if (!isSword) gun.GetComponent<SpriteRenderer>().sortingOrder = 2;
        }

        //DownLeft
        if(rot >= 180f && rot <= 255f)
        {
            PlayerCombat.pl = PlayerLookDirection.DownLeft;
            if (!isSword)
            {
                //if ((PDG.Player.instance.nowWeaponName.Equals("Ice_spear")/* || PDG.Player.instance.nowWeaponName.Contains("tracking_energy_gun")*/) && !PDG.Player.instance.isSubWeapon)
                //    gun.GetComponent<SpriteRenderer>().sortingOrder = 2;
                //else
                    gun.GetComponent<SpriteRenderer>().sortingOrder = 4;
            }
        }

        //Down
        if(rot >= 256f && rot <= 285f)
        {
            PlayerCombat.pl = PlayerLookDirection.Down;
            if (!isSword)
            {
                //if ((PDG.Player.instance.nowWeaponName.Equals("Ice_spear") /*|| PDG.Player.instance.nowWeaponName.Contains("tracking_energy_gun")*/) && !PDG.Player.instance.isSubWeapon)
                //    gun.GetComponent<SpriteRenderer>().sortingOrder = 2;
                //else
                    gun.GetComponent<SpriteRenderer>().sortingOrder = 4;
            }
        }

        //DownRight
        if(rot >= 286f && rot <= 360f)
        {
            PlayerCombat.pl = PlayerLookDirection.DownRight;
            if (!isSword)
            {
                //if ((PDG.Player.instance.nowWeaponName.Equals("Ice_spear") /*|| PDG.Player.instance.nowWeaponName.Contains("tracking_energy_gun")*/) && !PDG.Player.instance.isSubWeapon)
                //    gun.GetComponent<SpriteRenderer>().sortingOrder = 2;
                //else
                    gun.GetComponent<SpriteRenderer>().sortingOrder = 4;
            }
        }
    }
}
