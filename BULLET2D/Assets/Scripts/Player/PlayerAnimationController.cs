﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnimationController : MonoBehaviour
{
    public Animator ani;
    public Animator ani2;
    public RuntimeAnimatorController combatAniCon;
    public Vector3 AxisPosition;

    private void Awake()
    {
        ani.runtimeAnimatorController = combatAniCon;
    }

    private void Update()
    {
        if (PDG.Player.instance.isRollingTransform)
        {
            float x = Input.GetAxis("Horizontal");
            float y = Input.GetAxis("Vertical");
            AxisPosition = new Vector3(x, y, 0);

            if (AxisPosition.x > 0 && AxisPosition.y > 0)
            {
                PlayerCombat.pl = PlayerLookDirection.UpRight;
                ani2.GetComponent<SpriteRenderer>().flipX = true;
                ani2.GetComponent<SpriteRenderer>().flipY = false;
            }
            if (AxisPosition.x == 0 && AxisPosition.y > 0)
            {
                PlayerCombat.pl = PlayerLookDirection.Up;
                ani2.GetComponent<SpriteRenderer>().flipX = false;
                ani2.GetComponent<SpriteRenderer>().flipY = true;
            }
            if (AxisPosition.x < 0 && AxisPosition.y > 0)
            {
                PlayerCombat.pl = PlayerLookDirection.UpLeft;
                ani2.GetComponent<SpriteRenderer>().flipX = false;
                ani2.GetComponent<SpriteRenderer>().flipY = false;
            }
            if (AxisPosition.x < 0 && AxisPosition.y == 0)
            {
                PlayerCombat.pl = PlayerLookDirection.Left;
                ani2.GetComponent<SpriteRenderer>().flipX = false;
                ani2.GetComponent<SpriteRenderer>().flipY = false;
            }
            if (AxisPosition.x > 0 && AxisPosition.y == 0)
            {
                PlayerCombat.pl = PlayerLookDirection.Right;
                ani2.GetComponent<SpriteRenderer>().flipX = true;
                ani2.GetComponent<SpriteRenderer>().flipY = false;
            }
            if (AxisPosition.x > 0 && AxisPosition.y < 0)
            {
                PlayerCombat.pl = PlayerLookDirection.DownRight;
                ani2.GetComponent<SpriteRenderer>().flipX = true;
                ani2.GetComponent<SpriteRenderer>().flipY = false;
            }
            if (AxisPosition.x == 0 && AxisPosition.y < 0)
            {
                PlayerCombat.pl = PlayerLookDirection.Down;
                ani2.GetComponent<SpriteRenderer>().flipX = false;
                ani2.GetComponent<SpriteRenderer>().flipY = false;
            }
            if (AxisPosition.x < 0 && AxisPosition.y < 0)
            {
                PlayerCombat.pl = PlayerLookDirection.DownLeft;
                ani2.GetComponent<SpriteRenderer>().flipX = false;
                ani2.GetComponent<SpriteRenderer>().flipY = false;
            }

        }

        AniBoolTest();
        AniTest();
    }

    void AniTest()
    {
        /*
            Directions
            Up : 0
            Down : 1
            UpLeft : 2
            UpRight : 3
            DownLeft : 4
            DownRight : 5
         */

        switch (PlayerCombat.pl)
        {
            case PlayerLookDirection.Up:
                ani.SetInteger("Direction", 0);
                if (ani2.gameObject.activeSelf) ani2.SetInteger("Direction", 0);
                break;
            case PlayerLookDirection.Down :
                ani.SetInteger("Direction", 1);
                if (ani2.gameObject.activeSelf) ani2.SetInteger("Direction", 1);
                break;
            case PlayerLookDirection.UpLeft :
                ani.SetInteger("Direction", 2);
                if (ani2.gameObject.activeSelf) ani2.SetInteger("Direction", 2);
                break;
            case PlayerLookDirection.UpRight :
                ani.SetInteger("Direction", 3);
                if (ani2.gameObject.activeSelf) ani2.SetInteger("Direction", 3);
                break;
            case PlayerLookDirection.DownLeft :
                ani.SetInteger("Direction", 4);
                if (ani2.gameObject.activeSelf) ani2.SetInteger("Direction", 4);
                break;
            case PlayerLookDirection.DownRight:
                ani.SetInteger("Direction", 5);
                if (ani2.gameObject.activeSelf) ani2.SetInteger("Direction", 5);
                break;
            case PlayerLookDirection.Left:
                if (ani2.gameObject.activeSelf) ani2.SetInteger("Direction", 6);
                break;
            case PlayerLookDirection.Right:
                if (ani2.gameObject.activeSelf) ani2.SetInteger("Direction", 7);
                break;

        }
    }

    void AniBoolTest()
    {
        switch (PlayerCombat.ps)
        {
            case PlayerState.Idle:
                ani.SetBool("Idle", true);
                ani.SetBool("Walk", false);
                ani.SetBool("Avoid", false);
                ani.SetBool("Fall", false);
                ani.SetBool("Health", false);
                ani.SetBool("Swing", false);
                ani.SetBool("Predator", false);
                ani.SetBool("DeepImpact", false);
                if (ani2.gameObject.activeSelf) ani2.SetBool("Idle", true);
                if (ani2.gameObject.activeSelf) ani2.SetBool("Walk", false);
                if(ani2.gameObject.activeSelf)
                {
                    ani2.GetComponent<SpriteRenderer>().flipX = false;
                    ani2.GetComponent<SpriteRenderer>().flipY = false;
                    SoundManager.instance.StopAudio(new string[1] { "Character/rollingTransformMoving3" });
                }
                break;
            case PlayerState.Walk:
                ani.SetBool("Idle", false);
                ani.SetBool("Walk", true);
                ani.SetBool("Avoid", false);
                ani.SetBool("Fall", false);
                ani.SetBool("Health", false);
                ani.SetBool("Swing", false);
                ani.SetBool("Predator", false);
                ani.SetBool("DeepImpact", false);
                if (ani2.gameObject.activeSelf) ani2.SetBool("Idle", false);
                if (ani2.gameObject.activeSelf) ani2.SetBool("Walk", true);
                if (ani2.gameObject.activeSelf)
                {
                    SoundManager.instance.StartAudio(new string[1] { "Character/rollingTransformMoving3" }, VOLUME_TYPE.EFFECT, null, true);
                }
                break;
            case PlayerState.Avoid:
                ani.SetBool("Idle", false);
                ani.SetBool("Walk", false);
                ani.SetBool("Avoid", true);
                ani.SetBool("Fall", false);
                ani.SetBool("Health", false);
                ani.SetBool("Swing", false);
                ani.SetBool("Predator", false);
                ani.SetBool("DeepImpact", false);
                break;
            case PlayerState.Die:
                ani.ResetTrigger("Idle");
                break;
            case PlayerState.Fall:
                ani.SetBool("Idle", false);
                ani.SetBool("Walk", false);
                ani.SetBool("Avoid", false);
                ani.SetBool("Fall", true);
                ani.SetBool("Health", false);
                ani.SetBool("Swing", false);
                ani.SetBool("Predator", false);
                ani.SetBool("DeepImpact", false);
                break;
            case PlayerState.Health:
                ani.SetBool("Idle", false);
                ani.SetBool("Walk", false);
                ani.SetBool("Avoid", false);
                ani.SetBool("Fall", false);
                ani.SetBool("Health", true);
                ani.SetBool("Swing", false);
                ani.SetBool("Predator", false);
                ani.SetBool("DeepImpact", false);
                break;
            case PlayerState.Predators:
                ani.SetBool("Idle", false);
                ani.SetBool("Walk", false);
                ani.SetBool("Avoid", false);
                ani.SetBool("Fall", false);
                ani.SetBool("Health", false);
                ani.SetBool("Swing", false);
                ani.SetBool("Predator", true);
                ani.SetBool("DeepImpact", false);
                break;
            case PlayerState.DeepImpact:
                ani.SetBool("Idle", false);
                ani.SetBool("Walk", false);
                ani.SetBool("Avoid", false);
                ani.SetBool("Fall", false);
                ani.SetBool("Health", false);
                ani.SetBool("Swing", false);
                ani.SetBool("Predator", false);
                ani.SetBool("DeepImpact", true);
                break;
            case PlayerState.Swing:
                ani.SetBool("Idle", false);
                ani.SetBool("Walk", false);
                ani.SetBool("Avoid", false);
                ani.SetBool("Fall", false);
                ani.SetBool("Health", false);
                ani.SetBool("Swing", true);
                ani.SetBool("Predator", false);
                ani.SetBool("DeepImpact", false);
                break;

            default:
                break;
        }
    }
}
