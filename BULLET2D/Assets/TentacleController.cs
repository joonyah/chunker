﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TentacleController : MonoBehaviour
{
    [SerializeField] private AudioSource audio;
    [SerializeField] private AudioSource audio2;
    [SerializeField] private AudioSource audio3;
    [SerializeField] private Animator animator;
    [SerializeField] private SpriteRenderer sr;
    [SerializeField] private GameObject _obj;
    RaycastHit2D hit_right;
    RaycastHit2D hit_left;
    public bool isHit;
    [SerializeField] private float curHp = 5;
    [SerializeField] float hitTime = 0.25f;
    [SerializeField] private Rigidbody2D[] fragments;

    [SerializeField] private Color HitColor;
    [SerializeField] private Color HitColor2;
    [SerializeField] private Color HitColor3;
    [SerializeField] private Color HitColor4;
    [SerializeField] private float HitEffectDelay = 0.062f;
    [SerializeField] private GameObject HitEffec;
    Coroutine HitCoroutine;
    [SerializeField] private bool isDie = false;

    [SerializeField] private float disatance = 20;
    [SerializeField] private float defaultDistance = 0;

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireCube(transform.position + new Vector3(1.375f, -0.5f), new Vector3(2.25f, 1));
        Gizmos.color = Color.blue;
        Gizmos.DrawWireCube(transform.position + new Vector3(-1.375f, -0.5f), new Vector3(2.25f, 1));
    }

    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
        sr = GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        if (isDie) return;

        hit_right = Physics2D.BoxCast(transform.position + new Vector3(1.25f, -0.5f), new Vector2(2.5f, 1), 0, new Vector2(0, 0), 0, 1 << 9);
        hit_left = Physics2D.BoxCast(transform.position + new Vector3(-1.25f, -0.5f), new Vector2(2.5f, 1), 0, new Vector2(0, 0), 0, 1 << 9);
        if (hit_right)
        {
            sr.flipX = false;
            if (!animator.GetBool("isAttack"))
            {
                animator.SetTrigger("attack");
                animator.SetBool("isAttack", true);
            }
        }
        else if (hit_left)
        {
            sr.flipX = true;
            if (!animator.GetBool("isAttack"))
            {
                animator.SetTrigger("attack");
                animator.SetBool("isAttack", true);
            }
        }

        if(isHit)
        {
            hitTime -= Time.deltaTime;
            if (hitTime <= 0.0f)
            {
                isHit = false;
            }
        }
    }

    public void HitDamage()
    {
        float dis = Vector3.Distance(PDG.Player.instance.transform.position, transform.position);
        dis -= defaultDistance;

        if (dis > disatance) audio.volume = 0;
        if ((SoundManager.isMute || (PDG.Dungeon.instance == null ? false : PDG.Dungeon.instance.isPause) || (PDG.Player.instance == null ? false : PDG.Player.instance.isDie)))
        {
            audio.volume = 0;
        }
        else
        {
            float tVolume = SoundManager.EffectVolume;
            float t = tVolume / disatance;
            audio.volume = tVolume - (dis * t);
        }
        audio.Play();



        if (hit_right || hit_left)
        {
            if (hit_left && hit_left.collider.GetComponent<PDG.Player>())
            {
                hit_left.collider.GetComponent<PDG.Player>().SetDamage(1, LocalizeManager.GetLocalize("killedby_tantacle"), DEATH_TYPE.DEATH_FALL, string.Empty);
                GameObject obj2 = ObjManager.Call().GetObject("Hit_sprite7");
                obj2.transform.position = PDG.Player.instance.transform.position;
                obj2.transform.localScale = Vector3.one;
                obj2.GetComponent<SwordHitEffectController>()._effctName = "playerblood";
                obj2.GetComponent<SwordHitEffectController>().animSpeed = 1.0f;
                obj2.GetComponent<Animator>().Rebind();
                obj2.SetActive(true);
            }

            if (hit_right && hit_right.collider.GetComponent<PDG.Player>())
            {
                hit_right.collider.GetComponent<PDG.Player>().SetDamage(1, LocalizeManager.GetLocalize("killedby_tantacle"), DEATH_TYPE.DEATH_FALL, string.Empty);
                GameObject obj2 = ObjManager.Call().GetObject("Hit_sprite7");
                obj2.transform.position = PDG.Player.instance.transform.position;
                obj2.transform.localScale = Vector3.one;
                obj2.GetComponent<SwordHitEffectController>()._effctName = "playerblood";
                obj2.GetComponent<SwordHitEffectController>().animSpeed = 1.0f;
                obj2.GetComponent<Animator>().Rebind();
                obj2.SetActive(true);
            }
        }
    }
    public void AttackEnd()
    {
        animator.SetBool("isAttack", false);
    }
    public void SetDamage(float v)
    {
        if (isHit) return;
        isHit = true;
        hitTime = 0.25f;
        curHp -= v;

        if(HitEffec)
        {
            GameObject gg = Instantiate(HitEffec);
            gg.transform.position = transform.position;
            Destroy(gg, 2f);
        }

        float dis = Vector3.Distance(PDG.Player.instance.transform.position, transform.position);
        if (dis > 10) audio3.volume = 0;
        if ( (SoundManager.isMute || (PDG.Dungeon.instance == null ? false : PDG.Dungeon.instance.isPause) || (PDG.Player.instance == null ? false : PDG.Player.instance.isDie)))
        {
            audio3.volume = 0;
        }
        else
        {
            float tVolume = SoundManager.AmbientVolume;
            float t = tVolume / 10;
            audio3.volume = tVolume - (dis * t);
        }
        audio3.Play();
        if (curHp <= 0)
        {
            isDie = true;
            GetComponent<BoxCollider2D>().enabled = false;
            Destroy(_obj);
            gameObject.layer = 0;
            sr.enabled = false;
            for (int i = 0; i < fragments.Length; i++) 
            {
                fragments[i].gameObject.SetActive(true);
                fragments[i].AddForce(new Vector2(Random.Range(-1, 1), Random.Range(-1, 1)) * 2, ForceMode2D.Impulse);
                fragments[i].GetComponent<FragmentsController>().isStart = true;
            }

            audio2.volume =  (SoundManager.isMute || (PDG.Dungeon.instance == null ? false : PDG.Dungeon.instance.isPause) || (PDG.Player.instance == null ? false : PDG.Player.instance.isDie)) ? 0 : SoundManager.EffectVolume;
            audio2.Play();


        }
        else
        {
            if (HitCoroutine == null)
                HitCoroutine = StartCoroutine(HitEffect());
        }
    }

    IEnumerator HitEffect()
    {
        while (isHit && !isDie)
        {
            sr.color = HitColor;
            yield return new WaitForSeconds(HitEffectDelay);
            sr.color = HitColor2;
            yield return new WaitForSeconds(HitEffectDelay);
            sr.color = HitColor3;
            yield return new WaitForSeconds(HitEffectDelay);
            sr.color = HitColor4;
            yield return new WaitForSeconds(HitEffectDelay);
            sr.color = Color.white;
        }
        HitCoroutine = null;
    }
}
