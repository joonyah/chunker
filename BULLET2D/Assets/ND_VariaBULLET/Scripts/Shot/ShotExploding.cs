﻿#region Script Synopsis
    //A shot that has it's own controller/emitters. It triggers the child controller when the timer is reached and destroys the parent shot.
    //Learn more about this custom shot type at: https://neondagger.com/variabullet2d-scripting-guide/#exploding-bullets
#endregion

using UnityEngine;

namespace ND_VariaBULLET
{
    public class ShotExploding : ShotNonPhysics, IRePoolable
    {
        [Header("Explode Settings")]
        [Range(1, 20)]
        public int ExplodeTimer = 1;

        [Range(1, 10)]
        public int SlowdownRate = 1;

        private BasePattern childController;
        public bool isMonster = false;
        public string monsterName = string.Empty;
        public string bulletType;

        public override void Start()
        {
            base.Start();

            childController = transform.GetChild(0).GetChild(0).GetComponent<BasePattern>();
            rend = GetComponent<SpriteRenderer>();
        }

        public override void Update()
        {
            base.Update();

            OnEventTimerDo(
                shot => {
                    shot.ShotSpeed -= SlowdownRate * Time.deltaTime * shot.ShotSpeed / 2 * scale;

                    if (shot.ShotSpeed < 5 & shot.ShotSpeed > 2)
                    {
                        childController.TriggerAutoFire = true;
                        rend.enabled = false;
                    }
                    else if (shot.ShotSpeed < 2)
                        RePoolOrDestroy();

                }, ExplodeTimer * 5
            );
        }

        public override void RePool(IPooler poolingScript)
        {
            childController.TriggerAutoFire = false;
            rend.enabled = true;
            isMove = true;
            base.RePool(poolingScript);
        }

        protected override void OnTriggerEnter2D(Collider2D collision)
        {
            GameObject obj = null;
            if (collision.gameObject.layer.Equals(12) && !isMonster)
            {
                obj = ObjManager.Call().GetObject("Bullet_Hit");
                obj.GetComponent<SwordHitEffectController>()._isMonster = isMonster;
                collision.gameObject.GetComponent<MOB.Monster>().SetDamage(PDG.Player.instance.isLeftNear ? PDG.Player.instance.damageNear : PDG.Player.instance.damage, -Direction, 1, PDG.Player.instance.left_elemetanl, PDG.Player.instance.left_elemnetalTime);

                if (PDG.Player.instance.isLeftNear && PDG.Player.instance.isCritical)
                {
                    GameObject criEffect = ObjManager.Call().GetObject("CriticalHitEffect");
                    criEffect.transform.position = collision.transform.position;
                    criEffect.SetActive(true);
                }
                rend.enabled = false;
            }
            if (collision.gameObject.layer.Equals(9) && isMonster)
            {
                obj = ObjManager.Call().GetObject("Bullet_Hit");
                obj.GetComponent<SwordHitEffectController>()._isMonster = isMonster;
                collision.GetComponent<PDG.Player>().SetDamage(1, LocalizeManager.GetLocalize("monster_octopus_king"), DEATH_TYPE.DEATH_BOSS, string.Empty);
                rend.enabled = false;
            }

            if (collision.gameObject.layer.Equals(8)) obj = ObjManager.Call().GetObject("Bullet_WallCollision");
            if (obj != null)
            {
                obj.transform.position = transform.position;
                obj.GetComponent<Animator>().Rebind();
                obj.transform.localEulerAngles = new Vector3(0, 0, GetDirectionAngle(collision));
                obj.SetActive(true);
                isMove = false;
                childController.TriggerAutoFire = true;
                rend.enabled = false;
            }
        }
        private void OnTriggerStay2D(Collider2D collision)
        {
            if (collision.gameObject.layer.Equals(0) || collision.gameObject.layer.Equals(27))
            {
                GameObject obj = ObjManager.Call().GetObject("Bullet_Hit");
                obj.transform.position = transform.position;
                obj.GetComponent<Animator>().Rebind();
                obj.transform.localEulerAngles = new Vector3(0, 0, GetDirectionAngle(collision));
                obj.GetComponent<SwordHitEffectController>()._isMonster = isMonster;
                obj.SetActive(true);
                CameraShaker._instance.StartShake(0.2f, 0.02f, 0.2f);
                gameObject.SetActive(false);
            }
        }
        int GetDirectionAngle(Collider2D collision)
        {
            float angle = GetAngle(collision.transform.position, transform.position);
            while (angle > 360.0f) angle -= 360.0f;
            while (angle < 0) angle += 360.0f;

            if (angle >= 45.0f && angle < 135.0f)
            {
                // 위쪽
                return 270;
            }
            else if (angle >= 135.0f && angle < 225.0f)
            {
                // 왼쪽
                return 0;
            }
            else if (angle > 225.0f && angle < 315.0f)
            {
                //아래쪽
                return 90;
            }
            else
            {
                //오른쪽
                return 180;
            }
        }
        #region -- Tool --
        public static float GetAngle(Vector3 vStart, Vector3 vEnd)
        {
            Vector3 v = vEnd - vStart;

            return Mathf.Atan2(v.y, v.x) * Mathf.Rad2Deg;
        }
        public static Vector3 GetPosition(Vector3 vStart, float angle)
        {
            float x = Mathf.Cos(Mathf.Deg2Rad * angle) + vStart.x;
            float y = Mathf.Sin(Mathf.Deg2Rad * angle) + vStart.y;

            return new Vector3(x, y, 0);
        }
        #endregion
    }

}