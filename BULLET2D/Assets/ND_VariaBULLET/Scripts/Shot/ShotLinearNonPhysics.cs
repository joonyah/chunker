﻿#region Script Synopsis
    //The base class for all non-physics type shots that are re-poolable.
#endregion

using UnityEngine;

namespace ND_VariaBULLET
{
    public class ShotLinearNonPhysics : ShotNonPhysics, IRePoolable
    {
        //No implementation
        //A poolable version of ShotNonPhysics
        protected override void OnTriggerEnter2D(Collider2D collision)
        {
            base.OnTriggerEnter2D(collision);

            GameObject obj = null;
            if (collision.gameObject.layer.Equals(9))
            {
                obj = ObjManager.Call().GetObject("Bullet_Hit");
                obj.GetComponent<SwordHitEffectController>()._isMonster = true;
                collision.GetComponent<PDG.Player>().SetDamage(1, LocalizeManager.GetLocalize("monster_octopus_king"), DEATH_TYPE.DEATH_BOSS, string.Empty);
                rend.enabled = false;
            }

            if (collision.gameObject.layer.Equals(8)) obj = ObjManager.Call().GetObject("Bullet_WallCollision");
            if (obj != null)
            {
                obj.transform.position = transform.position;
                obj.GetComponent<Animator>().Rebind();
                obj.transform.localEulerAngles = new Vector3(0, 0, GetDirectionAngle(collision));
                obj.SetActive(true);
            }
        }
        private void OnTriggerStay2D(Collider2D collision)
        {
            if (collision.gameObject.layer.Equals(0) || collision.gameObject.layer.Equals(27))
            {
                GameObject obj = ObjManager.Call().GetObject("Bullet_Hit");
                obj.transform.position = transform.position;
                obj.GetComponent<SwordHitEffectController>()._isMonster = true;
                obj.GetComponent<Animator>().Rebind();
                obj.transform.localEulerAngles = new Vector3(0, 0, GetDirectionAngle(collision));
                obj.SetActive(true);
                CameraShaker._instance.StartShake(0.2f, 0.02f, 0.2f);
                gameObject.SetActive(false);
            }
        }
        int GetDirectionAngle(Collider2D collision)
        {
            float angle = GetAngle(collision.transform.position, transform.position);
            while (angle > 360.0f) angle -= 360.0f;
            while (angle < 0) angle += 360.0f;

            if (angle >= 45.0f && angle < 135.0f)
            {
                // 위쪽
                return 270;
            }
            else if (angle >= 135.0f && angle < 225.0f)
            {
                // 왼쪽
                return 0;
            }
            else if (angle > 225.0f && angle < 315.0f)
            {
                //아래쪽
                return 90;
            }
            else
            {
                //오른쪽
                return 180;
            }
        }
        #region -- Tool --
        public static float GetAngle(Vector3 vStart, Vector3 vEnd)
        {
            Vector3 v = vEnd - vStart;

            return Mathf.Atan2(v.y, v.x) * Mathf.Rad2Deg;
        }
        public static Vector3 GetPosition(Vector3 vStart, float angle)
        {
            float x = Mathf.Cos(Mathf.Deg2Rad * angle) + vStart.x;
            float y = Mathf.Sin(Mathf.Deg2Rad * angle) + vStart.y;

            return new Vector3(x, y, 0);
        }
        #endregion
    }
}