﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoblinSpawn : MonoBehaviour
{
    bool isStart = false;
    private void Update()
    {
        RaycastHit2D hit = Physics2D.CircleCast(transform.position, 4, Vector2.zero, 0, 1 << 9);
        if (hit && !isStart)
        {
            isStart = true;
            PDG.Dungeon.instance.SpawnGoblin(transform.position);
            Destroy(gameObject, 1f);
        }
    }
}
