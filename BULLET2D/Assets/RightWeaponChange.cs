﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RightWeaponChange : MonoBehaviour
{
    [SerializeField] private Animator animator;
    [SerializeField] private RuntimeAnimatorController[] animators;
    [SerializeField] private bool isSword;

    public void SettingAnimator(string _name)
    {
        RuntimeAnimatorController anim = System.Array.Find(animators, item => item.name.Equals(_name));
        if (anim) 
        {
            animator.runtimeAnimatorController = anim;
        }

        if (_name.Equals("living_arrow")) transform.localEulerAngles = new Vector3(0, 0, isSword ? 45 : -45);
        else transform.localEulerAngles = Vector3.zero;
    }
}
