﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossWaitingRoom3 : MonoBehaviour
{
    [SerializeField] private bool isStart = false;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.layer.Equals(9) && !isStart)
        {
            Dialogue.Instance.CommnetSetting("waitingroom3", 0, null);
            isStart = true;
        }
    }
}
