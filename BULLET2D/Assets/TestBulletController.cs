﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestBulletController : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(SpecialFire());
    }

    public Vector3 dir = Vector3.right;
    public float fRange = 10.0f;
    IEnumerator SpecialFire()
    {
        int signCount = 7;
        int t = 0;
        float tt = 0.0f;
        Vector3 endPosition = transform.position + (dir * fRange);
        Vector3 minusPosition = endPosition - transform.position;
        Vector3[] endPositions = new Vector3[signCount];
        for (int i = 0; i < endPositions.Length; i++)
            endPositions[i] = transform.position + ((minusPosition / signCount) * (i + 1));
        Vector3 startPosition = transform.position;

        while (true)
        {
            Vector3 centerPosition = startPosition + ((endPositions[t] - startPosition) / 2);
            Vector3 centerPositionTemp = GetPosition(centerPosition, (t % 2 == 0 ? -90 : 90));
            Vector3 centerPositionTemp2 = centerPositionTemp - centerPosition;
            centerPositionTemp2.Normalize();
            centerPosition += centerPositionTemp2 * 2.0f;

            Vector3 lerp_0 = Vector3.Lerp(startPosition, centerPosition, tt);
            Vector3 lerp_1 = Vector3.Lerp(centerPosition, endPositions[t], tt);
            transform.position = Vector3.Lerp(lerp_0, lerp_1, tt);
            tt += Time.fixedDeltaTime;
            if (tt >= 1.0f)
            {
                startPosition = endPositions[t];
                tt = 0.0f;
                t++;
                if (tt > signCount) break;
            }
            yield return new WaitForFixedUpdate();
        }
    }
    #region -- Tool --
    int GetDirectionAngle(Collider2D collision)
    {
        float angle = GetAngle(collision.transform.position, transform.position);
        while (angle > 360.0f) angle -= 360.0f;
        while (angle < 0) angle += 360.0f;

        if (angle >= 45.0f && angle < 135.0f)
        {
            // 위쪽
            return 270;
        }
        else if (angle >= 135.0f && angle < 225.0f)
        {
            // 왼쪽
            return 0;
        }
        else if (angle > 225.0f && angle < 315.0f)
        {
            //아래쪽
            return 90;
        }
        else
        {
            //오른쪽
            return 180;
        }
    }
    float GetAngle(Vector3 vStart, Vector3 vEnd)
    {
        Vector3 v = vEnd - vStart;

        return Mathf.Atan2(v.y, v.x) * Mathf.Rad2Deg;
    }
    Vector3 GetPosition(Vector3 vStart, float angle)
    {
        float x = Mathf.Cos(Mathf.Deg2Rad * angle) + vStart.x;
        float y = Mathf.Sin(Mathf.Deg2Rad * angle) + vStart.y;

        return new Vector3(x, y, 0);
    }
    float GetReflectAngle(Vector3 _hitPoint, Vector3 _hitPosition, Vector3 _dir)
    {
        Vector3 _dir2 = _hitPoint - _hitPosition;
        _dir2.Normalize();

        if (_dir.x == 0 && _dir.y > 0)       // 위 
        {
            return 270.0f;
        }
        else if (_dir.x == 0 && _dir.y < 0)  // 아래
        {
            return 90.0f;
        }
        else if (_dir.x > 0 && _dir.y == 0)  // 오른쪽
        {
            return 180.0f;
        }
        else if (_dir.x < 0 && _dir.y == 0)  // 왼쪽
        {
            return 0.0f;
        }
        else if (_dir.x > 0 && _dir.y > 0)  // 오른쪽 위 
        {
            if (_dir2.x > 0)
            {
                float zz = GetAngle(_hitPoint, transform.position);
                float f = zz - 180.0f; // 입사각
                return 360.0f - f; // 반사각
            }
            else if (_dir2.y > 0)
            {
                float zz = GetAngle(_hitPoint, transform.position);
                float f = 270.0f - zz; // 입사각
                return 90.0f + f; // 반사각
            }
            else
            {
                float hitAngle = GetAngle(_hitPosition, _hitPoint);
                if (hitAngle > 225.0f)
                {
                    float zz = GetAngle(_hitPoint, transform.position);
                    float f = zz - 180.0f; // 입사각
                    return 360.0f - f; // 반사각
                }
                else if (hitAngle < 225.0f)
                {
                    float zz = GetAngle(_hitPoint, transform.position);
                    float f = 270.0f - zz; // 입사각
                    return 90.0f + f; // 반사각
                }
                else
                {
                    return 225.0f;
                }

            }
        }
        else if (_dir.x < 0 && _dir.y > 0)   // 왼쪽 위
        {
            if (_dir2.x < 0)
            {
                float zz = GetAngle(_hitPoint, transform.position);
                float f = 360.0f - zz; // 입사각
                return 180.0f + f; // 반사각
            }
            else if (_dir2.y > 0)
            {
                float zz = GetAngle(_hitPoint, transform.position);
                float f = zz - 270.0f; // 입사각
                return f; // 반사각
            }
            else
            {
                float hitAngle = GetAngle(_hitPosition, _hitPoint);
                if (hitAngle > 315.0f)
                {
                    float zz = GetAngle(_hitPoint, transform.position);
                    float f = zz - 270.0f; // 입사각
                    return f; // 반사각
                }
                else if (hitAngle < 315.0f)
                {
                    float zz = GetAngle(_hitPoint, transform.position);
                    float f = 360.0f - zz; // 입사각
                    return 180.0f + f; // 반사각
                }
                else
                {
                    return 315.0f;
                }

            }
        }
        else if (_dir.x < 0 && _dir.y < 0)   // 왼쪽 아래
        {
            if (_dir2.x < 0)
            {
                float zz = GetAngle(_hitPoint, transform.position);
                float f = zz; // 입사각
                return 180.0f - f; // 반사각
            }
            else if (_dir2.y < 0)
            {
                float zz = GetAngle(_hitPoint, transform.position);
                float f = 90.0f - zz; // 입사각
                return 270.0f + f; // 반사각
            }
            else
            {
                float hitAngle = GetAngle(_hitPosition, _hitPoint);
                if (hitAngle > 45.0f)
                {
                    float zz = GetAngle(_hitPoint, transform.position);
                    float f = zz; // 입사각
                    return 180.0f - f; // 반사각
                }
                else if (hitAngle < 45.0f)
                {
                    float zz = GetAngle(_hitPoint, transform.position);
                    float f = 90.0f - zz; // 입사각
                    return 270.0f + f; // 반사각
                }
                else
                {
                    return 45.0f;
                }

            }
        }
        else if (_dir.x > 0 && _dir.y < 0)   // 오른쪽 아래
        {
            if (_dir2.x > 0)
            {
                float zz = GetAngle(_hitPoint, transform.position);
                float f = 180.0f - zz; // 입사각
                return f; // 반사각
            }
            else if (_dir2.y < 0)
            {
                float zz = GetAngle(_hitPoint, transform.position);
                float f = zz - 90.0f; // 입사각
                return 270.0f - f; // 반사각
            }
            else
            {
                float hitAngle = GetAngle(_hitPosition, _hitPoint);
                if (hitAngle > 135.0f)
                {
                    float zz = GetAngle(_hitPoint, transform.position);
                    float f = zz - 90.0f; // 입사각
                    return 270.0f - f; // 반사각
                }
                else if (hitAngle < 135.0f)
                {
                    float zz = GetAngle(_hitPoint, transform.position);
                    float f = 180.0f - zz; // 입사각
                    return f; // 반사각
                }
                else
                {
                    return 135.0f;
                }
            }
        }
        else
        {
            return 0.0f;
        }
    }
    #endregion
}
