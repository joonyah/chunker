﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DoctorController : MonoBehaviour
{
    public string mobType;
    string _spritePath;
    Sprite[] _sprites;
    private SpriteRenderer sr;
    public Animator animator;

    [SerializeField] private Sprite fSprite;
    [SerializeField] private GameObject controllerObj;
    [SerializeField] private SpriteRenderer controllerSR;
    [SerializeField] private bool isSelect = false;
    public Transform[] itemPositions;
    public int nowItem = 0;
    bool isChecking = false;
    [SerializeField] private string npcName;

    [Header("SlolText")]
    [SerializeField] private string[] SoloTexts;
    [SerializeField] private Text nowSoloText;
    [SerializeField] private Font _font;
    [SerializeField] private bool isTargeting = false;
    private void Awake()
    {
        animator = GetComponent<Animator>();
        sr = GetComponent<SpriteRenderer>();
    }
    // Start is called before the first frame update
    void Start()
    {
        Load();
        ItemSpawn();
        StartCoroutine(SoloText());
    }
    IEnumerator SoloText()
    {
        if (nowSoloText == null)
        {
            GameObject obj = new GameObject();
            Text nText = obj.AddComponent<Text>();
            nText.font = _font;
            nText.alignment = TextAnchor.MiddleCenter;
            nText.fontSize = 20;
            nText.horizontalOverflow = HorizontalWrapMode.Overflow;
            nowSoloText = nText;
            obj.transform.SetParent(GameObject.Find("Canvas").transform);
            obj.transform.localScale = Vector3.one;
            obj.transform.SetSiblingIndex(15);
        }

        while (true)
        {
            if (!isSelect)
            {
                nowSoloText.text = LocalizeManager.GetLocalize(SoloTexts[UnityEngine.Random.Range(0, SoloTexts.Length)]);
                yield return new WaitForSeconds(2.0f);
                nowSoloText.text = string.Empty;
            }
            else
            {
                nowSoloText.text = string.Empty;
            }
            yield return new WaitForSeconds(UnityEngine.Random.Range(2, 5));
        }
    }
    private void ItemSpawn()
    {
        string _date = DateTime.Now.ToString("yyyyMMdd");

        //MailSaveData mData = SaveAndLoadManager.instance.LoadMail("doctor_item");
        //if (mData._id.Equals(string.Empty))
        //{
        //    List<string> unlockData = new List<string>();
        //    UnlockListsData[] datas = SheetManager.Instance.UnlockListsDB.dataArray;
        //    for (int i = 0; i < datas.Length; i++)
        //    {
        //        if (datas[i].Itemid.Equals("predators")) continue;
        //        if (datas[i].NPCID.Equals("doctor"))
        //        {
        //            int t = SaveAndLoadManager.instance.LoadUnlock(datas[i].Itemid);
        //            if (t >= datas[i].Unlockcount)
        //            {
        //                unlockData.Add(datas[i].Itemid);
        //            }
        //        }
        //    }
        //    if (unlockData.Count == 0) return;
        //    SaveAndLoadManager.instance.SaveMail(new MailSaveData
        //    {
        //        _id = "doctor_item",
        //        date = _date,
        //        isCheck = false,
        //        isPush = true,
        //        items = new string[1] { unlockData[UnityEngine.Random.Range(0, unlockData.Count)] },
        //        isItems = new bool[1] { false }
        //    });
        //    isChecking = true;
        //}
        //else if (!mData.date.Equals(_date))
        //{
        //    List<string> unlockData = new List<string>();
        //    UnlockListsData[] datas = SheetManager.Instance.UnlockListsDB.dataArray;
        //    for (int i = 0; i < datas.Length; i++)
        //    {
        //        if (datas[i].NPCID.Equals("doctor"))
        //        {
        //            int t = SaveAndLoadManager.instance.LoadUnlock(datas[i].Itemid);
        //            if (t >= datas[i].Unlockcount)
        //            {
        //                unlockData.Add(datas[i].Itemid);
        //            }
        //        }
        //    }
        //    if (unlockData.Count == 0) return;
        //    SaveAndLoadManager.instance.SaveMail(new MailSaveData
        //    {
        //        _id = "doctor_item",
        //        date = _date,
        //        isCheck = false,
        //        isPush = true,
        //        items = new string[1] { unlockData[UnityEngine.Random.Range(0, unlockData.Count)] },
        //        isItems = new bool[1] { false }
        //    });
        //    isChecking = true;
        //}
        //else if (mData.isPush && !mData.isCheck)
        //{
        //    isChecking = true;
        //    for (int i = 0; i < itemPositions.Length; i++)
        //    {
        //        if (i >= mData.isItems.Length) break;
        //        if (mData.isItems[i])
        //        {
        //            itemPositions[i].gameObject.SetActive(false);
        //            continue;
        //        }
        //        int iDataIndex = Array.FindIndex(SheetManager.Instance.ItemsDB.dataArray, item => item.ID.Equals(mData.items[i]));
        //        if (iDataIndex != -1) 
        //        {
        //            ItemsData iData = SheetManager.Instance.ItemsDB.dataArray[iDataIndex];
        //            itemPositions[i].gameObject.SetActive(true);
        //            itemPositions[i].GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("Item/" + iData.Type1 + "/" + iData.Type2 + "/" + iData.ID);
        //            itemPositions[i].GetComponent<ItemSelectController>().iData = iData;
        //            itemPositions[i].GetComponent<ItemSelectController>().isItem = true;
        //            itemPositions[i].GetComponent<ItemSelectController>().num = i;
        //        }
        //    }
        //}
    }
    // Update is called once per frame
    void Update()
    {
        if (nowSoloText != null)
        {
            nowSoloText.gameObject.transform.position = Camera.main.WorldToScreenPoint(controllerObj.transform.position);
        }
        RaycastHit2D hit = Physics2D.BoxCast(transform.position, new Vector2(2.0f, 3.8f), 0, Vector2.zero, 0, 1 << 9);
        RaycastHit2D hit3 = Physics2D.CircleCast(transform.position, 15, Vector2.zero, 0, 1 << 9);
        if (hit3 && isTargeting)
        {
            Vector3 dir = hit3.collider.transform.position - transform.position;
            dir.Normalize();
            if (dir.x < 0) GetComponent<SpriteRenderer>().flipX = true;
            else GetComponent<SpriteRenderer>().flipX = false;
        }

        if (hit && !isSelect && !PDG.Dungeon.instance.newObjectSelected)
        {
            isSelect = true;
            if (nowSoloText != null) nowSoloText.text = string.Empty;
            PDG.Dungeon.instance.newObjectSelected = true;
            PDG.Dungeon.instance.newObjectSelectedObj = gameObject;
            controllerObj.SetActive(true);
            controllerObj.GetComponent<SpriteRenderer>().sprite = PDG.Dungeon.instance.isJoystrick ? PDG.Dungeon.GetJoyStrickKeySprite(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_ACTIVE]) : PDG.Dungeon.GetJoyStrickKeySprite(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ACTIVE]);
            animator.SetBool("isSelected", true);
            TextOpenController.instance.ShowText(LocalizeManager.GetLocalize(npcName), Color.white, 24, controllerObj, 0.5f, true);
        }
        else if (!hit)
        {
            isSelect = false;
            if (PDG.Dungeon.instance.newObjectSelectedObj == gameObject)
            {
                PDG.Dungeon.instance.newObjectSelectedObj = null;
                PDG.Dungeon.instance.newObjectSelected = false;
            }
            controllerObj.SetActive(false);
            animator.SetBool("isSelected", false);
            if (TextOpenController.instance.targetObj == controllerObj) TextOpenController.instance.TextClose(Color.white);
        }

        if (isSelect)
        {
            if ((PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ACTIVE], true) || PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_ACTIVE], true)) && PDG.Dungeon.instance.closeCoolTime > 0.2f)
            {
                PDG.Dungeon.instance.closeCoolTime = 0.0f;
                SoundManager.instance.StartAudio(new string[1] { "Object/active_F" }, VOLUME_TYPE.EFFECT);
                isSelect = false;
                if (PDG.Dungeon.instance.newObjectSelectedObj == gameObject)
                {
                    PDG.Dungeon.instance.newObjectSelectedObj = null;
                    PDG.Dungeon.instance.newObjectSelected = false;
                }
                controllerObj.SetActive(false);
                animator.SetBool("isSelected", false);
                MakeController.instance.ShowMake(string.Empty, "doctor", gameObject);
            }
        }
    }
    void LateUpdate()
    {
        if (sr == null || sr.sprite == null)
            return;
        Load();
        var name = sr.sprite.name;
        var sprite = System.Array.Find(_sprites, item => item.name == name);
        if (sprite) sr.sprite = sprite;
    }
    void Load()
    {
        var spritePath = "NPC/" + mobType;
        if (!spritePath.Equals(_spritePath))
        {
            _spritePath = spritePath;
            _sprites = Resources.LoadAll<Sprite>(_spritePath);
        }
    }
}
