﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BossRushWayPoint : MonoBehaviour
{
    public GameObject animObj;
    public GameObject entranceQuestionPanel;
    public Button YesBtn;
    public Button NoBtn;
    public Text entranceQuestionText;
    private float moveSpeed = 1500.0f;
    Coroutine endCoroutine;
    [SerializeField] private string stageName;
    [SerializeField] bool isOpen = false;
    [SerializeField] GameObject pObj;
    private void Start()
    {
        if (entranceQuestionPanel == null) entranceQuestionPanel = GameObject.Find("Canvas").transform.Find("QuestionPanel").gameObject;
        if (entranceQuestionText == null) entranceQuestionText = entranceQuestionPanel.transform.Find("Text").GetComponent<Text>();
        if (YesBtn == null) YesBtn = entranceQuestionPanel.transform.Find("BtnPanel").Find("YesBtn").GetComponent<Button>();
        if (NoBtn == null) NoBtn = entranceQuestionPanel.transform.Find("BtnPanel").Find("NoBtn").GetComponent<Button>();
    }
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(transform.position, 4);
    }
    private void Update()
    {
        RaycastHit2D hit = Physics2D.CapsuleCast(transform.position, new Vector2(4, 2.5f), CapsuleDirection2D.Horizontal, 0, Vector2.zero, 0, 1 << 9);
        if (hit && isOpen)
        {
            PDG.Dungeon.instance.newObjectSelectedObj = gameObject;
            Question();
        }
        else
        {
            if (endCoroutine == null && entranceQuestionPanel.activeSelf && PDG.Dungeon.instance.newObjectSelectedObj == gameObject)
            {
                PDG.Player.instance.isCameraOff = false;
                StopAllCoroutines();
                endCoroutine = StartCoroutine(EndQuestionEvent());
            }
        }

        if (entranceQuestionPanel.activeSelf && PDG.Dungeon.instance.newObjectSelectedObj == gameObject)
        {
            if ((Input.GetKeyDown(KeyCode.Escape) || Input.GetKeyDown(KeyCode.Backspace) || PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_CANCLE], true)) && PDG.Dungeon.instance.closeCoolTime > 0.2f)
            {
                PDG.Dungeon.instance.closeCoolTime = 0.0f;
                PDG.Player.instance.isCameraOff = false;
                StopAllCoroutines();
                StartCoroutine(EndQuestionEvent());
                SoundManager.instance.Click();
            }

            if ((Input.GetKeyDown(KeyCode.Return) || PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ACTIVE], true) || PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_CONFIRM], true)) && PDG.Dungeon.instance.closeCoolTime > 0.2f)
            {
                PDG.Dungeon.instance.closeCoolTime = 0.0f;
                SoundManager.instance.StartAudio(new string[1] { "Object/active_F" }, VOLUME_TYPE.EFFECT);
                PDG.Player.instance.isCameraOff = false;
                StartEntrance();
            }
        }
    }
    private void Question()
    {
        if (!entranceQuestionPanel.activeSelf)
        {
            entranceQuestionText.text = LocalizeManager.GetLocalize(stageName);
            entranceQuestionPanel.SetActive(true);
            StopAllCoroutines();
            endCoroutine = null;
            StartCoroutine(StartQuestionEvent());
            YesBtn.onClick.RemoveAllListeners();
            YesBtn.onClick.AddListener(() => {
                PDG.Player.instance.isCameraOff = false;
                StartEntrance();
            });
            NoBtn.onClick.RemoveAllListeners();
            NoBtn.onClick.AddListener(() => {
                PDG.Player.instance.isCameraOff = false;
                StopAllCoroutines();
                endCoroutine = StartCoroutine(EndQuestionEvent());
                SoundManager.instance.Click();
            });
        }
    }
    IEnumerator StartQuestionEvent()
    {
        RectTransform rt = entranceQuestionPanel.GetComponent<RectTransform>();
        float y = 500.0f;
        while (true)
        {
            rt.anchoredPosition3D = new Vector3(0, y, 0);
            y -= Time.deltaTime * moveSpeed;
            if (y <= 50.0f) break;
            yield return new WaitForSeconds(Time.deltaTime);
        }
        y = 50.0f;
        rt.anchoredPosition3D = new Vector3(0, y, 0);
        while (true)
        {
            rt.anchoredPosition3D = new Vector3(0, y, 0);
            y += Time.deltaTime * (moveSpeed / 4);
            if (y >= 70.0f) break;
            yield return new WaitForSeconds(Time.deltaTime);
        }
        y = 70.0f;
        rt.anchoredPosition3D = new Vector3(0, y, 0);
        while (true)
        {
            rt.anchoredPosition3D = new Vector3(0, y, 0);
            y -= Time.deltaTime * (moveSpeed / 4);
            if (y <= 50.0f) break;
            yield return new WaitForSeconds(Time.deltaTime);
        }
        y = 50.0f;
        rt.anchoredPosition3D = new Vector3(0, y, 0);
        while (true)
        {
            rt.anchoredPosition3D = new Vector3(0, y, 0);
            y += Time.deltaTime * (moveSpeed / 4);
            if (y >= 60.0f) break;
            yield return new WaitForSeconds(Time.deltaTime);
        }
        y = 60.0f;
        rt.anchoredPosition3D = new Vector3(0, y, 0);
        while (true)
        {
            rt.anchoredPosition3D = new Vector3(0, y, 0);
            y -= Time.deltaTime * (moveSpeed / 4);
            if (y <= 50.0f) break;
            yield return new WaitForSeconds(Time.deltaTime);
        }
        y = 50.0f;
        rt.anchoredPosition3D = new Vector3(0, y, 0);
        endCoroutine = null;
    }
    IEnumerator EndQuestionEvent()
    {
        RectTransform rt = entranceQuestionPanel.GetComponent<RectTransform>();
        float y = 50.0f;
        rt.anchoredPosition3D = new Vector3(0, y, 0);
        while (true)
        {
            rt.anchoredPosition3D = new Vector3(0, y, 0);
            y += Time.deltaTime * (moveSpeed / 2);
            if (y >= 500.0f) break;
            yield return new WaitForSeconds(Time.deltaTime);
        }
        y = 500.0f;
        rt.anchoredPosition3D = new Vector3(0, y, 0);
        endCoroutine = null;
        entranceQuestionPanel.SetActive(false);
    }
    void StartEntrance()
    {
        StartCoroutine(EntranceCoroutine());
    }
    IEnumerator EntranceCoroutine()
    {
        PDG.Dungeon.instance.stageBossRan = new int[4];
        for (int i = 0; i < PDG.Dungeon.instance.stageBossRan.Length; i++)
        {
            while (true)
            {
                PDG.Dungeon.instance.stageBossRan[i] = Random.Range(0, PDG.Dungeon.instance.stageBossRan.Length);
                bool c = false;
                for (int j = 0; j < i; j++)
                {
                    if (PDG.Dungeon.instance.stageBossRan[i] == PDG.Dungeon.instance.stageBossRan[j]) c = true;
                }
                if (!c) break;
            }
        }
        yield return null;
        for (int i = 0; i < PDG.Dungeon.instance.BossRushItemList.Count; i++)
        {
            PDG.Dungeon.instance.BossRushItemList[i].SetActive(false);
        }
        PDG.Dungeon.instance.BossRushItemList.Clear();
        TextOpenController.instance.TextClose(Color.white);
        PDG.Dungeon.instance.StageLevel = 0;
        string mapId = "d01_bossstage_0" + (PDG.Dungeon.instance.stageBossRan[PDG.Dungeon.instance.StageLevel] + 1);
        PDG.MapPiece piece = System.Array.Find(PDG.Dungeon.instance._MapPiece, item => item.id.Equals(mapId));
        Vector3 pos = pObj.transform.position;
        pos.x += 50.0f;
        pos.y += 50.0f;
        PDG.Dungeon.instance.isBossRushStart = true;
        GameObject oobj = Instantiate(piece.obj, null);
        oobj.transform.position = pos;
        pos.y += piece.doorPos[0].y;
        PDG.Player.instance.transform.position = pos;
        entranceQuestionPanel.SetActive(false);
        Destroy(pObj);
    }
}
