﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HpRecoveryController : MonoBehaviour
{
    [SerializeField] private GameObject controllerObj;
    [SerializeField] private SpriteRenderer controllerSR;
    [SerializeField] private bool isSelect = false;
    bool isOpen = false;
    public bool isUse = false;
    Animator animator;
    private float delay = 0.0f;

    [SerializeField] private int nowDungeon;
    [SerializeField] private bool isOne = false;

    [SerializeField] private Vector3 vOffset;
    [SerializeField] private Vector3 vSize;
    [SerializeField] private string firstCommnet;
    [SerializeField] private string secondCommnet;
    [SerializeField] private string thirdCommnet;
    [SerializeField] private string lackComment;
    [SerializeField] private int useCount;


    public bool isUsing = false;
    [SerializeField] private float fTime = 3.0f;
    [SerializeField] private AudioClip[] clips;
    [SerializeField] private AudioSource audio;
    [SerializeField] private Sprite noneSprite;
    [SerializeField] private GameObject timerObj;

    [SerializeField] private bool isStartMap;
    private void Awake()
    {
        audio = GetComponent<AudioSource>();
        animator = GetComponent<Animator>();
        clips = Resources.LoadAll<AudioClip>("Object/Dungeon_2_Computer");
    }
    // Start is called before the first frame update
    void Start()
    {
        if (animator) animator.enabled = false;
        timerObj = GameObject.Find("Canvas").transform.Find("Timer").gameObject;
    }

    // Update is called once per frame
    void Update()
    {
        delay -= Time.deltaTime;
        if (delay <= 0.0f) delay = 0.0f;

        RaycastHit2D hit = Physics2D.BoxCast(transform.position + vOffset, vSize, 0, Vector2.zero, 0, 1 << 9);
        if (hit && !isSelect && !isOpen && !isUse && !isUsing && !TextOpenController.instance.isOpen && !PDG.Dungeon.instance.newObjectSelected)
        {
            PDG.Dungeon.instance.newObjectSelected = true;
            PDG.Dungeon.instance.newObjectSelectedObj = gameObject;
            isSelect = true;
            controllerObj.SetActive(true);
            controllerObj.GetComponent<SpriteRenderer>().sprite = PDG.Dungeon.instance.isJoystrick ? PDG.Dungeon.GetJoyStrickKeySprite(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_ACTIVE]) : PDG.Dungeon.GetJoyStrickKeySprite(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ACTIVE]);
            TextOpenController.instance.ShowText(LocalizeManager.GetLocalize(firstCommnet), Color.white, 24, controllerObj, 0.5f, true);
        }
        if (hit && !isSelect && isOpen && !isUse && !isUsing && !TextOpenController.instance.isOpen)
        {
            isSelect = true;
            controllerObj.SetActive(true);
            TextOpenController.instance.ShowText(LocalizeManager.GetLocalize(secondCommnet), Color.white, 24, controllerObj, 0.5f, true);
        }
        else if (!hit)
        {
            if (PDG.Dungeon.instance.newObjectSelectedObj == gameObject)
            {
                PDG.Dungeon.instance.newObjectSelectedObj = null;
                PDG.Dungeon.instance.newObjectSelected = false;
            }
            isUsing = false;
            isSelect = false;
            isOpen = false;
            if (animator) animator.enabled = false;
            controllerObj.SetActive(false);
            if (TextOpenController.instance.targetObj == controllerObj) TextOpenController.instance.TextClose(Color.white);
        }
        if (isSelect)
        {
            if ((PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ACTIVE], true) || PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_ACTIVE], true)) && !isOpen && delay <= 0.0f) 
            {
                SoundManager.instance.StartAudio(new string[1] { "Click_Maca" }, VOLUME_TYPE.EFFECT);
                isOpen = true;
                isSelect = false;
                controllerObj.SetActive(false);
                delay = 0.1f;
            }
            if ((PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ACTIVE], true) || PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_ACTIVE], true)) && isOpen && delay <= 0.0f)
            {
                SoundManager.instance.StartAudio(new string[1] { "Click_Maca" }, VOLUME_TYPE.EFFECT);
                isSelect = false;
                controllerObj.SetActive(false);
                if (animator) animator.enabled = true;
                delay = 0.1f;
                if (Inventorys.Instance.glowindskull >= useCount)
                {
                    isUse = true;
                    if (nowDungeon == 3 && isOne) //분해합성기
                    {
                        if (PDG.Player.instance.nowRightWeaponName.Equals(string.Empty))
                        {
                            TextOpenController.instance.ShowText(LocalizeManager.GetLocalize(lackComment), Color.white, 24, controllerObj, 0.5f, true);
                        }
                        else
                        {
                            Inventorys.Instance.glowindskull -= useCount;
                            SaveAndLoadManager.instance.SaveItem(new ItemSave
                            {
                                _id = "glowing_skull",
                                _nCount = Inventorys.Instance.glowindskull
                            }, true);
                            Inventorys.Instance.ItemRetire(PDG.Player.instance.nowRightWeaponName);
                            PDG.Player.instance.nowRightWeaponName = string.Empty;
                            PDG.Player.instance.itemTypeRight = string.Empty;
                            PDG.Player.instance.damageRightOrigin = new int[2] { 0, 0 };
                            PDG.Player.instance.bulletRightRangeOrigin = 0;
                            if (Inventorys.Instance.QuickSlots[2].GetComponent<Animator>())
                            {
                                if (Inventorys.Instance.QuickSlots[2].GetComponent<Animator>().runtimeAnimatorController)
                                {
                                    Inventorys.Instance.QuickSlots[2].GetComponent<Animator>().enabled = false;
                                }
                            }
                            Inventorys.Instance.QuickSlots[2].sprite = noneSprite;
                            PDG.Dungeon.isRebuildDevice = true;
                            TextOpenController.instance.ShowText(LocalizeManager.GetLocalize(thirdCommnet), Color.white, 24, controllerObj, 0.5f, true);
                            if (GetComponent<SpriteOutline>()) GetComponent<SpriteOutline>().Clear();
                        }
                        if (PDG.Dungeon.instance.newObjectSelectedObj == gameObject)
                        {
                            PDG.Dungeon.instance.newObjectSelectedObj = null;
                            PDG.Dungeon.instance.newObjectSelected = false;
                        }

                        if (PDG.Dungeon.instance.isTestMode2)
                        {
                            isOpen = false;
                            isUsing = false;
                            isUse = false;
                        }
                    }
                    else if (nowDungeon == 3 && !isOne) // 자판기
                    {
                        Inventorys.Instance.glowindskull -= useCount;
                        SaveAndLoadManager.instance.SaveItem(new ItemSave
                        {
                            _id = "glowing_skull",
                            _nCount = Inventorys.Instance.glowindskull
                        }, true);
                        List<string> itemList2 = new List<string>();
                        ItemsData[] datas = System.Array.FindAll(SheetManager.Instance.ItemsDB.dataArray, item => item.Type1.Equals("Weapon"));
                        for (int i = 0; i < datas.Length; i++)
                        {
                            Sprite s = Resources.Load<Sprite>("Item/" + datas[i].Type1 + "/" + datas[i].Type2 + "/" + datas[i].ID);
                            if (s == null) continue;
                            if (datas[i].Grade.Equals("F")) continue;
                            if (datas[i].Grade.Equals("G")) continue;
                            if (Inventorys.Instance.UnlockAndMaxCountCheck(datas[i].ID))
                            {
                                itemList2.Add(datas[i].ID);
                            }
                        }
                        string info = itemList2[Random.Range(0, itemList2.Count)];
                        string tempID = info;
                        int ttt = tempID.LastIndexOf('_');
                        if (ttt != -1 && ttt == tempID.Length - 2)
                            tempID = tempID.Substring(0, tempID.Length - 2);
                        SaveAndLoadManager.instance.SaveItem(new ItemSave { _id = "EquipmentSave_" + tempID, _nCount = 1 });
                        SoundManager.instance.StartAudio(new string[1] { "Object/Computer" }, VOLUME_TYPE.EFFECT);
                        Inventorys.Instance.DropItem(info, 1, string.Empty, false);
                        TextOpenController.instance.ShowText(LocalizeManager.GetLocalize(thirdCommnet), Color.white, 24, controllerObj, 0.5f, true);
                        GetComponent<SpriteOutline>().Clear();
                        if (PDG.Dungeon.instance.newObjectSelectedObj == gameObject)
                        {
                            PDG.Dungeon.instance.newObjectSelectedObj = null;
                            PDG.Dungeon.instance.newObjectSelected = false;
                        }

                        if (PDG.Dungeon.instance.isTestMode2)
                        {
                            isOpen = false;
                            isUsing = false;
                            isUse = false;
                        }
                    }
                    else if (nowDungeon == 1 && !isOne)
                    {
                        isUsing = true;
                        isUse = false;
                        PlayerCombat.instance.tCoroutine = StartCoroutine(TimerCoroutine());
                    }
                    else if (nowDungeon == 1 && isOne)
                    {
                        SoundManager.instance.StartAudio(new string[1] { "Object/healthtower" }, VOLUME_TYPE.AMBIENT);
                        PlayerCombat.ps = PlayerState.Health;
                        StartCoroutine(ActionHealth());
                    }
                    else
                    {
                    }
                }
                else
                {
                    isUsing = true;
                    TextOpenController.instance.ShowText(LocalizeManager.GetLocalize(lackComment), Color.white, 24, controllerObj, 0.5f, true);
                    if (PDG.Dungeon.instance.newObjectSelectedObj == gameObject)
                    {
                        PDG.Dungeon.instance.newObjectSelectedObj = null;
                        PDG.Dungeon.instance.newObjectSelected = false;
                    }
                }
            }
        }
        if (timerObj.activeSelf && (Input.GetKeyDown(KeyCode.Escape) || PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_CANCLE], true)) && PDG.Dungeon.instance.closeCoolTime > 0.2f) 
        {
            PDG.Dungeon.instance.closeCoolTime = 0.0f;
            isSelect = false;
            if (PDG.Dungeon.instance.newObjectSelectedObj == gameObject)
            {
                PDG.Dungeon.instance.newObjectSelectedObj = null;
                PDG.Dungeon.instance.newObjectSelected = false;
            }
            isOpen = false;
            isUsing = false;
            if (audio) audio.Stop();
            if (animator) animator.enabled = false;
            controllerObj.SetActive(false);
            if (TextOpenController.instance.targetObj == controllerObj)
            {
                TextOpenController.instance.TextClose(Color.white);
                timerObj.SetActive(false);
            }
            PlayerCombat.instance.tCoroutine = null;
        }
    }
    IEnumerator TimerCoroutine()
    {
        timerObj.SetActive(true);
        float t = 0.0f;
        while (isUsing)
        {
            Vector3 p = controllerObj.transform.position;
            p.y -= 0.5f;
            timerObj.transform.position = Camera.main.WorldToScreenPoint(p);

            if (audio && !audio.isPlaying)
            {
                audio.clip = clips[Random.Range(0, clips.Length)];
                audio.volume =  (SoundManager.isMute || (PDG.Dungeon.instance == null ? false : PDG.Dungeon.instance.isPause) || (PDG.Player.instance == null ? false : PDG.Player.instance.isDie)) ? 0 : SoundManager.EffectVolume;
                audio.Play();
            }

            t += Time.deltaTime;
            int tInt = Mathf.CeilToInt(fTime - t);
            timerObj.transform.Find("Text").GetComponent<UnityEngine.UI.Text>().text = tInt.ToString();
            timerObj.transform.Find("Image").GetComponent<UnityEngine.UI.Image>().fillAmount = t / fTime;
            timerObj.transform.Find("TextCont").GetComponent<UnityEngine.UI.Text>().text = LocalizeManager.GetLocalize("d1_02_ing");
            if (t >= fTime) break;
            yield return new WaitForSeconds(Time.deltaTime);
        }
        if (isUsing)
        {
            Inventorys.Instance.glowindskull -= useCount;
            SaveAndLoadManager.instance.SaveItem(new ItemSave
            {
                _id = "glowing_skull",
                _nCount = Inventorys.Instance.glowindskull
            }, true);
            SoundManager.instance.StartAudio(new string[1] { "Object/Potion and Alchemy 14" }, VOLUME_TYPE.EFFECT);
            PDG.Player.instance.damagePlus += 3;
            PDG.Player.instance.damagePlusNear += 3;
            PDG.Player.instance.damageQPlus += 3;
            PDG.Player.instance.damageQPlusNear += 3;
            PDG.Player.instance.damageRightPlus += 3;
            PDG.Player.instance.damageRightPlusNear += 3;
            isUse = true;
            TextOpenController.instance.ShowText(LocalizeManager.GetLocalize(thirdCommnet), Color.white, 24, controllerObj, 0.5f, true);
            GetComponent<SpriteOutline>().Clear(); if (PDG.Dungeon.instance.newObjectSelectedObj == gameObject)
            {
                PDG.Dungeon.instance.newObjectSelectedObj = null;
                PDG.Dungeon.instance.newObjectSelected = false;
            }
        }
        timerObj.SetActive(false);
        PlayerCombat.instance.tCoroutine = null;

        if(PDG.Dungeon.instance.isTestMode2)
        {
            isOpen = false;
            isUsing = false;
            isUse = false;
        }
    }
    IEnumerator ActionHealth()
    {
        yield return StartCoroutine(SpakeCoroutine());
        PDG.Player.instance.HpRecovery(999);
        PlayerCombat.ps = PlayerState.Idle;
        SoundManager.instance.StopAudio(new string[1] { "Object/healthtower" });
        TextOpenController.instance.ShowText(LocalizeManager.GetLocalize(thirdCommnet), Color.white, 24, controllerObj, 0.5f, true);
        GetComponent<SpriteOutline>().Clear();
        if (PDG.Dungeon.instance.newObjectSelectedObj == gameObject)
        {
            PDG.Dungeon.instance.newObjectSelectedObj = null;
            PDG.Dungeon.instance.newObjectSelected = false;
        }


        if (PDG.Dungeon.instance.isTestMode2)
        {
            yield return new WaitForSeconds(0.5f);
            isOpen = false;
            isUsing = false;
            isUse = false;
        }
    }
    IEnumerator SpakeCoroutine()
    {
        float a = 0.0f;
        UnityEngine.UI.Image img = GameObject.Find("Canvas").transform.Find("Spake").GetComponent<UnityEngine.UI.Image>();
        while (true)
        {
            img.color = new Color(1, 1, 1, a);
            a += Time.deltaTime * 5;
            if (a >= 1.0f) break;
            yield return null;
        }
        img.color = Color.white;
        while (true)
        {
            img.color = new Color(1, 1, 1, a);
            a -= Time.deltaTime * 5;
            if (a <= 0.0f) break;
            yield return null;
        }
        img.color = new Color(1, 1, 1, 0);
    }
}
