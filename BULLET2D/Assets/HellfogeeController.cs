﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HellfogeeController : MonoBehaviour
{
    public string mobType;
    string _spritePath;
    Sprite[] _sprites;
    private SpriteRenderer sr;
    public Animator animator;

    public AudioClip[] clips;
    public bool isRandomMotion = false;
    public float randomTime = 0.0f;

    [SerializeField] private Sprite fSprite;
    [SerializeField] private GameObject controllerObj;
    [SerializeField] private SpriteRenderer controllerSR;
    [SerializeField] private bool isSelect = false;
    bool isChecking = false;
    bool isFireEyes = false;
    [SerializeField] private Sprite helldogSpriite;

    [SerializeField] private Transform[] itemPositions;
    public Transform[] itemPositions2;
    public int nowItem = 0;
    [SerializeField] private string[] dropItems;
    [SerializeField] private string npcName;

    [Header("SlolText")]
    [SerializeField] private string[] SoloTexts;
    [SerializeField] private Text nowSoloText;
    [SerializeField] private Font _font;
    [SerializeField] private bool isTargeting = false;

    private void Awake()
    {
        animator = GetComponent<Animator>();
        sr = GetComponent<SpriteRenderer>();
    }
    // Start is called before the first frame update
    void Start()
    {
        string stage = PDG.Dungeon.instance.StageName;
        Load();

        int t = SaveAndLoadManager.instance.LoadItem("HellFogeeFlag");
        if (t == 1)
            isFireEyes = true;

        if (isFireEyes)
        {
            ItemSpawn();
        }
        else
        {
            for (int i = 0; i < itemPositions.Length; i++)
            {
                itemPositions[i].gameObject.SetActive(false);
            }
        }
        StartCoroutine(SoloText());
    }

    IEnumerator SoloText()
    {
        if (nowSoloText == null)
        {
            GameObject obj = new GameObject();
            Text nText = obj.AddComponent<Text>();
            nText.font = _font;
            nText.alignment = TextAnchor.MiddleCenter;
            nText.fontSize = 20;
            nText.horizontalOverflow = HorizontalWrapMode.Overflow;
            nowSoloText = nText;
            obj.transform.SetParent(GameObject.Find("Canvas").transform);
            obj.transform.localScale = Vector3.one;
            obj.transform.SetSiblingIndex(15);
        }

        while (true)
        {
            if (!isSelect)
            {
                nowSoloText.text = LocalizeManager.GetLocalize(SoloTexts[UnityEngine.Random.Range(0, SoloTexts.Length)]);
                yield return new WaitForSeconds(2.0f);
                nowSoloText.text = string.Empty;
            }
            else
            {
                nowSoloText.text = string.Empty;
            }
            yield return new WaitForSeconds(UnityEngine.Random.Range(2, 5));
        }
    }
    private void ItemSpawn()
    {
        string _date = DateTime.Now.ToString("yyyyMMdd");

        MailSaveData mData = SaveAndLoadManager.instance.LoadMail("hellfogee_item");
        if (mData._id.Equals(string.Empty))
        {
            string[] array = new string[itemPositions.Length];
            for (int i = 0; i < itemPositions.Length; i++)
            {
                itemPositions[i].gameObject.SetActive(true);
                array[i] = dropItems[UnityEngine.Random.Range(0, dropItems.Length)];
                itemPositions[i].GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("Item/Material/" + array[i]);
                itemPositions[i].GetComponent<ItemSelectController>().itemPath = array[i];
            }
            SaveAndLoadManager.instance.SaveMail(new MailSaveData
            {
                _id = "hellfogee_item",
                date = _date,
                isCheck = false,
                isPush = true,
                items = array,
                isItems = new bool[3] { false, false, false }
            });
            isChecking = true;
        }
        else if (!mData.date.Equals(_date)) 
        {
            string[] array = new string[itemPositions.Length];
            for (int i = 0; i < itemPositions.Length; i++)
            {
                itemPositions[i].gameObject.SetActive(true);
                array[i] = dropItems[UnityEngine.Random.Range(0, dropItems.Length)];
                itemPositions[i].GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("Item/Material/" + array[i]);
                itemPositions[i].GetComponent<ItemSelectController>().itemPath = array[i];
                itemPositions[i].GetComponent<ItemSelectController>().num = i;
            }
            SaveAndLoadManager.instance.SaveMail(new MailSaveData
            {
                _id = "hellfogee_item",
                date = _date,
                isCheck = false,
                isPush = true,
                items = array,
                isItems = new bool[3] { false, false, false }
            });
            isChecking = true;
        }
        else if (mData.isPush && !mData.isCheck)
        {
            isChecking = true;
            for (int i = 0; i < itemPositions.Length; i++)
            {
                if (mData.isItems[i])
                {
                    itemPositions[i].gameObject.SetActive(false);
                    continue;
                }
                itemPositions[i].gameObject.SetActive(true);
                itemPositions[i].GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("Item/Material/" + mData.items[i]);
                itemPositions[i].GetComponent<ItemSelectController>().itemPath = mData.items[i];
                itemPositions[i].GetComponent<ItemSelectController>().num = i;
            }
        }
    }
    // Update is called once per frame
    void Update()
    {
        if (PDG.Player.instance == null) return;

        if (nowSoloText != null)
        {
            nowSoloText.gameObject.transform.position = Camera.main.WorldToScreenPoint(controllerObj.transform.position);
        }

        if (isRandomMotion)
        {
            randomTime -= Time.deltaTime;
            if (randomTime <= 0.0f)
            {
                int r = UnityEngine.Random.Range(0, 2);
                animator.SetInteger("RandomSelect", r);
                animator.SetTrigger("RandomChange");
                randomTime = UnityEngine.Random.Range(10, 30);
                if (r == 0) GetComponent<AudioSource>().clip = clips[1];
                else GetComponent<AudioSource>().clip = clips[0];
                GetComponent<AudioSource>().Play();
            }
            float dis = Vector3.Distance(PDG.Player.instance.transform.position, transform.position);
            if (dis > 10) GetComponent<AudioSource>().volume = 0;
            float tVolume =  (SoundManager.isMute || (PDG.Dungeon.instance == null ? false : PDG.Dungeon.instance.isPause) || (PDG.Player.instance == null ? false : PDG.Player.instance.isDie)) ? 0 : SoundManager.EffectVolume;
            float t = tVolume / 10;
            GetComponent<AudioSource>().volume = tVolume - (dis * t);
        }

        RaycastHit2D hit = Physics2D.BoxCast(transform.position + new Vector3(-0.2f, -0.4f), new Vector2(3.5f, 4.5f), 0, Vector2.zero, 0, 1 << 9);
        RaycastHit2D hit3 = Physics2D.CircleCast(transform.position, 15, Vector2.zero, 0, 1 << 9);
        if (hit3 && isTargeting)
        {
            Vector3 dir = hit3.collider.transform.position - transform.position;
            dir.Normalize();
            if (dir.x < 0) GetComponent<SpriteRenderer>().flipX = true;
            else GetComponent<SpriteRenderer>().flipX = false;
        }
        if (hit && !isSelect && !TextOpenController.instance.isOpen && !PDG.Dungeon.instance.newObjectSelected)
        {
            isSelect = true;
            PDG.Dungeon.instance.newObjectSelected = true;
            PDG.Dungeon.instance.newObjectSelectedObj = gameObject;
            if (nowSoloText != null) nowSoloText.text = string.Empty;
            controllerObj.SetActive(true);
            controllerObj.GetComponent<SpriteRenderer>().sprite = PDG.Dungeon.instance.isJoystrick? PDG.Dungeon.GetJoyStrickKeySprite(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_ACTIVE]) : PDG.Dungeon.GetJoyStrickKeySprite(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ACTIVE]);
            animator.SetBool("isSelected", true);
            TextOpenController.instance.ShowText(LocalizeManager.GetLocalize(npcName), Color.white, 24, controllerObj, 0.5f, true);
        }
        else if (!hit)
        {
            isSelect = false;
            if (PDG.Dungeon.instance.newObjectSelectedObj == gameObject)
            {
                PDG.Dungeon.instance.newObjectSelectedObj = null;
                PDG.Dungeon.instance.newObjectSelected = false;
            }
            controllerObj.SetActive(false);
            animator.SetBool("isSelected", false);
            if (TextOpenController.instance.targetObj == controllerObj)
            {
                TextOpenController.instance.TextClose(Color.white);
            }
        }

        if (isSelect)
        {
            if ((PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ACTIVE], true) || PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_ACTIVE], true)) && PDG.Dungeon.instance.closeCoolTime > 0.2f)
            {
                if (MakeController.instance.isOpen) return;
                PDG.Dungeon.instance.closeCoolTime = 0.0f;
                SoundManager.instance.StartAudio(new string[1] { "Object/active_F" }, VOLUME_TYPE.EFFECT);
                isSelect = false;
                if (PDG.Dungeon.instance.newObjectSelectedObj == gameObject)
                {
                    PDG.Dungeon.instance.newObjectSelectedObj = null;
                    PDG.Dungeon.instance.newObjectSelected = false;
                }
                controllerObj.SetActive(false);
                animator.SetBool("isSelected", false);
                if (!isFireEyes)
                {
                    int t = SaveAndLoadManager.instance.LoadItem("Boss_eyes_fire");
                    if (t > 0)
                    {
                        TextOpenController.instance.ShowText(LocalizeManager.GetLocalize("ajit_helldog_fireeyes_comment"), Color.white, 24, controllerObj, 2.0f);
                        Inventorys.Instance.ShowGetItemUI("fireeyes_helldog", helldogSpriite);
                        SaveAndLoadManager.instance.SaveItem(new ItemSave { _id = "HellFogeeFlag", _nCount = 1 }, true);
                        isFireEyes = true;
                        ItemSpawn();
                    }
                    else
                    {
                        TextOpenController.instance.ShowText(LocalizeManager.GetLocalize("ajit_helldog_fireeyes_not_comment"), Color.white, 24, controllerObj, 2.0f, true);
                    }
                }
                else
                {
                    MakeController.instance.ShowMake(string.Empty, "hellfogee", gameObject);
                }
            }
        }
    }
    void LateUpdate()
    {
        if (sr == null || sr.sprite == null)
            return;
        Load();
        var name = sr.sprite.name;
        var sprite = System.Array.Find(_sprites, item => item.name == name);
        if (sprite) sr.sprite = sprite;
    }
    void Load()
    {
        var spritePath = "NPC/" + mobType;
        if (!spritePath.Equals(_spritePath))
        {
            _spritePath = spritePath;
            _sprites = Resources.LoadAll<Sprite>(_spritePath);
        }
    }
}
