﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EquipmentUpgradeController : MonoBehaviour
{
    [SerializeField] private Sprite fSprite;
    [SerializeField] private GameObject controllerObj;
    [SerializeField] private SpriteRenderer controllerSR;
    [SerializeField] private bool isSelect = false;
    [SerializeField] private bool isTargeting = true;
    [SerializeField] private Vector3 offSet;
    [SerializeField] private Vector3 size;
    [SerializeField] private bool isBossRush = false;
    public int nowItemNum = 0;
    public Transform[] ItemPositions;
    // Start is called before the first frame update
    void Start()
    {
    }
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireCube(transform.position + offSet, size);
    }
    // Update is called once per frame
    void Update()
    {
        RaycastHit2D hit = Physics2D.BoxCast(transform.position + offSet, size, 0, Vector2.zero, 0, 1 << 9);
        RaycastHit2D hit3 = Physics2D.CircleCast(transform.position, 5, Vector2.zero, 0, 1 << 9);
        if (hit3 && isTargeting)
        {
            if (!GetComponent<Animator>().GetBool("Make"))
            {
                GetComponent<Animator>().SetTrigger("MakeTrigger");
                GetComponent<Animator>().SetBool("Make", true);
            }
        }
        else
        {
            GetComponent<Animator>().SetBool("Make", false);
        }
        if (hit && !isSelect)
        {
            isSelect = true;
            controllerObj.SetActive(true);
            controllerObj.GetComponent<SpriteRenderer>().sprite = PDG.Dungeon.instance.isJoystrick ? PDG.Dungeon.GetJoyStrickKeySprite(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_ACTIVE]) : PDG.Dungeon.GetJoyStrickKeySprite(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ACTIVE]);
            TextOpenController.instance.ShowText(LocalizeManager.GetLocalize("ajit_equipment"), Color.white, 24, controllerObj, 2.0f, true);
        }
        else if (!hit)
        {
            isSelect = false;
            controllerObj.SetActive(false);
            if (TextOpenController.instance.targetObj == controllerObj) TextOpenController.instance.TextClose(Color.white);
        }

        if (isSelect)
        {
            if ((PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ACTIVE], true) || PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_ACTIVE], true)) && PDG.Dungeon.instance.closeCoolTime > 0.2f)
            {
                PDG.Dungeon.instance.closeCoolTime = 0.0f;
                SoundManager.instance.StartAudio(new string[1] { "Click_Safe" }, VOLUME_TYPE.EFFECT);
                isSelect = false;
                controllerObj.SetActive(false);
                EquipmentUpgradeManager.instance.Show(gameObject, isBossRush);
                //MakeController.instance.ShowMake(string.Empty, "equipment", gameObject);
            }
        }
    }
}
