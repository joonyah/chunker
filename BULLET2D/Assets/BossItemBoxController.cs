﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class BossItemBoxController : MonoBehaviour
{
    [SerializeField] private Sprite fSprite;
    [SerializeField] private GameObject controllerObj;
    [SerializeField] private SpriteRenderer controllerSR;
    [SerializeField] private bool isSelect = false;

    [SerializeField] private Sprite defaultSprite;
    [SerializeField] private Sprite endSprite;

    [SerializeField] private SpriteRenderer sr;
    [SerializeField] private Animator animator;
    [SerializeField] private bool isOpen = false;

    public string[] itemName;
    public string _id;

    public bool isMaterial = false;

    [SerializeField] private bool isStartMap;
    [SerializeField] private bool isStartMap2;
    [SerializeField] private int StageLevel = 0;

    public List<GameObject> ItemLists = new List<GameObject>();
    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
        sr = GetComponent<SpriteRenderer>();

        if (animator) animator.enabled = false;
        sr.sprite = defaultSprite;
        if (isStartMap && !PDG.Dungeon.instance.isTestMode) gameObject.SetActive(false);

        if (isStartMap2)
        {
            PDG.Dungeon.instance.TestBoxs.Add(this);
            ItemSetting();
        }
    }
    void ItemSetting()
    {
        int t = (StageLevel + 1) * 10;
        PDG.Dungeon.instance.TestModeItemLIst.Clear();
        itemName = new string[t];
        for (int j = 0; j < t; j++)
        {
            while (true)
            {
                int _type = Random.Range(0, 2);
                ChestPerData data = System.Array.Find(SheetManager.Instance.ChestPerDB.dataArray, item => item.Stagelevel.Equals(StageLevel));
                int gradeNum = SaveAndLoadManager.GetBoxGrade(data);
                string BoxGrade = data.Chest[gradeNum];
                BoxGrade = BoxGrade.Substring(BoxGrade.Length - 1, 1);
                List<ItemsData> iDatas = System.Array.FindAll(SheetManager.Instance.ItemsDB.dataArray, item => item.Grade.ToUpper().Equals(BoxGrade.ToUpper()) && (_type == 0 ? item.Type1.Equals("Weapon") : item.Type1.Equals("Passive"))).ToList();
                for (int i = 0; i < iDatas.Count; i++)
                {
                    Sprite s = Resources.Load<Sprite>("Item/" + iDatas[i].Type1 + "/" + iDatas[i].Type2 + "/" + iDatas[i].ID);
                    if (s == null || !Inventorys.Instance.UnlockAndMaxCountCheck(iDatas[i].ID))
                    {
                        iDatas.RemoveAt(i);
                        i--;
                        continue;
                    }
                }
                if (iDatas.Count > 0)
                {
                    ItemsData iData = iDatas[UnityEngine.Random.Range(0, iDatas.Count)];
                    itemName[j] = iData.ID;
                    PDG.Dungeon.instance.TestModeItemLIst.Add(iData.ID);
                    break;
                }
            }
        }
    }
    // Update is called once per frame
    void Update()
    {
        RaycastHit2D hit = Physics2D.BoxCast(transform.position, new Vector2(2.0f, 3.0f), 0, Vector2.zero, 0, 1 << 9);
        if (hit && !isSelect && !PDG.Dungeon.instance.newObjectSelected)
        {
            isSelect = true;
            PDG.Dungeon.instance.newObjectSelected = true;
            PDG.Dungeon.instance.newObjectSelectedObj = gameObject;
            controllerObj.SetActive(true);
            controllerObj.GetComponent<SpriteRenderer>().sprite = PDG.Dungeon.instance.isJoystrick ? PDG.Dungeon.GetJoyStrickKeySprite(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_ACTIVE]) : PDG.Dungeon.GetJoyStrickKeySprite(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ACTIVE]);
            if(isStartMap)
            {
                TextOpenController.instance.ShowText(LocalizeManager.GetLocalize("sorry_box"), Color.white, 24, controllerObj, 2.0f, true);
            }
        }
        else if (!hit)
        {
            isSelect = false;
            if (PDG.Dungeon.instance.newObjectSelectedObj == gameObject)
            {
                PDG.Dungeon.instance.newObjectSelectedObj = null;
                PDG.Dungeon.instance.newObjectSelected = false;
            }
            if (TextOpenController.instance.targetObj == controllerObj)
            {
                TextOpenController.instance.TextClose(Color.white);
            }
            controllerObj.SetActive(false);
            if (animator && !isOpen) animator.enabled = false;
            sr.sprite = defaultSprite;
        }

        if (isSelect && !isOpen)
        {
            if ((PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ACTIVE], true) || PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_ACTIVE], true)) && PDG.Dungeon.instance.closeCoolTime > 0.2f) 
            {
                PDG.Dungeon.instance.closeCoolTime = 0.0f;
                isSelect = false;
                if (PDG.Dungeon.instance.newObjectSelectedObj == gameObject)
                {
                    PDG.Dungeon.instance.newObjectSelectedObj = null;
                    PDG.Dungeon.instance.newObjectSelected = false;
                }
                isOpen = true;
                if (PDG.Dungeon.instance.isTestMode2)
                {
                    for (int i = 0; i < PDG.Dungeon.instance.TestBoxs.Count; i++)
                        PDG.Dungeon.instance.TestBoxs[i].isOpen = true;
                }
                SoundManager.instance.StartAudio(new string[1] { "Object/active_F" }, VOLUME_TYPE.EFFECT);
                SoundManager.instance.StartAudio(new string[1] { "bossBoxOpen" }, VOLUME_TYPE.EFFECT);
                controllerObj.SetActive(false);
                if (animator) animator.enabled = true;
                if (animator) animator.Rebind();
                ItemDrop();
                if (isStartMap2) PDG.Dungeon.instance.StageLevel = StageLevel;
                SaveAndLoadManager.instance.SaveItem(new ItemSave { _id = "TEST_BOX", _nCount = 1 });
            }
        }
    }
    public void ResetBox()
    {
        if(animator)
        {
            animator.enabled = false;
            sr.sprite = defaultSprite;
            isOpen = false;
            if (isStartMap2)
            {
                for (int i = 0; i < ItemLists.Count; i++)
                {
                    if(ItemLists[i])
                    {
                        Destroy(ItemLists[i]);
                    }
                }
                ItemLists.Clear();
                ItemSetting();
            }
        }
    }
    public void ItemDrop()
    {
        UnlockListsData[] pikoUnlockDatas = System.Array.FindAll(SheetManager.Instance.UnlockListsDB.dataArray, item => item.NPCID.Equals(_id));
        for (int i = 0; i < pikoUnlockDatas.Length; i++)
        {
            if (SaveAndLoadManager.instance.LoadUnlock(pikoUnlockDatas[i].Itemid) < 1)
            {
                SaveAndLoadManager.instance.SaveUnlock(pikoUnlockDatas[i].Itemid, 1);
                ItemsData iData = System.Array.Find(SheetManager.Instance.ItemsDB.dataArray, item => item.ID.Equals(pikoUnlockDatas[i].Itemid));
                Inventorys.Instance.ShowGetItemUI("monster_unlockitem", Resources.Load<Sprite>("Item/" + iData.Type1 + "/" + iData.Type2 + "/" + iData.ID));
            }
        }

        Vector3 tPos = transform.position;
        for (int i = 0; i < itemName.Length; i++)
        {
            float angle = -90.0f;
            angle += (i * (360.0f / itemName.Length));
            Vector3 pos = GetPosition(tPos, angle);
            Vector3 dir = pos - tPos;
            dir.Normalize();
            GameObject obj = ObjManager.Call().GetObject("Item");
            if (obj != null)
            {
                Item _item = obj.GetComponent<Item>();
                if (!isMaterial)
                {
                    _item._Item = new ItemInfo
                    {
                        Data = Inventorys.Instance.CopyItem(System.Array.Find(SheetManager.Instance.ItemsDB.dataArray, item => item.ID.Equals(itemName[i])))
                    };
                    if (!_item._Item.Data.ID.Equals("glowing_skull"))
                    {
                        Inventorys.Instance.DiscardedItem.Add(_item._Item);
                        Inventorys.Instance.DiscardedItemString.Add(_item._Item.Data.ID);
                    }
                }
                else if(itemName[i].Equals("glowing_skull"))
                {
                    _item._Item = new ItemInfo
                    {
                        Data = Inventorys.Instance.CopyItem(System.Array.Find(SheetManager.Instance.ItemsDB.dataArray, item => item.ID.Equals(itemName[i])))
                    };
                }
                else
                {
                    string[] s = itemName[i].Split('_');
                    string t = string.Empty;
                    for (int j = 1; j < s.Length; j++)
                    {
                        if (j == 1) t += s[j];
                        else t += "_" + s[j];
                    }
                    _item._Item = new ItemInfo
                    {
                        Data = new ItemsData
                        {
                            Type1 = "Material",
                            Type2 = s[0],
                            ID = t
                        }
                    };
                }
                _item.dir = dir;
                if (_item._Item.Data.ID.Equals("glowing_skull"))
                    _item.itemCount = isStartMap ? 200 : 1;
                else if (_item._Item.Data.ID.Contains("boss"))
                    _item.itemCount = 1;
                else if (_item._Item.Data.ID.Equals("hellfogee"))
                    _item.itemCount = 1;
                else
                    _item.itemCount = isStartMap ? 10 : 1;
                _item.rPow = 1;
                float startDis2 = 1.0f * (itemName.Length / 20.0f);
                float angle2 = angle += 90.0f;
                if (angle2 <= 180) startDis2 -= (angle2 / 150.0f);
                else startDis2 -= ((180.0f / 150.0f) + ((180.0f - angle2) / 150.0f));

                obj.transform.position = pos + (dir * startDis2);
                obj.SetActive(true);
                if (!isMaterial)
                {
                    string tempID = itemName[i];
                    int ttt = tempID.LastIndexOf('_');
                    if (ttt != -1 && ttt == tempID.Length - 2)
                        tempID = tempID.Substring(0, tempID.Length - 2);
                    SaveAndLoadManager.instance.SaveItem(new ItemSave { _id = "EquipmentSave_" + tempID, _nCount = 1 });
                }
                if (PDG.Dungeon.instance.isTestMode2)
                {
                    ItemLists.Add(obj);
                }
            }
        }
    }
    #region -- Tool --
    public static float GetAngle(Vector3 vStart, Vector3 vEnd)
    {
        Vector3 v = vEnd - vStart;

        return Mathf.Atan2(v.y, v.x) * Mathf.Rad2Deg;
    }
    public static Vector3 GetPosition(Vector3 vStart, float angle)
    {
        float x = Mathf.Cos(Mathf.Deg2Rad * angle) + vStart.x;
        float y = Mathf.Sin(Mathf.Deg2Rad * angle) + vStart.y;

        return new Vector3(x, y, 0);
    }
    #endregion
}
