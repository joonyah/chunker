﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UBhCollistion : MonoBehaviour
{
    public UbhBullet ubBullet;
    public string _mobName;
    public GameObject hitEffect;
    private void OnEnable()
    {
        GetComponent<CircleCollider2D>().isTrigger = true;
    }
    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.layer.Equals(0) || collision.gameObject.layer.Equals(27) || collision.gameObject.layer.Equals(8))
        {
            if (collision.name.Equals("Shield"))
            {
                PDG.Player.instance.right_defence_gauge -= 1.0f;
                Inventorys.Instance.GuageShaker(1);
                CameraShaker._instance.StartShake(0.2f, 0.02f, 0.2f);
            }
            if (collision.name.Contains("dna_mo_blade"))
            {
                PDG.Player.instance.left_defence_gauge -= 1.0f;
                Inventorys.Instance.GuageShaker(0);
                CameraShaker._instance.StartShake(0.2f, 0.02f, 0.2f);
            }

            GameObject obj = ObjManager.Call().GetObject("Bullet_Hit");
            obj.transform.position = transform.position;
            obj.GetComponent<SwordHitEffectController>()._isMonster = true;
            obj.GetComponent<SwordHitEffectController>()._effctName = "pistol_bullet";
            obj.GetComponent<Animator>().Rebind();
            obj.transform.localEulerAngles = new Vector3(0, 0, 0);
            obj.SetActive(true);
            UbhObjectPool.instance.ReleaseBullet(ubBullet);
        }
        if (collision.gameObject.layer.Equals(9))
        {
            if(hitEffect)
            {
                GameObject obj = Instantiate(hitEffect);
                obj.transform.position = transform.position;
                Destroy(obj, 2f);
            }
            else
            {
                GameObject obj = ObjManager.Call().GetObject("Bullet_Hit");
                obj.transform.position = transform.position;
                obj.GetComponent<SwordHitEffectController>()._isMonster = true;
                obj.GetComponent<SwordHitEffectController>()._effctName = "pistol_bullet";
                obj.GetComponent<Animator>().Rebind();
                obj.transform.localEulerAngles = new Vector3(0, 0, 0);
                obj.SetActive(true);
            }

            if (_mobName.Equals("octopus_king") || _mobName.Equals("boss_stg") || _mobName.Equals("boss_npc") || _mobName.Equals("boss_golem") || _mobName.Equals("boss_stone"))
            {
                if (collision.gameObject.GetComponent<PDG.Player>())
                    collision.gameObject.GetComponent<PDG.Player>().SetDamage(1, LocalizeManager.GetLocalize("monster_" + _mobName), DEATH_TYPE.DEATH_BOSS, _mobName);
            }
            else if (_mobName.Equals("piko") || _mobName.Equals("slime_bright") || _mobName.Equals("golem_ice"))
            {
                if (collision.gameObject.GetComponent<PDG.Player>())
                    collision.gameObject.GetComponent<PDG.Player>().SetDamage(1, LocalizeManager.GetLocalize("monster_" + _mobName), DEATH_TYPE.DEATH_SUB_BOSS, _mobName);
            }
            else
            {
                if (collision.gameObject.GetComponent<PDG.Player>())
                    collision.gameObject.GetComponent<PDG.Player>().SetDamage(1, LocalizeManager.GetLocalize("monster_" + _mobName), DEATH_TYPE.DEATH_NORMAL, _mobName);
            }
            UbhObjectPool.instance.ReleaseBullet(ubBullet);
        }
    }
    int GetDirectionAngle(Collider2D collision)
    {
        float angle = GetAngle(collision.transform.position, transform.position);
        while (angle > 360.0f) angle -= 360.0f;
        while (angle < 0) angle += 360.0f;

        if (angle >= 45.0f && angle < 135.0f)
        {
            // 위쪽
            return 270;
        }
        else if (angle >= 135.0f && angle < 225.0f)
        {
            // 왼쪽
            return 0;
        }
        else if (angle > 225.0f && angle < 315.0f)
        {
            //아래쪽
            return 90;
        }
        else
        {
            //오른쪽
            return 180;
        }
    }
    #region -- Tool --
    public static float GetAngle(Vector3 vStart, Vector3 vEnd)
    {
        Vector3 v = vEnd - vStart;

        return Mathf.Atan2(v.y, v.x) * Mathf.Rad2Deg;
    }
    public static Vector3 GetPosition(Vector3 vStart, float angle)
    {
        float x = Mathf.Cos(Mathf.Deg2Rad * angle) + vStart.x;
        float y = Mathf.Sin(Mathf.Deg2Rad * angle) + vStart.y;

        return new Vector3(x, y, 0);
    }
    #endregion
}
