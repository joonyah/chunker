﻿using UnityEngine;

/// <summary>
/// Ubh bullet for sprite2d and rigidbody2d prefabs.
/// </summary>
public class UbhBulletSimpleSprite2d : UbhBullet
{
    private bool m_isActive;

    private void Start()
    {
        if (m_rigidbody2d == null) m_rigidbody2d = transform.GetChild(0).GetComponent<Rigidbody2D>();
        if (m_collider2ds == null) m_collider2ds = transform.GetChild(0).GetComponents<Collider2D>();
        if (m_spriteRenderers == null) m_spriteRenderers = transform.GetChild(0).GetComponents<SpriteRenderer>();
    }

    /// <summary>
    /// Activate/Inactivate flag
    /// Override this property when you want to change the behavior at Active / Inactive.
    /// </summary>
    public override bool isActive { get { return m_isActive; } }

    /// <summary>
    /// Activate/Inactivate Bullet
    /// </summary>
    public override void SetActive(bool isActive)
    {
        m_isActive = isActive;
        
        if (transform.childCount == 0)
        {
            GameObject newObj = new GameObject();
            newObj.transform.SetParent(transform);
            Rigidbody2D rr =  newObj.AddComponent<Rigidbody2D>();
            rr.bodyType = RigidbodyType2D.Kinematic;
            SpriteRenderer ss = newObj.AddComponent<SpriteRenderer>();
            ss.sprite = _sprite;
            CircleCollider2D cc = newObj.AddComponent<CircleCollider2D>();
            cc.radius = 0.25f;
            UBhCollistion tt = newObj.AddComponent<UBhCollistion>();
            tt._mobName = mobName;
            tt.hitEffect = hitEffect;
            tt.ubBullet = this;

            m_rigidbody2d = rr;
            m_collider2ds = new Collider2D[1];
            m_collider2ds[0] = cc;

            m_spriteRenderers = new SpriteRenderer[1];
            m_spriteRenderers[0] = ss;

            m_rigidbody2d.simulated = isActive;

            ss.sortingLayerName = "Attack";
            ss.sortingOrder = 2;

            newObj.transform.localPosition = Vector3.zero;
            newObj.layer = 25;
        }
        else
        {
            if (m_rigidbody2d == null) m_rigidbody2d = transform.GetChild(0).GetComponent<Rigidbody2D>();
            if (m_rigidbody2d != null) m_rigidbody2d.simulated = isActive;
        }




        if (m_collider2ds != null && m_collider2ds.Length > 0)
        {
            for (int i = 0; i < m_collider2ds.Length; i++)
            {
                m_collider2ds[i].enabled = isActive;
            }
        }

        if (m_spriteRenderers != null && m_spriteRenderers.Length > 0)
        {
            for (int i = 0; i < m_spriteRenderers.Length; i++)
            {
                m_spriteRenderers[i].enabled = isActive;
            }
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        Debug.Log(collision.name);
    }
}
