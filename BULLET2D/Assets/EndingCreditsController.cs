﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;
using UnityEngine.SceneManagement;

public class EndingCreditsController : MonoBehaviour
{
    [SerializeField] private PostProcessVolume pVolume;
    [SerializeField] private ChromaticAberration pAberration;
    [SerializeField] private float bounsSpeed = 1.0f;
    [SerializeField] private float creditSpeed = 1.0f;
    [SerializeField] private RectTransform creditsObj;

    [SerializeField] private bool isSkip = false;
    private void Awake()
    {
        //QualitySettings.vSyncCount = 0;
        Application.targetFrameRate = 60;
    }
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(MainBouns());
        StartCoroutine(SkipActive());
        StartCoroutine(BounsFade());
    }

    // Update is called once per frame
    void Update()
    {
        if (isSkip)
        {
            if ((Input.GetKeyDown(KeyCode.Escape) || PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_CANCLE], true)) && PDG.Dungeon.instance.closeCoolTime > 0.2f)
            {
                PDG.Dungeon.instance.closeCoolTime = 0.0f;
                SceneManager.LoadScene(2);
            }
        }
    }
    IEnumerator SkipActive()
    {
        yield return new WaitForSeconds(5.0f);
        isSkip = true;
    }
    IEnumerator BounsFade()
    {
        float a = 0.0f;
        GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, a);
        yield return new WaitForSeconds(1f);
        while (true)
        {
            a += Time.deltaTime / 2;
            GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, a);
            if (a >= 1.0f) break;
            yield return new WaitForSeconds(Time.deltaTime);
        }
        StartCoroutine(EndingCredits());
    }
    IEnumerator EndingCredits()
    {
        yield return new WaitForSeconds(1.0f);
        Vector3 pos = creditsObj.anchoredPosition3D;
        while(true)
        {
            pos.y += 1.0f * creditSpeed;
            creditsObj.anchoredPosition3D = pos;
            if (pos.y >= 0) break;
            yield return new WaitForSeconds(Time.deltaTime);
        }
        yield return new WaitForSeconds(1.0f);
        SceneManager.LoadScene(2);
    }
    IEnumerator MainBouns()
    {
        pVolume.profile.TryGetSettings(out pAberration);
        float a = 0.0f;
        bool isPlus = true;
        while (true)
        {
            if (isPlus)
            {
                a += Time.deltaTime * bounsSpeed;
                if (a >= 0.25f)
                {
                    a = 0.25f;
                    isPlus = !isPlus;
                }
            }
            else
            {
                a -= Time.deltaTime * bounsSpeed;
                if (a <= 0.0f)
                {
                    a = 0.0f;
                    isPlus = !isPlus;
                }
            }
            pAberration.intensity.value = a;
            yield return new WaitForSeconds(Time.deltaTime);
        }
    }
}
