﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SandMailManager : MonoBehaviour
{
    public static SandMailManager instance;

    [SerializeField] private GameObject mailListObj;
    [SerializeField] private GameObject mailContObj;
    [SerializeField] private GameObject openObj;

    [SerializeField] private GameObject mailPrefabs;
    [SerializeField] private Transform mailTransform;
    List<GameObject> mailList = new List<GameObject>();

    [Header("Content")]
    [SerializeField] private Text contText;
    [SerializeField] private Image gift_0;
    [SerializeField] private Image gift_1;

    [SerializeField] private string[] typeRan;
    public bool isOpen = false;
    private void Awake()
    {
        if (instance == null) instance = this;
    }
    private void Update()
    {
        if (isOpen && (Input.GetKeyDown(KeyCode.Escape) || PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_CANCLE], true)) && PDG.Dungeon.instance.closeCoolTime > 0.2f) 
        {
            PDG.Dungeon.instance.closeCoolTime = 0.0f;
            if (mailContObj.activeSelf)
            {
                mailListObj.SetActive(true);
                mailContObj.SetActive(false);
                ShowSandMail();
            }
            else
            {
                Cancle();
            }
        }
    }
    public void ShowSandMail()
    {
        isOpen = true;
        GetComponent<Image>().enabled = true;
        for (int i = 0; i < mailList.Count; i++)
        {
            GameObject obj = mailList[i];
            Destroy(obj);
        }

        mailListObj.SetActive(true);
        mailContObj.SetActive(false);
        
        string _date = DateTime.Now.ToString("yyyyMMdd");
        MailSaveData mData = SaveAndLoadManager.instance.LoadMail("mail_first");
        if (!mData.isCheck)
        {
            MailData mData_sheet = Array.Find(SheetManager.Instance.MailDB.dataArray, item => item.ID.Equals(mData._id));
            GameObject obj = Instantiate(mailPrefabs);
            obj.transform.Find("Cont").GetComponent<Text>().text = mData_sheet.Text;
            obj.transform.Find("From").GetComponent<Text>().text = mData_sheet.From;
            obj.GetComponent<Button>().onClick.AddListener(() => { SelectMail(mData_sheet, mData); });
            obj.transform.SetParent(mailTransform);
            obj.transform.localScale = Vector3.one;
            mailList.Add(obj);
        }
        MailSaveData mData2 = SaveAndLoadManager.instance.LoadMail("mail_loop_daily");
        if (!mData2.isCheck)
        {
            MailData mData_sheet = Array.Find(SheetManager.Instance.MailDB.dataArray, item => item.ID.Equals(mData._id));
            GameObject obj = Instantiate(mailPrefabs);
            obj.transform.Find("Cont").GetComponent<Text>().text = mData_sheet.Text;
            obj.transform.Find("From").GetComponent<Text>().text = mData_sheet.From;
            obj.GetComponent<Button>().onClick.AddListener(() => { SelectMail(mData_sheet, mData2); });
            obj.transform.SetParent(mailTransform);
            obj.transform.localScale = Vector3.one;
            mailList.Add(obj);
        }
        openObj.SetActive(true);
    }
    private void SelectMail(MailData mData_sheet, MailSaveData _sData)
    {
        _sData.isCheck = true;
        SaveAndLoadManager.instance.SaveMail(_sData);
        contText.text = "발송 : " + mData_sheet.From + "\n\n" + mData_sheet.Text;
        mailListObj.SetActive(false);
        mailContObj.SetActive(true);

        string g_1 = mData_sheet.Gift_1[UnityEngine.Random.Range(0, mData_sheet.Gift_1.Length)];
        string g_2 = mData_sheet.Gift_2[UnityEngine.Random.Range(0, mData_sheet.Gift_1.Length)];

        string type = typeRan[UnityEngine.Random.Range(0, typeRan.Length)];
        Sprite[] s = Resources.LoadAll<Sprite>("Item/Material/" + type);
        if (g_1.Equals("Material"))
        {
            gift_0.sprite = s[UnityEngine.Random.Range(0, s.Length)];
            Inventorys.Instance.AddItem(new ItemsData
            {
                Type1 = "Material",
                Type2 = type,
                ID = gift_0.sprite.name
            }, 1, true, false);
        }
        else
        {
            ItemsData[] iData = Array.FindAll(SheetManager.Instance.ItemsDB.dataArray, item => item.Type1.ToUpper().Equals(g_1.ToUpper()));
            if (iData.Length > 0)
            {
                int t = UnityEngine.Random.Range(0, iData.Length);
                gift_0.sprite = Resources.Load<Sprite>("Item/" + iData[t].Type1 + "/" + iData[t].Type2 + "/" + iData[t].ID);
                Inventorys.Instance.AddItem(iData[t], 1, true, false);
            }
        }

        if (g_2.Equals("Material"))
        {
            gift_1.sprite = s[UnityEngine.Random.Range(0, s.Length)];
            Inventorys.Instance.AddItem(new ItemsData
            {
                Type1 = "Material",
                Type2 = type,
                ID = gift_1.sprite.name
            }, 1, true, false);
        }
        else
        {
            ItemsData[] iData = Array.FindAll(SheetManager.Instance.ItemsDB.dataArray, item => item.Type1.ToUpper().Equals(g_1.ToUpper()));
            if (iData.Length > 0)
            {
                int t = UnityEngine.Random.Range(0, iData.Length);
                gift_1.sprite = Resources.Load<Sprite>("Item/" + iData[t].Type1 + "/" + iData[t].Type2 + "/" + iData[t].ID);
                Inventorys.Instance.AddItem(iData[t], 1, true, false);
            }
        }
    }
    public void Cancle()
    {
        isOpen = false;
        GetComponent<Image>().enabled = false;
        openObj.SetActive(false);
    }
}
