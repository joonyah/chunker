﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SafeController : MonoBehaviour
{
    [SerializeField] private Sprite fSprite;
    [SerializeField] private GameObject controllerObj;
    [SerializeField] private SpriteRenderer controllerSR;
    [SerializeField] private bool isSelect = false;
    [SerializeField] private Vector3 offSet;
    [SerializeField] private Vector3 size;

    [SerializeField] private GameObject tutorialPrefabs;
    private GameObject tutorialObj;
    // Start is called before the first frame update
    void Start()
    {
        int t2 = SaveAndLoadManager.instance.LoadItem("TUTORIAL_SAFE");
        if (t2 != 1)
        {
            tutorialObj = Instantiate(tutorialPrefabs);
            tutorialObj.transform.SetParent(GameObject.Find("Canvas").transform);
            tutorialObj.transform.localScale = Vector3.one;
            tutorialObj.transform.SetSiblingIndex(26);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (tutorialObj)
        {
            tutorialObj.transform.Find("Text").GetComponent<UnityEngine.UI.Text>().text = LocalizeManager.GetLocalize("tutorial_safe");
            Vector3 pos = Camera.main.WorldToScreenPoint(controllerObj.transform.position);
            pos.y += 15;
            tutorialObj.transform.position = pos;
        }

        RaycastHit2D hit = Physics2D.BoxCast(transform.position + offSet, size, 0, Vector2.zero, 0, 1 << 9);

        if (hit && !isSelect)
        {
            isSelect = true;
            controllerObj.SetActive(true);
            controllerObj.GetComponent<SpriteRenderer>().sprite = PDG.Dungeon.instance.isJoystrick ? PDG.Dungeon.GetJoyStrickKeySprite(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_ACTIVE]) : PDG.Dungeon.GetJoyStrickKeySprite(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ACTIVE]);
            TextOpenController.instance.ShowText(LocalizeManager.GetLocalize("ajit_safe"), Color.white, 24, controllerObj, 2.0f, true);
        }
        else if (!hit)
        {
            //GetComponent<Animator>().SetTrigger("close");
            //GetComponent<Animator>().SetBool("isOpen", false);
            isSelect = false;
            controllerObj.SetActive(false);
            if (TextOpenController.instance.targetObj == controllerObj) TextOpenController.instance.TextClose(Color.white);
        }

        if (isSelect)
        {
            if ((PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ACTIVE], true) || PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_ACTIVE], true)) && PDG.Dungeon.instance.closeCoolTime > 0.2f)
            {
                SaveAndLoadManager.instance.SaveItem(new ItemSave { _id = "TUTORIAL_SAFE", _nCount = 1 }, true);
                if (tutorialObj) Destroy(tutorialObj);
                PDG.Dungeon.instance.closeCoolTime = 0.0f;
                SoundManager.instance.StartAudio(new string[1] { "Click_Safe" }, VOLUME_TYPE.EFFECT);
                isSelect = false;
                controllerObj.SetActive(false);
                //GetComponent<Animator>().SetTrigger("open");
                //GetComponent<Animator>().SetBool("isOpen", true);
                SafeManager.instance.ShowMake();
            }
        }
    }
}
