﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SetSteamStat : MonoBehaviour
{
    [SerializeField] private Text targetText;
    [SerializeField] private string _statName;
    [SerializeField] private bool isTime = false;
    // Start is called before the first frame update
    void Start()
    {
        targetText = GetComponent<Text>();
    }
    public void InitSetting(string _statName2)
    {
        _statName = _statName2;
    }
    // Update is called once per frame
    void Update()
    {
        if (targetText == null) return;
        if (_statName.Equals(string.Empty)) return;
        targetText.text = "???";
        if (SteamStatsAndAchievements.instance == null) return;
        targetText.text = SteamStatsAndAchievements.instance.GettingStat(_statName).ToString();
    }
}
