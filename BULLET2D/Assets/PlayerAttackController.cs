﻿using PDG;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
[Serializable] public enum CollisionType { CIRCLE, BOX, CAPSULE, MAX };

public class PlayerAttackController : MonoBehaviour
{
    [SerializeField] private bool isFreeze;
    [SerializeField] private CollisionType type;
    [SerializeField] private Vector3 vSize;
    [SerializeField] private float fSize;
    [SerializeField] private Vector3 offSet;
    [SerializeField] private float fAngle;
    [SerializeField] CapsuleDirection2D capsuleType;
    public Vector3 vDir;
    [SerializeField] private float fDistance;
    [SerializeField] private LayerMask previewLayer;
    public float plusAngle;

    [SerializeField] private bool isDestroy;
    [SerializeField] private bool isInstance;
    [SerializeField] private GameObject DestroyObj;
    [SerializeField] private GameObject DestroyObj2;
    [SerializeField] private GameObject HitEffect;
    [SerializeField] private GameObject HitEffect2;
    [Header("광역")]
    [SerializeField] private bool isRangeDamage;
    [SerializeField] private float fDamageRange;
    [SerializeField] private Vector3 vDamageSize;
    public string weaponName = string.Empty;

    public Vector3 firePosition = Vector3.zero;
    public float destroyDistance = 10.0f;

    public int attackLevel = 0;

    public bool isAuto = false;
    private float gSpeed;

    [SerializeField] private bool isPublicList = false;

    [SerializeField] private List<GameObject> objList = new List<GameObject>();
    [SerializeField] private List<float> timeList = new List<float>();
    Coroutine tCoroutine = null;
    public float hitTime = 0.0f;
    [SerializeField] private Vector3 hitEffectSize = new Vector3(0.3f, 0.3f, 0.3f);
    public bool isRight = false;
    [SerializeField] private bool isFire = false;

    private int bounsCount = 0;
    float bounsDelayTime = 0.15f;

    void Start()
    {
        objList.Clear();
        timeList.Clear();
        StartCoroutine(UpdateCoroutine());
        StartCoroutine(Hoxy());
    }
    private void OnEnable()
    {
        bounsCount = 0;
        objList.Clear();
        StartCoroutine(UpdateCoroutine());
        if (gameObject.activeSelf) StartCoroutine(Hoxy());
    }
    void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireCube(transform.position + offSet, vSize);
        Gizmos.DrawWireSphere(transform.position, fSize);
    }
    IEnumerator Hoxy()
    {
        yield return new WaitForSeconds(5.0f);
        if (isDestroy)
        {
            if (DestroyObj)
            {
                GameObject obj = Instantiate(DestroyObj);
                obj.transform.position = transform.position;
                Destroy(obj, 2.0f);
            }
            if (DestroyObj2)
            {
                GameObject obj = Instantiate(DestroyObj2);
                obj.transform.position = transform.position;
                Destroy(obj, 2.0f);
            }
            if (!isInstance)
                Destroy(gameObject);
            else
            {
                tCoroutine = null;
                gameObject.SetActive(false);
            }
        }
    }
    IEnumerator UpdateCoroutine()
    {
        while (true)
        {
            if (Player.instance == null)
            {
                yield return new WaitForSeconds(Time.fixedDeltaTime);
                continue;
            }
            if (bounsDelayTime > 0)
            {
                bounsDelayTime -= Time.fixedDeltaTime;
                yield return new WaitForSeconds(Time.fixedDeltaTime);
                continue;
            }
            if (!isPublicList)
            {
                for (int i = 0; i < objList.Count; i++)
                {
                    if (objList[i] == null)
                    {
                        objList.RemoveAt(i);
                        timeList.RemoveAt(i);
                        i--;
                        continue;
                    }
                    else if (objList[i].GetComponent<MOB.MonsterArggro>())
                    {
                        if (objList[i].GetComponent<MOB.MonsterArggro>()._monster.isDie)
                        {
                            objList.RemoveAt(i);
                            timeList.RemoveAt(i);
                            i--;
                            continue;
                        }
                    }
                    else if (objList[i].GetComponent<MOB.Monster>())
                    {
                        if (objList[i].GetComponent<MOB.Monster>().isDie)
                        {
                            objList.RemoveAt(i);
                            timeList.RemoveAt(i);
                            i--;
                            continue;
                        }
                    }
                    timeList[i] += Time.fixedDeltaTime;
                }
            }

            float damage = 0;
            if (isRight)
                damage = Player.instance.isRightNear ? Player.instance.damageRightNear : Player.instance.damageRight;
            else
                damage = gameObject.name.Contains("multiple_bombing") ? (Player.instance.isQNear ? Player.instance.damageQNear : Player.instance.damageQ) : (Player.instance.nowWeaponName.Contains("electric_cellstem") ? Player.instance.damage : (Player.instance.isLeftNear ? Player.instance.damageNear : Player.instance.damage));
            bool isCritical = false;
            if (isRight)
                isCritical = Player.instance.isCriticalRight;
            else
                isCritical = gameObject.name.Contains("multiple_bombing") ? Player.instance.isCriticalQ : Player.instance.isCritical;
            bool isNear = false;
            if (isRight)
                isNear = Player.instance.isRightNear;
            else
                isNear = gameObject.name.Contains("multiple_bombing") ? Player.instance.isQNear : (Player.instance.nowWeaponName.Contains("electric_cellstem") ? false : Player.instance.isLeftNear);



            if (!weaponName.Equals(string.Empty) && weaponName.Equals("Ice_spear"))
            {
                float z = transform.eulerAngles.z;
                if (z > 90.0f && z < 270.0f)
                    transform.localScale = new Vector3(1, -1, 1);
                else
                    transform.localScale = new Vector3(1, 1, 1);
            }
            if (isAuto)
            {
                RaycastHit2D hitAuto = Physics2D.CircleCast(transform.position, 4, Vector2.zero, 0, previewLayer);
                if (hitAuto)
                {
                    Vector3 velocity = GetComponent<Rigidbody2D>().velocity;
                    Vector3 dir = (Vector3)hitAuto.point - transform.position;
                    dir.Normalize();
                    velocity += dir;
                    velocity.Normalize();
                    GetComponent<Rigidbody2D>().velocity = velocity * gSpeed;
                }
            }
            if (isDestroy)
            {
                if (Vector3.Distance(firePosition, transform.position) > destroyDistance)
                {
                    if (DestroyObj)
                    {
                        GameObject obj = Instantiate(DestroyObj);
                        obj.transform.position = transform.position;
                        Destroy(obj, 2.0f);
                    }
                    if (DestroyObj2)
                    {
                        GameObject obj = Instantiate(DestroyObj2);
                        obj.transform.position = transform.position;
                        Destroy(obj, 2.0f);
                    }
                    if (!isInstance) Destroy(gameObject);
                    else
                    {
                        tCoroutine = null;
                        gameObject.SetActive(false);
                        break;
                    }
                }
            }
            RaycastHit2D hit = new RaycastHit2D();
            RaycastHit2D[] hits = new RaycastHit2D[0];
            if (type == CollisionType.BOX) hit = Physics2D.BoxCast(transform.position + offSet, vSize, fAngle + plusAngle, vDir, fDistance, previewLayer | 1 << 23);
            else if (type == CollisionType.CAPSULE) hit = Physics2D.CapsuleCast(transform.position + offSet, vSize, capsuleType, fAngle + plusAngle, vDir, fDistance, previewLayer | 1 << 23);
            else if (type == CollisionType.CIRCLE) hit = Physics2D.CircleCast(transform.position + offSet, fSize, vDir, fDistance, previewLayer | 1 << 23);

            if (hit)
            {
                if (!isRangeDamage)
                {
                    GameObject obj33 = hit.collider.gameObject;
                    int index = -1;
                    if (isPublicList) index = Dungeon.instance.publicListObj.FindIndex(item => item.Equals(obj33));
                    else index = objList.FindIndex(item => item.Equals(obj33));
                    if (index != -1)
                    {
                        if (isPublicList)
                        {
                            if (hitTime > Dungeon.instance.publicListTime[index]) continue;
                            else Dungeon.instance.publicListTime[index] = 0.0f;
                        }
                        else
                        {
                            if (hitTime > timeList[index]) continue;
                            else timeList[index] = 0.0f;
                        }
                    }
                    else
                    {
                        if (isPublicList)
                        {
                            Dungeon.instance.publicListObj.Add(obj33);
                            Dungeon.instance.publicListTime.Add(0.0f);
                        }
                        else
                        {
                            objList.Add(obj33);
                            timeList.Add(0.0f);
                        }
                    }

                    if (hit.collider.gameObject.layer.Equals(24))
                    {
                        if (hit.collider.GetComponent<MOB.BossMonster_Six_Apear>()) hit.collider.GetComponent<MOB.BossMonster_Six_Apear>().SetDamage(damage + (attackLevel * 4));
                        if (hit.collider.GetComponent<Boss_STG>()) hit.collider.GetComponent<Boss_STG>().SetDamage(damage + (attackLevel * 4), hit.point);
                        if (hit.collider.GetComponent<Boss_NPC>()) hit.collider.GetComponent<Boss_NPC>().SetDamage(damage + (attackLevel * 4), hit.point);
                        if (hit.collider.GetComponent<Boss_Golem>()) hit.collider.GetComponent<Boss_Golem>().SetDamage(damage + (attackLevel * 4), hit.point);
                        if (hit.collider.GetComponent<Boss_Stone>()) hit.collider.GetComponent<Boss_Stone>().SetDamage(damage + (attackLevel * 4), hit.point, gameObject.name.Contains("multiple_bombing") ? true : false);
                        if (Player.instance.isStrategic_02) Player.instance.isStrategic_02 = false;

                        if (HitEffect)
                        {
                            GameObject obj = Instantiate(HitEffect);
                            obj.transform.position = hit.point;
                            obj.transform.localScale = hitEffectSize;
                            Destroy(obj, 2f);
                        }
                        if (HitEffect2)
                        {
                            GameObject obj = Instantiate(HitEffect2);
                            obj.transform.position = hit.point;
                            obj.transform.localScale = hitEffectSize;
                            Destroy(obj, 2f);
                        }
                        if (isNear && isCritical)
                        {
                            GameObject criEffect = ObjManager.Call().GetObject("CriticalHitEffect");
                            criEffect.transform.position = hit.point;
                            criEffect.SetActive(true);
                        }
                    }
                    else if (hit.collider.gameObject.layer.Equals(23))
                    {
                        if (hit.collider.transform.parent && hit.collider.transform.parent.GetComponent<TentacleController>())
                        {
                            hit.collider.transform.parent.GetComponent<TentacleController>().SetDamage(1);
                            if (HitEffect)
                            {
                                GameObject obj = Instantiate(HitEffect);
                                obj.transform.position = hit.point;
                                obj.transform.localScale = hitEffectSize;
                                Destroy(obj, 2f);
                            }
                            if (HitEffect2)
                            {
                                GameObject obj = Instantiate(HitEffect2);
                                obj.transform.position = hit.point;
                                obj.transform.localScale = hitEffectSize;
                                Destroy(obj, 2f);
                            }
                        }
                        Vector3 dir = hit.collider.gameObject.transform.position - PDG.Player.instance.transform.position;
                        dir.Normalize();
                        if (hit.collider.gameObject.GetComponent<DestroyObjects>())
                        {
                            hit.collider.gameObject.GetComponent<DestroyObjects>().DestroyObject(dir);
                            if (HitEffect)
                            {
                                GameObject obj = Instantiate(HitEffect);
                                obj.transform.position = hit.point;
                                obj.transform.localScale = hitEffectSize;
                                Destroy(obj, 2f);
                            }
                            if (HitEffect2)
                            {
                                GameObject obj = Instantiate(HitEffect2);
                                obj.transform.position = hit.point;
                                obj.transform.localScale = hitEffectSize;
                                Destroy(obj, 2f);
                            }
                        }
                        if (hit.collider.gameObject.GetComponent<ChestHit>() && !hit.collider.gameObject.GetComponent<ChestHit>().isOpenBefore)
                        {
                            hit.collider.gameObject.GetComponent<ChestHit>().SetDamage(1);
                            if (HitEffect)
                            {
                                GameObject obj = Instantiate(HitEffect);
                                obj.transform.position = hit.point;
                                obj.transform.localScale = hitEffectSize;
                                Destroy(obj, 2f);
                            }
                            if (HitEffect2)
                            {
                                GameObject obj = Instantiate(HitEffect2);
                                obj.transform.position = hit.point;
                                obj.transform.localScale = hitEffectSize;
                                Destroy(obj, 2f);
                            }
                        }
                        if (hit.collider.gameObject.GetComponent<DrumHit>())
                        {
                            hit.collider.gameObject.GetComponent<DrumHit>().SetDamage(1);
                            if (HitEffect)
                            {
                                GameObject obj = Instantiate(HitEffect);
                                obj.transform.position = hit.point;
                                obj.transform.localScale = hitEffectSize;
                                Destroy(obj, 2f);
                            }
                            if (HitEffect2)
                            {
                                GameObject obj = Instantiate(HitEffect2);
                                obj.transform.position = hit.point;
                                obj.transform.localScale = hitEffectSize;
                                Destroy(obj, 2f);
                            }
                        }
                    }
                    else if (hit.collider.gameObject.layer.Equals(8) && !weaponName.Equals(string.Empty) && weaponName.Equals("Ice_spear"))
                    {
                        for (int i = 0; i < 7; i++)
                        {
                            GameObject obj = ObjManager.Call().GetObject("IceSpearFragments2");
                            obj.GetComponent<PlayerAttackController>().firePosition = transform.position;
                            obj.GetComponent<PlayerAttackController>().destroyDistance = destroyDistance;
                            obj.SetActive(true);
                            float z = transform.eulerAngles.z;
                            z += 180.0f;
                            z -= (180 / 7) * Mathf.FloorToInt(7 / 2);
                            z += (i * (180 / 7));
                            Vector3 pos = PDG.Player.GetPosition(transform.position, z);
                            Vector3 dir = pos - transform.position;
                            dir.Normalize();
                            obj.transform.position = transform.position + dir;
                            obj.GetComponent<PlayerAttackController>().RigidBodyFire(dir, gSpeed);
                        }
                        if (HitEffect)
                        {
                            GameObject obj = Instantiate(HitEffect);
                            obj.transform.position = hit.point;
                            obj.transform.localScale = hitEffectSize;
                            Destroy(obj, 2f);
                        }
                        if (HitEffect2)
                        {
                            GameObject obj = Instantiate(HitEffect2);
                            obj.transform.position = hit.point;
                            obj.transform.localScale = hitEffectSize;
                            Destroy(obj, 2f);
                        }
                    }
                    else
                    {
                        Vector3 dir = hit.collider.transform.position - transform.position;
                        dir.Normalize();

                        if (hit.collider.GetComponent<MOB.Monster>())
                        {
                            if (!weaponName.Equals(string.Empty) && weaponName.Equals("Ice_spear"))
                            {
                                hit.collider.GetComponent<MOB.Monster>().SetDamage(damage + (attackLevel * 4), -dir, 1, ELEMENTAL_TYPE.ICE, 1.5f);
                            }
                            else
                            {
                                if (isFire)
                                {
                                    hit.collider.GetComponent<MOB.Monster>().fDotTime = 5.0f;
                                    hit.collider.GetComponent<MOB.Monster>().nowElementalType = ELEMENTAL_TYPE.FIRE;
                                    hit.collider.GetComponent<MOB.Monster>().elementalName = PDG.Player.instance.nowRightWeaponName;
                                }
                                hit.collider.GetComponent<MOB.Monster>().SetDamage(damage + (attackLevel * 4), -dir, 1, isFreeze ? ELEMENTAL_TYPE.FREEZE : ELEMENTAL_TYPE.NONE, 6.0f);
                            }
                            if (HitEffect)
                            {
                                GameObject obj = Instantiate(HitEffect);
                                obj.transform.position = hit.point;
                                obj.transform.localScale = hitEffectSize;
                                Destroy(obj, 2f);
                            }
                            if (HitEffect2)
                            {
                                GameObject obj = Instantiate(HitEffect2);
                                obj.transform.position = hit.point;
                                obj.transform.localScale = hitEffectSize;
                                Destroy(obj, 2f);
                            }
                            if (isNear && isCritical)
                            {
                                GameObject criEffect = ObjManager.Call().GetObject("CriticalHitEffect");
                                criEffect.transform.position = hit.point;
                                criEffect.SetActive(true);
                            }
                        }
                    }
                }
                else
                {
                    if (type == CollisionType.BOX) hits = Physics2D.BoxCastAll(transform.position + offSet, vDamageSize, fAngle + plusAngle, vDir, fDistance, previewLayer | 1 << 23);
                    else if (type == CollisionType.CAPSULE) hits = Physics2D.CapsuleCastAll(transform.position + offSet, vDamageSize, capsuleType, fAngle + plusAngle, vDir, fDistance, previewLayer | 1 << 23);
                    else if (type == CollisionType.CIRCLE) hits = Physics2D.CircleCastAll(transform.position + offSet, fDamageRange, vDir, fDistance, previewLayer | 1 << 23);
                    if (hits.Length > 0)
                    {
                        for (int i = 0; i < hits.Length; i++)
                        {
                            GameObject obj2 = hits[i].collider.gameObject;
                            int index = -1;
                            if (isPublicList) index = Dungeon.instance.publicListObj.FindIndex(item => item.Equals(obj2));
                            else index = objList.FindIndex(item => item.Equals(obj2));
                            if (index != -1)
                            {
                                if (isPublicList)
                                {
                                    if (hitTime > Dungeon.instance.publicListTime[index]) continue;
                                    else Dungeon.instance.publicListTime[index] = 0.0f;
                                }
                                else
                                {
                                    if (hitTime > timeList[index]) continue;
                                    else timeList[index] = 0.0f;
                                }
                            }
                            else
                            {
                                if (isPublicList)
                                {
                                    Dungeon.instance.publicListObj.Add(obj2);
                                    Dungeon.instance.publicListTime.Add(0.0f);
                                }
                                else
                                {
                                    objList.Add(obj2);
                                    timeList.Add(0.0f);
                                }
                            }


                            if (hits[i].collider.gameObject.layer.Equals(24))
                            {
                                if (hits[i].collider.GetComponent<MOB.BossMonster_Six_Apear>()) hits[i].collider.GetComponent<MOB.BossMonster_Six_Apear>().SetDamage(damage + (attackLevel * 4));
                                if (hits[i].collider.GetComponent<Boss_STG>()) hits[i].collider.GetComponent<Boss_STG>().SetDamage(damage + (attackLevel * 4), hits[i].point);
                                if (hits[i].collider.GetComponent<Boss_NPC>()) hits[i].collider.GetComponent<Boss_NPC>().SetDamage(damage + (attackLevel * 4), hits[i].point);
                                if (hits[i].collider.GetComponent<Boss_Golem>()) hits[i].collider.GetComponent<Boss_Golem>().SetDamage(damage + (attackLevel * 4), hits[i].point);
                                if (hits[i].collider.GetComponent<Boss_Stone>()) hits[i].collider.GetComponent<Boss_Stone>().SetDamage(damage + (attackLevel * 4), hits[i].point, gameObject.name.Contains("multiple_bombing") ? true : false);
                                if (Player.instance.isStrategic_02) Player.instance.isStrategic_02 = false;
                                if (HitEffect)
                                {
                                    GameObject obj = Instantiate(HitEffect);
                                    obj.transform.position = hits[i].point;
                                    obj.transform.localScale = hitEffectSize;
                                    Destroy(obj, 2f);
                                }
                                if (HitEffect2)
                                {
                                    GameObject obj = Instantiate(HitEffect2);
                                    obj.transform.position = hit.point;
                                    obj.transform.localScale = hitEffectSize;
                                    Destroy(obj, 2f);
                                }
                                if (isNear && isCritical)
                                {
                                    GameObject criEffect = ObjManager.Call().GetObject("CriticalHitEffect");
                                    criEffect.transform.position = hits[i].point;
                                    criEffect.SetActive(true);
                                }
                            }
                            else if (hits[i].collider.gameObject.layer.Equals(23))
                            {
                                if (hits[i].collider.transform.parent && hits[i].collider.transform.parent.GetComponent<TentacleController>())
                                {
                                    hits[i].collider.transform.parent.GetComponent<TentacleController>().SetDamage(1);
                                    if (HitEffect)
                                    {
                                        GameObject obj = Instantiate(HitEffect);
                                        obj.transform.position = hits[i].point;
                                        obj.transform.localScale = hitEffectSize;
                                        Destroy(obj, 2f);
                                    }
                                    if (HitEffect2)
                                    {
                                        GameObject obj = Instantiate(HitEffect2);
                                        obj.transform.position = hit.point;
                                        obj.transform.localScale = hitEffectSize;
                                        Destroy(obj, 2f);
                                    }
                                }
                                Vector3 dir = hits[i].collider.gameObject.transform.position - PDG.Player.instance.transform.position;
                                dir.Normalize();
                                if (hits[i].collider.gameObject.GetComponent<DestroyObjects>())
                                {
                                    hits[i].collider.gameObject.GetComponent<DestroyObjects>().DestroyObject(dir);
                                    if (HitEffect)
                                    {
                                        GameObject obj = Instantiate(HitEffect);
                                        obj.transform.position = hits[i].point;
                                        obj.transform.localScale = hitEffectSize;
                                        Destroy(obj, 2f);
                                    }
                                    if (HitEffect2)
                                    {
                                        GameObject obj = Instantiate(HitEffect2);
                                        obj.transform.position = hit.point;
                                        obj.transform.localScale = hitEffectSize;
                                        Destroy(obj, 2f);
                                    }
                                }
                                if (hits[i].collider.gameObject.GetComponent<ChestHit>() && !hits[i].collider.gameObject.GetComponent<ChestHit>().isOpenBefore)
                                {
                                    hits[i].collider.gameObject.GetComponent<ChestHit>().SetDamage(1);
                                    if (HitEffect)
                                    {
                                        GameObject obj = Instantiate(HitEffect);
                                        obj.transform.position = hits[i].point;
                                        obj.transform.localScale = hitEffectSize;
                                        Destroy(obj, 2f);
                                    }
                                    if (HitEffect2)
                                    {
                                        GameObject obj = Instantiate(HitEffect2);
                                        obj.transform.position = hit.point;
                                        obj.transform.localScale = hitEffectSize;
                                        Destroy(obj, 2f);
                                    }
                                }
                                if (hits[i].collider.gameObject.GetComponent<DrumHit>())
                                {
                                    hits[i].collider.gameObject.GetComponent<DrumHit>().SetDamage(1);
                                    if (HitEffect)
                                    {
                                        GameObject obj = Instantiate(HitEffect);
                                        obj.transform.position = hits[i].point;
                                        obj.transform.localScale = hitEffectSize;
                                        Destroy(obj, 2f);
                                    }
                                    if (HitEffect2)
                                    {
                                        GameObject obj = Instantiate(HitEffect2);
                                        obj.transform.position = hit.point;
                                        obj.transform.localScale = hitEffectSize;
                                        Destroy(obj, 2f);
                                    }
                                }
                            }
                            else
                            {
                                Vector3 dir = hits[i].collider.transform.position - transform.position;
                                dir.Normalize();

                                if (hits[i].collider.GetComponent<MOB.Monster>())
                                {
                                    if (isFire)
                                    {
                                        hits[i].collider.GetComponent<MOB.Monster>().fDotTime = 5.0f;
                                        hits[i].collider.GetComponent<MOB.Monster>().nowElementalType = ELEMENTAL_TYPE.FIRE;
                                        hits[i].collider.GetComponent<MOB.Monster>().elementalName = PDG.Player.instance.nowRightWeaponName;
                                    }
                                    hits[i].collider.GetComponent<MOB.Monster>().SetDamage(damage + (attackLevel * 4), -dir, 1, isFreeze ? ELEMENTAL_TYPE.FREEZE : ELEMENTAL_TYPE.NONE, 6.0f);
                                    if (HitEffect)
                                    {
                                        GameObject obj = Instantiate(HitEffect);
                                        obj.transform.position = hits[i].point;
                                        obj.transform.localScale = hitEffectSize;
                                        Destroy(obj, 2f);
                                    }
                                    if (HitEffect2)
                                    {
                                        GameObject obj = Instantiate(HitEffect2);
                                        obj.transform.position = hit.point;
                                        obj.transform.localScale = hitEffectSize;
                                        Destroy(obj, 2f);
                                    }
                                    if (isNear && isCritical)
                                    {
                                        GameObject criEffect = ObjManager.Call().GetObject("CriticalHitEffect");
                                        criEffect.transform.position = hits[i].point;
                                        criEffect.SetActive(true);
                                    }
                                }
                            }

                        }
                    }
                }
                if (Player.instance.isBulletBounce && (weaponName.Contains("biological_cannon") || weaponName.Contains("volatile") || weaponName.Contains("rocket_cannon")))
                {
                    if (DestroyObj)
                    {
                        GameObject dObj = Instantiate(DestroyObj, null);
                        dObj.transform.position = transform.position;
                        Destroy(dObj, 2);
                    }
                    if (DestroyObj2)
                    {
                        GameObject obj = Instantiate(DestroyObj2);
                        obj.transform.position = transform.position;
                        Destroy(obj, 2.0f);
                    }

                    if (bounsCount >= PDG.Player.instance.BounsCount)
                    {
                        if (isDestroy)
                        {
                            if (!isInstance) Destroy(gameObject);
                            else
                            {
                                tCoroutine = null;
                                gameObject.SetActive(false);
                                break;
                            }
                        }
                    }
                    bounsDelayTime = 0.15f;
                    RaycastHit2D hitt = hit;
                    if (hits.Length > 0 && isRangeDamage)
                        hitt = hits[0];

                    Vector3 dir = hitt.collider.transform.position - transform.position;
                    dir.Normalize();
                    float angle = GetReflectAngle(hitt.point, hitt.collider.transform.position, dir);
                    GetComponent<Rigidbody2D>().velocity = Vector2.zero;
                    dir = GetPosition(transform.position, angle) - transform.position;
                    dir.Normalize();
                    GetComponent<Rigidbody2D>().AddForce(dir * gSpeed, ForceMode2D.Impulse);
                    transform.localEulerAngles = new Vector3(0, 0, angle);
                    bounsCount++;
                }
                else
                {
                    if (DestroyObj)
                    {
                        GameObject dObj = Instantiate(DestroyObj, null);
                        dObj.transform.position = transform.position;
                        Destroy(dObj, 2);
                    }
                    if (DestroyObj2)
                    {
                        GameObject obj = Instantiate(DestroyObj2);
                        obj.transform.position = transform.position;
                        Destroy(obj, 2.0f);
                    }
                    if (isDestroy)
                    {
                        if (!isInstance) Destroy(gameObject);
                        else
                        {
                            tCoroutine = null;
                            gameObject.SetActive(false);
                            break;
                        }
                    }
                }

            }
            yield return new WaitForSeconds(Time.fixedDeltaTime);
        }
    }
    public void RigidBodyFire(Vector3 _dir, float _pow)
    {
        gSpeed = _pow;
        GetComponent<Rigidbody2D>().AddForce(_dir * _pow, ForceMode2D.Impulse);
    }
    #region -- Tool --
    int GetDirectionAngle(Collider2D collision)
    {
        float angle = GetAngle(collision.transform.position, transform.position);
        while (angle > 360.0f) angle -= 360.0f;
        while (angle < 0) angle += 360.0f;

        if (angle >= 45.0f && angle < 135.0f)
        {
            // 위쪽
            return 270;
        }
        else if (angle >= 135.0f && angle < 225.0f)
        {
            // 왼쪽
            return 0;
        }
        else if (angle > 225.0f && angle < 315.0f)
        {
            //아래쪽
            return 90;
        }
        else
        {
            //오른쪽
            return 180;
        }
    }
    float GetAngle(Vector3 vStart, Vector3 vEnd)
    {
        Vector3 v = vEnd - vStart;

        return Mathf.Atan2(v.y, v.x) * Mathf.Rad2Deg;
    }
    Vector3 GetPosition(Vector3 vStart, float angle)
    {
        float x = Mathf.Cos(Mathf.Deg2Rad * angle) + vStart.x;
        float y = Mathf.Sin(Mathf.Deg2Rad * angle) + vStart.y;

        return new Vector3(x, y, 0);
    }
    float GetReflectAngle(Vector3 _hitPoint, Vector3 _hitPosition, Vector3 _dir)
    {
        Vector3 _dir2 = _hitPoint - _hitPosition;
        _dir2.Normalize();

        if (_dir.x == 0 && _dir.y > 0)       // 위 
        {
            return 270.0f;
        }
        else if (_dir.x == 0 && _dir.y < 0)  // 아래
        {
            return 90.0f;
        }
        else if (_dir.x > 0 && _dir.y == 0)  // 오른쪽
        {
            return 180.0f;
        }
        else if (_dir.x < 0 && _dir.y == 0)  // 왼쪽
        {
            return 0.0f;
        }
        else if (_dir.x > 0 && _dir.y > 0)  // 오른쪽 위 
        {
            if (_dir2.x > 0)
            {
                float zz = GetAngle(_hitPoint, transform.position);
                float f = zz - 180.0f; // 입사각
                return 360.0f - f; // 반사각
            }
            else if (_dir2.y > 0)
            {
                float zz = GetAngle(_hitPoint, transform.position);
                float f = 270.0f - zz; // 입사각
                return 90.0f + f; // 반사각
            }
            else
            {
                float hitAngle = GetAngle(_hitPosition, _hitPoint);
                if (hitAngle > 225.0f)
                {
                    float zz = GetAngle(_hitPoint, transform.position);
                    float f = zz - 180.0f; // 입사각
                    return 360.0f - f; // 반사각
                }
                else if (hitAngle < 225.0f)
                {
                    float zz = GetAngle(_hitPoint, transform.position);
                    float f = 270.0f - zz; // 입사각
                    return 90.0f + f; // 반사각
                }
                else
                {
                    return 225.0f;
                }

            }
        }
        else if (_dir.x < 0 && _dir.y > 0)   // 왼쪽 위
        {
            if (_dir2.x < 0)
            {
                float zz = GetAngle(_hitPoint, transform.position);
                float f = 360.0f - zz; // 입사각
                return 180.0f + f; // 반사각
            }
            else if (_dir2.y > 0)
            {
                float zz = GetAngle(_hitPoint, transform.position);
                float f = zz - 270.0f; // 입사각
                return f; // 반사각
            }
            else
            {
                float hitAngle = GetAngle(_hitPosition, _hitPoint);
                if (hitAngle > 315.0f)
                {
                    float zz = GetAngle(_hitPoint, transform.position);
                    float f = zz - 270.0f; // 입사각
                    return f; // 반사각
                }
                else if (hitAngle < 315.0f)
                {
                    float zz = GetAngle(_hitPoint, transform.position);
                    float f = 360.0f - zz; // 입사각
                    return 180.0f + f; // 반사각
                }
                else
                {
                    return 315.0f;
                }

            }
        }
        else if (_dir.x < 0 && _dir.y < 0)   // 왼쪽 아래
        {
            if (_dir2.x < 0)
            {
                float zz = GetAngle(_hitPoint, transform.position);
                float f = zz; // 입사각
                return 180.0f - f; // 반사각
            }
            else if (_dir2.y < 0)
            {
                float zz = GetAngle(_hitPoint, transform.position);
                float f = 90.0f - zz; // 입사각
                return 270.0f + f; // 반사각
            }
            else
            {
                float hitAngle = GetAngle(_hitPosition, _hitPoint);
                if (hitAngle > 45.0f)
                {
                    float zz = GetAngle(_hitPoint, transform.position);
                    float f = zz; // 입사각
                    return 180.0f - f; // 반사각
                }
                else if (hitAngle < 45.0f)
                {
                    float zz = GetAngle(_hitPoint, transform.position);
                    float f = 90.0f - zz; // 입사각
                    return 270.0f + f; // 반사각
                }
                else
                {
                    return 45.0f;
                }

            }
        }
        else if (_dir.x > 0 && _dir.y < 0)   // 오른쪽 아래
        {
            if (_dir2.x > 0)
            {
                float zz = GetAngle(_hitPoint, transform.position);
                float f = 180.0f - zz; // 입사각
                return f; // 반사각
            }
            else if (_dir2.y < 0)
            {
                float zz = GetAngle(_hitPoint, transform.position);
                float f = zz - 90.0f; // 입사각
                return 270.0f - f; // 반사각
            }
            else
            {
                float hitAngle = GetAngle(_hitPosition, _hitPoint);
                if (hitAngle > 135.0f)
                {
                    float zz = GetAngle(_hitPoint, transform.position);
                    float f = zz - 90.0f; // 입사각
                    return 270.0f - f; // 반사각
                }
                else if (hitAngle < 135.0f)
                {
                    float zz = GetAngle(_hitPoint, transform.position);
                    float f = 180.0f - zz; // 입사각
                    return f; // 반사각
                }
                else
                {
                    return 135.0f;
                }
            }
        }
        else
        {
            return 0.0f;
        }
    }
    #endregion
}
