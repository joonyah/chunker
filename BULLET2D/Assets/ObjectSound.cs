﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectSound : MonoBehaviour
{
    [SerializeField] private bool isEffect;
    [SerializeField] private float disatance = 5;
    private AudioSource[] audio;
    [SerializeField] private bool isVolumeDown = true;
    [SerializeField] private float downValue = 1.0f;
    [SerializeField] private float defaultDistance = 0;
    // Start is called before the first frame update
    void Awake()
    {
        audio = GetComponents<AudioSource>();
        for (int i = 0; i < audio.Length; i++) audio[i].volume = 0;
    }
    private void Start()
    {
        if (PDG.Player.instance == null) return;
        if (PDG.Player.instance.isDie)
        {
            for (int i = 0; i < audio.Length; i++)
            {
                audio[i].volume = 0;
            }
            return;
        }
        float dis = Vector3.Distance(PDG.Player.instance.transform.position, transform.position);
        dis -= defaultDistance;
        for (int i = 0; i < audio.Length; i++)
        {
            if (dis > disatance) audio[i].volume = 0;
            if ((SoundManager.isMute || (PDG.Dungeon.instance == null ? false : PDG.Dungeon.instance.isPause) || (PDG.Player.instance == null ? false : PDG.Player.instance.isDie)))
            {
                audio[i].volume = 0;
            }
            else
            {
                float tVolume = isEffect ? SoundManager.EffectVolume : SoundManager.AmbientVolume;
                float t = tVolume / disatance;
                audio[i].volume = isVolumeDown ? (tVolume - (dis * t)) / downValue : tVolume - (dis * t);
            }
        }
        for (int i = 0; i < audio.Length; i++)
        {
            if (audio[i].playOnAwake) audio[i].Play();
        }
    }
    // Update is called once per frame
    void Update()
    {
        if (PDG.Player.instance == null) return;
        if (PDG.Player.instance.isDie)
        {
            for (int i = 0; i < audio.Length; i++)
            {
                audio[i].volume = 0;
            }
            return;
        }
        float dis = Vector3.Distance(PDG.Player.instance.transform.position, transform.position);
        dis -= defaultDistance;
        for (int i = 0; i < audio.Length; i++)
        {
            if (dis > disatance) audio[i].volume = 0;
            if ( (SoundManager.isMute || (PDG.Dungeon.instance == null ? false : PDG.Dungeon.instance.isPause) || (PDG.Player.instance == null ? false : PDG.Player.instance.isDie)))
            {
                audio[i].volume = 0;
            }
            else
            {
                float tVolume = isEffect ? SoundManager.EffectVolume : SoundManager.AmbientVolume;
                float t = tVolume / disatance;
                audio[i].volume = isVolumeDown ? (tVolume - (dis * t)) / downValue : tVolume - (dis * t);
            }
        }
    }
}
