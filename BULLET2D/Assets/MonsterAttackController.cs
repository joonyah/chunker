﻿using PDG;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonsterAttackController : MonoBehaviour
{
    [SerializeField] private bool isFreeze;
    [SerializeField] private CollisionType type;
    [SerializeField] private Vector3 vSize;
    [SerializeField] private float fSize;
    [SerializeField] private Vector3 offSet;
    [SerializeField] private float fAngle;
    [SerializeField] CapsuleDirection2D capsuleType;
    public Vector3 vDir;
    [SerializeField] private float fDistance;
    [SerializeField] private LayerMask previewLayer;
    public float plusAngle;

    [SerializeField] private bool isDestroy;
    [SerializeField] private bool isInstance;
    [SerializeField] private GameObject DestroyObj;
    [SerializeField] private GameObject DestroyObj2;
    [Header("광역")]
    [SerializeField] private bool isRangeDamage;
    [SerializeField] private float fDamageRange;
    [SerializeField] private Vector3 vDamageSize;

    public Vector3 firePosition = Vector3.zero;
    public float destroyDistance = 10.0f;
    public bool isAuto = false;
    private float gSpeed;
    public string whyDie;
    void Start()
    {
        StartCoroutine(UpdateCoroutine());
    }
    private void OnEnable()
    {
        StartCoroutine(UpdateCoroutine());
    }

    IEnumerator UpdateCoroutine()
    {
        while (true)
        {
            if (isAuto)
            {
                RaycastHit2D hitAuto = Physics2D.CircleCast(transform.position, 4, Vector2.zero, 0, previewLayer);
                if (hitAuto)
                {
                    Vector3 velocity = GetComponent<Rigidbody2D>().velocity;
                    Vector3 dir = (Vector3)hitAuto.point - transform.position;
                    dir.Normalize();
                    velocity += dir;
                    velocity.Normalize();
                    GetComponent<Rigidbody2D>().velocity = velocity * gSpeed;
                }
            }
            if (isDestroy)
            {
                if (Vector3.Distance(firePosition, transform.position) > destroyDistance)
                {
                    if (DestroyObj)
                    {
                        GameObject obj = Instantiate(DestroyObj);
                        obj.transform.position = transform.position;
                        Destroy(obj, 2.0f);
                    }
                    if (DestroyObj2)
                    {
                        GameObject obj = Instantiate(DestroyObj2);
                        obj.transform.position = transform.position;
                        Destroy(obj, 2.0f);
                    }
                    if (!isInstance) Destroy(gameObject);
                    else
                    {
                        gameObject.SetActive(false);
                        break;
                    }
                }
            }
            RaycastHit2D hit = new RaycastHit2D();
            if (type == CollisionType.BOX) hit = Physics2D.BoxCast(transform.position + offSet, vSize, fAngle + plusAngle, vDir, fDistance, previewLayer);
            else if (type == CollisionType.CAPSULE) hit = Physics2D.CapsuleCast(transform.position + offSet, vSize, capsuleType, fAngle + plusAngle, vDir, fDistance, previewLayer);
            else if (type == CollisionType.CIRCLE) hit = Physics2D.CircleCast(transform.position + offSet, fSize, vDir, fDistance, previewLayer);

            if (hit)
            {

                if (hit.collider.gameObject.layer.Equals(27))
                {
                    if (hit.collider.name.Equals("Shield"))
                    {
                        PDG.Player.instance.right_defence_gauge -= 1.0f;
                        Inventorys.Instance.GuageShaker(1);
                        CameraShaker._instance.StartShake(0.2f, 0.02f, 0.2f);
                    }
                    if (hit.collider.name.Contains("dna_mo_blade"))
                    {
                        PDG.Player.instance.left_defence_gauge -= 1.0f;
                        Inventorys.Instance.GuageShaker(0);
                        CameraShaker._instance.StartShake(0.2f, 0.02f, 0.2f);
                    }

                    if (DestroyObj)
                    {
                        GameObject dObj = Instantiate(DestroyObj, null);
                        dObj.transform.position = transform.position;
                        Destroy(dObj, 2);
                    }
                    if (DestroyObj2)
                    {
                        GameObject obj = Instantiate(DestroyObj2);
                        obj.transform.position = transform.position;
                        Destroy(obj, 2.0f);
                    }
                    if (isDestroy)
                    {
                        if (!isInstance) Destroy(gameObject);
                        else
                        {
                            gameObject.SetActive(false);
                            break;
                        }
                    }
                }


                if (!isRangeDamage)
                { if (hit.collider.gameObject.layer.Equals(23))
                    {
                        if (hit.collider.transform.parent && hit.collider.transform.parent.GetComponent<TentacleController>())
                        {
                            hit.collider.transform.parent.GetComponent<TentacleController>().SetDamage(1);
                        }
                        Vector3 dir = hit.collider.gameObject.transform.position - PDG.Player.instance.transform.position;
                        dir.Normalize();
                        if (hit.collider.gameObject.GetComponent<DestroyObjects>())
                        {
                            hit.collider.gameObject.GetComponent<DestroyObjects>().DestroyObject(dir);
                        }
                        if (hit.collider.gameObject.GetComponent<ChestHit>() && !hit.collider.gameObject.GetComponent<ChestHit>().isOpenBefore)
                        {
                            hit.collider.gameObject.GetComponent<ChestHit>().SetDamage(1);
                        }
                        if (hit.collider.gameObject.GetComponent<DrumHit>())
                        {
                            hit.collider.gameObject.GetComponent<DrumHit>().SetDamage(1);
                        }
                    }
                    else
                    {
                        if (hit.collider.GetComponent<Player>())
                        {
                            if (!hit.collider.GetComponent<Player>().isHit && !hit.collider.GetComponent<Player>().isDie)
                            {
                                hit.collider.GetComponent<Player>().SetDamage(1, whyDie, DEATH_TYPE.DEATH_BOSS, "boss_stg");
                            }
                            if (DestroyObj)
                            {
                                GameObject dObj = Instantiate(DestroyObj, null);
                                dObj.transform.position = transform.position;
                                Destroy(dObj, 2);
                            }
                            if (DestroyObj2)
                            {
                                GameObject obj = Instantiate(DestroyObj2);
                                obj.transform.position = transform.position;
                                Destroy(obj, 2.0f);
                            }
                            if (isDestroy)
                            {
                                if (!isInstance) Destroy(gameObject);
                                else
                                {
                                    gameObject.SetActive(false);
                                    break;
                                }
                            }
                        }
                    }
                }
                else
                {
                    RaycastHit2D[] hits = new RaycastHit2D[0];
                    if (type == CollisionType.BOX) hits = Physics2D.BoxCastAll(transform.position + offSet, vDamageSize, fAngle + plusAngle, vDir, fDistance, previewLayer | 1 << 23);
                    else if (type == CollisionType.CAPSULE) hits = Physics2D.CapsuleCastAll(transform.position + offSet, vDamageSize, capsuleType, fAngle + plusAngle, vDir, fDistance, previewLayer | 1 << 23);
                    else if (type == CollisionType.CIRCLE) hits = Physics2D.CircleCastAll(transform.position + offSet, fDamageRange, vDir, fDistance, previewLayer | 1 << 23);
                    if (hits.Length > 0)
                    {
                        for (int i = 0; i < hits.Length; i++)
                        {
                            if (hits[i].collider.gameObject.layer.Equals(23))
                            {
                                if (hits[i].collider.transform.parent && hits[i].collider.transform.parent.GetComponent<TentacleController>())
                                {
                                    hits[i].collider.transform.parent.GetComponent<TentacleController>().SetDamage(1);
                                }
                                Vector3 dir = hits[i].collider.gameObject.transform.position - PDG.Player.instance.transform.position;
                                dir.Normalize();
                                if (hits[i].collider.gameObject.GetComponent<DestroyObjects>())
                                {
                                    hits[i].collider.gameObject.GetComponent<DestroyObjects>().DestroyObject(dir);
                                }
                                if (hits[i].collider.gameObject.GetComponent<ChestHit>() && !hits[i].collider.gameObject.GetComponent<ChestHit>().isOpenBefore)
                                {
                                    hits[i].collider.gameObject.GetComponent<ChestHit>().SetDamage(1);
                                }
                                if (hits[i].collider.gameObject.GetComponent<DrumHit>())
                                {
                                    hits[i].collider.gameObject.GetComponent<DrumHit>().SetDamage(1);
                                }
                            }
                            else
                            {
                                if (hits[i].collider.GetComponent<Player>())
                                {
                                    if (!hits[i].collider.GetComponent<Player>().isHit && !hits[i].collider.GetComponent<Player>().isDie)
                                    {
                                        hits[i].collider.GetComponent<Player>().SetDamage(1, whyDie, DEATH_TYPE.DEATH_BOSS, "boss_stg");
                                    }
                                    if (DestroyObj)
                                    {
                                        GameObject dObj = Instantiate(DestroyObj, null);
                                        dObj.transform.position = transform.position;
                                        Destroy(dObj, 2);
                                    }
                                    if (DestroyObj2)
                                    {
                                        GameObject obj = Instantiate(DestroyObj2);
                                        obj.transform.position = transform.position;
                                        Destroy(obj, 2.0f);
                                    }
                                    if (isDestroy)
                                    {
                                        if (!isInstance) Destroy(gameObject);
                                        else
                                        {
                                            gameObject.SetActive(false);
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            yield return new WaitForSeconds(Time.deltaTime / 10);
        }
    }
    public void RigidBodyFire(Vector3 _dir, float _pow)
    {
        gSpeed = _pow;
        GetComponent<Rigidbody2D>().AddForce(_dir * _pow, ForceMode2D.Impulse);
    }
}
