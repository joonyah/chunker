﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public struct tagStageGroup
{
    public Text _nameText;
    public Text _valueText;
}

public class StateGroup : MonoBehaviour
{
    public static StateGroup instance;
    public bool isOpen;
    [SerializeField] private GameObject openObj;
    public bool isClearTimeStart = false;
    public bool isBattleTimeStart = false;

    [SerializeField] private Image bossImg;
    [SerializeField] private bool isTest;
    [SerializeField] private float cur = 0.0f;
    [SerializeField] private AudioSource scoreAudio;
    #region -- StageGroup --
    [Header("NORMAL")]
    public float fClearTime = 0.0f;
    public int nShotByHunter = 0;
    public int nChanrgingSkillsNormal = 0;
    public int nHealthTowerUse = 0;
    public int nChestOpened = 0;
    public int nMutantCell = 0;
    [Header("BOSS")]
    public float fBattleTime = 0.0f;
    public int nShotByBoss = 0;
    public int nChanrgingSkillsBoss = 0;
    #endregion
    #region -- StageInformation UI --
    [Header("NORMAL")]
    [SerializeField] private tagStageGroup clearTime;
    [SerializeField] private tagStageGroup ShotByHunter;
    [SerializeField] private tagStageGroup ChanrgingSkillsNormal;
    [SerializeField] private tagStageGroup HealtowerUse;
    [SerializeField] private tagStageGroup ChestOpened;
    [SerializeField] private tagStageGroup MutantCell;
    [Header("BOSS")]
    [SerializeField] private tagStageGroup BattleTime;
    [SerializeField] private tagStageGroup ShotByBoss;
    [SerializeField] private tagStageGroup ChanrgingSkillsBoss;
    #endregion

    #region -- Achievements --
    [Header("Achievements")]
    public float totalPlayTime = 0.0f;
    public bool isSubCountCheck = false;
    public int subShotCount = 0;
    #endregion
    private void Awake()
    {
        if (instance == null) instance = this;
    }
    private void Start()
    {
        totalPlayTime = SaveAndLoadManager.instance.LoadAchievements("endlesschallenges_5")._cur;
        StartCoroutine(TotalPlayCheck());
    }
    IEnumerator TotalPlayCheck()
    {
        AchievementsData aData_0 = System.Array.Find(SheetManager.Instance.AchievementsDB.dataArray, item => item.ID.Equals("endlesschallenges_0"));
        AchievementsData aData_1 = System.Array.Find(SheetManager.Instance.AchievementsDB.dataArray, item => item.ID.Equals("endlesschallenges_1"));
        AchievementsData aData_2 = System.Array.Find(SheetManager.Instance.AchievementsDB.dataArray, item => item.ID.Equals("endlesschallenges_2"));
        AchievementsData aData_3 = System.Array.Find(SheetManager.Instance.AchievementsDB.dataArray, item => item.ID.Equals("endlesschallenges_3"));
        AchievementsData aData_4 = System.Array.Find(SheetManager.Instance.AchievementsDB.dataArray, item => item.ID.Equals("endlesschallenges_4"));
        AchievementsData aData_5 = System.Array.Find(SheetManager.Instance.AchievementsDB.dataArray, item => item.ID.Equals("endlesschallenges_5"));
        while (true)
        {
            totalPlayTime += 1.0f;
            SaveAndLoadManager.instance.SaveAchievements(new AchievementData[6]
            {
                new AchievementData { _id = aData_0.ID, _cur = Mathf.FloorToInt(totalPlayTime), _max = (aData_0.MAX * 60)},
                new AchievementData { _id = aData_1.ID, _cur = Mathf.FloorToInt(totalPlayTime), _max = (aData_1.MAX * 60)},
                new AchievementData { _id = aData_2.ID, _cur = Mathf.FloorToInt(totalPlayTime), _max = (aData_2.MAX * 60)},
                new AchievementData { _id = aData_3.ID, _cur = Mathf.FloorToInt(totalPlayTime), _max = (aData_3.MAX * 60)},
                new AchievementData { _id = aData_4.ID, _cur = Mathf.FloorToInt(totalPlayTime), _max = (aData_4.MAX * 60)},
                new AchievementData { _id = aData_5.ID, _cur = Mathf.FloorToInt(totalPlayTime), _max = (aData_5.MAX * 60)}
            });
            yield return new WaitForSecondsRealtime(60f);
        }
    }
    void Update()
    {
        if (isClearTimeStart) fClearTime += Time.deltaTime;
        if (isBattleTimeStart) fBattleTime += Time.deltaTime;


        if (isTest) 
        {
            if(Input.GetKeyDown(KeyCode.K))
            {
                OpenResult("boss_golem");
            }
        }

        if (isOpen) 
        {
            if (Input.GetKeyDown(KeyCode.Escape) || PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_CANCLE], true) && PDG.Dungeon.instance.closeCoolTime > 0.2f)
            {
                PDG.Dungeon.instance.closeCoolTime = 0.0f;
                isOpen = false;
                bossImg.transform.Find("Image").gameObject.SetActive(false);
                StopAllCoroutines();
                openObj.SetActive(false);
            }
        }
    }
    public void OpenResult(string _bossName)
    {
        isOpen = true;
        clearTime._valueText.text = string.Empty;
        ShotByHunter._valueText.text = string.Empty;
        ChanrgingSkillsNormal._valueText.text = string.Empty;
        HealtowerUse._valueText.text = string.Empty;
        ChestOpened._valueText.text = string.Empty;
        MutantCell._valueText.text = string.Empty;

        BattleTime._valueText.text = string.Empty;
        ShotByBoss._valueText.text = string.Empty;
        ChanrgingSkillsBoss._valueText.text = string.Empty;
        bossImg.sprite = Resources.Load<Sprite>("Mob/" + _bossName + "/" + _bossName);
        bossImg.color = new Color(0, 0, 0, 0);
        openObj.SetActive(true);
        StartCoroutine(Score());
    }
    IEnumerator Score()
    {
        clearTime._valueText.text = FloatToTime(Mathf.FloorToInt(fClearTime));
        scoreAudio.Play();
        yield return new WaitForSeconds(0.2f);
        ShotByHunter._valueText.text = nShotByHunter.ToString();
        scoreAudio.Play();
        yield return new WaitForSeconds(0.2f);
        ChanrgingSkillsNormal._valueText.text = nChanrgingSkillsNormal.ToString();
        scoreAudio.Play();
        yield return new WaitForSeconds(0.2f);
        HealtowerUse._valueText.text = nHealthTowerUse.ToString();
        scoreAudio.Play();
        yield return new WaitForSeconds(0.2f);
        ChestOpened._valueText.text = nChestOpened.ToString();
        scoreAudio.Play();
        yield return new WaitForSeconds(0.2f);
        MutantCell._valueText.text = nMutantCell.ToString();
        scoreAudio.Play();
        yield return new WaitForSeconds(0.2f);
        BattleTime._valueText.text = FloatToTime(Mathf.FloorToInt(fBattleTime));
        scoreAudio.Play();
        yield return new WaitForSeconds(0.2f);
        ShotByBoss._valueText.text = nShotByBoss.ToString();
        scoreAudio.Play();
        yield return new WaitForSeconds(0.2f);
        ChanrgingSkillsBoss._valueText.text = nChanrgingSkillsBoss.ToString();
        scoreAudio.Play();
        yield return new WaitForSeconds(0.2f);
        bossImg.color = new Color(1, 1, 1, 1);
        bossImg.transform.Find("Image").gameObject.SetActive(true);
    }
    public void ClearValue()
    {
        fClearTime = 0.0f;
        nShotByHunter = 0;
        nChanrgingSkillsNormal = 0;
        nHealthTowerUse = 0;
        nChestOpened = 0;
        nMutantCell = 0;

        fBattleTime = 0.0f;
        nShotByBoss = 0;
        nChanrgingSkillsBoss = 0;
        isBattleTimeStart = false;
        isClearTimeStart = false;
    }
    private string FloatToTime(int _time)
    {
        if (_time == 0) return "0S";

        int day = 0;
        int hour = 0;
        int minute = 0;
        int secend = 0;
        int newTime = _time;

        while (newTime >= 86400)
        {
            newTime -= 86400;
            day++;
        }

        while (newTime >= 3600)
        {
            newTime -= 3600;
            hour++;
        }

        while (newTime >= 60)
        {
            newTime -= 60;
            minute++;
        }

        secend = newTime;

        return (day > 0 ? day + "d " : string.Empty) + (hour > 0 ? hour + "h " : string.Empty) + (minute > 0 ? minute + "m " : string.Empty) + (secend > 0 ? secend + "s" : string.Empty);
    }
}
