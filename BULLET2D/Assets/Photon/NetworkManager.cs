﻿using Photon.Pun;
using Photon.Realtime;
using Steamworks;
using UnityEngine;

public class NetworkManager : MonoBehaviourPunCallbacks
{
    public static NetworkManager instance;
    [SerializeField] CSteamID steamId;
    [SerializeField] private byte _maxPlayerCount;
    private void Awake()
    {
        if (!SteamManager.Initialized) return;
        if (instance == null) instance = this;
        DontDestroyOnLoad(gameObject);
    }
    // Start is called before the first frame update
    void Start()
    {
        if (!SteamManager.Initialized) return;
        steamId = SteamUser.GetSteamID();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Connect() => PhotonNetwork.ConnectUsingSettings();
    public void DisConnected() => PhotonNetwork.Disconnect();
    public void CreateRoom() => PhotonNetwork.CreateRoom(steamId.ToString(), new RoomOptions { MaxPlayers = _maxPlayerCount });
    public void JoinLobby() => PhotonNetwork.JoinLobby();
    public void JoinOrCreateRoom() => PhotonNetwork.JoinOrCreateRoom(steamId.ToString(), new RoomOptions { MaxPlayers = _maxPlayerCount }, null);
    public void JoinRandomRoom() => PhotonNetwork.JoinRandomRoom();
    public void LeaveRoom() => PhotonNetwork.LeaveRoom();
    public void JoinRoom() => PhotonNetwork.JoinRoom(steamId.ToString());
    public override void OnCreatedRoom()
    {
        base.OnCreatedRoom();
        print("방만들기");
    }
    public override void OnJoinedRoom()
    {
        base.OnJoinedRoom();
        print("방참가완료");
    }
    public override void OnDisconnected(DisconnectCause cause)
    {
        base.OnDisconnected(cause);
        print("연결 끊김");
    }
    public override void OnConnectedToMaster()
    {
        base.OnConnectedToMaster();
        print("서버연결완료");
    }
    public override void OnJoinedLobby()
    {
        base.OnJoinedLobby();
        print("로비접속");
    }

    [ContextMenu("Info")]
    void Info()
    {
        if (PhotonNetwork.InRoom)
        {
            print("현재 방 이름 " + PhotonNetwork.CurrentRoom.Name);
            print("현재 방 인원수 " + PhotonNetwork.CurrentRoom.PlayerCount);
            print("현재 방 최대인원수 " + PhotonNetwork.CurrentRoom.MaxPlayers);

            string playerStr = "방에 있는 플레이어 목록";
            for (int i = 0; i < PhotonNetwork.PlayerList.Length; i++) playerStr += PhotonNetwork.PlayerList[i].NickName + ",";
            print(playerStr);
        }
        else
        {
            print("접속한 인원 수 " + PhotonNetwork.CountOfPlayers);
            print("방 개수 " + PhotonNetwork.CountOfRooms);
            print("모든 방에 있는 인원 수 " + PhotonNetwork.CountOfPlayersInRooms);
            print("로비에 있는지 ? " + PhotonNetwork.InLobby);
            print("연결됨?" + PhotonNetwork.IsConnected);
        }
    }
}
