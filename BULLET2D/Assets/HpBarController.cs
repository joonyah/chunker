﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HpBarController : MonoBehaviour
{
    public GameObject parantObj;
    // Update is called once per frame
    void Update()
    {
        if (parantObj == null)
        {
            Destroy(gameObject);
        }
        if (!parantObj.gameObject.activeSelf)
        {
            Destroy(gameObject);
        }
        if (parantObj.GetComponent<MOB.MonsterArggro>())
        {
            if (parantObj.GetComponent<MOB.MonsterArggro>()._monster)
            {
                if (parantObj.GetComponent<MOB.MonsterArggro>()._monster.isDie)
                {
                    Destroy(gameObject);
                }
            }
        }
    }

}
