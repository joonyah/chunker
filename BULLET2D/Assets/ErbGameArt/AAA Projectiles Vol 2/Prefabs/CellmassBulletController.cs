﻿using PDG;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CellmassBulletController : MonoBehaviour
{
    public GameObject target;
    public Vector3 dir;
    public float fSpeed;
    public GameObject DestroyObj;
    // Update is called once per frame
    void Update()
    {
        dir = target.transform.position - transform.position;
        dir.Normalize();

        transform.Translate(dir * fSpeed * Time.deltaTime);

        int mask = 1 << 12 | 1 << 24;
        RaycastHit2D hit = Physics2D.CircleCast(transform.position, 0.2f, Vector2.zero, 0, mask);
        if (hit)
        {
            Collider2D collision = hit.collider;
            if (collision.gameObject.layer.Equals(12))
            {
                if (collision.GetComponent<MOB.Monster>())
                {
                    collision.GetComponent<MOB.Monster>().SetDamage((Player.instance.isLeftNear ? Player.instance.damageNear : Player.instance.damage), -dir, 1, PDG.Player.instance.left_elemetanl, PDG.Player.instance.left_elemnetalTime);

                    if (PDG.Player.instance.isLeftNear && PDG.Player.instance.isCritical)
                    {
                        GameObject criEffect = ObjManager.Call().GetObject("CriticalHitEffect");
                        criEffect.transform.position = hit.point;
                        criEffect.SetActive(true);
                    }
                }
                if (PDG.Player.instance.isStrategic_02) PDG.Player.instance.isStrategic_02 = false;
            }
            else if (collision.gameObject.layer.Equals(24))
            {
                if (collision.GetComponent<MOB.BossMonster_Six_Apear>()) collision.GetComponent<MOB.BossMonster_Six_Apear>().SetDamage(Player.instance.damage);
                if (collision.GetComponent<Boss_STG>()) collision.GetComponent<Boss_STG>().SetDamage(Player.instance.isLeftNear ? Player.instance.damageNear : Player.instance.damage, hit.point);
                if (collision.GetComponent<Boss_NPC>()) collision.GetComponent<Boss_NPC>().SetDamage(Player.instance.isLeftNear ? Player.instance.damageNear : Player.instance.damage, hit.point);
                if (collision.GetComponent<Boss_Golem>()) collision.GetComponent<Boss_Golem>().SetDamage(Player.instance.isLeftNear ? Player.instance.damageNear : Player.instance.damage, hit.point);
                if (collision.GetComponent<Boss_Stone>()) collision.GetComponent<Boss_Stone>().SetDamage(Player.instance.isLeftNear ? Player.instance.damageNear : Player.instance.damage, hit.point, false);
                if (PDG.Player.instance.isStrategic_02) PDG.Player.instance.isStrategic_02 = false;

                if (PDG.Player.instance.isLeftNear && PDG.Player.instance.isCritical)
                {
                    GameObject criEffect = ObjManager.Call().GetObject("CriticalHitEffect");
                    criEffect.transform.position = hit.point;
                    criEffect.SetActive(true);
                }
            }
            //
            GameObject obj = Instantiate(DestroyObj);
            obj.transform.position = transform.position;
            Destroy(obj, 2.0f);
            Destroy(gameObject, 0.3f);
        }

        if (Vector3.Distance(target.transform.position, transform.position) < 0.2f)
        {
            GameObject obj = Instantiate(DestroyObj);
            obj.transform.position = transform.position;
            Destroy(obj, 2.0f);
            Destroy(gameObject);
        }
    }
}
