/*This script created by using docs.unity3d.com/ScriptReference/MonoBehaviour.OnParticleCollision.html*/
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using PDG;

public class ParticleCollisionInstance : MonoBehaviour
{
    public GameObject[] EffectsOnCollision;
    public float Offset = 0;
    public Vector3 rotationOffset = new Vector3(0,0,0);
    public float DestroyTimeDelay = 5;
    public bool UseWorldSpacePosition;
    public bool UseFirePointRotation;
    public bool DestoyMainEffect = true;
    private ParticleSystem part;
    private List<ParticleCollisionEvent> collisionEvents = new List<ParticleCollisionEvent>();
    private ParticleSystem ps;

    void Start()
    {
        part = GetComponent<ParticleSystem>();
    }
    void OnParticleCollision(GameObject other)
    {
        if (other.layer.Equals(24))
        {
            if (other.GetComponent<MOB.BossMonster_Six_Apear>()) other.GetComponent<MOB.BossMonster_Six_Apear>().SetDamage(Player.instance.damage);
            if (other.GetComponent<Boss_STG>()) other.GetComponent<Boss_STG>().SetDamage(Player.instance.isLeftNear ? Player.instance.damageNear : Player.instance.damage, other.transform.position);
            if (other.GetComponent<Boss_NPC>()) other.GetComponent<Boss_NPC>().SetDamage(Player.instance.isLeftNear ? Player.instance.damageNear : Player.instance.damage, other.transform.position);
            if (other.GetComponent<Boss_Golem>()) other.GetComponent<Boss_Golem>().SetDamage(Player.instance.isLeftNear ? Player.instance.damageNear : Player.instance.damage, other.transform.position);
            if (other.GetComponent<Boss_Stone>()) other.GetComponent<Boss_Stone>().SetDamage(Player.instance.isLeftNear ? Player.instance.damageNear : Player.instance.damage, other.transform.position, false);
            if (Player.instance.isStrategic_02) Player.instance.isStrategic_02 = false;
            if (PDG.Player.instance.isLeftNear && PDG.Player.instance.isCritical)
            {
                GameObject criEffect = ObjManager.Call().GetObject("CriticalHitEffect");
                criEffect.transform.position = other.transform.position;
                criEffect.SetActive(true);
            }
        }
        else if (other.layer.Equals(23))
        {

            if (other.name.Contains("puddle")) return;
            if (other.name.Contains("grass_2")) return;
            if (other.name.Contains("door")) return;

            if (other.transform.parent && other.transform.parent.GetComponent<TentacleController>())
            {
                other.transform.parent.GetComponent<TentacleController>().SetDamage(1);
            }
            Vector3 dir = other.transform.position - PDG.Player.instance.transform.position;
            dir.Normalize();
            if (other.GetComponent<DestroyObjects>())
            {
                other.GetComponent<DestroyObjects>().DestroyObject(dir);
            }
            if (other.GetComponent<ChestHit>() && !other.GetComponent<ChestHit>().isOpenBefore)
            {
                other.GetComponent<ChestHit>().SetDamage(1);
            }
            if (other.GetComponent<DrumHit>())
            {
                other.GetComponent<DrumHit>().SetDamage(1);
            }
        }
        else
        {
            Vector3 dir = other.transform.position - transform.position;
            dir.Normalize();

            if (other.GetComponent<MOB.Monster>())
            {
                other.GetComponent<MOB.Monster>().SetDamage((Player.instance.isLeftNear ? Player.instance.damageNear : Player.instance.damage), -dir, 1, ELEMENTAL_TYPE.NONE, 3.0f);
                if (PDG.Player.instance.isLeftNear && PDG.Player.instance.isCritical)
                {
                    GameObject criEffect = ObjManager.Call().GetObject("CriticalHitEffect");
                    criEffect.transform.position = other.transform.position;
                    criEffect.SetActive(true);
                }
            }
        }
        //int numCollisionEvents = part.GetCollisionEvents(other, collisionEvents);     
        //for (int i = 0; i < numCollisionEvents; i++)
        //{
        //    foreach (var effect in EffectsOnCollision)
        //    {
        //        var instance = Instantiate(effect, collisionEvents[i].intersection + collisionEvents[i].normal * Offset, new Quaternion()) as GameObject;
        //        if (UseFirePointRotation) { instance.transform.LookAt(transform.position); }
        //        else if (rotationOffset != Vector3.zero) { instance.transform.rotation = Quaternion.Euler(rotationOffset); }
        //        else { instance.transform.LookAt(collisionEvents[i].intersection + collisionEvents[i].normal); }
        //        if (!UseWorldSpacePosition) instance.transform.parent = transform;
        //        Destroy(instance, 1.0f);
        //    }
        //}
        //if (DestoyMainEffect == true)
        //{
        //    Destroy(gameObject, DestroyTimeDelay + 0.5f);
        //}
    }
}
