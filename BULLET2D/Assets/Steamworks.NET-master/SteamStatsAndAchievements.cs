﻿using UnityEngine;
using System.Collections;
using System.ComponentModel;
using Steamworks;

// 공식 Steamworks 예제 SpaceWar의 StatsAndAchievements.cpp 포트입니다.
class SteamStatsAndAchievements : MonoBehaviour
{
    public static SteamStatsAndAchievements instance;

    private enum Achievement : int
    {
        TutorialEnd,
        Boss_0_Clear,
        Boss_1_Clear,
        Boss_2_Clear,
        Boss_3_Clear,
        Difficulty_Easy_Clear,
        Difficulty_Normal_Clear,
        Difficulty_Hard_Clear,
        Difficulty_Hell_Clear,
    };

    private Achievement_t[] m_Achievements = new Achievement_t[] {
        new Achievement_t(Achievement.TutorialEnd, "TutorialEnd", ""),
        new Achievement_t(Achievement.Boss_0_Clear, "Boss_0_Clear", ""),
        new Achievement_t(Achievement.Boss_1_Clear, "Boss_1_Clear", ""),
        new Achievement_t(Achievement.Boss_2_Clear, "Boss_2_Clear", ""),
        new Achievement_t(Achievement.Boss_3_Clear, "Boss_3_Clear", ""),
        new Achievement_t(Achievement.Difficulty_Easy_Clear, "Difficulty_Easy_Clear", ""),
        new Achievement_t(Achievement.Difficulty_Normal_Clear, "Difficulty_Normal_Clear", ""),
        new Achievement_t(Achievement.Difficulty_Hard_Clear, "Difficulty_Hard_Clear", ""),
        new Achievement_t(Achievement.Difficulty_Hell_Clear, "Difficulty_Hell_Clear", "")
    };

    // 우리 GameID
    private CGameID m_GameID;
    // Did we get the stats from Steam?
    private bool m_bRequestedStats;
    private bool m_bStatsValid;

    // Should we store stats this frame?
    public bool m_bStoreStats;

    protected Callback<UserStatsReceived_t> m_UserStatsReceived;
    protected Callback<UserStatsStored_t> m_UserStatsStored;
    protected Callback<UserAchievementStored_t> m_UserAchievementStored;

    public bool isGUI = false;

    void OnEnable()
    {
        if (!SteamManager.Initialized)
            return;

        if (instance == null) instance = this;
        if (instance != this) Destroy(gameObject);
        DontDestroyOnLoad(gameObject);
        // Cache the GameID for use in the Callbacks
        m_GameID = new CGameID(SteamUtils.GetAppID());

        m_UserStatsReceived = Callback<UserStatsReceived_t>.Create(OnUserStatsReceived);
        m_UserStatsStored = Callback<UserStatsStored_t>.Create(OnUserStatsStored);
        m_UserAchievementStored = Callback<UserAchievementStored_t>.Create(OnAchievementStored);

        // 편집자는 다시로드 할 때마다 값을 지우려고합니다.
        m_bRequestedStats = false;
        m_bStatsValid = false;

    }
    IEnumerator PlayTimeSetting()
    {
        while(true)
        {
            yield return new WaitForSecondsRealtime(60.0f);
            Debug.Log("1분추가");
            SettingStat("stat_total_playtime");
        }
    }
    private void Update()
    {
        if (!SteamManager.Initialized)
            return;

        if (!m_bRequestedStats)
        {
            // Is Steam Loaded? if no, can't get stats, done
            if (!SteamManager.Initialized)
            {
                m_bRequestedStats = true;
                return;
            }

            // If yes, request our stats
            bool bSuccess = SteamUserStats.RequestCurrentStats();

            // This function should only return false if we weren't logged in, and we already checked that.
            // But handle it being false again anyway, just ask again later.
            m_bRequestedStats = bSuccess;
        }

        if (!m_bStatsValid)
            return;

        // Get info from sources

        // Evaluate achievements
        foreach (Achievement_t achievement in m_Achievements)
        {
            if (achievement.m_bAchieved)
                continue;
            switch (achievement.m_eAchievementID)
            {
                //case Achievement.ACH_FIRST_LOGIN:
                //    {
                //        bool firstCheck = false;
                //        if (SteamUserStats.GetAchievement("FIRSTLOGIN", out firstCheck))
                //        {
                //            if (!firstCheck)
                //                UnlockAchievement(achievement);
                //        }
                //    }
                //    break;
            }
        }

        //Store stats in the Steam database if necessary
        if (m_bStoreStats)
        {
            // already set any achievements in UnlockAchievement
            
            // set stats
            //var pchName2 = new InteropHelp.UTF8StringHandle("anti");
            //Debug.Log(pchName2);
            //if (!SteamUserStats.SetStat("anti", nAntimatter)) Debug.LogError("anti SET STATE 실패 ");
            bool bSuccess = SteamUserStats.StoreStats();
            //
            //var pchName3 = new InteropHelp.UTF8StringHandle("tile");
            //Debug.Log(pchName3);
            //if (!SteamUserStats.SetStat("tile", nTile)) Debug.LogError("tile SET STATE 실패 ");
            bSuccess = SteamUserStats.StoreStats();
            // If this failed, we never sent anything to the server, try
            // again later.
            m_bStoreStats = !bSuccess;
        }
    }

    //-----------------------------------------------------------------------------
    // Purpose: Unlock this achievement
    //-----------------------------------------------------------------------------
    private void UnlockAchievement(Achievement_t achievement)
    {
        achievement.m_bAchieved = true;

        // the icon may change once it's unlocked
        //achievement.m_iIconImage = 0;

        // mark it down
        SteamUserStats.SetAchievement(achievement.m_eAchievementID.ToString());

        // Store stats end of frame
        m_bStoreStats = true;
    }

    public void SettingStat(string _id, int _plusCount = 1, bool _isMaxCheck = false)
    {
        int tempStat = 0;
        SteamUserStats.GetStat(_id, out tempStat);
        if(_isMaxCheck)
        {
            if (_plusCount > tempStat) SteamUserStats.SetStat(_id, _plusCount);
        }
        else
        {
            SteamUserStats.SetStat(_id, tempStat + _plusCount);
        }

        // Store stats end of frame
        m_bStoreStats = true;
    }

    public int GettingStat(string _id)
    {
        int tempStat = 0;
        SteamUserStats.GetStat(_id, out tempStat);
        return tempStat;
    }

    public void UnlockAchievement(string achievementName)
    {
        foreach (Achievement_t achievement in m_Achievements)
        {
            if (achievement.m_bAchieved)
                continue;
            if (achievement.m_eAchievementID.ToString().Equals(achievementName))
            {
                UnlockAchievement(achievement);
                break;
            }
        }
    }

    public bool IsUnlockAchievement(string achievementName)
    {
        foreach (Achievement_t achievement in m_Achievements)
        {
            if (achievement.m_eAchievementID.ToString().Equals(achievementName))
            {
                return achievement.m_bAchieved;
            }
        }
        return false;
    }

    //-----------------------------------------------------------------------------
    // Purpose: We have stats data from Steam. It is authoritative, so update
    //			our data with those results now.
    //-----------------------------------------------------------------------------
    private void OnUserStatsReceived(UserStatsReceived_t pCallback)
    {
        if (!SteamManager.Initialized)
            return;

        // we may get callbacks for other games' stats arriving, ignore them
        if ((ulong)m_GameID == pCallback.m_nGameID)
        {
            if (EResult.k_EResultOK == pCallback.m_eResult)
            {
                Debug.Log("Received stats and achievements from Steam\n");

                m_bStatsValid = true;

                // load achievements
                foreach (Achievement_t ach in m_Achievements)
                {
                    bool ret = SteamUserStats.GetAchievement(ach.m_eAchievementID.ToString(), out ach.m_bAchieved);
                    if (ret)
                    {
                        ach.m_strName = SteamUserStats.GetAchievementDisplayAttribute(ach.m_eAchievementID.ToString(), "name");
                        ach.m_strDescription = SteamUserStats.GetAchievementDisplayAttribute(ach.m_eAchievementID.ToString(), "desc");
                    }
                    else
                    {
                        Debug.LogWarning("SteamUserStats.GetAchievement failed for Achievement " + ach.m_eAchievementID + "\nIs it registered in the Steam Partner site?");
                    }
                }

                // load stats
                //Debug.Log("로드 한번 하는데 왜 값이 안 변해?");
                //SteamUserStats.GetStat("anti", out nAntimatter);
                //SteamUserStats.IndicateAchievementProgress("antimatter", (uint)nAntimatter, 1000);

                //if (nAntimatter >= 1000)
                //{
                //    SteamUserStats.GetAchievement("antimatter", out bool pbAchCheck);
                //    if (!pbAchCheck) UnlockAchievement("antimatter");
                //}
                //if (nAntimatter >= 10000)
                //{
                //    SteamUserStats.GetAchievement("antimatter_10000", out bool pbAchCheck);
                //    if (!pbAchCheck) UnlockAchievement("antimatter_10000");
                //}
                //if (nAntimatter >= 100000)
                //{
                //    SteamUserStats.GetAchievement("antimatter_100000", out bool pbAchCheck);
                //    if (!pbAchCheck) UnlockAchievement("antimatter_100000");
                //}
                //
                //SteamUserStats.GetStat("tile", out nTile);
                //SteamUserStats.IndicateAchievementProgress("Survivalbeginner_humantype", (uint)nTile, 500);
                //
                //if (nTile >= 500)
                //{
                //    SteamUserStats.GetAchievement("Survivalbeginner_humantype", out bool pbAchCheck);
                //    if (!pbAchCheck) UnlockAchievement("Survivalbeginner_humantype");
                //}
            }
            else
            {
                Debug.Log("RequestStats - failed, " + pCallback.m_eResult);
            }
        }
    }

    //-----------------------------------------------------------------------------
    // Purpose: Our stats data was stored!
    //-----------------------------------------------------------------------------
    private void OnUserStatsStored(UserStatsStored_t pCallback)
    {
        // we may get callbacks for other games' stats arriving, ignore them
        if ((ulong)m_GameID == pCallback.m_nGameID)
        {
            if (EResult.k_EResultOK == pCallback.m_eResult)
            {
                //Debug.Log("StoreStats - success");
            }
            else if (EResult.k_EResultInvalidParam == pCallback.m_eResult)
            {
                // One or more stats we set broke a constraint. They've been reverted,
                // and we should re-iterate the values now to keep in sync.
                Debug.Log("StoreStats - some failed to validate");
                // Fake up a callback here so that we re-load the values.
                UserStatsReceived_t callback = new UserStatsReceived_t();
                callback.m_eResult = EResult.k_EResultOK;
                callback.m_nGameID = (ulong)m_GameID;
                OnUserStatsReceived(callback);
            }
            else
            {
                Debug.Log("StoreStats - failed, " + pCallback.m_eResult);
            }
        }
    }

    //-----------------------------------------------------------------------------
    // Purpose: An achievement was stored
    //-----------------------------------------------------------------------------
    private void OnAchievementStored(UserAchievementStored_t pCallback)
    {
        // We may get callbacks for other games' stats arriving, ignore them
        if ((ulong)m_GameID == pCallback.m_nGameID)
        {
            if (0 == pCallback.m_nMaxProgress)
            {
                //Debug.Log("Achievement '" + pCallback.m_rgchAchievementName + "' unlocked!");
            }
            else
            {
                Debug.Log("Achievement '" + pCallback.m_rgchAchievementName + "' progress callback, (" + pCallback.m_nCurProgress + "," + pCallback.m_nMaxProgress + ")");
            }
        }
    }

    //-----------------------------------------------------------------------------
    // Purpose: Display the user's stats and achievements
    //-----------------------------------------------------------------------------
    void OnGUI()
    {
        if (isGUI)
        {
            //if (!SteamManager.Initialized)
            //{
            //    GUILayout.Label("Steamworks not Initialized");
            //    return;
            //}
            //
            //if (GameObject.Find("Canvas"))
            //{
            //    if (GameObject.Find("Canvas").transform.Find("InputField"))
            //    {
            //        GameObject.Find("Canvas").transform.Find("InputField").gameObject.SetActive(true);
            //    }
            //}
            //
            //// FOR TESTING PURPOSES ONLY!
            //if (GUILayout.Button("RESET STATS AND ACHIEVEMENTS"))
            //{
            //    SteamUserStats.ResetAllStats(true);
            //    SteamUserStats.RequestCurrentStats();
            //    //OnGameStateChange(EClientGameState.k_EClientGameActive);
            //}
            //GUILayout.Space(10);
            //GUILayout.BeginArea(new Rect(Screen.width - 300, 0, 300, 1200));
            //foreach (Achievement_t ach in m_Achievements)
            //{
            //    GUILayout.Label(ach.m_eAchievementID.ToString());
            //    GUILayout.Label(ach.m_strName + " - " + ach.m_strDescription);
            //    GUILayout.Label("Achieved: " + ach.m_bAchieved);
            //    GUILayout.Space(20);
            //}
            //GUILayout.EndArea();
        }   
    }

    private class Achievement_t
    {
        public Achievement m_eAchievementID;
        public string m_strName;
        public string m_strDescription;
        public bool m_bAchieved;

        /// <summary>
        /// Creates an Achievement. You must also mirror the data provided here in https://partner.steamgames.com/apps/achievements/yourappid
        /// </summary>
        /// <param name="achievement">The "API Name Progress Stat" used to uniquely identify the achievement.</param>
        /// <param name="name">The "Display Name" that will be shown to players in game and on the Steam Community.</param>
        /// <param name="desc">The "Description" that will be shown to players in game and on the Steam Community.</param>
        public Achievement_t(Achievement achievementID, string name, string desc)
        {
            m_eAchievementID = achievementID;
            m_strName = name;
            m_strDescription = desc;
            m_bAchieved = false;
        }
    }

    public void UnlockAchievementToPublic(string achievementID)
    {
        if (!SteamManager.Initialized) return;
        foreach (Achievement_t achievement in m_Achievements)
        {
            if (achievement.m_bAchieved)
                continue;

            if (achievement.m_eAchievementID.ToString() == achievementID)
                UnlockAchievement(achievement);
        }
    }
}