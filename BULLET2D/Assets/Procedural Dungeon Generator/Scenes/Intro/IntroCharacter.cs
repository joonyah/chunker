﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.Video;

public class IntroCharacter : MonoBehaviour
{
    [SerializeField] private Image logoImage;
    [SerializeField] private GameObject Canvas;
    [SerializeField] private VideoPlayer _video;
    [SerializeField] private VideoPlayer _video0;

    [SerializeField] private GameObject VideoObj;
    [SerializeField] private bool isIntro = false;
    [SerializeField] private bool isLogo = false;
    [SerializeField] private float closeTime = 0.0f;
    [SerializeField] private float curTime = 0f;
    private void Start()
    {
        StartCoroutine(LogoOpen());
    }
    void Update()
    {
        closeTime += Time.deltaTime;
        if (Input.anyKey && isIntro && closeTime > 0.2f)
        {
            closeTime = 0.0f;
            isIntro = false;
        }
        if (Input.anyKey && isLogo && closeTime > 0.2f)
        {
            closeTime = 0.0f;
            isLogo = false;
        }
    }
    IEnumerator LogoOpen()
    {
        isLogo = true;

        float maxTime = 4f;
        float curTime = 0f;
        while (isLogo)
        {
            curTime += Time.fixedDeltaTime;
            if (curTime > maxTime) break;
            yield return new WaitForFixedUpdate();
        }
        _video0.gameObject.SetActive(false);
        _video.enabled = true;
        _video.GetComponent<AudioSource>().enabled = true;
        maxTime = 10f;
        curTime = 0f;
        while (isLogo)
        {
            curTime += Time.fixedDeltaTime;
            if (curTime > maxTime) isLogo = false;
            yield return new WaitForFixedUpdate();
        }
        _video.GetComponent<AudioSource>().enabled = false;
        StartCoroutine(FirstIntro());
    }
    IEnumerator FirstIntro()
    {
        isIntro = true;
        VideoObj.SetActive(true);
        VideoObj.transform.Find("CHN_S").gameObject.SetActive(false);
        VideoObj.transform.Find("CHN_T").gameObject.SetActive(false);
        VideoObj.transform.Find("ENG").gameObject.SetActive(false);
        VideoObj.transform.Find("KOR").gameObject.SetActive(false);
        if (LocalizeManager.lang == LANGUAGE_SHIFTRICK.CHN_S)
            VideoObj.transform.Find("CHN_S").gameObject.SetActive(true);
        else if (LocalizeManager.lang == LANGUAGE_SHIFTRICK.CHN_T)
            VideoObj.transform.Find("CHN_T").gameObject.SetActive(true);
        else if (LocalizeManager.lang == LANGUAGE_SHIFTRICK.ENG)
            VideoObj.transform.Find("ENG").gameObject.SetActive(true);
        else if (LocalizeManager.lang == LANGUAGE_SHIFTRICK.KOR)
            VideoObj.transform.Find("KOR").gameObject.SetActive(true);
        float maxTime = 37f;
        while (isIntro)
        {
            curTime += Time.fixedDeltaTime;
            if (curTime > maxTime) isIntro = false;
            yield return new WaitForFixedUpdate();
        }
        _video.enabled = false;
        VideoObj.SetActive(false);
        SceneManager.LoadScene("Login");
    }
}
