﻿using Steamworks;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;


[Serializable]
public struct UnlockDatas
{
    public string _id;
    public int _nCount;
}
[Serializable]
public struct ItemSave
{
    public string _id;
    public int _nCount;
}
[Serializable]
public struct VolumeSave
{
    public int _type;
    public float _value;
}
[Serializable]
public struct ChoiseStateSave
{
    public int _level;
    public string _type;
    public string _id;
}
[Serializable]
public struct GameSaveData
{
    public string id;
    public int count;
}
[Serializable]
public struct MailSaveData
{
    public string _id;
    public bool isPush;
    public bool isCheck;
    public string date;
    public string[] items;
    public bool[] isItems;
}
[Serializable]
public struct CollectionData
{
    public string _id;
    public bool _isOpen;
    public bool _isWeapon;
}
[Serializable]
public struct KeySettingData
{
    public int _code;
    public int _num;
}
[Serializable]
public struct AchievementData
{
    public string _id;
    public int _max;
    public int _cur;
    public bool _clear;
    public string _date;
    public bool _reward;
}
[Serializable]
public struct ManufacturesData
{
    public string id;
    public bool isRush;
    public List<string> stateid;
    public List<int> statevalue;
    public List<int> statestep;
}
[Serializable]
public struct RushData
{
    public int diff;
    public float time;
    public string date;
    public string weapon;
    public int usesell;
}
[Serializable]
public struct SaveMap
{
    public string id;
    public int x;
    public int y;
}
public class SaveAndLoadManager : MonoBehaviour
{
    string m_strPath = "SaveData/";
    string dPath;
    public static SaveAndLoadManager instance;
    public static bool isLoad = false;
    public static int nDifficulty = 0;
    public static int specialID = -1;
    private void Awake()
    {
        dPath = Application.dataPath;
        int t = dPath.LastIndexOf('/');
        dPath = dPath.Substring(0, t);
        if (instance == null) instance = this;
        if (instance != this) Destroy(this.gameObject);

        DirectoryInfo di = new DirectoryInfo(dPath + "/" + m_strPath);
        if (di.Exists == false)
        {
            di.Create();
        }

        //DontDestroyOnLoad(this);
    }
    public void SaveItem(ItemSave _save, bool noSum = false)
    {
        var b = new BinaryFormatter();
        List<ItemSave> saveList = new List<ItemSave>();
        if (File.Exists(dPath + "/" + m_strPath + "inventory.Data"))
        {
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream stream = new FileStream(dPath + "/" + m_strPath + "inventory.Data", FileMode.Open);
            byte[] saveDataTemp = formatter.Deserialize(stream) as byte[];
            ItemSave[] saveDataLoad = ByteToObject(saveDataTemp) as ItemSave[];
            saveList = saveDataLoad.ToList();
            bool isChange = false;
            for (int i = 0; i < saveList.Count; i++)
            {
                if (saveList[i]._id.Equals(_save._id))
                {
                    ItemSave changeData = saveList[i];
                    if (noSum)
                        changeData._nCount = _save._nCount;
                    else
                        changeData._nCount += _save._nCount;
                    saveList[i] = changeData;
                    isChange = true;
                    break;
                }
            }
            if (!isChange) saveList.Add(_save);
            stream.Close();
            byte[] saveData = ObjectToByte(saveList.ToArray());
            var f = File.Create(dPath + "/" + m_strPath + "inventory.Data");
            b.Serialize(f, saveData);
            f.Close();
        }
        else
        {
            var f = File.Create(dPath + "/" + m_strPath + "inventory.Data");
            saveList.Add(_save);
            byte[] saveData = ObjectToByte(saveList.ToArray());
            b.Serialize(f, saveData);
            f.Close();
        }
    }
    public int LoadItem(string _id)
    {
        var b = new BinaryFormatter();
        if (File.Exists(dPath + "/" + m_strPath + "inventory.Data"))
        {
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream stream = new FileStream(dPath + "/" + m_strPath + "inventory.Data", FileMode.Open);
            byte[] saveDataTemp = formatter.Deserialize(stream) as byte[];
            ItemSave[] saveDataLoad = ByteToObject(saveDataTemp) as ItemSave[];
            stream.Close();
            for (int i = 0; i < saveDataLoad.Length; i++) 
            {
                if (saveDataLoad[i]._id.Equals(_id)) return saveDataLoad[i]._nCount;
            }
        }
        return -1;
    }
    public void DeleteItem(string _id)
    {
        var b = new BinaryFormatter();

        List<ItemSave> saveList = new List<ItemSave>();
        if (File.Exists(dPath + "/" + m_strPath + "inventory.Data"))
        {
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream stream = new FileStream(dPath + "/" + m_strPath + "inventory.Data", FileMode.Open);
            byte[] saveDataTemp = formatter.Deserialize(stream) as byte[];
            ItemSave[] saveDataLoad = ByteToObject(saveDataTemp) as ItemSave[];
            saveList = saveDataLoad.ToList();
            for (int i = 0; i < saveList.Count; i++)
            {
                if (saveList[i]._id.Equals(_id))
                {
                    saveList.RemoveAt(i);
                    break;
                }
            }
            stream.Close();
            byte[] saveData = ObjectToByte(saveList.ToArray());
            var f = File.Create(dPath + "/" + m_strPath + "inventory.Data");
            b.Serialize(f, saveData);
            f.Close();
        }
    }
    public void DeleteItemBossRush()
    {
        var b = new BinaryFormatter();

        List<ItemSave> saveList = new List<ItemSave>();
        if (File.Exists(dPath + "/" + m_strPath + "inventory.Data"))
        {
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream stream = new FileStream(dPath + "/" + m_strPath + "inventory.Data", FileMode.Open);
            byte[] saveDataTemp = formatter.Deserialize(stream) as byte[];
            ItemSave[] saveDataLoad = ByteToObject(saveDataTemp) as ItemSave[];
            saveList = saveDataLoad.ToList();
            for (int i = 0; i < saveList.Count; i++)
            {
                if (saveList[i]._id.Equals("fluid_attack2"))
                {
                    saveList.RemoveAt(i);
                    i--;
                }
                else if (saveList[i]._id.Equals("fluid_aim2"))
                {
                    saveList.RemoveAt(i);
                    i--;
                }
                else if (saveList[i]._id.Equals("fluid_range2"))
                {
                    saveList.RemoveAt(i);
                    i--;
                }
                else if (saveList[i]._id.Equals("fluid_critical2"))
                {
                    saveList.RemoveAt(i);
                    i--;
                }
                else if (saveList[i]._id.Equals("fluid_multiShoot2"))
                {
                    saveList.RemoveAt(i);
                    i--;
                }
                else if (saveList[i]._id.Equals("fluid_shootCount2"))
                {
                    saveList.RemoveAt(i);
                    i--;
                }
                else if (saveList[i]._id.Equals("fluid_shootDelay2"))
                {
                    saveList.RemoveAt(i);
                    i--;
                }
                else if (saveList[i]._id.Equals("fluid_criticalDamage2"))
                {
                    saveList.RemoveAt(i);
                    i--;
                }
            }
            stream.Close();
            byte[] saveData = ObjectToByte(saveList.ToArray());
            var f = File.Create(dPath + "/" + m_strPath + "inventory.Data");
            b.Serialize(f, saveData);
            f.Close();
        }
    }
    public void SaveUnlock(string nowUnlockID, int nowCount)
    {
        try
        {
            var b = new BinaryFormatter();
            List<UnlockDatas> saveList = new List<UnlockDatas>();
            if (File.Exists(dPath + "/" + m_strPath + "unlock.Data"))
            {
                BinaryFormatter formatter = new BinaryFormatter();
                FileStream stream = new FileStream(dPath + "/" + m_strPath + "unlock.Data", FileMode.Open);
                byte[] saveDataTemp = formatter.Deserialize(stream) as byte[];
                UnlockDatas[] saveDataLoad = ByteToObject(saveDataTemp) as UnlockDatas[];
                saveList = saveDataLoad.ToList();
                bool isChange = false;
                for (int i = 0; i < saveList.Count; i++)
                {
                    if (saveList[i]._id.Equals(nowUnlockID))
                    {
                        UnlockDatas changeData = saveList[i];
                        changeData._nCount = nowCount;
                        saveList[i] = changeData;
                        isChange = true;
                        break;
                    }
                }
                if (!isChange)
                {
                    UnlockDatas data = new UnlockDatas
                    {
                        _id = nowUnlockID,
                        _nCount = nowCount
                    };
                    saveList.Add(data);
                }
                stream.Close();

                var f = File.Create(dPath + "/" + m_strPath + "unlock.Data");
                byte[] saveData = ObjectToByte(saveList.ToArray());
                b.Serialize(f, saveData);
                f.Close();
            }
            else
            {
                var f = File.Create(dPath + "/" + m_strPath + "unlock.Data");
                UnlockDatas data = new UnlockDatas
                {
                    _id = nowUnlockID,
                    _nCount = nowCount
                };
                saveList.Add(data);
                byte[] saveData = ObjectToByte(saveList.ToArray());
                b.Serialize(f, saveData);
                f.Close();
            }

        }
        catch (System.Exception ex)
        {
            Debug.LogError(ex.Message);
        }
    }
    public UnlockDatas[] LoadUnlock()
    {
        if (File.Exists(dPath + "/" + m_strPath + "unlock.Data"))
        {
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream stream = new FileStream(dPath + "/" + m_strPath + "unlock.Data", FileMode.Open);
            byte[] saveDataTemp = formatter.Deserialize(stream) as byte[];
            UnlockDatas[] saveData = ByteToObject(saveDataTemp) as UnlockDatas[];
            stream.Close();
            return saveData;
        }
        return null;
    }
    public int LoadUnlock(string _id)
    {
        if (File.Exists(dPath + "/" + m_strPath + "unlock.Data"))
        {
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream stream = new FileStream(dPath + "/" + m_strPath + "unlock.Data", FileMode.Open);
            byte[] saveDataTemp = formatter.Deserialize(stream) as byte[];
            UnlockDatas[] saveData = ByteToObject(saveDataTemp) as UnlockDatas[];
            stream.Close();
            for (int i = 0; i < saveData.Length; i++)
            {
                if (saveData[i]._id.Equals(_id))
                {
                    return saveData[i]._nCount;
                }
            }
        }
        return 0;
    }
    public void SaveNPC(string _npc)
    {
        var b = new BinaryFormatter();
        List<string> saveList = new List<string>();
        if (File.Exists(dPath + "/" + m_strPath + "NPC.Data"))
        {
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream stream = new FileStream(dPath + "/" + m_strPath + "NPC.Data", FileMode.Open);
            byte[] saveDataTemp = formatter.Deserialize(stream) as byte[];
            string[] saveDataLoad = ByteToObject(saveDataTemp) as string[];
            saveList = saveDataLoad.ToList();
            bool isChange = false;
            for (int i = 0; i < saveList.Count; i++)
            {
                if (saveList[i].Equals(_npc))
                {
                    string changeData = saveList[i];
                    isChange = true;
                    break;
                }
            }
            if (!isChange) saveList.Add(_npc);
            stream.Close();
            byte[] saveData = ObjectToByte(saveList.ToArray());
            var f = File.Create(dPath + "/" + m_strPath + "NPC.Data");
            b.Serialize(f, saveData);
            f.Close();
        }
        else
        {
            var f = File.Create(dPath + "/" + m_strPath + "NPC.Data");
            saveList.Add(_npc);
            byte[] saveData = ObjectToByte(saveList.ToArray());
            b.Serialize(f, saveData);
            f.Close();
        }
    }
    public bool LoadNPC(string _npc)
    {
        if (File.Exists(dPath + "/" + m_strPath + "NPC.Data"))
        {
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream stream = new FileStream(dPath + "/" + m_strPath + "NPC.Data", FileMode.Open);
            byte[] saveDataTemp = formatter.Deserialize(stream) as byte[];
            string[] saveData = ByteToObject(saveDataTemp) as string[];
            stream.Close();
            for (int i = 0; i < saveData.Length; i++)
            {
                if (saveData[i].Equals(_npc))
                {
                    return true;
                }
            }
        }

        return false;
    }
    public void SaveVolume(VolumeSave _save)
    {
        try
        {
            var b = new BinaryFormatter();
            List<VolumeSave> saveList = new List<VolumeSave>();
            if (File.Exists(dPath + "/" + m_strPath + "volume.Data"))
            {
                BinaryFormatter formatter = new BinaryFormatter();
                FileStream stream = new FileStream(dPath + "/" + m_strPath + "volume.Data", FileMode.Open);
                byte[] saveDataTemp = formatter.Deserialize(stream) as byte[];
                VolumeSave[] saveDataLoad = ByteToObject(saveDataTemp) as VolumeSave[];
                saveList = saveDataLoad.ToList();
                bool isChange = false;
                for (int i = 0; i < saveList.Count; i++)
                {
                    if (saveList[i]._type.Equals(_save._type))
                    {
                        VolumeSave changeData = saveList[i];
                        changeData._value = _save._value;
                        saveList[i] = changeData;
                        isChange = true;
                        break;
                    }
                }
                if (!isChange)
                {
                    VolumeSave data = new VolumeSave
                    {
                        _type = _save._type,
                        _value = _save._value
                    };
                    saveList.Add(data);
                }
                stream.Close();

                var f = File.Create(dPath + "/" + m_strPath + "volume.Data");
                byte[] saveData = ObjectToByte(saveList.ToArray());
                b.Serialize(f, saveData);
                f.Close();
            }
            else
            {
                var f = File.Create(dPath + "/" + m_strPath + "volume.Data");
                VolumeSave data = new VolumeSave
                {
                    _type = _save._type,
                    _value = _save._value
                };
                saveList.Add(data);
                byte[] saveData = ObjectToByte(saveList.ToArray());
                b.Serialize(f, saveData);
                f.Close();
            }

        }
        catch (System.Exception ex)
        {
            Debug.LogError(ex.Message);
        }
    }
    public VolumeSave[] LoadVolume()
    {
        if (File.Exists(dPath + "/" + m_strPath + "volume.Data"))
        {
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream stream = new FileStream(dPath + "/" + m_strPath + "volume.Data", FileMode.Open);
            byte[] saveDataTemp = formatter.Deserialize(stream) as byte[];
            VolumeSave[] saveData = ByteToObject(saveDataTemp) as VolumeSave[];
            stream.Close();
            return saveData;
        }
        return null;
    }
    public void SaveChoiseState(ChoiseStateSave _save)
    {
        var b = new BinaryFormatter();
        List<ChoiseStateSave> saveList = new List<ChoiseStateSave>();
        if (File.Exists(dPath + "/" + m_strPath + "choiseStat.Data"))
        {
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream stream = new FileStream(dPath + "/" + m_strPath + "choiseStat.Data", FileMode.Open);
            byte[] saveDataTemp = formatter.Deserialize(stream) as byte[];
            ChoiseStateSave[] saveDataLoad = ByteToObject(saveDataTemp) as ChoiseStateSave[];
            saveList = saveDataLoad.ToList();
            bool isChange = false;
            for (int i = 0; i < saveList.Count; i++)
            {
                if (saveList[i]._id.Equals(_save._id))
                {
                    saveList[i] = _save;
                    isChange = true;
                    break;
                }
            }
            if (!isChange) saveList.Add(_save);
            stream.Close();
            byte[] saveData = ObjectToByte(saveList.ToArray());
            var f = File.Create(dPath + "/" + m_strPath + "choiseStat.Data");
            b.Serialize(f, saveData);
            f.Close();
        }
        else
        {
            var f = File.Create(dPath + "/" + m_strPath + "choiseStat.Data");
            saveList.Add(_save);
            byte[] saveData = ObjectToByte(saveList.ToArray());
            b.Serialize(f, saveData);
            f.Close();
        }
    }
    public ChoiseStateSave LoadChoiseState(string _id)
    {
        var b = new BinaryFormatter();
        if (File.Exists(dPath + "/" + m_strPath + "choiseStat.Data"))
        {
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream stream = new FileStream(dPath + "/" + m_strPath + "choiseStat.Data", FileMode.Open);
            byte[] saveDataTemp = formatter.Deserialize(stream) as byte[];
            ChoiseStateSave[] saveDataLoad = ByteToObject(saveDataTemp) as ChoiseStateSave[];
            stream.Close();
            for (int i = 0; i < saveDataLoad.Length; i++)
            {
                if (saveDataLoad[i]._id.Equals(_id)) return saveDataLoad[i];
            }
        }
        return new ChoiseStateSave { _id = "none", _level = 0, _type = null };
    }
    public bool GameSave(ItemSlot[] _slots)
    {
        List<GameSaveData> list = new List<GameSaveData>();
        for (int i = 0; i < _slots.Length; i++)
        {
            if (_slots[i]._Item != null)
            {
                if(!_slots[i]._Item.Data.ID.Equals(string.Empty))
                {
                    list.Add(new GameSaveData { id = _slots[i]._Item.Data.ID, count = _slots[i].itemCount });
                }
            }
        }
        list.Add(new GameSaveData { id = "key_normal", count = Inventorys.Instance.key });
        list.Add(new GameSaveData { id = "reinforcement", count = Inventorys.Instance.reinforcePoint });
        list.Add(new GameSaveData { id = "gold_bar", count = Inventorys.Instance.goldbar });
        list.Add(new GameSaveData { id = "savedata_nQChange", count = PDG.Dungeon.instance.nQChange });
        list.Add(new GameSaveData { id = "savedata_stage_level", count = PDG.Dungeon.instance.StageLevel });
        list.Add(new GameSaveData { id = "savedata_stage_step", count = PDG.Dungeon.instance.nowStageStep });
        list.Add(new GameSaveData { id = "savedata_stage_boss", count = PDG.Dungeon.instance.isStageBoss ? 1 : 0 });
        list.Add(new GameSaveData { id = "savedata_stage_rank_0", count = PDG.Dungeon.instance.stageRan[0] });
        list.Add(new GameSaveData { id = "savedata_stage_rank_1", count = PDG.Dungeon.instance.stageRan[1] });
        list.Add(new GameSaveData { id = "savedata_stage_rank_2", count = PDG.Dungeon.instance.stageRan[2] });
        list.Add(new GameSaveData { id = "savedata_stage_rank_3", count = PDG.Dungeon.instance.stageRan[3] });
        list.Add(new GameSaveData { id = "savedata_stage_boss_0", count = PDG.Dungeon.instance.stageBossRan[0] });
        list.Add(new GameSaveData { id = "savedata_stage_boss_1", count = PDG.Dungeon.instance.stageBossRan[1] });
        list.Add(new GameSaveData { id = "savedata_stage_boss_2", count = PDG.Dungeon.instance.stageBossRan[2] });
        list.Add(new GameSaveData { id = "savedata_stage_boss_3", count = PDG.Dungeon.instance.stageBossRan[3] });
        list.Add(new GameSaveData { id = "savedata_curseHealth", count = PDG.Dungeon.curseHealth });
        list.Add(new GameSaveData { id = "savedata_curseHealth2", count = PDG.Dungeon.curseHealth2 });
        list.Add(new GameSaveData { id = "savedata_curseAttack", count = PDG.Dungeon.curseAttack });
        list.Add(new GameSaveData { id = "savedata_curseAttack2", count = PDG.Dungeon.curseAttack2 });
        list.Add(new GameSaveData { id = "savedata_isMercenaryAjit", count = PDG.Dungeon.instance.isMercenaryAjit ? 1 : 0 });
        list.Add(new GameSaveData { id = "savedata_specialID", count = specialID });
        list.Add(new GameSaveData { id = "savedata_difficulty", count = nDifficulty });
        var b = new BinaryFormatter();
        var f = File.Create(dPath + "/" + m_strPath + "GameSave.Data");
        byte[] saveData = ObjectToByte(list.ToArray());
        b.Serialize(f, saveData);
        f.Close();
        return true;
    }
    public GameSaveData[] GameLoad()
    {
        var b = new BinaryFormatter();
        if (File.Exists(dPath + "/" + m_strPath + "GameSave.Data"))
        {
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream stream = new FileStream(dPath + "/" + m_strPath + "GameSave.Data", FileMode.Open);
            byte[] saveDataTemp = formatter.Deserialize(stream) as byte[];
            GameSaveData[] saveDataLoad = ByteToObject(saveDataTemp) as GameSaveData[];
            stream.Close();
            return saveDataLoad;
        }

        return null;
    }
    public bool GameDelete()
    {
        // 시작 아이템 레벨 체크
        string startLeft = string.Empty;
        if (PDG.Dungeon.instance != null)
        {
            if(PDG.Dungeon.instance.isStartGun)
            {
                int tLeft = LoadUnlock("start_gun_1");
                int tLeft2 = LoadUnlock("start_gun_2");
                if (tLeft2 == 0 && tLeft == 0) startLeft = "start_gun";
                else if(tLeft2 == 1) startLeft = "start_gun_2";
                else startLeft = "start_gun_1";
            }
            else
            {
                int tLeft = LoadUnlock("tentacle_arm_1");
                if (tLeft == 0) startLeft = "tentacle_arm";
                else startLeft = "tentacle_arm_1";
            }
        }

        int tRight = LoadUnlock("catching_tentacle_1");
        string startRight = string.Empty;
        if (tRight == 0) startRight = "catching_tentacle";
        else startRight = "catching_tentacle_1";

        string startQ = string.Empty;
        if(PDG.Dungeon.instance.nQChange == 0)
        {
            int tQ = LoadUnlock("spiny_cell_firing_1");
            if (tQ == 0) startQ = "spiny_cell_firing";
            else startQ = "spiny_cell_firing_1";
        }
        else
        {
            startQ = "q_change";
        }

        List<GameSaveData> list = new List<GameSaveData>();
        list.Add(new GameSaveData { id = "key_normal", count = Inventorys.Instance.key });
        list.Add(new GameSaveData { id = "reinforcement", count = Inventorys.Instance.reinforcePoint });
        list.Add(new GameSaveData { id = "gold_bar", count = Inventorys.Instance.goldbar });
        list.Add(new GameSaveData { id = "savedata_nQChange", count = PDG.Dungeon.instance.nQChange });
        list.Add(new GameSaveData { id = "savedata_stage_level", count = 0 });
        list.Add(new GameSaveData { id = "savedata_stage_step", count = 0 });
        list.Add(new GameSaveData { id = "savedata_stage_boss", count = 0 });
        list.Add(new GameSaveData { id = "savedata_stage_rank_0", count = -1 });
        list.Add(new GameSaveData { id = "savedata_stage_rank_1", count = -1 });
        list.Add(new GameSaveData { id = "savedata_stage_rank_2", count = -1 });
        list.Add(new GameSaveData { id = "savedata_stage_rank_3", count = -1 });
        list.Add(new GameSaveData { id = "savedata_stage_boss_0", count = -1 });
        list.Add(new GameSaveData { id = "savedata_stage_boss_1", count = -1 });
        list.Add(new GameSaveData { id = "savedata_stage_boss_2", count = -1 });
        list.Add(new GameSaveData { id = "savedata_stage_boss_3", count = -1 });
        list.Add(new GameSaveData { id = "savedata_curseHealth", count = 0 });
        list.Add(new GameSaveData { id = "savedata_curseAttack", count = 0 });
        list.Add(new GameSaveData { id = "savedata_isMercenaryAjit", count = 0 });
        list.Add(new GameSaveData { id = "savedata_specialID", count = -1 });
        list.Add(new GameSaveData { id = startLeft, count = 1 });
        list.Add(new GameSaveData { id = startRight, count = 1 });
        list.Add(new GameSaveData { id = startQ, count = 1 });
        list.Add(new GameSaveData { id = "rolling", count = 1 });

        var b = new BinaryFormatter();
        var f = File.Create(dPath + "/" + m_strPath + "GameSave.Data");
        byte[] saveData = ObjectToByte(list.ToArray());
        b.Serialize(f, saveData);
        f.Close();
        return true;
    }
    public bool GameCheck()
    {
        if(File.Exists(dPath + "/" + m_strPath + "GameSave.Data"))
        {
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream stream = new FileStream(dPath + "/" + m_strPath + "GameSave.Data", FileMode.Open);
            byte[] saveDataTemp = formatter.Deserialize(stream) as byte[];
            GameSaveData[] saveDataLoad = ByteToObject(saveDataTemp) as GameSaveData[];
            stream.Close();
            bool check = false;
            for (int i = 0; i < saveDataLoad.Length; i++)
            {
                if (saveDataLoad[i].id.Equals("savedata_stage_step") || saveDataLoad[i].id.Equals("savedata_stage_step")) 
                {
                    if (saveDataLoad[i].count != 0)
                        check = true;
                }
            }
            return check;
        }
        else
        {
            return false;
        }
    }
    public void SaveMail(MailSaveData _save)
    {
        var b = new BinaryFormatter();
        List<MailSaveData> saveList = new List<MailSaveData>();
        if (File.Exists(dPath + "/" + m_strPath + "Mail.Data"))
        {
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream stream = new FileStream(dPath + "/" + m_strPath + "Mail.Data", FileMode.Open);
            byte[] saveDataTemp = formatter.Deserialize(stream) as byte[];
            MailSaveData[] saveDataLoad = ByteToObject(saveDataTemp) as MailSaveData[];
            saveList = saveDataLoad.ToList();
            bool isChange = false;
            for (int i = 0; i < saveList.Count; i++)
            {
                if (saveList[i]._id.Equals(_save._id))
                {
                    MailSaveData changeData = saveList[i];
                    changeData.date = _save.date;
                    changeData.isCheck = _save.isCheck;
                    changeData.isPush = _save.isPush;
                    changeData.isItems = _save.isItems;
                    changeData.items = _save.items;
                    saveList[i] = changeData;
                    isChange = true;
                    break;
                }
            }
            if (!isChange) saveList.Add(_save);
            stream.Close();
            byte[] saveData = ObjectToByte(saveList.ToArray());
            var f = File.Create(dPath + "/" + m_strPath + "Mail.Data");
            b.Serialize(f, saveData);
            f.Close();
        }
        else
        {
            var f = File.Create(dPath + "/" + m_strPath + "Mail.Data");
            saveList.Add(_save);
            byte[] saveData = ObjectToByte(saveList.ToArray());
            b.Serialize(f, saveData);
            f.Close();
        }
    }
    public MailSaveData LoadMail(string _id)
    {
        var b = new BinaryFormatter();
        MailSaveData returnData = new MailSaveData
        {
            _id = string.Empty
        };
        if (File.Exists(dPath + "/" + m_strPath + "Mail.Data"))
        {
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream stream = new FileStream(dPath + "/" + m_strPath + "Mail.Data", FileMode.Open);
            byte[] saveDataTemp = formatter.Deserialize(stream) as byte[];
            MailSaveData[] saveDataLoad = ByteToObject(saveDataTemp) as MailSaveData[];
            bool isChange = false;
            for (int i = 0; i < saveDataLoad.Length; i++)
            {
                if (saveDataLoad[i]._id.Equals(_id))
                {
                    returnData = saveDataLoad[i];
                    break;
                }
            }
            stream.Close();
        }
        return returnData;
    }
    public void SaveCollection(CollectionData _save)
    {
        var b = new BinaryFormatter();
        List<CollectionData> saveList = new List<CollectionData>();
        if (File.Exists(dPath + "/" + m_strPath + "Collection.Data"))
        {
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream stream = new FileStream(dPath + "/" + m_strPath + "Collection.Data", FileMode.Open);
            byte[] saveDataTemp = formatter.Deserialize(stream) as byte[];
            CollectionData[] saveDataLoad = ByteToObject(saveDataTemp) as CollectionData[];
            saveList = saveDataLoad.ToList();
            bool isChange = false;
            for (int i = 0; i < saveList.Count; i++)
            {
                if (saveList[i]._id.Equals(_save._id))
                {
                    CollectionData changeData = saveList[i];
                    changeData._isOpen = _save._isOpen;
                    changeData._isWeapon = _save._isWeapon;
                    saveList[i] = changeData;
                    isChange = true;
                    break;
                }
            }
            if (!isChange) saveList.Add(_save);
            stream.Close();
            byte[] saveData = ObjectToByte(saveList.ToArray());
            var f = File.Create(dPath + "/" + m_strPath + "Collection.Data");
            b.Serialize(f, saveData);
            f.Close();
        }
        else
        {
            var f = File.Create(dPath + "/" + m_strPath + "Collection.Data");
            saveList.Add(_save);
            byte[] saveData = ObjectToByte(saveList.ToArray());
            b.Serialize(f, saveData);
            f.Close();
        }
    }
    public CollectionData[] LoadCollection()
    {
        var b = new BinaryFormatter();
        CollectionData[] returnData = null;
        if (File.Exists(dPath + "/" + m_strPath + "Collection.Data"))
        {
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream stream = new FileStream(dPath + "/" + m_strPath + "Collection.Data", FileMode.Open);
            byte[] saveDataTemp = formatter.Deserialize(stream) as byte[];
            CollectionData[] saveDataLoad = ByteToObject(saveDataTemp) as CollectionData[];
            returnData = saveDataLoad;
            stream.Close();
        }
        return returnData;
    }
    public bool SaveKey(KeyCode _key, int _nowKeySettingNum, bool _isKeybaord)
    {
        string fileName = _isKeybaord ? "KeyBoardSetting.Data" : "KeySetting.Data";
        var b = new BinaryFormatter();

        List<KeySettingData> saveList = new List<KeySettingData>();
        if (File.Exists(dPath + "/" + m_strPath + fileName))
        {
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream stream = new FileStream(dPath + "/" + m_strPath + fileName, FileMode.Open);
            byte[] saveDataTemp = formatter.Deserialize(stream) as byte[];
            KeySettingData[] saveDataLoad = ByteToObject(saveDataTemp) as KeySettingData[];
            saveList = saveDataLoad.ToList();
            bool isChange = false;
            bool overlab = false;
            int overlabNum = -1;
            for (int i = 0; i < saveList.Count; i++)
            {
                if (saveList[i]._num.Equals(_nowKeySettingNum)) continue;
                if (saveList[i]._code.Equals((int)_key))
                {
                    overlabNum = i;
                    overlab = true;
                    break;
                }
            }
            for (int i = 0; i < saveList.Count; i++)
            {
                if (saveList[i]._num.Equals(_nowKeySettingNum))
                {
                    KeySettingData changeData = saveList[i];

                    if (overlab)
                    {
                        KeySettingData overlabData = saveList[overlabNum];
                        overlabData._code = changeData._code;
                        saveList[overlabNum] = overlabData;
                    }

                    changeData._code = (int)_key;
                    saveList[i] = changeData;
                    isChange = true;
            
                    break;
                }
            }

            if (!isChange) saveList.Add(new KeySettingData { _code = (int)_key, _num = _nowKeySettingNum });
            stream.Close();
            byte[] saveData = ObjectToByte(saveList.ToArray());
            var f = File.Create(dPath + "/" + m_strPath + fileName);
            b.Serialize(f, saveData);
            f.Close();

            FPSDisplay.instance.LoadKey(!_isKeybaord);
            return !overlab;
        }
        else
        {
            var f = File.Create(dPath + "/" + m_strPath + fileName);
            saveList.Add(new KeySettingData { _code = (int)_key, _num = _nowKeySettingNum });
            byte[] saveData = ObjectToByte(saveList.ToArray());
            b.Serialize(f, saveData);
            f.Close();
            return true;
        }
    }
    public KeySettingData[] LoadKey(bool _isKeybaord)
    {
        string fileName = _isKeybaord ? "KeyBoardSetting.Data" : "KeySetting.Data";
        var b = new BinaryFormatter();
        KeySettingData[] returnData = null;
        if (File.Exists(dPath + "/" + m_strPath + fileName))
        {
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream stream = new FileStream(dPath + "/" + m_strPath + fileName, FileMode.Open);
            byte[] saveDataTemp = formatter.Deserialize(stream) as byte[];
            KeySettingData[] saveDataLoad = ByteToObject(saveDataTemp) as KeySettingData[];
            returnData = saveDataLoad;
            stream.Close();
        }
        return returnData;
    }
    public void SaveAchievements(AchievementData[] _save, bool _isReward = false)
    {
        bool isUnlockSave = false;
        string unlockID = string.Empty;
        var b = new BinaryFormatter();
        List<AchievementData> saveList = new List<AchievementData>();
        if (File.Exists(dPath + "/" + m_strPath + "Achievements.Data"))
        {
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream stream = new FileStream(dPath + "/" + m_strPath + "Achievements.Data", FileMode.Open);
            byte[] saveDataTemp = formatter.Deserialize(stream) as byte[];
            AchievementData[] saveDataLoad = ByteToObject(saveDataTemp) as AchievementData[];
            saveList = saveDataLoad.ToList();

            for (int i = 0; i < _save.Length; i++)
            {
                bool isChange = false;
                for (int j = 0; j < saveList.Count; j++)
                {
                    if (saveList[j]._id.Equals(_save[i]._id))
                    {
                        AchievementData changeData = saveList[j];
                        changeData._cur = _save[i]._cur;
                        changeData._max = _save[i]._max;
                        if (_isReward) changeData._reward = _save[i]._reward;
                        if (changeData._cur >= changeData._max)
                        {
                            changeData._cur = changeData._max;
                            if (!changeData._clear)
                            {
                                changeData._date = DateTime.Now.ToString("yyyyMMddHHmmss");
                                AchievementsShowUI(_save[i]._id, ref changeData, ref isUnlockSave, ref unlockID);
                            }
                            changeData._clear = true;
                        }
                        else changeData._clear = false;
                        saveList[j] = changeData;
                        isChange = true;
                        break;
                    }
                }
                if (!isChange)
                {
                    AchievementData changeData = _save[i];
                    changeData._cur = _save[i]._cur;
                    changeData._max = _save[i]._max;
                    if (changeData._cur >= changeData._max)
                    {
                        changeData._cur = changeData._max;
                        if (!changeData._clear)
                        {
                            changeData._date = DateTime.Now.ToString("yyyyMMddHHmmss");
                            AchievementsShowUI(_save[i]._id, ref changeData, ref isUnlockSave, ref unlockID);
                        }
                        changeData._clear = true;
                    }
                    else changeData._clear = false;
                    _save[i] = changeData;
                    saveList.Add(_save[i]);
                }
            }
            stream.Close();
            byte[] saveData = ObjectToByte(saveList.ToArray());
            var f = File.Create(dPath + "/" + m_strPath + "Achievements.Data");
            b.Serialize(f, saveData);
            f.Close();
        }
        else
        {
            var f = File.Create(dPath + "/" + m_strPath + "Achievements.Data");
            for (int i = 0; i < _save.Length; i++)
            {
                AchievementData changeData = _save[i];
                changeData._cur = _save[i]._cur;
                changeData._max = _save[i]._max;
                if (changeData._cur >= changeData._max)
                {
                    changeData._cur = changeData._max;
                    if (!changeData._clear)
                    {
                        changeData._date = DateTime.Now.ToString("yyyyMMddHHmmss");
                        AchievementsShowUI(_save[i]._id, ref changeData, ref isUnlockSave, ref unlockID);
                    }
                    changeData._clear = true;
                }
                else changeData._clear = false;
                _save[i] = changeData;
                saveList.Add(_save[i]);
            }
            byte[] saveData = ObjectToByte(saveList.ToArray());
            b.Serialize(f, saveData);
            f.Close();
        }

        if (isUnlockSave)
        {
            SaveUnlock(unlockID, 1);
        }
    }
    public AchievementData LoadAchievements(string _id)
    {
        var b = new BinaryFormatter();
        if (File.Exists(dPath + "/" + m_strPath + "Achievements.Data"))
        {
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream stream = new FileStream(dPath + "/" + m_strPath + "Achievements.Data", FileMode.Open);
            byte[] saveDataTemp = formatter.Deserialize(stream) as byte[];
            AchievementData[] saveDataLoad = ByteToObject(saveDataTemp) as AchievementData[];
            for (int i = 0; i < saveDataLoad.Length; i++)
            {
                if (saveDataLoad[i]._id.Equals(_id))
                {
                    stream.Close();
                    return saveDataLoad[i];
                }
            }
            stream.Close();
        }
        return new AchievementData { _id = string.Empty, _clear = false, _cur = 0, _max = 0 };
    }
    public void SaveManufacture(string _id, int step, string stateId, int stateValue, bool _isRush)
    {
        var b = new BinaryFormatter();
        List<ManufacturesData> saveList = new List<ManufacturesData>();
        if (File.Exists(dPath + "/" + m_strPath + "Manufacture.Data"))
        {
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream stream = new FileStream(dPath + "/" + m_strPath + "Manufacture.Data", FileMode.Open);
            byte[] saveDataTemp = formatter.Deserialize(stream) as byte[];
            ManufacturesData[] saveDataLoad = ByteToObject(saveDataTemp) as ManufacturesData[];
            saveList = saveDataLoad.ToList();
            bool isChange = false;
            for (int i = 0; i < saveList.Count; i++)
            {
                if (saveList[i].id.Equals(_id) && saveList[i].isRush == _isRush)
                {
                    ManufacturesData changeData = saveList[i];
                    bool stateCheck = false;
                    for (int j = 0; j < changeData.stateid.Count; j++)
                    {
                        if (changeData.stateid[j].Equals(stateId))
                        {
                            changeData.statevalue[j] += stateValue;
                            changeData.statestep[j] = step;
                            stateCheck = true;
                            break;
                        }
                    }
                    if (!stateCheck)
                    {
                        changeData.stateid.Add(stateId);
                        changeData.statevalue.Add(stateValue);
                        changeData.statestep.Add(step);
                    }
                    saveList[i] = changeData;
                    isChange = true;
                    break;
                }
            }
            if (!isChange)
            {
                ManufacturesData changeData = new ManufacturesData
                {
                    id = _id,
                    isRush = _isRush,
                    stateid = new List<string>(),
                    statevalue = new List<int>(),
                    statestep = new List<int>()
                };
                changeData.stateid.Add(stateId);
                changeData.statevalue.Add(stateValue);
                changeData.statestep.Add(step);
                saveList.Add(changeData);
            }
            stream.Close();
            byte[] saveData = ObjectToByte(saveList.ToArray());
            var f = File.Create(dPath + "/" + m_strPath + "Manufacture.Data");
            b.Serialize(f, saveData);
            f.Close();
        }
        else
        {
            var f = File.Create(dPath + "/" + m_strPath + "Manufacture.Data");
            ManufacturesData changeData = new ManufacturesData
            {
                id = _id,
                stateid = new List<string>(),
                statevalue = new List<int>(),
                statestep = new List<int>()
            };
            changeData.stateid.Add(stateId);
            changeData.statevalue.Add(stateValue);
            changeData.statestep.Add(step);
            saveList.Add(changeData);
            byte[] saveData = ObjectToByte(saveList.ToArray());
            b.Serialize(f, saveData);
            f.Close();
        }
    }
    public ManufacturesData LoadManufacture(string _id, bool _isRush)
    {
        var b = new BinaryFormatter();
        if (File.Exists(dPath + "/" + m_strPath + "Manufacture.Data"))
        {
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream stream = new FileStream(dPath + "/" + m_strPath + "Manufacture.Data", FileMode.Open);
            byte[] saveDataTemp = formatter.Deserialize(stream) as byte[];
            ManufacturesData[] saveDataLoad = ByteToObject(saveDataTemp) as ManufacturesData[];
            for (int i = 0; i < saveDataLoad.Length; i++)
            {
                if (saveDataLoad[i].id.Equals(_id) && saveDataLoad[i].isRush == _isRush)
                {
                    stream.Close();
                    return saveDataLoad[i];
                }
            }
            stream.Close();
        }
        return new ManufacturesData { id = string.Empty };
    }
    public void DeleteManufacture(int nRush)
    {
        var b = new BinaryFormatter();
        List<ManufacturesData> saveList = new List<ManufacturesData>();
        if (File.Exists(dPath + "/" + m_strPath + "Manufacture.Data"))
        {
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream stream = new FileStream(dPath + "/" + m_strPath + "Manufacture.Data", FileMode.Open);
            byte[] saveDataTemp = formatter.Deserialize(stream) as byte[];
            ManufacturesData[] saveDataLoad = ByteToObject(saveDataTemp) as ManufacturesData[];
            saveList = saveDataLoad.ToList();
            for (int i = 0; i < saveList.Count; i++)
            {
                if (nRush == 0)
                {
                    if (!saveList[i].isRush)
                    {
                        saveList.RemoveAt(i);
                        i--;
                        continue;
                    }
                }
                else if (nRush == 1)
                {
                    if (saveList[i].isRush)
                    {
                        saveList.RemoveAt(i);
                        i--;
                        continue;
                    }
                }
                else
                {
                    saveList.RemoveAt(i);
                    i--;
                    continue;
                }
            }
            stream.Close();
            byte[] saveData = ObjectToByte(saveList.ToArray());
            var f = File.Create(dPath + "/" + m_strPath + "Manufacture.Data");
            b.Serialize(f, saveData);
            f.Close();
        }
    }
    public void BossRushSave(string wName, int t)
    {
        var b = new BinaryFormatter();
        List<RushData> saveList = new List<RushData>();
        if (File.Exists(dPath + "/" + m_strPath + "Rush.Data"))
        {
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream stream = new FileStream(dPath + "/" + m_strPath + "Rush.Data", FileMode.Open);
            byte[] saveDataTemp = formatter.Deserialize(stream) as byte[];
            RushData[] saveDataLoad = ByteToObject(saveDataTemp) as RushData[];
            saveList = saveDataLoad.ToList();

            RushData changeData = new RushData
            {
                diff = nDifficulty,
                time = PDG.Dungeon.instance.fBossRushTime,
                date = DateTime.Now.ToString(("yyyyMMddHHmmss")),
                usesell = t,
                weapon = wName
            };
            saveList.Add(changeData);
            stream.Close();
            byte[] saveData = ObjectToByte(saveList.ToArray());
            var f = File.Create(dPath + "/" + m_strPath + "Rush.Data");
            b.Serialize(f, saveData);
            f.Close();
        }
        else
        {
            var f = File.Create(dPath + "/" + m_strPath + "Rush.Data");
            RushData changeData = new RushData
            {
                diff = nDifficulty,
                time = PDG.Dungeon.instance.fBossRushTime,
                date = DateTime.Now.ToString(("yyyyMMddHHmmss")),
                usesell = t,
                weapon = wName
            };
            saveList.Add(changeData);
            byte[] saveData = ObjectToByte(saveList.ToArray());
            b.Serialize(f, saveData);
            f.Close();
        }
    }
    public RushData[] LoadRushData()
    {
        var b = new BinaryFormatter();
        if (File.Exists(dPath + "/" + m_strPath + "Rush.Data"))
        {
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream stream = new FileStream(dPath + "/" + m_strPath + "Rush.Data", FileMode.Open);
            byte[] saveDataTemp = formatter.Deserialize(stream) as byte[];
            RushData[] saveDataLoad = ByteToObject(saveDataTemp) as RushData[];
            stream.Close();
            return saveDataLoad;
            stream.Close();
        }
        return null;
    }
    private void AchievementsShowUI(string _id, ref AchievementData changeData, ref bool isUnlockSave, ref string _unlockID)
    {
        Inventorys.Instance.ShowGetItemUI("unlock_achievements", Resources.Load<Sprite>("Achievements/" + _id), true, _id);
        if (_id.Equals("indomitablewill_0"))
        {
            Inventorys.Instance.ShowGetItemUI("unlock_mercenary", Resources.Load<Sprite>("Mercenary/fighter/fighter"));
            changeData._reward = true;
        }
        else if (_id.Equals("hunter_subboss_0"))
        {
            Inventorys.Instance.ShowGetItemUI("unlock_goblin", Resources.Load<Sprite>("Mob/goblin/goblin"), true);
            changeData._reward = true;
        }
        else if (_id.Equals("endlesschallenges_0"))
        {
            Inventorys.Instance.ShowGetItemUI("unlock_goblin", Resources.Load<Sprite>("Mob/goblin/1_goblin_idle_0"), true);
            changeData._reward = true;
        }
        else if (_id.Equals("endlesschallenges_2"))
        {
            Inventorys.Instance.ShowGetItemUI("ui_reward_popup", Resources.Load<Sprite>("Item/Passive/Buff/spectroscope"), true, "spectroscope");
            _unlockID = "spectroscope";
            isUnlockSave = true;
            changeData._reward = true;
        }
        else if (_id.Equals("endlesschallenges_3"))
        {
            Inventorys.Instance.ShowGetItemUI("ui_reward_popup", Resources.Load<Sprite>("Item/Passive/Buff/prism"), true, "prism");
            _unlockID = "prism";
            isUnlockSave = true;
            changeData._reward = true;
        }
        else if (_id.Equals("indomitablewill_1"))
        {
            Inventorys.Instance.ShowGetItemUI("ui_reward_popup", Resources.Load<Sprite>("Item/Passive/Buff/attack_r_manual"), true, "attack_r_manual");
            _unlockID = "attack_r_manual";
            isUnlockSave = true;
            changeData._reward = true;
        }
        else if (_id.Equals("indomitablewill_2"))
        {
            Inventorys.Instance.ShowGetItemUI("ui_reward_popup", Resources.Load<Sprite>("Item/Passive/Buff/attack_m_manual"), true, "attack_m_manual");
            _unlockID = "attack_m_manual";
            isUnlockSave = true;
            changeData._reward = true;
        }
        else if (_id.Equals("godcontrol"))
        {
            Inventorys.Instance.ShowGetItemUI("ui_reward_popup", Resources.Load<Sprite>("Item/Weapon/Arm/multi_canon"), true, "multi_canon");
            _unlockID = "multi_canon";
            isUnlockSave = true;
            changeData._reward = true;
        }
        else if (_id.Equals("serene"))
        {
            Inventorys.Instance.ShowGetItemUI("ui_reward_popup", Resources.Load<Sprite>("Item/Weapon/Gun/lasergun_recycle"), true, "lasergun_recycle");
            _unlockID = "lasergun_recycle";
            isUnlockSave = true;
            changeData._reward = true;
        }
        else if (_id.Equals("nodamageclear"))
        {
            Inventorys.Instance.ShowGetItemUI("ui_reward_popup", Resources.Load<Sprite>("Item/Weapon/Thorn/deep_impact"), true, "deep_impact");
            _unlockID = "deep_impact";
            isUnlockSave = true;
            changeData._reward = true;
        }
        else if (_id.Equals("tantaclemaster"))
        {
            Inventorys.Instance.ShowGetItemUI("ui_reward_popup", Resources.Load<Sprite>("Item/Weapon/Arm/energy_release"), true, "energy_release");
            _unlockID = "energy_release";
            isUnlockSave = true;
            changeData._reward = true;
        }
        else if (_id.Equals("huntermaster_golem"))
        {
            Inventorys.Instance.ShowGetItemUI("ui_reward_popup", Resources.Load<Sprite>("Item/Weapon/Sword/fire_blade"), true, "fire_blade");
            _unlockID = "fire_blade";
            isUnlockSave = true;
            changeData._reward = true;
        }
        else if (_id.Equals("huntermaster_npc"))
        {
            Inventorys.Instance.ShowGetItemUI("ui_reward_popup", Resources.Load<Sprite>("Item/Weapon/Arm/living_arrow"), true, "living_arrow");
            _unlockID = "living_arrow";
            isUnlockSave = true;
            changeData._reward = true;
        }
        else if (_id.Equals("huntermaster_stone"))
        {
            Inventorys.Instance.ShowGetItemUI("ui_reward_popup", Resources.Load<Sprite>("Item/Weapon/Gun/rocket_punch"), true, "rocket_punch");
            _unlockID = "rocket_punch";
            isUnlockSave = true;
            changeData._reward = true;
        }
        else if (_id.Equals("huntermaster_stg"))
        {
            Inventorys.Instance.ShowGetItemUI("ui_reward_popup", Resources.Load<Sprite>("Item/Weapon/Arm/frame_nova"), true, "frame_nova");
            _unlockID = "frame_nova";
            isUnlockSave = true;
            changeData._reward = true;
        }
        else if (_id.Equals("hunter_subboss_1"))
        {
            Inventorys.Instance.ShowGetItemUI("ui_reward_popup", Resources.Load<Sprite>("Item/Weapon/Arm/frozen_nova"), true, "frozen_nova");
            _unlockID = "frozen_nova";
            isUnlockSave = true;
            changeData._reward = true;
        }
        else if (_id.Equals("stage_clear_1"))
        {
            Inventorys.Instance.ShowGetItemUI("ui_reward_popup", Resources.Load<Sprite>("Item/Passive/Buff/amplifier_5"), true, "amplifier_5");
            _unlockID = "frozen_nova";
            isUnlockSave = true;
            changeData._reward = true;
        }
        else if (_id.Equals("stage_clear_2"))
        {
            Inventorys.Instance.ShowGetItemUI("ui_reward_popup", Resources.Load<Sprite>("Item/Passive/Buff/auxiliary_aura_5"), true, "auxiliary_aura_5");
            _unlockID = "frozen_nova";
            isUnlockSave = true;
            changeData._reward = true;
        }
        else if (_id.Equals("stage_clear_3"))
        {
            Inventorys.Instance.ShowGetItemUI("get_helmet", Resources.Load<Sprite>("Head/" + _id + "/" + _id), true, "");
            _unlockID = _id + "_Custom";
            isUnlockSave = true;
            changeData._reward = true;
        }
        else
        {
            changeData._reward = false;
        }
    }

    public void MapSave()
    {
        GameObject pMapObj = GameObject.Find("Dungeon");
        var b = new BinaryFormatter();
        List<SaveMap> saveList = new List<SaveMap>();
        var f = File.Create(dPath + "/" + m_strPath + "MapData_" + PDG.Dungeon.instance.stageRan[0] + "_0.txt");
        for (int i = 0; i < pMapObj.transform.childCount; i++)
        {
            string name = pMapObj.transform.GetChild(i).name;
            if (name.Contains("(Clone)"))
            {
                name = name.Substring(0, name.Length - 7);
            }
            if (name.Contains("d01_")) continue;
            if (name.Contains("exit")) continue;
            SaveMap data = new SaveMap
            {
                id = name,
                x = Mathf.FloorToInt(pMapObj.transform.GetChild(i).position.x),
                y = Mathf.FloorToInt(pMapObj.transform.GetChild(i).position.y)
            };
            saveList.Add(data);
        }
        byte[] saveData = ObjectToByte(saveList.ToArray());
        b.Serialize(f, saveData);
        f.Close();
    }
    public SaveMap[] LoadMap(int step)
    {
        var b = new BinaryFormatter();
        if (File.Exists(dPath + "/" + m_strPath + "MapData_" + step + "_0.Data"))
        {
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream stream = new FileStream(dPath + "/" + m_strPath + "MapData_" + step + "_0.Data", FileMode.Open);
            byte[] saveDataTemp = formatter.Deserialize(stream) as byte[];
            SaveMap[] saveDataLoad = ByteToObject(saveDataTemp) as SaveMap[];
            stream.Close();
            return saveDataLoad;
        }
        return null;
    }
    #region -- Byte to Obj And Obj to Byte --
    public byte[] ObjectToByte(object obj)
    {
        try
        {
            using (MemoryStream stream = new MemoryStream())
            {
                BinaryFormatter binaryFormatter = new BinaryFormatter();
                binaryFormatter.Serialize(stream, obj);
                return stream.ToArray();
            }
        }
        catch (Exception e)
        {
            Debug.LogError("아니 왜 직렬화가 !? O->B  = " + e.Message);
        }
        return null;

    }
    public object ByteToObject(byte[] buffer)
    {
        try
        {
            using (MemoryStream stream = new MemoryStream(buffer))
            {
                BinaryFormatter binaryFormatter = new BinaryFormatter();
                stream.Position = 0;
                return binaryFormatter.Deserialize(stream);
            }
        }
        catch (Exception e)
        {
            Debug.LogError("아니 왜 직렬화가 !? B->O = " + e.Message);
        }
        return null;
    }
    #endregion
    public static int GetBoxGrade(ChestPerData _data)
    {
        float[] diffPer = null;
        if (nDifficulty == 0) diffPer = _data.Difficultyeasyx;
        else if (nDifficulty == 1) diffPer = _data.Difficultynormalx;
        else if (nDifficulty == 2) diffPer = _data.Difficultyhard;
        else diffPer = _data.Difficultyhellx;
        float[] percent = _data.Chestprobability;
        int[] per = new int[percent.Length];
        for (int i = 0; i < percent.Length; i++)
        {
            float aa = percent[i] * diffPer[i];
            per[i] = Mathf.FloorToInt(aa * 1000);
        }

        List<int> perTotal = new List<int>();
        for (int i = 0; i < per.Length; i++)
        {
            for (int j = 0; j < per[i]; j++)
            {
                perTotal.Add(i);
            }
        }
        int a = UnityEngine.Random.Range(0, perTotal.Count);
        return perTotal[a];
    }
}
