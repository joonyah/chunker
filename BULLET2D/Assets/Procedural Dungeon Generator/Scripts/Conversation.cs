﻿using MOB;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class Conversation : MonoBehaviour
{
    public static Conversation Instance;

    [SerializeField] private GameObject UpPanel;
    [SerializeField] private GameObject DownPanel;
    [SerializeField] private Text NameText;
    [SerializeField] private Text NameText2;
    Coroutine EffectCoroutineStart = null;
    Coroutine EffectCoroutineEnd = null;

    public bool isTargeting = false;
    public Transform targetTransform;
    public PDG.Camera mCamera;
    public Transform targetOrigin = null;

    [Header("BOSS")]
    private bool isBoss = false;
    private GameObject bossObj = null;
    public GameObject MinimapObj;

    private void Awake()
    {
        if (Instance == null) Instance = this;
    }
    private void Start()
    {
        if (mCamera == null) mCamera = FindObjectOfType(typeof(PDG.Camera)) as PDG.Camera;
    }
    private void Update()
    {
    }
    public void StartEffect()
    {
        if (EffectCoroutineStart == null) EffectCoroutineStart = StartCoroutine(EffectStart());
        if (isTargeting)
        {
            targetOrigin = mCamera.target;
            mCamera.target = targetTransform;
        }
        isBoss = false;
    }
    public void StartEffect(Transform _obj, string _name, string _subName, bool _isBoss, bool _isTargeting, GameObject _bossObj)
    {
        if (EffectCoroutineStart == null) EffectCoroutineStart = StartCoroutine(EffectStart());
        if (_isTargeting)
        {
            isTargeting = _isTargeting;
            targetOrigin = mCamera.target;
            mCamera.target = _obj;
            NameText.text = _name;
            NameText2.text = _subName;
        }
        isBoss = _isBoss;
        if (isBoss)
        {
            PDG.Player.instance.isCameraOff = true;
            bossObj = _bossObj;
        }
    }
    public void EndEffect()
    {
        if (EffectCoroutineEnd == null) EffectCoroutineEnd = StartCoroutine(EffectEnd());
    }
    IEnumerator EffectStart()
    {
        GameObject.Find("Canvas").transform.Find("MiniMap_OutLine").gameObject.SetActive(false);
        GameObject.Find("Canvas").transform.Find("UI").gameObject.SetActive(false);
        while (true)
        {
            RectTransform rt1 = UpPanel.GetComponent<RectTransform>();
            RectTransform rt2 = DownPanel.GetComponent<RectTransform>();
            if (rt1.anchoredPosition3D.y > 0) rt1.anchoredPosition3D = new Vector3(rt1.anchoredPosition3D.x, rt1.anchoredPosition.y - 4, 0);
            if (rt2.anchoredPosition3D.y < 0) rt2.anchoredPosition3D = new Vector3(rt2.anchoredPosition3D.x, rt2.anchoredPosition.y + 4, 0);
            if (rt1.anchoredPosition3D.y <= 0 && rt2.anchoredPosition3D.y >= 0) break;
            yield return new WaitForSeconds(0.01f);
        }
        if (isBoss)
        {
            float cameraZoom = 7;
            while(true)
            {
                mCamera.GetComponent<UnityEngine.Camera>().orthographicSize = cameraZoom;
                cameraZoom -= 0.1f;
                if (cameraZoom <= 5.0f) break;
                yield return new WaitForSeconds(0.01f);
            }
            mCamera.GetComponent<UnityEngine.Camera>().orthographicSize = 5;
            NameText.gameObject.SetActive(true);
            NameText2.gameObject.SetActive(true);
            if (mCamera.target.GetComponent<MOB.BossMonster_Six_Apear>()) mCamera.target.GetComponent<MOB.BossMonster_Six_Apear>().StartApear();
            yield return new WaitForSeconds(1.0f);
            yield return StartCoroutine(NameOpen());
            yield return new WaitForSeconds(0.5f);
            yield return StartCoroutine(NameClose());
        }
        else
        {
            yield return new WaitForSeconds(1.0f);
        }
        NameText.gameObject.SetActive(false);
        NameText2.gameObject.SetActive(false);
        //yield return new WaitForSeconds(2.0f);
        if (isTargeting)
        {
            mCamera.target = targetOrigin;
            mCamera.GetComponent<UnityEngine.Camera>().orthographicSize = 7;
            //yield return new WaitForSeconds(1.0f);
            isTargeting = false;
        }
        EffectCoroutineStart = null;
        EndEffect();
    }
    IEnumerator NameOpen()
    {
        float a = 0;
        while (true)
        {
            NameText.color = new Color(1, 1, 1, a);
            a += 0.04f;
            if (a >= 1) break;
            yield return new WaitForSeconds(0.01f);
        }
        NameText.color = Color.white;
        a = 0;
        yield return new WaitForSeconds(0.2f);
        while (true)
        {
            NameText2.color = new Color(1, 1, 1, a);
            a += 0.04f;
            if (a >= 1) break;
            yield return new WaitForSeconds(0.01f);
        }
        NameText2.color = Color.white;
    }
    IEnumerator NameClose()
    {
        float a = 1;
        while (true)
        {
            NameText.color = new Color(1, 1, 1, a);
            NameText2.color = new Color(1, 1, 1, a);
            a -= 0.05f;
            if (a <= 0) break;
            yield return new WaitForSeconds(0.01f);
        }
        NameText.color = new Color(1, 1, 1, 0);
        NameText2.color = new Color(1, 1, 1, 0);
    }
    IEnumerator EffectEnd()
    {
        while (true)
        {
            RectTransform rt1 = UpPanel.GetComponent<RectTransform>();
            RectTransform rt2 = DownPanel.GetComponent<RectTransform>();
            if (rt1.anchoredPosition3D.y < 160) rt1.anchoredPosition3D = new Vector3(rt1.anchoredPosition3D.x, rt1.anchoredPosition.y + 4, 0);
            if (rt2.anchoredPosition3D.y > -160) rt2.anchoredPosition3D = new Vector3(rt2.anchoredPosition3D.x, rt2.anchoredPosition.y - 4, 0);
            if (rt1.anchoredPosition3D.y >= 160 && rt2.anchoredPosition3D.y <= -160) break;
            yield return new WaitForSeconds(0.01f);
        }
        EffectCoroutineEnd = null;
        MinimapObj.gameObject.SetActive(true);
        GameObject.Find("Canvas").transform.Find("UI").gameObject.SetActive(true);
        if (isBoss)
        {
            PDG.Player.instance.isCameraOff = false;
            if (bossObj.GetComponent<BossMonster_Six_Apear>()) bossObj.GetComponent<BossMonster_Six_Apear>().BossStart();
            if (bossObj.GetComponent<MonsterArggro>()) bossObj.GetComponent<MonsterArggro>().isWait = false;
        }
    }

}
