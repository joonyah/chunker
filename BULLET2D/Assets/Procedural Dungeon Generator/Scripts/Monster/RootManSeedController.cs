﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RootManSeedController : MonoBehaviour
{
    [SerializeField] private Vector3 offSet;
    [SerializeField] private Vector3 vSize;

    [SerializeField] private bool isActiveTrap = false;
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireCube(transform.position + offSet, vSize);
    }

    private void FixedUpdate()
    {
        RaycastHit2D hit = Physics2D.BoxCast(transform.position + offSet, vSize, 0, Vector2.zero, 0, 1 << 9);

        if (hit && !isActiveTrap)
        {
            GetComponent<Animator>().SetTrigger("Trap");
            isActiveTrap = true;
            StartCoroutine(ActiveTrap());
        }
    }

    IEnumerator ActiveTrap()
    {
        Vector3 pos = transform.position;
        pos.y -= 0.5f;
        PDG.Player.instance.transform.position = pos;
        PDG.Player.instance.isLock = true;
        float t = 0.0f;
        while(true)
        {
            t += Time.fixedDeltaTime;
            if (t >= 5.0f) break;
            yield return new WaitForFixedUpdate();
        }
        GetComponent<Animator>().SetTrigger("TrapEnd");
        PDG.Player.instance.isLock = false;
        int x = Mathf.FloorToInt(transform.position.x);
        int y = Mathf.FloorToInt(transform.position.y);
        PDG.Dungeon.instance.board.map[x, y] = null;
        yield return new WaitForSeconds(0.25f);
        Destroy(gameObject);
    }
}
