﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FragmentsController : MonoBehaviour
{
    public float time;

    public bool isStart = false;


    private void Start()
    {
        time = Random.Range(0.5f, 1);
    }

    private void Update()
    {
        if(isStart)
        {
            time -= Time.deltaTime;

            if (time <= 0.0f) 
            {
                isStart = false;
                GetComponent<Rigidbody2D>().velocity = Vector2.zero;
            }
        }
    }
}
