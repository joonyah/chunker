﻿using PDG;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class Boss_STG : MonoBehaviour
{
    public MonstersData mobData;
    public MOB.MonsterInfo mobInfo;

    [SerializeField] private Animator animator;
    public bool isIntroStart = false;
    [SerializeField] private GameObject[] BombTiles;
    int nowTile = 0;
    bool isPatternStart = false;
    [SerializeField] private AudioSource _audio;
    [SerializeField] private AudioClip _FootworkClip;
    [SerializeField] private AudioClip _BombClip;
    [SerializeField] private float fWheelSpeed = 50.0f;
    [SerializeField] private GameObject wheelwindAttackCollision;
    [SerializeField] private SpriteRenderer sr;
    [SerializeField] private GameObject nextTarget;

    [SerializeField] private UbhRandomShot ranShot;
    [SerializeField] private UbhShotCtrl ranShotCtl;

    [SerializeField] private UbhShotCtrl[] pattern1Ctls;
    [SerializeField] private UbhNwayShot accelTurnShot;
    [SerializeField] private UbhNwayShot trurnShoot;
    [SerializeField] private UbhHomingShot homingShot;
    [SerializeField] private string[] bulletNameArray;

    [Header("ArmPiece")]
    [SerializeField] private float angleRange = 20;
    [SerializeField] private GameObject armObj;
    [SerializeField] private GameObject[] armPieces;
    [SerializeField] private float armDistance;
    public bool isArm;
    bool isSwingAttack = false;
    Coroutine ArmCoroutine = null;
    public bool isBackgroundAttack;

    bool isDie = false;
    public bool isHit = false;

    [Header("Color")]
    public Color HitColor = new Color();
    public Color HitColor2 = new Color();
    public Color HitColor3 = new Color();
    public Color HitColor4 = new Color();
    private Coroutine HitCoroutine = null;
    float hitTime = 0.0f;
    bool isInvincibility = true;
    [SerializeField] private Vector3 dropPosition;
    [SerializeField] private Sprite lastSprite;
    [SerializeField] private GameObject[] nextHole;
    [SerializeField] private BossItemBoxController[] ItemBoxs;

    [SerializeField] private GameObject dieEffect_0;
    [SerializeField] private GameObject dieEffect_1;

    [SerializeField] private GameObject JumpAtferEffect;
    [SerializeField] private GameObject BulletObj;
    [SerializeField] private GameObject bulletEffectP;

    [SerializeField] private UbhShotCtrl tCtl;
    [SerializeField] private UbhShotCtrl tCtl2;
    // Start is called before the first frame update
    void Start()
    {
        sr = GetComponent<SpriteRenderer>();
        StageData sData = System.Array.Find(SheetManager.Instance.StageDB.dataArray, item => item.Roomid[0].Equals("d01_bossstage_01"));
        mobData = System.Array.Find(SheetManager.Instance.MonsterDB.dataArray, item => item.ID.Equals("boss_stg"));
        mobInfo = GetComponent<MOB.MonsterInfo>();
        if (PDG.Dungeon.instance.StageLevel == 0)
            mobInfo.maxHp = mobInfo.curHp = Random.Range(sData.Health[0], sData.Health[1]);
        else if (PDG.Dungeon.instance.StageLevel == 1)
            mobInfo.maxHp = mobInfo.curHp = Random.Range(sData.Health[0], sData.Health[1]) * 2;
        else if (PDG.Dungeon.instance.StageLevel == 2)
            mobInfo.maxHp = mobInfo.curHp = Random.Range(sData.Health[0], sData.Health[1]) * 3;
        else if (PDG.Dungeon.instance.StageLevel == 3)
            mobInfo.maxHp = mobInfo.curHp = Random.Range(sData.Health[0], sData.Health[1]) * 5;

        float hp = mobInfo.curHp;
        if (SaveAndLoadManager.nDifficulty == 0) mobInfo.maxHp = mobInfo.curHp = (hp / 2);
        else if (SaveAndLoadManager.nDifficulty == 1) mobInfo.maxHp = mobInfo.curHp = hp * 0.8f;
        else if (SaveAndLoadManager.nDifficulty == 2) mobInfo.maxHp = mobInfo.curHp = (hp * 2);
        else if (SaveAndLoadManager.nDifficulty == 3) mobInfo.maxHp = mobInfo.curHp = (hp * 4);
    }
    // Update is called once per frame
    void Update()
    {
        if (isDie) return;
        if (isIntroStart)
        {
            animator.SetTrigger("intro");
            animator.SetBool("introEnd", true);
            isIntroStart = false;
            StartCoroutine(Intro());
        }
        if (hitTime > 0)
        {
            hitTime -= Time.deltaTime;
        }
        else
        {
            isHit = false;
        }
    }
    private void LateUpdate()
    {
        GetComponent<SpriteOutline>().Regenerate();
    }
    IEnumerator Intro()
    {
        for (int i = 0; i < 6; i++)
        {
            SoundManager.instance.StartAudio(new string[1] { "Boss/stg_sword_swing_" + Random.Range(0, 3) }, VOLUME_TYPE.EFFECT);
            GameObject obj4 = ObjManager.Call().GetObject("Hit_sprite12");
            Vector3 pos4 = transform.position;
            obj4.transform.position = pos4;
            obj4.transform.localScale = Vector3.one;
            obj4.GetComponent<SwordHitEffectController>()._effctName = "PikoJumpDown2";
            obj4.GetComponent<SwordHitEffectController>().animSpeed = 1.0f;
            obj4.GetComponent<Animator>().Rebind();
            obj4.SetActive(true);
            yield return new WaitForSeconds(0.25f);
        }
        animator.SetBool("introEnd", false);
        isPatternStart = true;
        PDG.Player.instance.isCameraOff = false;
        (FindObjectOfType(typeof(PDG.Camera)) as PDG.Camera).target = PDG.Player.instance.transform;
        SoundManager.instance.StartAudio(new string[1] { "Boss/stg_start_bgm2" }, VOLUME_TYPE.BGM, null, true);
        StartCoroutine(Pattern());
    }
    public void Foot()
    {
        CameraShaker._instance.StartShake(0.2f, Time.deltaTime, 0.3f, true);
        GameObject obj4 = ObjManager.Call().GetObject("Hit_sprite12");
        Vector3 pos4 = transform.position;
        obj4.transform.position = pos4;
        obj4.transform.localScale = Vector3.one;
        obj4.GetComponent<SwordHitEffectController>()._effctName = "PikoJumpDown2";
        obj4.GetComponent<SwordHitEffectController>().animSpeed = 0.2f;
        obj4.GetComponent<Animator>().Rebind();
        obj4.SetActive(true);
        _audio.volume =  (SoundManager.isMute || (PDG.Dungeon.instance == null ? false : PDG.Dungeon.instance.isPause) || (PDG.Player.instance == null ? false : PDG.Player.instance.isDie)) ? 0 : SoundManager.EffectVolume;
        _audio.PlayOneShot(_FootworkClip);

        if (Dungeon.instance.StageLevel > 0)
        {
            ranShot.m_bulletNum = 100;
            ranShot.m_randomSpeedMin = 5;
            ranShot.m_randomDelayMax = 7;
            ranShot.m_randomDelayMin = 0;
            ranShot.m_randomDelayMax = 0;
            ranShotCtl._mobName = "Chunker_STG";
            ranShotCtl._bulletName = bulletNameArray[1];
            ranShotCtl.StartShotRoutine();
        }
    }
    public void TileBomb(int t)
    {
        StartCoroutine(TileBombCoroutine(t));
    }
    IEnumerator TileBombCoroutine(int type)
    {
        if (type == 1)
        {
            if (nowTile == 0) nowTile = 1;
            else nowTile = 0;
        }
        else
            nowTile = Random.Range(0, 2);
        int t = nowTile;
        int tt = nowTile;
        for (int i = 0; i < BombTiles.Length; i++)
        {
            if (i % 60 == 0)
            {
                t++;
                if (t > 1) t = 0;
            }
            if (i % 2 == t)
            {
                BombTiles[i].SetActive(true);
            }
        }
        yield return new WaitForSeconds(1.16f);
        _audio.volume =  (SoundManager.isMute || (PDG.Dungeon.instance == null ? false : PDG.Dungeon.instance.isPause) || (PDG.Player.instance == null ? false : PDG.Player.instance.isDie)) ? 0 : SoundManager.EffectVolume;
        _audio.PlayOneShot(_BombClip);
        yield return new WaitForSeconds(0.74f);
        t = tt;
        for (int i = 0; i < BombTiles.Length; i++)
        {
            if (i % 60 == 0)
            {
                t++;
                if (t > 1) t = 0;
            }
            if (i % 2 == t)
            {
                BombTiles[i].SetActive(false);
            }
        }
    }
    IEnumerator Pattern()
    {
        string tId = mobData.ID;
        if (tId.Contains("_t")) tId = tId.Substring(0, tId.Length - 3);
        BossHpController.instance.BossStart(LocalizeManager.GetLocalize("monster_" + tId), GetComponent<MOB.MonsterInfo>());
        isInvincibility = false;
        StateGroup.instance.isBattleTimeStart = true;
        int ranPattern = Random.Range(0, 4);
        int fixedPattern = -1;
        float pattern_time_0 = 4.0f;
        float pattern_time_0_1 = 1.5f;
        int pattern_count_0 = 0;
        float pattern_sound_0 = 0.2f;
        float pattern_time_2 = 8;
        float pattern_time_0_0 = 1.0f;
        float pattern_time_2_0 = 1.0f;
        while (!isDie)
        {
            if (fixedPattern == -1)
            {
                if (ranPattern == 0)
                {
                    pattern_time_0 -= Time.deltaTime;
                    if (pattern_time_0 <= 0.0f)
                    {
                        if (wheelwindAttackCollision.activeSelf) wheelwindAttackCollision.SetActive(false);
                        if (animator.GetBool("iswheelwind")) animator.SetBool("iswheelwind", false);
                        if (!animator.GetBool("move")) animator.SetBool("move", true);

                        pattern_time_0_1 -= Time.deltaTime;
                        if (pattern_time_0_1 <= 0.0f)
                        {
                            pattern_count_0++;
                            pattern_time_0 = 4.0f;
                            pattern_time_0_1 = 1.5f;

                            if (pattern_count_0 > 1)
                            {
                                if (animator.GetBool("move")) animator.SetBool("move", false);
                                yield return StartCoroutine(CenterJump());
                                pattern_count_0 = 0;
                                fixedPattern = 0;
                            }
                        }

                        pattern_time_0_0 -= Time.deltaTime;
                        if (Vector3.Distance(Player.instance.transform.position, transform.position) > 5 && pattern_time_0_0 <= 0.0f)
                        {
                            pattern_time_0_0 = 1.0f;
                            ranShot.m_bulletNum = 30;
                            ranShot.m_randomSpeedMin = 5;
                            ranShot.m_randomDelayMax = 7;
                            ranShot.m_randomDelayMin = 0;
                            ranShot.m_randomDelayMax = 0;
                            ranShotCtl._mobName = "Chunker_STG";
                            ranShotCtl._bulletName = bulletNameArray[1];
                            ranShotCtl.StartShotRoutine();
                        }
                        Vector3 dir = PDG.Player.instance.transform.position - transform.position;
                        dir.Normalize();
                        if (dir.x > 0)
                        {
                            sr.flipX = true;
                        }
                        else
                        {
                            sr.flipX = false;
                        }

                        RaycastHit2D hit = Physics2D.BoxCast(transform.position, new Vector2(1.5f, 2.5f), 0, new Vector2(0, -0.75f), 0, 1 << 0);
                        if (!hit) transform.Translate(dir * (Dungeon.instance.StageLevel > 1 ? fWheelSpeed * 1.5f : fWheelSpeed / 2) * Time.deltaTime);
                    }
                    else
                    {
                        pattern_time_0_0 -= Time.deltaTime;
                        if (pattern_time_0_0 <= 0.0f)
                        {
                            pattern_time_0_0 = 1.0f;
                            ranShot.m_bulletNum = 30;
                            ranShot.m_randomSpeedMin = 5;
                            ranShot.m_randomDelayMax = 7;
                            ranShot.m_randomDelayMin = 0;
                            ranShot.m_randomDelayMax = 0;
                            ranShotCtl._mobName = "Chunker_STG";
                            ranShotCtl._bulletName = bulletNameArray[1];
                            ranShotCtl.StartShotRoutine();
                        }
                        pattern_sound_0 -= Time.deltaTime;
                        if (pattern_sound_0 <= 0.0f)
                        {
                            pattern_sound_0 = 0.2f;
                            SoundManager.instance.StartAudio(new string[1] { "Boss/Small_Positve_Whoosh_0" + Random.Range(1, 5) }, VOLUME_TYPE.EFFECT);
                        }

                        if (!wheelwindAttackCollision.activeSelf) wheelwindAttackCollision.SetActive(true);
                        if (animator.GetBool("move")) animator.SetBool("move", false);
                        if (!animator.GetBool("iswheelwind")) animator.SetBool("iswheelwind", true);
                        sr.flipX = false;
                        Vector3 dir = PDG.Player.instance.transform.position - transform.position;
                        dir.Normalize();
                        RaycastHit2D hit = Physics2D.BoxCast(transform.position, new Vector2(1.5f, 2.5f), 0, new Vector2(0, -0.75f), 0, 1 << 0);
                        if (!hit) transform.Translate(dir * fWheelSpeed * Time.deltaTime);
                    }
                }
                else if (ranPattern == 1)
                {
                    if (!animator.GetBool("aura")) animator.SetBool("aura", true);
                    if (!bulletEffectP.activeSelf) bulletEffectP.SetActive(true);
                    for (int i = 0; i < 5; i++)
                    {
                        if (isDie) break;
                        int ran = Random.Range(0, pattern1Ctls.Length);
                        int ran2 = 0;
                        while (true)
                        {
                            ran2 = Random.Range(0, pattern1Ctls.Length);
                            if (ran != ran2) break;
                        }
                        pattern1Ctls[ran]._mobName = "Chunker_STG";
                        pattern1Ctls[ran]._bulletName = (ran == 2 ? bulletNameArray[0] : (ran == 1 ? bulletNameArray[2] : bulletNameArray[1]));
                        SoundManager.instance.StartAudio(new string[1] { "Boss/bossShot" }, VOLUME_TYPE.EFFECT);
                        pattern1Ctls[ran].StartShotRoutine();
                        pattern1Ctls[ran2]._mobName = "Chunker_STG";
                        pattern1Ctls[ran2]._bulletName = (ran2 == 2 ? bulletNameArray[0] : (ran2 == 1 ? bulletNameArray[2] : bulletNameArray[1]));
                        pattern1Ctls[ran2].StartShotRoutine();
                        yield return new WaitForSeconds(2.0f);
                    }
                    SoundManager.instance.StopAudio(new string[1] { "Boss/bossShot" });
                    yield return StartCoroutine(CenterJump());
                    if (animator.GetBool("aura")) animator.SetBool("aura", false);
                    if (bulletEffectP.activeSelf) bulletEffectP.SetActive(false);
                    fixedPattern = 0;
                }
                else if (ranPattern == 2)
                {
                    pattern_time_2 -= Time.deltaTime;
                    if (pattern_time_2 <= 0.0f)
                    {
                        pattern_time_2 = 8;
                        yield return StartCoroutine(CenterJump());
                        fixedPattern = 0;
                    }
                    else
                    {
                        float dis = Vector3.Distance(transform.position, PDG.Player.instance.transform.position);
                        pattern_time_2_0 -= Time.deltaTime;
                        if (!isSwingAttack && pattern_time_2_0 <= 0.0f)
                        {
                            pattern_time_2_0 = 1.0f;
                            animator.SetTrigger("attack");
                            isSwingAttack = true;
                            if (ArmCoroutine == null) ArmCoroutine = StartCoroutine(ArmCoroutineReal());
                        }


                        if (!isSwingAttack)
                        {
                            if (!animator.GetBool("move")) animator.SetBool("move", true);
                            Vector3 dir = PDG.Player.instance.transform.position - transform.position;
                            dir.Normalize();
                            if (dir.x > 0)
                            {
                                sr.flipX = true;
                            }
                            else
                            {
                                sr.flipX = false;
                            }
                            RaycastHit2D hit = Physics2D.BoxCast(transform.position, new Vector2(1.5f, 2.5f), 0, new Vector2(0, -0.75f), 0, 1 << 0);
                            if (!hit) transform.Translate(dir * fWheelSpeed * Time.deltaTime);
                        }
                    }
                }
                else if (ranPattern == 3)
                {
                    if (Dungeon.instance.StageLevel > 2)
                        animator.SetFloat("animSpeed", 2.0f);
                    else
                        animator.SetFloat("animSpeed", 1.0f);

                    PDG.Player.instance.GetComponent<BoxCollider2D>().offset = new Vector2(0, -0.4f);
                    PDG.Player.instance.GetComponent<BoxCollider2D>().size = new Vector2(0.15f, 0.15f);
                    for (int i = 0; i < 5; i++)
                    {
                        if (isDie) break;
                        animator.SetTrigger("footwork");
                        if (Dungeon.instance.StageLevel > 2)
                        {
                            yield return new WaitForSeconds(1.5f);
                            animator.SetTrigger("footwork2");
                        }
                        yield return new WaitForSeconds(2.0f);
                    }
                    yield return StartCoroutine(CenterJump());
                    fixedPattern = 0;
                    PDG.Player.instance.GetComponent<BoxCollider2D>().offset = new Vector2(0, -0.15f);
                    PDG.Player.instance.GetComponent<BoxCollider2D>().size = new Vector2(0.15f, 0.15f);
                }
            }
            else
            {
                pattern_time_2_0 = 1.0f;
                ranShot.m_bulletNum = 80;
                ranShot.m_randomSpeedMin = 5;
                ranShot.m_randomDelayMax = 7;
                ranShot.m_randomDelayMin = 0;
                ranShot.m_randomDelayMax = 0;
                ranShotCtl._mobName = "Chunker_STG";
                ranShotCtl._bulletName = bulletNameArray[1];
                ranShotCtl.StartShotRoutine();
                animator.SetBool("introEnd", false);
                fixedPattern = -1;
                ranPattern++;
                if (ranPattern > 3) ranPattern = 0;
                yield return new WaitForSeconds(1.0f);
            }
            yield return new WaitForSeconds(Time.deltaTime);
        }
    }
    IEnumerator ArmCoroutineReal()
    {
        yield return new WaitForSeconds(0.5f);
        SoundManager.instance.StartAudio(new string[1] { "Weapon/tentacle_arm" }, VOLUME_TYPE.EFFECT);
        isBackgroundAttack = false;
        Vector3 targetPos = PDG.Player.instance.transform.position;
        {
            //armObj.SetActive(true);
            //bool[] ss = new bool[5];
            //int[] ranAngle = new int[5];
            //System.Array.Clear(ss, 0, ss.Length);
            //System.Array.Clear(ranAngle, 0, ranAngle.Length);
            //for (int i = 0; i < 5; i++) ranAngle[i] = Random.Range(20, 40);
            //for (int j = 0; j < (armPieces.Length / 5); j++)
            //{
            //    for (int i = 0; i < 5; i++)
            //    {
            //        if (j > 15)
            //        {
            //            if (ss[i]) continue;
            //            int rr = Random.Range(0, 100);
            //            if (rr < 20)
            //            {
            //                ss[i] = true;
            //                continue;
            //            }
            //
            //        }
            //
            //        float angle = GetAngle(armObj.transform.position, targetPos);
            //        angle = angle + ((i * ranAngle[i]) - ranAngle[i]);
            //        Vector3 tDir = GetPosition(armObj.transform.position, angle) - armObj.transform.position;
            //        tDir.Normalize();
            //        if (isBackgroundAttack) break;
            //        armPieces[(i * (armPieces.Length / 5)) + j].SetActive(true);
            //        armPieces[(i * (armPieces.Length / 5)) + j].transform.localPosition = tDir * (armDistance * j);
            //        armPieces[(i * (armPieces.Length / 5)) + j].transform.localEulerAngles = new Vector3(0, 0, angle);
            //        armPieces[(i * (armPieces.Length / 5)) + j].GetComponent<SpriteRenderer>().sortingOrder = (i * (armPieces.Length / 5)) + j;
            //    }
            //    if (isDie) break;
            //    yield return null;
            //}
            //for (int j = (armPieces.Length / 5) - 1; j >= 0; j--)
            //{
            //    for (int i = 5 - 1; i >= 0; i--)
            //    {
            //        armPieces[(i * (armPieces.Length / 5)) + j].transform.localPosition = Vector3.zero;
            //        armPieces[(i * (armPieces.Length / 5)) + j].SetActive(false);
            //    }
            //    yield return null;
            //}
            //armObj.SetActive(false);
        }
        string tId = mobData.ID;
        if (tId.Contains("_t")) tId = tId.Substring(0, tId.Length - 3);
        for (int i = 0; i < 20; i++)
        {
            float angle = GetAngle(transform.position, targetPos);
            GameObject obj = Instantiate(BulletObj);
            obj.transform.position = transform.position;
            Vector3 pos = GetPosition(transform.position, angle + Random.Range(-20, 20));
            Vector3 dir = pos - transform.position;
            dir.Normalize();
            obj.GetComponent<MonsterAttackController>().firePosition = transform.position;
            obj.GetComponent<MonsterAttackController>().whyDie = LocalizeManager.GetLocalize("monster_" + tId);
            obj.GetComponent<MonsterAttackController>().RigidBodyFire(dir, Random.Range(4, 6));
            yield return new WaitForSeconds(Random.Range(0.01f, 0.05f));
        }

        ArmCoroutine = null;
        isSwingAttack = false;
    }
    IEnumerator CenterJump()
    {
        if (!isDie)
        {
            for (int i = 0; i < armPieces.Length; i++)
            {
                armPieces[i].SetActive(false);
            }

            isInvincibility = true;
            animator.SetTrigger("Trigger_2");
            Vector3 pos = transform.position;
            SoundManager.instance.StartAudio(new string[1] { "Boss/stg_jump" }, VOLUME_TYPE.EFFECT);
            SoundManager.instance.StartAudio(new string[1] { "Boss/stg_jump" }, VOLUME_TYPE.EFFECT);
            GameObject obj3 = ObjManager.Call().GetObject("Hit_sprite12");
            Vector3 pos3 = transform.position;
            pos3.y -= 2;
            obj3.transform.position = pos3;
            obj3.transform.localScale = Vector3.one;
            obj3.GetComponent<SwordHitEffectController>()._effctName = "JumpEffect";
            obj3.GetComponent<SwordHitEffectController>().animSpeed = 0.2f;
            obj3.GetComponent<Animator>().Rebind();
            obj3.SetActive(true);

            for (int i = 0; i < 30; i++)
            {
                pos.y += 1;
                transform.position = pos;
                yield return new WaitForSeconds(Time.deltaTime);
            }
            Vector3 p = nextTarget.transform.position;
            p.y += 50;
            transform.position = p;
            animator.SetTrigger("Trigger_3");
            animator.SetBool("introEnd", true);
            while (true)
            {
                pos.y -= 2;
                transform.position = pos;
                p = nextTarget.transform.position;
                p.y += 2;
                if (pos.y <= p.y)
                {
                    transform.position = p;
                    break;
                }
                yield return new WaitForSeconds(Time.deltaTime);
            }
            //GameObject obj4 = ObjManager.Call().GetObject("Hit_sprite12");
            //Vector3 pos4 = transform.position;
            //obj4.transform.position = pos4;
            //obj4.transform.localScale = Vector3.one;
            //obj4.GetComponent<SwordHitEffectController>()._effctName = "PikoJumpDown2";
            //obj4.GetComponent<SwordHitEffectController>().animSpeed = 0.2f;
            //obj4.GetComponent<Animator>().Rebind();
            //obj4.SetActive(true);
            SoundManager.instance.StartAudio(new string[1] { "Boss/stg_jump_down" }, VOLUME_TYPE.EFFECT);
            SoundManager.instance.StartAudio(new string[1] { "Boss/stg_jump_down" }, VOLUME_TYPE.EFFECT);
            CameraShaker._instance.StartShake(0.2f, 0.02f, 0.2f);
            isInvincibility = false;
            GameObject s = Instantiate(JumpAtferEffect);
            s.transform.position = transform.position;
            Destroy(s, 2f);
        }
    }
    public void SetDamage(float _damage, Vector3 _pos)
    {
        if (Dungeon.instance.isPause) return;
        if (isInvincibility) return;
        if (isDie) return;

        GameObject damageObjOrigin = Resources.Load<GameObject>("DamageText");
        GameObject damageObj = Instantiate(damageObjOrigin);
        damageObj.transform.SetParent(GameObject.Find("Canvas").transform);
        damageObj.transform.SetSiblingIndex(16);
        damageObj.transform.localScale = Vector3.one;
        damageObj.GetComponent<DamageText>().InitSetting(_damage, gameObject);

        mobInfo.curHp -= _damage;
        if (!isHit)
        {
            isHit = true;
            hitTime = 0.25f;
        }
        if (HitCoroutine == null) HitCoroutine = StartCoroutine(HitEffect());


        GameObject obj = ObjManager.Call().GetObject("DamageHit_Boss0");
        if (obj)
        {
            obj.transform.position = _pos;
            obj.SetActive(true);
        }
        string tId = mobData.ID;
        if (tId.Contains("_t")) tId = tId.Substring(0, tId.Length - 3);
        if (mobInfo.curHp <= 0)
        {
            PDG.Dungeon.instance.isBossRushStart = false;
            StateGroup.instance.isClearTimeStart = false;
            StateGroup.instance.isBattleTimeStart = false;
            SaveAndLoadManager.instance.SaveCollection(new CollectionData { _id = tId, _isOpen = true, _isWeapon = false });
            SoundManager.instance.StopAllSound();
            UbhObjectPool.instance.ReleaseAllBullet();
            ResultPanelScript.instance.bossList.Add(0);
            ScreenShot.instance.Screenshot(gameObject);
            isDie = true;
            PDG.Player.instance.fInvincibleTime_offensive_01 = 10;
            for (int i = 0; i < armPieces.Length; i++)
            {
                armPieces[i].SetActive(false);
            }
            wheelwindAttackCollision.SetActive(false);
            GetComponent<BoxCollider2D>().enabled = false;
            animator.SetTrigger("die");
            StartCoroutine(DieAction());
        }
    }
    IEnumerator DieAction()
    {
        if (GetComponent<BoxCollider2D>()) Destroy(GetComponent<BoxCollider2D>());
        dieEffect_0.SetActive(true);
        dieEffect_1.SetActive(true);
        pattern1Ctls[0].gameObject.SetActive(false);
        pattern1Ctls[1].gameObject.SetActive(false);
        pattern1Ctls[2].gameObject.SetActive(false);
        ranShotCtl.gameObject.SetActive(false);
        SoundManager.instance.StartAudio(new string[1] { "Monster/boss_shaker" }, VOLUME_TYPE.EFFECT);
        yield return new WaitForSecondsRealtime(0.3f);
        Time.timeScale = 0.1f;
        yield return new WaitForSecondsRealtime(2.0f);
        Time.timeScale = 1.0f;
        CameraShaker._instance.StartShake(0.1f, Time.deltaTime, 2.5f, true);
        Image spake = GameObject.Find("Canvas").transform.Find("Spake").GetComponent<Image>();
        float a = 0.0f;
        while (true)
        {
            spake.color = new Color(1, 1, 1, a);
            a += Time.fixedUnscaledDeltaTime;
            if (a >= 1.0f) break;
            yield return new WaitForSecondsRealtime(Time.fixedUnscaledDeltaTime);
        }
        GameObject.Find("Camera").GetComponent<PDG.Camera>().isTestScreenClear = true;
        spake.color = new Color(1, 1, 1, 1);
        SoundManager.instance.StartAudio(new string[1] { "BossSpake" }, VOLUME_TYPE.EFFECT);
        SoundManager.instance.StartAudio(new string[3] { "Boss/DistantBigExplosion2", "Boss/DistantBigExplosion3", "Boss/DistantBigExplosion4" }, VOLUME_TYPE.EFFECT);
        SoundManager.instance.StartAudio(new string[3] { "Boss/DistantBigExplosion2", "Boss/DistantBigExplosion3", "Boss/DistantBigExplosion4" }, VOLUME_TYPE.EFFECT);
        yield return new WaitForSecondsRealtime(1.0f);
        spake.color = new Color(1, 1, 1, 0);
        GameObject.Find("Camera").GetComponent<PDG.Camera>().isTestScreenClear = true;
        animator.SetTrigger("dieAfter");
        PDG.Player.instance.isCameraOff = true;
        (FindObjectOfType(typeof(PDG.Camera)) as PDG.Camera).target = transform;
        Dialogue.Instance.CommnetSetting("stg_intro_comment_5", 0, null);
    }
    public void EndEvent()
    {
        animator.enabled = false;
        sr.sprite = lastSprite;
        SoundManager.instance.StartAudio(new string[1] { "Boss/DieEnd" }, VOLUME_TYPE.EFFECT);
        StartCoroutine(EndEventCoroutine());
    }
    IEnumerator EndEventCoroutine()
    {
        string tId = mobData.ID;
        if (tId.Contains("_t")) tId = tId.Substring(0, tId.Length - 3);
        if (PDG.Dungeon.instance.isBossRush)
        {
            List<string> dropItems = new List<string>();
            int skullCount = 10 * (PDG.Dungeon.instance.StageLevel + 1);
            for (int i = 0; i < skullCount; i++) dropItems.Add("glowing_skull");
            dropItems.Add("recovery_syringe_2");
            dropItems.Add("recovery_syringe_2");
            dropItems.Add("recovery_syringe_2");
            for (int i = 0; i < ItemBoxs.Length; i++)
            {
                ItemBoxs[i].gameObject.SetActive(true);
                ItemBoxs[i]._id = tId;
                if (ItemBoxs[i].isMaterial)
                {
                    ItemBoxs[i].gameObject.SetActive(false);
                }
                else
                {
                    ItemBoxs[i].itemName = dropItems.ToArray();
                }
            }
        }
        else
        {
            int ata = 0;
            List<ItemsData> iDatas = null;
            while (true)
            {
                ChestPerData data = System.Array.Find(SheetManager.Instance.ChestPerDB.dataArray, item => item.Stagelevel.Equals(PDG.Dungeon.instance.StageLevel));
                string BoxGrade = data.Chest[SaveAndLoadManager.GetBoxGrade(data)];
                BoxGrade = BoxGrade.Substring(BoxGrade.Length - 1, 1);
                iDatas = System.Array.FindAll(SheetManager.Instance.ItemsDB.dataArray, item => item.Grade.ToUpper().Equals(BoxGrade.ToUpper())).ToList();
                for (int i = 0; i < iDatas.Count; i++)
                {
                    Sprite s = Resources.Load<Sprite>("Item/" + iDatas[i].Type1 + "/" + iDatas[i].Type2 + "/" + iDatas[i].ID);
                    if (s == null || !Inventorys.Instance.UnlockAndMaxCountCheck(iDatas[i].ID))
                    {
                        iDatas.RemoveAt(i);
                        i--;
                        continue;
                    }
                }
                ata++;
                if (ata >= 1000) break;
                if (iDatas.Count > 0) break;
            }
            List<string> dropItems = new List<string>();
            if (iDatas.Count > 0) dropItems.Add(iDatas[UnityEngine.Random.Range(0, iDatas.Count)].ID);
            int skullCount = UnityEngine.Random.Range(10, 20);
            if (SaveAndLoadManager.nDifficulty != 0)
            {
                skullCount *= (SaveAndLoadManager.nDifficulty * 2) - 1;
            }
            for (int i = 0; i < skullCount; i++) dropItems.Add("glowing_skull");
            //dropItems.Add("key_normal");
            dropItems.Add("muscle_enhancer_body");
            if (UnityEngine.Random.Range(0, 100) < 50) dropItems.Add("muscle_enhancer_body");

            yield return new WaitForSeconds(1.0f);
            for (int i = 0; i < ItemBoxs.Length; i++)
            {
                ItemBoxs[i].gameObject.SetActive(true);
                ItemBoxs[i]._id = tId;
                if (ItemBoxs[i].isMaterial)
                {
                    int ran = Random.Range(1, 4);
                    int ran2 = Random.Range(0, 3);
                    int ran3 = Random.Range(0, 3);
                    string[] ttt = new string[1 + ran];
                    for (int j = 0; j < 1 + ran; j++)
                    {
                        if (j == 0) ttt[j] = "boss_BossStg";
                        else ttt[j] = "normal_" + (ran2 == 0 ? "Cells" + (ran3 == 0 ? "_0" : "_1") : (ran2 == 1 ? "Ore" : "Parts" + (ran3 == 0 ? "_0" : (ran3 == 1 ? "_1" : "_2"))));
                    }
                    ItemBoxs[i].itemName = ttt;
                }
                else
                {
                    ItemBoxs[i].itemName = dropItems.ToArray();
                }
            }

            if (SteamStatsAndAchievements.instance != null) SteamStatsAndAchievements.instance.SettingStat(tId + "_hunting");
            if (SteamStatsAndAchievements.instance != null) SteamStatsAndAchievements.instance.SettingStat("stat_stage_clear_" + PDG.Dungeon.instance.StageLevel);
            if (SteamStatsAndAchievements.instance != null) SteamStatsAndAchievements.instance.SettingStat("stat_hunting_boss_count");

            List<AchievementData> savedatas = new List<AchievementData>();
            AchievementsData aData_0 = System.Array.Find(SheetManager.Instance.AchievementsDB.dataArray, item => item.ID.Equals("huntermaster_stg"));
            savedatas.Add(new AchievementData { _id = aData_0.ID, _cur = 1, _max = aData_0.MAX, _clear = false });
            AchievementsData aData_1 = System.Array.Find(SheetManager.Instance.AchievementsDB.dataArray, item => item.ID.Equals("stage_clear_" + PDG.Dungeon.instance.StageLevel));
            savedatas.Add(new AchievementData { _id = aData_1.ID, _cur = 1, _max = aData_1.MAX, _clear = false });
            if (StateGroup.instance.nShotByBoss == 0)
            {
                AchievementsData aData = System.Array.Find(SheetManager.Instance.AchievementsDB.dataArray, item => item.ID.Equals("godcontrol"));
                savedatas.Add(new AchievementData { _id = aData.ID, _cur = 1, _max = aData.MAX, _clear = false });
            }
            if (StateGroup.instance.nShotByHunter == 0)
            {
                AchievementsData aData = System.Array.Find(SheetManager.Instance.AchievementsDB.dataArray, item => item.ID.Equals("nodamageclear"));
                savedatas.Add(new AchievementData { _id = aData.ID, _cur = 1, _max = aData.MAX, _clear = false });
            }
            if (PDG.Player.instance.nowWeaponName.Contains("tentacle_arm"))
            {
                AchievementsData aData = System.Array.Find(SheetManager.Instance.AchievementsDB.dataArray, item => item.ID.Equals("tantaclemaster"));
                savedatas.Add(new AchievementData { _id = aData.ID, _cur = 1, _max = aData.MAX, _clear = false });
            }
            if (StateGroup.instance.fClearTime <= 720)
            {
                AchievementsData aData = System.Array.Find(SheetManager.Instance.AchievementsDB.dataArray, item => item.ID.Equals("possible_0"));
                savedatas.Add(new AchievementData { _id = aData.ID, _cur = 1, _max = aData.MAX, _clear = false });
            }
            if (StateGroup.instance.fClearTime <= 480)
            {
                AchievementsData aData = System.Array.Find(SheetManager.Instance.AchievementsDB.dataArray, item => item.ID.Equals("possible_1"));
                savedatas.Add(new AchievementData { _id = aData.ID, _cur = 1, _max = aData.MAX, _clear = false });
            }
            if (StateGroup.instance.fClearTime <= 180)
            {
                AchievementsData aData = System.Array.Find(SheetManager.Instance.AchievementsDB.dataArray, item => item.ID.Equals("possible_2"));
                savedatas.Add(new AchievementData { _id = aData.ID, _cur = 1, _max = aData.MAX, _clear = false });
            }
            SaveAndLoadManager.instance.SaveAchievements(savedatas.ToArray());

            PDG.Dungeon.instance.isStageBoss = false;
            PDG.Dungeon.instance.StageLevel++;
            PDG.Dungeon.instance.nowStageStep = 0;
            SaveAndLoadManager.instance.GameSave(Inventorys.Instance.slots);
        }

        PDG.Player.instance.isCameraOff = false;
        (FindObjectOfType(typeof(PDG.Camera)) as PDG.Camera).target = PDG.Player.instance.transform;
        SoundManager.instance.StartAudio(new string[1] { "warp_open" }, VOLUME_TYPE.EFFECT);
        GameObject.Find("Canvas").transform.Find("TutorialText").gameObject.SetActive(true);
        GameObject.Find("Canvas").transform.Find("TutorialText").GetComponent<Text>().text = LocalizeManager.GetLocalize("boss_open_title");
        if (!PDG.Dungeon.instance.isTestMode2)
            GameObject.Find("Canvas").transform.Find("ExitArrow").gameObject.SetActive(true);
        for (int i = 0; i < nextHole.Length; i++)
        {
            nextHole[i].GetComponent<DungeonNextController>().isBossOpen = true;
            nextHole[i].GetComponent<DungeonNextController>().sr.sprite = nextHole[i].GetComponent<DungeonNextController>().endSprite;
        }
        if (SteamStatsAndAchievements.instance != null) SteamStatsAndAchievements.instance.UnlockAchievementToPublic("Boss_0_Clear");
        StateGroup.instance.OpenResult(tId);
    }
    IEnumerator HitEffect()
    {
        while (isHit && !isDie)
        {
            sr.color = HitColor;
            yield return new WaitForSeconds(0.06f);
            sr.color = HitColor2;
            yield return new WaitForSeconds(0.06f);
            sr.color = HitColor3;
            yield return new WaitForSeconds(0.06f);
            sr.color = HitColor4;
            yield return new WaitForSeconds(0.06f);
            sr.color = Color.white;
        }
        HitCoroutine = null;
    }
    #region -- Tool --
    public static float GetAngle(Vector3 vStart, Vector3 vEnd)
    {
        Vector3 v = vEnd - vStart;

        return Mathf.Atan2(v.y, v.x) * Mathf.Rad2Deg;
    }
    public static Vector3 GetPosition(Vector3 vStart, float angle)
    {
        float x = Mathf.Cos(Mathf.Deg2Rad * angle) + vStart.x;
        float y = Mathf.Sin(Mathf.Deg2Rad * angle) + vStart.y;

        return new Vector3(x, y, 0);
    }
    private int GetPercentege(float _per, string[] list)
    {
        int[] per = new int[Mathf.FloorToInt(100 * list.Length)];
        for (int i = 0; i < per.Length; i++) per[i] = -1;

        for (int i = 0; i < list.Length; i++)
        {
            for (int j = 0; j < Mathf.FloorToInt(_per * 100); j++)
            {
                per[(i * 100) + j] = i;
            }
        }
        if (per.Length == 0) return -1;
        return per[Random.Range(0, per.Length)];
    }
    #endregion
}
