﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BossWaitingRoom : MonoBehaviour
{
    public bool isEntrance = false;

    public string stateName = string.Empty;
    public GameObject animObj;
    public GameObject entranceQuestionPanel;
    public Button YesBtn;
    public Button NoBtn;
    public Text entranceQuestionText;
    public GameObject fadeObj;
    private float fadeTime = 0.1f;
    private Animator WarpExitAnim;

    [SerializeField] private GameObject entranceMouse;
    public Coroutine mouseMoveCoroutine;
    [SerializeField] private bool isEntranceFirst = false;
    LocalizeData lData;
    LocalizeData lData2;

    [SerializeField] private bool isMouseMove = false;
    [SerializeField] private bool isSelect = false;
    bool downClear = false;
    private void Start()
    {
        if (entranceQuestionPanel == null) entranceQuestionPanel = GameObject.Find("Canvas").transform.Find("QuestionPanel").gameObject;
        if (entranceQuestionText == null) entranceQuestionText = entranceQuestionPanel.transform.Find("Text").GetComponent<Text>();
        if (YesBtn == null) YesBtn = entranceQuestionPanel.transform.Find("BtnPanel").Find("YesBtn").GetComponent<Button>();
        if (NoBtn == null) NoBtn = entranceQuestionPanel.transform.Find("BtnPanel").Find("NoBtn").GetComponent<Button>();
        if (fadeObj == null) fadeObj = GameObject.Find("Canvas").transform.Find("fadeObj").gameObject;

        if (WarpExitAnim == null) WarpExitAnim = GameObject.Find("Generator").transform.Find("WarpMoveExit").GetComponent<Animator>();

        lData = System.Array.Find(SheetManager.Instance.LocalDB.dataArray, item => item.ID.Equals("ui_bossentrance"));
        lData2 = System.Array.Find(SheetManager.Instance.LocalDB.dataArray, item => item.ID.Equals("ui_bossexit"));
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.layer.Equals(9))
        {
            isSelect = true;
            if (!isEntranceFirst && isEntrance && !isMouseMove)
            {
                if (mouseMoveCoroutine == null) mouseMoveCoroutine = StartCoroutine(MouseMove());
            }
            else
                StartCoroutine(Question());
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        isSelect = false;
        entranceQuestionPanel.SetActive(false);
    }
    IEnumerator MouseMove()
    {
        SoundManager.instance.StartAudio(new string[1] { "Object/bossEntrance" }, VOLUME_TYPE.EFFECT);
        BoxCollider2D box = GetComponent<BoxCollider2D>();
        box.offset = new Vector2(0, 1.5f);
        box.size = new Vector2(6, 3);
        Vector3 pos = new Vector3(0, 8, 0);
        while (true)
        {
            entranceMouse.transform.localPosition = pos;
            pos.y -= Time.deltaTime;
            if (pos.y <= 7.0f) break;
            yield return new WaitForSeconds(Time.deltaTime);
        }
        entranceMouse.transform.localPosition = new Vector3(0, 7, 0);
        isEntranceFirst = true;
        StartCoroutine(Question());
    }
    IEnumerator Question()
    {
        if (!entranceQuestionPanel.activeSelf)
        {
            if (isEntrance)
            {
                if (LocalizeManager.lang == LANGUAGE_SHIFTRICK.ENG)
                    entranceQuestionText.text = lData.ENG;
                else if (LocalizeManager.lang == LANGUAGE_SHIFTRICK.KOR)
                    entranceQuestionText.text = lData.KOR;
                else if (LocalizeManager.lang == LANGUAGE_SHIFTRICK.CHN_S)
                    entranceQuestionText.text = lData.CHNS;
                else if (LocalizeManager.lang == LANGUAGE_SHIFTRICK.CHN_T)
                    entranceQuestionText.text = lData.CHNT;
                else if (LocalizeManager.lang == LANGUAGE_SHIFTRICK.JA)
                    entranceQuestionText.text = lData.JA;
                else if (LocalizeManager.lang == LANGUAGE_SHIFTRICK.RU)
                    entranceQuestionText.text = lData.RU;
            }
            else
            {
                if (LocalizeManager.lang == LANGUAGE_SHIFTRICK.ENG)
                    entranceQuestionText.text = lData2.ENG;
                else if (LocalizeManager.lang == LANGUAGE_SHIFTRICK.KOR)
                    entranceQuestionText.text = lData2.KOR;
                else if (LocalizeManager.lang == LANGUAGE_SHIFTRICK.CHN_S)
                    entranceQuestionText.text = lData2.CHNS;
                else if (LocalizeManager.lang == LANGUAGE_SHIFTRICK.CHN_T)
                    entranceQuestionText.text = lData2.CHNT;
                else if (LocalizeManager.lang == LANGUAGE_SHIFTRICK.JA)
                    entranceQuestionText.text = lData2.JA;
                else if (LocalizeManager.lang == LANGUAGE_SHIFTRICK.RU)
                    entranceQuestionText.text = lData2.RU;
            }

            entranceQuestionPanel.SetActive(true);
            YesBtn.onClick.RemoveAllListeners();
            YesBtn.onClick.AddListener(() => {
                PDG.Player.instance.isCameraOff = false;
                SoundManager.instance.StartAudio(new string[1] { "Click_0" }, VOLUME_TYPE.EFFECT);
                Warp();
            });
            NoBtn.onClick.RemoveAllListeners();
            NoBtn.onClick.AddListener(() =>
            {
                PDG.Player.instance.isCameraOff = false;
                entranceQuestionPanel.SetActive(false);
            });
            yield return PDG.Dungeon.instance.StartCoroutine(PDG.Dungeon.instance.StartQuestionEvent(entranceQuestionPanel));
            downClear = true;
        }
    }
    private void Update()
    {
        if (!isSelect && downClear) 
        {
            downClear = false;
            entranceQuestionPanel.SetActive(false);
        }

        if (entranceQuestionPanel.activeSelf) 
        {
            if ((Input.GetKeyDown(KeyCode.Escape) || Input.GetKeyDown(KeyCode.Backspace) || PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_CANCLE], true)) && PDG.Dungeon.instance.closeCoolTime > 0.2f) 
            {
                PDG.Dungeon.instance.closeCoolTime = 0.0f;
                entranceQuestionPanel.SetActive(false);
                PDG.Player.instance.isCameraOff = false;
                SoundManager.instance.Click();
            }

            if ((Input.GetKeyDown(KeyCode.Return) || PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ACTIVE], true) || PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_CONFIRM], true)) && PDG.Dungeon.instance.closeCoolTime > 0.2f) 
            {
                PDG.Dungeon.instance.closeCoolTime = 0.0f;
                SoundManager.instance.StartAudio(new string[1] { "Click_0" }, VOLUME_TYPE.EFFECT);
                PDG.Player.instance.isCameraOff = false;
                Warp();
            }
        }
    }
    private void Warp()
    {
        StartCoroutine(WarpStart());
    }
    IEnumerator WarpStart()
    {
        PDG.Player.instance.gameObject.SetActive(false);
        entranceQuestionPanel.SetActive(false);
        if (isEntrance)
        {
            SoundManager.instance.StartAudio(new string[1] { "walking-5" }, VOLUME_TYPE.EFFECT);
            animObj.SetActive(true);
            animObj.GetComponent<Animator>().Rebind();
        }
        yield return new WaitForSeconds(1.5f);
        if (isEntrance) SoundManager.instance.StopAudio(new string[1] { "walking-5" });
        yield return StartCoroutine(FadeInOut());
        //
        if (!isEntrance)
        {
            WarpExitAnim.gameObject.SetActive(true);
            WarpExitAnim.Rebind();
            yield return new WaitForSeconds(1.5f);
            WarpExitAnim.gameObject.SetActive(false);
        }
        else
        {
            yield return new WaitForSeconds(1.0f);
        }
        PDG.Player.instance.gameObject.SetActive(true);
        PDG.Player.instance.GetComponent<PDG.Hero>().animator.SetInteger("Direction", 3);
        if (!isEntrance) PDG.Dungeon.instance.RoomExit();

        if (entranceMouse) entranceMouse.transform.localPosition = new Vector3(0, 8, 0);
        if (isEntrance) 
        {
            mouseMoveCoroutine = null;
            isEntranceFirst = false;
            BoxCollider2D box = GetComponent<BoxCollider2D>();
            box.offset = new Vector2(0, 0);
            box.size = new Vector2(6, 6);
            transform.parent.gameObject.SetActive(false);
            PDG.Dungeon.instance.nowRoomType = "boss";
        }
        else
        {
            transform.parent.Find("entrance").GetComponent<BossWaitingRoom>().mouseMoveCoroutine = null;
            transform.parent.Find("entrance").GetComponent<BossWaitingRoom>().isEntranceFirst = false;
            BoxCollider2D box = transform.parent.Find("entrance").GetComponent<BoxCollider2D>();
            box.offset = new Vector2(0, 0);
            box.size = new Vector2(6, 6);
        }
    }
    public IEnumerator FadeInOut()
    {
        fadeObj.SetActive(true);
        float a = 0.0f;
        while (a <= 1.0f)
        {
            fadeObj.GetComponent<Image>().color = new Color(0, 0, 0, a);
            a += Time.deltaTime / fadeTime;
            yield return new WaitForSeconds(Time.deltaTime);
        }
        fadeObj.GetComponent<Image>().color = new Color(0, 0, 0, 1);
        Vector3 endPos;
        if (isEntrance)
        {
            if (transform.parent.Find("FogDark")) transform.parent.Find("FogDark").gameObject.SetActive(false);
            if (transform.parent.Find("FogDark2")) transform.parent.Find("FogDark2").gameObject.SetActive(false);
            if (transform.parent.Find("FogDark3")) transform.parent.Find("FogDark3").gameObject.SetActive(false);
            if (transform.parent.Find("FogDark4")) transform.parent.Find("FogDark4").gameObject.SetActive(false);
            endPos = PDG.Dungeon.instance.waitingExit;
        }
        else endPos = PDG.Dungeon.instance.waitingEntrance;
        endPos.x += 1;
        PDG.Player.instance.transform.position = endPos;
        Vector2 minusPosition = new Vector2(0.5f, -0.5f);
        if (isEntrance) WarpExitAnim.transform.position = PDG.Dungeon.instance.waitingExit + minusPosition;
        else WarpExitAnim.transform.position = PDG.Dungeon.instance.waitingEntrance + minusPosition;
        yield return new WaitForSeconds(1.0f);
        while (a >= 0.0f)
        {
            fadeObj.GetComponent<Image>().color = new Color(0, 0, 0, a);
            a -= Time.deltaTime / fadeTime;
            yield return new WaitForSeconds(Time.deltaTime);
        }
        fadeObj.GetComponent<Image>().color = new Color(0, 0, 0, 0);
        fadeObj.SetActive(false);
        if (isEntrance)
        {
            if (PDG.Player.instance.isSubWeapon)
            {
                PDG.Player.instance.isSubWeapon = false;
            }

            PDG.Player.instance.buffTowerBuffTime_0 = 0.0f;
            PDG.Player.instance.buffTowerBuffTime_1 = 0.0f;
            PDG.Player.instance.buffTowerBuffTime_2 = 0.0f;
            PDG.Player.instance.buffTowerBuffTime_3 = 0.0f;

            PDG.Dungeon.instance.RoomInsert(0, false, true);
            BossRoomExitDelete(endPos);
            PDG.Dungeon.instance.spawnBat.SetActive(false);
            if (GameObject.Find("Canvas").transform.Find("MiniMap_OutLine").gameObject.activeSelf) GameObject.Find("Canvas").transform.Find("MiniMap_OutLine").gameObject.SetActive(false);
        }
    }
    private void BossRoomExitDelete(Vector2 pos)
    {
        int mask = 1 << 19;
        RaycastHit2D hit = Physics2D.BoxCast(PDG.Dungeon.instance.waitingExit, new Vector2(0.5f, 0.5f), 0, Vector2.zero, 0, mask);
        if (hit)
        {
            if (hit.collider.GetComponent<Exit>())
            {
                hit.collider.GetComponent<Exit>().isWarp = false;
                hit.collider.GetComponent<Exit>().isLock = false;
                hit.collider.GetComponent<Exit>().isNotMonster = true;
                hit.collider.GetComponent<Exit>().AdjacentLock(true);
            }
        }
    }
}
