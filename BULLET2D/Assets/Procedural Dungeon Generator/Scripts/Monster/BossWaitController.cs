﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossWaitController : MonoBehaviour
{
    [SerializeField] private bool isWaiting = true;
    public PDG.Camera _pCamera;
    public Animator bossWaitingAnim;
    public bool isFirstCommnetEnd = false;
    [SerializeField] private float upSpeed = 50.0f;
    [SerializeField] private float downSpeed = 50.0f;
    [SerializeField] private GameObject nextTarget;
    [SerializeField] private GameObject realBoss;
    private void Start()
    {
        _pCamera = FindObjectOfType(typeof(PDG.Camera)) as PDG.Camera;    
    }
    // Update is called once per frame
    void Update()
    {
        if (_pCamera == null) _pCamera = FindObjectOfType(typeof(PDG.Camera)) as PDG.Camera;
        if (isWaiting)
        {
            RaycastHit2D hit = Physics2D.CircleCast(transform.position, 30, Vector2.zero, 0, 1 << 9);
            if (hit) 
            {
                isWaiting = false;
                StartCoroutine(WaitingEnd());
                SoundManager.instance.StartAudio(new string[1] { "Boss/stg_start_bgm" }, VOLUME_TYPE.BGM, null, true);
            }
        }
    }
    
    IEnumerator WaitingEnd()
    {
        PDG.Player.instance.isCameraOff = true;
        _pCamera.target = transform;
        yield return new WaitForSeconds(0.5f);
        Dialogue.Instance.bwc = this;
        Dialogue.Instance.CommnetSetting("stg_intro_comment_0", 0, null);
        while(!isFirstCommnetEnd)
        {
            yield return null;
        }
        bossWaitingAnim.SetTrigger("Trigger_2");
        Vector3 pos = bossWaitingAnim.transform.position;
        SoundManager.instance.StartAudio(new string[1] { "Boss/stg_jump" }, VOLUME_TYPE.EFFECT);
        SoundManager.instance.StartAudio(new string[1] { "Boss/stg_jump" }, VOLUME_TYPE.EFFECT);
        GameObject obj3 = ObjManager.Call().GetObject("Hit_sprite12");
        Vector3 pos3 = bossWaitingAnim.transform.position;
        pos3.y -= 2;
        obj3.transform.position = pos3;
        obj3.transform.localScale = Vector3.one;
        obj3.GetComponent<SwordHitEffectController>()._effctName = "JumpEffect";
        obj3.GetComponent<SwordHitEffectController>().animSpeed = 0.2f;
        obj3.GetComponent<Animator>().Rebind();
        obj3.SetActive(true);

        for (int i = 0; i < 30; i++)
        {
            pos.y += upSpeed;
            bossWaitingAnim.transform.position = pos;
            yield return new WaitForSeconds(Time.deltaTime);
        }
        _pCamera.target = nextTarget.transform;
        bossWaitingAnim.SetTrigger("Trigger_3");
        while (true)
        {
            pos.y -= downSpeed;
            bossWaitingAnim.transform.position = pos;
            Vector3 p = nextTarget.transform.position;
            p.y += 3;
            if (pos.y <= p.y)
            {
                bossWaitingAnim.transform.position = p;
                break;
            }
            yield return new WaitForSeconds(Time.deltaTime);
        }


        GameObject obj4 = ObjManager.Call().GetObject("Hit_sprite12");
        Vector3 pos4 = bossWaitingAnim.transform.position;
        obj4.transform.position = pos4;
        obj4.transform.localScale = Vector3.one;
        obj4.GetComponent<SwordHitEffectController>()._effctName = "PikoJumpDown2";
        obj4.GetComponent<SwordHitEffectController>().animSpeed = 0.2f;
        obj4.GetComponent<Animator>().Rebind();
        obj4.SetActive(true);

        SoundManager.instance.StartAudio(new string[1] { "Boss/stg_jump_down" }, VOLUME_TYPE.EFFECT);
        SoundManager.instance.StartAudio(new string[1] { "Boss/stg_jump_down" }, VOLUME_TYPE.EFFECT);
        Dialogue.Instance.CommnetSetting("stg_intro_comment_4", 0, null);
        yield return new WaitForSeconds(1.5f);
        realBoss.transform.position = pos4;
        bossWaitingAnim.gameObject.SetActive(false);
        realBoss.SetActive(true);
    }
    public void IntroStart()
    {
        realBoss.GetComponent<Boss_STG>().isIntroStart = true;
    }
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, 30);
    }

    public void EndEvent()
    {
        realBoss.GetComponent<Boss_STG>().EndEvent();
    }
}
