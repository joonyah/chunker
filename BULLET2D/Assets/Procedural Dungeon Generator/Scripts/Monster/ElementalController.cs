﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]public enum ELEMENTAL_TYPE { NONE, ICE, FIRE, POISON, FREEZE, MAX };

public class ElementalController : MonoBehaviour
{
    public float elementalTime = 0.0f;
    public MOB.Monster _pMob;
    public Animator ParantAnim;
    public Animator MyAnim;
    public ELEMENTAL_TYPE eType;
    bool isSoundPlay = false;
    [SerializeField] private bool isPlayer = false;
    private void OnEnable()
    {
        MyAnim.Rebind();
        ParantAnim.SetFloat("AnimSpeed", 0.0f);
        MyAnim.SetFloat("AnimSpeed", 0.0f);
        isSoundPlay = false;
    }

    private void Update()
    {
        if (!isPlayer && _pMob.mobInfo.curHp <= 0)
        {
            MyAnim.SetFloat("AnimSpeed", 1.0f);
            ParantAnim.SetFloat("AnimSpeed", 1.0f);
            gameObject.SetActive(false);
            return;
        }
        else if (isPlayer && PDG.Player.curHp <= 0)
        {
            MyAnim.SetFloat("AnimSpeed", 1.0f);
            ParantAnim.SetFloat("AnimSpeed", 1.0f);
            gameObject.SetActive(false);
            return;
        }
        elementalTime -= Time.deltaTime;
        if (!isPlayer && elementalTime <= 0.0f && eType == ELEMENTAL_TYPE.ICE)
        {
            MyAnim.SetFloat("AnimSpeed", 1.0f);
            if (!isSoundPlay)
            {
                SoundManager.instance.StartAudio(new string[1] { "Character/rushing_ice_hit_end" }, VOLUME_TYPE.EFFECT);
                isSoundPlay = true;
            }
        }
        else if (isPlayer && elementalTime <= 0.0f && eType == ELEMENTAL_TYPE.ICE)
        {
            MyAnim.SetFloat("AnimSpeed", 1.0f);
            if (!isSoundPlay)
            {
                SoundManager.instance.StartAudio(new string[1] { "Character/rushing_ice_hit_end" }, VOLUME_TYPE.EFFECT);
                isSoundPlay = true;
            }
        }
    }

    public void AnimEnd()
    {
        ParantAnim.SetFloat("AnimSpeed", 1.0f);
        if (!isPlayer && _pMob) _pMob.isFreeze = false;
        else if (isPlayer)
        {
            PDG.Player.instance.isFreeze = false;
        }
        gameObject.SetActive(false);
    }
    public void AnimSetDamage()
    {
        if (isPlayer)
        {
            PDG.Player.instance.SetDamage(1, LocalizeManager.GetLocalize("death_ice"), DEATH_TYPE.DEATH_FALL, string.Empty);
        }
    }
}
