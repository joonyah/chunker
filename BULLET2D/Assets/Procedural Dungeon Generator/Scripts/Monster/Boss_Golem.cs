﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class Boss_Golem : MonoBehaviour
{
    public bool isInvincibility = false;
    public bool isDie = false;
    public bool isHit = false;
    public MOB.MonsterInfo mobInfo;
    public MonstersData mobData;
    [SerializeField] private Animator animator;
    [SerializeField] private Vector3 dropPosition;
    [SerializeField] private SpriteRenderer sr;
    [SerializeField] private Color HitColor;
    [SerializeField] private Color HitColor2;
    [SerializeField] private Color HitColor3;
    [SerializeField] private Color HitColor4;
    private float hitTime = 0.0f;
    private Coroutine HitCoroutine;

    public bool isWait = true;
    [SerializeField] private GameObject holeObj;
    [SerializeField] private GameObject bigBullet;
    [SerializeField] private GameObject smallBullet;
    [SerializeField] private GameObject attackForce;
    [SerializeField] private float moveSpeed = 10.0f;
    [SerializeField] private int pattern = 0;
    [SerializeField] private Transform smallFirePos;
    PDG.Camera _camera;
    [SerializeField] private UnityEngine.UI.Image spakeImage;
    [SerializeField] private bool isLock = false;
    [SerializeField] private LineRenderer leftLine;
    [SerializeField] private LineRenderer rightLine;
    [SerializeField] private GameObject LeftLineEndEffect;
    [SerializeField] private GameObject RightLineEndEffect;
    [SerializeField] private LineRenderer leftLine2;
    [SerializeField] private LineRenderer rightLine2;
    [SerializeField] private GameObject LeftLineEndEffect2;
    [SerializeField] private GameObject RightLineEndEffect2;
    [SerializeField] private LineRenderer leftLine3;
    [SerializeField] private LineRenderer rightLine3;
    [SerializeField] private GameObject LeftLineEndEffect3;
    [SerializeField] private GameObject RightLineEndEffect3;

    [SerializeField] private GameObject[] nextHole;
    [SerializeField] private BossItemBoxController[] ItemBoxs;

    [SerializeField] private GameObject dieEffect_0;
    [SerializeField] private GameObject dieEffect_1;
    [SerializeField] private GameObject subCollision;
    [SerializeField] private List<GameObject> SpawnList = new List<GameObject>();
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position + new Vector3(0, -17), 15);
    }
    void Start()
    {
        StageData sData = System.Array.Find(SheetManager.Instance.StageDB.dataArray, item => item.Roomid[0].Equals("d01_bossstage_03"));
        mobData = System.Array.Find(SheetManager.Instance.MonsterDB.dataArray, item => item.ID.Equals("boss_golem"));
        mobInfo = GetComponent<MOB.MonsterInfo>();
        if (PDG.Dungeon.instance.StageLevel == 0)
            mobInfo.maxHp = mobInfo.curHp = Random.Range(sData.Health[0], sData.Health[1]);
        else if (PDG.Dungeon.instance.StageLevel == 1)
            mobInfo.maxHp = mobInfo.curHp = Random.Range(sData.Health[0], sData.Health[1]) * 2;
        else if (PDG.Dungeon.instance.StageLevel == 2)
            mobInfo.maxHp = mobInfo.curHp = Random.Range(sData.Health[0], sData.Health[1]) * 3;
        else if (PDG.Dungeon.instance.StageLevel == 3)
            mobInfo.maxHp = mobInfo.curHp = Random.Range(sData.Health[0], sData.Health[1]) * 5;

        float hp = mobInfo.curHp;
        if (SaveAndLoadManager.nDifficulty == 0) mobInfo.maxHp = mobInfo.curHp = (hp / 2);
        else if (SaveAndLoadManager.nDifficulty == 1) mobInfo.maxHp = mobInfo.curHp = hp * 0.8f;
        else if (SaveAndLoadManager.nDifficulty == 2) mobInfo.maxHp = mobInfo.curHp = (hp * 2);
        else if (SaveAndLoadManager.nDifficulty == 3) mobInfo.maxHp = mobInfo.curHp = (hp * 4);

        StartCoroutine(PatternIntro());
        _camera = FindObjectOfType(typeof(PDG.Camera)) as PDG.Camera;
        spakeImage = GameObject.Find("Canvas").transform.Find("Spake").GetComponent<UnityEngine.UI.Image>();
        isInvincibility = true;
    }
    public float stageLevelMinusTime = 0.0f;
    public float stageLevelAnimSpeed = 1.0f;
    Coroutine LastPattrenC = null;
    bool isLastLaser = false;
    IEnumerator PatternIntro()
    {
        string tId = mobData.ID;
        if (tId.Contains("_t")) tId = tId.Substring(0, tId.Length - 3);
        if (PDG.Dungeon.instance.StageLevel == 0)
        {
            stageLevelMinusTime = 0.0f;
            stageLevelAnimSpeed = 1.0f;
        }
        else
        {
            stageLevelMinusTime = 2.0f;
            stageLevelAnimSpeed = 1.5f;
        }
        animator.SetFloat("animSpeed", stageLevelAnimSpeed);
        isInvincibility = true;
        while (isWait) yield return null;
        PDG.Player.instance.isCameraOff = true;
        CameraShaker._instance.isDontMoveCamera = true;
        _camera.target = transform.parent;
        float cSize = 7;
        while (!isDie)
        {
            cSize += Time.deltaTime * 4;
            Camera.main.orthographicSize = cSize;
            if (cSize >= 20)
            {
                cSize = 20;
                Camera.main.orthographicSize = cSize;
                break;
            }
            yield return new WaitForSeconds(Time.deltaTime / 4);
        }
        BossHpController.instance.BossStart(LocalizeManager.GetLocalize("monster_" + tId), GetComponent<MOB.MonsterInfo>());
        yield return new WaitForSeconds(0.1f);
        while (!isDie)
        {
            cSize -= Time.deltaTime * 4;
            Camera.main.orthographicSize = cSize;
            if (cSize <=  12)
            {
                cSize = 12;
                Camera.main.orthographicSize = cSize;
                break;
            }
            yield return new WaitForSeconds(Time.deltaTime / 4);
        }
        _camera.target = PDG.Player.instance.transform;
        PDG.Player.instance.isCameraOff = false;
        CameraShaker._instance.isDontMoveCamera = false;
        yield return new WaitForSeconds(3.0f);
        isInvincibility = false;
        StateGroup.instance.isBattleTimeStart = true;
        // 눈에서 빔
        Vector3 leftEnd = leftLine.transform.position;
        Vector3 rightEnd = rightLine.transform.position;

        Vector3 leftEnd2 = leftLine2.transform.position;
        Vector3 rightEnd2 = rightLine2.transform.position;

        Vector3 leftEnd3 = leftLine3.transform.position;
        Vector3 rightEnd3 = rightLine3.transform.position;

        float angle_right = 330.0f;
        float angle_right2 = 270.0f;
        float angle_right3 = 300.0f;
        float angle_left = 180.0f;
        float angle_left2 = 240.0f;
        float angle_left3 = 210.0f;
        bool isRightControl = false;
        bool isRightControl2 = false;
        bool isRightControl3 = false;
        bool isLeftControl = false;
        bool isLeftControl2 = false;
        bool isLeftControl3 = false;
        while (isLock)
        {
            leftLine.gameObject.SetActive(true);
            rightLine.gameObject.SetActive(true);
            LeftLineEndEffect.SetActive(true);
            RightLineEndEffect.SetActive(true);

            if (PDG.Dungeon.instance.StageLevel > 0)
            {
                leftLine2.gameObject.SetActive(true);
                LeftLineEndEffect2.SetActive(true);

                if (!isLeftControl2)
                {
                    angle_left2 += Time.deltaTime * (PDG.Dungeon.instance.StageLevel > 2 ? Random.Range(30, 60) : Random.Range(20, 40));
                    if (angle_left2 > 330)
                    {
                        angle_left2 = 330;
                        isLeftControl2 = true;
                    }
                }
                else
                {
                    angle_left2 -= Time.deltaTime * (PDG.Dungeon.instance.StageLevel > 2 ? Random.Range(30, 60) : Random.Range(20, 40));
                    if (angle_left2 < 190)
                    {
                        angle_left2 = 190;
                        isLeftControl2 = false;
                    }
                }
                Vector3 pos_left2 = GetPosition(leftLine2.transform.position, angle_left2);
                pos_left2 = pos_left2 - leftLine2.transform.position;
                pos_left2.Normalize();
                RaycastHit2D hit_left2 = Physics2D.BoxCast(leftLine2.transform.position, new Vector2(0.5f, 0.1f), angle_left2, pos_left2, 40, 1 << 8 | 1 << 9);
                if (hit_left2)
                    leftEnd2 = hit_left2.point;
                else
                    leftEnd2 = leftLine2.transform.position + pos_left2 * 40;
                LeftLineEndEffect2.transform.eulerAngles = new Vector3(0, 0, angle_left2 + 90);
                LeftLineEndEffect2.transform.position = leftEnd2;
                leftLine2.positionCount = 2;
                leftLine2.SetPosition(0, leftLine2.transform.position);
                leftLine2.SetPosition(1, leftEnd2);
            }
            if (PDG.Dungeon.instance.StageLevel > 1)
            {
                rightLine2.gameObject.SetActive(true);
                RightLineEndEffect2.SetActive(true);

                if (!isRightControl2)
                {
                    angle_right2 += Time.deltaTime * (PDG.Dungeon.instance.StageLevel > 2 ? Random.Range(30, 60) : Random.Range(20, 40));
                    if (angle_right2 > 330)
                    {
                        angle_right2 = 330;
                        isRightControl2 = true;
                    }
                }
                else
                {
                    angle_right2 -= Time.deltaTime * (PDG.Dungeon.instance.StageLevel > 2 ? Random.Range(30, 60) : Random.Range(20, 40));
                    if (angle_right2 < 190)
                    {
                        angle_right2 = 190;
                        isRightControl2 = false;
                    }
                }
                Vector3 pos_right2 = GetPosition(rightLine2.transform.position, angle_right2);
                pos_right2 = pos_right2 - rightLine2.transform.position;
                pos_right2.Normalize();
                RaycastHit2D hit_right2 = Physics2D.BoxCast(rightLine2.transform.position, new Vector2(0.5f, 0.1f), angle_right2, pos_right2, 40, 1 << 8 | 1 << 9);
                if (hit_right2)
                    rightEnd2 = hit_right2.point;
                else
                    rightEnd2 = rightLine2.transform.position + pos_right2 * 40;
                RightLineEndEffect2.transform.eulerAngles = new Vector3(0, 0, angle_right2 + 90);
                RightLineEndEffect2.transform.position = rightEnd2;
                rightLine2.positionCount = 2;
                rightLine2.SetPosition(0, rightLine2.transform.position);
                rightLine2.SetPosition(1, rightEnd2);
            }
            if (PDG.Dungeon.instance.StageLevel > 2)
            {
                leftLine3.gameObject.SetActive(true);
                LeftLineEndEffect3.SetActive(true);
                rightLine3.gameObject.SetActive(true);
                RightLineEndEffect3.SetActive(true);


                if (!isLeftControl3)
                {
                    angle_left3 += Time.deltaTime * (PDG.Dungeon.instance.StageLevel > 2 ? Random.Range(30, 60) : Random.Range(20, 40));
                    if (angle_left3 > 330)
                    {
                        angle_left3 = 330;
                        isLeftControl3 = true;
                    }
                }
                else
                {
                    angle_left3 -= Time.deltaTime * (PDG.Dungeon.instance.StageLevel > 2 ? Random.Range(30, 60) : Random.Range(20, 40));
                    if (angle_left3 < 190)
                    {
                        angle_left3 = 190;
                        isLeftControl3 = false;
                    }
                }
                Vector3 pos_left3 = GetPosition(leftLine3.transform.position, angle_left3);
                pos_left3 = pos_left3 - leftLine3.transform.position;
                pos_left3.Normalize();
                RaycastHit2D hit_left3 = Physics2D.BoxCast(leftLine3.transform.position, new Vector2(0.5f, 0.1f), angle_left3, pos_left3, 40, 1 << 8 | 1 << 9);
                if (hit_left3)
                    leftEnd3 = hit_left3.point;
                else
                    leftEnd3 = leftLine3.transform.position + pos_left3 * 40;
                LeftLineEndEffect3.transform.eulerAngles = new Vector3(0, 0, angle_left3 + 90);
                LeftLineEndEffect3.transform.position = leftEnd3;
                leftLine3.positionCount = 2;
                leftLine3.SetPosition(0, leftLine3.transform.position);
                leftLine3.SetPosition(1, leftEnd3);

                if (!isRightControl3)
                {
                    angle_right3 += Time.deltaTime * (PDG.Dungeon.instance.StageLevel > 2 ? Random.Range(30, 60) : Random.Range(20, 40));
                    if (angle_right3 > 330)
                    {
                        angle_right3 = 330;
                        isRightControl3 = true;
                    }
                }
                else
                {
                    angle_right3 -= Time.deltaTime * (PDG.Dungeon.instance.StageLevel > 2 ? Random.Range(30, 60) : Random.Range(20, 40));
                    if (angle_right3 < 190)
                    {
                        angle_right3 = 190;
                        isRightControl3 = false;
                    }
                }
                Vector3 pos_right3 = GetPosition(rightLine3.transform.position, angle_right3);
                pos_right3 = pos_right3 - rightLine3.transform.position;
                pos_right3.Normalize();
                RaycastHit2D hit_right3 = Physics2D.BoxCast(rightLine3.transform.position, new Vector2(0.5f, 0.1f), angle_right3, pos_right3, 40, 1 << 8 | 1 << 9);
                if (hit_right3)
                    rightEnd3 = hit_right3.point;
                else
                    rightEnd3 = rightLine3.transform.position + pos_right3 * 40;
                RightLineEndEffect3.transform.eulerAngles = new Vector3(0, 0, angle_right3 + 90);
                RightLineEndEffect3.transform.position = rightEnd3;
                rightLine3.positionCount = 2;
                rightLine3.SetPosition(0, rightLine3.transform.position);
                rightLine3.SetPosition(1, rightEnd3);
            }
            if (!isLeftControl)
            {
                angle_left += Time.deltaTime * (PDG.Dungeon.instance.StageLevel > 2 ? Random.Range(30, 60) : Random.Range(20, 40));
                if (angle_left > 330)
                {
                    angle_left = 330;
                    isLeftControl = true;
                }
            }
            else
            {
                angle_left -= Time.deltaTime * (PDG.Dungeon.instance.StageLevel > 2 ? Random.Range(30, 60) : Random.Range(20, 40));
                if (angle_left < 190)
                {
                    angle_left = 190;
                    isLeftControl = false;
                }
            }
            Vector3 pos_left = GetPosition(leftLine.transform.position, angle_left);
            pos_left = pos_left - leftLine.transform.position;
            pos_left.Normalize();
            RaycastHit2D hit_left = Physics2D.BoxCast(leftLine.transform.position, new Vector2(0.5f, 0.1f), angle_left, pos_left, 40, 1 << 8 | 1 << 9);
            if (hit_left)
                leftEnd = hit_left.point;
            else
                leftEnd = leftLine.transform.position + pos_left * 40;
            LeftLineEndEffect.transform.eulerAngles = new Vector3(0, 0, angle_left + 90);
            LeftLineEndEffect.transform.position = leftEnd;
            leftLine.positionCount = 2;
            leftLine.SetPosition(0, leftLine.transform.position);
            leftLine.SetPosition(1, leftEnd);

            if (!isRightControl)
            {
                angle_right += Time.deltaTime * (PDG.Dungeon.instance.StageLevel > 2 ? Random.Range(30, 60) : Random.Range(20, 40));
                if (angle_right > 330)
                {
                    angle_right = 330;
                    isRightControl = true;
                }
            }
            else
            {
                angle_right -= Time.deltaTime * (PDG.Dungeon.instance.StageLevel > 2 ? Random.Range(30, 60) : Random.Range(20, 40));
                if (angle_right < 190)
                {
                    angle_right = 190;
                    isRightControl = false;
                }
            }
            Vector3 pos = GetPosition(rightLine.transform.position, angle_right);
            pos = pos - rightLine.transform.position;
            pos.Normalize();
            RaycastHit2D hit = Physics2D.BoxCast(rightLine.transform.position, new Vector2(0.5f, 0.1f), angle_right, pos, 40, 1 << 8 | 1 << 9);
            if (hit)
                rightEnd = hit.point;
            else
                rightEnd = rightLine.transform.position + pos * 40;
            RightLineEndEffect.transform.eulerAngles = new Vector3(0, 0, angle_right + 90);
            RightLineEndEffect.transform.position = rightEnd;
            rightLine.positionCount = 2;
            rightLine.SetPosition(0, rightLine.transform.position);
            rightLine.SetPosition(1, rightEnd);
            yield return new WaitForSeconds(Time.deltaTime);
        }

        CameraShaker._instance.StartShake(0.1f, Time.deltaTime, 2.5f, true);
        yield return StartCoroutine(Spake(true));
        yield return new WaitForSeconds(0.2f);

        int ranMax = 4;
        int prevPattern = 0;
        pattern = 0;
        float dTime = 0.0f;
        float t = 0.0f;
        while (!isDie)
        {
            if (pattern == 0 && !isDie)
            {
                if (!animator.GetBool("isMove")) animator.SetBool("isMove", true);
                bool isleft = Random.Range(0, 2) == 0 ? true : false;

                if (PDG.Dungeon.instance.StageLevel > 2)
                {
                    isLastLaser = true;
                    LastPattrenC = StartCoroutine(LastPattern());
                }
                while(t < dTime && !isDie)
                {
                    Vector3 pos = transform.localPosition;
                    t += Time.deltaTime;
                    if (pos.x > -5.5f && isleft)
                        transform.Translate(Vector3.left * moveSpeed * Time.deltaTime);
                    else
                        isleft = false;

                    if (pos.x < 5.5f && !isleft)
                        transform.Translate(Vector3.right * moveSpeed * Time.deltaTime);
                    else
                        isleft = true;
                    yield return new WaitForSeconds(Time.deltaTime);
                }
                t = 0.0f;
                while (!isDie)
                {
                    pattern = Random.Range(0, ranMax);
                    if (pattern != prevPattern && pattern != 0) break;
                }
                if (PDG.Dungeon.instance.StageLevel > 2)
                {
                    isLastLaser = false;
                    StopCoroutine(LastPattrenC);
                    LastPattrenC = null;
                    leftLine.gameObject.SetActive(false);
                    rightLine.gameObject.SetActive(false);
                    LeftLineEndEffect.SetActive(false);
                    RightLineEndEffect.SetActive(false);
                }
                if (animator.GetBool("isMove")) animator.SetBool("isMove", false);
            }
            else if (pattern == 1 && !isDie)
            {
                for (int i = 0; i < 5; i++) 
                {
                    if (isDie) break;
                    int _type = Random.Range(0, 2);
                    GameObject obj = Instantiate(bigBullet);
                    obj.transform.position = transform.position;
                    obj.transform.eulerAngles = Vector3.zero;
                    obj.GetComponent<BigBulletController>().mapObj = transform.parent;
                    obj.GetComponent<BigBulletController>()._type = _type;
                    obj.GetComponent<BigBulletController>().StartBall();
                    SpawnList.Add(obj);
                    yield return new WaitForSeconds(1.0f / stageLevelAnimSpeed);
                }
                dTime = 5.0f - stageLevelMinusTime;
                if (dTime <= 0.0f) dTime = 0.0f;
                prevPattern = 1;
                pattern = 0;
            }
            else if (pattern == 2 && !isDie)
            {
                animator.SetTrigger("CrouchTrigger");
                yield return new WaitForSeconds(5.0f / stageLevelAnimSpeed);
                animator.SetTrigger("EnergyEndTrigger");
                yield return new WaitForSeconds(1.0f / stageLevelAnimSpeed);
                dTime = 3.0f - stageLevelMinusTime;
                if (dTime <= 0.0f) dTime = 0.0f;
                prevPattern = 2;
                pattern = 0;
            }
            else if (pattern == 3 && !isDie)
            {
                animator.SetTrigger("AttackTrigger");
                if (PDG.Dungeon.instance.StageLevel > 1)
                {
                    yield return new WaitForSeconds(2f / stageLevelAnimSpeed);
                    animator.SetTrigger("AttackTrigger");
                }
                yield return new WaitForSeconds(1.3f / stageLevelAnimSpeed);
                dTime = 1.0f - stageLevelMinusTime;
                if (dTime <= 0.0f) dTime = 0.0f;
                prevPattern = 3;
                pattern = 0;
            }
            yield return new WaitForSeconds(Time.deltaTime);
        }
    }
    IEnumerator LastPattern()
    {
        Vector3 leftEnd = leftLine.transform.position;
        Vector3 rightEnd = rightLine.transform.position;

        float angle_right = 330.0f;
        float angle_left = 180.0f;
        bool isRightControl = false;
        bool isLeftControl = false;
        while (isLastLaser)
        {
            leftLine.gameObject.SetActive(true);
            rightLine.gameObject.SetActive(true);
            LeftLineEndEffect.SetActive(true);
            RightLineEndEffect.SetActive(true);

            if (!isLeftControl)
            {
                angle_left += Time.deltaTime * (PDG.Dungeon.instance.StageLevel > 2 ? Random.Range(30, 60) : Random.Range(20, 40));
                if (angle_left > 330)
                {
                    angle_left = 330;
                    isLeftControl = true;
                }
            }
            else
            {
                angle_left -= Time.deltaTime * (PDG.Dungeon.instance.StageLevel > 2 ? Random.Range(30, 60) : Random.Range(20, 40));
                if (angle_left < 190)
                {
                    angle_left = 190;
                    isLeftControl = false;
                }
            }
            Vector3 pos_left = GetPosition(leftLine.transform.position, angle_left);
            pos_left = pos_left - leftLine.transform.position;
            pos_left.Normalize();
            RaycastHit2D hit_left = Physics2D.BoxCast(leftLine.transform.position, new Vector2(0.5f, 0.1f), angle_left, pos_left, 40, 1 << 8 | 1 << 9);
            if (hit_left)
                leftEnd = hit_left.point;
            else
                leftEnd = leftLine.transform.position + pos_left * 40;
            LeftLineEndEffect.transform.eulerAngles = new Vector3(0, 0, angle_left + 90);
            LeftLineEndEffect.transform.position = leftEnd;
            leftLine.positionCount = 2;
            leftLine.SetPosition(0, leftLine.transform.position);
            leftLine.SetPosition(1, leftEnd);

            if (!isRightControl)
            {
                angle_right += Time.deltaTime * (PDG.Dungeon.instance.StageLevel > 2 ? Random.Range(30, 60) : Random.Range(20, 40));
                if (angle_right > 330)
                {
                    angle_right = 330;
                    isRightControl = true;
                }
            }
            else
            {
                angle_right -= Time.deltaTime * (PDG.Dungeon.instance.StageLevel > 2 ? Random.Range(30, 60) : Random.Range(20, 40));
                if (angle_right < 190)
                {
                    angle_right = 190;
                    isRightControl = false;
                }
            }
            Vector3 pos = GetPosition(rightLine.transform.position, angle_right);
            pos = pos - rightLine.transform.position;
            pos.Normalize();
            RaycastHit2D hit = Physics2D.BoxCast(rightLine.transform.position, new Vector2(0.5f, 0.1f), angle_right, pos, 40, 1 << 8 | 1 << 9);
            if (hit)
                rightEnd = hit.point;
            else
                rightEnd = rightLine.transform.position + pos * 40;
            RightLineEndEffect.transform.eulerAngles = new Vector3(0, 0, angle_right + 90);
            RightLineEndEffect.transform.position = rightEnd;
            rightLine.positionCount = 2;
            rightLine.SetPosition(0, rightLine.transform.position);
            rightLine.SetPosition(1, rightEnd);
            yield return new WaitForSeconds(Time.deltaTime);
        }
    }
    private void Update()
    {
        if (isWait) 
        {
            RaycastHit2D hit = Physics2D.CircleCast(transform.position + new Vector3(0, -17), 15, Vector2.zero, 0, 1 << 9);
            if (hit)
            {
                SoundManager.instance.StartAudio(new string[1] { "BGM/Boss_2" }, VOLUME_TYPE.BGM, null, true);
                isWait = false;
            }
        }

        if (hitTime > 0.0f)
        {
            hitTime -= Time.deltaTime;
        }
        else if (hitTime <= 0.0f)
        {
            isHit = false;
        }
    }
    public void SetDamage(float _damage, Vector3 _pos)
    {
        if (PDG.Dungeon.instance.isPause) return;
        if (isInvincibility) return;
        if (isDie) return;

        GameObject obj = ObjManager.Call().GetObject("DamageHit_Boss2");
        if (obj)
        {
            obj.transform.position = _pos;
            obj.SetActive(true);
        }


        GameObject damageObjOrigin = Resources.Load<GameObject>("DamageText");
        GameObject damageObj = Instantiate(damageObjOrigin);
        damageObj.transform.SetParent(GameObject.Find("Canvas").transform);
        damageObj.transform.SetSiblingIndex(16);
        damageObj.transform.localScale = Vector3.one;
        damageObj.GetComponent<DamageText>().InitSetting(_damage, gameObject);

        mobInfo.curHp -= _damage;
        if(!isHit)
        {
            isHit = true;
            hitTime = 0.25f;
            GetComponent<AudioSource>().volume =  (SoundManager.isMute || (PDG.Dungeon.instance == null ? false : PDG.Dungeon.instance.isPause) || (PDG.Player.instance == null ? false : PDG.Player.instance.isDie)) ? 0 : SoundManager.EffectVolume;
            GetComponent<AudioSource>().Play();
        }
        if (HitCoroutine == null) HitCoroutine = StartCoroutine(HitEffect());

        if(mobInfo.curHp / mobInfo.maxHp < 0.9f && isLock)
        {
            isLock = false;
        }


        if (mobInfo.curHp <= 0)
        {
            PDG.Dungeon.instance.isBossRushStart = false;
            string tId = mobData.ID;
            if (tId.Contains("_t")) tId = tId.Substring(0, tId.Length - 3);
            StateGroup.instance.isClearTimeStart = false;
            StateGroup.instance.isBattleTimeStart = false;
            SaveAndLoadManager.instance.SaveCollection(new CollectionData { _id = tId, _isOpen = true, _isWeapon = false });
            SoundManager.instance.StopAllSound();
            UbhObjectPool.instance.ReleaseAllBullet();
            ResultPanelScript.instance.bossList.Add(2);
            ScreenShot.instance.Screenshot(gameObject);
            isDie = true;
            PDG.Player.instance.fInvincibleTime_offensive_01 = 10;
            GetComponent<PolygonCollider2D>().enabled = false;
            subCollision.SetActive(false);
            StartCoroutine(DieAction());
        }
    }
    IEnumerator DieAction()
    {
        if (GetComponent<PolygonCollider2D>()) Destroy(GetComponent<PolygonCollider2D>());
        for (int i = 0; i < SpawnList.Count; i++)
        {
            if (SpawnList[i]) 
            {
                Destroy(SpawnList[i]);
            }
        }

        dieEffect_0.SetActive(true);
        dieEffect_1.SetActive(true);
        CameraShaker._instance.StartShake(0.1f, Time.deltaTime, 2.5f, true);
        yield return StartCoroutine(Spake());
        yield return new WaitForSeconds(0.2f);
        PDG.Player.instance.isCameraOff = true;
        (FindObjectOfType(typeof(PDG.Camera)) as PDG.Camera).target = transform;
        animator.SetTrigger("DieTrigger");
        SoundManager.instance.StartAudio(new string[1] { "Monster/boss_shaker" }, VOLUME_TYPE.EFFECT);
        yield return new WaitForSecondsRealtime(0.3f);
        Time.timeScale = 0.1f;
        yield return new WaitForSecondsRealtime(3.0f);
        Time.timeScale = 1.0f;
        yield return new WaitForSeconds(0.5f);
        StartCoroutine(EndEventCoroutine());
    }
    IEnumerator EndEventCoroutine()
    {
        string tId = mobData.ID;
        if (tId.Contains("_t")) tId = tId.Substring(0, tId.Length - 3);
        if (PDG.Dungeon.instance.isBossRush)
        {
            List<string> dropItems = new List<string>();
            int skullCount = 10 * (PDG.Dungeon.instance.StageLevel + 1);
            for (int i = 0; i < skullCount; i++) dropItems.Add("glowing_skull");
            dropItems.Add("recovery_syringe_2");
            dropItems.Add("recovery_syringe_2");
            dropItems.Add("recovery_syringe_2");
            for (int i = 0; i < ItemBoxs.Length; i++)
            {
                ItemBoxs[i].gameObject.SetActive(true);
                ItemBoxs[i]._id = tId;
                if (ItemBoxs[i].isMaterial)
                {
                    ItemBoxs[i].gameObject.SetActive(false);
                }
                else
                {
                    ItemBoxs[i].itemName = dropItems.ToArray();
                }
            }
        }
        else
        {
            int ata = 0;
            List<ItemsData> iDatas = null;
            while (true)
            {
                ChestPerData data = System.Array.Find(SheetManager.Instance.ChestPerDB.dataArray, item => item.Stagelevel.Equals(PDG.Dungeon.instance.StageLevel));
                string BoxGrade = data.Chest[SaveAndLoadManager.GetBoxGrade(data)];
                BoxGrade = BoxGrade.Substring(BoxGrade.Length - 1, 1);
                iDatas = System.Array.FindAll(SheetManager.Instance.ItemsDB.dataArray, item => item.Grade.ToUpper().Equals(BoxGrade.ToUpper())).ToList();
                for (int i = 0; i < iDatas.Count; i++)
                {
                    Sprite s = Resources.Load<Sprite>("Item/" + iDatas[i].Type1 + "/" + iDatas[i].Type2 + "/" + iDatas[i].ID);
                    if (s == null || !Inventorys.Instance.UnlockAndMaxCountCheck(iDatas[i].ID))
                    {
                        iDatas.RemoveAt(i);
                        i--;
                        continue;
                    }
                }
                ata++;
                if (ata >= 1000) break;
                if (iDatas.Count > 0) break;
            }
            List<string> dropItems = new List<string>();
            if (iDatas.Count > 0) dropItems.Add(iDatas[UnityEngine.Random.Range(0, iDatas.Count)].ID);
            int skullCount = UnityEngine.Random.Range(10, 20);
            if (SaveAndLoadManager.nDifficulty != 0)
            {
                skullCount *= (SaveAndLoadManager.nDifficulty * 2) - 1;
            }
            for (int i = 0; i < skullCount; i++) dropItems.Add("glowing_skull");
            //dropItems.Add("key_normal");
            dropItems.Add("muscle_enhancer_body");
            int t = SaveAndLoadManager.instance.LoadItem("Boss_eyes_fire");
            int t2 = SaveAndLoadManager.instance.LoadItem("HellFogeeFlag");
            int aa = Random.Range(0, 100);
            if (t == -1 && t2 != 1 && (aa >= 30 && aa <= 70)) dropItems.Add("eyes_fire");
            if (UnityEngine.Random.Range(0, 100) < 50) dropItems.Add("muscle_enhancer_body");
            yield return new WaitForSeconds(1.0f);
            for (int i = 0; i < ItemBoxs.Length; i++)
            {
                ItemBoxs[i].gameObject.SetActive(true);
                ItemBoxs[i]._id = tId;
                if (ItemBoxs[i].isMaterial)
                {
                    int ran = Random.Range(1, 4);
                    int ran2 = Random.Range(0, 3);
                    int ran3 = Random.Range(0, 3);
                    string[] ttt = new string[1 + ran];
                    for (int j = 0; j < 1 + ran; j++)
                    {
                        if (j == 0) ttt[j] = "boss_Boss_IceGolem";
                        else ttt[j] = "normal_" + (ran2 == 0 ? "Cells" + (ran3 == 0 ? "_0" : "_1") : (ran2 == 1 ? "Ore" : "Parts" + (ran3 == 0 ? "_0" : (ran3 == 1 ? "_1" : "_2"))));
                    }
                    ItemBoxs[i].itemName = ttt;
                }
                else
                {
                    ItemBoxs[i].itemName = dropItems.ToArray();
                }
            }
            yield return new WaitForSeconds(1.0f);

            if (SteamStatsAndAchievements.instance != null) SteamStatsAndAchievements.instance.SettingStat(tId + "_hunting");
            if (SteamStatsAndAchievements.instance != null) SteamStatsAndAchievements.instance.SettingStat("stat_stage_clear_" + PDG.Dungeon.instance.StageLevel);
            if (SteamStatsAndAchievements.instance != null) SteamStatsAndAchievements.instance.SettingStat("stat_hunting_boss_count");

            List<AchievementData> savedatas = new List<AchievementData>();
            AchievementsData aData_0 = System.Array.Find(SheetManager.Instance.AchievementsDB.dataArray, item => item.ID.Equals("huntermaster_golem"));
            savedatas.Add(new AchievementData { _id = aData_0.ID, _cur = 1, _max = aData_0.MAX, _clear = false });
            AchievementsData aData_1 = System.Array.Find(SheetManager.Instance.AchievementsDB.dataArray, item => item.ID.Equals("stage_clear_" + PDG.Dungeon.instance.StageLevel));
            savedatas.Add(new AchievementData { _id = aData_1.ID, _cur = 1, _max = aData_1.MAX, _clear = false });

            if (StateGroup.instance.nShotByBoss == 0)
            {
                AchievementsData aData = System.Array.Find(SheetManager.Instance.AchievementsDB.dataArray, item => item.ID.Equals("godcontrol"));
                savedatas.Add(new AchievementData { _id = aData.ID, _cur = 1, _max = aData.MAX, _clear = false });
            }
            if (StateGroup.instance.nShotByHunter == 0)
            {
                AchievementsData aData = System.Array.Find(SheetManager.Instance.AchievementsDB.dataArray, item => item.ID.Equals("nodamageclear"));
                savedatas.Add(new AchievementData { _id = aData.ID, _cur = 1, _max = aData.MAX, _clear = false });
            }
            if (PDG.Player.instance.nowWeaponName.Contains("tentacle_arm"))
            {
                AchievementsData aData = System.Array.Find(SheetManager.Instance.AchievementsDB.dataArray, item => item.ID.Equals("tantaclemaster"));
                savedatas.Add(new AchievementData { _id = aData.ID, _cur = 1, _max = aData.MAX, _clear = false });
            }
            if (StateGroup.instance.fClearTime <= 720)
            {
                AchievementsData aData = System.Array.Find(SheetManager.Instance.AchievementsDB.dataArray, item => item.ID.Equals("possible_0"));
                savedatas.Add(new AchievementData { _id = aData.ID, _cur = 1, _max = aData.MAX, _clear = false });
            }
            if (StateGroup.instance.fClearTime <= 480)
            {
                AchievementsData aData = System.Array.Find(SheetManager.Instance.AchievementsDB.dataArray, item => item.ID.Equals("possible_1"));
                savedatas.Add(new AchievementData { _id = aData.ID, _cur = 1, _max = aData.MAX, _clear = false });
            }
            if (StateGroup.instance.fClearTime <= 180)
            {
                AchievementsData aData = System.Array.Find(SheetManager.Instance.AchievementsDB.dataArray, item => item.ID.Equals("possible_2"));
                savedatas.Add(new AchievementData { _id = aData.ID, _cur = 1, _max = aData.MAX, _clear = false });
            }

            SaveAndLoadManager.instance.SaveAchievements(savedatas.ToArray());

            PDG.Dungeon.instance.isStageBoss = false;
            PDG.Dungeon.instance.StageLevel++;
            PDG.Dungeon.instance.nowStageStep = 0;
            SaveAndLoadManager.instance.GameSave(Inventorys.Instance.slots);
            if (SteamStatsAndAchievements.instance != null) SteamStatsAndAchievements.instance.UnlockAchievementToPublic("Boss_2_Clear");
        }
        PDG.Player.instance.isCameraOff = false;
        (FindObjectOfType(typeof(PDG.Camera)) as PDG.Camera).target = PDG.Player.instance.transform;
        SoundManager.instance.StartAudio(new string[1] { "warp_open" }, VOLUME_TYPE.EFFECT);
        GameObject.Find("Canvas").transform.Find("TutorialText").gameObject.SetActive(true);
        GameObject.Find("Canvas").transform.Find("TutorialText").GetComponent<Text>().text = LocalizeManager.GetLocalize("boss_open_title");
        if (!PDG.Dungeon.instance.isTestMode2)
            GameObject.Find("Canvas").transform.Find("ExitArrow").gameObject.SetActive(true);
        for (int i = 0; i < nextHole.Length; i++)
        {
            nextHole[i].GetComponent<DungeonNextController>().isBossOpen = true;
            nextHole[i].GetComponent<DungeonNextController>().sr.sprite = nextHole[i].GetComponent<DungeonNextController>().endSprite;
        }
        StateGroup.instance.OpenResult(tId);
    }
    IEnumerator Spake(bool offObl = false)
    {
        float a = 0.0f;
        while (true)
        {
            a += Time.fixedUnscaledDeltaTime * 2;
            spakeImage.color = new Color(1, 1, 1, a);
            if (a > 1.0f) break;
            yield return new WaitForSecondsRealtime(Time.fixedUnscaledDeltaTime);
        }
        spakeImage.color = new Color(1, 1, 1, 1);
        if (offObl)
        {
            leftLine.gameObject.SetActive(false);
            leftLine2.gameObject.SetActive(false);
            leftLine3.gameObject.SetActive(false);
            LeftLineEndEffect.SetActive(false);
            LeftLineEndEffect2.SetActive(false);
            LeftLineEndEffect3.SetActive(false);
            rightLine.gameObject.SetActive(false);
            rightLine2.gameObject.SetActive(false);
            rightLine3.gameObject.SetActive(false);
            RightLineEndEffect.SetActive(false);
            RightLineEndEffect2.SetActive(false);
            RightLineEndEffect3.SetActive(false);
            transform.Find("Chain").gameObject.SetActive(false);
        }
        a = 1.0f;
        GameObject.Find("Camera").GetComponent<PDG.Camera>().isTestScreenClear = true;
        SoundManager.instance.StartAudio(new string[1] { "BossSpake" }, VOLUME_TYPE.EFFECT);
        while (true)
        {
            a -= Time.fixedUnscaledDeltaTime * 2;
            spakeImage.color = new Color(1, 1, 1, a);
            if (a <= 0.0f) break;
            yield return new WaitForSecondsRealtime(Time.fixedUnscaledDeltaTime);
        }
        GameObject.Find("Camera").GetComponent<PDG.Camera>().isTestScreenClear = true;
        spakeImage.color = new Color(1, 1, 1, 0);
    }
    public void ItemDrop(string[] itemName)
    {
        string tId = mobData.ID;
        if (tId.Contains("_t")) tId = tId.Substring(0, tId.Length - 3);
        UnlockListsData[] pikoUnlockDatas = System.Array.FindAll(SheetManager.Instance.UnlockListsDB.dataArray, item => item.NPCID.Equals(tId));
        for (int i = 0; i < pikoUnlockDatas.Length; i++)
        {
            if (SaveAndLoadManager.instance.LoadUnlock(pikoUnlockDatas[i].Itemid) < 1)
            {
                SaveAndLoadManager.instance.SaveUnlock(pikoUnlockDatas[i].Itemid, 1);
                ItemsData iData = System.Array.Find(SheetManager.Instance.ItemsDB.dataArray, item => item.ID.Equals(pikoUnlockDatas[i].Itemid));
                Inventorys.Instance.ShowGetItemUI("monster_unlockitem", Resources.Load<Sprite>("Item/" + iData.Type1 + "/" + iData.Type2 + "/" + iData.ID));
            }
        }

        Vector3 tPos = transform.parent.position;
        tPos += dropPosition;
        for (int i = 0; i < itemName.Length; i++)
        {
            float angle = -90.0f;
            angle += (i * (360.0f / itemName.Length));
            Vector3 pos = GetPosition(tPos, angle);
            Vector3 dir = pos - tPos;
            dir.Normalize();
            GameObject obj = ObjManager.Call().GetObject("Item");
            if (obj != null)
            {
                Item _item = obj.GetComponent<Item>();
                _item._Item = new ItemInfo
                {
                    Data = Inventorys.Instance.CopyItem(System.Array.Find(SheetManager.Instance.ItemsDB.dataArray, item => item.ID.Equals(itemName[i])))
                };
                _item.dir = dir;
                _item.itemCount = 1;
                _item.rPow = 1;
                float startDis2 = 2.0f;
                float angle2 = angle += 90.0f;
                if (angle2 <= 180) startDis2 -= (angle2 / 150.0f);
                else startDis2 -= ((180.0f / 150.0f) + ((180.0f - angle2) / 150.0f));

                obj.transform.position = pos + (dir * startDis2);
                obj.SetActive(true);
            }
        }
        PDG.Dungeon.DropMaterial(mobData.Type, mobData.Type2, transform.position);
    }
    IEnumerator HitEffect()
    {
        while (isHit && !isDie)
        {
            sr.color = HitColor;
            yield return new WaitForSeconds(0.06f);
            sr.color = HitColor2;
            yield return new WaitForSeconds(0.06f);
            sr.color = HitColor3;
            yield return new WaitForSeconds(0.06f);
            sr.color = HitColor4;
            yield return new WaitForSeconds(0.06f);
            sr.color = new Color32(229, 31, 65, 255);
        }
        HitCoroutine = null;
    }
    public void SmallBallFire()
    {
        for (int i = 0; i < (PDG.Dungeon.instance.StageLevel < 2 ? 1 : 2); i++)
        {
            GameObject obj = Instantiate(smallBullet);
            Vector3 pos = transform.position;
            pos.x += Random.Range(-4, 4);
            obj.transform.position = pos;
            obj.GetComponent<SmallBulletController>().startDir = new Vector3(Random.Range(-1.0f, 1.0f), Random.Range(-1, -0.1f));
            SpawnList.Add(obj);
        }
    }
    public void AttackFire()
    {
        GameObject obj = Instantiate(attackForce);
        Vector3 pos = transform.position;
        pos.y += 5.0f;
        obj.transform.position = pos;
        if (PDG.Dungeon.instance.StageLevel > 1)
        {
            obj.transform.localEulerAngles = new Vector3(0, 0, GetAngle(transform.position, PDG.Player.instance.transform.position) - 180f);
            Vector3 dir = PDG.Player.instance.transform.position - transform.position;
            dir.Normalize();
            obj.GetComponent<Rigidbody2D>().AddForce(dir * 250 * Time.deltaTime, ForceMode2D.Impulse);
        }
        else
        {
            obj.transform.localEulerAngles = new Vector3(0, 0, 90);
            obj.GetComponent<Rigidbody2D>().AddForce(Vector3.down * 250 * Time.deltaTime, ForceMode2D.Impulse);
        }
        if(PDG.Dungeon.instance.StageLevel > 1)
        {
            obj.transform.Find("RandomAllRange").gameObject.SetActive(true);
            obj.transform.Find("RandomAllRange").GetComponent<UbhShotCtrl>().StartShotRoutine();
        }
        SpawnList.Add(obj);
    }
    #region -- Tool --
    public static float GetAngle(Vector3 vStart, Vector3 vEnd)
    {
        Vector3 v = vEnd - vStart;

        return Mathf.Atan2(v.y, v.x) * Mathf.Rad2Deg;
    }
    public static Vector3 GetPosition(Vector3 vStart, float angle)
    {
        float x = Mathf.Cos(Mathf.Deg2Rad * angle) + vStart.x;
        float y = Mathf.Sin(Mathf.Deg2Rad * angle) + vStart.y;

        return new Vector3(x, y, 0);
    }
    private int GetPercentege(float _per, string[] list)
    {
        int[] per = new int[Mathf.FloorToInt(100 * list.Length)];
        for (int i = 0; i < per.Length; i++) per[i] = -1;

        for (int i = 0; i < list.Length; i++)
        {
            for (int j = 0; j < Mathf.FloorToInt(_per * 100); j++)
            {
                per[(i * 100) + j] = i;
            }
        }
        if (per.Length == 0) return -1;
        return per[Random.Range(0, per.Length)];
    }
    #endregion
}
