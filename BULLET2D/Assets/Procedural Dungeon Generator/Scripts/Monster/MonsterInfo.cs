﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MOB
{
    public class MonsterInfo : MonoBehaviour
    {
        public float maxHp = 100.0f;
        public float curHp = 100.0f;
        public float maxMp = 100.0f;
        public float curMp = 100.0f;
    }
}