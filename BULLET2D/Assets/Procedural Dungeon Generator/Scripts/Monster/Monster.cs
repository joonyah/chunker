﻿using MoreMountains.Feedbacks;
using Pathfinding;
using PDG;
using SplatterSystem;
using SplatterSystem.TopDown;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace MOB
{
    public enum Direction
    {
        None = 0, Down = 1, Right = 2, Left = 3, RightDown = 4, LeftDown = 5, Up = 6, RightUp = 7, LeftUp = 8, MAX = 9
    }

    [System.Serializable]
    public struct TagGunPosition
    {
        public string gunType;
        public Vector3 gunPos;
        public Vector3 gunFirePos;
        public Sprite sprite;
    }

    [RequireComponent(typeof(Rigidbody2D))]
    [RequireComponent(typeof(MonsterInfo))]
    [RequireComponent(typeof(Seeker))]
    public class Monster : MonoBehaviour
    {
        #region -- Variable --
        [Header("Predator")]
        [SerializeField] private Sprite[] dieFragments;
        [SerializeField] private GameObject controllerObj;
        public bool isUsing = false;
        [SerializeField] private float fTime = 2.0f;
        [SerializeField] private GameObject predatorEndEffect1;
        [SerializeField] private GameObject predatorEndEffect2;
        [SerializeField] private GameObject predatorEndEffect3;

        long pushCheck = 0;
        long pushCheck2 = 0;
        GameObject timerObj;
        bool isMove222 = false;
        [Header("Setting")]
        string _spritePath;
        Sprite[] _sprites;
        public TagGunPosition[] gunPosition;
        public GameObject simbolObj;

        [Header("Attack")]
        public float bulletSpeed = 10.0f;
        public float fAttackDelay = 0.5f;
        public float fAttackDelayNow = 0.5f;
        public float fBulletDelay = 0.0f;
        public int nFireCount = 1;
        public float nFireAngle = 0;
        public ND_VariaBULLET.FireBullet FB;
        public bool isAuto = false;
        public bool isAutoAim = false;
        public int nModCount = 1;
        public bool isElemental = false;
        public float fElementalTime = 0.0f;
        public float fBulletSize = 1.0f;
        public float knock = 0.0f;
        public string strElementalResis = string.Empty;
        public string mobType = string.Empty;


        private float fAttackInplaceTime = 1.0f;
        private float fAttackInplaceTimeNow = 0.0f;

        public float speed = 2f;
        public float speedOrigin = 2f;
        [SerializeField] private LayerMask obstacles;

        public Animator animator;
        public Animator attackAnim;
        public SpriteRenderer attackSr;
        private SpriteRenderer sr;
        [SerializeField] private Vector2 targetPosition;
        public MonsterInfo mobInfo;
        private BoxCollider2D bCol;
        public Rigidbody2D rigid;

        public Direction direction = Direction.None;
        public Direction directionWeapons = Direction.None;

        [Header("Weapons")]
        public bool isWeaponMode = false;
        [SerializeField] private GameObject WeaponsSlot;
        [SerializeField] private Transform WeaponsFireTransform;
        [SerializeField] private float angle = 0.0f;
        [SerializeField] private bool isAttack = false;
        public float fAttackRange = 15;
        public float fBulletRange = 15;
        public GameObject targetObj;
        [SerializeField] private AudioSource _audio;
        [Header("AI Movement")]
        //플레이어에게 갈 수 있는지 체크
        public bool isPathPossible = false;
        //이동할 좌표를 바꿔야할 상태인지 체크
        public bool isMoveChange = true;
        // 초마다 몇번의 길 업데이트를 할것인지
        public float updateRate = 2f;
        [HideInInspector] public bool pathIsEnabled = false;
        // AI에서 다음 웨이 포인트까지 계속되는 웨이 포인트까지의 최대 거리
        public float nextWaypointDistance = 1;
        // 우리가 현재 움직이고있는 웨이 포인트
        private int currentWaypoint = 0;
        // 계산 된 경로
        public Path path;
        public bool isTargeting = false;
        private Seeker seeker;
        public Vector3 dir;

        public bool isFire = true;
        public bool isDie = false;
        public bool isHit = false;
        private Coroutine HitCoroutine = null;
        [SerializeField] private float HitEffectDelay = 0.01f;
        public bool isMoveStop = false;

        [Header("Color")]
        public Color HitColor = new Color();
        public Color HitColor2 = new Color();
        public Color HitColor3 = new Color();
        public Color HitColor4 = new Color();
        public Color DieColor = new Color();

        [Header("Warp Monster")]
        public bool isWarpMonster = false;

        [Header("KnockBack")]
        private Vector2 knockDir;
        private float knockDis;
        public int knockSpeed = 3;
        public float fragmentPow = 2.0f;

        public MonstersData mobData;

        [Header("Grab")]
        public bool isGrab = false;
        public bool isPull = false;
        public Transform tGrab;
        [SerializeField] private float collRadius = 1;

        [Header("Feedbacks")]
        public MMFeedbacks WeaponUsedMMFeedback;
        public Material FeedbackMaterial;
        public ParticleSystem _particle;

        [Header("BossTrigger")]
        public bool isBoss = false;

        [Header("튜토 전용")]
        public bool isTutorial = false;
        public int tutoType = -1;
        public string tutoName;
        public bool isTutoGrab;

        [Header("Elemental")]
        public GameObject IceElemental;
        public bool isFreeze = false;
        public GameObject FireElemental;
        public float fDotTime = 0.0f;
        float fDotHitTime = 0.0f;
        public ELEMENTAL_TYPE nowElementalType;

        public bool isRoaming = false;
        public bool isNotKnock = false;
        public bool isBomb = false;

        [SerializeField] private AudioSource _audio2;
        [SerializeField] private AudioClip dieClip;
        [SerializeField] private AudioClip hitClip;
        [SerializeField] private AudioClip hitClip2;
        [SerializeField] private AudioClip bossShaker;
        [SerializeField] private AudioClip pikoDown;
        [SerializeField] private AudioSource _audio3;
        [SerializeField] private AudioSource _audio4;

        [SerializeField] private GameObject ExclamationObj;
        public GameObject hpBar;

        [Header("ArmPiece")]
        [SerializeField] private GameObject[] armPieces;
        public bool isArm;
        [SerializeField] private bool isArmAttack;
        [SerializeField] private GameObject armObj;
        Coroutine ArmCoroutine = null;
        float hittime = 0.0f;

        GameObject obj3;

        public bool isNoneInfo = false;

        [Header("Ghost")]
        [SerializeField] private bool isHideGhost = false;
        [SerializeField] private bool isHide = false;
        [SerializeField] private float hideTime = 3.0f;

        private int nJumpCount = 0;
        private int nJumpCountMax = 0;
        public bool isJump = false;

        [SerializeField] private LineRenderer leftLine;
        [SerializeField] private LineRenderer rightLine;
        [SerializeField] private GameObject LeftLineEndEffect;
        [SerializeField] private GameObject RightLineEndEffect;
        [SerializeField] private GameObject pixelfireEffect;
        public bool isSub = false;
        public string elementalName = string.Empty;
        [SerializeField] private GameObject dieEffect;
        SpriteOutline sOutline;
        SpriteOutline sOutline2;
        public bool isUnique = false;

        [SerializeField] private float subBossTime = 200.0f;
        [SerializeField] private GameObject JumpShadowObj;
        [SerializeField] private GameObject whiteAntHitEffect;
        [SerializeField] private float dieTime = 0.0f;
        [SerializeField] private bool isDieEnd = false;
        bool isSubSpawn = false;
        Coroutine rollingCoroutine = null;
        [SerializeField] private Vector2[] mobCollSize;
        [SerializeField] private Vector2[] mobCollOffset;
        [SerializeField] private List<string> mobcollID = new List<string>();
        [SerializeField] private GameObject minimapObj;
        [Header("MIMIC")]
        public bool isMimic = false;
        public bool isMiMicWait = true;
        [SerializeField] private int mimicHitCount = 3;
        [SerializeField] private GameObject BackCollision;

        [SerializeField] private SpriteRenderer minimapSR;
        [SerializeField] private Sprite defaultSimbol;
        [SerializeField] private Sprite mimicSimbol;
        [SerializeField] private GameObject monsterDeleteEffect;

        [Header("Goblin")]
        public bool isGoblin = false;
        [SerializeField] private int nGoblinMoveCount = 0;
        [SerializeField] private int nGoblinMoveCountMax = 5;
        [SerializeField] private float fGoblinDropPer = 0.5f;
        [SerializeField] private float fGoblinMoveTime;
        [SerializeField] private GameObject GoblinPortalObj;
        [SerializeField] private bool isGoblinMove = false;
        [SerializeField] private bool isGoblinEnd = false;
        [SerializeField] private Font _font;
        [SerializeField] private List<string> goblinItemList = new List<string>();
        [Header("Material")]
        [SerializeField] private Material[] stageMaterials;

        [SerializeField] private GameObject RootManAttackFireObj;
        [SerializeField] private GameObject RootManMoveObj;
        #endregion
        #region -- Method --
        #region -- Behavior Method (Init, Update, Die, Hit, Damage etc.. --
        private void Awake()
        {
            animator = GetComponent<Animator>();
            sr = GetComponent<SpriteRenderer>();
            seeker = GetComponent<Seeker>();
            mobInfo = GetComponent<MonsterInfo>();
            bCol = GetComponent<BoxCollider2D>();
            rigid = GetComponent<Rigidbody2D>();
            sOutline = GetComponent<SpriteOutline>();
            if(transform.Find("Gun"))
            {
                if(transform.Find("Gun").GetComponent<SpriteOutline>())
                    sOutline2 = transform.Find("Gun").GetComponent<SpriteOutline>();
            }
        }
        private void Start()
        {
            if (isNoneInfo) return;
            if (isTutorial)
            {
                InitSetting(mobData);
                fAttackDelayNow = Random.Range(-1.0f, 1.0f);
            }
            obj3 = Resources.Load<GameObject>("Object/Prefabs/ant_poison");
            Physics2D.IgnoreLayerCollision(8, 12, true);

            AnimatorStateInfo _stateInfo = animator.GetCurrentAnimatorStateInfo(0);
            animator.Play(_stateInfo.fullPathHash, -1, Random.Range(0.0f, 1.0f));

            nJumpCountMax = Random.Range(3, 6);

            timerObj = GameObject.Find("Canvas").transform.Find("Timer").gameObject;
        }
        public void InitSetting(MonstersData _mobData)
        {
            if (controllerObj) controllerObj.SetActive(false);
            if (sOutline) sOutline.Clear();
            if (sOutline2) sOutline2.Clear();
            if (FireElemental.activeSelf) FireElemental.SetActive(false);
            GetComponent<SpriteRenderer>().material = stageMaterials[0];
            mobData = _mobData;
            fAttackRange = mobData.Range;
            fBulletRange = mobData.Bulletrange;
            speedOrigin = mobData.Movespeed;
            isGrab = false;
            isPull = false;
            isMoveStop = false;
            isMove222 = false;
            isWeaponMode = false;
            isDie = false;

            string tId = mobData.ID;
            if (tId.Contains("_t")) tId = tId.Substring(0, tId.Length - 3);

            if (animator != null) animator.enabled = true;
            else
            {
                if (GetComponent<Animator>())
                {
                    animator = GetComponent<Animator>();
                    animator.enabled = true;
                }
            }

            isTargeting = false;
            isWarpMonster = false;

            if (bCol == null)
            {
                if (GetComponent<BoxCollider2D>())
                {
                    bCol = GetComponent<BoxCollider2D>();
                    bCol.enabled = true;
                }
            }
            else
            {
                bCol.enabled = true;
            }

            if (rigid == null) 
            {
                rigid = GetComponent<Rigidbody2D>();
                rigid.bodyType = RigidbodyType2D.Dynamic;
                rigid.simulated = true;
            }
            else
            {
                rigid.bodyType = RigidbodyType2D.Dynamic;
                rigid.simulated = true;
            }

            mobInfo.curHp = mobInfo.maxHp;
            isHit = false;
            isAttack = false;
            isPathPossible = false;
            direction = Direction.Left;

            if(sr != null)
            {
                sr.color = Color.white;
                sr.sortingOrder = 1;
                sr.sprite = Resources.Load<Sprite>("Mob/" + tId + "/" + animator.runtimeAnimatorController.name + "/idle/idle_downside/idle_downside_0");

                if (mobData.ID.Contains("mummy")) sr.flipX = true;
                else sr.flipX = false;
            }

            simbolObj.SetActive(true);
            minimapObj.SetActive(true);

            targetPosition = transform.position;
            animator.SetInteger("Direction", (int)direction);
            int index = System.Array.FindIndex(gunPosition, item => item.gunType.Equals(tId));
            if (index != -1 && !_mobData.Type.Equals("boss")) 
            {
                WeaponsSlot.SetActive(isFire);
                WeaponsSlot.transform.localPosition = gunPosition[index].gunPos;
                WeaponsFireTransform.localPosition = gunPosition[index].gunFirePos;
                WeaponsSlot.GetComponent<SpriteRenderer>().sprite = gunPosition[index].sprite;
            }
            else
            {
                WeaponsSlot.SetActive(false);
            }

            isMoveChange = true;
            _audio.clip = Resources.Load<AudioClip>("Sound/Monster/" + tId);
            fDotHitTime = 0.0f;
            pixelfireEffect.SetActive(false);

            if (mobData.ID.Contains("hoodie_fireshot") || mobData.ID.Contains("hoodie_pistol") || mobData.ID.Contains("hoodie_cannon"))
            {
                FireElemental.transform.localPosition = new Vector3(0, 1.75f, 0);
                ExclamationObj.transform.localPosition = new Vector3(0, 1.75f, 0);
            }
            else if (mobData.ID.Contains("dust") || mobData.ID.Contains("gslime") || mobData.ID.Contains("bat_bomb"))
            {
                FireElemental.transform.localPosition = new Vector3(0, 0.72f, 0);
                ExclamationObj.transform.localPosition = new Vector3(0, 0.72f, 0);
            }
            else if (mobData.ID.Contains("beholder") || mobData.ID.Contains("piko"))
            {
                FireElemental.transform.localPosition = new Vector3(0, 1.2f, 0);
                ExclamationObj.transform.localPosition = new Vector3(0, 1.2f, 0);
            }
            else
            {
                FireElemental.transform.localPosition = new Vector3(0, 1, 0);
                ExclamationObj.transform.localPosition = new Vector3(0, 1.2f, 0);
            }

            if (mobData.ID.Contains("ghost_hide")) isHideGhost = true;
            else isHideGhost = false;

            _audio3.Stop();

            for (int i = 0; i < armPieces.Length; i++)
            {
                armPieces[i].SetActive(false);
            }
            armObj.SetActive(false);

            if (mobData.ID.Contains("white_ant") && !mobData.ID.Contains("_sub"))
            {
                dieClip = Resources.Load<AudioClip>("Sound/Monster/" + tId + "_die");
                hitClip2 = Resources.Load<AudioClip>("Sound/Monster/hit_4");
            }
            else if (mobData.ID.Contains("hand_skull_green"))
            {
                dieClip = Resources.Load<AudioClip>("Sound/Monster/" + tId + "_die");
                hitClip2 = Resources.Load<AudioClip>("Sound/Monster/" + tId + "_hit");
            }
            else if (mobData.ID.Contains("skull_green"))
            {
                dieClip = Resources.Load<AudioClip>("Sound/Monster/" + tId + "_die");
                hitClip2 = Resources.Load<AudioClip>("Sound/Monster/" + tId + "_hit");
            }
            else if (mobData.ID.Contains("slime_sword"))
            {
                dieClip = Resources.Load<AudioClip>("Sound/Monster/" + tId + "_die");
                hitClip2 = Resources.Load<AudioClip>("Sound/Monster/" + tId + "_hit");
            }
            else
            {
                dieClip = Resources.Load<AudioClip>("Sound/Monster/die");
                hitClip2 = Resources.Load<AudioClip>("Sound/Monster/hit_4");
            }

            if (isUnique)
            {
                mobInfo.maxHp = mobInfo.curHp = (mobInfo.maxHp * 4);
                speed *= 1.5f;
            }

            transform.eulerAngles = Vector3.zero;
            subBossTime = 120.0f;
            isSub = false;
            isSubSpawn = false;
            leftLine.gameObject.SetActive(false);
            rightLine.gameObject.SetActive(false);
            LeftLineEndEffect.SetActive(false);
            RightLineEndEffect.SetActive(false);

            homingShoot.gameObject.SetActive(true);
            circleShoot.gameObject.SetActive(true);
            reversShot.gameObject.SetActive(true);
            randomAming.gameObject.SetActive(true);
            spiralLeftShoot.gameObject.SetActive(true);

            int t = mobcollID.FindIndex(item => item.Equals(tId));
            if(t != -1)
            {
                GetComponent<BoxCollider2D>().offset = mobCollOffset[t];
                GetComponent<BoxCollider2D>().size = mobCollSize[t];
            }
            minimapObj.GetComponent<SpriteRenderer>().enabled = false;
            minimapObj.GetComponent<MiniMap>().enabled = true;

            minimapSR.sprite = defaultSimbol;

            if (mobData.ID.Contains("mimic"))
            {
                BackCollision.SetActive(true);
                minimapSR.sprite = mimicSimbol;
                mimicHitCount = 3;
                isMiMicWait = true;
                isGoblin = false;
            }
            else if(mobData.ID.Contains("goblin"))
            {
                isGoblin = true;
                nGoblinMoveCount = 0;
                nGoblinMoveCountMax = 5;
                fGoblinDropPer = 0.5f;
                fGoblinMoveTime = Random.Range(3f, 6f);
            }
            else
            {
                isGoblin = false;
                BackCollision.SetActive(false);
                mimicHitCount = 0;
                isMiMicWait = false;
            }
            nGoblinMoveCount = 0;
            nGoblinMoveCountMax = 7;
            fGoblinMoveTime = 5.0f;
            isDieEnd = false;
            isGoblinEnd = false;
            isGoblinMove = false;
            pikoCoroutine = null;
            pikoInvincibility = null;
            mob5Coroutine = null;
            brightSlimePatternCoroutine = null;
            golemPatternCoroutine = null;
            knockDir = Vector2.zero;
            knockDis = 0;

            transform.localScale = Vector3.one;
            Load();
        }
        private void OnEnable()
        {
            if (isNoneInfo) return;
            _particle.gameObject.SetActive(false);
            StartCoroutine(UpdatePath());
        }
        void Load()
        {
            string tId = mobData.ID;
            if(isSub && tId.Contains("white_ant"))
            {
                tId = "white_ant_sub";
            }
            else
            {
                if (tId.Contains("_t")) tId = tId.Substring(0, tId.Length - 3);
            }
            var spritePath = "Mob/" + tId + "/" + animator.runtimeAnimatorController.name;
            if (!spritePath.Equals(_spritePath))
            {
                _spritePath = spritePath;
                _sprites = Resources.LoadAll<Sprite>(_spritePath);
            }
        }
        void LateUpdate()
        {
            if (isNoneInfo) return;
            if (sr == null || sr.sprite == null)
                return;
            Load();
            var name = sr.sprite.name;
            var sprite = System.Array.Find(_sprites, item => item.name == name);
            if (sprite) sr.sprite = sprite;
            if (attackSr != null)
            {
                if (attackSr.sprite != null)
                {
                    var name2 = attackSr.sprite.name;
                    var spriteAttack = System.Array.Find(_sprites, item => item.name == name2);
                    if (spriteAttack) attackSr.sprite = spriteAttack;
                }
            }
        }
        void Update()
        {
            string tId = mobData.ID;
            if (tId.Contains("_t")) tId = tId.Substring(0, tId.Length - 3);

            if (Dungeon.isPredators && !isUnique && !isBoss && mobInfo.curHp < mobInfo.maxHp && !isDie && !isGrab && !isPull && !isGoblin && !Dungeon.instance.isPredatorCollTime)
            {
                RaycastHit2D hit = Physics2D.CircleCast(transform.position, 3, Vector2.zero, 0, 1 << 9 | 1 << 30);
                if (hit && !Dungeon.instance.newObjectSelected && hit.collider.gameObject.GetComponent<Player>())
                {
                    controllerObj.GetComponent<SpriteRenderer>().sprite = PDG.Dungeon.instance.isJoystrick ? PDG.Dungeon.GetJoyStrickKeySprite(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_ACTIVE]) : PDG.Dungeon.GetJoyStrickKeySprite(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ACTIVE]);
                    TextOpenController.instance.ShowText(LocalizeManager.GetLocalize("predators_prev"), Color.white, 24, controllerObj, 0.5f, true);
                    Dungeon.instance.newObjectSelected = true;
                    Dungeon.instance.newObjectSelectedObj = gameObject;
                    controllerObj.SetActive(true);
                }
                else if (!hit)
                {
                    if(Dungeon.instance.newObjectSelectedObj == gameObject)
                    {
                        TextOpenController.instance.TextClose(Color.white);
                        Dungeon.instance.newObjectSelected = false;
                        Dungeon.instance.newObjectSelectedObj = null;
                    }
                    pushCheck = 0;
                    controllerObj.SetActive(false);
                }

                if (Dungeon.instance.newObjectSelectedObj == gameObject)
                {
                    Vector3 pos = transform.position;
                    pos.y += GetComponent<BoxCollider2D>().size.y;
                    controllerObj.transform.position = pos;
                    if (PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ACTIVE], false) || Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_ACTIVE], false))
                    {
                        isUsing = true;
                        if (PlayerCombat.instance.tCoroutine == null && pushCheck == 0) PlayerCombat.instance.tCoroutine = StartCoroutine(TimerCoroutine());
                        pushCheck++;
                    }
                    if(pushCheck == pushCheck2 && pushCheck != 0)
                    {
                        if (isUsing)  PlayerCombat.ps = PlayerState.Idle;
                        isUsing = false;
                        pushCheck = 0;
                        controllerObj.SetActive(false);
                        TextOpenController.instance.TextClose(Color.white);
                        Dungeon.instance.newObjectSelected = false;
                        Dungeon.instance.newObjectSelectedObj = null;
                        GetComponent<BoxCollider2D>().enabled = true;
                    }
                    pushCheck2 = pushCheck;
                }
            }
            dieTime += Time.deltaTime;
            fDotHitTime += Time.deltaTime;
            if (PDG.Player.instance == null) return;
            if (Dungeon.isEliteSpawnWait) return;
            if (isMimic && isMiMicWait) return;
            if (Vector3.Distance(PDG.Player.instance.transform.position, transform.position) > 60 && !isBoss) return;
            if (isBoss && GameObject.Find("Canvas").transform.Find("TutorialText2").gameObject.activeSelf)
            {
                subBossTime -= Time.deltaTime;
                if (!Dungeon.isEliteSpawn) subBossTime = 0.0f;
                if (subBossTime <= 0.0f)
                {
                    Dungeon.isEliteSpawn = false;
                    if (hpBar != null) Destroy(hpBar);
                    NPC[] _npcs = FindObjectsOfType(typeof(NPC)) as NPC[];
                    if (_npcs != null && _npcs.Length > 0)
                    {
                        for (int i = 0; i < _npcs.Length; i++)
                        {
                            if (_npcs[i].isMonsterSpawn)
                            {
                                _npcs[i].isOFF = false;
                                _npcs[i].GetComponent<Animator>().SetTrigger("ReSet");
                                if (_npcs[i].transform.Find("elite_monster_minimap")) _npcs[i].transform.Find("elite_monster_minimap").gameObject.SetActive(true);
                                break;
                            }
                        }
                        SoundManager.instance.StartAudio(new string[1] { "BGM/Dungeon_" + Dungeon.instance.stageRan[Dungeon.instance.StageLevel] }, VOLUME_TYPE.BGM, null, true);
                        GameObject dObj = Instantiate(monsterDeleteEffect);
                        dObj.transform.position = transform.position;
                        Destroy(dObj, 2f);
                        ResetMonster();
                    }
                }
                else
                {
                    if (subBossTime > 30)
                        GameObject.Find("Canvas").transform.Find("TutorialText2").GetComponent<Text>().color = Color.white;
                    else
                        GameObject.Find("Canvas").transform.Find("TutorialText2").GetComponent<Text>().color = Color.red;
                    GameObject.Find("Canvas").transform.Find("TutorialText2").GetComponent<Text>().text = LocalizeManager.GetLocalize("eliteTimer") + subBossTime.ToString("F2");
                }
            }
            if(isBoss && !Dungeon.isEliteSpawn && mobInfo.curHp > 0)
            {
                if (hpBar != null) Destroy(hpBar);
                ResetMonster();
            }
            if (nowElementalType == ELEMENTAL_TYPE.FREEZE) speed = speedOrigin / 4;
            else speed = speedOrigin;
            if (isHit)
            {
                hittime -= Time.deltaTime;
                if (hittime <= 0.0f)
                {
                    isHit = false;
                }
            }
            if (isNoneInfo) return;
            if (mobData.ID.Contains("bat_bomb") && Dungeon.instance.LoadingBar.fillAmount >= 1.0f && !_audio3.isPlaying) _audio3.Play();
            if (!isFire && WeaponsSlot.activeSelf) WeaponsSlot.SetActive(false);
            if (!IceElemental.activeSelf && animator.enabled) animator.SetFloat("AnimSpeed", 1.0f);
            if(_audio3.isPlaying)
            {
                float dis = Vector3.Distance(PDG.Player.instance.transform.position, transform.position);
                if (dis > 10) _audio3.volume = 0;
                float tVolume =  (SoundManager.isMute || (PDG.Dungeon.instance == null ? false : PDG.Dungeon.instance.isPause) || (PDG.Player.instance == null ? false : PDG.Player.instance.isDie)) ? 0 : SoundManager.EffectVolume;
                float t = tVolume / 10;
                _audio3.volume = tVolume - (dis * t);
            }
            if(!isTutorial && Vector3.Distance(Player.instance.transform.position, transform.position) > 30 && !isDie && !mobData.ID.Contains("piko") && !isRoaming && !isAttack && !isBoss && !isPull)
            {
                if (hpBar != null) Destroy(hpBar);
                transform.parent.GetComponent<MonsterArggro>().InitSetting(PDG.Player.instance.gameObject, mobData, Mathf.FloorToInt(mobInfo.maxHp), transform.parent.GetComponent<MonsterArggro>().isWarp, transform.parent.GetComponent<MonsterArggro>().animName);
            }
            if(hpBar != null)
            {
                Vector3 pos = UnityEngine.Camera.main.WorldToScreenPoint(transform.Find("HpPos").position);
                hpBar.transform.position = pos;
                hpBar.transform.Find("Image").GetComponent<UnityEngine.UI.Image>().fillAmount = mobInfo.curHp / mobInfo.maxHp;
            }
            if (fDotTime > 0.0f && !isDie)
            {
                if (!elementalName.Equals(string.Empty) && elementalName.Contains("pixelfire_small")) pixelfireEffect.SetActive(true);
                else pixelfireEffect.SetActive(false);
                fDotTime -= Time.deltaTime;
                if (fDotHitTime > (elementalName.Contains("drum") ? 0.3f : (elementalName.Contains("frame_nova") ? Player.instance.tickTimeR : Player.instance.tickTime)) && nowElementalType == ELEMENTAL_TYPE.FIRE)
                {
                    if (_audio2) _audio2.volume =  (SoundManager.isMute || (PDG.Dungeon.instance == null ? false : PDG.Dungeon.instance.isPause) || (PDG.Player.instance == null ? false : PDG.Player.instance.isDie)) ? 0 : SoundManager.EffectVolume;
                    if (_audio2) _audio2.PlayOneShot(hitClip2);

                    float damage = 0;
                    if (elementalName.Contains("frame_nova"))
                        damage = Player.instance.damageRight;
                    else if (elementalName.Contains("drum"))
                        damage = Random.Range(1, 4);
                    else
                        damage = Player.instance.damage;

                    mobInfo.curHp -= damage;
                    fDotHitTime = 0.0f;

                    if (mobInfo.curHp <= 0)
                    {
                        if (isMimic)
                        {
                            SoundManager.instance.StartAudio(new string[1] { "Monster/mimic_die2" }, VOLUME_TYPE.EFFECT);
                        }
                        if (isBoss && GameObject.Find("Canvas").transform.Find("TutorialText2").gameObject.activeSelf) GameObject.Find("Canvas").transform.Find("TutorialText2").gameObject.SetActive(false);
                        if (hpBar != null) Destroy(hpBar);
                        if (_audio2) _audio2.volume = (SoundManager.isMute || (PDG.Dungeon.instance == null ? false : PDG.Dungeon.instance.isPause) || (PDG.Player.instance == null ? false : PDG.Player.instance.isDie)) ? 0 : SoundManager.EffectVolume;
                        if (_audio2) _audio2.PlayOneShot(dieClip);
                        mobInfo.curHp = 0;
                        if (isWarpMonster) Dungeon.instance.curWarpCount++;
                        if (WeaponsSlot) WeaponsSlot.SetActive(false);
                        isDie = true;
                        if (PlayerBuffController.buffList.FindIndex(item => item.Equals("offensive_02")) != -1)
                        {
                            Player.instance.fPlusDamageTime_offensive02 = 1.0f;
                            Player.instance.fPlusDamage_offensive02 = 2;
                        }
                        if (PlayerBuffController.buffList.FindIndex(item => item.Equals("offensive_03")) != -1)
                        {
                            Player.instance.nBuff_offensive03_count++;
                        }
                        if (PlayerBuffController.buffList.FindIndex(item => item.Equals("offensive_04")) != -1)
                        {
                            Player.instance.fPlusDamageTime_offensive04 = 5.0f;
                            if (Player.instance.fPlusDamage_offensive02 < 30) Player.instance.fPlusDamage_offensive02 += 5;
                        }

                        if (SteamStatsAndAchievements.instance != null) SteamStatsAndAchievements.instance.SettingStat(tId + "_hunting");
                        if (isBoss && SteamStatsAndAchievements.instance != null)
                        {
                            SteamStatsAndAchievements.instance.SettingStat("stat_hunting_unique_count");

                            int bossCount = SteamStatsAndAchievements.instance.GettingStat("stat_hunting_unique_count");
                            AchievementsData aData_0 = System.Array.Find(SheetManager.Instance.AchievementsDB.dataArray, item => item.ID.Equals("hunter_subboss_0"));
                            AchievementsData aData_1 = System.Array.Find(SheetManager.Instance.AchievementsDB.dataArray, item => item.ID.Equals("hunter_subboss_1"));
                            AchievementsData aData_2 = System.Array.Find(SheetManager.Instance.AchievementsDB.dataArray, item => item.ID.Equals("hunter_subboss_2"));
                            SaveAndLoadManager.instance.SaveAchievements(new AchievementData[3] {
                                new AchievementData { _id = aData_0.ID, _cur = bossCount, _max = aData_0.MAX, _clear = false },
                                new AchievementData { _id = aData_1.ID, _cur = bossCount, _max = aData_1.MAX, _clear = false },
                                new AchievementData { _id = aData_2.ID, _cur = bossCount, _max = aData_2.MAX, _clear = false }
                            });
                        }
                        if (!isUnique && !isBoss && SteamStatsAndAchievements.instance != null) SteamStatsAndAchievements.instance.SettingStat("stat_hunting_total_count");

                        if (isBoss)
                        {
                            if (StateGroup.instance.subShotCount == 0)
                            {
                                AchievementsData aData_0 = System.Array.Find(SheetManager.Instance.AchievementsDB.dataArray, item => item.ID.Equals("serene"));
                                SaveAndLoadManager.instance.SaveAchievements(new AchievementData[1] {
                                    new AchievementData { _id = aData_0.ID, _cur = 1, _max = aData_0.MAX, _clear = false }
                                });
                            }
                            Dungeon.isEliteSpawn = false;
                            if (mobData.ID.Contains("piko"))
                            {
                                Sprite[] sprites = System.Array.FindAll(_sprites, item => item.name.Contains("fragments"));
                                if (sprites.Length > 0)
                                {
                                    if (!_particle.gameObject.activeSelf) _particle.gameObject.SetActive(true);
                                    for (int i = 0; i < sprites.Length; i++)
                                    {
                                        FeedbackMaterial.SetTexture("_MainTex", sprites[i].texture);
                                        WeaponUsedMMFeedback.PlayFeedbacks(transform.position);
                                    }
                                }
                            }
                            PikoDieStart();
                            _audio2.volume = (SoundManager.isMute || (PDG.Dungeon.instance == null ? false : PDG.Dungeon.instance.isPause) || (PDG.Player.instance == null ? false : PDG.Player.instance.isDie)) ? 0 : SoundManager.EffectVolume;
                            _audio2.PlayOneShot(bossShaker);
                        }
                        else if (isGoblin)
                        {
                            int goblinCount = SaveAndLoadManager.instance.LoadAchievements("thiefmaster_3")._cur;
                            AchievementsData aData_0 = System.Array.Find(SheetManager.Instance.AchievementsDB.dataArray, item => item.ID.Equals("thiefmaster_0"));
                            AchievementsData aData_1 = System.Array.Find(SheetManager.Instance.AchievementsDB.dataArray, item => item.ID.Equals("thiefmaster_1"));
                            AchievementsData aData_2 = System.Array.Find(SheetManager.Instance.AchievementsDB.dataArray, item => item.ID.Equals("thiefmaster_2"));
                            AchievementsData aData_3 = System.Array.Find(SheetManager.Instance.AchievementsDB.dataArray, item => item.ID.Equals("thiefmaster_3"));
                            SaveAndLoadManager.instance.SaveAchievements(new AchievementData[4] {
                                new AchievementData { _id = aData_0.ID, _cur = goblinCount + 1, _max = aData_0.MAX, _clear = false },
                                new AchievementData { _id = aData_1.ID, _cur = goblinCount + 1, _max = aData_1.MAX, _clear = false },
                                new AchievementData { _id = aData_2.ID, _cur = goblinCount + 1, _max = aData_2.MAX, _clear = false },
                                new AchievementData { _id = aData_3.ID, _cur = goblinCount + 1, _max = aData_3.MAX, _clear = false }
                            });
                            DieStart(false);
                        }
                        else if (!isNoneInfo)
                        {
                            if (isTutorial && tutoType == 0)
                            {
                                Dialogue.Instance.CommnetSetting("tutorial_second_map_npc_2", 0, null);
                                GameObject.Find("Canvas").transform.Find("TutorialText").gameObject.SetActive(false);
                            }
                            else
                            {
                                if (!isSub) ItemDrop();
                            }
                            DieStart(false);
                        }
                        else if (isNoneInfo)
                        {
                            Destroy(gameObject);
                        }
                        if (bCol) bCol.enabled = false;
                        GameObject obj = ObjManager.Call().GetObject("MonsterDeath");
                        obj.transform.position = transform.position;
                        if (obj.GetComponent<Animator>()) obj.GetComponent<Animator>().Rebind();
                        obj.SetActive(true);
                        ResultPanelScript.instance.killCount++;
                    }
                    if (isGrab)
                    {
                        Player.instance.SetDamage(1, LocalizeManager.GetLocalize("death_fire"), DEATH_TYPE.DEATH_FIRE, tId);
                    }
                }
                if (nowElementalType == ELEMENTAL_TYPE.FIRE) sr.color = Color.red;
                else if (nowElementalType == ELEMENTAL_TYPE.FREEZE) sr.color = new Color32(76, 189, 255, 255);
                if (!FireElemental.activeSelf && nowElementalType == ELEMENTAL_TYPE.FIRE) FireElemental.SetActive(true);
            }
            else if (!isDie)
            {
                pixelfireEffect.SetActive(false);
                if (FireElemental.activeSelf) FireElemental.SetActive(false);
                nowElementalType = ELEMENTAL_TYPE.NONE;
                sr.color = Color.white;
                fDotHitTime = 0.0f;
            }
            if(isBoss && GameObject.Find("Canvas").transform.Find("TutorialText3").gameObject.activeSelf)
            {
                Vector3 pos = transform.position;
                pos.y += 1.8f;
                pos = UnityEngine.Camera.main.WorldToScreenPoint(pos);
                GameObject.Find("Canvas").transform.Find("TutorialText3").transform.position = pos;
            }
            armObj.transform.localScale = transform.localScale;
            fAttackDelayNow += Time.deltaTime;
            if (isDie)
            {
                if (ExclamationObj.activeSelf) ExclamationObj.SetActive(false);

                if (sr.color != DieColor && dieTime > 0.5f && !isDieEnd)
                {
                    if (!isBoss) DieStart(false);
                    else PikoDieStart();
                }
            }
            else if (isGrab)
            {
                for (int i = 0; i < armPieces.Length; i++)
                {
                    armPieces[i].SetActive(false);
                }
                armObj.SetActive(false);
                transform.position = tGrab.position;
            }
            else if (isPull)
            {
                int mask = 1 << 8 | 1 << 12 | 1 << 23;
                RaycastHit2D hit = Physics2D.CircleCast(transform.position, 1f, Vector2.zero, 1, mask);
                if (hit)
                {
                    if (PlayerBuffController.buffList.FindIndex(item => item.Equals("offensive_02")) != -1)
                    {
                        Player.instance.fPlusDamageTime_offensive02 = 1.0f;
                        Player.instance.fPlusDamage_offensive02 = 2;
                    }
                    if (PlayerBuffController.buffList.FindIndex(item => item.Equals("offensive_03")) != -1)
                    {
                        Player.instance.nBuff_offensive03_count++;
                    }
                    if (PlayerBuffController.buffList.FindIndex(item => item.Equals("offensive_04")) != -1)
                    {
                        Player.instance.fPlusDamageTime_offensive04 = 5.0f;
                        if (Player.instance.fPlusDamage_offensive02 < 30) Player.instance.fPlusDamage_offensive02 += 5;
                    }

                    _audio2.volume = (SoundManager.isMute || (PDG.Dungeon.instance == null ? false : PDG.Dungeon.instance.isPause) || (PDG.Player.instance == null ? false : PDG.Player.instance.isDie)) ? 0 : SoundManager.EffectVolume;
                    _audio2.PlayOneShot(hitClip);
                    if (hpBar != null) Destroy(hpBar);
                    isDie = true;
                    DieStart(false);
                    //ResultPanelScript.instance.chargeGuage += Mathf.FloorToInt(5 + (5 * PDG.Player.instance.plusQAmount));
                    ResultPanelScript.instance.killCount++;
                    WeaponsSlot.SetActive(false);
                    if (!isTutorial) ItemDrop();
                    GameObject obj = ObjManager.Call().GetObject("MonsterDeath");
                    obj.transform.position = transform.position;
                    if (obj.GetComponent<Animator>()) obj.GetComponent<Animator>().Rebind();
                    obj.SetActive(true);
                    if (SteamStatsAndAchievements.instance != null) SteamStatsAndAchievements.instance.SettingStat(tId + "_hunting");
                    if (SteamStatsAndAchievements.instance != null) SteamStatsAndAchievements.instance.SettingStat("stat_hunting_total_count");
                    if (hit.collider.gameObject.layer.Equals(12))
                    {
                        if (hit.collider.GetComponent<Monster>().isTutorial)
                        {
                            if (hit.collider.GetComponent<Monster>().tutoType == 2)
                            {
                                if (PlayerBuffController.buffList.FindIndex(item => item.Equals("offensive_03")) != -1)
                                {
                                    Player.instance.nBuff_offensive03_count++;
                                }
                                if (hit.collider.GetComponent<Monster>().hpBar != null) Destroy(hit.collider.GetComponent<Monster>().hpBar);
                                hit.collider.GetComponent<Monster>().isDie = true;
                                hit.collider.GetComponent<Monster>().DieStart(false);
                                hit.collider.GetComponent<Monster>().WeaponsSlot.SetActive(false);
                                var target2 = hit.collider.GetComponent<Target>();
                                if (target2 != null)
                                {
                                    target2.HandleHit(1, Vector2.zero);
                                }
                                GameObject obj2 = ObjManager.Call().GetObject("MonsterDeath");
                                obj2.transform.position = hit.collider.transform.position;
                                obj2.GetComponent<Animator>().Rebind();
                                obj2.SetActive(true);
                                Dialogue.Instance.CommnetSetting("tutorial_second_map_npc_3", 0, null);
                                GameObject.Find("Canvas").transform.Find("TutorialText").gameObject.SetActive(false);
                            }
                        }
                        else
                        {
                            Vector3 dir = hit.collider.transform.position - transform.position;
                            dir.Normalize();
                            hit.collider.GetComponent<Monster>().SetDamage(15, -dir, 2, ELEMENTAL_TYPE.NONE, 0.0f);
                        }
                    }
                    else if (hit.collider.gameObject.layer.Equals(23))
                    {
                        if (hit.collider.transform.parent && hit.collider.transform.parent.GetComponent<TentacleController>())
                        {
                            hit.collider.transform.parent.GetComponent<TentacleController>().SetDamage(1);
                        }
                        Vector3 dir = hit.collider.gameObject.transform.position - PDG.Player.instance.transform.position;
                        dir.Normalize();
                        if (hit.collider.gameObject.GetComponent<DestroyObjects>())
                        {
                            hit.collider.gameObject.GetComponent<DestroyObjects>().DestroyObject(dir);
                        }
                        if (hit.collider.gameObject.GetComponent<ChestHit>() && !hit.collider.gameObject.GetComponent<ChestHit>().isOpenBefore)
                        {
                            hit.collider.gameObject.GetComponent<ChestHit>().SetDamage(1);
                        }
                        if (hit.collider.gameObject.GetComponent<DrumHit>())
                        {
                            hit.collider.gameObject.GetComponent<DrumHit>().SetDamage(1);
                        }
                    }
                    if (isWarpMonster) Dungeon.instance.curWarpCount++;
                    if (rigid) rigid.velocity = Vector2.zero;
                    CameraShaker._instance.StartShake(0.5f, 0.02f, 0.2f);
                    var target = GetComponent<Target>();
                    if (target != null)
                    {
                        target.HandleHit(1, Vector2.zero);
                    }
                }
            }
            else if (isUsing)
            {

            }
            else
            {
                try
                {
                    Flip();
                    if (isUnique)
                    {
                        transform.localScale *= 1.5f;
                        sOutline.Regenerate();
                        if (sOutline2) sOutline2.Regenerate();
                    }
                    isWeaponMode = true;
                    if (mobData != null && (mobData.ID.Contains("beholder") || mobData.ID.Contains("hoodie_fireshot") || mobData.ID.Contains("white_ant") || mobData.ID.Contains("slime_sword") || mobData.Animtype.Contains("Mob5") || mobData.Animtype.Contains("Mob6")
                         || mobData.ID.Contains("ghost_hide") || mobData.ID.Contains("ghost_hide") || mobData.ID.Contains("slime_flying") || mobData.ID.Contains("snake_ice")
                         || mobData.ID.Contains("starfish_ice") || mobData.ID.Contains("slime_fire") || mobData.ID.Contains("mimic") || mobData.ID.Contains("goblin") || mobData.Animtype.Contains("Plant")))
                    {
                        WeaponsSlot.SetActive(false);
                    }
                    else
                    {
                        WeaponsSlot.SetActive(isFire);
                    }
                    if (isTutorial && tutoType == 3)
                    {
                        if (targetObj == null) targetObj = Player.instance.gameObject;
                        isWeaponMode = true;
                        Fire();
                    }
                    else if (!isBoss && !isRoaming && !isGoblin)
                    {
                        if (isHideGhost)
                        {
                            HideGhost();
                        }
                        else
                        {
                            AIMove();
                        }
                        WeaponsControl();
                        if (isWeaponMode)
                        {
                            if (this.transform.localScale.x > 0) angle = angle - 180.0f;
                            else angle *= -1;

                            WeaponsSlot.transform.localEulerAngles = new Vector3(0, 0, angle);
                        }
                    }
                    else if (!isBoss && isRoaming && !isGoblin)
                    {
                        RaycastHit2D hit = Physics2D.CircleCast(transform.position, 6, Vector2.zero, 0, 1 << 9 | 1 << 30);
                        if (hit && (!mobData.ID.Contains("skull_green") || mobData.ID.Contains("hand"))) 
                        {
                            if (targetObj == null) targetObj = hit.collider.gameObject;
                            if (!isAttack && isBomb)
                            {
                                StartCoroutine(ExclamationMark());
                                isAttack = true;
                                isMoveChange = true;
                                StartCoroutine(Dash());
                            }
                            else if (mobData.ID.Contains("woodlouse"))
                            {
                                if (!isAttack)
                                {
                                    StartCoroutine(ExclamationMark());
                                    SoundManager.instance.StartAudio(new string[1] { "Monster/ade" }, VOLUME_TYPE.EFFECT);
                                    isAttack = true;
                                    animator.SetTrigger("attack");
                                    animator.SetBool("attackEnd", false);
                                    if (rollingCoroutine != null) StopCoroutine(rollingCoroutine);
                                    rollingCoroutine = StartCoroutine(AttackRolling());
                                }
                            }
                            else
                            {
                                StartCoroutine(ExclamationMark());
                                SoundManager.instance.StartAudio(new string[1] { "Monster/ade" }, VOLUME_TYPE.EFFECT);
                                isRoaming = false;
                                isTargeting = true;
                                isPathPossible = true;
                                isMoveChange = true;

                                if (isHideGhost)
                                {
                                    isHide = true;
                                    isInvincibility = true;
                                    animator.SetTrigger("hide");
                                }
                            }
                        }
                        else
                        {
                            RoadingMove();
                            WeaponsControl();
                        }
                    }
                    else if (isGoblin)
                    {
                        GoblinMove();
                        WeaponsControl();
                    }
                    else if (mobData.ID.Contains("piko"))
                    {
                        WeaponsControl();
                        PikoPattern();
                    }
                    else if (mobData.ID.Contains("slime_bright"))
                    {
                        WeaponsControl();
                        BrightSlimePattern();
                    }
                    else if (mobData.ID.Contains("golem_ice"))
                    {
                        WeaponsControl();
                        GolemIcePattern();
                    }
                    else if (mobData.ID.Contains("radishman"))
                    {
                        WeaponsControl();
                        RadishPattern();
                    }

                }
                catch (System.Exception e)
                {
                    //Debug.LogError(e.Message);
                }
            }
        }
        private void ResetMonster()
        {
            GameObject.Find("Canvas").transform.Find("TutorialText3").gameObject.SetActive(false);
            if(isBoss)
            {
                Dungeon.instance.StartCoroutine(Dungeon.instance.BossTextClose());
            }
            transform.parent.gameObject.SetActive(false);
        }
        IEnumerator AttackRolling(bool isRotate = false)
        {
            float hitAngle = 0.0f;
            float time = 10.0f;
            Vector3 _dir = Player.instance.transform.position - transform.position;
            _dir.Normalize();
            int c = 0;
            float zzz = 0.0f;
            float hithitTime = 0.0f;
            while(time > 0.0f && c < 3 && !isDie)
            {
                if (!isFreeze && !isGrab && !isPull && !isUsing)
                {
                    zzz += 30.0f;
                    hithitTime -= Time.deltaTime;
                    time -= Time.deltaTime;
                    RaycastHit2D ray = Physics2D.Raycast(transform.position, _dir, 1, 1 << 8);
                    if (ray && hithitTime < 0.0f)
                    {
                        SoundManager.instance.StartAudio(new string[1] { "Monster/woodlouse_collision" }, VOLUME_TYPE.EFFECT);
                        float f = GetReflectAngle(ray.point, ray.collider.transform.position, _dir);
                        while (f < 0)
                        {
                            f += 360.0f;
                            if (f > 0) break;
                        }
                        while (f >= 360.0f)
                        {
                            f -= 360.0f;
                            if (f < 360.0f) break;
                        }
                        _dir = GetPosition(ray.point, f);
                        _dir -= (Vector3)ray.point;
                        _dir.Normalize();
                        hithitTime = 0.1f;
                        c++;
                    }
                    transform.eulerAngles = Vector3.zero;
                    Move(_dir, speed * 2, 1, false);
                    float zz = GetAngle(transform.position, transform.position + _dir);
                    zz -= 270.0f;
                    if (isRotate)
                        transform.eulerAngles = new Vector3(0, 0, zzz);
                    else
                        transform.eulerAngles = new Vector3(0, 0, zz);
                }
                yield return new WaitForSeconds(Time.deltaTime);
            }
            isAttack = false;
            if (!isDie) animator.SetBool("attackEnd", true);
            isMoveChange = true;
            if (isRotate) isMoveStop = false;
        }
        float GetReflectAngle(Vector3 _hitPoint, Vector3 _hitPosition, Vector3 _dir)
        {
            Vector3 _dir2 = _hitPoint - _hitPosition;
            _dir2.Normalize();

            if (_dir.x == 0 && _dir.y > 0)       // 위 
            {
                return 270.0f;
            }
            else if (_dir.x == 0 && _dir.y < 0)  // 아래
            {
                return 90.0f;
            }
            else if (_dir.x > 0 && _dir.y == 0)  // 오른쪽
            {
                return 180.0f;
            }
            else if (_dir.x < 0 && _dir.y == 0)  // 왼쪽
            {
                return 0.0f;
            }
            else if (_dir.x > 0 && _dir.y > 0)  // 오른쪽 위 
            {
                if (_dir2.x > 0)
                {
                    float zz = GetAngle(_hitPoint, transform.position);
                    float f = zz - 180.0f; // 입사각
                    return 360.0f - f; // 반사각
                }
                else if (_dir2.y > 0)
                {
                    float zz = GetAngle(_hitPoint, transform.position);
                    float f = 270.0f - zz; // 입사각
                    return 90.0f + f; // 반사각
                }
                else
                {
                    float hitAngle = GetAngle(_hitPosition, _hitPoint);
                    if (hitAngle > 225.0f)
                    {
                        float zz = GetAngle(_hitPoint, transform.position);
                        float f = zz - 180.0f; // 입사각
                        return 360.0f - f; // 반사각
                    }
                    else if (hitAngle < 225.0f)
                    {
                        float zz = GetAngle(_hitPoint, transform.position);
                        float f = 270.0f - zz; // 입사각
                        return 90.0f + f; // 반사각
                    }
                    else
                    {
                        return 225.0f;
                    }

                }
            }
            else if (_dir.x < 0 && _dir.y > 0)   // 왼쪽 위
            {
                if (_dir2.x < 0)
                {
                    float zz = GetAngle(_hitPoint, transform.position);
                    float f = 360.0f - zz; // 입사각
                    return 180.0f + f; // 반사각
                }
                else if (_dir2.y > 0)
                {
                    float zz = GetAngle(_hitPoint, transform.position);
                    float f = zz - 270.0f; // 입사각
                    return f; // 반사각
                }
                else
                {
                    float hitAngle = GetAngle(_hitPosition, _hitPoint);
                    if (hitAngle > 315.0f)
                    {
                        float zz = GetAngle(_hitPoint, transform.position);
                        float f = zz - 270.0f; // 입사각
                        return f; // 반사각
                    }
                    else if (hitAngle < 315.0f)
                    {
                        float zz = GetAngle(_hitPoint, transform.position);
                        float f = 360.0f - zz; // 입사각
                        return 180.0f + f; // 반사각
                    }
                    else
                    {
                        return 315.0f;
                    }

                }
            }
            else if (_dir.x < 0 && _dir.y < 0)   // 왼쪽 아래
            {
                if (_dir2.x < 0)
                {
                    float zz = GetAngle(_hitPoint, transform.position);
                    float f = zz; // 입사각
                    return 180.0f - f; // 반사각
                }
                else if (_dir2.y < 0)
                {
                    float zz = GetAngle(_hitPoint, transform.position);
                    float f = 90.0f - zz; // 입사각
                    return 270.0f + f; // 반사각
                }
                else
                {
                    float hitAngle = GetAngle(_hitPosition, _hitPoint);
                    if (hitAngle > 45.0f)
                    {
                        float zz = GetAngle(_hitPoint, transform.position);
                        float f = zz; // 입사각
                        return 180.0f - f; // 반사각
                    }
                    else if (hitAngle < 45.0f)
                    {
                        float zz = GetAngle(_hitPoint, transform.position);
                        float f = 90.0f - zz; // 입사각
                        return 270.0f + f; // 반사각
                    }
                    else
                    {
                        return 45.0f;
                    }

                }
            }
            else if (_dir.x > 0 && _dir.y < 0)   // 오른쪽 아래
            {
                if (_dir2.x > 0)
                {
                    float zz = GetAngle(_hitPoint, transform.position);
                    float f = 180.0f - zz; // 입사각
                    return f; // 반사각
                }
                else if (_dir2.y < 0)
                {
                    float zz = GetAngle(_hitPoint, transform.position);
                    float f = zz - 90.0f; // 입사각
                    return 270.0f - f; // 반사각
                }
                else
                {
                    float hitAngle = GetAngle(_hitPosition, _hitPoint);
                    if (hitAngle > 135.0f)
                    {
                        float zz = GetAngle(_hitPoint, transform.position);
                        float f = zz - 90.0f; // 입사각
                        return 270.0f - f; // 반사각
                    }
                    else if (hitAngle < 135.0f)
                    {
                        float zz = GetAngle(_hitPoint, transform.position);
                        float f = 180.0f - zz; // 입사각
                        return f; // 반사각
                    }
                    else
                    {
                        return 135.0f;
                    }
                }
            }
            else
            {
                return 0.0f;
            }
        }
        public void DieEnd()
        {
            sr.color = DieColor;
            sr.sortingOrder = 0;
            animator.enabled = false;
            simbolObj.SetActive(false);
            minimapObj.SetActive(false);
            rigid.bodyType = RigidbodyType2D.Static;
            rigid.simulated = false;
            if (!isBoss) StopAllCoroutines();
            isDieEnd = true;
        }
        public void DieStart(bool _isPredators)
        {
            string tId = mobData.ID;
            if (tId.Contains("_t")) tId = tId.Substring(0, tId.Length - 3);

            SaveAndLoadManager.instance.SaveCollection(new CollectionData { _id = tId, _isOpen = true, _isWeapon = false });
            if (Dungeon.instance.newObjectSelectedObj == gameObject)
            {
                Dungeon.instance.newObjectSelectedObj = null;
                Dungeon.instance.newObjectSelected = false;
                controllerObj.SetActive(false);
                TextOpenController.instance.TextClose(Color.white);
            }
            dieTime = 0.0f;

            if(isUsing)
            {
                PlayerCombat.ps = PlayerState.Idle;
                isUsing = false;
                timerObj.SetActive(false);
                PlayerCombat.instance.tCoroutine = null;
            }

            StopAllCoroutines();
            pikoCoroutine = null;
            pikoInvincibility = null;
            mob5Coroutine = null;
            brightSlimePatternCoroutine = null;
            golemPatternCoroutine = null;
            dieEffect.SetActive(true);
            dieEffect.GetComponent<ParticleSystem>().Play();
            GetComponent<SpriteOutline>().Clear();
            pixelfireEffect.SetActive(false);
            FireElemental.SetActive(false);
            nowElementalType = ELEMENTAL_TYPE.NONE;
            sr.color = Color.white;
            fDotHitTime = 0.0f;
            simbolObj.SetActive(false);
            minimapObj.SetActive(false);
            if (isGoblin) 
            {
                leftLine.gameObject.SetActive(false);
                rightLine.gameObject.SetActive(false);
                LeftLineEndEffect.SetActive(false);
                RightLineEndEffect.SetActive(false);

                homingShoot.gameObject.SetActive(false);
                circleShoot.gameObject.SetActive(false);
                reversShot.gameObject.SetActive(false);
                spiralLeftShoot.gameObject.SetActive(false);
                randomAming.gameObject.SetActive(false);

                homingShoot.StopShotRoutine();
                randomAming.StopShotRoutine();
                circleShoot.StopShotRoutine();
                reversShot.StopShotRoutine();

                animator.SetTrigger("DieTrigger");

                int xPos = Mathf.FloorToInt(transform.position.x);
                int yPos = Mathf.FloorToInt(transform.position.y);
                int t = 0;
                Dungeon.isChoiceItem = false;
                goblinItemList.Clear();
                for (int i = 1; i < 10; i++)
                {
                    for (int x = (-1 * i); x <= (1 * i); x++)
                    {
                        for (int y = (-1 * i); y <= (1 * i); y++)
                        {
                            if (x == 0 && y == 0) continue;
                            if (Dungeon.instance.matrix.map[xPos + x, yPos + y] == Dungeon.instance.floor_1)
                            {
                                string itemName = string.Empty;
                                if (t == 0)
                                {
                                    List<string> itemList = new List<string>();
                                    for (int j = 0; j < Inventorys.Instance.DiscardedItem.Count; j++)
                                    {
                                        if (Inventorys.Instance.DiscardedItem[j].Data.Type1.Equals("Weapon"))
                                        {
                                            bool c = false;
                                            for (int k = 0; k < Inventorys.Instance.slots.Length; k++)
                                            {
                                                if (Inventorys.Instance.slots[k]._Item != null && Inventorys.Instance.slots[k]._Item.Data.ID.Equals(Inventorys.Instance.DiscardedItem[j].Data.ID)) c = true;
                                            }
                                            if (!c) itemList.Add(Inventorys.Instance.DiscardedItem[j].Data.ID);
                                        }
                                    }
                                    if (itemList.Count > 0) itemName = itemList[Random.Range(0, itemList.Count)];
                                }
                                else if (t == 1)
                                {
                                    List<string> itemList = new List<string>();
                                    for (int j = 0; j < Inventorys.Instance.DiscardedItem.Count; j++)
                                    {
                                        if (Inventorys.Instance.DiscardedItem[j].Data.Type1.Equals("Weapon"))
                                        {
                                            bool c = false;
                                            for (int k = 0; k < Inventorys.Instance.slots.Length; k++)
                                            {
                                                if (Inventorys.Instance.slots[k]._Item != null && Inventorys.Instance.slots[k]._Item.Data.ID.Equals(Inventorys.Instance.DiscardedItem[j].Data.ID)) c = true;
                                            }
                                            if (!c) itemList.Add(Inventorys.Instance.DiscardedItem[j].Data.ID);
                                        }
                                    }
                                    if (itemList.Count > 0) itemName = itemList[Random.Range(0, itemList.Count)];
                                }
                                else if (t == 2)
                                {
                                    bool c = false;
                                    List<string> itemList = new List<string>();
                                    for (int k = 0; k < Inventorys.Instance.slots.Length; k++)
                                    {
                                        if (Inventorys.Instance.slots[k]._Item != null && Inventorys.Instance.slots[k]._Item.Data.ID.Equals("secret_blade")) c = true;
                                    }
                                    if (!c) itemList.Add("secret_blade");
                                    c = false;
                                    for (int k = 0; k < Inventorys.Instance.slots.Length; k++)
                                    {
                                        if (Inventorys.Instance.slots[k]._Item != null && Inventorys.Instance.slots[k]._Item.Data.ID.Equals("secret_lasergun")) c = true;
                                    }
                                    if (!c) itemList.Add("secret_lasergun");
                                    c = false;
                                    for (int k = 0; k < Inventorys.Instance.slots.Length; k++)
                                    {
                                        if (Inventorys.Instance.slots[k]._Item != null && Inventorys.Instance.slots[k]._Item.Data.ID.Equals("secret_watergun")) c = true;
                                    }
                                    if (!c) itemList.Add("secret_watergun");
                                    itemName = itemList[Random.Range(0, itemList.Count)];
                                }
                                else if (t == 3)
                                {
                                    ItemsData[] itemList = System.Array.FindAll(SheetManager.Instance.ItemsDB.dataArray, item => item.Type1.Equals("Potion") && !item.Grade.Equals("F"));
                                    itemName = itemList[Random.Range(0, itemList.Length)].ID;
                                }
                                if (itemName.Equals(string.Empty))
                                {
                                    t++;
                                    if (t >= 4) break;
                                    continue;
                                }
                                if (goblinItemList.FindIndex(item => item.Equals(itemName)) != -1)
                                {
                                    t++;
                                    if (t >= 4) break;
                                    continue;
                                }
                                goblinItemList.Add(itemName);
                                GameObject obj2 = ObjManager.Call().GetObject("Item");
                                if (obj2 != null)
                                {
                                    Item _item = obj2.GetComponent<Item>();
                                    _item._Item = new ItemInfo
                                    {
                                        Data = System.Array.Find(SheetManager.Instance.ItemsDB.dataArray, item => item.ID.Equals(itemName))
                                    };
                                    _item.dir = dir;
                                    _item.rPow = 0;
                                    _item.itemCount = 1;
                                    _item.isChoiceItem = true;
                                    obj2.transform.position = new Vector3(xPos + x, yPos + y, 0);
                                    obj2.SetActive(true);
                                }
                                t++;
                                if (t >= 4) break;
                            }
                        }
                        if (t >= 4) break;
                    }
                    if (t >= 4) break;
                }

                isDie = true;
                isDieEnd = true;
                StartCoroutine(GoblinReset());
            }
            else
            {
                if (_isPredators)
                {
                    SoundManager.instance.StartAudio(new string[1] { "Monster/predators_end" }, VOLUME_TYPE.EFFECT);
                    Player.instance.HpRecovery(1);

                    leftLine.gameObject.SetActive(false);
                    rightLine.gameObject.SetActive(false);
                    LeftLineEndEffect.SetActive(false);
                    RightLineEndEffect.SetActive(false);

                    homingShoot.gameObject.SetActive(false);
                    circleShoot.gameObject.SetActive(false);
                    reversShot.gameObject.SetActive(false);
                    spiralLeftShoot.gameObject.SetActive(false);
                    randomAming.gameObject.SetActive(false);

                    homingShoot.StopShotRoutine();
                    randomAming.StopShotRoutine();
                    circleShoot.StopShotRoutine();
                    reversShot.StopShotRoutine();

                    isDieEnd = true;
                    rigid.bodyType = RigidbodyType2D.Static;
                    rigid.simulated = false;

                    animator.enabled = false;
                    sr.color = new Color(1, 1, 1, 1);
                    sr.sprite = dieFragments[Random.Range(0, dieFragments.Length)];
                    sr.sortingOrder = 0;
                    StopAllCoroutines();
                }
                else if (isBomb)
                {
                    SoundManager.instance.StartAudio(new string[1] { "Object/DrumBomb_0" }, VOLUME_TYPE.EFFECT);
                    animator.SetTrigger("Bomb");
                    RaycastHit2D hit = Physics2D.CircleCast(transform.position, 2, Vector2.zero, 0, 1 << 9 | 1 << 30);
                    if (hit && hit.collider.GetComponent<Player>()) hit.collider.GetComponent<Player>().SetDamage(1, LocalizeManager.GetLocalize("monster_" + tId), isUnique ? DEATH_TYPE.DEATH_UNIQUE : DEATH_TYPE.DEATH_NORMAL, tId);
                    sr.sortingOrder = -2;
                    isDie = true;
                    rigid.bodyType = RigidbodyType2D.Static;
                    rigid.simulated = false;
                    isDieEnd = true;
                }
                else
                {
                    leftLine.gameObject.SetActive(false);
                    rightLine.gameObject.SetActive(false);
                    LeftLineEndEffect.SetActive(false);
                    RightLineEndEffect.SetActive(false);

                    homingShoot.gameObject.SetActive(false);
                    circleShoot.gameObject.SetActive(false);
                    reversShot.gameObject.SetActive(false);
                    spiralLeftShoot.gameObject.SetActive(false);
                    randomAming.gameObject.SetActive(false);

                    homingShoot.StopShotRoutine();
                    randomAming.StopShotRoutine();
                    circleShoot.StopShotRoutine();
                    reversShot.StopShotRoutine();
                    Sprite[] sprites = System.Array.FindAll(_sprites, item => item.name.Contains("fragments"));
                    if (sprites.Length > 0)
                    {
                        if (mobData.ID.Contains("snake_ice"))
                        {
                            for (int i = 0; i < sprites.Length; i++)
                            {
                                GameObject obj = new GameObject();
                                SpriteRenderer sssr = obj.AddComponent<SpriteRenderer>();
                                Rigidbody2D rrrrigid = obj.AddComponent<Rigidbody2D>();
                                FragmentsController fff = obj.AddComponent<FragmentsController>();
                                fff.isStart = true;
                                fff.time = 0.1f;
                                rrrrigid.gravityScale = 0;
                                sssr.sprite = sprites[i];
                                sssr.sortingLayerName = "Monster";
                                obj.transform.position = transform.position;
                                obj.transform.eulerAngles = new Vector3(0, 0, Random.Range(0, 360));
                                rrrrigid.AddForce(new Vector2(Random.Range(-2.5f, 2.5f), Random.Range(-2.5f, 2.5f)), ForceMode2D.Impulse);
                                sssr.color = DieColor;
                            }
                        }
                        else
                        {
                            _particle.gameObject.SetActive(true);
                            for (int i = 0; i < sprites.Length; i++)
                            {
                                FeedbackMaterial.SetTexture("_MainTex", sprites[i].texture);
                                WeaponUsedMMFeedback.PlayFeedbacks(transform.position);
                            }
                        }
                        sr.color = new Color(0, 0, 0, 0);
                        sr.sortingOrder = 0;
                        animator.enabled = false;
                        rigid.bodyType = RigidbodyType2D.Static;
                        rigid.simulated = false;
                        StopAllCoroutines();
                        isDieEnd = true;
                    }
                    else
                    {
                        animator.SetTrigger("DieTrigger");
                    }
                    if (mobData.ID.Contains("white_ant") && !isSubSpawn && !mobData.ID.Contains("_sub"))
                    {
                        isSubSpawn = true;
                        StageData sData = System.Array.Find(SheetManager.Instance.StageDB.dataArray, item => item.Type2.Equals("normalstage") && item.Type1.Equals(Dungeon.instance.StageName));
                        for (int k = 0; k < 3; k++)
                        {
                            MonstersData mobDataSub = System.Array.Find(SheetManager.Instance.MonsterDB.dataArray, item => item.ID.Equals(mobData.ID + "_sub"));
                            GameObject _obj = ObjManager.Call().GetObject("Monster");
                            _obj.transform.position = transform.position;
                            _obj.SetActive(true);
                            _obj.GetComponent<MonsterArggro>().InitSetting(Player.instance.gameObject, mobDataSub, Random.Range(10, 15), true, mobDataSub.Animtype);
                            _obj.GetComponent<MonsterArggro>().isStart = true;
                            _obj.GetComponent<MonsterArggro>()._monster.gameObject.SetActive(true);
                            _obj.GetComponent<MonsterArggro>()._monster.isSub = true;
                        }
                    }
                }
            }
        }
        IEnumerator GoblinReset()
        {
            GameObject obj = Instantiate(GoblinPortalObj);
            obj.transform.localEulerAngles = new Vector3(-40, 180, 180);
            obj.transform.position = new Vector3(transform.position.x, transform.position.y - 0.5f, 0);
            obj.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
            Destroy(obj, 3f);
            yield return new WaitForSeconds(2f);
            ResetMonster();
        }
        [Header("TEST")]
        private float kPow = 10.0f;
        public GameObject brightslimeHitObj;
        int dieTT = 0;
        public void SetDamage(float _damage, Vector2 _knock, int _knockDis, ELEMENTAL_TYPE _eType, float eTime, bool isQ = false)
        {
            if(mobInfo.curHp < 0)
            {
                dieTT++;
                if (dieTT > 3 && dieTime > 0.5f)
                {
                    isMoveStop = true;
                    isDie = true;
                    DieStart(false);
                }
                return;
            }
            if (Dungeon.instance.isPause) return;
            if (isTutorial)
            {
                if (tutoType == 1) return;
                if (tutoType == 2) return;
                if (tutoType == 3 && !isQ) return;
            }
            if (isDie) return;
            if (isInvincibility) return;
            if (isMimic && isMiMicWait) 
            {
                if (!isHit)
                {
                    SoundManager.instance.StartAudio(new string[1] { "Object/ChestHit_hit" }, VOLUME_TYPE.EFFECT);
                    if (animator) animator.SetTrigger("HitTrigger");
                    isHit = true;
                    hittime = 0.25f;
                    StartCoroutine(HitEffect());
                    isMoveChange = true;
                }
                mimicHitCount--;
                if (mimicHitCount <= 0)
                {
                    BackCollision.SetActive(false);
                    minimapSR.sprite = defaultSimbol;
                    SoundManager.instance.StartAudio(new string[1] { "Monster/mimic_open" }, VOLUME_TYPE.EFFECT);
                    isMiMicWait = false;
                }
                return;
            }
            if(mobData.ID.Contains("white_ant"))
            {
                GameObject hObj = Instantiate(whiteAntHitEffect);
                hObj.transform.position = transform.position;
                hObj.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
                Destroy(hObj, 2f);
            }
            if (mobData.ID.Contains("slime_bright"))
            {
                GameObject obj = Instantiate(brightslimeHitObj);
                obj.transform.position = transform.position;
                Destroy(obj, 2f);
            }
            if(isBoss)
            {
                targetObj = Player.instance.gameObject;
            }
            else
            {
                RaycastHit2D hit = Physics2D.CircleCast(transform.position, 40, Vector2.zero, 0, 1 << 9 | 1 << 30);
                if (hit && targetObj == null) targetObj = hit.collider.gameObject;
            }

            string tId = mobData.ID;
            if (tId.Contains("_t")) tId = tId.Substring(0, tId.Length - 3);

            if (Player.instance.fInvincibleTime_strategic_04 > 0.0f)
            {
                _damage *= 2;
                Player.instance.fInvincibleTime_strategic_04 = 0.0f;
            }
            //속성 데미지
            if (_eType == ELEMENTAL_TYPE.ICE)
            {
                if(isBoss || isUnique)
                {
                    fDotTime = eTime;
                    nowElementalType = ELEMENTAL_TYPE.FREEZE;
                }
                else
                {
                    if (IceElemental != null && !IceElemental.activeSelf)
                    {
                        IceElemental.GetComponent<ElementalController>()._pMob = this;
                        IceElemental.GetComponent<ElementalController>().elementalTime = eTime;
                        IceElemental.GetComponent<ElementalController>().eType = _eType;
                        isFreeze = true;
                        IceElemental.SetActive(true);
                    }
                }
            }
            else if(_eType == ELEMENTAL_TYPE.FREEZE)
            {
                fDotTime = eTime;
                nowElementalType = _eType;
            }

            var target = GetComponent<Target>();
            if (target != null)
            {
                target.HandleHit(0.1f, Vector2.zero);
            }
            if (isBomb && !isAttack)
            {
                isAttack = true;
                isMoveChange = true;
                StartCoroutine(Dash());
            }
            if (!isTargeting && !isTutorial && (!mobData.ID.Contains("skull_green") || mobData.ID.Contains("hand")) && !mobData.ID.Contains("woodlouse") && !mobData.ID.Contains("goblin") && !isNoneInfo)
            {
                isTargeting = true;
                if (!isBomb)
                {
                    isRoaming = false;
                    isMoveChange = true;
                }
                if (!mobData.ID.Contains("piko"))
                {
                    isWeaponMode = true;
                    if (isFire) WeaponsSlot.SetActive(true);
                    else WeaponsSlot.SetActive(false);
                    animator.SetBool("isWeapons", isWeaponMode);
                }
            }
            else if (mobData.ID.Contains("woodlouse") && !isHit)
            {
                isAttack = true;
                animator.SetTrigger("attack");
                animator.SetBool("attackEnd", false);
                if (rollingCoroutine != null) StopCoroutine(rollingCoroutine);
                rollingCoroutine = StartCoroutine(AttackRolling());
            }

            if (_audio2) _audio2.volume =  (SoundManager.isMute || (PDG.Dungeon.instance == null ? false : PDG.Dungeon.instance.isPause) || (PDG.Player.instance == null ? false : PDG.Player.instance.isDie)) ? 0 : SoundManager.EffectVolume;
            if (_audio2) _audio2.PlayOneShot(hitClip2);


            GameObject damageObjOrigin = Resources.Load<GameObject>("DamageText");
            GameObject damageObj = Instantiate(damageObjOrigin);
            damageObj.transform.SetParent(GameObject.Find("Canvas").transform);
            damageObj.transform.SetSiblingIndex(16);
            damageObj.transform.localScale = Vector3.one;
            damageObj.GetComponent<DamageText>().InitSetting(_damage, gameObject);

            if (mobData.ID.Contains("goblin"))
            {
                if (!isGoblinMove)
                {
                    isGoblinMove = true;
                }

                mobInfo.curHp -= _damage;
                ///string[] types = new string[4] { "normal", "unique", "subboss", "boss" };
                ///string[] types2Normal = new string[3] { "Cells", "Parts", "Ore" };
                ///string[] types2Subboss = new string[3] { "Piko", "BrightSlime", "GolemIce" };
                ///string[] types2Boss = new string[4] { "BossStg", "boss_npc", "Boss_IceGolem", "Boss_Stone" };

                int t = GetPercentege(fGoblinDropPer, new string[1] { "A" });
                if(t != -1)
                {
                    GameObject obj = ObjManager.Call().GetObject("Item");
                    if (obj != null)
                    {
                        Item _item = obj.GetComponent<Item>();
                        _item._Item = new ItemInfo
                        {
                            Data = System.Array.Find(SheetManager.Instance.ItemsDB.dataArray, item => item.ID.Equals("glowing_skull"))
                        };
                        _item.dir = dir;
                        _item.rPow = 0;
                        _item.itemCount = 1;
                        obj.transform.position = transform.position;
                        obj.SetActive(true);
                    }

                    ///int tt = Random.Range(0, 4);
                    ///int tt2 = tt == 3 ? Random.Range(0, 4) : Random.Range(0, 3);
                    ///Dungeon.DropMaterial(types[tt], tt == 2 ? types2Subboss[tt2] : (tt == 3 ? types2Boss[tt2] : types2Normal[tt2]), transform.position);
                }
            }
            else mobInfo.curHp -= _damage;
            if (mobInfo.curHp <= 0)
            {
                if (isMimic)
                {
                    SoundManager.instance.StartAudio(new string[1] { "Monster/mimic_die2" }, VOLUME_TYPE.EFFECT);
                }
                if (isBoss && GameObject.Find("Canvas").transform.Find("TutorialText2").gameObject.activeSelf) GameObject.Find("Canvas").transform.Find("TutorialText2").gameObject.SetActive(false);
                if (hpBar != null) Destroy(hpBar);
                if (isQ && isTutorial && tutoType == 3)
                {
                    Dungeon.instance.TutorialQCount++;
                }
                if (_audio2) _audio2.volume =  (SoundManager.isMute || (PDG.Dungeon.instance == null ? false : PDG.Dungeon.instance.isPause) || (PDG.Player.instance == null ? false : PDG.Player.instance.isDie)) ? 0 : SoundManager.EffectVolume;
                if (_audio2) _audio2.PlayOneShot(dieClip);
                mobInfo.curHp = 0;
                if (isWarpMonster) Dungeon.instance.curWarpCount++;
                if (WeaponsSlot) WeaponsSlot.SetActive(false);
                isDie = true;
                if (PlayerBuffController.buffList.FindIndex(item => item.Equals("offensive_02")) != -1)
                {
                    Player.instance.fPlusDamageTime_offensive02 = 1.0f;
                    Player.instance.fPlusDamage_offensive02 = 2;
                }
                if (PlayerBuffController.buffList.FindIndex(item => item.Equals("offensive_03")) != -1)
                {
                    Player.instance.nBuff_offensive03_count++;
                }
                if (PlayerBuffController.buffList.FindIndex(item => item.Equals("offensive_04")) != -1)
                {
                    Player.instance.fPlusDamageTime_offensive04 = 5.0f;
                    if (Player.instance.fPlusDamage_offensive02 < 30) Player.instance.fPlusDamage_offensive02 += 5;
                }

                if (SteamStatsAndAchievements.instance != null) SteamStatsAndAchievements.instance.SettingStat(tId + "_hunting");
                if (isBoss && SteamStatsAndAchievements.instance != null)
                {
                    SteamStatsAndAchievements.instance.SettingStat("stat_hunting_unique_count");

                    int bossCount = SteamStatsAndAchievements.instance.GettingStat("stat_hunting_unique_count");
                    AchievementsData aData_0 = System.Array.Find(SheetManager.Instance.AchievementsDB.dataArray, item => item.ID.Equals("hunter_subboss_0"));
                    AchievementsData aData_1 = System.Array.Find(SheetManager.Instance.AchievementsDB.dataArray, item => item.ID.Equals("hunter_subboss_1"));
                    AchievementsData aData_2 = System.Array.Find(SheetManager.Instance.AchievementsDB.dataArray, item => item.ID.Equals("hunter_subboss_2"));
                    SaveAndLoadManager.instance.SaveAchievements(new AchievementData[3] {
                        new AchievementData { _id = aData_0.ID, _cur = bossCount, _max = aData_0.MAX, _clear = false },
                        new AchievementData { _id = aData_1.ID, _cur = bossCount, _max = aData_1.MAX, _clear = false },
                        new AchievementData { _id = aData_2.ID, _cur = bossCount, _max = aData_2.MAX, _clear = false }
                    });
                }
                if (!isUnique && !isBoss && SteamStatsAndAchievements.instance != null) SteamStatsAndAchievements.instance.SettingStat("stat_hunting_total_count");

                if (isBoss)
                {
                    if (StateGroup.instance.subShotCount == 0)
                    {
                        AchievementsData aData_0 = System.Array.Find(SheetManager.Instance.AchievementsDB.dataArray, item => item.ID.Equals("serene"));
                        SaveAndLoadManager.instance.SaveAchievements(new AchievementData[1] {
                        new AchievementData { _id = aData_0.ID, _cur = 1, _max = aData_0.MAX, _clear = false }
                    });
                    }
                    Dungeon.isEliteSpawn = false;
                    if (mobData.ID.Contains("piko"))
                    {
                        Sprite[] sprites = System.Array.FindAll(_sprites, item => item.name.Contains("fragments"));
                        if (sprites.Length > 0)
                        {
                            if (!_particle.gameObject.activeSelf) _particle.gameObject.SetActive(true);
                            for (int i = 0; i < sprites.Length; i++)
                            {
                                FeedbackMaterial.SetTexture("_MainTex", sprites[i].texture);
                                WeaponUsedMMFeedback.PlayFeedbacks(transform.position);
                            }
                        }
                    }
                    PikoDieStart();
                    _audio2.volume = (SoundManager.isMute || (PDG.Dungeon.instance == null ? false : PDG.Dungeon.instance.isPause) || (PDG.Player.instance == null ? false : PDG.Player.instance.isDie)) ? 0 : SoundManager.EffectVolume;
                    _audio2.PlayOneShot(bossShaker);
                }
                else if (isGoblin)
                {
                    int goblinCount = SaveAndLoadManager.instance.LoadAchievements("thiefmaster_3")._cur;
                    AchievementsData aData_0 = System.Array.Find(SheetManager.Instance.AchievementsDB.dataArray, item => item.ID.Equals("thiefmaster_0"));
                    AchievementsData aData_1 = System.Array.Find(SheetManager.Instance.AchievementsDB.dataArray, item => item.ID.Equals("thiefmaster_1"));
                    AchievementsData aData_2 = System.Array.Find(SheetManager.Instance.AchievementsDB.dataArray, item => item.ID.Equals("thiefmaster_2"));
                    AchievementsData aData_3 = System.Array.Find(SheetManager.Instance.AchievementsDB.dataArray, item => item.ID.Equals("thiefmaster_3"));
                    SaveAndLoadManager.instance.SaveAchievements(new AchievementData[4] {
                        new AchievementData { _id = aData_0.ID, _cur = goblinCount + 1, _max = aData_0.MAX, _clear = false },
                        new AchievementData { _id = aData_1.ID, _cur = goblinCount + 1, _max = aData_1.MAX, _clear = false },
                        new AchievementData { _id = aData_2.ID, _cur = goblinCount + 1, _max = aData_2.MAX, _clear = false },
                        new AchievementData { _id = aData_3.ID, _cur = goblinCount + 1, _max = aData_3.MAX, _clear = false }
                    });
                    DieStart(false);
                }
                else if(!isNoneInfo)
                {
                    if (isTutorial && tutoType == 0)
                    {
                        Dialogue.Instance.CommnetSetting("tutorial_second_map_npc_2", 0, null);
                        GameObject.Find("Canvas").transform.Find("TutorialText").gameObject.SetActive(false);
                    }
                    else
                    {
                        if (!isSub) ItemDrop();
                    }
                    DieStart(false);
                }
                else if(isNoneInfo)
                {
                    Destroy(gameObject);
                }
                if (bCol) bCol.enabled = false;
                GameObject obj = ObjManager.Call().GetObject("MonsterDeath");
                obj.transform.position = transform.position;
                if (obj.GetComponent<Animator>()) obj.GetComponent<Animator>().Rebind();
                obj.SetActive(true);
                ResultPanelScript.instance.killCount++;
            }
            else if (!isHit)
            {
                if(!isBoss && !isUnique && !isTutorial && !isNotKnock && !isNoneInfo)
                {
                    knockDir = _knock;
                    knockDis = _knockDis;
                }
                if(isBoss)
                {
                    knockDir = Vector2.zero;
                    knockDis = 0;
                }
                if (animator) animator.SetTrigger("HitTrigger");
                isHit = true;
                hittime = 0.25f;
                StartCoroutine(HitEffect());
                if (!isBomb && !isGoblin) isMoveChange = true;
            }
            if (mobData.ID.Contains("piko") && Random.Range(0, 5) == 0)
            {
                Sprite[] sprites = System.Array.FindAll(_sprites, item => item.name.Contains("fragments"));
                if (sprites.Length > 0)
                {
                    if (!_particle.gameObject.activeSelf) _particle.gameObject.SetActive(true);
                    for (int i = 0; i < sprites.Length; i++)
                    {
                        FeedbackMaterial.SetTexture("_MainTex", sprites[i].texture);
                        WeaponUsedMMFeedback.PlayFeedbacks(transform.position);
                    }
                }
            }
        }
        public void Knock(GameObject obj)
        {
            if (isBoss) return;
            if(isBomb)
            {
                DieStart(false);
            }
            else
            {
                if (!isHit && !isDie)
                {
                    Physics2D.IgnoreLayerCollision(8, 12, false);
                    knockDir = obj.transform.position - transform.position;
                    knockDir.Normalize();
                    knockDis = 2;
                    animator.SetTrigger("HitTrigger");
                    isHit = true;
                }
            }
        }
        public void HitEnd()
        {
            isHit = false;
            sr.color = Color.white;
        }
        IEnumerator HitEffect()
        {
            while (isHit && !isDie)
            {
                sr.color = HitColor;
                yield return new WaitForSeconds(HitEffectDelay);
                sr.color = HitColor2;
                yield return new WaitForSeconds(HitEffectDelay);
                sr.color = HitColor3;
                yield return new WaitForSeconds(HitEffectDelay);
                sr.color = HitColor4;
                yield return new WaitForSeconds(HitEffectDelay);
                sr.color = Color.white;
            }
            HitCoroutine = null;
        }
        IEnumerator TimerCoroutine()
        {
            string tId = mobData.ID;
            if (tId.Contains("_t")) tId = tId.Substring(0, tId.Length - 3);

            SoundManager.instance.StartAudio(new string[1] { "Monster/predator_start" }, VOLUME_TYPE.EFFECT);
            TextOpenController.instance.TextClose(Color.white);
            PlayerCombat.ps = PlayerState.Predators;
            Vector3 pos = Player.instance.transform.position;
            if (Player.instance.GetComponent<SpriteRenderer>().flipX)
                pos.x -= 1.25f;
            else
                pos.x += 1.25f;
            GetComponent<BoxCollider2D>().enabled = false;
            transform.position = pos;
            isMoveChange = true;
            timerObj.SetActive(true);
            float t = 0.0f;
            while (isUsing)
            {
                if (_audio4 && !_audio4.isPlaying)
                {
                    _audio4.volume = (SoundManager.isMute || (PDG.Dungeon.instance == null ? false : PDG.Dungeon.instance.isPause) || (PDG.Player.instance == null ? false : PDG.Player.instance.isDie)) ? 0 : SoundManager.EffectVolume;
                    _audio4.Play();
                }

                Vector3 p = controllerObj.transform.position;
                p.y += 0.5f;
                timerObj.transform.position = UnityEngine.Camera.main.WorldToScreenPoint(p);
                t += Time.deltaTime;
                int tInt = Mathf.CeilToInt(fTime - t);
                timerObj.transform.Find("Text").GetComponent<Text>().text = tInt.ToString();
                timerObj.transform.Find("Image").GetComponent<Image>().fillAmount = t / fTime;
                timerObj.transform.Find("TextCont").GetComponent<Text>().text = LocalizeManager.GetLocalize("predators_ing");
                if (t >= fTime) break;
                yield return new WaitForSeconds(Time.deltaTime);
            }
            if (isUsing)
            {
                Dungeon.instance.isPredatorCollTime = true;
                Dungeon.instance.predatorCoolTimeNow = 0.0f;
                Inventorys.Instance.predatorCollTimeImage.transform.parent.Find("CollTimeImageP").gameObject.SetActive(true);
                Inventorys.Instance.predatorCollTimeImage.fillAmount = 0;

                if (hpBar != null) Destroy(hpBar);
                mobInfo.curHp = 0;
                if (isWarpMonster) Dungeon.instance.curWarpCount++;
                if (WeaponsSlot) WeaponsSlot.SetActive(false);
                isDie = true;
                if (PlayerBuffController.buffList.FindIndex(item => item.Equals("offensive_02")) != -1)
                {
                    Player.instance.fPlusDamageTime_offensive02 = 1.0f;
                    Player.instance.fPlusDamage_offensive02 = 2;
                }
                if (PlayerBuffController.buffList.FindIndex(item => item.Equals("offensive_03")) != -1)
                {
                    Player.instance.nBuff_offensive03_count++;
                }
                if (PlayerBuffController.buffList.FindIndex(item => item.Equals("offensive_04")) != -1)
                {
                    Player.instance.fPlusDamageTime_offensive04 = 5.0f;
                    if (Player.instance.fPlusDamage_offensive02 < 30) Player.instance.fPlusDamage_offensive02 += 5;
                }
                if (!isSub) ItemDrop();
                if (bCol) bCol.enabled = false;
                GameObject obj2 = ObjManager.Call().GetObject("MonsterDeath");
                obj2.transform.position = transform.position;
                if (obj2.GetComponent<Animator>()) obj2.GetComponent<Animator>().Rebind();
                obj2.SetActive(true);
                ResultPanelScript.instance.killCount++;
                if (SteamStatsAndAchievements.instance != null) SteamStatsAndAchievements.instance.SettingStat(tId + "_hunting");
                if (SteamStatsAndAchievements.instance != null) SteamStatsAndAchievements.instance.SettingStat("stat_hunting_total_count");
                DieStart(true);
                if (Dungeon.instance.newObjectSelectedObj == gameObject)
                {
                    Dungeon.instance.newObjectSelectedObj = null;
                    Dungeon.instance.newObjectSelected = false;
                    controllerObj.SetActive(false);
                }
                if (predatorEndEffect1)
                {
                    GameObject obj = Instantiate(predatorEndEffect1);
                    obj.transform.position = transform.position;
                    Destroy(obj, 2f);
                }
                if (predatorEndEffect2)
                {
                    GameObject obj = Instantiate(predatorEndEffect2);
                    obj.transform.position = transform.position;
                    Destroy(obj, 2f);
                }
                if (predatorEndEffect3)
                {
                    GameObject obj = Instantiate(predatorEndEffect3);
                    obj.transform.position = transform.position;
                    Destroy(obj, 2f);
                }
            }
            PlayerCombat.ps = PlayerState.Idle;
            isUsing = false;
            timerObj.SetActive(false);
            PlayerCombat.instance.tCoroutine = null;
        }
        #endregion
        #region -- Move Method --
        private void WeaponsControl()
        {
            if (isTutorial) return;
            if (isWeaponMode)
            {
                Vector3 firePos = WeaponsSlot.transform.position;
                Vector3 crossPos = targetObj.transform.position;
                firePos.y += 0.4f;
                crossPos.y += 0.4f;
                firePos.z = crossPos.z = 0;
                angle = GetAngle(firePos, crossPos);
                while (angle > 360.0f) angle -= 360.0f;
                while (angle < 0) angle += 360.0f;

                if ((angle >= 0.0f && angle <= 22.5f) || (angle > 337.5f && angle <= 360.0f)) directionWeapons = Direction.Right;
                else if (angle > 22.5f && angle <= 67.5f) directionWeapons = Direction.RightUp;
                else if (angle > 67.5f && angle <= 112.5f) directionWeapons = Direction.Up;
                else if (angle > 112.5f && angle <= 157.5f) directionWeapons = Direction.LeftUp;
                else if (angle > 157.5f && angle <= 202.5f) directionWeapons = Direction.Left;
                else if (angle > 202.5f && angle <= 247.5f) directionWeapons = Direction.LeftDown;
                else if (angle > 247.5f && angle <= 292.5f) directionWeapons = Direction.Down;
                else if (angle > 292.5f && angle <= 337.5f) directionWeapons = Direction.RightDown;
                else directionWeapons = Direction.None;
                animator.SetInteger("Direction", (int)directionWeapons);
            }
            else
            {
                Vector3 firePos = transform.position;
                firePos.y += 0.4f;
                firePos.z = 0;
                angle = GetAngle(firePos, firePos+ dir);
                while (angle > 360.0f) angle -= 360.0f;
                while (angle < 0) angle += 360.0f;

                if ((angle >= 0.0f && angle <= 22.5f) || (angle > 337.5f && angle <= 360.0f)) direction = Direction.Right;
                else if (angle > 22.5f && angle <= 67.5f) direction = Direction.RightUp;
                else if (angle > 67.5f && angle <= 112.5f) direction = Direction.Up;
                else if (angle > 112.5f && angle <= 157.5f) direction = Direction.LeftUp;
                else if (angle > 157.5f && angle <= 202.5f) direction = Direction.Left;
                else if (angle > 202.5f && angle <= 247.5f) direction = Direction.LeftDown;
                else if (angle > 247.5f && angle <= 292.5f) direction = Direction.Down;
                else if (angle > 292.5f && angle <= 337.5f) direction = Direction.RightDown;
                else direction = Direction.None;
                animator.SetInteger("Direction", (int)direction);
            }
        }
        private void Move(Vector2 axisDirection, float _speed, float _dis, bool _isKnock)
        {
            if (animator.GetBool("isMove") && !animator.GetBool("isWeapons")) animator.SetInteger("Direction", (int)direction);
            animator.SetBool("isMove", true);
            if(_isKnock && !isNotKnock)
            {
                float zz = transform.localEulerAngles.z;
                transform.localEulerAngles = new Vector3(0, 0, 0);
                RaycastHit2D hit2 = Physics2D.Raycast(transform.position + (Vector3)axisDirection, axisDirection, 1, 1 << 8);
                if (!hit2) transform.Translate(axisDirection * _speed * Time.deltaTime);
                transform.localEulerAngles = new Vector3(0, 0, zz);
            }
            else
            {
                transform.Translate(axisDirection * _speed * Time.deltaTime);
            }
        }
        private void Flip()
        {
            Vector2 theScale = transform.localScale;
            switch (isWeaponMode ? directionWeapons : direction)
            {
                case Direction.Right:
                    {
                        theScale.x = -1;
                        break;
                    }
                case Direction.RightDown:
                    {
                        theScale.x = -1;
                        break;
                    }
                case Direction.RightUp:
                    {
                        theScale.x = -1;
                        break;
                    }
                case Direction.Left:
                    {
                        theScale.x = 1;
                        break;
                    }
                case Direction.LeftUp:
                    {
                        theScale.x = 1;
                        break;
                    }
                case Direction.LeftDown:
                    {
                        theScale.x = 1;
                        break;
                    }
                case Direction.Up:
                    {
                        theScale.x = 1;
                        break;
                    }
                case Direction.Down:
                    {
                        theScale.x = 1;
                        break;
                    }
                default:
                    theScale.x = 1;
                    break;
            }
            theScale.y = 1;
            transform.localScale = theScale;
        }
        #endregion
        #region -- Tool --
        public static float GetAngle(Vector3 vStart, Vector3 vEnd)
        {
            Vector3 v = vEnd - vStart;

            return Mathf.Atan2(v.y, v.x) * Mathf.Rad2Deg;
        }
        public static Vector3 GetPosition(Vector3 vStart, float angle)
        {
            float x = Mathf.Cos(Mathf.Deg2Rad * angle) + vStart.x;
            float y = Mathf.Sin(Mathf.Deg2Rad * angle) + vStart.y;

            return new Vector3(x, y, 0);
        }
        private int GetPercentege(float _per, string[] list)
        {
            int[] per = new int[Mathf.FloorToInt(100 * list.Length)];
            for (int i = 0; i < per.Length; i++) per[i] = -1;

            for (int i = 0; i < list.Length; i++)
            {
                for (int j = 0; j < Mathf.FloorToInt(_per * 100); j++) 
                {
                    per[(i * 100) + j] = i;
                }
            }
            if (per.Length == 0) return -1;
            return per[Random.Range(0, per.Length)];
        }
        #endregion
        #region -- AI --
        void GoblinMove()
        {
            if (!isGoblinMove) return;
            if (isGrab) return;
            if (isPull) return;
            if (isUsing) return;
            if (isFreeze) return;
            if (isAttack) return;
            if (isPathPossible)
            {
                if (path == null)
                {
                    isMoveChange = true;
                    return;
                }

                Physics2D.IgnoreLayerCollision(8, 12, true);
                if (currentWaypoint >= path.vectorPath.Count)
                {
                    if (pathIsEnabled) return;
                    //경로의 끝에 도착
                    pathIsEnabled = true;
                    isMoveChange = true;
                    return;
                }
                pathIsEnabled = false;
                fGoblinMoveTime -= Time.deltaTime;
                if (fGoblinMoveTime <= 0.0f && !isMoveStop)
                {
                    isMoveChange = true;
                    fGoblinMoveTime = Random.Range(3f, 6f);
                    nGoblinMoveCount++;
                    if (nGoblinMoveCount >= nGoblinMoveCountMax)
                    {
                        fGoblinMoveTime = 2.0f;
                        GetComponent<SpriteRenderer>().material = Resources.Load<Material>("Item/Material/auxiliary_aura_5");
                        isMoveStop = true;
                    }
                    return;
                }
                else if (fGoblinMoveTime < 0.0f && isMoveStop && mobInfo.curHp > 0 && !isGoblinEnd)  
                {
                    GameObject obj = Instantiate(GoblinPortalObj);
                    obj.transform.localEulerAngles = new Vector3(-40, 180, 180);
                    obj.transform.position = new Vector3(transform.position.x, transform.position.y - 0.5f, 0);
                    obj.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
                    Destroy(obj, 6f);
                    fGoblinMoveTime = 5.0f;
                    isGoblinEnd = true;
                }
                else if (fGoblinMoveTime < 0.0f && isMoveStop && mobInfo.curHp > 0 && isGoblinEnd)
                {
                    ResetMonster();
                }
                if (isMoveStop) return;
                // 다음 웨이 포인트 방향
                dir = (path.vectorPath[currentWaypoint] - transform.position).normalized;
                transform.eulerAngles = Vector3.zero;
                Move(dir, speed, 1, false);
                float dist = Vector3.Distance(transform.position, path.vectorPath[currentWaypoint]);
                if (dist < nextWaypointDistance)
                {
                    currentWaypoint++;
                    return;
                }
            }
        }
        void RoadingMove()
        {
            if (isGrab) return;
            if (isPull) return;
            if (isUsing) return;
            if (isFreeze) return;
            if (isAttack) return;
            if (isPathPossible)
            {
                if (path == null)
                {
                    isMoveChange = true;
                    return;
                }

                Physics2D.IgnoreLayerCollision(8, 12, true);
                if (currentWaypoint >= path.vectorPath.Count)
                {
                    if (pathIsEnabled) return;
                    //경로의 끝에 도착
                    pathIsEnabled = true;
                    isMoveChange = true;
                    return;
                }
                pathIsEnabled = false;
                if (isMoveStop) return;
                // 다음 웨이 포인트 방향
                dir = (path.vectorPath[currentWaypoint] - transform.position).normalized;
                transform.eulerAngles = Vector3.zero;
                Move(dir, speed, 1, false);
                if (mobData.ID.Contains("woodlouse") || mobData.ID.Contains("snake_ice"))
                {
                    float zz = GetAngle(transform.position, path.vectorPath[currentWaypoint]);
                    zz -= 270.0f;
                    transform.eulerAngles = new Vector3(0, 0, zz);
                }
                float dist = Vector3.Distance(transform.position, path.vectorPath[currentWaypoint]);
                if (dist < nextWaypointDistance)
                {
                    currentWaypoint++;
                    return;
                }
            }
        }
        IEnumerator Dash()
        {
            // 띠링 + 느낌표
            SoundManager.instance.StartAudio(new string[1] { "Monster/ade" }, VOLUME_TYPE.EFFECT);
            RaycastHit2D hit = Physics2D.CircleCast(transform.position, 20, Vector2.zero, 0, 1 << 9 | 1 << 30);
            if (hit && targetObj == null) targetObj = hit.collider.gameObject;
            Vector3 targetPos = targetObj.transform.position;
            Vector3 dir = targetPos - transform.position;
            dir.Normalize();
            yield return new WaitForSeconds(0.5f);
            while(!isDie)
            {
                float dis = Vector3.Distance(transform.position, targetPos);
                if(dis <= 1.8f && !isGrab && !isPull && !isPull && !isUsing)
                {
                    // 터짐?
                    DieStart(false);
                    break;
                }
                else
                {
                    if (!isFreeze && !isGrab && !isPull && !isUsing)
                    {
                        if (!animator.GetBool("Attack"))
                        {
                            animator.SetBool("Attack", true);
                            animator.SetTrigger("AttackTrigger");
                        }
                        if (isPathPossible)
                        {
                            if (path == null)
                            {
                                isMoveChange = true;
                            }
                            else
                            {
                                if (currentWaypoint >= path.vectorPath.Count)
                                {
                                    if (!pathIsEnabled)
                                    {
                                        //경로의 끝에 도착
                                        pathIsEnabled = true;
                                        DieStart(false);
                                    }
                                }
                                else
                                {
                                    pathIsEnabled = false;
                                    if (!isMoveStop)
                                    {
                                        // 다음 웨이 포인트 방향
                                        dir = (path.vectorPath[currentWaypoint] - transform.position).normalized;
                                        transform.eulerAngles = Vector3.zero;
                                        Move(dir, speed * 3, 1, false);
                                        float dist = Vector3.Distance(transform.position, path.vectorPath[currentWaypoint]);
                                        if (dist < nextWaypointDistance)
                                        {
                                            currentWaypoint++;
                                        }
                                    }
                                }
                            }
                        }
                        yield return new WaitForSeconds(Time.deltaTime);
                    }
                    else
                    {
                        yield return null;
                    }
                }
            }
        }
        void AIMove()
        {
            if (isFreeze) return;

            RaycastHit2D hit = Physics2D.BoxCast(transform.position, new Vector2(0.2f, 0.2f), 0, Vector2.zero, 0, 1 << 8);
            if (hit && !isMove222 && !isPathPossible && Dungeon.instance.nowRoomType.Equals("normal")) 
            {
                isMove222 = true;
            }
            else if(isMove222)
            {
                Vector3 endpos = targetObj.transform.position;
                Vector3 dir = endpos - transform.position;
                dir.Normalize();
                float dis = Vector3.Distance(transform.position, endpos);
                if (dis > 5) transform.Translate(dir * speed * Time.deltaTime);
                else
                {
                    isTargeting = true;
                    if (!mobData.ID.Contains("piko"))
                    {
                        isWeaponMode = true;
                        if (isFire) WeaponsSlot.SetActive(true);
                        else WeaponsSlot.SetActive(false);
                        animator.SetBool("isWeapons", isWeaponMode);
                    }
                    else
                    {
                        WeaponsSlot.SetActive(false);
                    }
                }
                isMove222 = false;
            }
            else if (isPathPossible)
            {
                if (path == null)
                {
                    isMoveChange = true;
                    return;
                }
                if (!isHit)
                {
                    Physics2D.IgnoreLayerCollision(8, 12, true);
                    if (Vector3.Distance(targetObj.transform.position, transform.position) < fAttackRange)
                    {
                        // 공격해 
                        if (isFire && !isArm)
                        {
                            if (mobData.Animtype.Equals("Mob2") || mobData.Animtype.Equals("Mob3") || mobData.Animtype.Equals("Mob4") || mobData.Animtype.Equals("Mob5") || mobData.Animtype.Equals("Mob6") ||
                                mobData.Animtype.Equals("SnakeIce") || mobData.Animtype.Equals("StarFishIce") || mobData.Animtype.Equals("Slime_fire") || mobData.Animtype.Equals("Plant"))
                            {
                                if (fAttackDelayNow > fAttackDelay)
                                {
                                    if (mobData.Animtype.Equals("StarFishIce") && !isAttack)
                                    {
                                        isAttack = true;
                                        if (Random.Range(0, 200) < 80 && !isSub)
                                        {
                                            animator.SetTrigger("AttackSplit");
                                            StarFishSplit();
                                        }
                                        else
                                        {
                                            StartFishAttack();
                                            animator.SetTrigger("AttackTrigger");
                                        }
                                    }
                                    else if (mobData.ID.Contains("slime_fire") && !isAttack) 
                                    {
                                        isAttack = true;
                                        SlimeFire();
                                    }
                                    else if(mobData.ID.Contains("mush") || mobData.ID.Contains("nepenthes_man"))
                                    {
                                        isAttack = true;
                                        MushRedFire();
                                    }
                                    else if (mobData.ID.Contains("rootman") && !isAttack)
                                    {
                                        isAttack = true;
                                        RootManAttack();
                                    }
                                    else if (mobData.ID.Contains("swampman") && !isAttack)
                                    {
                                        isAttack = true;
                                        SwampmanAttack();
                                    }
                                    else
                                        animator.SetTrigger("AttackTrigger");


                                    fAttackDelayNow = 0.0f;
                                }
                            }
                            else
                                Fire();
                        }
                        else if(isArm)
                        {
                            ArmAttack();
                        }
                        else
                        {
                            pathIsEnabled = true;
                            isMoveChange = true;
                        }
                    }
                    else
                    {
                        if (mobData.ID.Contains("nepenthes_man"))
                            isMoveStop = false;
                    }
                    {
                        if (currentWaypoint >= path.vectorPath.Count)
                        {
                            if (pathIsEnabled)
                                return;
                            //경로의 끝에 도착
                            pathIsEnabled = true;
                            isMoveChange = true;
                            return;
                        }
                        pathIsEnabled = false;

                        if (isMoveStop) return;
                        // 다음 웨이 포인트 방향
                        dir = (path.vectorPath[currentWaypoint] - transform.position).normalized;
                        transform.eulerAngles = Vector3.zero;
                        Move(dir, speed, 1, false);
                        if (mobData.ID.Contains("woodlouse") || mobData.ID.Contains("snake_ice"))
                        {
                            float zz = GetAngle(transform.position, path.vectorPath[currentWaypoint]);
                            zz -= 270.0f;
                            transform.eulerAngles = new Vector3(0, 0, zz);
                        }
                        float dist = Vector3.Distance(transform.position, path.vectorPath[currentWaypoint]);
                        if (dist < nextWaypointDistance)
                        {
                            currentWaypoint++;
                            if(mobData.ID.Contains("rootman") && Random.Range(0, 100) > 90 && isTargeting)
                            {
                                int seedcount = 1;
                                int[] xt = new int[seedcount];
                                int[] yt = new int[seedcount];
                                for (int j = 0; j < seedcount; j++)
                                {
                                    while (true)
                                    {
                                        bool check = false;
                                        int x = Mathf.FloorToInt(transform.position.x);
                                        int y = Mathf.FloorToInt(transform.position.y);
                                        x += Random.Range(-3, 3);
                                        y += Random.Range(-3, 3);
                                        for (int i = 0; i < j; i++)
                                        {
                                            if (xt[i] == x && yt[i] == y)
                                            {
                                                if (Dungeon.instance.board.map[xt[i], yt[i]] != RootManMoveObj)
                                                {
                                                    check = true;
                                                    break;
                                                }
                                            }
                                        }
                                        if (!check)
                                        {
                                            xt[j] = x;
                                            yt[j] = y;
                                            break;
                                        }
                                    }
                                    
                                    GameObject obj = Instantiate(RootManMoveObj);
                                    obj.transform.position = new Vector3(xt[j], yt[j]);
                                    Dungeon.instance.board.map[xt[j], yt[j]] = RootManMoveObj;
                                }
                            }
                            return;
                        }
                    }
                }
                else
                {
                    dir = (transform.position - (transform.position + (Vector3)knockDir)).normalized;
                    Move(dir, knockSpeed * knockDis, knockDis, true);
                }
            }
            else if (isHit)
            {
                Physics2D.IgnoreLayerCollision(8, 12, false);
                dir = (transform.position - (transform.position + (Vector3)knockDir)).normalized;
                Move(dir, knockSpeed * knockDis, knockDis, true);
            }
        }
        private void MushRedFire()
        {
            _audio.volume = (SoundManager.isMute || (PDG.Dungeon.instance == null ? false : PDG.Dungeon.instance.isPause) || (PDG.Player.instance == null ? false : PDG.Player.instance.isDie)) ? 0 : SoundManager.EffectVolume;
            _audio.Play();
            isMoveStop = true;
            animator.SetTrigger("AttackTrigger");
            StartCoroutine(BulletFire(0));
        }
        private void SwampmanAttack()
        {
            _audio.volume = (SoundManager.isMute || (PDG.Dungeon.instance == null ? false : PDG.Dungeon.instance.isPause) || (PDG.Player.instance == null ? false : PDG.Player.instance.isDie)) ? 0 : SoundManager.EffectVolume;
            _audio.Play();
            isMoveStop = true;
            animator.SetTrigger("AttackTrigger");
            StartCoroutine(SwampAttack());
        }
        IEnumerator SwampAttack()
        {
            yield return new WaitForSeconds(0.25f);
            GameObject pObj = Resources.Load<GameObject>("Object/Prefabs/ant_poison");
            int ran = Random.Range(1, 5);
            for (int i = 0; i < ran; i++)
            {
                int x = Mathf.FloorToInt(transform.position.x);
                int y = Mathf.FloorToInt(transform.position.y);

                int ranX = x + (Random.Range(-2, 2));
                int ranY = y + (Random.Range(-2, 2));

                GameObject obj = Instantiate(pObj);
                obj.transform.position = new Vector3(ranX, ranY);
                if (obj.GetComponent<DrumBomb>()) obj.GetComponent<DrumBomb>().InitSetting(2, new Vector3(ranX, ranY), pObj, true, false, 0.25f, mobData.ID);
            }
            yield return new WaitForSeconds(0.125f);
            isAttack = false;
            isMoveStop = false;
        }
        private void RootManAttack()
        {
            _audio.volume = (SoundManager.isMute || (PDG.Dungeon.instance == null ? false : PDG.Dungeon.instance.isPause) || (PDG.Player.instance == null ? false : PDG.Player.instance.isDie)) ? 0 : SoundManager.EffectVolume;
            _audio.Play();
            isMoveStop = true;
            animator.SetTrigger("AttackTrigger");
            StartCoroutine(RootManAttack2());
        }
        IEnumerator RootManAttack2()
        {
            yield return new WaitForSeconds(0.25f);
            GameObject obj = Instantiate(RootManAttackFireObj);
            obj.transform.position = WeaponsFireTransform.position;
            Destroy(obj, 1);
            yield return new WaitForSeconds(0.25f);
            isMoveStop = false;
            isAttack = false;
        }
        private void SlimeFire()
        {
            StartCoroutine(SlimeFires());
        }
        IEnumerator SlimeFires()
        {
            string tId = mobData.ID;
            if (tId.Contains("_t")) tId = tId.Substring(0, tId.Length - 3);
            isMoveStop = true;
            SoundManager.instance.StartAudio(new string[1] { "Monster/" + tId }, VOLUME_TYPE.EFFECT);
            if (!isGrab && !isUsing && !isDie && !isPull && !isFreeze) animator.SetTrigger("AttackTrigger");
            yield return new WaitForSeconds(0.5f);
            isMoveStop = false;
            isAttack = false;
        }
        private void StarFishSplit()
        {
            StartCoroutine(StarFishSplitCoroutine());
        }
        IEnumerator StarFishSplitCoroutine()
        {
            string tId = mobData.ID;
            if (tId.Contains("_t")) tId = tId.Substring(0, tId.Length - 3);
            AudioClip clip = Resources.Load<AudioClip>("Sound/Monster/" + tId);
            float dis = Vector3.Distance(PDG.Player.instance.transform.position, transform.position);
            if (dis > 10) _audio2.volume = 0;
            float tVolume =  (SoundManager.isMute || (PDG.Dungeon.instance == null ? false : PDG.Dungeon.instance.isPause) || (PDG.Player.instance == null ? false : PDG.Player.instance.isDie)) ? 0 : SoundManager.EffectVolume;
            float t = tVolume / 10;
            _audio2.volume = tVolume - (dis * t);
            _audio2.PlayOneShot(clip);

            isMoveStop = true;
            yield return new WaitForSeconds(2.0f);
            isMoveStop = false;
            isAttack = false;
        }
        public void AttackSplit()
        {
            //분열
            if (isGrab || isUsing || isDie || isPull || isFreeze) return;
            StageData sData = System.Array.Find(SheetManager.Instance.StageDB.dataArray, item => item.Type2.Equals("normalstage") && item.Type1.Equals(Dungeon.instance.StageName));
            MonstersData mobDataSub = System.Array.Find(SheetManager.Instance.MonsterDB.dataArray, item => item.ID.Equals(mobData.ID));
            GameObject _obj = ObjManager.Call().GetObject("Monster");
            _obj.transform.position = transform.position;
            _obj.SetActive(true);
            _obj.GetComponent<MonsterArggro>().InitSetting(Player.instance.gameObject, mobDataSub, Random.Range(10, 15), false, mobDataSub.Animtype);
            _obj.GetComponent<MonsterArggro>().isStart = true;
            _obj.GetComponent<MonsterArggro>()._monster.gameObject.SetActive(true);
            _obj.GetComponent<MonsterArggro>()._monster.isSub = true;
        }
        public void StartFishAttack()
        {
            if (!isDie) animator.SetBool("attackEnd", false);
            isMoveStop = true;
            if (rollingCoroutine != null) StopCoroutine(rollingCoroutine);
            rollingCoroutine = StartCoroutine(AttackRolling(true));
        }
        void HideGhost()
        {
            if (isFreeze) return;

            RaycastHit2D hit = Physics2D.BoxCast(transform.position, new Vector2(0.2f, 0.2f), 0, Vector2.zero, 0, 1 << 8);
            if (hit && !isMove222 && !isPathPossible && Dungeon.instance.nowRoomType.Equals("normal"))
            {
                isMove222 = true;
            }
            else if (isMove222)
            {
                Vector3 endpos = targetObj.transform.position;
                Vector3 dir = endpos - transform.position;
                dir.Normalize();
                float dis = Vector3.Distance(transform.position, endpos);
                if (dis > 5) transform.Translate(dir * speed * Time.deltaTime);
                else
                {
                    isTargeting = true;
                    if (!mobData.ID.Contains("piko"))
                    {
                        isWeaponMode = true;
                        if (isFire) WeaponsSlot.SetActive(true);
                        else WeaponsSlot.SetActive(false);
                        animator.SetBool("isWeapons", isWeaponMode);
                    }
                    else
                    {
                        WeaponsSlot.SetActive(false);
                    }
                }
                isMove222 = false;
            }
            else if (isHit)
            {
                Physics2D.IgnoreLayerCollision(8, 12, false);
                dir = (transform.position - (transform.position + (Vector3)knockDir)).normalized;
                Move(dir, knockSpeed * knockDis, knockDis, true);
            }
            else if (isPathPossible)
            {
                if (path == null)
                {
                    return;
                }
                if (!isHit)
                {
                    Physics2D.IgnoreLayerCollision(8, 12, true);
                    if (Vector3.Distance(targetObj.transform.position, transform.position) < fAttackRange)
                    {
                        // 공격해 
                        if(isHide && !isAttack)
                        {
                            isAttack = true;
                            StartCoroutine(ShowAttack(true));
                        }
                        else if(!isHide && !isAttack)
                        {
                            isAttack = true;
                            StartCoroutine(ShowAttack(false));
                        }
                        pathIsEnabled = true;
                        isMoveChange = true;
                    }

                    if (currentWaypoint >= path.vectorPath.Count)
                    {
                        if (pathIsEnabled)
                            return;
                        //경로의 끝에 도착
                        pathIsEnabled = true;
                        isMoveChange = true;
                        return;
                    }
                    pathIsEnabled = false;

                    if (isMoveStop) return;
                    // 다음 웨이 포인트 방향
                    dir = (path.vectorPath[currentWaypoint] - transform.position).normalized;
                    Move(dir, speed, 1, false);
                    float dist = Vector3.Distance(transform.position, path.vectorPath[currentWaypoint]);
                    if (dist < nextWaypointDistance)
                    {
                        currentWaypoint++;
                        return;
                    }
                }
                else
                {
                    Physics2D.IgnoreLayerCollision(8, 12, false);
                    dir = (transform.position - (transform.position + (Vector3)knockDir)).normalized;
                    Move(dir, knockSpeed * knockDis, knockDis, true);
                }
            }
         
        }
        IEnumerator ShowAttack(bool _isHide)
        {
            string tId = mobData.ID;
            if (tId.Contains("_t")) tId = tId.Substring(0, tId.Length - 3);

            if (_isHide)
            {
                animator.SetTrigger("show");
                yield return new WaitForSeconds(1.0f);
                isInvincibility = false;
            }
            isMoveStop = true;
            Vector3[] dirs;
            List<GameObject> bulletList2 = new List<GameObject>();
            _audio.volume =  (SoundManager.isMute || (PDG.Dungeon.instance == null ? false : PDG.Dungeon.instance.isPause) || (PDG.Player.instance == null ? false : PDG.Player.instance.isDie)) ? 0 : SoundManager.EffectVolume;
            _audio.Play();
            Vector3 nPos = transform.position;
            for (int i = 0; i < 8; i++)
            {
                float angle = (360.0f / 8.0f) * i;

                Vector3 pos = GetPosition(nPos, angle);
                Vector3 dir = pos - nPos;
                dir.Normalize();
                GameObject obj = ObjManager.Call().GetObject("Bullet");
                obj.GetComponent<BulletController>().firePosition = transform.position;
                obj.GetComponent<BulletController>().isMonster = true;
                obj.GetComponent<BulletController>().monsterName = tId;
                obj.GetComponent<BulletController>().bulletType = "Monster_Bullet";
                obj.GetComponent<BulletController>().fSpeed = mobData.Bulletspeed;
                obj.GetComponent<BulletController>().fAngle = 0;
                obj.GetComponent<BulletController>().dir = Vector2.zero;
                obj.GetComponent<BulletController>().fRange = 20;
                obj.GetComponent<BulletController>().isPenetrate = false;
                obj.GetComponent<BulletController>().isRightAttack = false;
                obj.transform.position = pos;
                obj.transform.localEulerAngles = Vector3.zero;
                obj.SetActive(true);
                obj.GetComponent<BulletController>().BulletSetting();
                bulletList2.Add(obj);
            }
            yield return new WaitForSeconds(0.3f);
            Vector3 dir2 = PDG.Player.instance.transform.position - nPos;
            dir2.Normalize();
            for (int i = 0; i < bulletList2.Count; i++)
            {
                if (!bulletList2[i].activeSelf) continue;
                bulletList2[i].GetComponent<BulletController>().dir = dir2;
                bulletList2[i].GetComponent<BulletController>().BulletFire();
            }
            yield return new WaitForSeconds(3.0f);
            isAttack = false;
            animator.SetTrigger("hide");
            isHide = true;
            isInvincibility = true;
            isMoveStop = false;
        }
        void ArmAttack()
        {
            if(ArmCoroutine == null && !isGrab && fAttackDelayNow > fAttackDelay)
            {
                ArmCoroutine = StartCoroutine(ArmCoroutineReal());
                fAttackDelayNow = 0.0f;
            }
        }
        public bool isBackgroundAttack = false;
        public float armDistance = 1.5f;
        IEnumerator ArmCoroutineReal()
        {
            SoundManager.instance.StartAudio(new string[1] { "Weapon/tentacle_arm" }, VOLUME_TYPE.EFFECT);
            Vector3 targetPos = PDG.Player.instance.transform.position;
            Vector3 tDir = targetPos - armObj.transform.position;
            tDir.Normalize();
            float angle = GetAngle(armObj.transform.position, targetPos);
            armObj.SetActive(true);
            isBackgroundAttack = false;
            for (int i = 0; i < armPieces.Length; i++)
            {
                if (isBackgroundAttack) break;
                armPieces[i].SetActive(true);
                armPieces[i].transform.localPosition = tDir * (armDistance * i);
                armPieces[i].transform.localEulerAngles = new Vector3(0, 0, angle + Random.Range(-mobData.Aim, mobData.Aim));
                armPieces[i].GetComponent<SpriteRenderer>().sortingOrder = i;
                if (i % 2 == 0) yield return null;
            }
            for (int i = armPieces.Length - 1; i >= 0; i--) 
            {
                armPieces[i].transform.localPosition = Vector3.zero;
                armPieces[i].SetActive(false);
                if (i % 2 == 0) yield return null;
            }
            armObj.SetActive(false);
            ArmCoroutine = null;
        }
        IEnumerator UpdatePath()
        {
            while(!Dungeon.LoadingClear)
            {
                yield return null;
            }
            float mTime = 3.0f;
            Vector3 pPos = transform.position;
            Vector3 bombPos = targetObj == null ? Vector3.zero : targetObj.transform.position;
            bool isBombTarget = false;
            while (!isDie)
            {
                Vector3 pos = transform.position;
                if(pPos.Equals(pos)) mTime -= Time.deltaTime;
                if(mTime <= 0.0f)
                {
                    pPos = transform.position;
                    mTime = 3.0f;
                    isMoveChange = true;
                }
                if (isRoaming || isGoblin)
                {
                    if (isMoveChange)
                    {
                        Vector3 newPos;
                        if(isAttack)
                        {
                            newPos = Player.instance.transform.position;
                        }
                        else
                        {
                            while (true)
                            {
                                int X = Random.Range(80, Dungeon.instance.matrix.rows - 80);
                                int Y = Random.Range(80, Dungeon.instance.matrix.columns - 80);
                                if (Dungeon.instance.Find(X, Y, Dungeon.instance.floor_1))
                                {
                                    newPos = new Vector3(X, Y);
                                    break;
                                }
                            }
                        }
                        Vector3 newPos2 = new Vector3(transform.position.x, transform.position.y, 0);
                        Vector3 newPos3 = new Vector3(newPos.x, newPos.y, 0);
                        //현재 자신의 노드와 목표지점의 노드를 비교하여 그곳까지 갈수있는지 확인
                        GraphNode node1 = AstarPath.active.GetNearest(newPos2, NNConstraint.Default).node;
                        GraphNode node2 = AstarPath.active.GetNearest(newPos3, NNConstraint.Default).node;
                        //못간다
                        if (!PathUtilities.IsPathPossible(node1, node2))
                        {
                            path = null;
                            isPathPossible = false;
                            if (!isTutorial) isTargeting = false;
                        }
                        else
                            isPathPossible = true;

                        if (isPathPossible)
                        {
                            seeker.StartPath(transform.position, newPos, OnPathComplete);
                            isMoveChange = false;
                        }
                    }
                }
                else if (isTargeting)
                {
                    if (targetObj != null)
                    {
                        Vector3 newPos;
                        if(isBomb && !isBombTarget)
                        {
                            isBombTarget = true;
                            bombPos = targetObj.transform.position;
                        }

                        if (isBomb) newPos = bombPos;
                        else newPos = targetObj.transform.position;
                        Vector3 newPos2 = new Vector3(transform.position.x, transform.position.y, 0);
                        Vector3 newPos3 = new Vector3(newPos.x, newPos.y, 0);
                        //현재 자신의 노드와 목표지점의 노드를 비교하여 그곳까지 갈수있는지 확인
                        GraphNode node1 = AstarPath.active.GetNearest(newPos2, NNConstraint.Default).node;
                        GraphNode node2 = AstarPath.active.GetNearest(newPos3, NNConstraint.Default).node;
                        //못간다
                        if (!PathUtilities.IsPathPossible(node1, node2))
                        {
                            path = null;
                            isPathPossible = false;
                            if (!isTutorial) isTargeting = false;
                        }
                        else
                            isPathPossible = true;

                        if (isPathPossible)
                        {
                            if (isMoveChange)
                            {
                                seeker.StartPath(transform.position, newPos, OnPathComplete);
                                isMoveChange = false;
                            }
                        }
                    }
                    else
                    {
                        RaycastHit2D hit = Physics2D.CircleCast(transform.position, 40, Vector2.zero, 0, 1 << 9 | 1 << 30);
                        if (hit)
                        {
                            isTargeting = true;
                            targetObj = hit.collider.gameObject;
                        }
                    }
                }
                else
                {
                    if(isBoss)
                    {
                        isTargeting = true;
                        targetObj = Player.instance.gameObject;
                    }
                    else
                    {
                        RaycastHit2D hit = Physics2D.CircleCast(transform.position, 40, Vector2.zero, 0, 1 << 9 | 1 << 30);
                        if (hit)
                        {
                            isTargeting = true;
                            targetObj = hit.collider.gameObject;
                        }
                    }
                        
                }
                yield return null;
            }
        }
        public void OnPathComplete(Path _path)
        {
            //길에 오류가 있는지 검사
            if (!_path.error)
            {
                path = _path;
                currentWaypoint = 0;
            }
            else
                isMoveChange = true;
        }
        #endregion
        #region -- Attack --
        private void Fire()
        {
            if (fAttackDelayNow > fAttackDelay && !isUsing)
            {
                _audio.volume =  (SoundManager.isMute || (PDG.Dungeon.instance == null ? false : PDG.Dungeon.instance.isPause) || (PDG.Player.instance == null ? false : PDG.Player.instance.isDie)) ? 0 : SoundManager.EffectVolume;
                _audio.Play();
                attackAnim.SetTrigger("AttackTrigger");
                if (mobData.ID.Contains("hoodie_cannon"))
                {
                    StartCoroutine(BulletFireSpecial());
                }
                else
                {
                    StartCoroutine(BulletFire(10));
                }
                fAttackDelayNow = 0.0f;
            }
        }
        public void Mob2Fire()
        {
            if (!isMoveStop && !isUsing)
            {
                isMoveStop = true;
                StartCoroutine(BiholderAttack());
            }
        }
        public void Mob3Fire()
        {
            if (!isMoveStop && !isUsing)
            {
                StartCoroutine(AntFire());
            }
        }
        IEnumerator AntFire()
        {
            string tId = mobData.ID;
            if (tId.Contains("_t")) tId = tId.Substring(0, tId.Length - 3);
            isMoveStop = true;
            _audio.volume = (SoundManager.isMute || (PDG.Dungeon.instance == null ? false : PDG.Dungeon.instance.isPause) || (PDG.Player.instance == null ? false : PDG.Player.instance.isDie)) ? 0 : SoundManager.EffectVolume;
            _audio.Play();
            GameObject shootObj = Instantiate(Resources.Load<GameObject>("Mob/" + tId + "/" + tId + "_bomb"));
            shootObj.GetComponent<White_ant_bomb>().Throw(transform.position, PDG.Player.instance.transform.position, false, tId);
            yield return new WaitForSeconds(0.5f);
            isMoveStop = false;
            animator.SetBool("isMove", false);
        }
        public void Mob4Fire()
        {
            if (!isMoveStop && !isUsing)
            {
                isMoveStop = true;
                StartCoroutine(SwordSlimeAttack());
            }
        }
        public void Mob6Fire()
        {
            if (!isMoveStop && !isUsing)
            {
                if (mobData.ID.Contains("octopus_ball") && !isGrab)
                {
                    isMoveStop = true;
                    StartCoroutine(OctopusBallFire());
                }
                else if (mobData.ID.Contains("lion_ice"))
                    StartCoroutine(IceLionFire());
            }
        }
        public void IceSnakeFire()
        {
            StartCoroutine(IceSnakeAttackFire());
        }
        public void IceSnakeAttackStart()
        {
            isMoveStop = true;
        }
        IEnumerator IceSnakeAttackFire()
        {
            string tId = mobData.ID;
            if (tId.Contains("_t")) tId = tId.Substring(0, tId.Length - 3);
            GameObject obj = Instantiate(Resources.Load<GameObject>("Mob/" + tId + "/IceFire"), null);
            obj.transform.position = transform.Find("SnakeFirePosition").position;
            obj.GetComponent<Rigidbody2D>().AddForce((PDG.Player.instance.transform.position - transform.position).normalized * bulletSpeed, ForceMode2D.Impulse);
            yield return new WaitForSeconds(0.5f);
            isMoveStop = false;
        }
        IEnumerator IceLionFire()
        {
            // 눈에서 빔
            Vector3 leftEnd = leftLine.transform.position;
            Vector3 rightEnd = rightLine.transform.position;

            Vector3 leftpPos = PDG.Player.instance.transform.position;
            if (Vector3.Distance(Player.instance.transform.position, leftLine.transform.position) > 5)
                leftpPos = leftLine.transform.position + ((Player.instance.transform.position - leftLine.transform.position).normalized * 5);
            else
                leftpPos = Player.instance.transform.position;

            float Leftt = 0.0f;
            float angle_right = 330.0f;
            bool isRightControl = false;

            float ttt = 3.0f;
            while (ttt > 0.0f && !isDie && !isGrab && !isFreeze && !isUsing)
            {
                ttt -= Time.deltaTime;

                leftLine.gameObject.SetActive(true);
                rightLine.gameObject.SetActive(true);
                LeftLineEndEffect.SetActive(true);
                RightLineEndEffect.SetActive(true);


                Leftt += Time.deltaTime * Random.Range(2, 5);
                if (Leftt > 1.0f)
                {
                    Leftt = 0.0f;
                    leftEnd = leftpPos;
                    if (Vector3.Distance(Player.instance.transform.position, leftLine.transform.position) > 5)
                        leftpPos = leftLine.transform.position + ((Player.instance.transform.position - leftLine.transform.position).normalized * 5);
                    else
                        leftpPos = Player.instance.transform.position;
                }
                Vector3 leftEnds = Vector3.Lerp(leftEnd, leftpPos, Leftt);
                Vector3 dir = leftEnds - leftLine.transform.position;
                dir.Normalize();

                RaycastHit2D hitLeft = Physics2D.BoxCast(leftLine.transform.position, new Vector2(0.5f, 0.1f), GetAngle(leftLine.transform.position, leftLine.transform.position + dir), dir,
                    Vector3.Distance(leftLine.transform.position, leftEnds), 1 << 8 | 1 << 9 | 1 << 30);
                leftLine.positionCount = 2;
                leftLine.SetPosition(0, leftLine.transform.position);
                LeftLineEndEffect.transform.eulerAngles = new Vector3(0, 0, GetAngle(leftLine.transform.position, leftLine.GetPosition(1)) + 90);
                if (hitLeft)
                {
                    leftLine.SetPosition(1, hitLeft.point);
                    LeftLineEndEffect.transform.position = hitLeft.point;
                }
                else
                {
                    leftLine.SetPosition(1, leftEnds);
                    LeftLineEndEffect.transform.position = leftLine.GetPosition(1);
                }

                if (!isRightControl)
                {
                    angle_right += Time.deltaTime * Random.Range(32, 40);
                    if (angle_right > 330)
                    {
                        angle_right = 330;
                        isRightControl = true;
                    }
                }
                else
                {
                    angle_right -= Time.deltaTime * Random.Range(32, 40);
                    if (angle_right < 190)
                    {
                        angle_right = 190;
                        isRightControl = false;
                    }
                }

                Vector3 pos = GetPosition(rightLine.transform.position, angle_right);
                pos = pos - rightLine.transform.position;
                pos.Normalize();
                RaycastHit2D hit = Physics2D.BoxCast(rightLine.transform.position, new Vector2(0.5f, 0.1f), angle_right, pos, 5, 1 << 8 | 1 << 9 | 1 << 30);
                if (hit)
                    rightEnd = hit.point;
                else
                    rightEnd = rightLine.transform.position + pos * 5;

                RightLineEndEffect.transform.eulerAngles = new Vector3(0, 0, angle_right + 90);
                RightLineEndEffect.transform.position = rightEnd;
                rightLine.positionCount = 2;
                rightLine.SetPosition(0, rightLine.transform.position);
                rightLine.SetPosition(1, rightEnd);
                yield return new WaitForSeconds(Time.deltaTime);
            }
            leftLine.gameObject.SetActive(false);
            rightLine.gameObject.SetActive(false);
            LeftLineEndEffect.SetActive(false);
            RightLineEndEffect.SetActive(false);
            if (!isDie) animator.SetBool("AttackEnd", true);

            yield return new WaitForSeconds(0.1f);
            isMoveStop = false;
            if (!isDie) animator.SetBool("AttackEnd", false);
        }
        IEnumerator OctopusBallFire()
        {
            if (!isGrab && !isUsing)
            {
                string tId = mobData.ID;
                if (tId.Contains("_t")) tId = tId.Substring(0, tId.Length - 3);
                GameObject obj = Resources.Load<GameObject>("Mob/" + tId + "/octopusBall");
                obj = Instantiate(obj, null);
                obj.transform.position = transform.position;
                yield return new WaitForSeconds(1.0f / 60.0f * 10.0f);
                animator.SetBool("AttackEnd", true);
                yield return new WaitForSeconds(0.5f);
                isMoveStop = false;
                animator.SetBool("AttackEnd", false);
            }
        }
        Coroutine mob5Coroutine = null;
        public Transform mob5FirePos;
        public UbhShotCtrl homingShoot;
        public UbhShotCtrl randomAming;
        public UbhShotCtrl circleShoot;
        public void Mob5Fire()
        {
            if (mob5Coroutine == null)
            {
                mob5Coroutine = StartCoroutine(Mob5FireFunction());
            }
        }
        IEnumerator Mob5FireFunction()
        {
            isMoveStop = true;
            string tId = mobData.ID;
            if (tId.Contains("_t")) tId = tId.Substring(0, tId.Length - 3);
            if (mobData.ID.Contains("mummy_green"))
            {
                if(!isDie && !isGrab && !isUsing)
                {
                    randomAming._mobName = tId;
                    randomAming._bulletName = "Monster_Bullet3";
                    randomAming.StartShotRoutine();
                    yield return new WaitForSeconds(3);
                }
            }
            else if (mobData.ID.Contains("mummy_white") && !isUsing)
            {
                GameObject obj = Instantiate(Resources.Load<GameObject>("Mob/" + tId + "/" + tId + "_fire"), null);
                Vector3 _dir = PDG.Player.instance.transform.position - transform.position;
                _dir.Normalize();
                float zz = GetAngle(transform.position, PDG.Player.instance.transform.position);
                obj.transform.eulerAngles = new Vector3(0, 0, zz);
                obj.transform.position = transform.position + (_dir * 2);
                obj.GetComponent<Rigidbody2D>().AddForce(_dir * bulletSpeed, ForceMode2D.Impulse);
            }
            else
            {
                homingShoot._mobName = tId;
                homingShoot._bulletName = "stg_bullet";
                for (int i = 0; i < 4; i++)
                {
                    if (isGrab || isDie && isUsing) break;
                    homingShoot.StartShotRoutine();
                    yield return new WaitForSeconds(homingShoot.m_shotList[0].m_afterDelay);
                }
            }
            if (!isDie) 
            {
                animator.SetTrigger("AttackEnd");
            }
            isMoveStop = false;
            mob5Coroutine = null;
        }
        IEnumerator SwordSlimeAttack()
        {
            yield return StartCoroutine(BulletFire(30));
            isMoveStop = false;
        }
        IEnumerator BiholderAttack()
        {
            for (int i = 0; i < 3; i++)
            {
                _audio.volume =  (SoundManager.isMute || (PDG.Dungeon.instance == null ? false : PDG.Dungeon.instance.isPause) || (PDG.Player.instance == null ? false : PDG.Player.instance.isDie)) ? 0 : SoundManager.EffectVolume;
                _audio.Play();
                StartCoroutine(BulletFire(30));
                yield return new WaitForSeconds(0.5f);
            }
            isMoveStop = false;
        }
        IEnumerator BulletFireSpecial()
        {
            string tId = mobData.ID;
            if (tId.Contains("_t")) tId = tId.Substring(0, tId.Length - 3);

            Vector3 fireDir = targetObj.transform.position - transform.position;
            fireDir.Normalize();
            GameObject obj = ObjManager.Call().GetObject("Bullet");
            obj.GetComponent<BulletController>().firePosition = transform.position;
            obj.GetComponent<BulletController>().isMonster = true;
            obj.GetComponent<BulletController>().monsterName = tId;
            obj.GetComponent<BulletController>().bulletType = "Monster_Bullet";
            obj.GetComponent<BulletController>().fSpeed = bulletSpeed;
            obj.GetComponent<BulletController>().fAngle = angle;
            obj.GetComponent<BulletController>().dir = fireDir;
            obj.GetComponent<BulletController>().fRange = fBulletRange;
            obj.GetComponent<BulletController>().isRightAttack = false;
            obj.transform.position = WeaponsFireTransform.position;
            obj.SetActive(true);
            obj.GetComponent<BulletController>().BulletFire();
            yield return new WaitForSeconds(1.0f);
            if (obj.activeSelf)
            {
                obj.SetActive(false);
                int ranAngle = Random.Range(0, 60);
                for (int i = 0; i < 4; i++)
                {
                    fireDir = GetPosition(obj.transform.position, (i * 90) + ranAngle) - obj.transform.position;
                    fireDir.Normalize();
                    GameObject obj2 = ObjManager.Call().GetObject("Bullet");
                    obj2.GetComponent<BulletController>().firePosition = transform.position;
                    obj2.GetComponent<BulletController>().isMonster = true;
                    obj2.GetComponent<BulletController>().monsterName = tId;
                    obj2.GetComponent<BulletController>().bulletType = "Monster_Bullet";
                    obj2.GetComponent<BulletController>().fSpeed = bulletSpeed;
                    obj2.GetComponent<BulletController>().fAngle = (i * 90) + ranAngle;
                    obj2.GetComponent<BulletController>().dir = fireDir;
                    obj2.GetComponent<BulletController>().fRange = fBulletRange;
                    obj.GetComponent<BulletController>().isRightAttack = false;
                    obj2.transform.position = obj.transform.position;
                    obj2.SetActive(true);
                    obj2.GetComponent<BulletController>().BulletFire();
                    yield return new WaitForSeconds(fBulletDelay);
                }
            }
        }
        IEnumerator BulletFire(int modAngle)
        {
            if (mobData.ID.Contains("mush") || mobData.ID.Contains("nepenthes_man"))
            {
                yield return new WaitForSeconds(0.25f);
            }

            string tId = mobData.ID;
            if (tId.Contains("_t")) tId = tId.Substring(0, tId.Length - 3);

            Vector3 tPosition = targetObj.transform.position;
            for (int i = 0; i < nFireCount; i++)
            {
                for (int j = 0; j < nModCount; j++)
                {
                    if (isGrab) break;
                    if (isPull) break;
                    if (isUsing) break;
                    if (isFreeze)
                    {
                        while(true)
                        {
                            if (!isFreeze) break;
                            fAttackDelayNow = 0.0f;
                            yield return null;
                        }
                    }
                    float angleangle = 0.0f;
                    if (modAngle != 0)
                    {
                        if (nModCount == 1) angleangle = 0.0f;
                        else
                        {
                            int v = nModCount / 2;
                            v *= modAngle;
                            angleangle = -v + (modAngle * (nModCount % 2 == 0 ? j + 1 : j));
                        }
                    }
                    float randomAngle = (nFireAngle == 0 ? 0 : Random.Range(-nFireAngle, nFireAngle));
                    float _angle = GetAngle(WeaponsFireTransform.position, mobData.ID.Contains("mush_green_cap") ? tPosition : targetObj.transform.position) + randomAngle + angleangle;

                    Vector3 firePos = WeaponsFireTransform.position;
                    Vector3 crossPos = GetPosition(WeaponsFireTransform.position, _angle);
                    firePos.z = crossPos.z = 0;
                    Vector3 dir = crossPos - firePos;
                    dir.Normalize();

                    GameObject obj = ObjManager.Call().GetObject("Bullet");
                    obj.GetComponent<BulletController>().isMonster = true;
                    obj.GetComponent<BulletController>().firePosition = transform.position;
                    obj.GetComponent<BulletController>().monsterName = tId;
                    obj.GetComponent<BulletController>().bulletType = (mobData.ID.Contains("beholder") ? "beholder_bullet_0" : "Monster_Bullet");
                    obj.GetComponent<BulletController>().fSpeed = bulletSpeed;
                    obj.GetComponent<BulletController>().fAngle = randomAngle + angleangle;
                    obj.GetComponent<BulletController>().fRange = fBulletRange;
                    obj.GetComponent<BulletController>().isPenetrate = false;
                    obj.GetComponent<BulletController>().isRightAttack = false;
                    obj.GetComponent<BulletController>().dir = dir;

                    if (mobData.ID.Contains("mush_red_cap"))
                    {
                        Vector3 dirrr = Vector3.zero;
                        if (j % 3 == 1)
                        {
                            Vector3 downPosition = GetPosition(WeaponsFireTransform.position, _angle - 90);
                            dirrr = downPosition - WeaponsFireTransform.position;
                            dirrr.Normalize();
                            obj.transform.position = WeaponsFireTransform.position + dirrr;
                        }
                        else if (j % 3 == 2)
                        {
                            Vector3 downPosition = GetPosition(WeaponsFireTransform.position, _angle + 90);
                            dirrr = downPosition - WeaponsFireTransform.position;
                            dirrr.Normalize();
                            obj.transform.position = WeaponsFireTransform.position + dirrr;
                        }
                        else
                        {
                            obj.transform.position = WeaponsFireTransform.position;
                        }
                    }
                    else  obj.transform.position = WeaponsFireTransform.position;

                    obj.SetActive(true);

                    if(mobData.ID.Contains("mush_green_cap"))
                        obj.GetComponent<BulletController>().BulletFire(true);
                    else
                        obj.GetComponent<BulletController>().BulletFire();
                }
                yield return new WaitForSeconds(fBulletDelay);
            }
            if (mobData.ID.Contains("mush"))
            {
                isMoveStop = false;
            }
        }
        IEnumerator ExclamationMark()
        {
            Vector3 pos = GetPosition(transform.position, 90);
            ExclamationObj.transform.position = pos;
            ExclamationObj.SetActive(true);
            yield return new WaitForSeconds(0.4f);
            ExclamationObj.SetActive(false);
        }
        #endregion
        #region -- Item Drop --
        public void ItemDrop()
        {
            if (isTutorial)
            {
                if (tutoName.Equals("stage2"))
                {
                    Dungeon.instance.TutorialCount++;
                }
                return;
            }
            List<string> itemList = new List<string>();
            //itemList.Add("gold_bar");
            float goldPlusPer = 0.0f;
            float skullPlusPer = 0.0f;
            if (Player.instance.isGold_Level_1) goldPlusPer += 0.5f;
            if (Player.instance.isGold_Level_3)
            {
                goldPlusPer += 0.5f;
                skullPlusPer += 0.5f;
            }
            goldPlusPer += Dungeon.instance.triggerKeyDropAmount;
            if (Random.Range(0, 100) < 20 + Mathf.FloorToInt(20 * Dungeon.instance.triggerSkullDropAmount))
            {
                itemList.Add("glowing_skull");
            }
            if (SaveAndLoadManager.nDifficulty != 0)
            {
                for (int i = 0; i < (SaveAndLoadManager.nDifficulty * 2) - 1; i++)
                {
                    if (Random.Range(0, 100) < 20 + Mathf.FloorToInt(20 * Dungeon.instance.triggerSkullDropAmount))
                    {
                        itemList.Add("glowing_skull");
                    }
                }
            }

            if (GetPercentege(skullPlusPer, new string[1]) != -1) itemList.Add("glowing_skull");
            int t = SaveAndLoadManager.instance.LoadItem("Boss_eyes_fire");
            int t2 = SaveAndLoadManager.instance.LoadItem("HellFogeeFlag");
            int aa = Random.Range(0, 100);
            if ((t == -1 && t2 != 1) && (aa >= 54 && aa < 64) && isUnique) itemList.Add("eyes_fire");

            Vector3 tPos = transform.position;
            tPos.y += 1.0f;
            for (int i = 0; i < itemList.Count; i++)
            {
                float angle = -90.0f;
                angle += (i * (360.0f / itemList.Count));
                Vector3 pos = GetPosition(tPos, angle);
                Vector3 dir = pos - tPos;
                dir.Normalize();
                GameObject obj = ObjManager.Call().GetObject("Item");
                if (obj != null)
                {
                    Item _item = obj.GetComponent<Item>();
                    _item._Item = new ItemInfo
                    {
                        Data = System.Array.Find(SheetManager.Instance.ItemsDB.dataArray, item => item.ID.Equals(itemList[i]))
                    };
                    _item.dir = dir;
                    _item.rPow = 0.5f;
                    _item.itemCount = 1;
                    obj.transform.position = pos + (dir * 0.5f);
                    obj.SetActive(true);
                }
            }
            if(SaveAndLoadManager.instance.LoadItem("Item_Guide") == -1)
            {
                StartCoroutine(ItemGuide("Item_Guide"));
            }
            PDG.Dungeon.DropMaterial((isUnique ? "unique" : mobData.Type), mobData.Type2, transform.position);
        }
        IEnumerator ItemGuide(string _id)
        {
            Inventorys.Instance.ItemGuideObj.transform.Find("Text").GetComponent<SetLanguage>().InitSetting(_id);
            yield return new WaitForSeconds(0.2f);
            Time.timeScale = 0;
            SoundManager.instance.StartAudio(new string[1] { "pressanykey" }, VOLUME_TYPE.EFFECT);
            Inventorys.Instance.ItemGuideObj.SetActive(true);
            Inventorys.Instance.isGuid = true;
        }
        public void ItemDropPiko()
        {
            if (isTutorial)
            {
                if (tutoName.Equals("stage2"))
                {
                    Dungeon.instance.TutorialCount++;
                }
                return;
            }

            UnlockListsData[] pikoUnlockDatas = System.Array.FindAll(SheetManager.Instance.UnlockListsDB.dataArray, item => item.NPCID.Equals("piko"));
            for (int i = 0; i < pikoUnlockDatas.Length; i++) 
            {
                if(SaveAndLoadManager.instance.LoadUnlock(pikoUnlockDatas[i].Itemid) < 1)
                {
                    SaveAndLoadManager.instance.SaveUnlock(pikoUnlockDatas[i].Itemid, 1);
                    ItemsData iData = System.Array.Find(SheetManager.Instance.ItemsDB.dataArray, item => item.ID.Equals(pikoUnlockDatas[i].Itemid));
                    Inventorys.Instance.ShowGetItemUI("monster_unlockitem", Resources.Load<Sprite>("Item/" + iData.Type1 + "/" + iData.Type2 + "/" + iData.ID));
                }
            }
            List<string> itemList = new List<string>();
            List<string> itemList2 = new List<string>();
            ItemsData[] datas = System.Array.FindAll(SheetManager.Instance.ItemsDB.dataArray, item => item.Type1.Equals("Weapon"));
            for (int i = 0; i < datas.Length; i++)
            {
                Sprite s = Resources.Load<Sprite>("Item/" + datas[i].Type1 + "/" + datas[i].Type2 + "/" + datas[i].ID);
                if (s == null) continue;
                if (datas[i].Grade.Equals("F")) continue;
                if (datas[i].Grade.Equals("G")) continue;
                if (Inventorys.Instance.UnlockAndMaxCountCheck(datas[i].ID))
                {
                    string info = datas[i].ID;
                    string tempID = info;
                    int ttt = tempID.LastIndexOf('_');
                    if (ttt != -1 && ttt == tempID.Length - 2)
                        tempID = tempID.Substring(0, tempID.Length - 2);
                    SaveAndLoadManager.instance.SaveItem(new ItemSave { _id = "EquipmentSave_" + tempID, _nCount = 1 });
                    itemList2.Add(datas[i].ID);
                }
            }

            //int goldCount = Random.Range(4, 10);
            //for (int i = 0; i < goldCount; i++)
            //    itemList.Add("gold_bar");

            int skullCount = Random.Range(4, 6);

            if (SaveAndLoadManager.nDifficulty != 0)
            {
                skullCount *= (SaveAndLoadManager.nDifficulty * 2) - 1;
            }

            if (itemList2.Count > 0)
            {
                for (int i = 0; i < skullCount; i++)
                    itemList.Add("glowing_skull");
                itemList.Add(itemList2[Random.Range(0, itemList2.Count)]);
            }
            else
            {
                skullCount += 20;
                for (int i = 0; i < skullCount; i++)
                    itemList.Add("glowing_skull");
            }


            for (int i = 0; i < itemList.Count; i++)
            {
                float angle = -90.0f;
                angle += (i * (360.0f / itemList.Count));
                Vector3 pos = GetPosition(transform.position, angle);
                Vector3 dir = pos - transform.position;
                dir.Normalize();
                GameObject obj = ObjManager.Call().GetObject("Item");
                if (obj != null)
                {
                    ItemsData iData = System.Array.Find(SheetManager.Instance.ItemsDB.dataArray, item => item.ID.Equals(itemList[i]));
                    if (iData == null) continue;
                    Item _item = obj.GetComponent<Item>();
                    _item._Item = new ItemInfo
                    {
                        Data = Inventorys.Instance.CopyItem(iData)
                    };
                    if (!_item._Item.Data.ID.Equals("glowing_skull"))
                    {
                        Inventorys.Instance.DiscardedItem.Add(_item._Item);
                        Inventorys.Instance.DiscardedItemString.Add(_item._Item.Data.ID);
                    }
                    _item.dir = (i == (itemList.Count - 1) ? Vector3.zero : dir);
                    _item.itemCount = 1;
                    _item.rPow = 1;
                    float startDis2 = 2.0f;
                    float angle2 = angle += 90.0f;
                    if (angle2 <= 180) startDis2 -= (angle2 / 150.0f);
                    else startDis2 -= ((180.0f / 150.0f) + ((180.0f - angle2) / 150.0f));

                    obj.transform.position = (i == (itemList.Count - 1) ? transform.position + new Vector3(-0.5f, 0) : pos + (dir * startDis2));
                    obj.SetActive(true);
                }
            }
            PDG.Dungeon.DropMaterial(mobData.Type, mobData.Type2, transform.position + new Vector3(0.5f, 0));
        }
        #endregion
        #region -- Piko --
        [Header("Piko")]
        List<GameObject> objList = new List<GameObject>();
        [SerializeField] private float pikoPatternTime = 3.0f;
        [SerializeField] private GameObject monsterShadow;
        [SerializeField] private Sprite _shadowSprite;
        [SerializeField] private Vector3 endPos;
        [SerializeField] private bool isShadowEnd;
        [SerializeField] private bool timeover = false;
        public bool isInvincibility = false;
        Coroutine pikoCoroutine = null;
        Coroutine pikoInvincibility = null;
        Coroutine shadow = null;
        [SerializeField] private float fireDelay = 1.0f;
        [SerializeField] private bool isPikoFire = false;
        public float startSpeed = 5.0f;
        private void PikoPattern()
        {
            if (pikoCoroutine == null) pikoCoroutine = StartCoroutine(PikoAction());
            if (pikoInvincibility == null) pikoInvincibility = StartCoroutine(pikoInvincibilityCoroutine());
        }
        private void PikoDieStart()
        {
            string tId = mobData.ID;
            if (tId.Contains("_t")) tId = tId.Substring(0, tId.Length - 3);
            SaveAndLoadManager.instance.SaveCollection(new CollectionData { _id = tId, _isOpen = true, _isWeapon = false });
            dieTime = 0.0f;
            ResultPanelScript.instance.eliteList.Add(LocalizeManager.GetLocalize("monster_" + tId));
            StartCoroutine(PikoDieEffect());
        }
        IEnumerator PikoDieEffect()
        {
            homingShoot.gameObject.SetActive(false);
            circleShoot.gameObject.SetActive(false);
            reversShot.gameObject.SetActive(false);
            spiralLeftShoot.gameObject.SetActive(false);
            randomAming.gameObject.SetActive(false);
            GameObject.Find("Canvas").transform.Find("TutorialText3").gameObject.SetActive(false);
            isDieEnd = true;
            for (int i = 0; i < objList.Count; i++)
            {
                if (objList[i] != null) Destroy(objList[i]);
            }
            GameObject.Find("Camera").GetComponent<PDG.Camera>().isTestScreenClear = true;
            simbolObj.SetActive(false);
            minimapObj.SetActive(false);
            PDG.Camera _camera = GameObject.Find("Camera").GetComponent<PDG.Camera>();
            Transform targetOrigin = _camera.target;
            _camera.target = transform;
            Player.instance.isCameraOff = true;
            Time.timeScale = 0.1f;
            yield return new WaitForSeconds(0.2f);
            Time.timeScale = 1;
            Vector3 originPos = transform.position;
            CameraShaker._instance.StartShake(0.1f, Time.deltaTime, 1.0f);
            for (int i = 0; i < 40; i++) 
            {
                if(i % 2 == 0) transform.position = originPos + new Vector3(0.1f, 0);
                else transform.position = originPos + new Vector3(-0.1f, 0);
                yield return new WaitForSeconds(Time.deltaTime);
            }
            animator.SetTrigger("DieTrigger");
            Player.instance.isCameraOff = false;
            _camera.target = targetOrigin;
            Image spake = GameObject.Find("Canvas").transform.Find("Spake").GetComponent<Image>();
            float a = 0.0f;
            while (true)
            {
                a += Time.fixedUnscaledDeltaTime * 5;
                spake.color = new Color(1, 1, 1, a);
                if (a > 1.0f) break;
                yield return new WaitForSecondsRealtime(Time.fixedUnscaledDeltaTime);
            }
            spake.color = new Color(1, 1, 1, 1);
            GameObject.Find("Camera").GetComponent<PDG.Camera>().isTestScreenClear = true;
            a = 1.0f;
            while (true)
            {
                a -= Time.fixedUnscaledDeltaTime * 5;
                spake.color = new Color(1, 1, 1, a);
                if (a <= 0.0f) break;
                yield return new WaitForSecondsRealtime(Time.fixedUnscaledDeltaTime);
            }
            spake.color = new Color(1, 1, 1, 0);
            SoundManager.instance.StartAudio(new string[1] { "BGM/Dungeon_" + Dungeon.instance.stageRan[Dungeon.instance.StageLevel] }, VOLUME_TYPE.BGM, null, true);
            StopAllCoroutines();
            GameObject.Find("Canvas").transform.Find("TutorialText3").gameObject.SetActive(false);
            ItemDropPiko();
        }
        IEnumerator pikoInvincibilityCoroutine()
        {
            float t = 5f;
            while(true)
            {
                if (!isInvincibility)
                {
                    t -= Time.deltaTime;
                    if (t <= 0.0f) 
                    {
                        t = 5f;
                        isInvincibility = true;
                        GameObject.Find("Canvas").transform.Find("TutorialText3").gameObject.SetActive(true);
                        GameObject.Find("Canvas").transform.Find("TutorialText3").GetComponent<Text>().text = LocalizeManager.GetLocalize("Barrier");
                        animator.SetBool("isInvincibility", true);
                        //if (SaveAndLoadManager.instance.LoadItem("Piko_Guide") == -1)
                        //{
                        //    StartCoroutine(ItemGuide("Piko_Guide"));
                        //}
                    }
                }
                if (shadow != null) endPos = monsterShadow.transform.position = targetObj.transform.position;
                yield return new WaitForSeconds(Time.deltaTime);
            }
            pikoInvincibility = null;
        }
        IEnumerator PikoAction()
        {
            BoxCollider2D coll = GetComponent<BoxCollider2D>();
            while(!isDie)
            {
                if (pikoPatternTime <= 0.0f)
                {
                    timeover = true;
                    pikoPatternTime = 4.0f;
                }
                if(fireDelay <= 0.0f)
                {
                    isPikoFire = true;
                    fireDelay = 1.0f;
                }
                if (isPikoFire)
                {
                    StartCoroutine(FirePiko());
                    isPikoFire = false;
                }
                else if (timeover)
                {
                    if (shadow == null && !isShadowEnd) shadow = StartCoroutine(ShadowAlpha());
                    if (isShadowEnd)
                    {
                        GameObject.Find("Canvas").transform.Find("TutorialText3").gameObject.SetActive(true);
                        GameObject.Find("Canvas").transform.Find("TutorialText3").GetComponent<Text>().text = LocalizeManager.GetLocalize("Barrier");
                        animator.SetBool("isInvincibility", true);
                        animator.SetBool("isJump", true);
                        animator.SetTrigger("jumpTrigger");
                        yield return new WaitForSeconds(0.5f);
                        shadow = null;
                        monsterShadow.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 0);
                        yield return StartCoroutine(JumpTempUp());
                        sr.enabled = false;
                        coll.enabled = false;
                        Vector3 tempPos = endPos;
                        tempPos.y += 1.0f;
                        transform.position = tempPos;
                        sr.enabled = true;
                        coll.enabled = true;
                        animator.SetBool("isJump", false);
                        yield return StartCoroutine(JumpTempDown());
                        animator.SetBool("isInvincibility", false);
                        GameObject.Find("Canvas").transform.Find("TutorialText3").gameObject.SetActive(false);
                        isInvincibility = false;
                        isShadowEnd = false;
                        yield return new WaitForSeconds(3.0f);
                        timeover = false;
                        isPathPossible = true;
                        isMoveChange = true;
                        isTargeting = true;
                    }
                }
                else
                {
                    pikoPatternTime -= Time.deltaTime;
                    fireDelay -= Time.deltaTime;
                    if (isPathPossible)
                    {
                        if (path != null)
                        {
                            if (!isHit)
                            {
                                if (Vector3.Distance(targetObj.transform.position, transform.position) < fAttackRange)
                                {
                                    pathIsEnabled = true;
                                    isMoveChange = true;
                                }

                                if (currentWaypoint >= path.vectorPath.Count)
                                {
                                    if (!pathIsEnabled)
                                    {
                                        pathIsEnabled = true;
                                        isMoveChange = true;
                                    }
                                }
                                else
                                {
                                    pathIsEnabled = false;
                                    dir = (path.vectorPath[currentWaypoint] - transform.position).normalized;
                                    Move(dir, speed, 1, false);
                                    float dist = Vector3.Distance(transform.position, path.vectorPath[currentWaypoint]);
                                    if (dist < nextWaypointDistance) currentWaypoint++;
                                }
                            }
                        }
                    }
                }
                yield return new WaitForSeconds(Time.deltaTime);
            }
            pikoCoroutine = null;
        }
        IEnumerator JumpTempUp()
        {
            Vector3 pos = transform.position;
            float y = 0.0f;
            while(true)
            {
                y += Time.deltaTime * 20;
                pos.y += Time.deltaTime * 150;
                transform.position = pos;
                if (y >= 5.0f) break;
                yield return new WaitForSeconds(Time.deltaTime);
            }

        }
        IEnumerator JumpTempDown()
        {
            Vector3 pos = endPos;
            pos.y += 32.5f;
            float y = 0.0f;
            while (true)
            {
                y += Time.deltaTime * 20;
                pos.y -= Time.deltaTime * 150;
                transform.position = pos;
                if (y >= 5.0f) break;
                yield return new WaitForSeconds(Time.deltaTime);
            }
            transform.position = endPos;
            CameraShaker._instance.StartShake(0.05f, 0.01f, 0.5f);

            GameObject obj2 = ObjManager.Call().GetObject("Hit_sprite5");
            Vector3 pos2 = endPos;
            pos2.y -= 0.5f;
            obj2.transform.position = pos2;
            obj2.transform.localScale = Vector3.one;
            obj2.GetComponent<SwordHitEffectController>()._effctName = "PikoJumpDown";
            obj2.GetComponent<SwordHitEffectController>().animSpeed = 1.0f;
            obj2.GetComponent<Animator>().Rebind();
            obj2.SetActive(true);

            _audio2.volume =  (SoundManager.isMute || (PDG.Dungeon.instance == null ? false : PDG.Dungeon.instance.isPause) || (PDG.Player.instance == null ? false : PDG.Player.instance.isDie)) ? 0 : SoundManager.EffectVolume;
            _audio2.PlayOneShot(pikoDown);

            GameObject obj3 = ObjManager.Call().GetObject("Hit_sprite12");
            Vector3 pos3 = endPos;
            obj3.transform.position = pos3;
            obj3.transform.localScale = Vector3.one;
            obj3.GetComponent<SwordHitEffectController>()._effctName = "PikoJumpDown2";
            obj3.GetComponent<SwordHitEffectController>().animSpeed = 0.2f;
            obj3.GetComponent<Animator>().Rebind();
            obj3.SetActive(true);
        }
        IEnumerator ShadowAlpha()
        {
            GameObject obj3 = ObjManager.Call().GetObject("Hit_sprite4_1");
            obj3.transform.position = targetObj.transform.position;
            obj3.transform.localScale = Vector3.one;
            obj3.transform.localEulerAngles = Vector2.zero;
            obj3.GetComponent<SwordHitEffectController>()._effctName = "piko_shadow";
            obj3.GetComponent<SwordHitEffectController>().animSpeed = 0.15f;
            obj3.GetComponent<SwordHitEffectController>().isPenetrate = true;
            obj3.GetComponent<SwordHitEffectController>().isAttack = false;
            obj3.GetComponent<SwordHitEffectController>().nRange = 5;
            obj3.GetComponent<Animator>().Rebind();
            obj3.SetActive(true);
            obj3.GetComponent<SwordHitEffectController>().isPlayerTarget = true;
            if (!isDie)
            {
                endPos = targetObj.transform.position;
                monsterShadow.GetComponent<SpriteRenderer>().sprite = _shadowSprite;
                for (float i = 0.0f; i < 0.5f; i += Time.deltaTime)
                {
                    monsterShadow.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, i);
                    yield return new WaitForSeconds(Time.deltaTime);
                }
                yield return new WaitForSeconds(0.5f);
                isShadowEnd = true;
                yield return new WaitForSeconds(0.3f);
                obj3.SetActive(false);
            }
        }
        IEnumerator FirePiko()
        {
            string tId = mobData.ID;
            if (tId.Contains("_t")) tId = tId.Substring(0, tId.Length - 3);

            Vector3[] dirs;
            List<GameObject> bulletList = new List<GameObject>();
            _audio.volume =  (SoundManager.isMute || (PDG.Dungeon.instance == null ? false : PDG.Dungeon.instance.isPause) || (PDG.Player.instance == null ? false : PDG.Player.instance.isDie)) ? 0 : SoundManager.EffectVolume;
            _audio.Play();
            Vector3 nPos = transform.position;
            for (int i = 0; i < 8; i++)
            {
                float angle = (360.0f / 8.0f) * i;

                Vector3 pos = GetPosition(nPos, angle);
                Vector3 dir = pos - nPos;
                dir.Normalize();
                GameObject obj = ObjManager.Call().GetObject("Bullet");
                obj.GetComponent<BulletController>().firePosition = transform.position;
                obj.GetComponent<BulletController>().isMonster = true;
                obj.GetComponent<BulletController>().monsterName = tId;
                obj.GetComponent<BulletController>().bulletType = "Monster_Bullet";
                obj.GetComponent<BulletController>().fSpeed = mobData.Bulletspeed;
                obj.GetComponent<BulletController>().fAngle = 0;
                obj.GetComponent<BulletController>().dir = Vector2.zero;
                obj.GetComponent<BulletController>().fRange = 20;
                obj.GetComponent<BulletController>().isRightAttack = false;
                obj.transform.position = pos;
                obj.transform.localEulerAngles = Vector3.zero;
                obj.SetActive(true);
                obj.GetComponent<BulletController>().BulletSetting();
                bulletList.Add(obj);
                yield return new WaitForSeconds(0.05f);
            }
            dirs = new Vector3[bulletList.Count];
            for (int i = 0; i < bulletList.Count; i++)
            {
                float angle = (360.0f / 8.0f) * i;
                Vector3 pos = GetPosition(nPos, angle);
                Vector3 dir = pos - nPos;
                dir.Normalize();
                dirs[i] = dir;
            }
            float t = 0.0f;
            while (true)
            {
                for (int i = 0; i < bulletList.Count; i++)
                {
                    bulletList[i].transform.Translate(dirs[i] * startSpeed * Time.deltaTime);
                }
                t += Time.deltaTime;
                if (t >= 0.2f) break;
                yield return new WaitForSeconds(Time.deltaTime);
            }
            yield return new WaitForSeconds(1.0f);
            Vector3 dir2 = PDG.Player.instance.transform.position - nPos;
            dir2.Normalize();
            for (int i = 0; i < bulletList.Count; i++)
            {
                if (!bulletList[i].activeSelf) continue;
                bulletList[i].GetComponent<BulletController>().dir = dir2;
                bulletList[i].GetComponent<BulletController>().BulletFire();
            }
        }
        #endregion
        #region -- 그랩 당함 --
        public void Grab(Transform _transform)
        {
            if (isNoneInfo) return;
            if (isDie) return;
            if (isBoss) return;
            if (isGoblin) return;
            if (isTutorial && tutoType != 1) return;
            if (isTutorial && tutoType == 1) isTutoGrab = true;
            if (isUnique && !Player.instance.nowRightWeaponName.Equals("catching_tentacle_1")) return;
            if (isMiMicWait && isMimic) return;
            isGrab = true;
            tGrab = _transform;
            if (WeaponsSlot) WeaponsSlot.SetActive(false);
            isWeaponMode = false;
            bCol.enabled = false;

            var target = GetComponent<Target>();
            if (target != null)
            {
                target.HandleHit(1, Vector2.zero);
            }
            homingShoot.StopShotRoutine();
            randomAming.gameObject.SetActive(false);
            randomAming.StopShotRoutine();
            circleShoot.StopShotRoutine();
            reversShot.StopShotRoutine();
            if (mobData.ID.Contains("slime_fire")) animator.SetTrigger("GrabTrigger");
            int totalCount = SaveAndLoadManager.instance.LoadAchievements("grabmaster_1")._cur;
            AchievementsData aData_0 = System.Array.Find(SheetManager.Instance.AchievementsDB.dataArray, item => item.ID.Equals("grabmaster_0"));
            AchievementsData aData_1 = System.Array.Find(SheetManager.Instance.AchievementsDB.dataArray, item => item.ID.Equals("grabmaster_1"));
            SaveAndLoadManager.instance.SaveAchievements(new AchievementData[2] {
                      new AchievementData { _id = aData_0.ID, _cur = totalCount + 1, _max = aData_0.MAX, _clear = false },
                      new AchievementData { _id = aData_1.ID, _cur = totalCount + 1, _max = aData_1.MAX, _clear = false }
                    });
        }
        public void UnGrab()
        {
            isGrab = false;
            isPull = true;
            randomAming.StopShotRoutine();
            randomAming.gameObject.SetActive(true);
        }
        public void UnGrab2()
        {
            if (isDie) return;
            isGrab = false;
            if (isFire) WeaponsSlot.SetActive(true);
            else WeaponsSlot.SetActive(false);
            isWeaponMode = true;
            bCol.enabled = true;
            if (!isTutorial && (!mobData.ID.Contains("skull_green") || mobData.ID.Contains("hand")) && !mobData.ID.Contains("woodlouse")) isTargeting = true;

            randomAming.StopShotRoutine();
            randomAming.gameObject.SetActive(true);
            isMoveChange = true;

            if (isTutoGrab)
            {
                CameraShaker._instance.StartShake(0.1f, Time.deltaTime, 0.1f);
                Dialogue.Instance.CommnetSetting("tutorial_ungrab_guide", 0, null);
                isDie = true;
                DieStart(false);
                bCol.enabled = false;
                GameObject obj = ObjManager.Call().GetObject("MonsterDeath");
                obj.transform.position = transform.position;
                obj.GetComponent<Animator>().Rebind();
                obj.SetActive(true);
            }
        }
        #endregion
        #region -- BrightSlime --
        Coroutine brightSlimePatternCoroutine = null;
        [SerializeField] private UbhShotCtrl spiralLeftShoot;
        [SerializeField] private GameObject BrightSlimeAttackEffect;
        private void BrightSlimePattern()
        {
            if (brightSlimePatternCoroutine == null)
                brightSlimePatternCoroutine = StartCoroutine(BrightSlimePatternCoroutine());
        }
        IEnumerator BrightSlimePatternCoroutine()
        {
            fAttackDelay = mobData.Shootdelay;
            float rangeAttackTime = 10.0f;
            float rangeAttackTimeNow = 0.0f;
            float longRangeAttackTime = 3.0f;
            float longRangeAttackTimeNow = 0.0f;
            while (!isDie)
            {
                if (isTargeting) rangeAttackTimeNow += Time.deltaTime;
                if (isTargeting) longRangeAttackTimeNow += Time.deltaTime;

                if (rangeAttackTimeNow >= rangeAttackTime)
                {
                    rangeAttackTimeNow = 0.0f;
                    if (isDie) break;
                    spiralLeftShoot.StartShotRoutine();
                }
                else if (isTargeting)
                {
                    float dis = Vector3.Distance(Player.instance.transform.position, transform.position);
                    if (dis < 4 && !isAttack)
                    {
                        RaycastHit2D hit;
                        hit = Physics2D.CircleCast(transform.position, 4, Vector2.zero, 0, 1 << 9 | 1 << 30);
                        if (hit && !isAttack)
                        {
                            if (isDie) break;
                            animator.SetTrigger("AttackMelee");
                            float angle = GetAngle(transform.position, hit.transform.position);
                            while (true)
                            {
                                if (angle < 0) angle += 360.0f;
                                if (angle > 360.0f) angle -= 360.0f;
                                if (angle >= 0 && angle <= 360) break;
                            }
                            if (angle <= 22.5f || angle > 337.5f)
                            {
                                animator.SetInteger("Direction", 0);
                            }
                            if (angle > 22.5f && angle <= 67.5f)
                            {
                                animator.SetInteger("Direction", 4);
                            }
                            if (angle > 67.5f && angle <= 112.5f)
                            {
                                animator.SetInteger("Direction", 1);
                            }
                            if (angle > 112.5f && angle <= 157.5f)
                            {
                                animator.SetInteger("Direction", 4);
                            }
                            if (angle > 157.5f && angle <= 202.5f)
                            {
                                animator.SetInteger("Direction", 0);
                            }
                            if (angle > 202.5f && angle <= 247.5f)
                            {
                                animator.SetInteger("Direction", 3);
                            }
                            if (angle > 247.5f && angle <= 292.5f)
                            {
                                animator.SetInteger("Direction", 2);
                            }
                            if (angle > 292.5f && angle <= 337.5f)
                            {
                                animator.SetInteger("Direction", 3);
                            }
                            isAttack = true;
                        }
                    }
                    //else if (dis < fAttackRange && fAttackDelayNow > fAttackDelay && !isAttack)
                    //{
                    //    isLongAttack = false;
                    //    fAttackDelayNow = 0.0f;
                    //    if (isDie) break;
                    //    isAttack = true;
                    //    animator.SetTrigger("AttackRange");
                    //}
                    else if (longRangeAttackTimeNow > longRangeAttackTime && !isAttack)
                    {
                        longRangeAttackTime = Random.Range(2.0f, 5.0f);
                        longRangeAttackTimeNow = 0.0f;
                        if (isDie) break;
                        isAttack = true;
                        animator.SetTrigger("AttackRange");
                    }
                    else
                    {
                        if (isPathPossible && !isMoveStop)
                        {
                            if (path != null)
                            {
                                if (currentWaypoint >= path.vectorPath.Count)
                                {
                                    if (!pathIsEnabled)
                                    {
                                        pathIsEnabled = true;
                                        isMoveChange = true;
                                    }
                                }
                                else
                                {
                                    if (isDie) break;
                                    pathIsEnabled = false;
                                    dir = (path.vectorPath[currentWaypoint] - transform.position).normalized;
                                    Move(dir, speed, 1, false);
                                    float dist = Vector3.Distance(transform.position, path.vectorPath[currentWaypoint]);
                                    if (dist < nextWaypointDistance)
                                    {
                                        currentWaypoint++;
                                        GameObject obj2 = Instantiate(obj3);
                                        Vector3 pos = transform.position;
                                        pos.y -= 0.5f;
                                        obj2.GetComponent<DrumBomb>().InitSetting(pos);
                                    }
                                }
                            }
                        }
                    }
                }
                yield return new WaitForSeconds(Time.deltaTime);
            }
            brightSlimePatternCoroutine = null;
            spiralLeftShoot.StopShotRoutine();
        }
        public void BrightSlimeAttackEnd()
        {
            if (isDie) return;

            string tId = mobData.ID;
            if (tId.Contains("_t")) tId = tId.Substring(0, tId.Length - 3);

            isMoveStop = false;
            RaycastHit2D hit =  Physics2D.CircleCast(transform.position, 4, Vector2.zero, 0, 1 << 9 | 1 << 30);
            if (hit)
            {
                if (hit.collider.GetComponent<Player>())
                {
                    hit.collider.GetComponent<Player>().SetDamage(1, LocalizeManager.GetLocalize("monster_" + tId), DEATH_TYPE.DEATH_SUB_BOSS, tId);
                    if(BrightSlimeAttackEffect)
                    {
                        GameObject obj = Instantiate(BrightSlimeAttackEffect);
                        obj.transform.position = hit.point;
                        Destroy(obj, 2f);
                    }
                }
            }
            StartCoroutine(AttackTimeAfter());
        }
        IEnumerator AttackTimeAfter()
        {
            yield return new WaitForSeconds(0.5f);
            isAttack = false;
        }
        public void BrightSlimeRangeAttack()
        {
            string tId = mobData.ID;
            if (tId.Contains("_t")) tId = tId.Substring(0, tId.Length - 3);

            if (Random.Range(1, 10) % 3 == 0)
            {
                StartCoroutine(BrightSlimeLongRangeAttack());
            }
            else
            {
                GameObject shootObj = Instantiate(Resources.Load<GameObject>("Mob/" + tId + "/" + tId + "_bomb"));
                shootObj.GetComponent<White_ant_bomb>().Throw(transform.position, PDG.Player.instance.transform.position, false, tId);
                isAttack = false;
            }
        }
        IEnumerator BrightSlimeLongRangeAttack()
        {
            string tId = mobData.ID;
            if (tId.Contains("_t")) tId = tId.Substring(0, tId.Length - 3);

            int randomCount = Random.Range(12, 15);
            for (int i = 0; i < randomCount; i++)
            {
                Vector3 endPos = Player.instance.transform.position;
                endPos += new Vector3(Random.Range(-10, 10), Random.Range(-10, 10));
                GameObject shootObj = Instantiate(Resources.Load<GameObject>("Mob/" + tId + "/" + tId + "_bomb"));
                shootObj.GetComponent<White_ant_bomb>().Throw(transform.position, endPos, false, tId);
                if (isDie) break;
                yield return new WaitForSeconds(0.1f);
            }
            isAttack = false;
        }
        #endregion
        #region -- Jump Slime -- 
        public void JumpDownStart()
        {
            isMoveStop = true;
            if (isTargeting && nJumpCount == 0)
            {
                circleShoot.m_shotList[0].m_shotObj.m_bulletNum = 10;
                circleShoot.m_shotList[0].m_shotObj.m_bulletSpeed = 2;
                if (isGrab || isDie || isPull || isFreeze || isUsing) return;
                circleShoot.StartShotRoutine();
            }
        }
        public void JumpDownEnd()
        {
            isMoveStop = false;
        }
        public void JumpUpStart()
        {
            if (isGrab || isDie || isPull || isFreeze || isUsing) return;
            isMoveStop = false;
            nJumpCount++;
            if(nJumpCount == nJumpCountMax && !isFreeze)
            {
                nJumpCount = 0;
                isMoveStop = true;
                animator.SetBool("isJumping", true);
                StartCoroutine(Jumping());
            }

        }
        IEnumerator Jumping()
        {
            isJump = true;
            if (path != null && !isGrab && !isDie && !isPull && !isFreeze && !isUsing)
            {
                currentWaypoint += 3;
                if (currentWaypoint >= path.vectorPath.Count) currentWaypoint = path.vectorPath.Count - 1;
                Vector3 startPos = transform.position;
                Vector3 endPos = path.vectorPath[currentWaypoint];

                Vector3 differencePos = endPos - startPos;
                Vector3 center1 = differencePos / 4;
                Vector3 center2 = center1 * 3;
                center1 += startPos;
                center2 += startPos;

                Vector3 temp1 = GetPosition(center1, 90);
                Vector3 temp1_1 = temp1 - center1;
                temp1_1.Normalize();
                center1 = center1 + (temp1_1 * 5);

                Vector3 temp2 = GetPosition(center2, 90);
                Vector3 temp2_1 = temp2 - center2;
                temp2_1.Normalize();
                center2 = center1 + (temp2_1 * 5);
                GetComponent<BoxCollider2D>().enabled = false;
                float t = 0.0f;
                JumpShadowObj.SetActive(true);
                JumpShadowObj.transform.position = Vector3.Lerp(startPos, endPos, t);
                while (!isDie) 
                {
                    Vector3 lerp1_0 = Vector3.Lerp(startPos, center1, t);
                    Vector3 lerp1_1 = Vector3.Lerp(center1, center2, t);
                    Vector3 lerp1_2 = Vector3.Lerp(center2, endPos, t);

                    Vector3 lerp2_0 = Vector3.Lerp(lerp1_0, lerp1_1, t);
                    Vector3 lerp2_1 = Vector3.Lerp(lerp1_1, lerp1_2, t);

                    Vector3 lerp = Vector3.Lerp(lerp2_0, lerp2_1, t);
                    transform.position = lerp;
                    JumpShadowObj.transform.position = Vector3.Lerp(startPos, endPos, t);
                    t += Time.deltaTime;
                    if (t >= 1.0f) break;
                    if (isGrab || isDie || isPull || isFreeze || isUsing) break;
                    yield return new WaitForSeconds(Time.deltaTime);
                }
                GetComponent<BoxCollider2D>().enabled = true;
                JumpShadowObj.transform.position = Vector3.Lerp(startPos, endPos, 1);
                JumpShadowObj.SetActive(false);
            }
            isJump = false;
            isMoveStop = false;
            pathIsEnabled = true;
            isMoveChange = true;
            if (!isDie) animator.SetBool("isJumping", false);
        }
        #endregion
        #region -- ICE GOLEM --
        [SerializeField] private GameObject snowballObj;
        [SerializeField] private GameObject magiccircleObj;
        [SerializeField] private GameObject bllizardObj;
        [SerializeField] private UbhShotCtrl reversShot;
        [SerializeField] private GameObject golemiceJumpEffect;
        bool isIntro = false;
        Coroutine golemPatternCoroutine = null;
        private void GolemIcePattern()
        {
            if (golemPatternCoroutine == null) golemPatternCoroutine = StartCoroutine(GolemPatternCoroutine());
        }
        IEnumerator GolemPatternCoroutine()
        {
            isIntro = true;
            isInvincibility = true;
            yield return new WaitForSeconds(0.6f);
            animator.SetTrigger("AttackPunch");
            yield return new WaitForSeconds(0.6f);
            animator.SetTrigger("AttackPunch");
            yield return new WaitForSeconds(0.6f);
            isIntro = false;
            isInvincibility = false;
            float attackpunchTime = 2.5f;
            while(!isDie)
            {
                attackpunchTime -= Time.deltaTime;
                if (attackpunchTime <= 0.0f )
                {
                    if (animator.GetBool("isMove")) animator.SetBool("Move", false);
                    attackpunchTime = Random.Range(1.5f, 3.0f);
                    animator.SetTrigger("AttackPunch");
                    isAttack = true;
                }
                if (Vector3.Distance(transform.position, PDG.Player.instance.transform.position) > 7 && !isAttack && !isMoveStop)
                {
                    if (animator.GetBool("isMove")) animator.SetBool("Move", false);
                    isAttack = true;
                    animator.SetTrigger("Jump");
                    StartCoroutine(GolemJump());
                }
                else if (isPathPossible && !isAttack && !isMoveStop) 
                {
                    if (path != null)
                    {
                        if (Vector3.Distance(targetObj.transform.position, transform.position) < fAttackRange)
                        {
                            pathIsEnabled = true;
                            isMoveChange = true;
                        }

                        if (currentWaypoint >= path.vectorPath.Count)
                        {
                            if (!pathIsEnabled)
                            {
                                pathIsEnabled = true;
                                isMoveChange = true;
                            }
                        }
                        else
                        {
                            if (!animator.GetBool("isMove")) animator.SetBool("Move", true);
                            pathIsEnabled = false;
                            dir = (path.vectorPath[currentWaypoint] - transform.position).normalized;
                            Move(dir, speed, 1, false);
                            float dist = Vector3.Distance(transform.position, path.vectorPath[currentWaypoint]);
                            if (dist < nextWaypointDistance) currentWaypoint++;
                        }
                    }
                }
                yield return new WaitForSeconds(Time.deltaTime);
            }
            golemPatternCoroutine = null;
        }
        public void PunchAttackEnd()
        {
            SoundManager.instance.StartAudio(new string[1] { "Monster/golemPunch" }, VOLUME_TYPE.EFFECT);
            isAttack = false;
            if (isIntro) return;
            if (Random.Range(0, 100) < 60)
            {
                StartCoroutine(SnowBallFire());
            }
            else if (Random.Range(0, 100) < 60)
            {
                float f = GetAngle(transform.position, Player.instance.transform.position);
                reversShot.m_shotList[0].m_shotObj.GetComponent<UbhNwayShot>().m_centerAngle = f - 90.0f;
                reversShot.StartShotRoutine();
            }
            else
            {
                StartCoroutine(MagicCircle());
            }
        }
        IEnumerator SnowBallFire()
        {
            isMoveStop = true;
            int ran = Random.Range(1, 5);
            for (int i = 0; i < ran; i++)
            {
                if (isDie) break;
                Vector3 pPos = PDG.Player.instance.transform.position;
                float f = GetAngle(transform.position, pPos);
                f += Random.Range(-mobData.Aim, mobData.Aim);
                Vector3 pos = GetPosition(transform.position, f);
                Vector3 _dir = pos - transform.position;
                _dir.Normalize();
                GameObject obj = Instantiate(snowballObj, null);
                obj.transform.position = transform.position;
                obj.GetComponent<Rigidbody2D>().AddForce(_dir * 20, ForceMode2D.Impulse);
                yield return new WaitForSeconds(0.25f);
            }
            isMoveStop = false;
        }
        IEnumerator GolemJump()
        {
            Vector3 startPos = transform.position;
            Vector3 endPos = PDG.Player.instance.transform.position;

            Vector3 differencePos = endPos - startPos;
            Vector3 center1 = differencePos / 4;
            Vector3 center2 = center1 * 3;
            center1 += startPos;
            center2 += startPos;

            Vector3 temp1 = GetPosition(center1, 90);
            Vector3 temp1_1 = temp1 - center1;
            temp1_1.Normalize();
            center1 = center1 + (temp1_1 * 5);

            Vector3 temp2 = GetPosition(center2, 90);
            Vector3 temp2_1 = temp2 - center2;
            temp2_1.Normalize();
            center2 = center1 + (temp2_1 * 5);

            float t = 0.0f;
            JumpShadowObj.SetActive(true);
            JumpShadowObj.transform.position = Vector3.Lerp(startPos, endPos, t);
            GetComponent<BoxCollider2D>().enabled = false;
            while (!isDie)
            {
                Vector3 lerp1_0 = Vector3.Lerp(startPos, center1, t);
                Vector3 lerp1_1 = Vector3.Lerp(center1, center2, t);
                Vector3 lerp1_2 = Vector3.Lerp(center2, endPos, t);

                Vector3 lerp2_0 = Vector3.Lerp(lerp1_0, lerp1_1, t);
                Vector3 lerp2_1 = Vector3.Lerp(lerp1_1, lerp1_2, t);

                Vector3 lerp = Vector3.Lerp(lerp2_0, lerp2_1, t);
                transform.position = lerp;
                JumpShadowObj.transform.position = Vector3.Lerp(startPos, endPos, t);
                t += Time.deltaTime;
                if (t >= 1.0f)
                {
                    lerp = Vector3.Lerp(lerp2_0, lerp2_1, 1);
                    transform.position = lerp;
                    JumpShadowObj.transform.position = Vector3.Lerp(startPos, endPos, 1);
                    break;
                }
                yield return new WaitForSeconds(Time.deltaTime);
            }
            if (golemiceJumpEffect)
            {
                GameObject obj22 = Instantiate(golemiceJumpEffect);
                obj22.transform.position = transform.position;
                Destroy(obj22, 2f);
            }
            GetComponent<BoxCollider2D>().enabled = true;
            JumpShadowObj.SetActive(false);
            if (!isDie)
            {
                animator.SetTrigger("JumpEnd");
                CameraShaker._instance.StartShake(0.1f, Time.deltaTime, 0.1f, true);
                circleShoot.m_shotList[0].m_shotObj.m_bulletNum = 50;
                circleShoot.m_shotList[0].m_shotObj.m_bulletSpeed = 4;
                circleShoot.StartShotRoutine();
                isMoveStop = false;
                pathIsEnabled = true;
                isMoveChange = true;
            }
        }
        public void GolemJumpDown()
        {
        }
        IEnumerator MagicCircle()
        {
            Vector3 pPos = Player.instance.transform.position;
            GameObject obj = Instantiate(magiccircleObj, null);
            float f = 0.0f;
            while (!isDie)
            {
                f += Time.deltaTime;
                pPos = Player.instance.transform.position;
                obj.transform.position = pPos;
                if (f >= 1.0f) break;
                yield return new WaitForSeconds(Time.deltaTime);
            }
            int c = Random.Range(15, 20);
            for (int i = 0; i < c; i++)
            {
                if (isDie) break;
                int c2 = Random.Range(1, 5);
                for (int j = 0; j < c2; j++)
                {
                    if (isDie) break;
                    GameObject bObj = Instantiate(bllizardObj, null);
                    pPos.y += 5.0f;
                    pPos.x += Random.Range(-0.5f, 0.5f);
                    bObj.transform.position = pPos;
                    bObj.GetComponent<BlizzardController>().y = obj.transform.position.y;
                    bObj.GetComponent<Rigidbody2D>().AddForce(Vector3.down * 15, ForceMode2D.Impulse);
                    objList.Add(bObj);
                    if (isDie) break;
                }
                if (isDie) break;
                yield return new WaitForSeconds(Random.Range(0.05f, 0.1f));
            }
            Destroy(obj);
        }
        #endregion
        #region -- Radish --
        Coroutine RadishPatternCoroutine = null;
        private void RadishPattern()
        {

        }
        IEnumerator RadishPatternActive()
        {
            yield return null;
        }
        #endregion
        #endregion
    }
}