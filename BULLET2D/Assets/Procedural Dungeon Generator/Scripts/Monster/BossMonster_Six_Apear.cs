﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace MOB
{
    [RequireComponent(typeof(MonsterInfo))]
    public class BossMonster_Six_Apear : MonoBehaviour
    {
        #region -- Variable --
        [Header("Setting")]
        string _spritePath;
        Sprite[] _sprites;
        public GameObject simbolObj;
        private Animator animator;
        private SpriteRenderer sr;
        private MonsterInfo mobInfo;
        private BoxCollider2D bCol;
        private Rigidbody2D rigid;
        public MonstersData mobData;
        public bool isDie = false;
        public bool isTargeting = false;
        public bool isHit = false;
        public bool isInvincibility;
        public bool isAttack = false;
        public bool isStart = false;
        public Direction direction = Direction.None;

        [SerializeField] private GameObject targetObj;
        [SerializeField] private GameObject[] shotList = null;
        [SerializeField] private GameObject monsterShadow;

        [Header("Pattern")]
        [SerializeField] private bool isShadowEnd = false;
        [SerializeField] private bool timeover = false;
        [SerializeField] private float fireTime = 25.0f;
        [SerializeField] private float fireTimeNow = 0.0f;
        [SerializeField] private bool isFogPlayingEnd = false;
        [SerializeField] private bool isFogEnd = false;
        [SerializeField] private Vector3 endPos;
        public float moveSpeed = 50.0f;
        public GameObject fogParticleObj;
        Coroutine shadow = null;
        public ParticleSystem[] fogParticles;

        public float radius = 25;
        [SerializeField] private Vector3 dropPosition;
        AudioSource _audio;
        [Header("Die")]
        public GameObject[] DieObjs;
        public float diePow = 1.5f;
        public GameObject holeObj;

        float hitTime = 0.0f;

        [SerializeField] private AudioClip dieClip;
        [SerializeField] private AudioClip hitClip;
        [SerializeField] private AudioClip hitClip2;
        [SerializeField] private AudioClip bossBomb;
        [SerializeField] private AudioClip bossShaker;
        [SerializeField] private AudioClip shootClip1;
        [SerializeField] private AudioClip shootClip2;
        [SerializeField] private AudioClip shootClip3;

        [Header("Color")]
        public Color HitColor = new Color();
        public Color HitColor2 = new Color();
        public Color HitColor3 = new Color();
        public Color HitColor4 = new Color();
        private Coroutine HitCoroutine = null;

        #endregion
        private void Awake()
        {
            animator = GetComponent<Animator>();
            sr = GetComponent<SpriteRenderer>();
            mobInfo = GetComponent<MonsterInfo>();
            bCol = GetComponent<BoxCollider2D>();
            rigid = GetComponent<Rigidbody2D>();
            _audio = GetComponent<AudioSource>();
        }
        public void InitSetting(MonstersData _mobData)
        {
            mobData = _mobData;

            isDie = false;
            if (animator != null) animator.enabled = true;
            isTargeting = false;
            rigid.bodyType = RigidbodyType2D.Dynamic;
            rigid.simulated = true;
            mobInfo.curHp = mobInfo.maxHp;
            isHit = false;
            isAttack = false;
            direction = Direction.Left;
            sr.color = Color.white;
            sr.sortingOrder = 1;
            simbolObj.SetActive(true);
            Load();
        }
        void Load()
        {
            string tId = mobData.ID;
            if (tId.Contains("_t")) tId = tId.Substring(0, tId.Length - 3);
            var spritePath = "Mob/" + tId + "/" + animator.runtimeAnimatorController.name;
            if (!spritePath.Equals(_spritePath))
            {
                _spritePath = spritePath;
                _sprites = Resources.LoadAll<Sprite>(_spritePath);
            }
        }
        void LateUpdate()
        {
            if (sr == null || sr.sprite == null)
                return;
            Load();
            var name = sr.sprite.name;
            var sprite = System.Array.Find(_sprites, item => item.name == name);
            if (sprite) sr.sprite = sprite;
        }
        // Start is called before the first frame update
        void Start()
        {
            string tId = mobData.ID;
            if (tId.Contains("_t")) tId = tId.Substring(0, tId.Length - 3);
            _audio.clip = Resources.Load<AudioClip>("Sound/Monster/" + tId);
        }
        // Update is called once per frame
        void Update()
        {
            int mask = 1 << 9;
            RaycastHit2D hit = Physics2D.CircleCast(transform.position, radius, Vector2.zero, 1, mask);
            if (hit && !isStart)
            {
                isStart = true;
                targetObj = hit.collider.gameObject;
                StartCoroutine(DelayStart());
            }

            if(isDie)
            {
                if (shotList[0].activeSelf) shotList[0].SetActive(false);
                if (shotList[1].activeSelf) shotList[1].SetActive(false);
                if (shotList[2].activeSelf) shotList[2].SetActive(false);
            }

            if(isHit)
            {
                hitTime -= Time.deltaTime;
                if (hitTime <= 0.0f) isHit = false;
            }
        }
        IEnumerator DelayStart()
        {
            yield return new WaitForSeconds(1.0f);

            string tId = mobData.ID;
            if (tId.Contains("_t")) tId = tId.Substring(0, tId.Length - 3);
            Conversation.Instance.StartEffect(transform, LocalizeManager.GetLocalize("monster_" + tId), LocalizeManager.GetLocalize("monster_sub_" + tId), true, true, gameObject);
        }
        private void OnDrawGizmos()
        {
            Gizmos.color = Color.red;
            Gizmos.DrawWireSphere(transform.position, radius);
        }
        public void StartApear()
        {
            animator.SetTrigger("Apear");
        }
        public void BossStart()
        {
            StartCoroutine(TT());
            string tId = mobData.ID;
            if (tId.Contains("_t")) tId = tId.Substring(0, tId.Length - 3);
            BossHpController.instance.BossStart(LocalizeManager.GetLocalize("monster_" + tId), GetComponent<MOB.MonsterInfo>());
            StartCoroutine(Pattern());
        }
        IEnumerator TT()
        {
            yield return new WaitForSeconds(0.5f);
            isInvincibility = false; 
        }
        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.gameObject.layer.Equals(9))
            {
                string tId = mobData.ID;
                if (tId.Contains("_t")) tId = tId.Substring(0, tId.Length - 3);
                collision.gameObject.GetComponent<PDG.Player>().SetDamage(1, LocalizeManager.GetLocalize("monster_" + tId), DEATH_TYPE.DEATH_BOSS, tId);
            }
        }
        IEnumerator Pattern()
        {
            StartCoroutine(FogTimer());
            while (!isDie)
            {
                if(!timeover)
                {
                    if (shadow == null)
                    {
                        if (shotList[0].activeSelf) shotList[0].SetActive(false);
                        if (!shotList[1].activeSelf) shotList[1].SetActive(true);
                        if (shotList[2].activeSelf) shotList[2].SetActive(false);
                        _audio.volume =  (SoundManager.isMute || (PDG.Dungeon.instance == null ? false : PDG.Dungeon.instance.isPause) || (PDG.Player.instance == null ? false : PDG.Player.instance.isDie)) ? 0 : SoundManager.EffectVolume;
                        _audio.PlayOneShot(shootClip2);
                        shotList[1].GetComponent<UbhShotCtrl>()._mobName = mobData.Name;
                        shotList[1].GetComponent<UbhShotCtrl>()._bulletName = "Monster_Bullet";
                        shotList[1].GetComponent<UbhShotCtrl>().StartShotRoutine();
                        shadow = StartCoroutine(ShadowAlpha());
                    }
                    if (isShadowEnd)
                    {
                        _audio.volume =  (SoundManager.isMute || (PDG.Dungeon.instance == null ? false : PDG.Dungeon.instance.isPause) || (PDG.Player.instance == null ? false : PDG.Player.instance.isDie)) ? 0 : SoundManager.EffectVolume;
                        _audio.PlayOneShot(shootClip3);
                        Vector3 dir = endPos - transform.position;
                        dir.Normalize();
                        transform.Translate(dir * moveSpeed * Time.deltaTime);
                        if (Vector3.Distance(endPos, transform.position) < 2.0f)
                        {
                            if (!shotList[0].activeSelf) shotList[0].SetActive(true);
                            if (shotList[1].activeSelf) shotList[1].SetActive(false);
                            if (shotList[2].activeSelf) shotList[2].SetActive(false);


                            for (int i = 0; i < 5; i++)
                            {
                                _audio.volume =  (SoundManager.isMute || (PDG.Dungeon.instance == null ? false : PDG.Dungeon.instance.isPause) || (PDG.Player.instance == null ? false : PDG.Player.instance.isDie)) ? 0 : SoundManager.EffectVolume;
                                _audio.PlayOneShot(shootClip1);
                                shotList[0].GetComponent<UbhShotCtrl>()._mobName = mobData.Name;
                                shotList[0].GetComponent<UbhShotCtrl>()._bulletName = "Monster_Bullet";
                                shotList[0].GetComponent<UbhShotCtrl>().StartShotRoutine();
                                yield return new WaitForSeconds(1.0f);
                            }
                            if (!shotList[1].activeSelf)
                            {
                                shotList[1].SetActive(true);
                                _audio.volume =  (SoundManager.isMute || (PDG.Dungeon.instance == null ? false : PDG.Dungeon.instance.isPause) || (PDG.Player.instance == null ? false : PDG.Player.instance.isDie)) ? 0 : SoundManager.EffectVolume;
                                _audio.PlayOneShot(shootClip2);
                                shotList[1].GetComponent<UbhShotCtrl>()._bulletName = "Monster_Bullet";
                                shotList[1].GetComponent<UbhShotCtrl>().StartShotRoutine();
                            }
                            yield return new WaitForSeconds(5.0f);
                            shadow = null;
                            isShadowEnd = false;
                        }
                    }
                    yield return new WaitForSeconds(Time.deltaTime);
                }
                else
                {
                    if (shotList[0].activeSelf) shotList[0].SetActive(false);
                    if (shotList[1].activeSelf) shotList[1].SetActive(false);
                    if (!shotList[2].activeSelf) shotList[2].SetActive(true);
                    if (!fogParticleObj.activeSelf)
                    {
                        shotList[2].GetComponent<UbhShotCtrl>()._mobName = mobData.Name;
                        shotList[2].GetComponent<UbhShotCtrl>()._bulletName = "Monster_Bullet";
                        shotList[2].GetComponent<UbhShotCtrl>().StartShotRoutine();
                        fogParticleObj.SetActive(true);
                        GetComponent<CircleCollider2D>().enabled = false;
                        for (int i = 0; i < fogParticles.Length; i++) fogParticles[i].Play();
                    }

                    for (int i = 0; i < fogParticles.Length; i++)
                    {
                        ParticleSystem.EmissionModule EM = fogParticles[i].emission;
                        ParticleSystem.Burst _burst = EM.GetBurst(0);
                        if (!isFogPlayingEnd) _burst.cycleCount++;
                        else _burst.cycleCount--;
                        EM.SetBurst(0, _burst);
                        if (_burst.cycleCount >= 300) isFogPlayingEnd = true;
                        if (isFogPlayingEnd && _burst.cycleCount < 10)
                        {
                            _burst.cycleCount = 1;
                            yield return new WaitForSeconds(2.0f);
                            fogParticles[i].Stop();
                            isFogEnd = true;
                            shotList[2].transform.localPosition = Vector3.zero;
                        }
                        else
                        {
                            float x = targetObj.transform.position.x;
                            float y = targetObj.transform.position.y;
                            shotList[2].transform.position = new Vector3(UnityEngine.Random.Range(x - 20, x + 20), UnityEngine.Random.Range(y - 20, y + 20), 0);
                        }
                    }
                    if (fogParticleObj.activeSelf && isFogEnd)
                    {
                        fogParticleObj.SetActive(false);
                        timeover = false;
                        isFogPlayingEnd = false;
                        isFogEnd = false;
                        GetComponent<CircleCollider2D>().enabled = true;
                    }
                    yield return new WaitForSeconds(Time.deltaTime);
                }
            }
        }
        IEnumerator FogTimer()
        {
            while(!isDie)
            {
                if (!timeover) fireTimeNow += Time.deltaTime;
                if (fireTimeNow > fireTime)
                {
                    if (shadow == null)
                    {
                        fireTimeNow = 0.0f;
                        timeover = true;
                    }
                }
                yield return new WaitForSeconds(Time.deltaTime);
            }
        }
        IEnumerator ShadowAlpha()
        {
            if (!isDie)
            {
                endPos = targetObj.transform.position;
                monsterShadow.transform.position = targetObj.transform.position;
                for (float i = 0.0f; i < 0.5f; i += Time.deltaTime)
                {
                    monsterShadow.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, i);
                    yield return new WaitForSeconds(Time.deltaTime);
                }
                yield return new WaitForSeconds(0.5f);
                for (float i = 0.0f; i < 0.5f; i += Time.deltaTime)
                {
                    monsterShadow.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 0.5f - i);
                    yield return new WaitForSeconds(Time.deltaTime);
                }
                monsterShadow.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 0);

                isShadowEnd = true;
            }
        }
        IEnumerator HitAction()
        {
            Vector3 originPos = transform.position;
            float t = 0.0f;
            while (t < 0.2f)
            {
                transform.position = new Vector3(originPos.x - UnityEngine.Random.Range(-0.1f, 0.1f), originPos.y - UnityEngine.Random.Range(-0.1f, 0.1f), 0);
                t += Time.deltaTime;
                yield return new WaitForSeconds(Time.deltaTime);
            }
            transform.position = originPos;
        }
        IEnumerator HitEffect()
        {
            while (isHit && !isDie)
            {
                sr.color = HitColor;
                yield return new WaitForSeconds(0.06f);
                sr.color = HitColor2;
                yield return new WaitForSeconds(0.06f);
                sr.color = HitColor3;
                yield return new WaitForSeconds(0.06f);
                sr.color = HitColor4;
                yield return new WaitForSeconds(0.06f);
                sr.color = Color.white;
            }
            HitCoroutine = null;
        }
        public void SetDamage(float _damage)
        {
            if (isInvincibility) return;
            if (isDie) return;
            if (isHit) return;
            mobInfo.curHp -= _damage;
            isHit = true;
            if (HitCoroutine == null) HitCoroutine = StartCoroutine(HitEffect());
            hitTime = 0.25f;
            StartCoroutine(HitAction());
            _audio.volume =  (SoundManager.isMute || (PDG.Dungeon.instance == null ? false : PDG.Dungeon.instance.isPause) || (PDG.Player.instance == null ? false : PDG.Player.instance.isDie)) ? 0 : SoundManager.EffectVolume;
            _audio.PlayOneShot(hitClip);
            if (mobInfo.curHp <= 0)
            {
                UbhObjectPool.instance.ReleaseAllBullet();
                ScreenShot.instance.Screenshot(gameObject);
                isDie = true;
                StartCoroutine(PikoDieEffect());
            }
            else
            {
                animator.SetTrigger("Hit");
            }
        }
        IEnumerator PikoDieEffect()
        {
            yield return null;
            animator.SetTrigger("Hit");
            PDG.Camera _camera = GameObject.Find("Camera").GetComponent<PDG.Camera>();
            Transform targetOrigin = _camera.target;
            _camera.target = transform;
            PDG.Player.instance.isCameraOff = true;
            Time.timeScale = 0.1f;
            _audio.volume =  (SoundManager.isMute || (PDG.Dungeon.instance == null ? false : PDG.Dungeon.instance.isPause) || (PDG.Player.instance == null ? false : PDG.Player.instance.isDie)) ? 0 : SoundManager.EffectVolume;
            _audio.PlayOneShot(bossShaker);
            yield return new WaitForSeconds(0.5f);
            Time.timeScale = 1;
            Vector3 originPos = transform.position;
            CameraShaker._instance.StartShake(0.1f, Time.deltaTime, 1.0f);
            _audio.volume =  (SoundManager.isMute || (PDG.Dungeon.instance == null ? false : PDG.Dungeon.instance.isPause) || (PDG.Player.instance == null ? false : PDG.Player.instance.isDie)) ? 0 : SoundManager.EffectVolume;
            _audio.PlayOneShot(hitClip2);
            for (int i = 0; i < 60; i++)
            {
                if (i % 2 == 0) transform.position = originPos + new Vector3(0.1f, 0);
                else transform.position = originPos + new Vector3(-0.1f, 0);
                yield return new WaitForSeconds(Time.deltaTime);
            }
            animator.SetTrigger("Die");
            UnityEngine.UI.Image spake = GameObject.Find("Canvas").transform.Find("Spake").GetComponent<UnityEngine.UI.Image>();
            float a = 0.0f;
            while (true)
            {
                a += Time.deltaTime * 5;
                spake.color = new Color(1, 1, 1, a);
                if (a > 1.0f) break;
                yield return new WaitForSeconds(Time.deltaTime);
            }
            spake.color = new Color(1, 1, 1, 1);

            _audio.volume =  (SoundManager.isMute || (PDG.Dungeon.instance == null ? false : PDG.Dungeon.instance.isPause) || (PDG.Player.instance == null ? false : PDG.Player.instance.isDie)) ? 0 : SoundManager.EffectVolume;
            _audio.PlayOneShot(bossBomb);
            for (int i = 0; i < DieObjs.Length; i++)
            {
                DieObjs[i].SetActive(true);
                DieObjs[i].GetComponent<Rigidbody2D>().AddForce(new Vector2(UnityEngine.Random.Range(-5, 5), UnityEngine.Random.Range(-5, 5)) * diePow, ForceMode2D.Impulse);
            }

            animator.enabled = false;
            GetComponent<SpriteRenderer>().enabled = false;
            GetComponent<Collider2D>().enabled = false;

            a = 1.0f;
            while (true)
            {
                a -= Time.deltaTime * 5;
                spake.color = new Color(1, 1, 1, a);
                if (a <= 0.0f) break;
                yield return new WaitForSeconds(Time.deltaTime);
            }
            spake.color = new Color(1, 1, 1, 0);

            string stageName = PDG.Dungeon.instance.StageName;
            StageData sData = Array.Find(SheetManager.Instance.StageDB.dataArray, item => item.Type1.Equals(stageName) && item.Type2.Equals("bossstage"));
            string BoxGrade = sData.Chest[GetBoxGrade(sData.Chestprobability)];
            BoxGrade = BoxGrade.Substring(BoxGrade.Length - 1, 1);
            List<ItemsData> iDatas = Array.FindAll(SheetManager.Instance.ItemsDB.dataArray, item => item.Grade.ToUpper().Equals(BoxGrade.ToUpper())).ToList();
            for (int i = 0; i < iDatas.Count; i++)
            {
                Sprite s = Resources.Load<Sprite>("Item/" + iDatas[i].Type1 + "/" + iDatas[i].Type2 + "/" + iDatas[i].ID);
                if (s == null || !Inventorys.Instance.UnlockAndMaxCountCheck(iDatas[i].ID))
                {
                    iDatas.RemoveAt(i);
                    i--;
                    continue;
                }
            }
            List<string> dropItems = new List<string>();
            if (iDatas.Count > 0) dropItems.Add(iDatas[UnityEngine.Random.Range(0, iDatas.Count)].ID);
            int skullCount = UnityEngine.Random.Range(4, 7);
            for (int i = 0; i < skullCount; i++) dropItems.Add("glowing_skull");
            dropItems.Add("key_normal");
            dropItems.Add("muscle_enhancer_body");
            if (UnityEngine.Random.Range(0, 100) < 50) dropItems.Add("muscle_enhancer_body");

            Vector3 tPos = transform.parent.position;
            tPos += dropPosition;
            GameObject objhole = Instantiate(holeObj, tPos, Quaternion.identity, null);
            _camera.target = objhole.transform;
            yield return new WaitForSeconds(1.0f);
            CameraShaker._instance.StartShake(0.1f, Time.deltaTime, 2.0f, true);

            ItemDrop(dropItems.ToArray());
            Inventorys.Instance.ShowGetItemUI("get_reinforcement", Resources.Load<Sprite>("Item/reinforcement"));
            yield return new WaitForSeconds(10.0f);
            ResultPanelScript.instance.ShowResult(LocalizeManager.GetLocalize("ui_win"), true);
        }
        public void ItemDrop(string[] itemName)
        {
            string tId = mobData.ID;
            if (tId.Contains("_t")) tId = tId.Substring(0, tId.Length - 3);
            UnlockListsData[] pikoUnlockDatas = System.Array.FindAll(SheetManager.Instance.UnlockListsDB.dataArray, item => item.NPCID.Equals(tId));
            for (int i = 0; i < pikoUnlockDatas.Length; i++)
            {
                if (SaveAndLoadManager.instance.LoadUnlock(pikoUnlockDatas[i].Itemid) < 1)
                {
                    SaveAndLoadManager.instance.SaveUnlock(pikoUnlockDatas[i].Itemid, 1);
                    ItemsData iData = System.Array.Find(SheetManager.Instance.ItemsDB.dataArray, item => item.ID.Equals(pikoUnlockDatas[i].Itemid));
                    Inventorys.Instance.ShowGetItemUI("monster_unlockitem", Resources.Load<Sprite>("Item/" + iData.Type1 + "/" + iData.Type2 + "/" + iData.ID));
                }
            }

            Vector3 tPos = transform.parent.position;
            tPos += dropPosition;
            for (int i = 0; i < itemName.Length; i++)
            {
                float angle = -90.0f;
                angle += (i * (360.0f / itemName.Length));
                Vector3 pos = GetPosition(tPos, angle);
                Vector3 dir = pos - tPos;
                dir.Normalize();
                GameObject obj = ObjManager.Call().GetObject("Item");
                if (obj != null)
                {
                    Item _item = obj.GetComponent<Item>();
                    _item._Item = new ItemInfo
                    {
                        Data = Inventorys.Instance.CopyItem(Array.Find(SheetManager.Instance.ItemsDB.dataArray, item => item.ID.Equals(itemName[i])))
                    };
                    _item.dir = dir;
                    _item.itemCount = 1;
                    _item.rPow = 1;
                    float startDis2 = 2.0f;
                    float angle2 = angle += 90.0f;
                    if (angle2 <= 180) startDis2 -= (angle2 / 150.0f);
                    else startDis2 -= ((180.0f / 150.0f) + ((180.0f - angle2) / 150.0f));

                    obj.transform.position = pos + (dir * startDis2);
                    obj.SetActive(true);
                }
            }
            PDG.Dungeon.DropMaterial(mobData.Type, mobData.Type2, transform.position);
        }
        #region -- Tool --
        public static float GetAngle(Vector3 vStart, Vector3 vEnd)
        {
            Vector3 v = vEnd - vStart;

            return Mathf.Atan2(v.y, v.x) * Mathf.Rad2Deg;
        }
        public static Vector3 GetPosition(Vector3 vStart, float angle)
        {
            float x = Mathf.Cos(Mathf.Deg2Rad * angle) + vStart.x;
            float y = Mathf.Sin(Mathf.Deg2Rad * angle) + vStart.y;

            return new Vector3(x, y, 0);
        }
        private int GetBoxGrade(float[] percent)
        {
            int[] per = new int[percent.Length];
            for (int i = 0; i < percent.Length; i++)
            {
                per[i] = Mathf.FloorToInt(percent[i] * 1000);
            }

            int[] perTotal = new int[1000];
            int t = 0;
            for (int i = 0; i < per.Length; i++)
            {
                for (int j = 0; j < per[i]; j++)
                {
                    perTotal[t] = i;
                    t++;
                }
            }

            return perTotal[UnityEngine.Random.Range(0, 1000)];
        }
        #endregion
    }
}
