﻿using PDG;
using System;
using System.Collections;
using UnityEngine;

namespace MOB
{
    public class MonsterArggro : MonoBehaviour
    {
        [Header("Aggro")]
        [SerializeField] private float AggroRadius = 10.0f;

        public Monster _monster;
        [SerializeField] private Animator SpawnEffect;
        public bool isStart = false;

        public bool isWait = false;
        public bool isTuto = false;

        [SerializeField] private AudioSource _audio;
        [SerializeField] private RuntimeAnimatorController[] _animControls;

        [SerializeField] private GameObject hpBarObj;
        public bool isWarp;
        public string animName;

        // Start is called before the first frame update
        public void InitSetting(GameObject _heroObj, MonstersData mobData, int _health, bool _isWarp, string _animName, bool _isUnique = false)
        {
            isStart = false;
            transform.Find("MonsterMinimap").gameObject.SetActive(true);
            _monster.transform.localPosition = Vector3.zero;
            _monster.animator.runtimeAnimatorController = Array.Find(_animControls, item => item.name.Equals(_animName));
            if (mobData.ID.Contains("goblin"))
                _monster.mobInfo.maxHp = _monster.mobInfo.curHp = _health * 1.3f;
            else
            {
                if (Dungeon.instance.StageLevel == 0)
                    _monster.mobInfo.maxHp = _monster.mobInfo.curHp = _health;
                else if (Dungeon.instance.StageLevel == 1)
                    _monster.mobInfo.maxHp = _monster.mobInfo.curHp = _health * 2.5f;
                else if (Dungeon.instance.StageLevel == 2)
                    _monster.mobInfo.maxHp = _monster.mobInfo.curHp = _health * 4;
                else if (Dungeon.instance.StageLevel == 3)
                    _monster.mobInfo.maxHp = _monster.mobInfo.curHp = _health * 5;

                float hp = _monster.mobInfo.curHp;
                if (SaveAndLoadManager.nDifficulty == 0) _monster.mobInfo.maxHp = _monster.mobInfo.curHp = (hp / 2);
                else if (SaveAndLoadManager.nDifficulty == 1) _monster.mobInfo.maxHp = _monster.mobInfo.curHp = (hp * 0.8f);
                else if (SaveAndLoadManager.nDifficulty == 2) _monster.mobInfo.maxHp = _monster.mobInfo.curHp = (hp * 2);
                else if (SaveAndLoadManager.nDifficulty == 3) _monster.mobInfo.maxHp = _monster.mobInfo.curHp = (hp * 4);
            }
            _monster.isUnique = _isUnique;
            _monster.InitSetting(mobData);
            isWarp = _isWarp;
            if (_isWarp)
            {
                _monster.isWarpMonster = true;
                Dungeon.instance.WarpSpawnMonsters.Add(gameObject);
            }
            else
            {
                _monster.isWarpMonster = false;
                Dungeon.instance.SpawnMonsters.Add(gameObject);
            }
            _monster.bulletSpeed = mobData.Bulletspeed;
            _monster.fAttackDelay = mobData.Shootdelay;
            _monster.fAttackDelayNow = mobData.Shootdelay;
            _monster.fBulletDelay = mobData.Bulletdelay;
            _monster.nFireCount = mobData.Shootcount;
            _monster.nFireAngle = mobData.Aim;
            _monster.fAttackRange = mobData.Range;
            _monster.fBulletRange = mobData.Bulletrange;
            _monster.knock = mobData.Force;
            _monster.fBulletSize = mobData.Bulletsize;
            _monster.isAuto = mobData.Auto;
            _monster.isAutoAim = mobData.Autoaim;
            _monster.nModCount = mobData.Multishoot;
            _monster.isElemental = mobData.Elemental;
            _monster.fElementalTime = mobData.Elementaltime;
            _monster.strElementalResis = mobData.Elementalresist;
            _monster.speedOrigin = mobData.Movespeed;
            _monster.isInvincibility = false;
            _monster.mobType = mobData.Type2;

            GameObject obj = Instantiate(hpBarObj, GameObject.Find("Canvas").transform);
            obj.GetComponent<HpBarController>().parantObj = gameObject;
            obj.transform.SetParent(GameObject.Find("Canvas").transform);
            obj.transform.SetAsFirstSibling();
            Dungeon.instance.HpList.Add(obj);
            _monster.hpBar = obj;

            if (mobData.ID.Contains("dust") || mobData.ID.Contains("gslime") || mobData.ID.Contains("bat_bomb") || (mobData.ID.Contains("skull_green") && !mobData.ID.Contains("hand")) ||
                mobData.ID.Contains("woodlouse") || mobData.ID.Contains("slime_flying") || mobData.ID.Contains("goblin")) _monster.isFire = false;
            else _monster.isFire = true;

            animName = _animName;
            if (mobData.Type.Equals("subboss"))
            {
                _monster.isBoss = true;
                _monster.isFire = false;
            }
            else
            {
                _monster.isBoss = false;
            }

            isWait = false;
            obj.SetActive(false);
            if (mobData.ID.Contains("hoodie_fireshot")) _monster.isArm = true;
            else _monster.isArm = false;

            if (mobData.ID.Contains("bat_bomb"))
            {
                isStart = true;
                _monster.isMimic = false;
                _monster.isHit = false;
                _monster.isRoaming = true;
                _monster.isNotKnock = true;
                _monster.isBomb = true;
                transform.Find("MonsterMinimap").gameObject.SetActive(false);
                _monster.gameObject.SetActive(true);
            }
            else if (mobData.ID.Contains("skull_green"))
            {
                isStart = true;
                _monster.isMimic = false;
                _monster.isHit = false;
                _monster.isRoaming = true;
                _monster.isNotKnock = true;
                _monster.isBomb = false;
                transform.Find("MonsterMinimap").gameObject.SetActive(false);
                _monster.gameObject.SetActive(true);
            }
            else if (mobData.ID.Contains("ghost_hide"))
            {
                isStart = true;
                _monster.isMimic = false;
                _monster.isHit = false;
                _monster.isRoaming = true;
                _monster.isNotKnock = false;
                _monster.isBomb = false;
                transform.Find("MonsterMinimap").gameObject.SetActive(false);
                _monster.gameObject.SetActive(true);
            }
            else if (mobData.ID.Contains("slime_flying"))
            {
                isStart = true;
                _monster.isMimic = false;
                _monster.isHit = false;
                _monster.isRoaming = true;
                _monster.isNotKnock = true;
                _monster.isBomb = false;
                transform.Find("MonsterMinimap").gameObject.SetActive(false);
                _monster.gameObject.SetActive(true);
            }
            else if (mobData.ID.Contains("woodlouse") || mobData.ID.Contains("snake_ice"))
            {
                isStart = true;
                _monster.isMimic = false;
                _monster.isHit = false;
                _monster.isRoaming = true;
                _monster.isNotKnock = false;
                _monster.isBomb = false;
                transform.Find("MonsterMinimap").gameObject.SetActive(false);
                _monster.gameObject.SetActive(true);
            }
            else if (mobData.ID.Contains("mimic"))
            {
                isStart = true;
                _monster.isMimic = true;
                _monster.isHit = false;
                _monster.isRoaming = false;
                _monster.isNotKnock = false;
                _monster.isBomb = false;
                transform.Find("MonsterMinimap").gameObject.SetActive(false);
                _monster.gameObject.SetActive(true);
            }
            else if (mobData.ID.Contains("goblin"))
            {
                isStart = true;
                _monster.isMimic = false;
                _monster.isHit = false;
                _monster.isRoaming = false;
                _monster.isNotKnock = false;
                _monster.isBomb = false;
                transform.Find("MonsterMinimap").gameObject.SetActive(false);
                _monster.gameObject.SetActive(true);
            }
            else
            {
                _monster.isMimic = false;
                _monster.isRoaming = false;
                _monster.isNotKnock = false;
                _monster.isBomb = false;
                if(!isWarp)
                {
                    if (mobData.Type.Equals("subboss"))
                    {
                        _monster.gameObject.SetActive(false);
                    }
                    else if (UnityEngine.Random.Range(0, 100) < 30)
                    {
                        isStart = true;
                        _monster.isRoaming = true;
                        _monster.isHit = false;
                        transform.Find("MonsterMinimap").gameObject.SetActive(false);
                        _monster.gameObject.SetActive(true);
                    }
                    else
                    {
                        _monster.gameObject.SetActive(false);
                    }
                }
                else
                {
                    _monster.gameObject.SetActive(false);
                }
            }
            if (transform.Find("Shadow")) transform.Find("Shadow").localPosition = Vector3.zero;
        }
        private void Update()
        {
            if (isTuto || _monster.isRoaming) return;
            int mask = 1 << 9;
            RaycastHit2D hit = Physics2D.CircleCast(transform.position, AggroRadius, Vector2.zero, 1, mask);
            if (hit && !isStart)
            {
                SpawnEffectStart();
            }
            else if(!isStart)
            {
                _monster.gameObject.SetActive(false);
            }
        }
        public void SpawnEffectStart()
        {
            transform.Find("MonsterMinimap").gameObject.SetActive(false);
            _audio.volume =  (SoundManager.isMute || (PDG.Dungeon.instance == null ? false : PDG.Dungeon.instance.isPause) || (PDG.Player.instance == null ? false : PDG.Player.instance.isDie)) ? 0 : SoundManager.EffectVolume;
            _audio.Play();
            isStart = true;
            SpawnEffect.enabled = true;
            SpawnEffect.Rebind();
        }
        public void EffectEnd()
        {
            StartCoroutine(WaitCoroutine());
        }
        IEnumerator WaitCoroutine()
        {
            _monster.gameObject.SetActive(true);
            while (isWait)
            {
                _monster.isInvincibility = true;
                _monster.isHit = true;
                yield return null;
            }
            if (_monster.mobData.ID.Contains("gslime")) yield return new WaitForSeconds(UnityEngine.Random.Range(0.3f, 1.0f));
            _monster.isHit = false;
            _monster.isInvincibility = false;
            _monster.isTargeting = true;
            if (!_monster.mobData.Type.Equals("subboss"))
            {
                _monster.isWeaponMode = true;
            }
            else
            {
                StateGroup.instance.subShotCount = 0;
                Dungeon.isEliteSpawnWait = false;
                if (!GameObject.Find("Canvas").transform.Find("TutorialText2").gameObject.activeSelf)
                    GameObject.Find("Canvas").transform.Find("TutorialText2").gameObject.SetActive(true);
                _monster.hpBar.SetActive(true);
            }
        }
    }

}