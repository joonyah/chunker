﻿using PDG;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class Boss_NPC : MonoBehaviour
{
    [Header("State")]
    [SerializeField] private bool isScythe;
    [SerializeField] private bool isEyes;
    [SerializeField] private bool isBody;

    [SerializeField] private UnityEngine.UI.Image spakeImage;

    public MonstersData mobData;
    public MOB.MonsterInfo mobInfo;

    private Animator animator;
    private SpriteRenderer sr;
    private bool isInvincibility = false;
    private bool isDie = false;
    public bool isHit = false;
    private bool isIntro = false;

    [Header("Color")]
    public Color HitColor = new Color();
    public Color HitColor2 = new Color();
    public Color HitColor3 = new Color();
    public Color HitColor4 = new Color();
    private Coroutine HitCoroutine = null;
    float hitTime = 0.0f;

    [Header("Sub")]
    [SerializeField] private Boss_NPC scythe;
    [SerializeField] private Boss_NPC eyes;
    [SerializeField] private Transform firePos;
    [SerializeField] private GameObject subEyesObj;
    [SerializeField] private GameObject BodyShootObj;
    List<GameObject> scytheList = new List<GameObject>();
    List<GameObject> eyesList = new List<GameObject>();
    [Header("Laser")]
    [SerializeField] private GameObject LaserObj;
    [SerializeField] private GameObject[] LaserArray;
    [SerializeField] private bool isLaserLeft = false;
    [SerializeField] private float z = 0.0f;
    [SerializeField] private float fRotateSpeed = 20.0f;
    [Header("Quake")]
    [SerializeField] private Transform quakeRightPos;
    [SerializeField] private Transform quakeLeftPos;
    [SerializeField] private GameObject quakeObj;
    [SerializeField] private float quakeDelayTime = 0.4f;

    [SerializeField] private GameObject holeObj;
    [SerializeField] private Vector3 dropPosition;

    [SerializeField] private float laserStateTime = 10.0f;
    [SerializeField] private float quakeTime = 5.0f;
    [SerializeField] private bool isQuake = false;
    [SerializeField] private int ran;
    [SerializeField] private int ran2;

    [SerializeField] private GameObject FragmentsObj;
    [SerializeField] private Rigidbody2D[] Fragments;
    [SerializeField] private float fragmentPow;

    [SerializeField] private GameObject[] nextHole;
    [SerializeField] private BossItemBoxController[] ItemBoxs;
    [SerializeField] private GameObject dieEffect_0;
    [SerializeField] private GameObject dieEffect_1;
    [SerializeField] private GameObject subCollision;
    [SerializeField] private UbhShotCtrl ScytheHitCtl;
    // Start is called before the first frame update
    void Start()
    {
        StageData sData = System.Array.Find(SheetManager.Instance.StageDB.dataArray, item => item.Roomid[0].Equals("d01_bossstage_02"));
        mobData = System.Array.Find(SheetManager.Instance.MonsterDB.dataArray, item => item.ID.Equals("boss_npc"));
        mobInfo = GetComponent<MOB.MonsterInfo>();
        if (PDG.Dungeon.instance.StageLevel == 0)
            mobInfo.maxHp = mobInfo.curHp = Random.Range(sData.Health[0], sData.Health[1]);
        else if (PDG.Dungeon.instance.StageLevel == 1)
            mobInfo.maxHp = mobInfo.curHp = Random.Range(sData.Health[0], sData.Health[1]) * 2;
        else if (PDG.Dungeon.instance.StageLevel == 2)
            mobInfo.maxHp = mobInfo.curHp = Random.Range(sData.Health[0], sData.Health[1]) * 3;
        else if (PDG.Dungeon.instance.StageLevel == 3)
            mobInfo.maxHp = mobInfo.curHp = Random.Range(sData.Health[0], sData.Health[1]) * 5;

        float hp = mobInfo.curHp;
        if (SaveAndLoadManager.nDifficulty == 0) mobInfo.maxHp = mobInfo.curHp = (hp / 2);
        else if (SaveAndLoadManager.nDifficulty == 1) mobInfo.maxHp = mobInfo.curHp = hp * 0.8f;
        else if (SaveAndLoadManager.nDifficulty == 2) mobInfo.maxHp = mobInfo.curHp = (hp * 2);
        else if (SaveAndLoadManager.nDifficulty == 3) mobInfo.maxHp = mobInfo.curHp = (hp * 4);
        sr = GetComponent<SpriteRenderer>();
        animator = GetComponent<Animator>();
        isInvincibility = true;
        if (isBody) StartCoroutine(Intro());

        spakeImage = GameObject.Find("Canvas").transform.Find("Spake").GetComponent<UnityEngine.UI.Image>();
    }
    IEnumerator Intro()
    {
        while(!isIntro)
        {
            yield return null;
        }
        yield return new WaitForSeconds(1.0f);
        Dialogue.Instance.CommnetSetting("stage2_boss_intro_2", 0, null);
        while (Dialogue.Instance.isCommnet)
        {
            yield return null;
        }
        SoundManager.instance.StartAudio(new string[1] { "BGM/Boss_1" }, VOLUME_TYPE.BGM, null, true);
        StartCoroutine(Pattern());
    }
    void Update()
    {
        RaycastHit2D hit = Physics2D.BoxCast(transform.parent.position, new Vector2(30, 20), 0, Vector2.zero, 0, 1 << 9);
        if (hit && !isIntro)
        {
            isIntro = true;
        }
        if (hitTime > 0)
        {
            hitTime -= Time.deltaTime;
        }
        else
        {
            isHit = false;
        }

        if(LaserObj != null && LaserObj.activeSelf)
        {
            if (isLaserLeft) z += Time.deltaTime * fRotateSpeed;
            else z -= Time.deltaTime * fRotateSpeed;
            LaserObj.transform.localEulerAngles = new Vector3(0, 0, z);
        }
    }
    IEnumerator Pattern()
    {
        (FindObjectOfType(typeof(PDG.Camera)) as PDG.Camera).target = PDG.Player.instance.transform;
        yield return new WaitForSeconds(1.0f);
        StateGroup.instance.isBattleTimeStart = true;
        string tId = mobData.ID;
        if (tId.Contains("_t")) tId = tId.Substring(0, tId.Length - 3);
        BossHpController.instance.BossStart(LocalizeManager.GetLocalize("monster_" + tId), GetComponent<MOB.MonsterInfo>());
        scythe.isInvincibility = false;
        eyes.isInvincibility = false;
        StartCoroutine(ScythePattern());
        StartCoroutine(EyesPattern());

        while(!scythe.isDie || !eyes.isDie)
        {
            yield return null;
        }
        yield return StartCoroutine(Spake());
        animator.SetBool("isBody", true);
        yield return new WaitForSeconds(1.0f);
        isInvincibility = false;
        ran = Random.Range(0, 3);
        ran2 = Random.Range(0, 4);

        while (!isDie)
        {
            if (ran == 0)
            {
                animator.SetTrigger("bodyShoot");
                if (Dungeon.instance.StageLevel > 1)
                {
                    ScytheAttackFire();
                }
                if (PDG.Dungeon.instance.StageLevel > 0)
                {
                    yield return new WaitForSeconds(1.0f);
                    animator.SetTrigger("bodyShoot");

                    if (Dungeon.instance.StageLevel > 1)
                    {
                        ScytheAttackFire();
                    }
                }
                yield return new WaitForSeconds(2.5f);
                ran = Random.Range(0, 3);
            }
            else if (ran == 1)
            {
                laserStateTime -= Time.deltaTime;
                if (animator.GetBool("bodyLaserEnd"))
                {
                    isLaserLeft = Random.Range(0, 2) == 0 ? !isLaserLeft : isLaserLeft;
                    laserStateTime = 10.0f;
                    LaserOnOff(true);
                    animator.SetBool("bodyLaserEnd", false);
                    animator.SetTrigger("bodyLaser");
                }
                if(laserStateTime <= 0.0f)
                {
                    laserStateTime = 10.0f;
                    animator.SetBool("bodyLaserEnd", true);
                    LaserOnOff(false);
                    while (true)
                    {
                        ran = Random.Range(0, 3);
                        if (ran != 1) break;
                    }
                }
                //else
                //{
                //    isLaserLeft = Random.Range(0, 100) == 0 ? !isLaserLeft : isLaserLeft;
                //}
            }
            else
            {
                quakeTime -= Time.deltaTime;
                if (!isQuake)
                {
                    isQuake = true;
                    animator.SetTrigger("bodyQuake");
                    if (PDG.Dungeon.instance.StageLevel > 0)
                    {
                        yield return new WaitForSeconds(0.5f);
                        animator.SetTrigger("bodyQuake");
                    }
                }

                if (quakeTime <= 0.0f)
                {
                    isQuake = false;
                    quakeTime = 2.0f;
                    while (true)
                    {
                        ran = Random.Range(0, 3);
                        if (ran != 2) break;
                    }
                }
            }

            if (ran2 == 0)
            {
                transform.Translate(Vector2.left * mobData.Movespeed * Time.deltaTime);
                if (transform.localPosition.x < -12)
                {
                    ran2 = Random.Range(0, 4);
                    while (true)
                    {
                        ran2 = Random.Range(0, 4);
                        if (ran2 != 0) break;
                    }
                }
                else
                {
                    if (Random.Range(0, 80) == 0)
                    {
                        ran2 = Random.Range(0, 4);
                        while (true)
                        {
                            ran2 = Random.Range(0, 4);
                            if (ran2 != 0) break;
                        }
                    }
                }
            }
            else if (ran2 == 1)
            {
                transform.Translate(Vector2.right * mobData.Movespeed * Time.deltaTime);
                if (transform.localPosition.x > 12)
                {
                    ran2 = Random.Range(0, 4);
                    while (true)
                    {
                        ran2 = Random.Range(0, 4);
                        if (ran2 != 1) break;
                    }
                }
                else
                {
                    if (Random.Range(0, 80) == 0)
                    {
                        ran2 = Random.Range(0, 4);
                        while (true)
                        {
                            ran2 = Random.Range(0, 4);
                            if (ran2 != 1) break;
                        }
                    }
                }
            }
            else if (ran2 == 2)
            {
                transform.Translate(Vector2.up * mobData.Movespeed * Time.deltaTime);
                if (transform.localPosition.y > 6)
                {
                    ran2 = Random.Range(0, 4);
                    while (true)
                    {
                        ran2 = Random.Range(0, 4);
                        if (ran2 != 2) break;
                    }
                }
                else
                {
                    if (Random.Range(0, 80) == 0)
                    {
                        ran2 = Random.Range(0, 4);
                        while (true)
                        {
                            ran2 = Random.Range(0, 4);
                            if (ran2 != 2) break;
                        }
                    }
                }
            }
            else if (ran2 == 3)
            {
                transform.Translate(Vector2.down * mobData.Movespeed * Time.deltaTime);
                if (transform.localPosition.y < 5)
                {
                    ran2 = Random.Range(0, 4);
                    while (true)
                    {
                        ran2 = Random.Range(0, 4);
                        if (ran2 != 3) break;
                    }
                }
                else
                {
                    if (Random.Range(0, 80) == 0)
                    {
                        ran2 = Random.Range(0, 4);
                        while (true)
                        {
                            ran2 = Random.Range(0, 4);
                            if (ran2 != 3) break;
                        }
                    }
                }
            }

            yield return new WaitForSeconds(Time.deltaTime);
        }
    }
    IEnumerator ScythePattern()
    {
        float sTime = 1.0f;
        while (!scythe.isDie)
        {
            sTime -= Time.deltaTime;
            if (sTime <= 0.0f)
            {
                sTime = 5.0f;
                scythe.ScytheAttack();
            }
            yield return new WaitForSeconds(Time.deltaTime);
        }
    }
    IEnumerator EyesPattern()
    {
        float sTime = Random.Range(0.5f, 1.5f);
        while (!eyes.isDie)
        {
            sTime -= Time.deltaTime;
            if (sTime <= 0.0f)
            {
                sTime = Random.Range(0.5f, 1.5f);
                eyes.EyesAttack();
            }
            yield return new WaitForSeconds(Time.deltaTime);
        }
    }
    public void ScytheAttack()
    {
        if(animator.GetBool("isAttackEnd"))
        {
            SoundManager.instance.StartAudio(new string[1] { "boss/boss_npc_scythe" }, VOLUME_TYPE.EFFECT);
            animator.SetBool("isAttackEnd", false);
            animator.SetTrigger("attack");
        }
    }
    public void ScytheAttackFire()
    {
        GameObject obj = ObjManager.Call().GetObject("bossScythe");
        obj.transform.position = firePos.position;
        if (firePos.name.Equals("Boss"))
            obj.GetComponent<BossScytheController>().parantTransform = transform;
        else
            obj.GetComponent<BossScytheController>().parantTransform = transform.parent;
        obj.SetActive(true);
        StartCoroutine(ScytheAttack2());
        scytheList.Add(obj);
    }
    IEnumerator ScytheAttack2()
    {
        yield return new WaitForSeconds(1.0f);
        animator.SetBool("isAttackEnd", true);
    }
    public void EyesAttack()
    {
        SoundManager.instance.StartAudio(new string[1] { "boss/boss_npc_eyes" }, VOLUME_TYPE.EFFECT);
        GameObject obj = Instantiate(subEyesObj, null);
        obj.transform.position = firePos.position;
        obj.GetComponent<Rigidbody2D>().AddForce(new Vector2(Random.Range(-1, 1), Random.Range(-1, 1)) * Random.Range(0.1f, 1.5f), ForceMode2D.Impulse);
        eyesList.Add(obj);
    }
    public void SetDamage(float _damage, Vector3 _pos)
    {
        if (Dungeon.instance.isPause) return;
        if (isInvincibility) return;
        if (isDie) return;

        GameObject damageObjOrigin = Resources.Load<GameObject>("DamageText");
        GameObject damageObj = Instantiate(damageObjOrigin);
        damageObj.transform.SetParent(GameObject.Find("Canvas").transform);
        damageObj.transform.SetSiblingIndex(16);
        damageObj.transform.localScale = Vector3.one;
        damageObj.GetComponent<DamageText>().InitSetting(_damage, gameObject);
        mobInfo.curHp -= _damage;
        if (!isHit)
        {
            isHit = true;
            hitTime = 0.25f;
        }
        if (HitCoroutine == null) HitCoroutine = StartCoroutine(HitEffect());


        GameObject obj = ObjManager.Call().GetObject("DamageHit_Boss1");
        if (obj)
        {
            obj.transform.position = _pos;
            obj.SetActive(true);
        }

        string tId = mobData.ID;
        if (tId.Contains("_t")) tId = tId.Substring(0, tId.Length - 3);

        if (isScythe)
        {
            ScytheHitCtl.StartShotRoutine();
            BossHpController.instance.BossStart(LocalizeManager.GetLocalize("monster_" + tId + "_scythe"), GetComponent<MOB.MonsterInfo>());
        }
        else if (isEyes)
            BossHpController.instance.BossStart(LocalizeManager.GetLocalize("monster_" + tId + "_eyes"), GetComponent<MOB.MonsterInfo>());
        else
            BossHpController.instance.BossStart(LocalizeManager.GetLocalize("monster_" + tId), GetComponent<MOB.MonsterInfo>());

        if (mobInfo.curHp <= 0)
        {
            isDie = true;
            if(isScythe)
            {
                ScytheHitCtl.gameObject.SetActive(false);
                GetComponent<PolygonCollider2D>().enabled = false;
                StartCoroutine(SubDieEffect());
            }
            else if(isEyes)
            {
                GetComponent<PolygonCollider2D>().enabled = false;
                StartCoroutine(SubDieEffect());
            }
            else
            {
                PDG.Dungeon.instance.isBossRushStart = false;
                StateGroup.instance.isClearTimeStart = false;
                StateGroup.instance.isBattleTimeStart = false;
                SaveAndLoadManager.instance.SaveCollection(new CollectionData { _id = tId, _isOpen = true, _isWeapon = false });
                UbhObjectPool.instance.ReleaseAllBullet();
                ResultPanelScript.instance.bossList.Add(1);
                ScreenShot.instance.Screenshot(gameObject);
                SoundManager.instance.StopAllSound();
                PDG.Player.instance.fInvincibleTime_offensive_01 = 10;
                for (int i = 0; i < scytheList.Count; i++)
                {
                    scytheList[i].SetActive(false);
                }
                for (int i = 0; i < eyesList.Count; i++)
                {
                    Destroy(eyesList[i]);
                }
                UbhObjectPool.instance.ReleaseAllBullet();
                LaserOnOff(false);
                GetComponent<BoxCollider2D>().enabled = false;
                subCollision.SetActive(false);
                StartCoroutine(DieAction());
            }
        }
    }
    IEnumerator SubDieEffect()
    {
        for (int i = 0; i < 3; i++)
        {
            if (dieEffect_0)
            {
                GameObject dieeffect0 = Instantiate(dieEffect_0);
                dieeffect0.transform.position = transform.position;
                Destroy(dieeffect0, 2f);
            }
            if (dieEffect_1)
            {
                GameObject dieeffect1 = Instantiate(dieEffect_1);
                dieeffect1.transform.position = transform.position;
                Destroy(dieeffect1, 2f);
            }
            yield return new WaitForSeconds(0.5f);
        }
        gameObject.SetActive(false);
    }
    IEnumerator DieAction()
    {
        if (GetComponent<BoxCollider2D>()) Destroy(GetComponent<BoxCollider2D>());
        dieEffect_0.SetActive(true);
        dieEffect_1.SetActive(true);
        CameraShaker._instance.StartShake(0.1f, Time.deltaTime, 2.5f, true);
        yield return StartCoroutine(Spake());
        yield return new WaitForSeconds(0.2f);
        PDG.Player.instance.isCameraOff = true;
        (FindObjectOfType(typeof(PDG.Camera)) as PDG.Camera).target = transform;
        animator.SetTrigger("isDie");
        FragmentsObj.SetActive(true);
        for (int i = 0; i < Fragments.Length; i++)
        {
            Fragments[i].AddForce(new Vector2(Random.Range(-1, 1), Random.Range(-1, 1)) * fragmentPow, ForceMode2D.Impulse);
            Fragments[i].GetComponent<FragmentsController>().isStart = true;
        }

        SoundManager.instance.StartAudio(new string[1] { "Monster/boss_shaker" }, VOLUME_TYPE.EFFECT);
        yield return new WaitForSecondsRealtime(0.3f);
        Time.timeScale = 0.1f;
        yield return new WaitForSecondsRealtime(3.0f);
        Time.timeScale = 1.0f;

        yield return new WaitForSeconds(0.5f);
        animator.SetTrigger("isDieEnd");
        Dialogue.Instance.CommnetSetting("stage2_boss_end", 0, null);
        while(true)
        {
            if (Dialogue.Instance.isBossEnd) break;
            yield return null;
        }
        StartCoroutine(EndEventCoroutine());
    }
    IEnumerator EndEventCoroutine()
    {
        string tId = mobData.ID;
        if (tId.Contains("_t")) tId = tId.Substring(0, tId.Length - 3);
        if (PDG.Dungeon.instance.isBossRush)
        {
            List<string> dropItems = new List<string>();
            int skullCount = 10 * (PDG.Dungeon.instance.StageLevel + 1);
            for (int i = 0; i < skullCount; i++) dropItems.Add("glowing_skull");
            dropItems.Add("recovery_syringe_2");
            dropItems.Add("recovery_syringe_2");
            dropItems.Add("recovery_syringe_2");
            for (int i = 0; i < ItemBoxs.Length; i++)
            {
                ItemBoxs[i].gameObject.SetActive(true);
                ItemBoxs[i]._id = tId;
                if (ItemBoxs[i].isMaterial)
                {
                    ItemBoxs[i].gameObject.SetActive(false);
                }
                else
                {
                    ItemBoxs[i].itemName = dropItems.ToArray();
                }
            }
        }
        else
        {
            int ata = 0;
            List<ItemsData> iDatas = null;
            while (true)
            {
                ChestPerData data = System.Array.Find(SheetManager.Instance.ChestPerDB.dataArray, item => item.Stagelevel.Equals(PDG.Dungeon.instance.StageLevel));
                string BoxGrade = data.Chest[SaveAndLoadManager.GetBoxGrade(data)];
                BoxGrade = BoxGrade.Substring(BoxGrade.Length - 1, 1);
                iDatas = System.Array.FindAll(SheetManager.Instance.ItemsDB.dataArray, item => item.Grade.ToUpper().Equals(BoxGrade.ToUpper())).ToList();
                for (int i = 0; i < iDatas.Count; i++)
                {
                    Sprite s = Resources.Load<Sprite>("Item/" + iDatas[i].Type1 + "/" + iDatas[i].Type2 + "/" + iDatas[i].ID);
                    if (s == null || !Inventorys.Instance.UnlockAndMaxCountCheck(iDatas[i].ID))
                    {
                        iDatas.RemoveAt(i);
                        i--;
                        continue;
                    }
                }
                ata++;
                if (ata >= 1000) break;
                if (iDatas.Count > 0) break;
            }
            List<string> dropItems = new List<string>();
            if (iDatas.Count > 0) dropItems.Add(iDatas[UnityEngine.Random.Range(0, iDatas.Count)].ID);
            int skullCount = UnityEngine.Random.Range(10, 20);
            if (SaveAndLoadManager.nDifficulty != 0)
            {
                skullCount *= (SaveAndLoadManager.nDifficulty * 2) - 1;
            }
            for (int i = 0; i < skullCount; i++) dropItems.Add("glowing_skull");
            //dropItems.Add("key_normal");
            dropItems.Add("muscle_enhancer_body");
            if (UnityEngine.Random.Range(0, 100) < 50) dropItems.Add("muscle_enhancer_body");
            yield return new WaitForSeconds(1.0f);
            for (int i = 0; i < ItemBoxs.Length; i++)
            {
                ItemBoxs[i].gameObject.SetActive(true);
                ItemBoxs[i]._id = tId;
                if (ItemBoxs[i].isMaterial)
                {
                    int ran = Random.Range(1, 4);
                    int ran2 = Random.Range(0, 3);
                    int ran3 = Random.Range(0, 3);
                    string[] ttt = new string[1 + ran];
                    for (int j = 0; j < 1 + ran; j++)
                    {
                        if (j == 0) ttt[j] = "boss_boss_npc";
                        else ttt[j] = "normal_" + (ran2 == 0 ? "Cells" + (ran3 == 0 ? "_0" : "_1") : (ran2 == 1 ? "Ore" : "Parts" + (ran3 == 0 ? "_0" : (ran3 == 1 ? "_1" : "_2"))));
                    }
                    ItemBoxs[i].itemName = ttt;
                }
                else
                {
                    ItemBoxs[i].itemName = dropItems.ToArray();
                }
            }
            
            if (SteamStatsAndAchievements.instance != null) SteamStatsAndAchievements.instance.SettingStat(tId + "_hunting");
            if (SteamStatsAndAchievements.instance != null) SteamStatsAndAchievements.instance.SettingStat("stat_stage_clear_" + PDG.Dungeon.instance.StageLevel);
            if (SteamStatsAndAchievements.instance != null) SteamStatsAndAchievements.instance.SettingStat("stat_hunting_boss_count");

            List<AchievementData> savedatas = new List<AchievementData>();
            AchievementsData aData_0 = System.Array.Find(SheetManager.Instance.AchievementsDB.dataArray, item => item.ID.Equals("huntermaster_npc"));
            savedatas.Add(new AchievementData { _id = aData_0.ID, _cur = 1, _max = aData_0.MAX, _clear = false });
            AchievementsData aData_1 = System.Array.Find(SheetManager.Instance.AchievementsDB.dataArray, item => item.ID.Equals("stage_clear_" + PDG.Dungeon.instance.StageLevel));
            savedatas.Add(new AchievementData { _id = aData_1.ID, _cur = 1, _max = aData_1.MAX, _clear = false });
            if (StateGroup.instance.nShotByBoss == 0)
            {
                AchievementsData aData = System.Array.Find(SheetManager.Instance.AchievementsDB.dataArray, item => item.ID.Equals("godcontrol"));
                savedatas.Add(new AchievementData { _id = aData.ID, _cur = 1, _max = aData.MAX, _clear = false });
            }
            if (StateGroup.instance.nShotByHunter == 0)
            {
                AchievementsData aData = System.Array.Find(SheetManager.Instance.AchievementsDB.dataArray, item => item.ID.Equals("nodamageclear"));
                savedatas.Add(new AchievementData { _id = aData.ID, _cur = 1, _max = aData.MAX, _clear = false });
            }
            if (PDG.Player.instance.nowWeaponName.Contains("tentacle_arm"))
            {
                AchievementsData aData = System.Array.Find(SheetManager.Instance.AchievementsDB.dataArray, item => item.ID.Equals("tantaclemaster"));
                savedatas.Add(new AchievementData { _id = aData.ID, _cur = 1, _max = aData.MAX, _clear = false });
            }
            if (StateGroup.instance.fClearTime <= 720)
            {
                AchievementsData aData = System.Array.Find(SheetManager.Instance.AchievementsDB.dataArray, item => item.ID.Equals("possible_0"));
                savedatas.Add(new AchievementData { _id = aData.ID, _cur = 1, _max = aData.MAX, _clear = false });
            }
            if (StateGroup.instance.fClearTime <= 480)
            {
                AchievementsData aData = System.Array.Find(SheetManager.Instance.AchievementsDB.dataArray, item => item.ID.Equals("possible_1"));
                savedatas.Add(new AchievementData { _id = aData.ID, _cur = 1, _max = aData.MAX, _clear = false });
            }
            if (StateGroup.instance.fClearTime <= 180)
            {
                AchievementsData aData = System.Array.Find(SheetManager.Instance.AchievementsDB.dataArray, item => item.ID.Equals("possible_2"));
                savedatas.Add(new AchievementData { _id = aData.ID, _cur = 1, _max = aData.MAX, _clear = false });
            }
            SaveAndLoadManager.instance.SaveAchievements(savedatas.ToArray());

            PDG.Dungeon.instance.isStageBoss = false;
            PDG.Dungeon.instance.StageLevel++;
            PDG.Dungeon.instance.nowStageStep = 0;
            SaveAndLoadManager.instance.GameSave(Inventorys.Instance.slots);
            if (SteamStatsAndAchievements.instance != null) SteamStatsAndAchievements.instance.UnlockAchievementToPublic("Boss_1_Clear");
        }
        PDG.Player.instance.isCameraOff = false;
        (FindObjectOfType(typeof(PDG.Camera)) as PDG.Camera).target = PDG.Player.instance.transform;
        SoundManager.instance.StartAudio(new string[1] { "warp_open" }, VOLUME_TYPE.EFFECT);
        GameObject.Find("Canvas").transform.Find("TutorialText").gameObject.SetActive(true);
        GameObject.Find("Canvas").transform.Find("TutorialText").GetComponent<Text>().text = LocalizeManager.GetLocalize("boss_open_title");
        if (!PDG.Dungeon.instance.isTestMode2)
            GameObject.Find("Canvas").transform.Find("ExitArrow").gameObject.SetActive(true);
        for (int i = 0; i < nextHole.Length; i++)
        {
            nextHole[i].GetComponent<DungeonNextController>().isBossOpen = true;
            nextHole[i].GetComponent<DungeonNextController>().sr.sprite = nextHole[i].GetComponent<DungeonNextController>().endSprite;
        }

        StateGroup.instance.OpenResult(tId);
    }
    IEnumerator HitEffect()
    {
        while (isHit && !isDie)
        {
            sr.color = HitColor;
            yield return new WaitForSeconds(0.06f);
            sr.color = HitColor2;
            yield return new WaitForSeconds(0.06f);
            sr.color = HitColor3;
            yield return new WaitForSeconds(0.06f);
            sr.color = HitColor4;
            yield return new WaitForSeconds(0.06f);
            sr.color = Color.white;
        }
        HitCoroutine = null;
    }
    IEnumerator Spake()
    {
        float a = 0.0f;
        while (true)
        {
            a += Time.fixedUnscaledDeltaTime * 5;
            spakeImage.color = new Color(1, 1, 1, a);
            if (a > 1.0f) break;
            yield return new WaitForSecondsRealtime(Time.fixedUnscaledDeltaTime);
        }
        spakeImage.color = new Color(1, 1, 1, 1);
        a = 1.0f;
        GameObject.Find("Camera").GetComponent<PDG.Camera>().isTestScreenClear = true;
        SoundManager.instance.StartAudio(new string[1] { "BossSpake" }, VOLUME_TYPE.EFFECT);
        while (true)
        {
            a -= Time.fixedUnscaledDeltaTime * 5;
            spakeImage.color = new Color(1, 1, 1, a);
            if (a <= 0.0f) break;
            yield return new WaitForSecondsRealtime(Time.fixedUnscaledDeltaTime);
        }
        spakeImage.color = new Color(1, 1, 1, 0);
        GameObject.Find("Camera").GetComponent<PDG.Camera>().isTestScreenClear = true;
    }
    public void BodyShoot()
    {
        GameObject obj = Instantiate(BodyShootObj, null);
        obj.transform.position = transform.position;
        obj.GetComponent<MOB.MonsterInfo>().maxHp = obj.GetComponent<MOB.MonsterInfo>().curHp = (Dungeon.instance.StageLevel > 0 ? 40 : 15);
        obj.GetComponent<Rigidbody2D>().AddForce(new Vector2(Random.Range(-1, 1), Random.Range(-1, 1)) * Random.Range(0.1f, 1.5f), ForceMode2D.Impulse);
        eyesList.Add(obj);
    }
    private void LaserOnOff(bool isOn)
    {
        LaserObj.SetActive(isOn);
        if (isOn)
        {
            for (int i = 0; i < LaserArray.Length; i++)
            {
                AnimatorStateInfo _stateInfo = LaserArray[i].GetComponent<Animator>().GetCurrentAnimatorStateInfo(0);
                LaserArray[i].GetComponent<Animator>().Play(_stateInfo.fullPathHash, -1, Random.Range(0.0f, 1.0f));
            }
        }
    }
    public void QuakeAttack(int _isLeft)
    {
        if (Dungeon.instance.StageLevel > 0)
        {
            transform.Find("HoleCircleLockOnShot").GetComponent<UbhShotCtrl>().StartShotRoutine();
        }
        SoundManager.instance.StartAudio(new string[1] { "boss/QuakeAttack" }, VOLUME_TYPE.EFFECT);
        StartCoroutine(QuakeAttackCoroutine(_isLeft == 0 ? false : true));
        
    }
    IEnumerator QuakeAttackCoroutine(bool _isLeft)
    {
        float t = 0.0f;
        Vector3 sPos = (_isLeft ? quakeLeftPos.position : quakeRightPos.position);
        Vector3 pPos = PDG.Player.instance.transform.position;
        pPos.y -= 0.4f;
        Vector3 dir = pPos - sPos;
        dir.Normalize();
        for (int i = 0; i < 15; i++)
        {
            GameObject obj = Instantiate(quakeObj, null);
            obj.transform.position = sPos + (dir * t);
            t += 1.0f;
            yield return new WaitForSeconds(quakeDelayTime);
        }
    }
    #region -- Tool --
    public static float GetAngle(Vector3 vStart, Vector3 vEnd)
    {
        Vector3 v = vEnd - vStart;

        return Mathf.Atan2(v.y, v.x) * Mathf.Rad2Deg;
    }
    public static Vector3 GetPosition(Vector3 vStart, float angle)
    {
        float x = Mathf.Cos(Mathf.Deg2Rad * angle) + vStart.x;
        float y = Mathf.Sin(Mathf.Deg2Rad * angle) + vStart.y;

        return new Vector3(x, y, 0);
    }
    private int GetPercentege(float _per, string[] list)
    {
        int[] per = new int[Mathf.FloorToInt(100 * list.Length)];
        for (int i = 0; i < per.Length; i++) per[i] = -1;

        for (int i = 0; i < list.Length; i++)
        {
            for (int j = 0; j < Mathf.FloorToInt(_per * 100); j++)
            {
                per[(i * 100) + j] = i;
            }
        }
        if (per.Length == 0) return -1;
        return per[Random.Range(0, per.Length)];
    }
    #endregion
}
