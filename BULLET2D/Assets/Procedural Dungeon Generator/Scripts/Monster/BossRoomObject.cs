﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossRoomObject : MonoBehaviour
{
    [SerializeField] private float curHp = 5;
    [SerializeField] private bool isHit = false;
    [SerializeField] private float hitTime = 0.0f;
    [SerializeField] private bool isDie = false;

    [SerializeField] private BoxCollider2D backColl;
    [SerializeField] private Animator animator;
    [SerializeField] private Rigidbody2D[] fragments;
    
    [SerializeField] private string _name;

    [SerializeField] private AudioSource audio;
    [SerializeField] private AudioClip hitClip;
    [SerializeField] private AudioClip destroyClip;
    private void Start()
    {
        curHp = 10;
        animator = GetComponent<Animator>();
        audio = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        int mask = 1 << 13 | 1 << 25;
        RaycastHit2D hit = Physics2D.BoxCast(transform.position, new Vector2(1.6f, 3.25f), 0, new Vector2(0, 0.35f), 0, mask);
        if(hit && !isHit && !isDie)
        {
            isHit = true;
            hitTime = 0.25f;
            curHp -= 1;
            audio.volume =  (SoundManager.isMute || (PDG.Dungeon.instance == null ? false : PDG.Dungeon.instance.isPause) || (PDG.Player.instance == null ? false : PDG.Player.instance.isDie)) ? 0 : SoundManager.EffectVolume;
            if (curHp <= 0)
            {
                audio.PlayOneShot(destroyClip);
                isDie = true;
                backColl.enabled = false;
                GetComponent<SpriteRenderer>().enabled = false;
                GetComponent<BoxCollider2D>().enabled = false;
                transform.Find("shadow").gameObject.SetActive(false);
                for (int i=0; i< fragments.Length; i++)
                {
                    fragments[i].AddForce(new Vector2(Random.Range(-0.5f, 0.5f), Random.Range(-0.5f, 0.5f)) * Random.Range(6, 8), ForceMode2D.Impulse);
                    fragments[i].GetComponent<FragmentsController>().isStart = true;
                }
            }
            else
            {
                audio.PlayOneShot(hitClip);
                animator.SetTrigger("Hit");
            }
        }

        if(isHit && !isDie)
        {
            hitTime -= Time.deltaTime;
            if (hitTime <= 0.0f)
            {
                isHit = false;
                hitTime = 0.0f;
            }
        }
    }

    public void SetDamage()
    {
        if (!isHit && !isDie)
        {
            isHit = true;
            hitTime = 0.25f;
            curHp -= 1;
            audio.volume =  (SoundManager.isMute || (PDG.Dungeon.instance == null ? false : PDG.Dungeon.instance.isPause) || (PDG.Player.instance == null ? false : PDG.Player.instance.isDie)) ? 0 : SoundManager.EffectVolume;
            if (curHp <= 0)
            {
                audio.PlayOneShot(destroyClip);
                isDie = true;
                backColl.enabled = false;
                GetComponent<SpriteRenderer>().enabled = false;
                GetComponent<BoxCollider2D>().enabled = false;
                transform.Find("shadow").gameObject.SetActive(false);
                for (int i = 0; i < fragments.Length; i++)
                {
                    fragments[i].AddForce(new Vector2(Random.Range(-0.5f, 0.5f), Random.Range(-0.5f, 0.5f)) * Random.Range(6, 8), ForceMode2D.Impulse);
                    fragments[i].GetComponent<FragmentsController>().isStart = true;
                }
            }
            else
            {
                audio.PlayOneShot(hitClip);
                animator.SetTrigger("Hit");
            }
        }

        if (isHit && !isDie)
        {
            hitTime -= Time.deltaTime;
            if (hitTime <= 0.0f)
            {
                isHit = false;
                hitTime = 0.0f;
            }
        }
    }
}
