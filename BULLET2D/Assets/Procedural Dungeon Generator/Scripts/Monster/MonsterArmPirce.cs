﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonsterArmPirce : MonoBehaviour
{
    public MOB.Monster _mob;
    public Boss_STG _bossSTG;
    bool isEffect = false;
    [SerializeField] private bool isBoss;
    private void OnEnable()
    {
        isEffect = false;
    }
    // Update is called once per frame
    void Update()
    {
        RaycastHit2D hit;
        if (isBoss) hit = Physics2D.BoxCast(transform.position, new Vector2(0.5625f, 0.5f), transform.localEulerAngles.z, Vector2.zero, 0, 1 << 9);
        else hit = Physics2D.BoxCast(transform.position, new Vector2(0.5625f, 0.5f), transform.localEulerAngles.z, Vector2.zero, 0, 1 << 9 | 1 << 8);

        if (hit)
        {
            if(hit.collider.gameObject.layer.Equals(9))
            {
                if (_mob != null)
                {
                    string tId = _mob.mobData.ID;
                    if (tId.Contains("_t")) tId = tId.Substring(0, tId.Length - 3);
                    _mob.isBackgroundAttack = true;
                    if (_mob.mobData.ID.Contains("octopus_king") || _mob.mobData.ID.Contains("boss_stg") || _mob.mobData.ID.Contains("boss_npc") || _mob.mobData.ID.Contains("boss_golem") || _mob.mobData.ID.Contains("boss_stone"))
                        hit.collider.gameObject.GetComponent<PDG.Player>().SetDamage(1, LocalizeManager.GetLocalize("monster_" + tId), DEATH_TYPE.DEATH_SUB_BOSS, tId);
                    else if (_mob.mobData.ID.Contains("piko") || _mob.mobData.ID.Contains("slime_bright") || _mob.mobData.ID.Contains("golem_ice"))
                        hit.collider.gameObject.GetComponent<PDG.Player>().SetDamage(1, LocalizeManager.GetLocalize("monster_" + tId), DEATH_TYPE.DEATH_SUB_BOSS, tId);
                    else
                        hit.collider.gameObject.GetComponent<PDG.Player>().SetDamage(1, LocalizeManager.GetLocalize("monster_" + tId), DEATH_TYPE.DEATH_NORMAL, tId);

                }
                if (_bossSTG != null)
                {
                    string tId = _bossSTG.mobData.ID;
                    if (tId.Contains("_t")) tId = tId.Substring(0, tId.Length - 3);
                    _bossSTG.isBackgroundAttack = true;
                    hit.collider.gameObject.GetComponent<PDG.Player>().SetDamage(1, LocalizeManager.GetLocalize("monster_" + tId), DEATH_TYPE.DEATH_SUB_BOSS, tId);
                }

            }
            else
            {
                if (_mob != null) _mob.isBackgroundAttack = true;
            }
            if(!isEffect)
            {
                isEffect = true;
                GameObject obj2 = ObjManager.Call().GetObject("Hit_sprite4");
                obj2.transform.position = hit.collider.transform.position;
                obj2.transform.localScale = Vector3.one;
                obj2.transform.localEulerAngles = Vector2.zero;
                obj2.GetComponent<SwordHitEffectController>()._effctName = "tentacleHit_" + Random.Range(0, 3);
                obj2.GetComponent<SwordHitEffectController>().animSpeed = 1.0f;
                obj2.GetComponent<SwordHitEffectController>().isPenetrate = false;
                obj2.GetComponent<SwordHitEffectController>().isAttack = false;
                obj2.GetComponent<Animator>().Rebind();
                obj2.SetActive(true);
            }
        }
    }
}
