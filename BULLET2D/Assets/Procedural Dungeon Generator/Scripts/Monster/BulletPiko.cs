﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletPiko : MonoBehaviour
{
    public bool testButton;
    Vector3[] dirs;
    public List<GameObject> bulletList = new List<GameObject>();
    public float startSpeed = 5.0f;
    private void Update()
    {
        if(testButton)
        {
            testButton = false;
            StartCoroutine(Fire());
        }
    }

    IEnumerator Fire()
    {
        bulletList.Clear();
        for (int i = 0; i < 8; i++)
        {
            float angle = (360.0f / 8.0f) * i;

            Vector3 pos = GetPosition(transform.position, angle);
            Vector3 dir = pos - transform.position;
            dir.Normalize();
            GameObject obj = ObjManager.Call().GetObject("Bullet");
            obj.GetComponent<BulletController>().firePosition = transform.position;
            obj.GetComponent<BulletController>().isMonster = true;
            obj.GetComponent<BulletController>().monsterName = "piko";
            obj.GetComponent<BulletController>().bulletType = "Monster_Bullet";
            obj.GetComponent<BulletController>().fSpeed = 4;
            obj.GetComponent<BulletController>().fAngle = 0;
            obj.GetComponent<BulletController>().dir = Vector2.zero;
            obj.GetComponent<BulletController>().fRange = 20;
            obj.GetComponent<BulletController>().isRightAttack = false;
            obj.transform.position = pos;
            obj.transform.localEulerAngles = Vector3.zero;
            obj.SetActive(true);
            obj.GetComponent<BulletController>().BulletSetting();
            bulletList.Add(obj);
            yield return new WaitForSeconds(0.1f);
        }
        dirs = new Vector3[bulletList.Count];
        for (int i = 0; i < bulletList.Count; i++)
        {
            float angle = (360.0f / 8.0f) * i;
            Vector3 pos = GetPosition(transform.position, angle);
            Vector3 dir = pos - transform.position;
            dir.Normalize();
            dirs[i] = dir;
        }
        float t = 0.0f;
        while (true)
        {
            for (int i = 0; i < bulletList.Count; i++)
            {
                bulletList[i].transform.Translate(dirs[i] * startSpeed * Time.deltaTime);
                Debug.Log(dirs[i]);
            }
            t += Time.deltaTime;
            if (t >= 0.2f) break;
            yield return new WaitForSeconds(Time.deltaTime);
        }
        yield return new WaitForSeconds(1.0f);
        Vector3 dir2 = PDG.Player.instance.transform.position - transform.position;
        dir2.Normalize();
        for (int i = 0; i < bulletList.Count; i++)
        {
            if (!bulletList[i].activeSelf) continue;
            bulletList[i].GetComponent<BulletController>().dir = dir2;
            bulletList[i].GetComponent<BulletController>().BulletFire();
        }
    }
    #region -- Tool --
    public static float GetAngle(Vector3 vStart, Vector3 vEnd)
    {
        Vector3 v = vEnd - vStart;

        return Mathf.Atan2(v.y, v.x) * Mathf.Rad2Deg;
    }
    public static Vector3 GetPosition(Vector3 vStart, float angle)
    {
        float x = Mathf.Cos(Mathf.Deg2Rad * angle) + vStart.x;
        float y = Mathf.Sin(Mathf.Deg2Rad * angle) + vStart.y;

        return new Vector3(x, y, 0);
    }
    #endregion
}
