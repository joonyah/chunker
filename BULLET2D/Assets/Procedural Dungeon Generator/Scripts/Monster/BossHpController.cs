﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BossHpController : MonoBehaviour
{
    public static BossHpController instance;
    [SerializeField] private Text nameText;
    [SerializeField] private Image hpBar;
    [SerializeField] private MOB.MonsterInfo curInfo;
    [SerializeField] private GameObject hpObj;


    private void Awake()
    {
        if (instance == null) instance = this;
    }
    private void Update()
    {
        if(hpObj.activeSelf)
        {
            if (PDG.Player.instance.isDie) hpObj.SetActive(false);
            if (curInfo.curHp <= 0) hpObj.SetActive(false);
            hpBar.fillAmount = curInfo.curHp / curInfo.maxHp;
        }
    }

    public void BossStart(string name, MOB.MonsterInfo _info)
    {
        nameText.text = name;
        curInfo = _info;
        hpBar.fillAmount = curInfo.curHp / curInfo.maxHp;
        hpObj.SetActive(true);
    }
}
