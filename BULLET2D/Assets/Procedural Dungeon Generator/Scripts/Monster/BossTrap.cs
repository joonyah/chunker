﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossTrap : MonoBehaviour
{
    [SerializeField] private string whyName;
    [SerializeField] private GameObject hitEffect;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.layer.Equals(9))
        {
            if (hitEffect && !collision.gameObject.GetComponent<PDG.Player>().isHit && !collision.GetComponent<PDG.Player>().isDie)
            {
                GameObject obj = Instantiate(hitEffect);
                obj.transform.position = collision.transform.position;
                Destroy(obj, 2f);
            }
            collision.gameObject.GetComponent<PDG.Player>().SetDamage(1, LocalizeManager.GetLocalize(whyName), DEATH_TYPE.DEATH_BOSS, "boss_stg");
        }
    }
}
