﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextOpenController : MonoBehaviour
{
    public static TextOpenController instance;
    public Text _text;
    public bool isOpen;
    public GameObject targetObj;
    private void Awake()
    {
        if (instance == null) instance = this;
        _text = GetComponent<Text>();
    }

    public void ShowText(string _cont, Color _color, int _size, GameObject _target = null, float _time = 0.5f, bool _isNotClose = false)
    {
        targetObj = (_target == null ? PDG.Player.instance.gameObject : _target);
        StopAllCoroutines();
        StartCoroutine(TextOpen(_cont, _color, _size, _isNotClose, _time));
    }

    void Update()
    {
        if (isOpen || _text.color.a > 0.0f)
        {
            if (targetObj == null) return;
            Vector3 p = targetObj.transform.position;
            p.y += 0.8f;
            transform.position = Camera.main.WorldToScreenPoint(p);
        }
    }

    IEnumerator TextOpen(string _cont, Color _color, int _size, bool _isNotClose, float _time)
    {
        isOpen = true;
        _text.text = _cont;
        _text.fontSize = _size;
        float a = 1.0f;
        _text.color = new Color(_color.r, _color.g, _color.b, a);
        yield return new WaitForSeconds(_time);
        if(!_isNotClose)
        {
            while (isOpen)
            {
                _text.color = new Color(_color.r, _color.g, _color.b, a);
                a -= Time.deltaTime * 4;
                if (a <= 0.0f) break;
                yield return new WaitForSeconds(Time.deltaTime);
            }
            a = 0.0f;
            _text.color = new Color(_color.r, _color.g, _color.b, a);
            isOpen = false;
        }
        else
        {
            isOpen = false;
        }
    }

    public void TextClose(Color _color)
    {
        _text.color = new Color(_color.r, _color.g, _color.b, 0);
        isOpen = false;
    }
}
