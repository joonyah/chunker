﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ReinforceController : MonoBehaviour
{
    public static ReinforceController instance;
    public bool isOpen = false;
    public GameObject obj;
    public GameObject titleObj;
    public GameObject contObj;
    public Transform scrollView;
    public bool testBtn;
    public Dictionary<string, List<string>> dic = new Dictionary<string, List<string>>();
    public List<GameObject> objList = new List<GameObject>();
    public List<GameObject> titleList = new List<GameObject>();
    [SerializeField] GameObject noticeText;
    public GameObject popUpObj;

    float openTime = 1.0f;
    private void Awake()
    {
        if (instance == null) instance = this;
    }

    private void Update()
    {
        if(testBtn)
        {
            testBtn = false;
            ShowReinforce();
        }

        if (openTime > 0.0f) openTime -= Time.deltaTime;
        if (openTime < 0.0f) noticeText.SetActive(false);

        if (isOpen)
        {
            if ((Input.GetKeyDown(KeyCode.Escape) || Input.GetKeyDown(KeyCode.Backspace)) && popUpObj.activeSelf) popUpObj.SetActive(false);
            else if ((Input.GetKeyDown(KeyCode.Escape) || Input.GetKeyDown(KeyCode.Backspace)) && !popUpObj.activeSelf)
            {
                isOpen = false;
                obj.SetActive(false);
                GetComponent<Image>().enabled = false;
                Time.timeScale = 1;
                PDG.Dungeon.instance.closeCoolTime = 0.0f;
            }

            if (Input.GetKeyDown(KeyCode.R) && !popUpObj.activeSelf && PlayerBuffController.buffList.Count > 0) 
            {
                SoundManager.instance.StartAudio(new string[1] { "reinforce_selected" }, VOLUME_TYPE.EFFECT);
                popUpObj.SetActive(true);
                popUpObj.transform.Find("title").GetComponent<Text>().text = LocalizeManager.GetLocalize("reinforce_reset");
                popUpObj.transform.Find("cont").GetComponent<Text>().text = string.Empty;
                popUpObj.transform.Find("question").gameObject.SetActive(false);
                popUpObj.transform.Find("Btn_0").GetComponent<Button>().onClick.RemoveAllListeners();
                popUpObj.transform.Find("Btn_0").GetComponent<Button>().onClick.AddListener(() => Cancle());
                popUpObj.transform.Find("Btn_1").GetComponent<Button>().onClick.RemoveAllListeners();
                popUpObj.transform.Find("Btn_1").GetComponent<Button>().onClick.AddListener(() => ResetReinforce());
            }
        }
    }
    public void ShowReinforce()
    {
        isOpen = true;
        for (int i = 0; i < titleList.Count; i++)
        {
            GameObject obj = titleList[i];
            titleList.RemoveAt(i);
            Destroy(obj);
            i--;
        }
        for (int i = 0; i < objList.Count; i++)
        {
            GameObject obj = objList[i];
            objList.RemoveAt(i);
            Destroy(obj);
            i--;
        }
        dic.Clear();
        if (dic.Count == 0)
        {
            ReinforceData[] array = SheetManager.Instance.ReinforceDB.dataArray;
            for (int i = 0; i < array.Length; i++)
            {
                if (dic.ContainsKey(array[i].Categoriesid)) dic[array[i].Categoriesid].Add(array[i].ID);
                else
                {
                    dic.Add(array[i].Categoriesid, new List<string>());
                    dic[array[i].Categoriesid].Add(array[i].ID);
                }
            }
            foreach (KeyValuePair<string, List<string>> items in dic)
            {
                GameObject title = Instantiate(titleObj, scrollView);
                string titleName = Array.Find(array, item => item.Categoriesid.Equals(items.Key)).ID;
                titleList.Add(title);
                title.transform.Find("Text").GetComponent<Text>().text = LocalizeManager.GetLocalize(titleName);

                if (titleName.Contains("offensive")) title.transform.Find("Text").GetComponent<Text>().color = Color.red;
                else if (titleName.Contains("strategic")) title.transform.Find("Text").GetComponent<Text>().color = new Color32(180, 86, 255, 255);
                else if (titleName.Contains("survival")) title.transform.Find("Text").GetComponent<Text>().color = Color.green;

                for (int i = 0; i < items.Value.Count; i++)
                {
                    ReinforceData rData = Array.Find(array, item => item.ID.Equals(items.Value[i]));
                    GameObject cont = Instantiate(contObj, scrollView);
                    cont.transform.Find("left").GetComponent<Text>().text = LocalizeManager.GetLocalize(rData.ID + "_maincont");
                    cont.transform.Find("right").GetComponent<Text>().text = LocalizeManager.GetLocalize(rData.ID + "_cont");
                    //cont.GetComponent<Button>().onClick.AddListener(() => SelecteCont(cont, rData));
                    cont.GetComponent<ReinforceItem>().InitSetting(cont, rData);
                    if (rData.ID.Contains("offensive")) cont.transform.Find("left").GetComponent<Text>().color = Color.red;
                    else if (rData.ID.Contains("strategic")) cont.transform.Find("left").GetComponent<Text>().color = new Color32(180, 86, 255, 255);
                    else if (rData.ID.Contains("survival")) cont.transform.Find("left").GetComponent<Text>().color = Color.green;
                    objList.Add(cont);

                    if (PlayerBuffController.buffList.FindIndex(item => item.Equals(items.Value[i])) != -1)
                    {
                        cont.transform.Find("left").GetComponent<Text>().enabled = false;
                        cont.transform.Find("left").Find("Image").gameObject.SetActive(true);
                    }
                }
            }

        }
        obj.SetActive(true);
        GetComponent<Image>().enabled = true;
        Time.timeScale = 0;
    }
    private void ResetReinforce()
    {
        if (Inventorys.Instance.reinforcePoint < 1) return;
        Inventorys.Instance.reinforcePoint -= 1;
        Inventorys.Instance.reinforcePoint += PlayerBuffController.buffList.Count;
        PlayerBuffController.buffList.Clear();
        popUpObj.SetActive(false);
        ShowReinforce();
    }
    public void SelecteCont(GameObject cont)
    {
        SoundManager.instance.StartAudio(new string[1] { "reinforce_selected" }, VOLUME_TYPE.EFFECT);
        for (int i=0; i< objList.Count; i++)
        {
            if (objList[i] == cont)
            {
                objList[i].transform.Find("select").GetComponent<Image>().color = new Color32(0, 0, 0, 0);
                objList[i].GetComponent<ReinforceItem>().isSelected = true;
            }
            else
            {
                objList[i].transform.Find("select").GetComponent<Image>().color = new Color32(0, 0, 0, 100);
                objList[i].GetComponent<ReinforceItem>().isSelected = false;
            }
        }

    }
    public void SelectRight(GameObject cont, ReinforceData _rData)
    {
        if (Inventorys.Instance.reinforcePoint <= 0) return;
        if (PlayerBuffController.buffList.Count >= 3)
        {
            openTime = 1.0f;
            noticeText.SetActive(true);
            return;
        }
        if (!cont.transform.Find("left").GetComponent<Text>().enabled) return;
        SoundManager.instance.StartAudio(new string[1] { "reinforce_selected" }, VOLUME_TYPE.EFFECT);
        popUpObj.SetActive(true);
        popUpObj.transform.Find("title").GetComponent<Text>().text = LocalizeManager.GetLocalize(_rData.ID);
        popUpObj.transform.Find("cont").GetComponent<Text>().text = LocalizeManager.GetLocalize(_rData.ID + "_cont");
        popUpObj.transform.Find("question").gameObject.SetActive(true);
        popUpObj.transform.Find("Btn_0").GetComponent<Button>().onClick.RemoveAllListeners();
        popUpObj.transform.Find("Btn_0").GetComponent<Button>().onClick.AddListener(() => Cancle());
        popUpObj.transform.Find("Btn_1").GetComponent<Button>().onClick.RemoveAllListeners();
        popUpObj.transform.Find("Btn_1").GetComponent<Button>().onClick.AddListener(() => Check(cont, _rData.ID));
    }
    private void Cancle()
    {
        SoundManager.instance.StartAudio(new string[1] { "reinforce_selected" }, VOLUME_TYPE.EFFECT);
        popUpObj.SetActive(false);
    }
    private void Check(GameObject cont, string _id)
    {
        SoundManager.instance.StartAudio(new string[1] { "reinforce_selected" }, VOLUME_TYPE.EFFECT);
        popUpObj.SetActive(false);
        cont.transform.Find("left").GetComponent<Text>().enabled = false;
        cont.transform.Find("left").Find("Image").gameObject.SetActive(true);
        // 배워져라!
        Inventorys.Instance.reinforcePoint--;
        PlayerBuffController.instance.AddBuff(_id);

    }
}
