﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialEvent_stage_01 : MonoBehaviour
{
    public GameObject npcObj;

    public bool isMove;
    public Exit exit;
    public string _type;
    public MOB.Monster monster2;
    public Vector3 mobPosition;
    public bool isThird = false;
    Coroutine t = null;
    public List<GameObject> objList = new List<GameObject>();
    // Start is called before the first frame update
    void Start()
    {
        GameObject.Find("Canvas").transform.Find("TutorialText").gameObject.SetActive(false);
        StartCoroutine(NPCComment());

    }
    IEnumerator NPCComment()
    {
        yield return new WaitForSeconds(1.0f);
        Dialogue.Instance.CommnetSetting("tutorial_" + _type + "_map_npc", 0, null);
        while(true)
        {
            if(isMove)
            {
                npcObj.transform.localPosition = new Vector3(16.5f, 2, 0);
            }

            if(monster2 != null && monster2.isDie)
            {
                GameObject obj = ObjManager.Call().GetObject("Monster");
                obj.transform.position = transform.position + mobPosition;
                obj.SetActive(true);
                obj.GetComponent<MOB.MonsterArggro>().InitSetting(null, new MonstersData {
                    ID = "hoodie_pistol",
                    Name = "복면권총",
                    Type = "normal"
                }, 100, false, "Mob1");
                obj.GetComponent<MOB.MonsterArggro>().SpawnEffectStart();
                monster2 = obj.transform.Find("Monster").GetComponent<MOB.Monster>();
                monster2.isTutorial = true;
                monster2.tutoType = 1;
                monster2.tutoName = "stage2";
            }
            yield return new WaitForSeconds(Time.deltaTime);
        }

    }

}
