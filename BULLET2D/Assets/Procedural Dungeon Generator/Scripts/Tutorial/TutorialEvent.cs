﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialEvent : MonoBehaviour
{
    public bool isStart = false;
    public bool isCharacterChange = false;
    public SpriteRenderer changeSpriteRender;
    public Sprite change_1;
    public Sprite change_2;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(isStart)
        {
            if (Dialogue.Instance.tutorialCharacterChange && !isCharacterChange) 
            {
                isCharacterChange = true;
                changeSpriteRender.sprite = change_1;
                StartCoroutine(changeSprite());
            }
        }
    }

    IEnumerator changeSprite()
    {
        yield return new WaitForSeconds(1.0f);
        changeSpriteRender.sprite = change_2;
    } 
}
