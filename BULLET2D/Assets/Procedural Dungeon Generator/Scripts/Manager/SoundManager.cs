﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[Serializable] public enum VOLUME_TYPE { MASTER, BGM, EFFECT, AMBIENT };

public struct AudioDic
{
    public AudioSource _audio;
    public VOLUME_TYPE _vType;
}

public class SoundManager : MonoBehaviour
{
    public static SoundManager instance;

    public List<AudioDic> audioList = new List<AudioDic>();

    public static float MasterVolume = 0.5f;
    public static float BGMVolume = 1.0f;
    public static float EffectVolume = 1.0f;
    public static float AmbientVolume = 1.0f;
    public static bool isMute = false;

    [Header("Slider")]
    public Slider mSlider;
    public Slider bSlider;
    public Slider eSlider;
    public Slider aSlider;
    [SerializeField] private Sprite checkSprite;
    [SerializeField] private Sprite unCheckSprite;
    [SerializeField] private Image uiImage;
    [SerializeField] private GameObject subObj;

    private void Awake()
    {
        if (instance == null) instance = this;
    }
    private void Start()
    {
        VolumeSave[] v = SaveAndLoadManager.instance.LoadVolume();
        if(v != null)
        {
            for (int i = 0; i < v.Length; i++)
            {
                if (v[i]._type == 0 && mSlider) mSlider.value = v[i]._value;
                else if (v[i]._type == 1 && bSlider) bSlider.value = v[i]._value;
                else if (v[i]._type == 2 && eSlider) eSlider.value = v[i]._value;
                else if (v[i]._type == 3 && aSlider) aSlider.value = v[i]._value;
                else if (v[i]._type == 4) isMute = (v[i]._value == 1 ? true : false);
            }
        }

        if (isMute && uiImage) uiImage.sprite = checkSprite;
        else if (uiImage) uiImage.sprite = unCheckSprite;
    }
    public void StopBGM()
    {
        for (int j = 0; j < audioList.Count; j++)
        {
            if (audioList[j]._vType == VOLUME_TYPE.BGM)
            {
                audioList[j]._audio.Stop();
            }
        }
    }
    public void StopAllSound()
    {
        for (int j = 0; j < audioList.Count; j++)
        {
            audioList[j]._audio.Stop();
        }
    }
    public void StopAudio(string[] _names)
    {
        for (int i = 0; i < _names.Length; i++)
        {
            string name = _names[i];
            if (name == null) continue;
            if (name.Equals(string.Empty)) continue;

            if (_names[i].LastIndexOf('/') != -1)
            {
                name = _names[i].Substring(_names[i].LastIndexOf('/') + 1, _names[i].Length - _names[i].LastIndexOf('/') - 1);
            }


            for (int j = 0; j < audioList.Count; j++)
            {
                if (audioList[j]._audio == null) continue;
                if (audioList[j]._audio.clip == null) continue;
                if (audioList[j]._audio.clip.name.Equals(name))
                {
                    audioList[j]._audio.Stop();
                    break;
                }
            }
        }
    }
    public void StartAudio(string[] _names, VOLUME_TYPE _type, GameObject target = null, bool isLoop = false)
    {
        if(_type == VOLUME_TYPE.BGM)
        {
            string name = _names[0];
            if (_names[0].LastIndexOf('/') != -1)
            {
                name = _names[0].Substring(_names[0].LastIndexOf('/') + 1, _names[0].Length - _names[0].LastIndexOf('/') - 1);
            }
            for (int j = 0; j < audioList.Count; j++)
            {
                if (audioList[j]._vType  == VOLUME_TYPE.BGM)
                {
                    if(!audioList[j]._audio.clip.name.Equals(name))
                        audioList[j]._audio.Stop();
                    break;
                }
            }
        }

        for (int i = 0; i < _names.Length; i++)
        {
            AudioClip clip = Resources.Load<AudioClip>("Sound/" + _names[i]);
            if (clip == null) continue;
            string name = _names[i];
            if (_names[i].LastIndexOf('/') != -1)
            {
                name = _names[i].Substring(_names[i].LastIndexOf('/') + 1, _names[i].Length - _names[i].LastIndexOf('/') - 1);
            }
            // 현재 사운드 트랙이 하나도 없으면 새로 만들어줌
            if (audioList.Count <= i)
            {
                AudioSource source = subObj.AddComponent<AudioSource>();
                source.priority = 0;
                audioList.Add(new AudioDic
                {
                    _audio = source,
                    _vType = _type
                });
                source.loop = isLoop;
                source.clip = clip;
                VolumeSetting(source, _type);
                if (target != null)
                {
                    float dis = Vector3.Distance(PDG.Player.instance.transform.position, target.transform.position);
                    if (dis > 25) source.volume = 0;
                    if (isMute)
                    {
                        source.volume = 0;
                    }
                    else
                    {
                        float tVolume = _type == VOLUME_TYPE.EFFECT ? EffectVolume : AmbientVolume;
                        float t = tVolume / 25;
                        source.volume = tVolume - (dis * t);
                    }
                }
                if (isLoop) source.Play();
                else if (clip != null) source.PlayOneShot(clip);
            }
            else
            {
                // 루프를 돌아야하는데 현재 돌고 있는 루프중에 이미 사용 중이라면 PASS
                if (isLoop)
                {
                    for (int j = 0; j < audioList.Count; j++)
                    {
                        if (audioList[j]._audio.clip.name.Equals(name))
                        {
                            audioList[j]._audio.loop = isLoop;
                            if (!audioList[j]._audio.isPlaying) audioList[j]._audio.Play();
                            return;
                        }
                    }
                }

                bool end = false;
                // 현재 가진 트랙 중 사용 중이지 않는 트랙이 있다면 찾아서 사용해줌
                for (int j = 0; j < audioList.Count; j++)
                {
                    if (!audioList[j]._audio.isPlaying)
                    {
                        audioList[j]._audio.loop = isLoop;
                        audioList[j]._audio.clip = clip;
                        AudioDic t = audioList[j];
                        t._vType = _type;
                        audioList[j] = t;
                        VolumeSetting(audioList[j]._audio, audioList[j]._vType);
                        if (target != null)
                        {
                            float dis = Vector3.Distance(PDG.Player.instance.transform.position, target.transform.position);
                            if (dis > 25) audioList[j]._audio.volume = 0;
                            if (isMute)
                            {
                                audioList[j]._audio.volume = 0;
                            }
                            else
                            {
                                float tVolume = _type == VOLUME_TYPE.EFFECT ? EffectVolume : AmbientVolume;
                                float tt = tVolume / 25;
                                audioList[j]._audio.volume = tVolume - (dis * tt);
                            }
                        }
                        if (isLoop) audioList[j]._audio.Play();
                        else if (clip != null) audioList[j]._audio.PlayOneShot(clip);
                        end = true;
                        break;
                    }
                }

                // 모두 사용중이라면 새로 하나 생성
                if (!end)
                {
                    AudioSource source = subObj.AddComponent<AudioSource>();
                    source.priority = 0;
                    audioList.Add(new AudioDic
                    {
                        _audio = source,
                        _vType = _type
                    });
                    source.loop = isLoop;
                    source.clip = clip;
                    VolumeSetting(source, _type);
                    if (target != null)
                    {
                        float dis = Vector3.Distance(PDG.Player.instance.transform.position, target.transform.position);
                        if (dis > 25) source.volume = 0;
                        if (isMute)
                        {
                            source.volume = 0;
                        }
                        else
                        {
                            float tVolume = _type == VOLUME_TYPE.EFFECT ? EffectVolume : AmbientVolume;
                            float t = tVolume / 25;
                            source.volume = tVolume - (dis * t);
                        }
                    }
                    if (isLoop) source.Play();
                    else if (clip != null) source.PlayOneShot(clip);
                }
            }
        }
    }
    private void VolumeSetting(AudioSource audioSource, VOLUME_TYPE type)
    {
        float _volume = 1.0f;

        MasterVolume = mSlider.value;
        BGMVolume = MasterVolume * bSlider.value;
        EffectVolume = MasterVolume * eSlider.value;
        AmbientVolume = MasterVolume * aSlider.value;
        if (isMute)
        {
            _volume = 0.0f;
        }
        else
        {
            switch (type)
            {
                case VOLUME_TYPE.BGM: _volume = BGMVolume; break;
                case VOLUME_TYPE.EFFECT: _volume = EffectVolume; break;
                case VOLUME_TYPE.AMBIENT: _volume = AmbientVolume; break;
            }
        }
        audioSource.volume = _volume;
    }
    public void Click()
    {
        StartAudio(new string[1] { "Click_0" }, VOLUME_TYPE.EFFECT);
    }
    public void MenuClick()
    {
        StartAudio(new string[1] { "menu_click" }, VOLUME_TYPE.EFFECT);
    }
    public void MuteChange(Image _image)
    {
        isMute = !isMute;
        if (isMute) _image.sprite = checkSprite;
        else _image.sprite = unCheckSprite;
        for (int i = 0; i < audioList.Count; i++)
        {
            VolumeSetting(audioList[i]._audio, audioList[i]._vType);
        }
        SaveAndLoadManager.instance.SaveVolume(new VolumeSave { _type = 4, _value = (isMute ? 1 : 0) });
    }
    public void VolumeChange(int _type)
    {
        VolumeSave saveData = new VolumeSave();
        saveData._type = _type;
        if (_type == 0) saveData._value = mSlider.value;
        else if (_type == 1) saveData._value = bSlider.value;
        else if (_type == 2) saveData._value = eSlider.value;
        else if (_type == 3) saveData._value = aSlider.value;
        SaveAndLoadManager.instance.SaveVolume(saveData);
        for (int i = 0; i < audioList.Count; i++)
        {
            VolumeSetting(audioList[i]._audio, audioList[i]._vType);
        }
    }
    public void VolumeChange(int _type, float _value)
    {
        VolumeSave saveData = new VolumeSave();
        saveData._type = _type;
        if (_type == 0) saveData._value = mSlider.value = _value;
        else if (_type == 1) saveData._value = bSlider.value = _value;
        else if (_type == 2) saveData._value = eSlider.value = _value;
        else if (_type == 3) saveData._value = aSlider.value = _value;
        SaveAndLoadManager.instance.SaveVolume(saveData);
        for (int i = 0; i < audioList.Count; i++)
        {
            VolumeSetting(audioList[i]._audio, audioList[i]._vType);
        }
    }
    public void QuitGame()
    {
        Application.Quit();
    }
    internal void AllMute(bool _mute)
    {
        for (int i = 0; i < audioList.Count; i++)
        {
            if (_mute)
                audioList[i]._audio.Pause();
            else
                audioList[i]._audio.UnPause();
        }
    }
}
