﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjManager : MonoBehaviour {

    // 싱글톤
    static ObjManager st;
    public static ObjManager Call() { return st; }

    public delegate GameObject SpawnDelegate(Vector3 position, System.Guid assetId);
    public delegate void UnSpawnDelegate(GameObject spawned);

    Dictionary<string, List<GameObject>> objList = new Dictionary<string, List<GameObject>>();
    Dictionary<string, System.Guid> objAssetId = new Dictionary<string, System.Guid>();

    void Awake() { st = this; }
    // 게임종료 후 메모리 날려버림.
    void OnDestroy()
    {
        MemoryDelete();
        st = null;
    }

    public GameObject[] Origin;         // 프리팹 원본.
    public int startNum = 10;
    public List<GameObject> Manager;    // 생성된 객체들을 저장할 리스트.

    private void Start()
    {
        ObjManagerStart();
    }
    public void ObjManagerStart()
    {
        for (int i = 0; i < Origin.Length; i++)
        {
            int n = startNum;
            if (Origin[i].name.Equals("Monster"))
                n *= 4;
            SetObject(Origin[i], n);
        }
    }

    // 오브젝트를 받아 생성. (생성한 원본 오브젝트, 생성할 갯수, 생성할 객체의 이름)
    public void SetObject(GameObject _Obj, int _Count)
    {
        for (int i = 0; i < _Count; i++)
        {
            GameObject obj;

            obj = Instantiate(_Obj, Vector3.zero, Quaternion.identity);
            obj.transform.name = _Obj.name;

            obj.transform.localPosition = Vector3.zero;     // 위치를 정한다.
            obj.SetActive(false);                           // 객체를 비활성화.
            obj.transform.parent = transform;               // 매니저 객체의 자식으로.
            Manager.Add(obj);                               // 리스트에 저장.
        }
    }
    public GameObject SetObject(GameObject _Obj)
    {
        GameObject obj;
        obj = Instantiate(_Obj, Vector3.zero, Quaternion.identity);
        obj.transform.name = _Obj.name;
        obj.transform.localPosition = Vector3.zero;     // 위치를 정한다.
        obj.SetActive(false);                           // 객체를 비활성화.
        obj.transform.parent = transform;               // 매니저 객체의 자식으로.
        Manager.Add(obj);                               // 리스트에 저장.
        return obj;
    }

    // 필요한 오브젝트를 찾아 반환.
    public GameObject GetObject(string _Name)
    {
        if (Manager == null)
            return null;

        int Count = Manager.Count;
        for (int i = 0; i < Count; i++)
        {
            // 이름이 같지 않으면. 
            if (Manager[i] == null) 
            {
                Manager.RemoveAt(i);
                i--;
                Count = Manager.Count;
                continue;
            }
            if (_Name != Manager[i].name)
                continue;

            GameObject Obj = Manager[i];

            // 활성화가 되어있다면.
            if (Obj.activeSelf == true)
            {
                // 리스트의 마지막까지 돌았지만 모든 객체가 사용중이라면.
                if (i == Count - 1)
                {
                    SetObject(Obj, 1);

                    return Manager[i + 1];
                }
                continue;
            }
            return Manager[i];
        }
        for (int i = 0; i < Count; i++)
        {
            // 이름이 같지 않으면. 
            if (_Name != Manager[i].name)
                continue;

            GameObject Obj = Manager[i];

            SetObject(Obj, 1);

            return Manager[i + 1];
        }
        // 만들어 진게 없으면
        for (int i = 0; i < Origin.Length; i++)
        {
            // 이름이 같지 않으면. 
            if (_Name != Origin[i].name)
                continue;
            GameObject Obj = Origin[i];
            return SetObject(Obj);
        }
        return null;
    }

    // 메모리 삭제.
    public void MemoryDelete()
    {
        if (Manager == null)
            return;

        int Count = Manager.Count;

        for (int i = 0; i < Count; i++)
        {
            GameObject obj = Manager[i];
            GameObject.Destroy(obj);
        }
        Manager = null;
    }

    public GameObject GetFromPool(Vector3 position)
    {
        foreach (var obj in Manager)
        {
            if (!obj.activeInHierarchy)
            {
                obj.transform.position = position;
                obj.SetActive(true);
                return obj;
            }
        }
        Debug.LogError("Could not grab GameObject from pool, nothing available");
        return null;
    }

    public GameObject GetFromPool(Vector3 position, List<GameObject> _list)
    {
        foreach (var obj in _list)
        {
            if (!obj.activeInHierarchy)
            {
                obj.transform.position = position;
                obj.SetActive(true);
                return obj;
            }
        }
        Debug.LogError("Could not grab GameObject from pool, nothing available");
        return null;
    }

    public GameObject SpawnObject(Vector3 position, System.Guid assetId)
    {
        return GetFromPool(position);
    }

    public void UnSpawnObject(GameObject spawned)
    {
        Debug.Log("Re-pooling GameObject " + spawned.name);
        spawned.SetActive(false);
    }
}
