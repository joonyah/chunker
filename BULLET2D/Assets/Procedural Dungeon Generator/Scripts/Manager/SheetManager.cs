﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SheetManager : MonoBehaviour
{
    public static SheetManager Instance;

    public Items ItemsDB;
    public Stage StageDB;
    public Monsters MonsterDB;
    public Merchant MerchantDB;
    public NPCLists NPCListsDB;
    public UnlockLists UnlockListsDB;
    public Price PriceDB;
    public Reinforce ReinforceDB;
    public Stats StatsDB;
    public Localize LocalDB;
    public Make MakeDB;
    public Mail MailDB;
    public Totem TotemDB;
    public Mercenary MercenaryDB;
    public Achievements AchievementsDB;
    public ChestPer ChestPerDB;
    public Manufacture ManufactureDB;
    public Enhance EnhanceDB;
    private void Awake()
    {
        if (Instance == null) Instance = this;
    }
}
