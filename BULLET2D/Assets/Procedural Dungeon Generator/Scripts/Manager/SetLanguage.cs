﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SetLanguage : MonoBehaviour
{
    public string _id;
    private Text _text;
    private LocalizeData myData;
    private void Awake()
    {
        _text = GetComponent<Text>();
    }
    private void Start()
    {
        myData = Array.Find(SheetManager.Instance.LocalDB.dataArray, item => item.ID.Equals(_id));
    }
    public void InitSetting(string _id2)
    {
        _id = _id2;
        myData = Array.Find(SheetManager.Instance.LocalDB.dataArray, item => item.ID.Equals(_id2));
    }
    private void Update()
    {
        if (_id.Equals(string.Empty)) return;
        if (LocalizeManager.lang == LANGUAGE_SHIFTRICK.ENG && !_text.text.Equals(myData.ENG)) _text.text = myData.ENG;
        else if (LocalizeManager.lang == LANGUAGE_SHIFTRICK.KOR && !_text.text.Equals(myData.KOR)) _text.text = myData.KOR;
        else if (LocalizeManager.lang == LANGUAGE_SHIFTRICK.CHN_S && !_text.text.Equals(myData.CHNS)) _text.text = myData.CHNS;
        else if (LocalizeManager.lang == LANGUAGE_SHIFTRICK.CHN_T && !_text.text.Equals(myData.CHNT)) _text.text = myData.CHNT;
        else if (LocalizeManager.lang == LANGUAGE_SHIFTRICK.RU && !_text.text.Equals(myData.RU)) _text.text = myData.RU;
        else if (LocalizeManager.lang == LANGUAGE_SHIFTRICK.JA && !_text.text.Equals(myData.JA)) _text.text = myData.JA;
    }

}
