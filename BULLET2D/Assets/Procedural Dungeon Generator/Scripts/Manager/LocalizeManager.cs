﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[Serializable] public enum LANGUAGE_SHIFTRICK { KOR, ENG, CHN_S, CHN_T, RU, JA }

public class LocalizeManager : MonoBehaviour
{
    public static LocalizeManager instance;
    public static LANGUAGE_SHIFTRICK lang = LANGUAGE_SHIFTRICK.KOR;

    public LANGUAGE_SHIFTRICK[] langList;
    public string[] langListText;
    public int nowLang = 0;
    [SerializeField] private Image[] langSelectebars;
    private void Awake()
    {
        if (instance == null) instance = this;
    }
    //private void OnGUI()
    //{
    //    GUILayout.BeginArea(new Rect(20, 140, 1200, 1200));
    //    GUILayout.Label(Application.systemLanguage.ToString());
    //    GUILayout.Space(20);
    //    GUILayout.EndArea();
    //}
    private void Start()
    {
        int t = SaveAndLoadManager.instance.LoadItem("LANGUAGE");
        if (Application.systemLanguage == SystemLanguage.Korean)
        {
            if (t == -1) nowLang = 0;
            else nowLang = t;
        }
        else if (Application.systemLanguage == SystemLanguage.Chinese)
        {
            if (t == -1) nowLang = 3;
            else nowLang = t;
        }
        else if (Application.systemLanguage == SystemLanguage.ChineseTraditional)
        {
            if (t == -1) nowLang = 2;
            else nowLang = t;
        }
        else
        {
            if (t == -1) nowLang = 1;
            else nowLang = t;
        }
        lang = langList[nowLang];
    }
    public static string GetLocalize(string _id)
    {
        LocalizeData lData = Array.Find(SheetManager.Instance.LocalDB.dataArray, item => item.ID.Equals(_id));
        if (lData == null) return string.Empty;

        if (lang == LANGUAGE_SHIFTRICK.ENG) return lData.ENG;
        else if (lang == LANGUAGE_SHIFTRICK.KOR) return lData.KOR;
        else if (lang == LANGUAGE_SHIFTRICK.CHN_S) return lData.CHNS;
        else if (lang == LANGUAGE_SHIFTRICK.CHN_T) return lData.CHNT;
        else if (lang == LANGUAGE_SHIFTRICK.JA) return lData.JA;
        else if (lang == LANGUAGE_SHIFTRICK.RU) return lData.RU;

        return string.Empty;
    }
    public void LanguageChange(bool isNext)
    {
        if (isNext) nowLang++;
        else nowLang--;
        if (nowLang >= langList.Length) nowLang = 0;
        else if (nowLang < 0) nowLang = langList.Length - 1;
        lang = langList[nowLang];
        SaveAndLoadManager.instance.SaveItem(new ItemSave
        {
            _id = "LANGUAGE",
            _nCount = nowLang
        }, true);
        if (Inventorys.Instance != null) Inventorys.Instance.LocaliChange();
    }
    public void LanguageChange(int _type)
    {
        if (_type > 4) return;
        nowLang = _type;
        lang = langList[_type];
        SaveAndLoadManager.instance.SaveItem(new ItemSave
        {
            _id = "LANGUAGE",
            _nCount = _type
        }, true);
        if (Inventorys.Instance != null) Inventorys.Instance.LocaliChange();
    }
    public void SelectBarChange(Image _image)
    {
        for (int i = 0; i < langSelectebars.Length; i++)
        {
            if(langSelectebars[i] == _image)
            {
                langSelectebars[i].enabled = true;
            }
            else
            {
                langSelectebars[i].enabled = false;
            }
        }
    }
}
