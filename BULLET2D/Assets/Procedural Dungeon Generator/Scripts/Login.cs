﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Login : MonoBehaviour
{
    public bool isDemo = false;
    public SpriteRenderer logoSprite;
    public Text pressKey;
    public GameObject textButton;
    public GameObject textButton2;
    bool isStart = false;
    public int pressKeySpeed = 5;

    [Header("Option")]
    [SerializeField] private GameObject optionCanvas;
    [SerializeField] private GameObject gameplayCanvas;
    [SerializeField] private GameObject videoCanvas;
    [SerializeField] private GameObject audioCanvas;
    [SerializeField] private GameObject languageCanvas;
    [SerializeField] private GameObject keySettingCanvas;

    [SerializeField] private Image[] SettingSelectImages;
    [SerializeField] private int nowSettingSelectImageNum;
    [SerializeField] private int nowSelectLoginNum = 0;
    [SerializeField] private Text[] LoginMenus;
    [SerializeField] private Text[] LoginMenus2;
    [SerializeField] private int optionOcount = 4;
    [SerializeField] private int optionGcount = 3;
    [SerializeField] private int optionVcount = 3;
    [SerializeField] private int optionAcount = 3;
    [SerializeField] private int optionLcount = 3;
    [SerializeField] private int optionKcount = 3;
    [SerializeField] private float closeCoolTime = 0.0f;

    [SerializeField] private int settingTrigger = 0;
    private int nowCrossHairNum = 4;
    [SerializeField] private Sprite[] crossHairSprites;
    [SerializeField] private Image crossHairImage_UI;
    [SerializeField] private Image crossHairImage;

    [SerializeField] private bool isFullScreenMode = false;
    [SerializeField] private Sprite _checkSprite;
    [SerializeField] private Sprite _unCheckSprite;
    [SerializeField] private Image window_Option;
    [SerializeField] private Image full_Option;
    public Image vSync_Option;
    public Text ui_FreamText;
    public bool isVsyncMode = false;
    [SerializeField] private GameObject thankpanel;
    [SerializeField] private float moveSpeed;
    [SerializeField] private GameObject loadObj;
    public Image UI_EscPause;
    public Image[] CameraShakerLevelImage;
    public int nCameraShakerLevel;
    public bool isEscPause;
    bool isThanks = false;
    [SerializeField] private PostProcessVolume pVolume;
    [SerializeField] private ChromaticAberration pAberration;
    [SerializeField] private float bounsSpeed = 1.0f;

    public List<string> joystricks = new List<string>();
    public bool isJoystrick = false;
    [SerializeField] private Sprite[] joyKeySprites;
    [SerializeField] private bool isChangeKey = false;
    [SerializeField] private int nowKeySettingNum = 0;
    [SerializeField] private GameObject AnyKeyObj;
    [SerializeField] private GameObject KeyboardPanel;
    [SerializeField] private GameObject JoystickPanel;
    [SerializeField] private Text KeyChangeGuideText;
    [SerializeField] private float logoSpeed = 3.0f;
    bool isPrev = false;
    private void Awake()
    {
        //QualitySettings.vSyncCount = 0;
        Application.targetFrameRate = 60;
    }
    private void Start()
    {
        VolumeSave[] v = SaveAndLoadManager.instance.LoadVolume();
        if(v != null)
        {
            for (int i = 0; i < v.Length; i++)
            {
                if (v[i]._type == 5) isFullScreenMode = (v[i]._value == 1 ? true : false);
                else if (v[i]._type == 7) isVsyncMode = (v[i]._value == 1 ? true : false);
            }
        }
        else
        {
            isFullScreenMode = false;
        }

        if (isFullScreenMode)
        {
            full_Option.sprite = _checkSprite;
            window_Option.sprite = _unCheckSprite;
        }
        else
        {
            full_Option.sprite = _unCheckSprite;
            window_Option.sprite = _checkSprite;
        }
        if (isVsyncMode)
        {
            QualitySettings.vSyncCount = 1;
            vSync_Option.sprite = _checkSprite;
        }
        else
        {
            QualitySettings.vSyncCount = 0;
            vSync_Option.sprite = _unCheckSprite;
        }
        SoundManager.instance.StartAudio(new string[2] { "Login/loginBGM", "loginmain" }, VOLUME_TYPE.BGM, null, true);
        int camShakerLevel = SaveAndLoadManager.instance.LoadItem("OPTION_CAMERA_SHAKER");
        if (camShakerLevel == -1) nCameraShakerLevel = 2;
        else nCameraShakerLevel = camShakerLevel;
        for (int i = 0; i < CameraShakerLevelImage.Length; i++)
        {
            if (i == nCameraShakerLevel) CameraShakerLevelImage[i].sprite = _checkSprite;
            else CameraShakerLevelImage[i].sprite = _unCheckSprite;
        }

        int t = SaveAndLoadManager.instance.LoadItem("ESC_PAUSE");
        if (t == -1) isEscPause = true;
        else isEscPause = System.Convert.ToBoolean(t);

        if (isEscPause) UI_EscPause.sprite = _checkSprite;
        else UI_EscPause.sprite = _unCheckSprite;

        int loadFream = SaveAndLoadManager.instance.LoadItem("FREAM_RATE");
        if (loadFream == -1)
        {
            loadFream = 60;
            SaveAndLoadManager.instance.SaveItem(new ItemSave { _id = "FREAM_RATE", _nCount = 60 }, true);
        }
        Application.targetFrameRate = loadFream;

        float vvv = ((float)loadFream - 30f) / 90f;
        ui_FreamText.transform.parent.transform.Find("SelectedBar_Fream").Find("Scrollbar").GetComponent<Scrollbar>().value = vvv;
        if (isVsyncMode)
        {
            ui_FreamText.text = LocalizeManager.GetLocalize("ui_fream") + "\n" + LocalizeManager.GetLocalize("ui_vsync_use");
        }
        else
        {
            ui_FreamText.text = LocalizeManager.GetLocalize("ui_fream") + "\n(" + loadFream + "FPS)";
        }

        nowCrossHairNum = SaveAndLoadManager.instance.LoadItem("UI_CrossHair");
        if (nowCrossHairNum != -1)
        {
            crossHairImage.sprite = crossHairSprites[nowCrossHairNum];
            crossHairImage_UI.sprite = crossHairSprites[nowCrossHairNum];
        }
        else
        {
            nowCrossHairNum = 0;
            crossHairImage.sprite = crossHairSprites[0];
            crossHairImage_UI.sprite = crossHairSprites[0];
        }

        loadObj.SetActive(SaveAndLoadManager.instance.GameCheck());
        StartCoroutine(LogoOn());
        StartCoroutine(MainBouns());


    }
    IEnumerator MainBouns()
    {
        pVolume.profile.TryGetSettings(out pAberration);
        float a = 0.0f;
        bool isPlus = true;
        while(true)
        {
            if(isPlus)
            {
                a += Time.deltaTime * bounsSpeed;
                if (a >= 0.25f)
                {
                    a = 0.25f;
                    isPlus = !isPlus;
                }
            }
            else
            {
                a -= Time.deltaTime * bounsSpeed;
                if (a <= 0.0f)
                {
                    a = 0.0f;
                    isPlus = !isPlus;
                }
            }
            pAberration.intensity.value = a;
            yield return new WaitForSeconds(Time.deltaTime);
        }
    }
    
    private void Update()
    {
        joystricks = Input.GetJoystickNames().ToList();
        for (int i = 0; i < joystricks.Count; i++)
        {
            if (joystricks[i].Equals(string.Empty))
            {
                joystricks.RemoveAt(i);
                i--;
            }
        }

        //컨트롤러 전환
        GetKeyDownCheck((keydown) =>
        {
            if (keydown)
            {
                //키보드로 전환
                isJoystrick = false;

                crossHairImage.gameObject.SetActive(true);
                JoystickPanel.SetActive(false);
                KeyboardPanel.SetActive(true);
            }
            else
            {
                //조이스틱 전환
                if (joystricks.Count > 0)
                {
                    isJoystrick = true;

                    crossHairImage.gameObject.SetActive(false);
                    JoystickPanel.SetActive(true);
                    KeyboardPanel.SetActive(false);
                }
            }
        });

        closeCoolTime += Time.deltaTime;
        if (isChangeKey)
        {
            if (isJoystrick)
            {
                if (joystricks[0].Contains("DS4") || joystricks[0].Contains("PC") || joystricks[0].Contains("Wireless"))
                {
                    float joyCrossX = Input.GetAxisRaw("PS4_crossX");
                    float joyCrossY = Input.GetAxisRaw("PS4_crossY");
                    if (joyCrossY > 0 && closeCoolTime > 0.2f)
                    {
                        if (SaveAndLoadManager.instance.SaveKey(KeyCode.Joystick6Button0, nowKeySettingNum, false))
                            KeyChangeGuideText.text = "";
                        else
                            KeyChangeGuideText.text = LocalizeManager.GetLocalize("ui_keysetting_overlabkey");
                        closeCoolTime = 0.0f;
                        isChangeKey = false;
                        AnyKeyObj.SetActive(false);
                        return;
                    }
                    if (joyCrossY < 0 && closeCoolTime > 0.2f)
                    {
                        if (SaveAndLoadManager.instance.SaveKey(KeyCode.Joystick6Button1, nowKeySettingNum, false))
                            KeyChangeGuideText.text = "";
                        else
                            KeyChangeGuideText.text = LocalizeManager.GetLocalize("ui_keysetting_overlabkey");
                        closeCoolTime = 0.0f;
                        isChangeKey = false;
                        AnyKeyObj.SetActive(false);
                        return;
                    }
                    if (joyCrossX > 0 && closeCoolTime > 0.2f)
                    {
                        if (SaveAndLoadManager.instance.SaveKey(KeyCode.Joystick6Button3, nowKeySettingNum, false))
                            KeyChangeGuideText.text = "";
                        else
                            KeyChangeGuideText.text = LocalizeManager.GetLocalize("ui_keysetting_overlabkey");
                        closeCoolTime = 0.0f;
                        isChangeKey = false;
                        AnyKeyObj.SetActive(false);
                        return;
                    }
                    if (joyCrossX < 0 && closeCoolTime > 0.2f)
                    {
                        if (SaveAndLoadManager.instance.SaveKey(KeyCode.Joystick6Button4, nowKeySettingNum, false))
                            KeyChangeGuideText.text = "";
                        else
                            KeyChangeGuideText.text = LocalizeManager.GetLocalize("ui_keysetting_overlabkey");
                        closeCoolTime = 0.0f;
                        isChangeKey = false;
                        AnyKeyObj.SetActive(false);
                        return;
                    }
                }
                else
                {
                    float joyCrossX = Input.GetAxisRaw("Horizontal_cross");
                    float joyCrossY = Input.GetAxisRaw("Vertical_cross");
                    float JoyStrickTrigger = Input.GetAxisRaw("JoyStrickTrigger");

                    if (joyCrossY > 0 && closeCoolTime > 0.2f)
                    {
                        if (SaveAndLoadManager.instance.SaveKey(KeyCode.Joystick6Button0, nowKeySettingNum, false))
                            KeyChangeGuideText.text = "";
                        else
                            KeyChangeGuideText.text = LocalizeManager.GetLocalize("ui_keysetting_overlabkey");
                        closeCoolTime = 0.0f;
                        isChangeKey = false;
                        AnyKeyObj.SetActive(false);
                        return;
                    }
                    if (joyCrossY < 0 && closeCoolTime > 0.2f)
                    {
                        if (SaveAndLoadManager.instance.SaveKey(KeyCode.Joystick6Button1, nowKeySettingNum, false))
                            KeyChangeGuideText.text = "";
                        else
                            KeyChangeGuideText.text = LocalizeManager.GetLocalize("ui_keysetting_overlabkey");
                        closeCoolTime = 0.0f;
                        isChangeKey = false;
                        AnyKeyObj.SetActive(false);
                        return;
                    }
                    if (joyCrossX > 0 && closeCoolTime > 0.2f)
                    {
                        if (SaveAndLoadManager.instance.SaveKey(KeyCode.Joystick6Button3, nowKeySettingNum, false))
                            KeyChangeGuideText.text = "";
                        else
                            KeyChangeGuideText.text = LocalizeManager.GetLocalize("ui_keysetting_overlabkey");
                        closeCoolTime = 0.0f;
                        isChangeKey = false;
                        AnyKeyObj.SetActive(false);
                        return;
                    }
                    if (joyCrossX < 0 && closeCoolTime > 0.2f)
                    {
                        if (SaveAndLoadManager.instance.SaveKey(KeyCode.Joystick6Button4, nowKeySettingNum, false))
                            KeyChangeGuideText.text = "";
                        else
                            KeyChangeGuideText.text = LocalizeManager.GetLocalize("ui_keysetting_overlabkey");
                        closeCoolTime = 0.0f;
                        isChangeKey = false;
                        AnyKeyObj.SetActive(false);
                        return;
                    }
                    if (JoyStrickTrigger > 0 && closeCoolTime > 0.2f)
                    {
                        if (SaveAndLoadManager.instance.SaveKey(KeyCode.Joystick7Button0, nowKeySettingNum, false))
                            KeyChangeGuideText.text = "";
                        else
                            KeyChangeGuideText.text = LocalizeManager.GetLocalize("ui_keysetting_overlabkey");
                        closeCoolTime = 0.0f;
                        isChangeKey = false;
                        AnyKeyObj.SetActive(false);
                        return;
                    }
                    if (JoyStrickTrigger < 0 && closeCoolTime > 0.2f)
                    {
                        if (SaveAndLoadManager.instance.SaveKey(KeyCode.Joystick7Button1, nowKeySettingNum, false))
                            KeyChangeGuideText.text = "";
                        else
                            KeyChangeGuideText.text = LocalizeManager.GetLocalize("ui_keysetting_overlabkey");
                        closeCoolTime = 0.0f;
                        isChangeKey = false;
                        AnyKeyObj.SetActive(false);
                        return;
                    }
                }
                if (Input.anyKeyDown && closeCoolTime > 0.2f)
                {
                    foreach (KeyCode _key in (KeyCode[])System.Enum.GetValues(typeof(KeyCode)))
                    {
                        if (_key.ToString().Contains("Joystick1")) continue;
                        if (_key.ToString().Contains("Joystick2")) continue;
                        if (_key.ToString().Contains("Joystick3")) continue;
                        if (_key.ToString().Contains("Joystick4")) continue;
                        if (_key.ToString().Contains("Joystick5")) continue;
                        if (Input.GetKeyDown(_key))
                        {
                            //Debug.Log(_key.ToString());
                            if (CheckingKeyChange(_key))
                            {
                                if ((joystricks[0].Contains("DS4") || joystricks[0].Contains("PC") || joystricks[0].Contains("Wireless")) && _key == KeyCode.JoystickButton6)
                                {
                                    if (SaveAndLoadManager.instance.SaveKey(KeyCode.Joystick7Button1, nowKeySettingNum, false))
                                        KeyChangeGuideText.text = "";
                                    else
                                        KeyChangeGuideText.text = LocalizeManager.GetLocalize("ui_keysetting_overlabkey");
                                }
                                else if ((joystricks[0].Contains("DS4") || joystricks[0].Contains("PC") || joystricks[0].Contains("Wireless")) && _key == KeyCode.JoystickButton7)
                                {
                                    if (SaveAndLoadManager.instance.SaveKey(KeyCode.Joystick7Button0, nowKeySettingNum, false))
                                        KeyChangeGuideText.text = "";
                                    else
                                        KeyChangeGuideText.text = LocalizeManager.GetLocalize("ui_keysetting_overlabkey");
                                }
                                else
                                {
                                    if (SaveAndLoadManager.instance.SaveKey(_key, nowKeySettingNum, false))
                                        KeyChangeGuideText.text = "";
                                    else
                                        KeyChangeGuideText.text = LocalizeManager.GetLocalize("ui_keysetting_overlabkey");
                                }

                                closeCoolTime = 0.0f;
                                isChangeKey = false;
                                AnyKeyObj.SetActive(false);
                            }
                            else
                            {
                                closeCoolTime = 0.0f;
                                KeyChangeGuideText.color = Color.red;
                                KeyChangeGuideText.text = LocalizeManager.GetLocalize("ui_keysetting_unusablekey");
                            }
                        }
                    }
                }
            }
            else
            {
                if (Input.anyKeyDown && closeCoolTime > 0.2f)
                {
                    foreach (KeyCode _key in (KeyCode[])System.Enum.GetValues(typeof(KeyCode)))
                    {
                        if (Input.GetKeyDown(KeyCode.Escape))
                        {
                            closeCoolTime = 0.0f;
                            isChangeKey = false;
                            AnyKeyObj.SetActive(false);
                            return;
                        }

                        if (Input.GetKeyDown(_key))
                        {
                            if (CheckingKeyChange(_key))
                            {
                                if (SaveAndLoadManager.instance.SaveKey(_key, nowKeySettingNum, true))
                                    KeyChangeGuideText.text = "";
                                else
                                    KeyChangeGuideText.text = LocalizeManager.GetLocalize("ui_keysetting_overlabkey");
                                closeCoolTime = 0.0f;
                                isChangeKey = false;
                                AnyKeyObj.SetActive(false);
                            }
                            else
                            {
                                closeCoolTime = 0.0f;
                                KeyChangeGuideText.color = Color.red;
                                KeyChangeGuideText.text = LocalizeManager.GetLocalize("ui_keysetting_unusablekey");
                            }
                        }
                    }
                }
            }
            return;
        }

        GetComponent<AudioSource>().volume = SoundManager.isMute ? 0 : SoundManager.BGMVolume;
        Cursor.lockState = CursorLockMode.Confined;
        Cursor.visible = false;
        crossHairImage.transform.position = Input.mousePosition;
        if (Input.anyKeyDown && !isStart)
        {
            closeCoolTime = 0.0f;
            SoundManager.instance.StartAudio(new string[1] { "pressanykey" }, VOLUME_TYPE.EFFECT);
            isStart = true;
            pressKey.gameObject.SetActive(false);
            if (isDemo && !isPrev)
                StartCoroutine(StartQuestionEvent());
            else
                StartCoroutine(StartButton());
        }
        if (Input.anyKeyDown && isThanks)
        {
            closeCoolTime = 0.0f;
            SoundManager.instance.StartAudio(new string[1] { "pressanykey" }, VOLUME_TYPE.EFFECT);
            isThanks = false;
            pressKey.gameObject.SetActive(false);
            StartCoroutine(StartButton());
        }

        if ((Input.GetKeyDown(KeyCode.Escape) || Input.GetKeyDown(KeyCode.Backspace) || JoyStickKeyInputCheck(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_CANCLE], true)) && closeCoolTime > 0.2f && isStart)
        {
            if(textButton2.activeSelf)
            {
                textButton2.SetActive(false);
                textButton.SetActive(true);
            }
            else
            {
                if(!optionCanvas.activeSelf)
                {
                    isStart = false;
                    GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 1);
                    logoSprite.transform.localPosition = new Vector3(0, 0, 0);
                    pressKey.gameObject.SetActive(true);
                    textButton.SetActive(false);
                }
            }
        }

        if (settingTrigger == 1 && (Input.GetKeyDown(KeyCode.Escape) || Input.GetKeyDown(KeyCode.Backspace) || JoyStickKeyInputCheck(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_CANCLE], true)) && closeCoolTime > 0.2f)
        {
            closeCoolTime = 0.0f;
            settingTrigger = 0;
            optionCanvas.SetActive(false);
        }
        else if (settingTrigger == 3 && (Input.GetKeyDown(KeyCode.Escape) || Input.GetKeyDown(KeyCode.Backspace) || JoyStickKeyInputCheck(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_CANCLE], true)) && closeCoolTime > 0.2f)
        {
            closeCoolTime = 0.0f;
            settingTrigger = 1;
            optionCanvas.SetActive(true);
            gameplayCanvas.SetActive(false);
            for (int i = 0; i < SettingSelectImages.Length; i++)
            {
                SettingSelectImages[i].enabled = false;
            }
            nowSettingSelectImageNum = 0;
            SelectBarChange(SettingSelectImages[nowSettingSelectImageNum]);
        }
        else if (settingTrigger == 4 && (Input.GetKeyDown(KeyCode.Escape) || Input.GetKeyDown(KeyCode.Backspace) || JoyStickKeyInputCheck(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_CANCLE], true)) && closeCoolTime > 0.2f)
        {
            closeCoolTime = 0.0f;
            settingTrigger = 1;
            optionCanvas.SetActive(true);
            videoCanvas.SetActive(false);
            for (int i = 0; i < SettingSelectImages.Length; i++)
            {
                SettingSelectImages[i].enabled = false;
            }
            nowSettingSelectImageNum = 1;
            SelectBarChange(SettingSelectImages[nowSettingSelectImageNum]);
        }
        else if (settingTrigger == 5 && (Input.GetKeyDown(KeyCode.Escape) || Input.GetKeyDown(KeyCode.Backspace) || JoyStickKeyInputCheck(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_CANCLE], true)) && closeCoolTime > 0.2f)
        {
            closeCoolTime = 0.0f;
            settingTrigger = 1;
            optionCanvas.SetActive(true);
            audioCanvas.SetActive(false);
            for (int i = 0; i < SettingSelectImages.Length; i++)
            {
                SettingSelectImages[i].enabled = false;
            }
            nowSettingSelectImageNum = 2;
            SelectBarChange(SettingSelectImages[nowSettingSelectImageNum]);
        }
        else if (settingTrigger == 6 && (Input.GetKeyDown(KeyCode.Escape) || Input.GetKeyDown(KeyCode.Backspace) || JoyStickKeyInputCheck(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_CANCLE], true)) && closeCoolTime > 0.2f)
        {
            closeCoolTime = 0.0f;
            settingTrigger = 1;
            optionCanvas.SetActive(true);
            languageCanvas.SetActive(false);
            for (int i = 0; i < SettingSelectImages.Length; i++)
            {
                SettingSelectImages[i].enabled = false;
            }
            nowSettingSelectImageNum = 3;
            SelectBarChange(SettingSelectImages[nowSettingSelectImageNum]);
        }
        else if (settingTrigger == 7 && (Input.GetKeyDown(KeyCode.Escape) || Input.GetKeyDown(KeyCode.Backspace) || JoyStickKeyInputCheck(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_CANCLE], true)) && closeCoolTime > 0.2f)
        {
            closeCoolTime = 0.0f;
            settingTrigger = 1;
            optionCanvas.SetActive(true);
            keySettingCanvas.SetActive(false);
            for (int i = 0; i < SettingSelectImages.Length; i++)
            {
                SettingSelectImages[i].enabled = false;
            }
            nowSettingSelectImageNum = 3;
            SelectBarChange(SettingSelectImages[nowSettingSelectImageNum]);
        }
        //설정창 키보드 조작
        float x = Input.GetAxisRaw("Horizontal_joy");
        float y = Input.GetAxisRaw("Vertical_joy");
        if (settingTrigger != 0)
        {
            if (KeyboardInputCheck(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ARROW_LEFT], false) || Input.GetKey(KeyCode.LeftArrow) || x < 0)
            {
                if (settingTrigger == 3 && nowSettingSelectImageNum == optionOcount && closeCoolTime > 0.1f)
                {
                    CrossHairChange(true);
                    closeCoolTime = 0.0f;
                }
                if (settingTrigger == 3 && nowSettingSelectImageNum == optionOcount + 1 && closeCoolTime > 0.1f)
                {
                    CameraShakerLevelChange(nCameraShakerLevel - 1);
                    closeCoolTime = 0.0f;
                }
                if (settingTrigger == 5 && nowSettingSelectImageNum == optionOcount + optionGcount + optionVcount + 1 && closeCoolTime > 0.1f)
                {
                    SoundManager.instance.VolumeChange(0, SoundManager.instance.mSlider.value - 0.1f);
                    closeCoolTime = 0.0f;
                }
                if (settingTrigger == 5 && nowSettingSelectImageNum == optionOcount + optionGcount + optionVcount + 2 && closeCoolTime > 0.1f)
                {
                    SoundManager.instance.VolumeChange(1, SoundManager.instance.bSlider.value - 0.1f);
                    closeCoolTime = 0.0f;
                }
                if (settingTrigger == 5 && nowSettingSelectImageNum == optionOcount + optionGcount + optionVcount + 3 && closeCoolTime > 0.1f)
                {
                    SoundManager.instance.VolumeChange(2, SoundManager.instance.eSlider.value - 0.1f);
                    closeCoolTime = 0.0f;
                }
                if (settingTrigger == 5 && nowSettingSelectImageNum == optionOcount + optionGcount + optionVcount + 4 && closeCoolTime > 0.1f)
                {
                    SoundManager.instance.VolumeChange(3, SoundManager.instance.aSlider.value - 0.1f);
                    closeCoolTime = 0.0f;
                }
                if (settingTrigger == 4 && nowSettingSelectImageNum == optionOcount + optionGcount + 3 && closeCoolTime > 0.1f)
                {
                    SoundManager.instance.StartAudio(new string[1] { "Click_0" }, VOLUME_TYPE.EFFECT);
                    Scrollbar scroll = SettingSelectImages[nowSettingSelectImageNum].transform.Find("Scrollbar").GetComponent<Scrollbar>();
                    FreamChange(scroll, scroll.value - 0.05f);
                    closeCoolTime = 0.0f;
                }
            }
            if (KeyboardInputCheck(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ARROW_RIGHT], false) || Input.GetKey(KeyCode.RightArrow) || x > 0)
            {
                if (settingTrigger == 3 && nowSettingSelectImageNum == optionOcount && closeCoolTime > 0.1f)
                {
                    CrossHairChange(false);
                    closeCoolTime = 0.0f;
                }
                if (settingTrigger == 3 && nowSettingSelectImageNum == optionOcount + 1 && closeCoolTime > 0.1f)
                {
                    CameraShakerLevelChange(nCameraShakerLevel + 1);
                    closeCoolTime = 0.0f;
                }
                if (settingTrigger == 5 && nowSettingSelectImageNum == optionOcount + optionGcount + optionVcount + 1 && closeCoolTime > 0.1f)
                {
                    SoundManager.instance.VolumeChange(0, SoundManager.instance.mSlider.value + 0.1f);
                    closeCoolTime = 0.0f;
                }
                if (settingTrigger == 5 && nowSettingSelectImageNum == optionOcount + optionGcount + optionVcount + 2 && closeCoolTime > 0.1f)
                {
                    SoundManager.instance.VolumeChange(1, SoundManager.instance.bSlider.value + 0.1f);
                    closeCoolTime = 0.0f;
                }
                if (settingTrigger == 5 && nowSettingSelectImageNum == optionOcount + optionGcount + optionVcount + 3 && closeCoolTime > 0.1f)
                {
                    SoundManager.instance.VolumeChange(2, SoundManager.instance.eSlider.value + 0.1f);
                    closeCoolTime = 0.0f;
                }
                if (settingTrigger == 5 && nowSettingSelectImageNum == optionOcount + optionGcount + optionVcount + 4 && closeCoolTime > 0.1f)
                {
                    SoundManager.instance.VolumeChange(3, SoundManager.instance.aSlider.value + 0.1f);
                    closeCoolTime = 0.0f;
                }
                if (settingTrigger == 4 && nowSettingSelectImageNum == optionOcount + optionGcount + 3 && closeCoolTime > 0.1f)
                {
                    SoundManager.instance.StartAudio(new string[1] { "Click_0" }, VOLUME_TYPE.EFFECT);
                    Scrollbar scroll = SettingSelectImages[nowSettingSelectImageNum].transform.Find("Scrollbar").GetComponent<Scrollbar>();
                    FreamChange(scroll, scroll.value + 0.05f);
                    closeCoolTime = 0.0f;
                }
            }
            if ((KeyboardInputCheck(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ARROW_UP], false) || Input.GetKeyDown(KeyCode.UpArrow) || y > 0) && closeCoolTime > 0.2f) 
            {
                closeCoolTime = 0.0f;
                if (settingTrigger == 1)
                {
                    nowSettingSelectImageNum--;
                    if (nowSettingSelectImageNum < 0) nowSettingSelectImageNum = SettingSelectImages.Length - 1 - optionGcount - optionVcount - optionAcount - optionLcount - (optionKcount + 14);
                    SelectBarChange(SettingSelectImages[nowSettingSelectImageNum]);
                }
                if (settingTrigger == 3)
                {
                    nowSettingSelectImageNum--;
                    if (nowSettingSelectImageNum < optionOcount) nowSettingSelectImageNum = SettingSelectImages.Length - 1 - optionVcount - optionAcount - optionLcount - (optionKcount + 14);
                    SelectBarChange(SettingSelectImages[nowSettingSelectImageNum]);
                }
                if (settingTrigger == 4)
                {
                    nowSettingSelectImageNum--;
                    if (nowSettingSelectImageNum < optionOcount + optionGcount) nowSettingSelectImageNum = SettingSelectImages.Length - 1 - optionAcount - optionLcount - (optionKcount + 14);
                    SelectBarChange(SettingSelectImages[nowSettingSelectImageNum]);
                }
                if (settingTrigger == 5)
                {
                    nowSettingSelectImageNum--;
                    if (nowSettingSelectImageNum < optionOcount + optionGcount + optionVcount) nowSettingSelectImageNum = SettingSelectImages.Length - 1 - optionLcount - (optionKcount + 14);
                    SelectBarChange(SettingSelectImages[nowSettingSelectImageNum]);
                }
                if (settingTrigger == 6)
                {
                    nowSettingSelectImageNum--;
                    if (nowSettingSelectImageNum < optionOcount + optionGcount + optionVcount + optionAcount) nowSettingSelectImageNum = SettingSelectImages.Length - 1 - optionLcount - 22;
                    SelectBarChange(SettingSelectImages[nowSettingSelectImageNum]);
                }
                if (settingTrigger == 7)
                {
                    nowSettingSelectImageNum--;
                    if (nowSettingSelectImageNum < optionOcount + optionGcount + optionVcount + optionAcount + optionLcount)
                        nowSettingSelectImageNum = SettingSelectImages.Length - (isJoystrick ? 15 : 13);
                    if (!isJoystrick)
                        SelectBarChange(SettingSelectImages[nowSettingSelectImageNum + 12]);
                    else
                        SelectBarChange(SettingSelectImages[nowSettingSelectImageNum]);
                }
            }
            if ((KeyboardInputCheck(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ARROW_DOWN], false) || Input.GetKeyDown(KeyCode.DownArrow) || y < 0) && closeCoolTime > 0.2f) 
            {
                closeCoolTime = 0.0f;
                if (settingTrigger == 1)
                {
                    nowSettingSelectImageNum++;
                    if (nowSettingSelectImageNum >= SettingSelectImages.Length - optionGcount - optionVcount - optionAcount - optionLcount - (optionKcount + 14)) nowSettingSelectImageNum = 0;
                    SelectBarChange(SettingSelectImages[nowSettingSelectImageNum]);
                }
                if (settingTrigger == 3)
                {
                    nowSettingSelectImageNum++;
                    if (nowSettingSelectImageNum >= SettingSelectImages.Length - optionVcount - optionAcount - optionLcount - (optionKcount + 14)) nowSettingSelectImageNum = optionOcount;
                    SelectBarChange(SettingSelectImages[nowSettingSelectImageNum]);
                }
                if (settingTrigger == 4)
                {
                    nowSettingSelectImageNum++;
                    if (nowSettingSelectImageNum >= SettingSelectImages.Length - optionAcount - optionLcount - (optionKcount + 14)) nowSettingSelectImageNum = optionOcount + optionGcount;
                    SelectBarChange(SettingSelectImages[nowSettingSelectImageNum]);
                }
                if (settingTrigger == 5)
                {
                    nowSettingSelectImageNum++;
                    if (nowSettingSelectImageNum >= SettingSelectImages.Length - optionLcount - (optionKcount + 14)) nowSettingSelectImageNum = optionOcount + optionGcount + optionVcount;
                    SelectBarChange(SettingSelectImages[nowSettingSelectImageNum]);
                }
                if (settingTrigger == 6)
                {
                    nowSettingSelectImageNum++;
                    if (nowSettingSelectImageNum >= SettingSelectImages.Length - (optionKcount + 14)) nowSettingSelectImageNum = optionOcount + optionGcount + optionVcount + optionAcount;
                    SelectBarChange(SettingSelectImages[nowSettingSelectImageNum]);
                }
                if (settingTrigger == 7)
                {
                    nowSettingSelectImageNum++;
                    if (nowSettingSelectImageNum >= SettingSelectImages.Length - (isJoystrick ? 14 : 12)) nowSettingSelectImageNum = optionOcount + optionGcount + optionVcount + optionAcount + optionLcount;
                    if (!isJoystrick)
                        SelectBarChange(SettingSelectImages[nowSettingSelectImageNum + 12]);
                    else
                        SelectBarChange(SettingSelectImages[nowSettingSelectImageNum]);
                }
            }
            if ((KeyboardInputCheck(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ACTIVE], false) || Input.GetKeyDown(KeyCode.Return) || JoyStickKeyInputCheck(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_CONFIRM], true)) && closeCoolTime > 0.2f) 
            {
                closeCoolTime = 0.0f;
                if (settingTrigger == 1)
                {
                    OptionMenuClick(nowSettingSelectImageNum + 3);
                    return;
                }
                if (settingTrigger == 3 && nowSettingSelectImageNum == optionOcount + 2)
                {
                    OnOffPause();
                    return;
                }
                if (settingTrigger == 4 && nowSettingSelectImageNum == optionOcount + optionGcount)
                {
                    ScreenModeChange(true);
                    return;
                }
                if (settingTrigger == 4 && nowSettingSelectImageNum == optionOcount + optionGcount + 1)
                {
                    ScreenModeChange(false);
                    return;
                }
                if (settingTrigger == 4 && nowSettingSelectImageNum == optionOcount + optionGcount + 2)
                {
                    VsyncModeChange(!isVsyncMode);
                    return;
                }
                if (settingTrigger == 5 && nowSettingSelectImageNum == optionOcount + optionGcount + optionVcount)
                {
                    SoundManager.instance.MuteChange(SettingSelectImages[nowSettingSelectImageNum].transform.Find("Mute").Find("Image").GetComponent<Image>());
                    return;
                }
                if (settingTrigger == 6 && nowSettingSelectImageNum == optionOcount + optionGcount + optionVcount + optionAcount)
                {
                    LocalizeManager.instance.LanguageChange(0);
                    return;
                }
                if (settingTrigger == 6 && nowSettingSelectImageNum == optionOcount + optionGcount + optionVcount + optionAcount + 1)
                {
                    LocalizeManager.instance.LanguageChange(1);
                    return;
                }
                if (settingTrigger == 6 && nowSettingSelectImageNum == optionOcount + optionGcount + optionVcount + optionAcount + 2)
                {
                    LocalizeManager.instance.LanguageChange(2);
                    return;
                }
                if (settingTrigger == 6 && nowSettingSelectImageNum == optionOcount + optionGcount + optionVcount + optionAcount + 3)
                {
                    LocalizeManager.instance.LanguageChange(3);
                    return;
                }
                if (settingTrigger == 6 && nowSettingSelectImageNum == optionOcount + optionGcount + optionVcount + optionAcount + 4)
                {
                    LocalizeManager.instance.LanguageChange(4);
                    return;
                }
                if (settingTrigger == 6 && nowSettingSelectImageNum == optionOcount + optionGcount + optionVcount + optionAcount + 5)
                {
                    LocalizeManager.instance.LanguageChange(5);
                    return;
                }
                if (settingTrigger == 7)
                {
                    for (int i = 0; i < (isJoystrick ? 12 : 14); i++)
                    {
                        if (nowSettingSelectImageNum == optionOcount + optionGcount + optionVcount + optionAcount + optionLcount + i)
                        {
                            KeySetting(i);
                        }
                    }
                    return;
                }
            }
        }
        else
        {
            if ((KeyboardInputCheck(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ARROW_UP], true) || Input.GetKeyDown(KeyCode.UpArrow) || y > 0) && closeCoolTime > 0.2f) 
            {
                closeCoolTime = 0.0f;
                nowSelectLoginNum--;
                if(textButton2.activeSelf)
                {
                    if (nowSelectLoginNum < 6) nowSelectLoginNum = 10;
                }
                else if (loadObj.activeSelf)
                {
                    if (nowSelectLoginNum < 0) nowSelectLoginNum = 5;
                }
                else
                {
                    if (nowSelectLoginNum <= 0) nowSelectLoginNum = 5;
                }
            }
            if ((KeyboardInputCheck(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ARROW_DOWN], true) || Input.GetKeyDown(KeyCode.DownArrow) || y < 0) && closeCoolTime > 0.2f) 
            {
                closeCoolTime = 0.0f;
                nowSelectLoginNum++;

                if (textButton2.activeSelf)
                {
                    if (nowSelectLoginNum > 10) nowSelectLoginNum = 6;
                }
                else if (loadObj.activeSelf)
                {
                    if (nowSelectLoginNum > 5) nowSelectLoginNum = 0;
                }
                else
                {
                    if (nowSelectLoginNum > 5) nowSelectLoginNum = 1;
                }
            }
            if ((KeyboardInputCheck(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ACTIVE], true) || Input.GetKeyDown(KeyCode.Return) || JoyStickKeyInputCheck(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_CONFIRM], true)) && closeCoolTime > 0.2f) 
            {
                closeCoolTime = 0.0f;
                if (nowSelectLoginNum == 0) GameLoad();
                if (nowSelectLoginNum == 1) GameStart();
                if (nowSelectLoginNum == 2)
                {
                    OptionOpen();
                    closeCoolTime = 0.0f;
                    return;
                }
                if (nowSelectLoginNum == 3)
                {
                    //Debug.Log("QUIT");
                    Application.Quit();
                }
                if (nowSelectLoginNum == 4) GameObject.Find("Canvas").transform.Find("Join").Find("Steam").GetComponent<Button>().onClick.Invoke();
                if (nowSelectLoginNum == 5) GameObject.Find("Canvas").transform.Find("Join").Find("Discord").GetComponent<Button>().onClick.Invoke();
                if (nowSelectLoginNum == 6) DifficultyChange(0);
                if (nowSelectLoginNum == 7) DifficultyChange(1);
                if (nowSelectLoginNum == 8) DifficultyChange(2);
                if (nowSelectLoginNum == 9) DifficultyChange(3);
                if (nowSelectLoginNum == 10) DifficultyChange(4);
            }
        }

        if(nowSelectLoginNum > 3)
        {
            if (nowSelectLoginNum == 4)
            {
                GameObject.Find("Canvas").transform.Find("Join").Find("Steam").Find("OutLine").gameObject.SetActive(true);
                GameObject.Find("Canvas").transform.Find("Join").Find("Discord").Find("OutLine").gameObject.SetActive(false);
            }
            else if (nowSelectLoginNum == 5)
            {
                GameObject.Find("Canvas").transform.Find("Join").Find("Steam").Find("OutLine").gameObject.SetActive(false);
                GameObject.Find("Canvas").transform.Find("Join").Find("Discord").Find("OutLine").gameObject.SetActive(true);
            }
            else
            {
                for (int i = 0; i < LoginMenus2.Length; i++)
                {
                    if (i == (nowSelectLoginNum - 6)) 
                    {
                        LoginMenus2[i].GetComponent<Text>().fontSize = 32;
                        LoginMenus2[i].GetComponent<Text>().color = new Color32(255, 255, 255, 255);
                    }
                    else
                    {
                        LoginMenus2[i].GetComponent<Text>().fontSize = 28;
                        LoginMenus2[i].GetComponent<Text>().color = new Color32(100, 100, 100, 255);
                    }
                }
            }
            for (int i = 0; i < LoginMenus.Length; i++)
            {
                LoginMenus[i].GetComponent<Text>().fontSize = 28;
                LoginMenus[i].GetComponent<Text>().color = new Color32(100, 100, 100, 255);
            }
        }
        else
        {
            GameObject.Find("Canvas").transform.Find("Join").Find("Steam").Find("OutLine").gameObject.SetActive(false);
            GameObject.Find("Canvas").transform.Find("Join").Find("Discord").Find("OutLine").gameObject.SetActive(false);
            for (int i = 0; i < LoginMenus.Length; i++)
            {
                if (i == nowSelectLoginNum)
                {
                    LoginMenus[i].GetComponent<Text>().fontSize = 32;
                    LoginMenus[i].GetComponent<Text>().color = new Color32(255, 255, 255, 255);
                }
                else
                {
                    LoginMenus[i].GetComponent<Text>().fontSize = 28;
                    LoginMenus[i].GetComponent<Text>().color = new Color32(100, 100, 100, 255);
                }
            }
            for (int i = 0; i < LoginMenus2.Length; i++)
            {
                LoginMenus2[i].GetComponent<Text>().fontSize = 28;
                LoginMenus2[i].GetComponent<Text>().color = new Color32(100, 100, 100, 255);
            }
        }
    }
    IEnumerator StartQuestionEvent()
    {
        RectTransform rt = thankpanel.GetComponent<RectTransform>();
        float y = 650;
        while (true)
        {
            rt.anchoredPosition3D = new Vector3(0, y, 0);
            y -= Time.deltaTime * moveSpeed;
            if (y <= 70.0f) break;
            yield return new WaitForSeconds(Time.deltaTime);
        }
        y = 70.0f;
        rt.anchoredPosition3D = new Vector3(0, y, 0);
        while (true)
        {
            rt.anchoredPosition3D = new Vector3(0, y, 0);
            y += Time.deltaTime * (moveSpeed / 4);
            if (y >= 90.0f) break;
            yield return new WaitForSeconds(Time.deltaTime);
        }
        y = 90.0f;
        rt.anchoredPosition3D = new Vector3(0, y, 0);
        while (true)
        {
            rt.anchoredPosition3D = new Vector3(0, y, 0);
            y -= Time.deltaTime * (moveSpeed / 4);
            if (y <= 70.0f) break;
            yield return new WaitForSeconds(Time.deltaTime);
        }
        y = 70.0f;
        rt.anchoredPosition3D = new Vector3(0, y, 0);
        while (true)
        {
            rt.anchoredPosition3D = new Vector3(0, y, 0);
            y += Time.deltaTime * (moveSpeed / 4);
            if (y >= 80.0f) break;
            yield return new WaitForSeconds(Time.deltaTime);
        }
        y = 80.0f;
        rt.anchoredPosition3D = new Vector3(0, y, 0);
        while (true)
        {
            rt.anchoredPosition3D = new Vector3(0, y, 0);
            y -= Time.deltaTime * (moveSpeed / 4);
            if (y <= 70.0f) break;
            yield return new WaitForSeconds(Time.deltaTime);
        }
        y = 70.0f;
        pressKey.gameObject.SetActive(true);
        isThanks = true;
    }
    IEnumerator StartButton()
    {
        float y = 50.0f;
        if(isDemo && !isPrev)
        {
            RectTransform rt = thankpanel.GetComponent<RectTransform>();
            while (true)
            {
                rt.anchoredPosition3D = new Vector3(0, y, 0);
                y += Time.deltaTime * moveSpeed;
                if (y >= 900.0f) break;
                yield return new WaitForSeconds(Time.deltaTime);
            }
            isPrev = true;
        }
        y = 0.0f;
        float max = 12.5f;
        float c = 1.0f;
        while (true)
        {
            y += 0.1f * logoSpeed;
            logoSprite.transform.localPosition = new Vector3(0, y, 0);
            c = 1 -(0.5f * y / max);
            GetComponent<SpriteRenderer>().color = new Color(c, c, c, 1);
            if (y >= max) break;
            yield return null;
        }
        y = max;
        logoSprite.transform.localPosition = new Vector3(0, y, 0);
        textButton.SetActive(true);

    }
    IEnumerator KeyOnOff()
    {
        while(true)
        {
            float a = 0.0f;
            while (a < 1.0f)
            {
                pressKey.color = new Color(1, 1, 1, a);
                a += Time.deltaTime * pressKeySpeed;
                yield return new WaitForSeconds(Time.deltaTime * pressKeySpeed);
            }
            a = 1.0f;
            pressKey.color = new Color(1, 1, 1, a);
            while (a > 0.0f)
            {
                pressKey.color = new Color(1, 1, 1, a);
                a -= Time.deltaTime * pressKeySpeed;
                yield return new WaitForSeconds(Time.deltaTime * pressKeySpeed);
            }
            a = 0.0f;
            pressKey.color = new Color(1, 1, 1, a);
        }
    }
    IEnumerator LogoOn()
    {
        yield return new WaitForSeconds(0.5f);
        StartCoroutine(KeyOnOff());
    }
    public void GameStart()
    {
        textButton.SetActive(false);
        textButton2.SetActive(true);
    }
    public void DifficultyChange(int t)
    {
        if(t == 4)
        {
            textButton2.SetActive(false);
            textButton.SetActive(true);
        }
        else
        {
            SaveAndLoadManager.nDifficulty = t;
            SceneManager.LoadScene(2);
        }
    }
    public void GameLoad()
    {
        SaveAndLoadManager.isLoad = true;
        SceneManager.LoadScene(2);
    }
    public void OptionOpen()
    {
        SoundManager.instance.StartAudio(new string[1] { "menu_click" }, VOLUME_TYPE.EFFECT);
        optionCanvas.SetActive(true);
        settingTrigger = 1;
        for (int i = 0; i < SettingSelectImages.Length; i++)
        {
            SettingSelectImages[i].enabled = false;
        }
        nowSettingSelectImageNum = 0;
        SelectBarChange(SettingSelectImages[nowSettingSelectImageNum]);
    }
    public void SelectBarChange(Image _image)
    {
        for (int i = 0; i < SettingSelectImages.Length; i++)
        {
            if (SettingSelectImages[i] == _image)
            {
                SettingSelectImages[i].enabled = true;
            }
            else
            {
                SettingSelectImages[i].enabled = false;
            }
        }
    }
    public void OptionMenuClick(int _type)
    {
        SoundManager.instance.StartAudio(new string[1] { "menu_click" }, VOLUME_TYPE.EFFECT);
        settingTrigger = _type;
        if (_type == 3)
        {
            nowSettingSelectImageNum = optionOcount;
            SelectBarChange(SettingSelectImages[nowSettingSelectImageNum]);
            gameplayCanvas.SetActive(true);
        }
        else if (_type == 4)
        {
            nowSettingSelectImageNum = optionOcount + optionGcount;
            SelectBarChange(SettingSelectImages[nowSettingSelectImageNum]);
            videoCanvas.SetActive(true);
        }
        else if (_type == 5)
        {
            nowSettingSelectImageNum = optionOcount + optionGcount + optionVcount;
            SelectBarChange(SettingSelectImages[nowSettingSelectImageNum]);
            audioCanvas.SetActive(true);
        }
        else if (_type == 6)
        {
            nowSettingSelectImageNum = optionOcount + optionGcount + optionVcount + optionAcount;
            SelectBarChange(SettingSelectImages[nowSettingSelectImageNum]);
            languageCanvas.SetActive(true);
        }
        else if (_type == 7)
        {
            nowSettingSelectImageNum = optionOcount + optionGcount + optionVcount + optionAcount + optionLcount;
            SelectBarChange(SettingSelectImages[nowSettingSelectImageNum]);
            keySettingCanvas.SetActive(true);
        }
    }
    public void CrossHairChange(bool prev)
    {
        if (prev)
        {
            nowCrossHairNum--;
            if (nowCrossHairNum < 0) nowCrossHairNum = crossHairSprites.Length - 1;
        }
        else
        {
            nowCrossHairNum++;
            if (nowCrossHairNum >= crossHairSprites.Length) nowCrossHairNum = 0;
        }
        crossHairImage.sprite =  crossHairImage_UI.sprite = crossHairSprites[nowCrossHairNum];
        SaveAndLoadManager.instance.SaveItem(new ItemSave { _id = "UI_CrossHair", _nCount = nowCrossHairNum }, true);
    }
    public void ScreenModeChange(bool _fullScreen)
    {
        SoundManager.instance.StartAudio(new string[1] { "menu_click" }, VOLUME_TYPE.EFFECT);
        isFullScreenMode = _fullScreen;
        if (_fullScreen)
        {
            full_Option.sprite = _checkSprite;
            window_Option.sprite = _unCheckSprite;
        }
        else
        {
            full_Option.sprite = _unCheckSprite;
            window_Option.sprite = _checkSprite;
        }
        Screen.SetResolution(1600, 900, isFullScreenMode, 60);
        SaveAndLoadManager.instance.SaveVolume(new VolumeSave { _type = 5, _value = (isFullScreenMode ? 1 : 0) });
    }
    public void VsyncModeChange(bool _isVsyncMode)
    {
        SoundManager.instance.StartAudio(new string[1] { "menu_click" }, VOLUME_TYPE.EFFECT);
        isVsyncMode = _isVsyncMode;
        vSync_Option.sprite = (isVsyncMode ? _checkSprite : _unCheckSprite);
        if (isVsyncMode)
        {
            QualitySettings.vSyncCount = 1;
            ui_FreamText.text = LocalizeManager.GetLocalize("ui_fream") + "\n" + LocalizeManager.GetLocalize("ui_vsync_use");
        }
        else
        {
            QualitySettings.vSyncCount = 0;
            ui_FreamText.text = LocalizeManager.GetLocalize("ui_fream") + "\n(" + Application.targetFrameRate + "FPS)";
        }
        SaveAndLoadManager.instance.SaveVolume(new VolumeSave { _type = 7, _value = (isVsyncMode ? 1 : 0) });
    }
    public void VsyncModeChange()
    {
        SoundManager.instance.StartAudio(new string[1] { "menu_click" }, VOLUME_TYPE.EFFECT);
        isVsyncMode = !isVsyncMode;
        vSync_Option.sprite = (isVsyncMode ? _checkSprite : _unCheckSprite);
        if (isVsyncMode)
        {
            QualitySettings.vSyncCount = 1;
            ui_FreamText.text = LocalizeManager.GetLocalize("ui_fream") + "\n" + LocalizeManager.GetLocalize("ui_vsync_use");
        }
        else
        {
            QualitySettings.vSyncCount = 0;
            ui_FreamText.text = LocalizeManager.GetLocalize("ui_fream") + "\n(" + Application.targetFrameRate + "FPS)";
        }
        SaveAndLoadManager.instance.SaveVolume(new VolumeSave { _type = 7, _value = (isVsyncMode ? 1 : 0) });
    }
    public void FreamChange(Scrollbar _slider)
    {
        float v = _slider.value;
        v = (90 * v) + 30;
        int vv = Mathf.FloorToInt(v);
        Application.targetFrameRate = vv;
        if (isVsyncMode)
        {
            ui_FreamText.text = LocalizeManager.GetLocalize("ui_fream") + "\n" + LocalizeManager.GetLocalize("ui_vsync_use");
        }
        else
        {
            ui_FreamText.text = LocalizeManager.GetLocalize("ui_fream") + "\n(" + vv + "FPS)";
        }
        SaveAndLoadManager.instance.SaveItem(new ItemSave { _id = "FREAM_RATE", _nCount = vv }, true);
    }
    public void FreamChange(Scrollbar _slider, float _value)
    {
        if (_value < 0.0f) _value = 0;
        if (_value > 1.0f) _value = 1;
        float v = _slider.value = _value;
        v = (90 * v) + 30;
        int vv = Mathf.FloorToInt(v);
        Application.targetFrameRate = vv;
        if (isVsyncMode)
        {
            ui_FreamText.text = LocalizeManager.GetLocalize("ui_fream") + "\n" + LocalizeManager.GetLocalize("ui_vsync_use");
        }
        else
        {
            ui_FreamText.text = LocalizeManager.GetLocalize("ui_fream") + "\n(" + vv + "FPS)";
        }
        SaveAndLoadManager.instance.SaveItem(new ItemSave { _id = "FREAM_RATE", _nCount = vv }, true);
    }
    public void CameraShakerLevelChange(int _level)
    {
        if (_level < 0) _level = 4;
        if (_level > 4) _level = 0;
        nCameraShakerLevel = _level;
        for (int i = 0; i < CameraShakerLevelImage.Length; i++)
        {
            if (i == nCameraShakerLevel) CameraShakerLevelImage[i].sprite = _checkSprite;
            else CameraShakerLevelImage[i].sprite = _unCheckSprite;
        }
        SaveAndLoadManager.instance.SaveItem(new ItemSave { _id = "OPTION_CAMERA_SHAKER", _nCount = nCameraShakerLevel }, true);
    }
    public void OnOffPause()
    {
        isEscPause = !isEscPause;
        SaveAndLoadManager.instance.SaveItem(new ItemSave { _id = "ESC_PAUSE", _nCount = System.Convert.ToInt32(isEscPause) }, true);
        if (isEscPause) UI_EscPause.sprite = _checkSprite;
        else UI_EscPause.sprite = _unCheckSprite;
    }
    public void LoginMenuChange(int _type)
    {
        nowSelectLoginNum = _type;
    }
    public bool JoyStickKeyInputCheck(KeyCode _joyKey, bool _isDown)
    {
        if (joystricks.Count == 0) return false;
        if (joystricks[0].Contains("DS4") || joystricks[0].Contains("PC") || joystricks[0].Contains("Wireless"))
        {
            float joyCrossX = Input.GetAxisRaw("PS4_crossX");
            float joyCrossY = Input.GetAxisRaw("PS4_crossY");

            if (_joyKey == KeyCode.Joystick6Button0)
            {
                if (joyCrossY > 0) return true;
            }
            else if (_joyKey == KeyCode.Joystick6Button1)
            {
                if (joyCrossY < 0) return true;
            }
            else if (_joyKey == KeyCode.Joystick6Button3)
            {
                if (joyCrossX > 0) return true;
            }
            else if (_joyKey == KeyCode.Joystick6Button4)
            {
                if (joyCrossX < 0) return true;
            }
            else if (_joyKey == KeyCode.Joystick7Button0)
            {
                return _isDown ? Input.GetKeyDown(KeyCode.JoystickButton7) : Input.GetKey(KeyCode.JoystickButton7);
            }
            else if (_joyKey == KeyCode.Joystick7Button1)
            {
                return _isDown ? Input.GetKeyDown(KeyCode.JoystickButton6) : Input.GetKey(KeyCode.JoystickButton6);
            }
            else if (_joyKey == KeyCode.JoystickButton8)
            {
                return _isDown ? Input.GetKeyDown(KeyCode.JoystickButton10) : Input.GetKey(KeyCode.JoystickButton10);
            }
            else if (_joyKey == KeyCode.JoystickButton9)
            {
                return _isDown ? Input.GetKeyDown(KeyCode.JoystickButton11) : Input.GetKey(KeyCode.JoystickButton11);
            }
            else
            {
                return _isDown ? Input.GetKeyDown(_joyKey) : Input.GetKey(_joyKey);
            }
        }
        else
        {
            float joyCrossX = Input.GetAxisRaw("Horizontal_cross");
            float joyCrossY = Input.GetAxisRaw("Vertical_cross");
            float JoyStrickTrigger = Input.GetAxisRaw("JoyStrickTrigger");

            if (_joyKey == KeyCode.Joystick6Button0)
            {
                if (joyCrossY > 0) return true;
            }
            else if (_joyKey == KeyCode.Joystick6Button1)
            {
                if (joyCrossY < 0) return true;
            }
            else if (_joyKey == KeyCode.Joystick6Button3)
            {
                if (joyCrossX > 0) return true;
            }
            else if (_joyKey == KeyCode.Joystick6Button4)
            {
                if (joyCrossX < 0) return true;
            }
            else if (_joyKey == KeyCode.Joystick7Button0)
            {
                if (JoyStrickTrigger > 0) return true;
            }
            else if (_joyKey == KeyCode.Joystick7Button1)
            {
                if (JoyStrickTrigger < 0) return true;
            }
            else
            {
                return _isDown ? Input.GetKeyDown(_joyKey) : Input.GetKey(_joyKey);
            }
        }

        return false;
    }
    public bool KeyboardInputCheck(KeyCode _keyCode, bool _isDown)
    {
        if (_isDown) return Input.GetKeyDown(_keyCode);
        else return Input.GetKey(_keyCode);
    }
    public Sprite GetJoyStrickKeySprite(KeyCode _joyKey)
    {
        if (!isJoystrick)
        {
            return System.Array.Find(joyKeySprites, item => item.name.Equals("keyboard_" + _joyKey.ToString()));
        }

        Sprite _sprite = null;

        if (joystricks[0].Contains("DS4") || joystricks[0].Contains("PC") || joystricks[0].Contains("Wireless"))
        {
            if (_joyKey == KeyCode.Joystick6Button0)
            {
                _sprite = System.Array.Find(joyKeySprites, item => item.name.Equals("up"));
            }
            else if (_joyKey == KeyCode.Joystick6Button1)
            {
                _sprite = System.Array.Find(joyKeySprites, item => item.name.Equals("down"));
            }
            else if (_joyKey == KeyCode.Joystick6Button3)
            {
                _sprite = System.Array.Find(joyKeySprites, item => item.name.Equals("right"));
            }
            else if (_joyKey == KeyCode.Joystick6Button4)
            {
                _sprite = System.Array.Find(joyKeySprites, item => item.name.Equals("left"));
            }
            else if (_joyKey == KeyCode.Joystick7Button0)
            {
                _sprite = System.Array.Find(joyKeySprites, item => item.name.Equals("rt"));
            }
            else if (_joyKey == KeyCode.Joystick7Button1)
            {
                _sprite = System.Array.Find(joyKeySprites, item => item.name.Equals("lt"));
            }
            else if (_joyKey == KeyCode.Joystick8Button0)
            {
                _sprite = System.Array.Find(joyKeySprites, item => item.name.Equals("arrow"));
            }
            else
            {
                if (_joyKey == KeyCode.JoystickButton0) _sprite = System.Array.Find(joyKeySprites, item => item.name.Equals("Square"));
                else if (_joyKey == KeyCode.JoystickButton1) _sprite = System.Array.Find(joyKeySprites, item => item.name.Equals("x"));
                else if (_joyKey == KeyCode.JoystickButton2) _sprite = System.Array.Find(joyKeySprites, item => item.name.Equals("O"));
                else if (_joyKey == KeyCode.JoystickButton3) _sprite = System.Array.Find(joyKeySprites, item => item.name.Equals("Triangle"));
                else if (_joyKey == KeyCode.JoystickButton4) _sprite = System.Array.Find(joyKeySprites, item => item.name.Equals("L1"));
                else if (_joyKey == KeyCode.JoystickButton5) _sprite = System.Array.Find(joyKeySprites, item => item.name.Equals("r1"));
                else if (_joyKey == KeyCode.JoystickButton8) _sprite = System.Array.Find(joyKeySprites, item => item.name.Equals("L2"));
                else if (_joyKey == KeyCode.JoystickButton9) _sprite = System.Array.Find(joyKeySprites, item => item.name.Equals("r2"));
            }
        }
        else
        {
            if (_joyKey == KeyCode.Joystick6Button0)
            {
                _sprite = System.Array.Find(joyKeySprites, item => item.name.Equals("up"));
            }
            else if (_joyKey == KeyCode.Joystick6Button1)
            {
                _sprite = System.Array.Find(joyKeySprites, item => item.name.Equals("down"));
            }
            else if (_joyKey == KeyCode.Joystick6Button3)
            {
                _sprite = System.Array.Find(joyKeySprites, item => item.name.Equals("right"));
            }
            else if (_joyKey == KeyCode.Joystick6Button4)
            {
                _sprite = System.Array.Find(joyKeySprites, item => item.name.Equals("left"));
            }
            else if (_joyKey == KeyCode.Joystick7Button0)
            {
                _sprite = System.Array.Find(joyKeySprites, item => item.name.Equals("rt"));
            }
            else if (_joyKey == KeyCode.Joystick7Button1)
            {
                _sprite = System.Array.Find(joyKeySprites, item => item.name.Equals("lt"));
            }
            else if (_joyKey == KeyCode.Joystick8Button0)
            {
                _sprite = System.Array.Find(joyKeySprites, item => item.name.Equals("arrow"));
            }
            else
            {
                if (_joyKey == KeyCode.JoystickButton0) _sprite = System.Array.Find(joyKeySprites, item => item.name.Equals("a"));
                else if (_joyKey == KeyCode.JoystickButton1) _sprite = System.Array.Find(joyKeySprites, item => item.name.Equals("b"));
                else if (_joyKey == KeyCode.JoystickButton2) _sprite = System.Array.Find(joyKeySprites, item => item.name.Equals("x"));
                else if (_joyKey == KeyCode.JoystickButton3) _sprite = System.Array.Find(joyKeySprites, item => item.name.Equals("y"));
                else if (_joyKey == KeyCode.JoystickButton4) _sprite = System.Array.Find(joyKeySprites, item => item.name.Equals("lb"));
                else if (_joyKey == KeyCode.JoystickButton5) _sprite = System.Array.Find(joyKeySprites, item => item.name.Equals("rb"));
                else if (_joyKey == KeyCode.JoystickButton8) _sprite = System.Array.Find(joyKeySprites, item => item.name.Equals("ls"));
                else if (_joyKey == KeyCode.JoystickButton9) _sprite = System.Array.Find(joyKeySprites, item => item.name.Equals("rs"));
            }
        }
        return _sprite;
    }
    public void KeySetting(int _type)
    {
        nowKeySettingNum = _type;
        isChangeKey = true;
        AnyKeyObj.transform.Find("GameObject").Find("Image").GetComponent<SetUiSprite>()._joyKey = (JoystickKeySet)_type;
        if (isJoystrick)
            AnyKeyObj.transform.Find("GameObject").Find("Text").GetComponent<Text>().text = LocalizeManager.GetLocalize("ui_keysetting_" + ((JoystickKeySet)_type).ToString());
        else
            AnyKeyObj.transform.Find("GameObject").Find("Text").GetComponent<Text>().text = LocalizeManager.GetLocalize("keybaord_setting_" + _type);
        AnyKeyObj.SetActive(true);
    }
    private bool CheckingKeyChange(KeyCode _code)
    {
        if (!isJoystrick)
        {
            if (_code == KeyCode.Mouse0) return true;
            if (_code == KeyCode.Mouse1) return true;
            if (_code == KeyCode.BackQuote) return true;
            if (_code == KeyCode.Alpha0) return true;
            if (_code == KeyCode.Alpha1) return true;
            if (_code == KeyCode.Alpha2) return true;
            if (_code == KeyCode.Alpha3) return true;
            if (_code == KeyCode.Alpha4) return true;
            if (_code == KeyCode.Alpha5) return true;
            if (_code == KeyCode.Alpha6) return true;
            if (_code == KeyCode.Alpha7) return true;
            if (_code == KeyCode.Alpha8) return true;
            if (_code == KeyCode.Alpha9) return true;
            if (_code == KeyCode.Minus) return true;
            if (_code == KeyCode.Equals) return true;
            if (_code == KeyCode.Tab) return true;
            if (_code == KeyCode.CapsLock) return true;
            if (_code == KeyCode.A) return true;
            if (_code == KeyCode.B) return true;
            if (_code == KeyCode.C) return true;
            if (_code == KeyCode.D) return true;
            if (_code == KeyCode.E) return true;
            if (_code == KeyCode.F) return true;
            if (_code == KeyCode.G) return true;
            if (_code == KeyCode.H) return true;
            if (_code == KeyCode.I) return true;
            if (_code == KeyCode.J) return true;
            if (_code == KeyCode.K) return true;
            if (_code == KeyCode.L) return true;
            if (_code == KeyCode.M) return true;
            if (_code == KeyCode.N) return true;
            if (_code == KeyCode.O) return true;
            if (_code == KeyCode.P) return true;
            if (_code == KeyCode.Q) return true;
            if (_code == KeyCode.R) return true;
            if (_code == KeyCode.S) return true;
            if (_code == KeyCode.T) return true;
            if (_code == KeyCode.U) return true;
            if (_code == KeyCode.V) return true;
            if (_code == KeyCode.W) return true;
            if (_code == KeyCode.X) return true;
            if (_code == KeyCode.Y) return true;
            if (_code == KeyCode.Z) return true;
            if (_code == KeyCode.LeftBracket) return true;
            if (_code == KeyCode.RightBracket) return true;
            if (_code == KeyCode.Backslash) return true;
            if (_code == KeyCode.Semicolon) return true;
            if (_code == KeyCode.Quote) return true;
            if (_code == KeyCode.Comma) return true;
            if (_code == KeyCode.Period) return true;
            if (_code == KeyCode.Slash) return true;
            if (_code == KeyCode.Space) return true;
            return false;
        }

        if (joystricks[0].Contains("DS4") || joystricks[0].Contains("PC") || joystricks[0].Contains("Wireless"))
        {
            if (_code == KeyCode.JoystickButton0) return true;
            if (_code == KeyCode.JoystickButton1) return true;
            if (_code == KeyCode.JoystickButton2) return true;
            if (_code == KeyCode.JoystickButton3) return true;
            if (_code == KeyCode.JoystickButton4) return true;
            if (_code == KeyCode.JoystickButton5) return true;
            if (_code == KeyCode.JoystickButton6) return true;
            if (_code == KeyCode.JoystickButton7) return true;
            if (_code == KeyCode.JoystickButton10) return true;
            if (_code == KeyCode.JoystickButton11) return true;
        }
        else
        {
            if (_code == KeyCode.JoystickButton0) return true;
            if (_code == KeyCode.JoystickButton1) return true;
            if (_code == KeyCode.JoystickButton2) return true;
            if (_code == KeyCode.JoystickButton3) return true;
            if (_code == KeyCode.JoystickButton4) return true;
            if (_code == KeyCode.JoystickButton5) return true;
            if (_code == KeyCode.JoystickButton8) return true;
            if (_code == KeyCode.JoystickButton9) return true;
        }

        return false;
    }
    public void BtnClickEvent(int _type)
    {
        if (_type == 0) Application.OpenURL("https://store.steampowered.com/app/1231670/Chunker/");   // 스팀
        else if (_type == 1) Application.OpenURL("https://discord.gg/VQcZ8D3");                  // 디스코드
    }
    public GameObject KeyResetObj;
    public void KeyBoardReSetBtn()
    {
        KeyResetObj.SetActive(true);
    }
    public void ReSetClose()
    {
        KeyResetObj.SetActive(false);
    }
    public void ReSetActive()
    {
        FPSDisplay.instance.ReSetKeyBoard();
        KeyResetObj.SetActive(false);
    }

    private void GetKeyDownCheck(Action<bool> _keyDown)
    {
        if( Input.GetKeyDown(KeyCode.Mouse0         ) ||
            Input.GetKeyDown(KeyCode.Mouse1         ) ||
            Input.GetKeyDown(KeyCode.BackQuote      ) ||
            Input.GetKeyDown(KeyCode.BackQuote      ) ||
            Input.GetKeyDown(KeyCode.Alpha0         ) ||
            Input.GetKeyDown(KeyCode.Alpha1         ) ||
            Input.GetKeyDown(KeyCode.Alpha2         ) ||
            Input.GetKeyDown(KeyCode.Alpha3         ) ||
            Input.GetKeyDown(KeyCode.Alpha4         ) ||
            Input.GetKeyDown(KeyCode.Alpha5         ) ||
            Input.GetKeyDown(KeyCode.Alpha6         ) ||
            Input.GetKeyDown(KeyCode.Alpha7         ) ||
            Input.GetKeyDown(KeyCode.Alpha8         ) ||
            Input.GetKeyDown(KeyCode.Alpha9         ) ||
            Input.GetKeyDown(KeyCode.Minus          ) ||
            Input.GetKeyDown(KeyCode.Equals         ) ||
            Input.GetKeyDown(KeyCode.Tab            ) ||
            Input.GetKeyDown(KeyCode.CapsLock       ) ||
            Input.GetKeyDown(KeyCode.A              ) ||
            Input.GetKeyDown(KeyCode.B              ) ||
            Input.GetKeyDown(KeyCode.C              ) ||
            Input.GetKeyDown(KeyCode.D              ) ||
            Input.GetKeyDown(KeyCode.E              ) ||
            Input.GetKeyDown(KeyCode.F              ) ||
            Input.GetKeyDown(KeyCode.G              ) ||
            Input.GetKeyDown(KeyCode.H              ) ||
            Input.GetKeyDown(KeyCode.I              ) ||
            Input.GetKeyDown(KeyCode.J              ) ||
            Input.GetKeyDown(KeyCode.K              ) ||
            Input.GetKeyDown(KeyCode.L              ) ||
            Input.GetKeyDown(KeyCode.M              ) ||
            Input.GetKeyDown(KeyCode.N              ) ||
            Input.GetKeyDown(KeyCode.O              ) ||
            Input.GetKeyDown(KeyCode.P              ) ||
            Input.GetKeyDown(KeyCode.Q              ) ||
            Input.GetKeyDown(KeyCode.R              ) ||
            Input.GetKeyDown(KeyCode.S              ) ||
            Input.GetKeyDown(KeyCode.T              ) ||
            Input.GetKeyDown(KeyCode.U              ) ||
            Input.GetKeyDown(KeyCode.V              ) ||
            Input.GetKeyDown(KeyCode.W              ) ||
            Input.GetKeyDown(KeyCode.X              ) ||
            Input.GetKeyDown(KeyCode.Y              ) ||
            Input.GetKeyDown(KeyCode.Z              ) ||
            Input.GetKeyDown(KeyCode.LeftBracket    ) ||
            Input.GetKeyDown(KeyCode.RightBracket   ) ||
            Input.GetKeyDown(KeyCode.Backslash      ) ||
            Input.GetKeyDown(KeyCode.Semicolon      ) ||
            Input.GetKeyDown(KeyCode.Quote          ) ||
            Input.GetKeyDown(KeyCode.Comma          ) ||
            Input.GetKeyDown(KeyCode.Period         ) ||
            Input.GetKeyDown(KeyCode.Slash          ) ||
            Input.GetKeyDown(KeyCode.UpArrow        ) ||
            Input.GetKeyDown(KeyCode.DownArrow      ) ||
            Input.GetKeyDown(KeyCode.RightArrow     ) ||
            Input.GetKeyDown(KeyCode.LeftArrow      ) ||
            Input.GetKeyDown(KeyCode.Space          ))
        {
            _keyDown(true);
        }
        else
        {
            if (joystricks.Count == 0) return;
            if (joystricks[0].Contains("DS4") || joystricks[0].Contains("PC") || joystricks[0].Contains("Wireless"))
            {
                float joyCrossX = Input.GetAxisRaw("PS4_crossX");
                float joyCrossY = Input.GetAxisRaw("PS4_crossY");

                if (Input.GetKeyDown(KeyCode.JoystickButton0) ||
                    Input.GetKeyDown(KeyCode.JoystickButton1) ||
                    Input.GetKeyDown(KeyCode.JoystickButton2) ||
                    Input.GetKeyDown(KeyCode.JoystickButton3) ||
                    Input.GetKeyDown(KeyCode.JoystickButton4) ||
                    Input.GetKeyDown(KeyCode.JoystickButton5) ||
                    Input.GetKeyDown(KeyCode.JoystickButton6) ||
                    Input.GetKeyDown(KeyCode.JoystickButton7) ||
                    Input.GetKeyDown(KeyCode.JoystickButton10) ||
                    Input.GetKeyDown(KeyCode.JoystickButton11) ||
                    joyCrossY > 0 || joyCrossY < 0 || 
                    joyCrossX > 0 || joyCrossX < 0
                    )
                {
                    _keyDown(false);
                }
            }
            else
            {
                float joyCrossX = Input.GetAxisRaw("Horizontal_joy");
                float joyCrossY = Input.GetAxisRaw("Vertical_joy");

                if (Input.GetKeyDown(KeyCode.JoystickButton0) ||
                    Input.GetKeyDown(KeyCode.JoystickButton1) ||
                    Input.GetKeyDown(KeyCode.JoystickButton2) ||
                    Input.GetKeyDown(KeyCode.JoystickButton3) ||
                    Input.GetKeyDown(KeyCode.JoystickButton4) ||
                    Input.GetKeyDown(KeyCode.JoystickButton5) ||
                    Input.GetKeyDown(KeyCode.JoystickButton8) ||
                    Input.GetKeyDown(KeyCode.JoystickButton9) ||
                    joyCrossY > 0 || joyCrossY < 0 || 
                    joyCrossX > 0 || joyCrossX < 0
                    )
                {
                    _keyDown(false);
                }
            }
        }
    }
}
