﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class ScreenShot : MonoBehaviour
{
    public static ScreenShot instance;

    public Camera camera;       //보여지는 카메라.
    public Image capImg;
    private int resWidth;
    private int resHeight;
    string path;
    public bool isTestScreenshot;

    private void Awake()
    {
        if (instance == null) instance = this;
    }

    // Use this for initialization
    void Start()
    {
        resWidth = 640;
        resHeight = 360;
        path = Application.dataPath + "/ScreenShot/";
    }

    private void Update()
    {

        if(isTestScreenshot)
        {
            isTestScreenshot = false;
            StartCoroutine(ClickScreenShot());
        }
    }
    public void Screenshot(GameObject target)
    {
        camera.GetComponent<ScreenShotCamera>().target = target;
        camera.transform.position = target.transform.position;
        StartCoroutine(ClickScreenShot());
    }
    IEnumerator ClickScreenShot()
    {
        yield return null;
        DirectoryInfo dir = new DirectoryInfo(path);
        if (!dir.Exists)
        {
            Directory.CreateDirectory(path);
        }
        string name;
        name = path + System.DateTime.Now.ToString("yyyyMMddHHmmss") + ".png";
        RenderTexture rt = new RenderTexture(resWidth, resHeight, 24);
        camera.targetTexture = rt;
        Texture2D screenShot = new Texture2D(resWidth, resHeight, TextureFormat.ARGB32, false);
        Rect rec = new Rect(0, 0, screenShot.width, screenShot.height);
        camera.Render();
        RenderTexture.active = rt;
        screenShot.ReadPixels(new Rect(0, 0, resWidth, resHeight), 0, 0);
        screenShot.Apply();
        //byte[] bytes = screenShot.EncodeToPNG();
        //File.WriteAllBytes(name, bytes);
        capImg.sprite = Sprite.Create(screenShot, new Rect(0, 0, resWidth, resHeight), new Vector2(0.5f, 0.5f)); ;

    }
}
