﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShopBuyInfo : MonoBehaviour
{
    public static ShopBuyInfo instance;
    [SerializeField] private Text nameText;
    [SerializeField] private Text contText;
    [SerializeField] private Image itemImage;
    [SerializeField] private GameObject infoObj;
    [SerializeField] private bool isSelected = false;
    [SerializeField] private Transform target = null;
    [SerializeField] private Image[] StateImages;
    [SerializeField] private Text[] StateValues;
    [SerializeField] private Sprite[] stateIcons;

    Coroutine openCoroutine = null;
    Coroutine openCoroutine2 = null;
    private void Awake()
    {
        if (instance == null) instance = this;
    }
    public void ShowInfo(Transform _transform, string _id)
    {
        ItemsData iData = Array.Find(SheetManager.Instance.ItemsDB.dataArray, item => item.ID.Equals(_id));
        isSelected = true;
        target = _transform;

        string tempID = string.Empty;
        if (iData.Grade.Equals("S") || iData.Grade.Equals("G")) tempID += "<color=#ff3700>";
        else if (iData.Grade.Equals("A")) tempID += "<color=#ffe500>";
        else if (iData.Grade.Equals("B")) tempID += "<color=#00ffe0>";
        else if (iData.Grade.Equals("C")) tempID += "<color=#1dff1d>";
        else if (iData.Grade.Equals("D")) tempID += "<color=#999BFF>";
        else if (iData.Grade.Equals("E")) tempID += "<color=#FFFFFF>";
        else if (iData.Grade.Equals("F")) tempID += "<color=#A0A0A0>";
        tempID += LocalizeManager.GetLocalize(iData.ID) + "</color>";

        nameText.text = tempID;
        contText.text = LocalizeManager.GetLocalize(iData.ID + "_cont");
        itemImage.sprite = Resources.Load<Sprite>("Item/" + iData.Type1 + "/" + iData.Type2 + "/" + iData.ID);

        if (iData.ID.Contains("q_change") || iData.ID.Contains("cell_shield"))
        {
            StateValues[0].text = "-";
            StateValues[1].text = "-";
            StateValues[2].text = "-";
            StateValues[3].text = "-";
            StateValues[4].text = "-";
            StateValues[5].text = "-";
            StateValues[6].text = "-";
            StateValues[7].text = "-";
        }
        else if (iData.Type1.Equals("Weapon"))
        {
            if (iData.Attacktype.Equals("M"))
            {
                StateImages[0].sprite = Array.Find(stateIcons, item => item.name.Equals("item_icon_attack_m"));
                StateValues[0].text = iData.Damagenear[0] + " - " + iData.Damagenear[1];
            }
            else
            {
                StateImages[0].sprite = Array.Find(stateIcons, item => item.name.Equals("item_icon_attack_l"));
                StateValues[0].text = iData.Damage[0] + " - " + iData.Damage[1];
            }
            StateValues[1].text = iData.Critical.ToString();
            StateValues[2].text = iData.Criticaldamage.ToString();
            StateValues[3].text = iData.Range.ToString();
            StateValues[4].text = iData.Aim.ToString();
            StateValues[5].text = iData.Multishoot.ToString();
            StateValues[6].text = iData.Shootcount.ToString();
            StateValues[7].text = iData.Shootdelay.ToString();
        }
        else if(iData.Type1.Equals("Passive"))
        {
            StateValues[0].text = "-";
            StateValues[1].text = "-";
            StateValues[2].text = "-";
            StateValues[3].text = "-";
            StateValues[4].text = "-";
            StateValues[5].text = "-";
            StateValues[6].text = "-";
            StateValues[7].text = "-";
            if (iData.ID.Contains("blasting_fire")) StateValues[5].text = iData.Buffamount[0].ToString();
            if (iData.ID.Contains("continuous_shooting")) StateValues[6].text = iData.Buffamount[0].ToString();
            if (iData.ID.Contains("nerve_enhancer_arm")) StateValues[0].text = iData.Buffamount[1].ToString();
            if (iData.ID.Contains("nerve_enhancer_spine")) StateValues[7].text = iData.Buffamount[0].ToString();
            if (iData.ID.Contains("nerve_enhancer_leg")) StateValues[0].text = iData.Buffamount[1].ToString();
            if (iData.ID.Contains("nerve_enhancer_head")) StateValues[4].text = iData.Buffamount[0].ToString();
            if (iData.ID.Contains("muscle_enhancer_arm")) StateValues[0].text = iData.Buffamount[0].ToString();
            if (iData.ID.Contains("nerve_enhancer_eyes")) StateValues[3].text = iData.Buffamount[0].ToString();
            if (iData.ID.Contains("electric_cell")) StateValues[0].text = iData.Buffamount[1].ToString();
            if (iData.ID.Contains("dex_fire"))
            {
                StateValues[4].text = iData.Buffamount[1].ToString();
                StateValues[0].text = iData.Buffamount[2].ToString();
                StateValues[1].text = iData.Buffamount[3].ToString();
            }
            if (iData.ID.Contains("increase_cri_damage")) StateValues[1].text = iData.Buffamount[0].ToString();
            if (iData.ID.Contains("attack_m_manual"))
            {
                StateImages[0].sprite = Array.Find(stateIcons, item => item.name.Equals("item_icon_attack_m"));
                StateValues[0].text = iData.Buffamount[0].ToString();
            }
            if (iData.ID.Contains("attack_r_manual"))
            {
                StateImages[0].sprite = Array.Find(stateIcons, item => item.name.Equals("item_icon_attack_l"));
                StateValues[0].text = iData.Buffamount[0].ToString();
            }
        }
        else
        {
            StateValues[0].text = "-";
            StateValues[1].text = "-";
            StateValues[2].text = "-";
            StateValues[3].text = "-";
            StateValues[4].text = "-";
            StateValues[5].text = "-";
            StateValues[6].text = "-";
            StateValues[7].text = "-";
        }
        if (openCoroutine2 != null)
        {
            StopCoroutine(openCoroutine2);
            openCoroutine2 = null;
        }

        if (openCoroutine == null)
        {
            openCoroutine = StartCoroutine(OpenCoroutine());
        }

    }
    IEnumerator OpenCoroutine()
    {
        Vector3 pos = infoObj.GetComponent<RectTransform>().anchoredPosition3D;
        if (pos.y <= 0)
        {
            while(true)
            {
                pos.y += 10f;
                infoObj.GetComponent<RectTransform>().anchoredPosition3D = pos;
                if (pos.y >= 0)
                {
                    infoObj.GetComponent<RectTransform>().anchoredPosition3D = Vector3.zero;
                    break;
                }
                yield return null;
            }
        }
        openCoroutine = null;
    }
    public void HideInfo()
    {

        if (openCoroutine != null)
        {
            StopCoroutine(openCoroutine);
            openCoroutine = null;
        }
        StartCoroutine(CloseCoroutine());
    }

    IEnumerator CloseCoroutine()
    {
        isSelected = false;
        target = null;
        Vector3 pos = infoObj.GetComponent<RectTransform>().anchoredPosition3D;

        while (true)
        {
            pos.y -= 10f;
            infoObj.GetComponent<RectTransform>().anchoredPosition3D = pos;
            if (pos.y <= -268)
            {
                infoObj.GetComponent<RectTransform>().anchoredPosition3D = new Vector3(0, -268, 0);
                break;
            }
            yield return null;
        }
    }

    public bool BuyItem(string _id)
    {
        ItemsData iData = Array.Find(SheetManager.Instance.ItemsDB.dataArray, item => item.ID.Equals(_id));
        PriceData pData = Array.Find(SheetManager.Instance.PriceDB.dataArray, item => item.Grade.Equals(iData.Grade));
        int price = pData.Price;
        if (PDG.Player.instance.isGold_Level_2)
        {
            price = Mathf.FloorToInt(price + (price * 0.3f));
        }
        if (Inventorys.Instance.goldbar < price) return false;
        Inventorys.Instance.goldbar -= price;
        return true;
    }
}
