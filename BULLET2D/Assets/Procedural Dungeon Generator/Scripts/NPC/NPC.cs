﻿using MOB;
using PDG;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NPC : MonoBehaviour
{
    public string mobType;
    string _spritePath;
    Sprite[] _sprites;
    private SpriteRenderer sr;
    public Animator animator;
    GameObject pressKey;
    GameObject dialogue;
    public bool isSelected = false;
    public bool isCommentStart = false;

    [Header("Monster Spawn")]
    public bool isMonsterSpawn = false;
    public bool isOFF = false;
    public string spawnMobName = string.Empty;

    public bool isRandomComment = false;
    public int randomMaxCount = 0;
    public AudioClip[] clips;
    public bool isRandomMotion = false;
    public float randomTime = 0.0f;

    [SerializeField] private AudioSource audio;
    [SerializeField] private float fTime = 3.0f;
    [SerializeField] private GameObject timerObj;
    public bool isUsing = false;
    private void Awake()
    {
        animator = GetComponent<Animator>();
        sr = GetComponent<SpriteRenderer>();
        audio = GetComponent<AudioSource>();
        pressKey = GameObject.Find("Canvas").transform.Find("PressKey").gameObject;
    }
    // Start is called before the first frame update
    void Start()
    {
        if (isMonsterSpawn) clips = Resources.LoadAll<AudioClip>("Object/Dungeon_2_Computer");

        timerObj = GameObject.Find("Canvas").transform.Find("Timer").gameObject;
        string stage = PDG.Dungeon.instance.StageName;
        int level = Dungeon.instance.StageLevel;
        MonstersData[] mobDatas = Array.FindAll(SheetManager.Instance.MonsterDB.dataArray, item => item.Type.Equals("subboss") && (level == 0 ? !item.ID.Contains("_t") : item.ID.Contains("_t" + (level + 1))) && !item.ID.Contains("radishman"));
        int t = UnityEngine.Random.Range(0, mobDatas.Length);
        if (t == Dungeon.prevEliteNum)
        {
            while(true)
            {
                t = UnityEngine.Random.Range(0, mobDatas.Length);
                if (t != Dungeon.prevEliteNum) break;
            }
        }
        Dungeon.prevEliteNum = t;
        if (mobDatas.Length > 0) spawnMobName = mobDatas[t].ID;
        Load();
    }
    private void Update()
    {
        if (PDG.Player.instance == null) return;

        if(isRandomMotion)
        {
            randomTime -= Time.deltaTime;
            if (randomTime <= 0.0f)
            {
                int r = UnityEngine.Random.Range(0, 2);
                animator.SetInteger("RandomSelect", r);
                animator.SetTrigger("RandomChange");
                randomTime = UnityEngine.Random.Range(10, 30);
                if (r == 0) GetComponent<AudioSource>().clip = clips[1];
                else GetComponent<AudioSource>().clip = clips[0];
                GetComponent<AudioSource>().Play();
            }
            float dis = Vector3.Distance(PDG.Player.instance.transform.position, transform.position);
            if (dis > 10) GetComponent<AudioSource>().volume = 0;
            float tVolume =  (SoundManager.isMute || (PDG.Dungeon.instance == null ? false : PDG.Dungeon.instance.isPause) || (PDG.Player.instance == null ? false : PDG.Player.instance.isDie)) ? 0 : SoundManager.EffectVolume;
            float t = tVolume / 10;
            GetComponent<AudioSource>().volume = tVolume - (dis * t);
        }
        if (isSelected && !isCommentStart && !isOFF)
        {
            if (pressKey.GetComponent<SpriteRenderer>()) pressKey.GetComponent<SpriteRenderer>().sprite = PDG.Dungeon.instance.isJoystrick ? PDG.Dungeon.GetJoyStrickKeySprite(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_ACTIVE]) : PDG.Dungeon.GetJoyStrickKeySprite(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ACTIVE]);
            else if (pressKey.GetComponent<Image>()) pressKey.GetComponent<Image>().sprite = PDG.Dungeon.instance.isJoystrick ? PDG.Dungeon.GetJoyStrickKeySprite(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_ACTIVE]) : PDG.Dungeon.GetJoyStrickKeySprite(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ACTIVE]);
            pressKey.SetActive(true);
            Vector3 pos = transform.position;
            pos.y += 1.8f;
            pressKey.transform.position = UnityEngine.Camera.main.WorldToScreenPoint(pos);
            if (!animator.GetBool("isSelected")) animator.SetBool("isSelected", true);
            if (isMonsterSpawn) TextOpenController.instance.ShowText(LocalizeManager.GetLocalize("eliteMonsterSelect"), Color.white, 24, gameObject, 0.5f, true);
            if ((PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ACTIVE], true) || PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_ACTIVE], true)) && !PDG.Dungeon.instance.isPause && !isUsing && Dungeon.instance.closeCoolTime > 0.2f) 
            {
                Dungeon.instance.closeCoolTime = 0.0f;
                SoundManager.instance.StartAudio(new string[1] { "Click_0" }, VOLUME_TYPE.EFFECT);
                isSelected = false;
                pressKey.SetActive(false);

                string mType = mobType;
                if(isRandomComment)
                {
                    mType = mobType + "_" + UnityEngine.Random.Range(0, randomMaxCount);
                }
                if (!isMonsterSpawn)
                {
                    Dialogue.Instance.CommnetSetting(mType, 0, this);
                    isCommentStart = true;
                }
                else
                {
                    if (TextOpenController.instance.targetObj == gameObject) TextOpenController.instance.TextClose(Color.white);
                    isCommentStart = true;
                    isUsing = true;
                    PlayerCombat.instance.tCoroutine = StartCoroutine(TimerCoroutine());
                }

                if (isRandomComment)
                {
                    animator.SetTrigger("Off");
                    isOFF = true;
                }
            }
        }

        if (timerObj.activeSelf && (Input.GetKeyDown(KeyCode.Escape) || Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_CANCLE], true)) && Dungeon.instance.closeCoolTime > 0.2f) 
        {
            Dungeon.instance.closeCoolTime = 0.0f;
            Dungeon.instance.newObjectSelectedObj = null;
            Dungeon.instance.newObjectSelected = false;
            isUsing = false;
            isCommentStart = false;
            isSelected = false;
            pressKey.SetActive(false);
            if (animator.GetBool("isSelected")) animator.SetBool("isSelected", false);
            PlayerCombat.instance.tCoroutine = null;
        }

        if (!Dungeon.instance.isSelect)
        {
            if (TextOpenController.instance.targetObj == gameObject)
            {
                TextOpenController.instance.TextClose(Color.white);
                timerObj.SetActive(false);
            }
            isCommentStart = false;
            isSelected = false;
            isUsing = false;
            pressKey.SetActive(false);
            if (animator.GetBool("isSelected")) animator.SetBool("isSelected", false);
        }
    }
    void LateUpdate()
    {
        if (sr == null || sr.sprite == null)
            return;
        Load();
        var name = sr.sprite.name;
        var sprite = System.Array.Find(_sprites, item => item.name == name);
        if (sprite) sr.sprite = sprite;
    }
    void Load()
    {
        var spritePath = "NPC/" + (mobType.Contains("npc_mr_dark") ? "npc_mr_dark" : mobType);
        if (!spritePath.Equals(_spritePath))
        {
            _spritePath = spritePath;
            _sprites = Resources.LoadAll<Sprite>(_spritePath);
        }
    }
    private void MonsterSpawn()
    {
        MonstersData mobData = Array.Find(SheetManager.Instance.MonsterDB.dataArray, item => item.ID.Equals(spawnMobName));
        GameObject obj = ObjManager.Call().GetObject("Monster");
        Vector3 pos = transform.position;
        pos.x -= 2.0f;
        obj.transform.position = pos;
        obj.GetComponent<MonsterArggro>().InitSetting(Player.instance.gameObject, mobData, 600, false, mobData.Animtype);
        obj.GetComponent<MonsterArggro>().isWait = true;
        obj.SetActive(true);
        SoundManager.instance.StartAudio(new string[1] { "Monster/bossBGM" }, VOLUME_TYPE.BGM, null, true);

        string tId = mobData.ID;
        if (tId.Contains("_t")) tId = tId.Substring(0, tId.Length - 3);
        Conversation.Instance.StartEffect(obj.transform, LocalizeManager.GetLocalize("monster_" + tId), LocalizeManager.GetLocalize("monster_sub_" + tId), true, true, obj);
        if (transform.Find("elite_monster_minimap")) transform.Find("elite_monster_minimap").gameObject.SetActive(false);
        Dungeon.isEliteSpawn = true;
        Dungeon.isEliteSpawnWait = true;
    }
    public void HealthStart()
    {
        if (PDG.Player.curHp < 10)
        {
            SoundManager.instance.StartAudio(new string[1] { "Object/healthtower" }, VOLUME_TYPE.AMBIENT);
            PlayerCombat.ps = PlayerState.Health;
        }
    }
    public void HealthEnd()
    {
        if (PDG.Player.curHp < 10)
        {
            SoundManager.instance.StartAudio(new string[1] { "Object/healthtower_end" }, VOLUME_TYPE.AMBIENT);
            SoundManager.instance.StopAudio(new string[1] { "Object/healthtower" });
            PlayerCombat.ps = PlayerState.Idle;
            PDG.Player.instance.HpRecovery(1);
            StartCoroutine(SpakeCoroutine());
        }
    }
    IEnumerator SpakeCoroutine()
    {
        float a = 0.0f;
        UnityEngine.UI.Image img = GameObject.Find("Canvas").transform.Find("Spake").GetComponent<UnityEngine.UI.Image>();
        while (true)
        {
            img.color = new Color(1, 1, 1, a);
            a += Time.deltaTime * 5;
            if (a >= 1.0f) break;
            yield return null;
        }
        img.color = Color.white;
        while (true)
        {
            img.color = new Color(1, 1, 1, a);
            a -= Time.deltaTime * 5;
            if (a <= 0.0f) break;
            yield return null;
        }
        img.color = new Color(1, 1, 1, 0);
    }
    IEnumerator TimerCoroutine()
    {
        Dungeon.instance.newObjectSelectedObj = gameObject;
        Dungeon.instance.newObjectSelected = true;
        timerObj.SetActive(true);
        float t = 0.0f;
        while (isUsing)
        {
            Vector3 p = transform.position;
            p.y += 1.7f;
            timerObj.transform.position = UnityEngine.Camera.main.WorldToScreenPoint(p);

            if (audio && !audio.isPlaying)
            {
                audio.clip = clips[UnityEngine.Random.Range(0, clips.Length)];
                audio.volume =  (SoundManager.isMute || (PDG.Dungeon.instance == null ? false : PDG.Dungeon.instance.isPause) || (PDG.Player.instance == null ? false : PDG.Player.instance.isDie)) ? 0 : SoundManager.EffectVolume;
                audio.Play();
            }
            t += Time.deltaTime;
            int tInt = Mathf.CeilToInt(fTime - t);
            timerObj.transform.Find("Text").GetComponent<Text>().text = tInt.ToString();
            timerObj.transform.Find("Image").GetComponent<Image>().fillAmount = t / fTime;
            timerObj.transform.Find("TextCont").GetComponent<Text>().text = LocalizeManager.GetLocalize("spawning");
            if (t >= fTime) break;
            yield return new WaitForSeconds(Time.deltaTime);
        }
        audio.Stop();
        if (isUsing)
        {
            MonsterSpawn();
            animator.SetTrigger("Off");
            isOFF = true;
        }
        isSelected = false;
        isCommentStart = false;
        pressKey.SetActive(false);
        timerObj.SetActive(false);
        Dungeon.instance.newObjectSelectedObj = null;
        Dungeon.instance.newObjectSelected = false;
        PlayerCombat.instance.tCoroutine = null;
    }
}
