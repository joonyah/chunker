﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UnLock : MonoBehaviour
{
    public static UnLock Instance;
    [SerializeField] private GameObject UIObj;
    [SerializeField] private Transform itemparent;
    [SerializeField] private GameObject ListItem;
    [SerializeField] private Text nameText;
    [SerializeField] private Text SkullCoin;
    [SerializeField] private Text GoldCoin;
    [SerializeField] private Scrollbar _scrollbar;
    [SerializeField] private RectTransform rt;
    public List<UnLockItem> ulList = new List<UnLockItem>();

    public Image infoImage;
    public Text infoTitle;
    public Text infoCont;

    public bool isOpen = false;
    public Canvas mycanvas; // raycast가 될 캔버스

    private void Awake()
    {
        if (Instance == null) Instance = this;
    }

    private void Update()
    {
        if(isOpen)
        {
            if ((Input.GetKeyDown(KeyCode.Escape) || Input.GetKeyDown(KeyCode.Backspace) || PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_CANCLE], true)) && PDG.Dungeon.instance.closeCoolTime > 0.2f) 
            {
                SoundManager.instance.StartAudio(new string[1] { "Click_0" }, VOLUME_TYPE.EFFECT);
                UIObj.SetActive(false);
                isOpen = false;
                Time.timeScale = 1;
                GetComponent<Image>().enabled = false;
                PDG.Dungeon.instance.closeCoolTime = 0.0f;
            }

            if (rt.anchoredPosition.y > -(ulList.Count * 100)) rt.anchoredPosition = new Vector2(0, -(ulList.Count * 100));
            if(rt.anchoredPosition.y < -700) rt.anchoredPosition = new Vector2(0, -700);

        }
        SkullCoin.text = Inventorys.Instance.glowindskull.ToString();
        GoldCoin.text = Inventorys.Instance.goldbar.ToString();
    }

    public void SettingUnLock(string _npcID, string _npcName)
    {
        Clear();
        nameText.text = _npcName;
        UnlockListsData[] uDatas = System.Array.FindAll(SheetManager.Instance.UnlockListsDB.dataArray, item => item.NPCID.Equals(_npcID));
        for (int i = 0; i < uDatas.Length; i++) 
        {
            GameObject obj = Instantiate(ListItem);
            obj.GetComponent<UnLockItem>().InitSetting(uDatas[i]);
            obj.transform.SetParent(itemparent);
            obj.transform.localScale = Vector3.one;
            ulList.Add(obj.GetComponent<UnLockItem>());
        }
        UIObj.SetActive(true);
        isOpen = true;
        _scrollbar.value = 1;
        rt.anchoredPosition = new Vector2(0, -(ulList.Count * 100));
        Time.timeScale = 0;
        GetComponent<Image>().enabled = true;
    }
    private void Clear()
    {
        for (int i = 0; i < ulList.Count; i++)
        {
            Destroy(ulList[i].gameObject);
        }
        ulList.Clear();
        infoImage.color = new Color(0, 0, 0, 0);
        infoTitle.text = string.Empty;
        infoCont.text = string.Empty;
    }
}
