﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Guide : MonoBehaviour
{
    GameObject pressKey;
    [SerializeField] private Sprite noneSprite;
    [SerializeField] private Sprite outlineSprite;
    bool isSelected = false;
    bool isComment = false;
    SpriteRenderer sr;
    [SerializeField] private string dialogueID;
    [SerializeField] private bool isReturn;
    [SerializeField] private GameObject returnObj;
    private void Start()
    {
        sr = GetComponent<SpriteRenderer>();
        pressKey = GameObject.Find("Canvas").transform.Find("PressKeyGuide").gameObject;
    }
    private void Update()
    {
        RaycastHit2D hit = Physics2D.BoxCast(transform.position, new Vector2(0.8f, 1.8f), 0, new Vector2(0.15f, -0.2f), 1, 1 << 9);
        if(hit)
        {
            pressKey.SetActive(true);
            isSelected = true;
            sr.sprite = outlineSprite;
            pressKey.transform.position = Camera.main.WorldToScreenPoint(transform.position);

            if ((PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ACTIVE], true) || PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_CONFIRM], true)) && PDG.Dungeon.instance.closeCoolTime > 0.2f) 
            {
                PDG.Dungeon.instance.closeCoolTime = 0.0f;
                SoundManager.instance.StartAudio(new string[1] { "Object/active_F" }, VOLUME_TYPE.EFFECT);
                if (!isReturn) Dialogue.Instance.CommnetSetting(dialogueID, 0, null);
                else PDG.Player.instance.transform.position = returnObj.transform.position;
            }
        }
        else
        {
            if (isSelected)
            {
                pressKey.SetActive(false);
                sr.sprite = noneSprite;
            }
        }
    }
}
