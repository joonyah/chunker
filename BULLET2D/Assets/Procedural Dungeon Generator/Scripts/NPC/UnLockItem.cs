﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class UnLockItem : MonoBehaviour
{
    [SerializeField] private Image Icon;
    [SerializeField] private Image ProgressImage;
    public int nowCount;
    [SerializeField] private int maxCount;
    [SerializeField] private Text countText;
    [SerializeField] private bool isClear = false;
    [SerializeField] private Button btn;
    public string nowUnlockID = string.Empty;
    [SerializeField] private Sprite unlockSprite;
    [SerializeField] private Sprite lockSprite;
    [SerializeField] private Image clearImage;
    [SerializeField] private bool isClick = false;
    [SerializeField] private float pushTime = 0.1f;
    [SerializeField] private float pushTimeNow = 0.0f;
    [SerializeField] private bool isFirst = false;
    [SerializeField] private long pushCheck = 0;
    [SerializeField] private long pushCheck2 = 0;

    GraphicRaycaster gr;
    PointerEventData ped;
    // Use this for initialization
    void Start()
    {
        gr = UnLock.Instance.mycanvas.GetComponent<GraphicRaycaster>();
        ped = new PointerEventData(null);
    }

    private void Update()
    {
        if(isFirst)
        {
            ped.position = Input.mousePosition;
            List<RaycastResult> results = new List<RaycastResult>(); // 여기에 히트 된 개체 저장
            gr.Raycast(ped, results);
            if (results.Count != 0)
            {
                GameObject obj = results[0].gameObject;
                if (obj == gameObject)
                {
                    if (PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ATTACK], false) || PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_ATTACK], false))
                    {
                        isClick = true;
                        pushCheck++;
                    }
                }
                else
                {
                    isClick = false;
                }
            }

            if (isClick)
            {
                if (pushCheck == pushCheck2)
                {
                    isClick = false;
                    pushCheck = 0;
                }
                else
                {
                    pushTimeNow += Time.unscaledDeltaTime;
                    if (pushTimeNow > pushTime)
                    {
                        ClickEvent();
                        pushTimeNow = 0.0f;
                    }
                }
                pushCheck2 = pushCheck;
            }
            else
            {
                pushCheck = 0;
                pushTimeNow = 0.0f;
            }
        }
    }

    public void InitSetting(UnlockListsData _data)
    {
        ItemsData _item = System.Array.Find(SheetManager.Instance.ItemsDB.dataArray, item => item.ID.Equals(_data.Itemid));
        Icon.sprite = Resources.Load<Sprite>("Item/" + _item.Type1 + "/" + _item.Type2 + "/" + _item.ID);
        nowUnlockID = _item.ID;
        nowCount = SaveAndLoadManager.instance.LoadUnlock(nowUnlockID);
        maxCount = _data.Unlockcount;
        ProgressImage.fillAmount = (float)nowCount / (float)maxCount;
        countText.text = (maxCount - nowCount).ToString();
        BtnClear();
        if (nowCount == maxCount)
        {
            isClear = true;
            ProgressImage.fillAmount = 1;
            countText.gameObject.SetActive(false);
            clearImage.sprite = unlockSprite;
        }
        else
        {
            clearImage.sprite = lockSprite;
        }

    }
    public void BtnClear()
    {
        btn.onClick.RemoveAllListeners();
        btn.onClick.AddListener(() => { FirstClick(); });
    }
    public void BtnNext()
    {
        btn.onClick.RemoveAllListeners();
        btn.onClick.AddListener(() => { ClickEvent(); });
    }
    public void FirstClick()
    {
        SoundManager.instance.StartAudio(new string[1] { "Click_0" }, VOLUME_TYPE.EFFECT);
        for (int i = 0; i < UnLock.Instance.ulList.Count; i++)
        {
            if (UnLock.Instance.ulList[i] == this)
            {
                UnLock.Instance.ulList[i].transform.Find("Image").gameObject.SetActive(true);
                UnLock.Instance.ulList[i].isFirst = true;
                UnLock.Instance.ulList[i].BtnNext();

                ItemsData _item = System.Array.Find(SheetManager.Instance.ItemsDB.dataArray, item => item.ID.Equals(nowUnlockID));
                UnLock.Instance.infoImage.sprite = UnLock.Instance.ulList[i].Icon.sprite;
                UnLock.Instance.infoImage.color = Color.white;
                UnLock.Instance.infoTitle.text = LocalizeManager.GetLocalize(_item.ID);
                UnLock.Instance.infoCont.text = LocalizeManager.GetLocalize(_item.ID + "_cont");
            }
            else
            {
                UnLock.Instance.ulList[i].isFirst = false;
                UnLock.Instance.ulList[i].transform.Find("Image").gameObject.SetActive(false);
                UnLock.Instance.ulList[i].BtnClear();
            }
        }
    }
    public void ClickEvent()
    {
        isClick = true;
        SoundManager.instance.StartAudio(new string[1] { "Click_0" }, VOLUME_TYPE.EFFECT);
        if (nowCount == maxCount) return;
        if (Inventorys.Instance.glowindskull <= 0) return;
        nowCount++;
        Inventorys.Instance.glowindskull--;
        SaveAndLoadManager.instance.SaveItem(new ItemSave {
            _id = "glowing_skull",
            _nCount = -1
        });
        if (nowCount == maxCount)
        {
            isClear = true;
            ProgressImage.fillAmount = 1;
            countText.gameObject.SetActive(false);
            clearImage.sprite = unlockSprite;
            Inventorys.Instance.ShowGetItemUI("UnLockComment", Icon.sprite);
            //Inventorys.Instance.DropItem(nowUnlockID, 1);
        }
        else
        {
            ProgressImage.fillAmount = (float)nowCount / (float)maxCount;
            countText.text = (maxCount - nowCount).ToString();
            clearImage.sprite = lockSprite;
        }
        SaveAndLoadManager.instance.SaveUnlock(nowUnlockID, nowCount);
    }
}
