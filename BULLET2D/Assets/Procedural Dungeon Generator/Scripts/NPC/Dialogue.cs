﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class Dialogue : MonoBehaviour
{
    public static Dialogue Instance;
    public GameObject dialogueObj;
    public GameObject ChiocePanel;
    public Text[] Chioces;
    public Text NameText;
    public Text ContentText;
    public bool isCommnet = false;
    NPCListsData[] nDatas = null;
    [SerializeField] private int nowTextNum = 0;
    [SerializeField] bool isSkip = false;
    Coroutine commentCoroutine = null;
    [SerializeField] string nowNPC = string.Empty;
    [SerializeField] int nowChiocesNum = 0;
    [SerializeField] bool isChiocesSelected = false;
    [SerializeField] bool isChiocesSelected2 = false;

    public Canvas mycanvas;
    GraphicRaycaster gr;
    PointerEventData ped;
    public GameObject offObj;

    NPC nowNPCScript = null;
    bool change = false;
    public bool tutorialCharacterChange = false;
    public Image spakeImage;
    public Sprite reinforceSprite;
    public Sprite mrDarkSprites;
    public Sprite helldogSpriite;

    public BossWaitController bwc;
    public bool isBossEnd = false;
    GameObject nowObj;
    private void Awake()
    {
        if (Instance == null) Instance = this;
    }
    private void Start()
    {
        gr = mycanvas.GetComponent<GraphicRaycaster>();
        ped = new PointerEventData(null);
    }
    private void Update()
    {
        if (isCommnet)
        {
            if ((Input.GetKeyDown(KeyCode.Escape) || Input.GetKeyDown(KeyCode.Backspace) || PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_CANCLE], true)) && PDG.Dungeon.instance.closeCoolTime > 0.2f) 
            {
                PDG.Dungeon.instance.closeCoolTime = 0.0f;
                ClickEvent();
                SoundManager.instance.StartAudio(new string[1] { "Click_0" }, VOLUME_TYPE.EFFECT);
            }

            if ((PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ACTIVE], true) || Input.GetKeyDown(KeyCode.Space) || PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_CONFIRM], true)) && !isChiocesSelected && PDG.Dungeon.instance.closeCoolTime > 0.2f) 
            {
                PDG.Dungeon.instance.closeCoolTime = 0.0f;
                ClickEvent();
            }
        }
        if (isChiocesSelected)
        {
            float x = Input.GetAxis("Horizontal_joy");
            float y = Input.GetAxis("Vertical_joy");

            if ((Input.GetKeyDown(KeyCode.UpArrow) || PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ARROW_UP], true) || y > 0) && PDG.Dungeon.instance.closeCoolTime > 0.2f)
            {
                PDG.Dungeon.instance.closeCoolTime = 0.0f;
                nowChiocesNum--;
            }
            if ((Input.GetKeyDown(KeyCode.LeftArrow) || PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ARROW_LEFT], true) || x < 0) && PDG.Dungeon.instance.closeCoolTime > 0.2f)
            {
                PDG.Dungeon.instance.closeCoolTime = 0.0f;
                nowChiocesNum--;
            }
            if ((Input.GetKeyDown(KeyCode.DownArrow) || PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ARROW_DOWN], true) || y < 0) && PDG.Dungeon.instance.closeCoolTime > 0.2f)
            {
                PDG.Dungeon.instance.closeCoolTime = 0.0f;
                nowChiocesNum++;
            }
            if ((Input.GetKeyDown(KeyCode.RightArrow) || PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ARROW_RIGHT], true) || x > 0) && PDG.Dungeon.instance.closeCoolTime > 0.2f)
            {
                PDG.Dungeon.instance.closeCoolTime = 0.0f;
                nowChiocesNum++;
            }

            if (nowChiocesNum < 0) nowChiocesNum = nDatas.Length - 1;
            if (nowChiocesNum >= nDatas.Length) nowChiocesNum = 0;

            ped.position = Input.mousePosition;
            List<RaycastResult> results = new List<RaycastResult>(); // 여기에 히트 된 개체 저장
            gr.Raycast(ped, results);
            if (results.Count != 0)
            {
                GameObject obj = results[0].gameObject;
                if (obj.name.Contains("Chioce_")) // 히트 된 오브젝트의 태그와 맞으면 실행
                {
                    nowChiocesNum = int.Parse(obj.name.Substring(obj.name.Length - 1, 1));
                }
            }

            for (int i = 0; i < Chioces.Length; i++)
            {
                if (i == nowChiocesNum) Chioces[i].transform.Find("ChioceField").gameObject.SetActive(true);
                else Chioces[i].transform.Find("ChioceField").gameObject.SetActive(false);
            }
        }
    }
    public void CommnetSetting(string _npcID, int isChioce, NPC nPC, GameObject _nowObj = null)
    {
        isBossEnd = false;
        AudioSource _audio = GetComponent<AudioSource>();
        _audio.Stop();
        if (!dialogueObj.activeSelf) dialogueObj.SetActive(true);
        for (int i = 0; i < Chioces.Length; i++) Chioces[i].gameObject.SetActive(false);
        isChiocesSelected2 = false;
        nowTextNum = 0;
        nowNPC = _npcID;
        nowNPCScript = nPC;
        nowObj = _nowObj;
        isCommnet = true;
        isSkip = false;
        nDatas = System.Array.FindAll(SheetManager.Instance.NPCListsDB.dataArray, item => item.ID.ToUpper().Equals(_npcID.ToUpper()));
        NameText.text = (LocalizeManager.lang == LANGUAGE_SHIFTRICK.ENG ? nDatas[0].Nameeng :
            (LocalizeManager.lang == LANGUAGE_SHIFTRICK.CHN_S ? nDatas[0].Namechns :
            (LocalizeManager.lang == LANGUAGE_SHIFTRICK.CHN_T ? nDatas[0].Namechnt :
            (LocalizeManager.lang == LANGUAGE_SHIFTRICK.JA ? nDatas[0].Nameja :
            (LocalizeManager.lang == LANGUAGE_SHIFTRICK.RU ? nDatas[0].Nameru : nDatas[0].Name)))));
        if (commentCoroutine == null)
        {
            commentCoroutine = StartCoroutine(TextStart(isChioce));
            if (isChioce == 0) StartCoroutine(SoundCoroutine());
        }
    }
    IEnumerator SoundCoroutine()
    {
        AudioSource _audio = GetComponent<AudioSource>();
        while (!isSkip && dialogueObj.activeSelf)
        {
            _audio.Stop();
            _audio.volume =  (SoundManager.isMute || (PDG.Dungeon.instance == null ? false : PDG.Dungeon.instance.isPause) || (PDG.Player.instance == null ? false : PDG.Player.instance.isDie)) ? 0 : SoundManager.EffectVolume;
            _audio.Play();
            yield return new WaitForSeconds(0.1f);
        }
        _audio.Stop();
    }
    IEnumerator TextStart(int isChioce)
    {
        ContentText.transform.Find("Image_0").gameObject.SetActive(false);
        ContentText.transform.Find("Image_1").gameObject.SetActive(false);
        string nowString = (LocalizeManager.lang == LANGUAGE_SHIFTRICK.ENG ? nDatas[nowTextNum].Dialogueeng :
            (LocalizeManager.lang == LANGUAGE_SHIFTRICK.CHN_S ? nDatas[nowTextNum].Dialoguechns :
            (LocalizeManager.lang == LANGUAGE_SHIFTRICK.CHN_T ? nDatas[nowTextNum].Dialoguechnt :
            (LocalizeManager.lang == LANGUAGE_SHIFTRICK.JA ? nDatas[nowTextNum].Dialogueja :
            (LocalizeManager.lang == LANGUAGE_SHIFTRICK.RU ? nDatas[nowTextNum].Dialogueru : nDatas[nowTextNum].Dialogue)))));
        ContentText.text = string.Empty;
        if (isChioce == 1)
        {
            nowChiocesNum = 0;
            isChiocesSelected = true;
            for (int i = 0; i < nDatas.Length; i++)
            {
                Chioces[i].gameObject.SetActive(true);
                Chioces[i].text = (LocalizeManager.lang == LANGUAGE_SHIFTRICK.ENG ? nDatas[i].Dialogueeng :
                (LocalizeManager.lang == LANGUAGE_SHIFTRICK.CHN_S ? nDatas[i].Dialoguechns :
                (LocalizeManager.lang == LANGUAGE_SHIFTRICK.CHN_T ? nDatas[i].Dialoguechnt :
                (LocalizeManager.lang == LANGUAGE_SHIFTRICK.JA ? nDatas[0].Dialogueja :
                (LocalizeManager.lang == LANGUAGE_SHIFTRICK.RU ? nDatas[0].Dialogueru : nDatas[0].Dialogue)))));
            }
        }
        else if (isChioce == 2)
        {
            if (nDatas[nowTextNum].Result.Equals("RecoveryHealth"))
            {
                isSkip = false;
                isCommnet = false;
                dialogueObj.SetActive(false);
                PDG.Dungeon.instance.isSelect = false;
                nowNPCScript.animator.SetTrigger("health");
            }
            else
            {
                nowTextNum = nowChiocesNum;
                nowString = (LocalizeManager.lang == LANGUAGE_SHIFTRICK.ENG ? nDatas[nowTextNum].Dialogue2eng :
                    (LocalizeManager.lang == LANGUAGE_SHIFTRICK.CHN_S ? nDatas[nowTextNum].Dialogue2chns :
                    (LocalizeManager.lang == LANGUAGE_SHIFTRICK.CHN_T ? nDatas[nowTextNum].Dialogue2chnt :
                    (LocalizeManager.lang == LANGUAGE_SHIFTRICK.JA ? nDatas[nowTextNum].Dialogue2ja :
                    (LocalizeManager.lang == LANGUAGE_SHIFTRICK.RU ? nDatas[nowTextNum].Dialogue2ru : nDatas[nowTextNum].Dialogue2)))));
                isChiocesSelected = false;
                isChiocesSelected2 = true;
                for (int i = 0; i < nowString.Length; i++)
                {
                    if (isSkip) break;
                    ContentText.text += nowString[i].ToString();
                    if (isSkip) break;
                    yield return new WaitForSeconds(0.01f);
                }
                isSkip = true;
                ContentText.text = nowString;
            }
        }
        else if (isChioce == 0)
        {
            NameText.text = (LocalizeManager.lang == LANGUAGE_SHIFTRICK.ENG ? nDatas[0].Nameeng :
                (LocalizeManager.lang == LANGUAGE_SHIFTRICK.CHN_S ? nDatas[0].Namechns :
                (LocalizeManager.lang == LANGUAGE_SHIFTRICK.CHN_T ? nDatas[0].Namechnt :
                (LocalizeManager.lang == LANGUAGE_SHIFTRICK.JA ? nDatas[0].Nameja :
                (LocalizeManager.lang == LANGUAGE_SHIFTRICK.RU ? nDatas[0].Nameru : nDatas[0].Name)))));
            isChiocesSelected = false;
            for (int i = 0; i < nowString.Length; i++)
            {
                if (isSkip) break;
                ContentText.text += nowString[i].ToString();
                if (nowString[i].ToString().Equals("#")) ContentText.transform.Find("Image_0").gameObject.SetActive(true);
                if (nowString[i].ToString().Equals("$")) ContentText.transform.Find("Image_1").gameObject.SetActive(true);
                if (isSkip) break;
                yield return new WaitForSeconds(0.01f);
            }
            isSkip = true;
            ContentText.text = nowString;
        }
        else if (isChioce == 3)
        {
            NameText.text = (LocalizeManager.lang == LANGUAGE_SHIFTRICK.ENG ? nDatas[nowTextNum].Changenpceng :
                (LocalizeManager.lang == LANGUAGE_SHIFTRICK.CHN_S ? nDatas[nowTextNum].Changenpcchns :
                (LocalizeManager.lang == LANGUAGE_SHIFTRICK.CHN_T ? nDatas[nowTextNum].Changenpcchnt :
                (LocalizeManager.lang == LANGUAGE_SHIFTRICK.JA ? nDatas[nowTextNum].Changenpcja :
                (LocalizeManager.lang == LANGUAGE_SHIFTRICK.RU ? nDatas[nowTextNum].Changenpcru : nDatas[nowTextNum].Changenpc)))));
            nowString = (LocalizeManager.lang == LANGUAGE_SHIFTRICK.ENG ? nDatas[nowTextNum].Dialogue2eng :
                (LocalizeManager.lang == LANGUAGE_SHIFTRICK.CHN_S ? nDatas[nowTextNum].Dialogue2chns :
                (LocalizeManager.lang == LANGUAGE_SHIFTRICK.CHN_T ? nDatas[nowTextNum].Dialogue2chnt :
                (LocalizeManager.lang == LANGUAGE_SHIFTRICK.JA ? nDatas[nowTextNum].Dialogue2ja :
                (LocalizeManager.lang == LANGUAGE_SHIFTRICK.RU ? nDatas[nowTextNum].Dialogue2ru : nDatas[nowTextNum].Dialogue2)))));
            isChiocesSelected = false;
            for (int i = 0; i < nowString.Length; i++)
            {
                if (isSkip) break;
                ContentText.text += nowString[i].ToString();
                if (isSkip) break;
                yield return new WaitForSeconds(0.01f);
            }
            isSkip = true;
            ContentText.text = nowString;
            change = true;
        }
        commentCoroutine = null;
    }
    public void ClickEvent()
    {
        AudioSource _audio = GetComponent<AudioSource>();
        _audio.Stop();
        SoundManager.instance.StartAudio(new string[1] { "menu_click" }, VOLUME_TYPE.EFFECT);
        if (!isSkip)
        {
            if (nDatas[nowTextNum].Chioce && !isChiocesSelected2)
            {
                CommnetSetting(nowNPC, 2, nowNPCScript);
            }
            else
            {
                if (nDatas[nowTextNum].Chioce)
                    ContentText.text = (LocalizeManager.lang == LANGUAGE_SHIFTRICK.ENG ? nDatas[nowTextNum].Dialogue2eng :
                        (LocalizeManager.lang == LANGUAGE_SHIFTRICK.CHN_S ? nDatas[nowTextNum].Dialogue2chns :
                        (LocalizeManager.lang == LANGUAGE_SHIFTRICK.CHN_T ? nDatas[nowTextNum].Dialogue2chnt :
                        (LocalizeManager.lang == LANGUAGE_SHIFTRICK.JA ? nDatas[nowTextNum].Dialogue2ja :
                        (LocalizeManager.lang == LANGUAGE_SHIFTRICK.RU ? nDatas[nowTextNum].Dialogue2ru : nDatas[nowTextNum].Dialogue2)))));
                else
                    ContentText.text = (LocalizeManager.lang == LANGUAGE_SHIFTRICK.ENG ? nDatas[nowTextNum].Dialogueeng :
                        (LocalizeManager.lang == LANGUAGE_SHIFTRICK.CHN_S ? nDatas[nowTextNum].Dialoguechns :
                        (LocalizeManager.lang == LANGUAGE_SHIFTRICK.CHN_T ? nDatas[nowTextNum].Dialoguechnt :
                        (LocalizeManager.lang == LANGUAGE_SHIFTRICK.JA ? nDatas[nowTextNum].Dialogueja :
                        (LocalizeManager.lang == LANGUAGE_SHIFTRICK.RU ? nDatas[nowTextNum].Dialogueru : nDatas[nowTextNum].Dialogue)))));
                isSkip = true;
            }
        }
        else
        {
            isSkip = false;
            if (nDatas[nowTextNum].Result.Equals("End"))
            {
                isCommnet = false;
                dialogueObj.SetActive(false);
                PDG.Dungeon.instance.isSelect = false;
                if (isChiocesSelected2)
                {
                    Debug.Log("아이템 줘!");
                }
            }
            if (nDatas[nowTextNum].Result.Equals("TutorialEnd"))
            {
                isCommnet = false;
                dialogueObj.SetActive(false);
                PDG.Dungeon.instance.isSelect = false;
                PDG.Dungeon.instance.nowTutorialNum = 0;
                PDG.Dungeon.instance.GoAjit();
            }
            else if (nDatas[nowTextNum].Result.Contains("Chioce"))
            {
                CommnetSetting(nowNPC + "_" + nDatas[nowTextNum].Result, 1, nowNPCScript);
            }
            else if (nDatas[nowTextNum].Result.Contains("TutorialNext"))
            {
                isCommnet = false;
                dialogueObj.SetActive(false);
                PDG.Dungeon.instance.isSelect = false;
                PDG.Dungeon.instance.nowTutorialNum++;
                PDG.Dungeon.instance.GoTutorial();
            }
            else if (nDatas[nowTextNum].Result.Equals("tutorial_first_map_npc_health"))
            {
                isCommnet = false;
                dialogueObj.SetActive(false);
                PDG.Dungeon.instance.isSelect = false;
                GameObject.Find("Canvas").transform.Find("TutorialText").gameObject.SetActive(true);
                GameObject.Find("Canvas").transform.Find("TutorialText").GetComponent<Text>().text = LocalizeManager.GetLocalize("tutorial_healthtower");
            }
            else if (nDatas[nowTextNum].Result.Contains("TutorialReturn"))
            {
                isCommnet = false;
                dialogueObj.SetActive(false);
                PDG.Dungeon.instance.isSelect = false;
                PDG.Dungeon.instance.GoTutorial();
            }
            else if (nDatas[nowTextNum].Result.Contains("UnlockList"))
            {
                UnLock.Instance.SettingUnLock(nDatas[nowTextNum].ID, LocalizeManager.lang == LANGUAGE_SHIFTRICK.ENG ? nDatas[nowTextNum].Nameeng :
                    (LocalizeManager.lang == LANGUAGE_SHIFTRICK.CHN_S ? nDatas[nowTextNum].Namechns :
                    (LocalizeManager.lang == LANGUAGE_SHIFTRICK.CHN_T ? nDatas[nowTextNum].Namechnt :
                    (LocalizeManager.lang == LANGUAGE_SHIFTRICK.JA ? nDatas[nowTextNum].Nameja :
                    (LocalizeManager.lang == LANGUAGE_SHIFTRICK.RU ? nDatas[nowTextNum].Nameru : nDatas[nowTextNum].Name)))));
                isCommnet = false;
                dialogueObj.SetActive(false);
                PDG.Dungeon.instance.isSelect = false;
            }
            else if (nDatas[nowTextNum].Result.Contains("rescue"))
            {
                if (!nDatas[nowTextNum].Dialogue2.Equals(string.Empty) && !change)
                {
                    if (!tutorialCharacterChange) tutorialCharacterChange = true;
                    commentCoroutine = StartCoroutine(TextStart(3));
                    StartCoroutine(SoundCoroutine());
                }
                else
                {
                    SoundManager.instance.StartAudio(new string[1] { "Object/YouWin" }, VOLUME_TYPE.EFFECT);
                    isCommnet = false;
                    dialogueObj.SetActive(false);
                    PDG.Dungeon.instance.isSelect = false;
                    SaveAndLoadManager.instance.SaveNPC("reinforce");
                    Inventorys.Instance.ShowGetItemUI("get_reinforce", reinforceSprite);
                    nowNPCScript.gameObject.SetActive(false);
                }
            }
            else if (nDatas[nowTextNum].Result.Contains("get_choiseSkill"))
            {
                SoundManager.instance.StartAudio(new string[1] { "Object/YouWin" }, VOLUME_TYPE.EFFECT);
                isCommnet = false;
                dialogueObj.SetActive(false);
                PDG.Dungeon.instance.isSelect = false;
                SaveAndLoadManager.instance.SaveNPC("mr_dark");
                Inventorys.Instance.ShowGetItemUI("get_mrdark", mrDarkSprites);
                nowNPCScript.transform.parent.Find("Return").gameObject.SetActive(true);
                nowNPCScript.gameObject.SetActive(false);
            }
            else if (nDatas[nowTextNum].Result.Contains("get_hellDog"))
            {
                isCommnet = false;
                dialogueObj.SetActive(false);
                PDG.Dungeon.instance.isSelect = false;
                SaveAndLoadManager.instance.SaveNPC("hellDog");
                Inventorys.Instance.ShowGetItemUI("get_helldog", helldogSpriite);
                offObj.SetActive(false);
            }
            else if (nDatas[nowTextNum].Result.Equals("reinforce"))
            {
                isCommnet = false;
                dialogueObj.SetActive(false);
                PDG.Dungeon.instance.isSelect = false;
                ReinforceController.instance.ShowReinforce();
            }
            else if (nDatas[nowTextNum].Result.Equals("Tutorial_Comment"))
            {
                if (!nDatas[nowTextNum].Dialogue2.Equals(string.Empty) && !change)
                {
                    if (!tutorialCharacterChange) tutorialCharacterChange = true;
                    commentCoroutine = StartCoroutine(TextStart(3));
                    StartCoroutine(SoundCoroutine());
                }
                else
                {
                    change = false;
                    nowTextNum++;

                    if (nDatas.Length <= nowTextNum)
                    {
                        isCommnet = false;
                        dialogueObj.SetActive(false);
                    }
                    else
                    {
                        commentCoroutine = StartCoroutine(TextStart(0));
                        StartCoroutine(SoundCoroutine());
                    }
                }
            }
            else if (nDatas[nowTextNum].Result.Equals("Tutorial_Comment_2"))
            {
                nDatas[nowTextNum].Result = "Tutorial_Comment";
                if (!nDatas[nowTextNum].Dialogue2.Equals(string.Empty) && !change)
                {
                    if (!tutorialCharacterChange) tutorialCharacterChange = true;
                    commentCoroutine = StartCoroutine(TextStart(3));
                    StartCoroutine(SoundCoroutine());
                }
                else
                {
                    change = false;
                    nowTextNum++;
                    commentCoroutine = StartCoroutine(TextStart(0));
                    StartCoroutine(SoundCoroutine());
                }
                StartCoroutine(Spake());
            }
            else if (nDatas[nowTextNum].Result.Equals("Tutorial_first_end"))
            {
                isCommnet = false;
                dialogueObj.SetActive(false);
                PDG.Dungeon.instance.isSelect = false;

                StartCoroutine(StartQuestionEvent());

            }
            else if (nDatas[nowTextNum].Result.Equals("tutorial_first_map_npc_move"))
            {
                isCommnet = false;
                dialogueObj.SetActive(false);
                PDG.Dungeon.instance.isSelect = false;

                TutorialEvent_stage_01 obj = FindObjectOfType(typeof(TutorialEvent_stage_01)) as TutorialEvent_stage_01;
                if (obj != null)
                {
                    obj.isMove = true;
                    ItemsData iData = System.Array.Find(SheetManager.Instance.ItemsDB.dataArray, item => item.ID.Equals("rolling"));
                    Inventorys.Instance.AddItem(Inventorys.Instance.CopyItem(iData), 1, true, true);
                    PDG.Player.instance.SettingItem(Inventorys.Instance.CopyItem(iData));
                }
                GameObject.Find("Canvas").transform.Find("TutorialText").gameObject.SetActive(true);
                GameObject.Find("Canvas").transform.Find("TutorialText").GetComponent<Text>().text = LocalizeManager.GetLocalize("tutorial_avoid");
            }
            else if (nDatas[nowTextNum].Result.Equals("tutorial_first_map_clear"))
            {
                isCommnet = false;
                dialogueObj.SetActive(false);
                PDG.Dungeon.instance.isSelect = false;

                TutorialEvent_stage_01 obj = FindObjectOfType(typeof(TutorialEvent_stage_01)) as TutorialEvent_stage_01;
                obj.exit.isLock = false;
                obj.exit.AdjacentUnLock();
                //PDG.Dungeon.isStageClear = true;
                GameObject.Find("Canvas").transform.Find("TutorialText").gameObject.SetActive(true);
                GameObject.Find("Canvas").transform.Find("TutorialText").GetComponent<Text>().text = LocalizeManager.GetLocalize("tutorial_move");
            }
            else if (nDatas[nowTextNum].Result.Equals("tutorial_second_end"))
            {
                isCommnet = false;
                dialogueObj.SetActive(false);
                PDG.Dungeon.instance.isSelect = false;
                TutorialEvent_stage_01 obj = FindObjectOfType(typeof(TutorialEvent_stage_01)) as TutorialEvent_stage_01;
                ItemsData iData = System.Array.Find(SheetManager.Instance.ItemsDB.dataArray, item => item.ID.Equals("tentacle_arm"));
                Inventorys.Instance.AddItem(Inventorys.Instance.CopyItem(iData), 1, true, true);
                PDG.Player.instance.SettingItem(Inventorys.Instance.CopyItem(iData));

                GameObject.Find("Canvas").transform.Find("TutorialText").gameObject.SetActive(true);
                GameObject.Find("Canvas").transform.Find("TutorialText").GetComponent<Text>().text = LocalizeManager.GetLocalize("tutorial_leftclick");
            }
            else if (nDatas[nowTextNum].Result.Equals("tutorial_second_end_2"))
            {
                isCommnet = false;
                dialogueObj.SetActive(false);
                PDG.Dungeon.instance.isSelect = false;
                TutorialEvent_stage_01 obj = FindObjectOfType(typeof(TutorialEvent_stage_01)) as TutorialEvent_stage_01;
                ItemsData iData = System.Array.Find(SheetManager.Instance.ItemsDB.dataArray, item => item.ID.Equals("catching_tentacle"));
                Inventorys.Instance.AddItem(Inventorys.Instance.CopyItem(iData), 1, true, true);
                PDG.Player.instance.SettingItem(Inventorys.Instance.CopyItem(iData));

                GameObject.Find("Canvas").transform.Find("TutorialText").gameObject.SetActive(true);
                GameObject.Find("Canvas").transform.Find("TutorialText").GetComponent<Text>().text = LocalizeManager.GetLocalize("tutorial_rightclick");
            }
            else if (nDatas[nowTextNum].Result.Equals("tutorial_second_clear"))
            {
                isCommnet = false;
                dialogueObj.SetActive(false);
                PDG.Dungeon.instance.isSelect = false;

                TutorialEvent_stage_01 obj = FindObjectOfType(typeof(TutorialEvent_stage_01)) as TutorialEvent_stage_01;
                obj.exit.isLock = false;
                obj.exit.AdjacentUnLock();
                //PDG.Dungeon.isStageClear = true;
                GameObject.Find("Canvas").transform.Find("TutorialText").gameObject.SetActive(true);
                GameObject.Find("Canvas").transform.Find("TutorialText").GetComponent<Text>().text = LocalizeManager.GetLocalize("tutorial_move");
            }
            else if (nDatas[nowTextNum].Result.Equals("tutorial_third_end"))
            {
                isCommnet = false;
                dialogueObj.SetActive(false);
                PDG.Dungeon.instance.isSelect = false;

                ItemsData iData = System.Array.Find(SheetManager.Instance.ItemsDB.dataArray, item => item.ID.Equals("spiny_cell_firing"));
                Inventorys.Instance.AddItem(Inventorys.Instance.CopyItem(iData), 1, true, true);
                PDG.Player.instance.SettingItem(Inventorys.Instance.CopyItem(iData));

                TutorialEvent_stage_01 obj = FindObjectOfType(typeof(TutorialEvent_stage_01)) as TutorialEvent_stage_01;
                ResultPanelScript.instance.Q_SkillCount = 1;

                GameObject.Find("Canvas").transform.Find("TutorialText").gameObject.SetActive(true);
                GameObject.Find("Canvas").transform.Find("TutorialText").GetComponent<Text>().text = LocalizeManager.GetLocalize("tutorial_qskill");
            }
            else if (nDatas[nowTextNum].Result.Equals("tutorial_four_end"))
            {
                isCommnet = false;
                dialogueObj.SetActive(false);
                PDG.Dungeon.instance.isSelect = false;
                GameObject.Find("Canvas").transform.Find("TutorialText").gameObject.SetActive(true);
                GameObject.Find("Canvas").transform.Find("TutorialText").GetComponent<Text>().text = LocalizeManager.GetLocalize("tutorial_object");
            }
            else if (nDatas[nowTextNum].Result.Equals("tutorial_third_clear"))
            {
                isCommnet = false;
                dialogueObj.SetActive(false);
                PDG.Dungeon.instance.isSelect = false;

                TutorialEvent_stage_01 obj = FindObjectOfType(typeof(TutorialEvent_stage_01)) as TutorialEvent_stage_01;
                obj.exit.isLock = false;
                obj.exit.AdjacentUnLock();
                //PDG.Dungeon.isStageClear = true;
                GameObject.Find("Canvas").transform.Find("TutorialText").gameObject.SetActive(true);
                GameObject.Find("Canvas").transform.Find("TutorialText").GetComponent<Text>().text = LocalizeManager.GetLocalize("tutorial_move");
            }
            else if (nDatas[nowTextNum].Result.Equals("tutorial_four_clear"))
            {
                isCommnet = false;
                dialogueObj.SetActive(false);
                PDG.Dungeon.instance.isSelect = false;

                TutorialEvent_stage_01 obj = FindObjectOfType(typeof(TutorialEvent_stage_01)) as TutorialEvent_stage_01;
                obj.exit.isLock = false;
                obj.exit.AdjacentUnLock();
                //PDG.Dungeon.isStageClear = true;
                GameObject.Find("Canvas").transform.Find("TutorialText").gameObject.SetActive(true);
                GameObject.Find("Canvas").transform.Find("TutorialText").GetComponent<Text>().text = LocalizeManager.GetLocalize("tutorial_move");
            }
            else if (nDatas[nowTextNum].Result.Equals("choiseSkill"))
            {
                isCommnet = false;
                dialogueObj.SetActive(false);
                PDG.Dungeon.instance.isSelect = false;
                StateUpgradeController.instance.ShowUpgrade();
            }
            else if (nDatas[nowTextNum].Result.Equals("hellDogCity_0"))
            {
                ItemSetting("S", "A");
            }
            else if (nDatas[nowTextNum].Result.Equals("hellDogCity_1"))
            {
                ItemSetting("B", "C");
            }
            else if (nDatas[nowTextNum].Result.Equals("hellDogCity_2"))
            {
                ItemSetting("D", "E");
            }
            else if (nDatas[nowTextNum].Result.Equals("stg_intro_comment_1"))
            {
                isCommnet = false;
                dialogueObj.SetActive(false);
                PDG.Dungeon.instance.isSelect = false;
                if (bwc != null) bwc.bossWaitingAnim.SetTrigger("Trigger_0");
                CommnetSetting("stg_intro_comment_1", 0, null);
            }
            else if (nDatas[nowTextNum].Result.Equals("stg_intro_comment_2"))
            {
                isCommnet = false;
                dialogueObj.SetActive(false);
                PDG.Dungeon.instance.isSelect = false;
                CommnetSetting("stg_intro_comment_2", 0, null);
            }
            else if (nDatas[nowTextNum].Result.Equals("stg_intro_comment_3"))
            {
                isCommnet = false;
                dialogueObj.SetActive(false);
                PDG.Dungeon.instance.isSelect = false;
                if (bwc != null)
                {
                    bwc._pCamera.target = bwc.transform;
                    bwc.bossWaitingAnim.SetTrigger("Trigger_1");
                }
                CommnetSetting("stg_intro_comment_3", 0, null);
                CameraShaker._instance.StartShake(0.2f, 0.02f, 0.2f);
                SoundManager.instance.StartAudio(new string[1] { "Boss/stg_standing_punch" }, VOLUME_TYPE.EFFECT);
                SoundManager.instance.StartAudio(new string[1] { "Boss/stg_standing_punch" }, VOLUME_TYPE.EFFECT);
                SoundManager.instance.StartAudio(new string[1] { "Boss/stg_standing_punch" }, VOLUME_TYPE.EFFECT);
            }
            else if (nDatas[nowTextNum].Result.Equals("stg_intro_comment_4"))
            {
                isCommnet = false;
                dialogueObj.SetActive(false);
                PDG.Dungeon.instance.isSelect = false;
                if (bwc != null)
                {
                    bwc.isFirstCommnetEnd = true;
                }
            }
            else if (nDatas[nowTextNum].Result.Equals("stg_intro_comment_5"))
            {
                isCommnet = false;
                dialogueObj.SetActive(false);
                PDG.Dungeon.instance.isSelect = false;
                if (bwc != null)
                {
                    bwc.IntroStart();
                }
            }
            else if (nDatas[nowTextNum].Result.Equals("stg_intro_comment_6"))
            {
                isCommnet = false;
                dialogueObj.SetActive(false);
                PDG.Dungeon.instance.isSelect = false;
                if (bwc != null)
                {
                    bwc.EndEvent();
                }
            }
            else if (nDatas[nowTextNum].Result.Equals("stage2_boss_intro_end"))
            {
                isCommnet = false;
                dialogueObj.SetActive(false);
                PDG.Dungeon.instance.isSelect = false;
                StartCoroutine(Spake(true));
            }
            else if (nDatas[nowTextNum].Result.Equals("stage2_boss_intro_end2"))
            {
                if (!nDatas[nowTextNum].Dialogue2.Equals(string.Empty) && !change)
                {
                    if (!tutorialCharacterChange) tutorialCharacterChange = true;
                    commentCoroutine = StartCoroutine(TextStart(3));
                    StartCoroutine(SoundCoroutine());
                }
                else
                {
                    change = false;
                    nowTextNum++;

                    if (nDatas.Length <= nowTextNum)
                    {
                        isCommnet = false;
                        dialogueObj.SetActive(false);
                        PDG.Dungeon.instance.isSelect = false;
                    }
                    else
                    {
                        commentCoroutine = StartCoroutine(TextStart(0));
                        StartCoroutine(SoundCoroutine());
                    }
                }
            }
            else if (nDatas[nowTextNum].Result.Equals("stage2_boss_intro_end3"))
            {
                if (!nDatas[nowTextNum].Dialogue2.Equals(string.Empty) && !change)
                {
                    if (!tutorialCharacterChange) tutorialCharacterChange = true;
                    commentCoroutine = StartCoroutine(TextStart(3));
                    StartCoroutine(SoundCoroutine());
                }
                else
                {
                    change = false;
                    nowTextNum++;

                    if (nDatas.Length <= nowTextNum)
                    {
                        isCommnet = false;
                        dialogueObj.SetActive(false);
                        PDG.Dungeon.instance.isSelect = false;
                        isBossEnd = true;
                    }
                    else
                    {
                        commentCoroutine = StartCoroutine(TextStart(0));
                        StartCoroutine(SoundCoroutine());
                    }
                }
            }
            else if (nDatas[nowTextNum].Result.Equals("Making"))
            {
                isCommnet = false;
                dialogueObj.SetActive(false);
                PDG.Dungeon.instance.isSelect = false;
                MakeController.instance.ShowMake(string.Empty, "hellfogee", nowObj);
            }
            else if (nDatas[nowTextNum].Result.Equals("Decomposition"))
            {
                isCommnet = false;
                dialogueObj.SetActive(false);
                PDG.Dungeon.instance.isSelect = false;
            }
        }
    }
    IEnumerator Spake(bool isBoss = false)
    {
        float a = 0.0f;
        while (true)
        {
            a += Time.deltaTime * 5;
            spakeImage.color = new Color(1, 1, 1, a);
            if (a > 1.0f) break;
            yield return new WaitForSeconds(Time.deltaTime);
        }
        spakeImage.color = new Color(1, 1, 1, 1);
        a = 1.0f;
        if (isBoss)
        {
            Vector3 endPos = PDG.Dungeon.instance.waitingExit;
            endPos.x += 1;
            PDG.Player.instance.transform.position = endPos;
            PDG.Dungeon.instance.RoomInsert(0, false, true);
            BossRoomExitDelete(endPos);
            PDG.Dungeon.instance.spawnBat.SetActive(false);
            PDG.Dungeon.instance.nowRoomType = "boss";
            yield return new WaitForSeconds(0.5f);
            if (PDG.Player.instance.isSubWeapon)
            {
                PDG.Player.instance.isSubWeapon = false;
            }
            PDG.Player.instance.buffTowerBuffTime_0 = 0.0f;
            PDG.Player.instance.buffTowerBuffTime_1 = 0.0f;
            PDG.Player.instance.buffTowerBuffTime_2 = 0.0f;
            PDG.Player.instance.buffTowerBuffTime_3 = 0.0f;
        }
        while (true)
        {
            a -= Time.deltaTime * 5;
            spakeImage.color = new Color(1, 1, 1, a);
            if (a <= 0.0f) break;
            yield return new WaitForSeconds(Time.deltaTime);
        }
        spakeImage.color = new Color(1, 1, 1, 0);
    }
    private void ItemSetting(string gradeA, string gradeB)
    {
        string[] itemName;
        while (true)
        {
            List<ItemsData> iDatas = System.Array.FindAll(SheetManager.Instance.ItemsDB.dataArray, item => item.Grade.ToUpper().Equals(gradeA.ToUpper()) || item.Grade.ToUpper().Equals(gradeB.ToUpper())).ToList();
            for (int i = 0; i < iDatas.Count; i++)
            {
                Sprite s = Resources.Load<Sprite>("Item/" + iDatas[i].Type1 + "/" + iDatas[i].Type2 + "/" + iDatas[i].ID);
                if (s == null || !Inventorys.Instance.UnlockAndMaxCountCheck(iDatas[i].ID) || iDatas[i].Type1.Equals("Potion"))
                {
                    iDatas.RemoveAt(i);
                    i--;
                    continue;
                }
            }
            if (iDatas.Count > 0)
            {
                ItemsData iData = iDatas[UnityEngine.Random.Range(0, iDatas.Count)];
                itemName = new string[1];
                itemName[0] = iData.ID;
                Inventorys.Instance.shopOpenItems.Add(new ItemInfo() { Data = Inventorys.Instance.CopyItem(iData) });
                break;
            }
        }
        Vector3 tPos = nowNPCScript.transform.position;
        tPos.y += 1.0f;
        for (int i = 0; i < 1; i++)
        {
            float angle = -90.0f;
            Vector3 pos = GetPosition(tPos, angle);
            Vector3 dir = pos - tPos;
            dir.Normalize();
            GameObject obj = ObjManager.Call().GetObject("Item");
            if (obj != null)
            {
                Item _item = obj.GetComponent<Item>();
                _item._Item = new ItemInfo
                {
                    Data = System.Array.Find(SheetManager.Instance.ItemsDB.dataArray, item => item.ID.Equals(itemName[i]))
                };
                _item.dir = dir;
                _item.itemCount = 1;
                _item.rPow = 1;
                float startDis2 = 2;
                float angle2 = angle += 90.0f;
                if (angle2 <= 180) startDis2 -= (angle2 / 150.0f);
                else startDis2 -= ((180.0f / 150.0f) + ((180.0f - angle2) / 150.0f));

                obj.transform.position = pos + (dir * startDis2);
                obj.SetActive(true);
            }
        }

        isCommnet = false;
        dialogueObj.SetActive(false);
        PDG.Dungeon.instance.isSelect = false;
    }
    IEnumerator StartQuestionEvent()
    {
        GameObject entranceQuestionPanel = GameObject.Find("Canvas").transform.Find("QuestionPanel").gameObject;

        Text entranceQuestionText = entranceQuestionPanel.transform.Find("Text").GetComponent<Text>();
        Button YesBtn = entranceQuestionPanel.transform.Find("BtnPanel").Find("YesBtn").GetComponent<Button>();
        Button NoBtn = entranceQuestionPanel.transform.Find("BtnPanel").Find("NoBtn").GetComponent<Button>();
        entranceQuestionPanel.SetActive(true);
        entranceQuestionText.text = LocalizeManager.GetLocalize("tutorial_continue");
        entranceQuestionPanel.SetActive(true);
        NoBtn.onClick.RemoveAllListeners();
        NoBtn.onClick.AddListener(() => {
            SaveAndLoadManager.instance.SaveVolume(new VolumeSave { _type = 6, _value = 1 });
            PDG.Dungeon.instance.GoAjit();
            entranceQuestionPanel.SetActive(false);
        });
        YesBtn.onClick.RemoveAllListeners();
        YesBtn.onClick.AddListener(() => {
            PDG.Dungeon.instance.GoTutorial();
            entranceQuestionPanel.SetActive(false);
        });

        RectTransform rt = entranceQuestionPanel.GetComponent<RectTransform>();
        float y = 500.0f;
        while (true)
        {
            rt.anchoredPosition3D = new Vector3(0, y, 0);
            y -= Time.deltaTime * 1500;
            if (y <= 50.0f) break;
            yield return new WaitForSeconds(Time.deltaTime);
        }
        y = 50.0f;
        rt.anchoredPosition3D = new Vector3(0, y, 0);
        while (true)
        {
            rt.anchoredPosition3D = new Vector3(0, y, 0);
            y += Time.deltaTime * (1500 / 4);
            if (y >= 70.0f) break;
            yield return new WaitForSeconds(Time.deltaTime);
        }
        y = 70.0f;
        rt.anchoredPosition3D = new Vector3(0, y, 0);
        while (true)
        {
            rt.anchoredPosition3D = new Vector3(0, y, 0);
            y -= Time.deltaTime * (1500 / 4);
            if (y <= 50.0f) break;
            yield return new WaitForSeconds(Time.deltaTime);
        }
        y = 50.0f;
        rt.anchoredPosition3D = new Vector3(0, y, 0);
        while (true)
        {
            rt.anchoredPosition3D = new Vector3(0, y, 0);
            y += Time.deltaTime * (1500 / 4);
            if (y >= 60.0f) break;
            yield return new WaitForSeconds(Time.deltaTime);
        }
        y = 60.0f;
        rt.anchoredPosition3D = new Vector3(0, y, 0);
        while (true)
        {
            rt.anchoredPosition3D = new Vector3(0, y, 0);
            y -= Time.deltaTime * (1500 / 4);
            if (y <= 50.0f) break;
            yield return new WaitForSeconds(Time.deltaTime);
        }
        y = 50.0f;


        while (entranceQuestionPanel.activeSelf)
        {
            if ((PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ACTIVE], true) || PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_CONFIRM], true)) && PDG.Dungeon.instance.closeCoolTime > 0.2f)
            {
                PDG.Dungeon.instance.closeCoolTime = 0.0f;
                PDG.Dungeon.instance.GoTutorial();
                entranceQuestionPanel.SetActive(false);
                break;
            }
            else if ((Input.GetKeyDown(KeyCode.Escape) || PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_CANCLE], true)) && PDG.Dungeon.instance.closeCoolTime > 0.2f) 
            {
                PDG.Dungeon.instance.closeCoolTime = 0.0f;
                SaveAndLoadManager.instance.SaveVolume(new VolumeSave { _type = 6, _value = 1 });
                PDG.Dungeon.instance.GoAjit();
                entranceQuestionPanel.SetActive(false);
                break;
            }
            yield return new WaitForSeconds(Time.deltaTime);
        }

    }
    private void BossRoomExitDelete(Vector2 pos)
    {
        int mask = 1 << 19;
        RaycastHit2D hit = Physics2D.BoxCast(PDG.Dungeon.instance.waitingExit, new Vector2(0.5f, 0.5f), 0, Vector2.zero, 0, mask);
        if (hit)
        {
            if (hit.collider.GetComponent<Exit>())
            {
                hit.collider.GetComponent<Exit>().isWarp = false;
                hit.collider.GetComponent<Exit>().isLock = false;
                hit.collider.GetComponent<Exit>().isNotMonster = true;
                hit.collider.GetComponent<Exit>().AdjacentLock(true);
            }
        }
    }
    #region -- Tool --
    public static float GetAngle(Vector3 vStart, Vector3 vEnd)
    {
        Vector3 v = vEnd - vStart;

        return Mathf.Atan2(v.y, v.x) * Mathf.Rad2Deg;
    }
    public static Vector3 GetPosition(Vector3 vStart, float angle)
    {
        float x = Mathf.Cos(Mathf.Deg2Rad * angle) + vStart.x;
        float y = Mathf.Sin(Mathf.Deg2Rad * angle) + vStart.y;

        return new Vector3(x, y, 0);
    }
    #endregion
}
