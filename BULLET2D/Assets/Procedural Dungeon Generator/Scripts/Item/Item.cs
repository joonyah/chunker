﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class Item : MonoBehaviour
{
    public ItemInfo _Item;
    public Vector2 dir;
    public float rPow = 2.0f;

    private Rigidbody2D rigid;
    private SpriteRenderer sr;
    private Animator animator;
    public Sprite outlineSprite;
    public Sprite defaultSprite;

    public int itemCount = 0;
    public bool isShop = false;
    private float ActiveTime = 0.5f;
    private float ActiveTimeNow = 0.0f;

    private float autoAddTime = 5.0f;
    private float autoAddTimeNow = 0.0f;
    public GameObject NPCObj;
    GameObject pKey;
    [SerializeField] private GameObject fireEffect;

    [Header("Capsule전용")]
    [SerializeField] private int nCapsuleNum = -1;
    [SerializeField] private SpriteOutline sOutline;

    [Header("Recycle")]
    [SerializeField] private bool isRecycleBefore = false;
    [SerializeField] private GameObject recycleEffect;
    public bool isBossRush = false;
    public bool isChoiceItem = false;
    private void Awake()
    {
        sr = GetComponent<SpriteRenderer>();
        animator = GetComponent<Animator>();
        rigid = GetComponent<Rigidbody2D>();
        pKey = GameObject.Find("Canvas").transform.Find("PressKey").gameObject;
    }
    private void Start()
    {
    }
    private void OnEnable()
    {
        InitSetting();
    }
    public void InitSetting()
    {
        isBossRush = false;
        sr.sprite = Resources.Load<Sprite>("Item/" + _Item.Data.Type1 + "/" + _Item.Data.Type2 + "/" + _Item.Data.ID);
        outlineSprite = Resources.Load<Sprite>("Item/" + _Item.Data.Type1 + "/" + _Item.Data.Type2 + "/" + _Item.Data.ID + "_outline");
        defaultSprite = sr.sprite;
        RuntimeAnimatorController run = Resources.Load<RuntimeAnimatorController>("Item/" + _Item.Data.Type1 + "/" + _Item.Data.Type2 + "/" + _Item.Data.ID + "/" + _Item.Data.ID);
        if (run == null)
        {
            animator.enabled = false;
        }
        else if(!_Item.Data.ID.Contains("old"))
        {
            animator.runtimeAnimatorController = run;
            animator.enabled = true;
        }
        else
        {
            animator.enabled = false;
        }

        rigid.AddForce(dir * rPow, ForceMode2D.Impulse);
        rigid.simulated = true;
        isShop = false;
        autoAddTimeNow = 0.0f;

        if (_Item.Data.ID.Contains("pixelfire_small"))
        {
            ParticleSystem p1 = fireEffect.transform.Find("Fire").GetComponent<ParticleSystem>();
            ParticleSystem p2 = fireEffect.transform.Find("Fire").Find("Spark01").GetComponent<ParticleSystem>();
            ParticleSystem p3 = fireEffect.transform.Find("Fire").Find("Spark02").GetComponent<ParticleSystem>();

            ParticleSystem.MainModule pm1 = p1.main;
            ParticleSystemGradientMode mode1 = pm1.startColor.mode;
            mode1 = ParticleSystemGradientMode.TwoColors;
            Color32 color1_1 = pm1.startColor.colorMin;
            Color32 color1_2 = pm1.startColor.colorMax;
            color1_1 = new Color32(236, 133, 66, 255);
            color1_2 = new Color32(255, 79, 0, 255);
            ParticleSystem.MinMaxGradient pc1 = new ParticleSystem.MinMaxGradient(color1_1, color1_2);
            pm1.startColor = pc1;

            ParticleSystem.MainModule pm2 = p2.main;
            ParticleSystemGradientMode mode2 = pm2.startColor.mode;
            mode2 = ParticleSystemGradientMode.TwoColors;
            Color32 color2_1 = pm2.startColor.colorMin;
            Color32 color2_2 = pm2.startColor.colorMax;
            color2_1 = new Color32(236, 133, 66, 255);
            color2_2 = new Color32(255, 79, 0, 255);
            ParticleSystem.MinMaxGradient pc2 = new ParticleSystem.MinMaxGradient(color2_1, color2_2);
            pm2.startColor = pc2;

            ParticleSystem.MainModule pm3 = p3.main;
            ParticleSystemGradientMode mode3 = pm3.startColor.mode;
            mode3 = ParticleSystemGradientMode.TwoColors;
            Color32 color3_1 = pm3.startColor.colorMin;
            Color32 color3_2 = pm3.startColor.colorMax;
            color3_1 = new Color32(236, 133, 66, 255);
            color3_2 = new Color32(255, 79, 0, 255);
            ParticleSystem.MinMaxGradient pc3 = new ParticleSystem.MinMaxGradient(color3_1, color3_2);
            pm3.startColor = pc3;


            fireEffect.SetActive(true);
        }
        else if (_Item.Data.ID.Contains("cold_cell_wave"))
        {
            ParticleSystem p1 = fireEffect.transform.Find("Fire").GetComponent<ParticleSystem>();
            ParticleSystem p2 = fireEffect.transform.Find("Fire").Find("Spark01").GetComponent<ParticleSystem>();
            ParticleSystem p3 = fireEffect.transform.Find("Fire").Find("Spark02").GetComponent<ParticleSystem>();

            ParticleSystem.MainModule pm1 = p1.main;
            ParticleSystemGradientMode mode1 = pm1.startColor.mode;
            mode1 = ParticleSystemGradientMode.TwoColors;
            Color32 color1_1 = pm1.startColor.colorMin;
            Color32 color1_2 = pm1.startColor.colorMax;
            color1_1 = new Color32(66, 166, 236, 255);
            color1_2 = new Color32(0, 31, 255, 255);
            ParticleSystem.MinMaxGradient pc1 = new ParticleSystem.MinMaxGradient(color1_1, color1_2);
            pm1.startColor = pc1;

            ParticleSystem.MainModule pm2 = p2.main;
            ParticleSystemGradientMode mode2 = pm2.startColor.mode;
            mode2 = ParticleSystemGradientMode.TwoColors;
            Color32 color2_1 = pm2.startColor.colorMin;
            Color32 color2_2 = pm2.startColor.colorMax;
            color2_1 = new Color32(66, 166, 236, 255);
            color2_2 = new Color32(0, 31, 255, 255);
            ParticleSystem.MinMaxGradient pc2 = new ParticleSystem.MinMaxGradient(color2_1, color2_2);
            pm2.startColor = pc2;

            ParticleSystem.MainModule pm3 = p3.main;
            ParticleSystemGradientMode mode3 = pm3.startColor.mode;
            mode3 = ParticleSystemGradientMode.TwoColors;
            Color32 color3_1 = pm3.startColor.colorMin;
            Color32 color3_2 = pm3.startColor.colorMax;
            color3_1 = new Color32(66, 166, 236, 255);
            color3_2 = new Color32(0, 31, 255, 255);
            ParticleSystem.MinMaxGradient pc3 = new ParticleSystem.MinMaxGradient(color3_1, color3_2);
            pm3.startColor = pc3;

            fireEffect.SetActive(true);
        }
        else fireEffect.SetActive(false);
        transform.Find("MiniMap").GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 1);
        transform.Find("MiniMap").GetComponent<SpriteRenderer>().sprite = defaultSprite;

        if (_Item.Data.Type1.Equals("Material"))
        {
            Color _color = Color.white;
            if (_Item.Data.Type2.Equals("normal")) _color = Color.blue;
            else if (_Item.Data.Type2.Equals("unique")) _color = new Color32(173, 0, 255, 255);
            else if (_Item.Data.Type2.Equals("subboss")) _color = Color.yellow;
            else if (_Item.Data.Type2.Equals("boss")) _color = Color.green;
            else if (_Item.Data.Type2.Equals("Boss")) _color = Color.yellow;
            sOutline.color = _color;
            sOutline.Regenerate();
        }
        else
        {
            sOutline.Clear();
        }

    }
    private void Update()
    {
        if (isChoiceItem && PDG.Dungeon.isChoiceItem)
        {
            GameObject obj = ObjManager.Call().GetObject("GetItem");
            if (obj != null)
            {
                obj.transform.position = transform.position;
                obj.SetActive(true);
            }
            isChoiceItem = false;
            gameObject.SetActive(false);
        }

        autoAddTimeNow += Time.deltaTime;
        ActiveTimeNow += Time.deltaTime;
        if(isShop && transform.Find("MiniMap").gameObject.activeSelf)
        {
            transform.Find("MiniMap").gameObject.SetActive(false);
        }
        if (autoAddTimeNow > autoAddTime && !isShop && outlineSprite == null)
        {
            if (Inventorys.Instance.AddItem(_Item, itemCount, true))
            {
                GameObject obj = ObjManager.Call().GetObject("GetItem");
                if (obj != null)
                {
                    obj.transform.position = transform.position;
                    obj.SetActive(true);
                }

                GameObject obj2 = ObjManager.Call().GetObject("ItemLoop");
                if (obj2)
                {
                    obj2.transform.position = transform.position;
                    obj2.SetActive(true);
                    obj2.GetComponent<ItemLoopController>().GoItem(sr.sprite, (_Item.Data.Type1.Equals("Material") ? true : false));
                }
                gameObject.SetActive(false);
            }
        }

        int mask = 1 << 9;
        RaycastHit2D hit = Physics2D.CircleCast(transform.position, 0.8f, Vector2.zero, 1, mask);
        if (hit && (PDG.Dungeon.ItemSelected ? (PDG.Dungeon.SelecingItem == null ? true : (PDG.Dungeon.SelecingItem == gameObject ? true : false)) : true) && hit.collider.GetComponent<PDG.Player>() && !PDG.Dungeon.instance.isPause)
        {
            if(!_Item.Data.Type1.Equals("Material"))
                ShopBuyInfo.instance.ShowInfo(transform, _Item.Data.ID);
            if (isShop)
            {
                PDG.Dungeon.ItemSelected = true;
                PDG.Dungeon.SelecingItem = gameObject;
                pKey.SetActive(true);
                if (pKey.GetComponent<SpriteRenderer>())
                    pKey.GetComponent<SpriteRenderer>().sprite = PDG.Dungeon.instance.isJoystrick ? PDG.Dungeon.GetJoyStrickKeySprite(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_ACTIVE]) : PDG.Dungeon.GetJoyStrickKeySprite(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ACTIVE]);
                else if (pKey.GetComponent<Image>())
                    pKey.GetComponent<Image>().sprite = PDG.Dungeon.instance.isJoystrick ? PDG.Dungeon.GetJoyStrickKeySprite(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_ACTIVE]) : PDG.Dungeon.GetJoyStrickKeySprite(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ACTIVE]);
                Vector3 pos = transform.position;
                pos.y += 0.8f;
                pKey.transform.position = Camera.main.WorldToScreenPoint(pos);

                if (transform.Find("MiniMap").gameObject.activeSelf) transform.Find("MiniMap").gameObject.SetActive(false);
                if (outlineSprite != null) sr.sprite = outlineSprite;
                if ((PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ACTIVE], true) || PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_ACTIVE], true)) && PDG.Dungeon.instance.closeCoolTime > 0.5f)
                {
                    PDG.Dungeon.instance.closeCoolTime = 0.0f;
                    SoundManager.instance.StartAudio(new string[1] { "UI/ShopBuy" }, VOLUME_TYPE.EFFECT);
                    if (Inventorys.Instance.AddItem(_Item, itemCount, true))
                    {
                        GameObject.Find("Canvas").transform.Find("TutorialText4").gameObject.SetActive(false);
                        GameObject obj = ObjManager.Call().GetObject("GetItem");
                        if (obj != null)
                        {
                            obj.transform.position = transform.position;
                            obj.SetActive(true);
                        }
                        PDG.Player.instance.SettingItem(_Item.Data);
                        gameObject.SetActive(false);
                        NewShopController.instance.ReSetItem(_Item);
                        PDG.Dungeon.ItemSelected = false;
                        PDG.Dungeon.SelecingItem = null;
                    }
                }
            }
            else
            {
                if (outlineSprite != null)
                {
                    PDG.Dungeon.ItemSelected = true;
                    PDG.Dungeon.SelecingItem = gameObject;
                    pKey.SetActive(true);
                    if (pKey.GetComponent<SpriteRenderer>())
                        pKey.GetComponent<SpriteRenderer>().sprite = PDG.Dungeon.instance.isJoystrick ? PDG.Dungeon.GetJoyStrickKeySprite(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_ACTIVE]) : PDG.Dungeon.GetJoyStrickKeySprite(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ACTIVE]);
                    else if (pKey.GetComponent<Image>())
                        pKey.GetComponent<Image>().sprite = PDG.Dungeon.instance.isJoystrick ? PDG.Dungeon.GetJoyStrickKeySprite(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_ACTIVE]) : PDG.Dungeon.GetJoyStrickKeySprite(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ACTIVE]);
                    Vector3 pos = transform.position;
                    pos.y += 2.0f;
                    pKey.transform.position = Camera.main.WorldToScreenPoint(pos);
                    sr.sprite = outlineSprite;
                    pos.y -= 0.5f;
                    GameObject.Find("Canvas").transform.Find("TutorialText4").gameObject.SetActive(true);
                    if (!isRecycleBefore)
                    {
                        string tempID = string.Empty;
                        if (_Item.Data.Grade.Equals("S") || _Item.Data.Grade.Equals("G")) tempID += "<color=#ff3700>";
                        else if (_Item.Data.Grade.Equals("A")) tempID += "<color=#ffe500>";
                        else if (_Item.Data.Grade.Equals("B")) tempID += "<color=#00ffe0>";
                        else if (_Item.Data.Grade.Equals("C")) tempID += "<color=#1dff1d>";
                        else if (_Item.Data.Grade.Equals("D")) tempID += "<color=#999BFF>";
                        else if (_Item.Data.Grade.Equals("E")) tempID += "<color=#FFFFFF>";
                        else if (_Item.Data.Grade.Equals("F")) tempID += "<color=#A0A0A0>";
                        tempID += LocalizeManager.GetLocalize(_Item.Data.ID) + "</color>";
                        GameObject.Find("Canvas").transform.Find("TutorialText4").GetComponent<Text>().text = tempID;
                    }
                    else
                        GameObject.Find("Canvas").transform.Find("TutorialText4").GetComponent<Text>().text = LocalizeManager.GetLocalize("recycle_item_cont");
                    GameObject.Find("Canvas").transform.Find("TutorialText4").transform.position = Camera.main.WorldToScreenPoint(pos);
                    if (PDG.Dungeon.isRecycle && !isChoiceItem && !isBossRush)
                    {
                        GameObject.Find("Canvas").transform.Find("TutorialText4").Find("Recycle").gameObject.SetActive(true);
                        if ((PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_RECYCLE], true) || PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_RECYCLE], true)) && PDG.Dungeon.instance.closeCoolTime > 0.5f)
                        {
                            PDG.Dungeon.instance.closeCoolTime = 0.0f;
                            if (!isRecycleBefore)
                                isRecycleBefore = true;
                            else
                            {
                                if (SteamStatsAndAchievements.instance != null) SteamStatsAndAchievements.instance.SettingStat("stat_recycle_count");
                                SoundManager.instance.StartAudio(new string[1] { "Object/recycle" }, VOLUME_TYPE.EFFECT);
                                if (recycleEffect)
                                {
                                    GameObject obj2 = Instantiate(recycleEffect);
                                    obj2.transform.position = transform.position;
                                    obj2.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
                                   Destroy(obj2, 2f);
                                }
                                int dropCount = Random.Range(3, 7);
                                Vector3 tPos = transform.position;
                                tPos.y += 1.0f;
                                for (int i = 0; i < dropCount; i++)
                                {
                                    float angle = -90.0f;
                                    angle += (i * (360.0f / dropCount));
                                    Vector3 pos2 = GetPosition(tPos, angle);
                                    Vector3 dir = pos2 - tPos;
                                    dir.Normalize();
                                    GameObject obj = ObjManager.Call().GetObject("Item");
                                    if (obj != null)
                                    {
                                        Item _item = obj.GetComponent<Item>();
                                        _item._Item = new ItemInfo
                                        {
                                            Data = System.Array.Find(SheetManager.Instance.ItemsDB.dataArray, item => item.ID.Equals("glowing_skull"))
                                        };
                                        _item.dir = dir;
                                        _item.rPow = 0.25f;
                                        _item.itemCount = 1;
                                        obj.transform.position = pos2 + (dir * 0.25f);
                                        obj.SetActive(true);
                                    }
                                }
                                GameObject.Find("Canvas").transform.Find("TutorialText4").gameObject.SetActive(false);
                                gameObject.SetActive(false);

                                PDG.Dungeon.ItemSelected = false;
                                PDG.Dungeon.SelecingItem = null;
                                isRecycleBefore = false;
                            }
                        }
                    }
                    if ((PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ACTIVE], true) || PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_ACTIVE], true)) && PDG.Dungeon.instance.closeCoolTime > 0.5f)
                    {
                        if (Inventorys.Instance.AddItem(_Item, itemCount, true))
                        {
                            PDG.Dungeon.instance.closeCoolTime = 0.0f;
                            GameObject.Find("Canvas").transform.Find("TutorialText4").gameObject.SetActive(false);
                            GameObject obj = ObjManager.Call().GetObject("GetItem");
                            if (obj != null)
                            {
                                obj.transform.position = transform.position;
                                obj.SetActive(true);
                            }
                            PDG.Player.instance.SettingItem(_Item.Data);
                            if (isChoiceItem) PDG.Dungeon.isChoiceItem = true;
                            if (nCapsuleNum == 0) ItemCapsule.isGetItem_0 = true;
                            else if (nCapsuleNum == 1) ItemCapsule.isGetItem_1 = true;
                            else if (nCapsuleNum == 2) ItemCapsule.isGetItem_2 = true;
                            PDG.Dungeon.ItemSelected = false;
                            PDG.Dungeon.SelecingItem = null;
                            isChoiceItem = false;
                            gameObject.SetActive(false);
                        }
                    }
                }
                else
                {
                    if (Inventorys.Instance.AddItem(_Item, itemCount, true))
                    {
                        GameObject obj = ObjManager.Call().GetObject("GetItem");
                        if (obj != null)
                        {
                            obj.transform.position = transform.position;
                            obj.SetActive(true);
                        }
                        gameObject.SetActive(false);
                        return;
                    }
                }
            }
        }
        else
        {
            if (PDG.Dungeon.SelecingItem == gameObject)
            {
                PDG.Dungeon.SelecingItem = null;
                if (PDG.Dungeon.ItemSelected) PDG.Dungeon.ItemSelected = false;
                isRecycleBefore = false;
                GameObject.Find("Canvas").transform.Find("TutorialText4").gameObject.SetActive(false);
            }
            sr.sprite = defaultSprite;
        }
        if (ActiveTimeNow > ActiveTime)
        {
            rigid.velocity = Vector2.zero;
        }
    }
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, 0.5f);
    }
    #region -- Tool --
    public static float GetAngle(Vector3 vStart, Vector3 vEnd)
    {
        Vector3 v = vEnd - vStart;

        return Mathf.Atan2(v.y, v.x) * Mathf.Rad2Deg;
    }
    public static Vector3 GetPosition(Vector3 vStart, float angle)
    {
        float x = Mathf.Cos(Mathf.Deg2Rad * angle) + vStart.x;
        float y = Mathf.Sin(Mathf.Deg2Rad * angle) + vStart.y;

        return new Vector3(x, y, 0);
    }
    private int GetPercentege(float _per, string[] list)
    {
        int[] per = new int[Mathf.FloorToInt(100 * list.Length)];
        for (int i = 0; i < per.Length; i++) per[i] = -1;

        for (int i = 0; i < list.Length; i++)
        {
            for (int j = 0; j < Mathf.FloorToInt(_per * 100); j++)
            {
                per[(i * 100) + j] = i;
            }
        }
        if (per.Length == 0) return -1;
        return per[Random.Range(0, per.Length)];
    }
    #endregion
}
