﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GetItem : MonoBehaviour
{
    float t = 0.0f;
    private void OnEnable()
    {
        t = 0.0f;
    }

    private void Update()
    {
        t += Time.deltaTime;

        if(t > 1.0f)
        {
            gameObject.SetActive(false);
        }
    }
}
