﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;



[System.Serializable]
public class ItemSlot : MonoBehaviour
{
    public Image img;
    [SerializeField] private Animator animator;
    [SerializeField] private Text countText;

    public int itemCount = 0;

    private void OnValidate()
    {
        countText = GetComponentInChildren<Text>();
        img = GetComponent<Image>();
        animator = GetComponent<Animator>();
    }

    public ItemInfo _Item = null;

    private void OnEnable()
    {
        if(_Item==null)
        {
            img.color = new Color(0, 0, 0, 0);
        }
        else if (_Item.Data == null)
        {
            img.color = new Color(0, 0, 0, 0);
        }
        else if (_Item.Data.ID.Equals(string.Empty))
        {
            img.color = new Color(0, 0, 0, 0);
        }
        else
        {
            img.color = new Color(1, 1, 1, 1);
        }
    }

    public void SetItem(ItemInfo itemss)
    {
        _Item = itemss;
        if (_Item != null)
        {
            img.color = Color.white;
            img.sprite = Resources.Load<Sprite>("Item/" + _Item.Data.Type1 + "/" + _Item.Data.Type2 + "/" + _Item.Data.ID);
            RuntimeAnimatorController run = Resources.Load<RuntimeAnimatorController>("Item/" + _Item.Data.Type1 + "/" + _Item.Data.Type2 + "/" + _Item.Data.ID + "/" + _Item.Data.ID);
            if (run == null)
            {
                animator.enabled = false;
            }
            else
            {
                animator.runtimeAnimatorController = run;
                animator.enabled = true;
            }
        }
        else
        {
            img.color = new Color32(0, 0, 0, 0);
            animator.runtimeAnimatorController = null;
            animator.enabled = false;
            img.sprite = null;
            itemCount = 0;
            countText.gameObject.SetActive(false);
        }
    }

    private void Awake()
    {
        if (countText == null) countText = GetComponentInChildren<Text>();
        if (img == null) img = GetComponent<Image>();
        if (animator == null) animator = GetComponent<Animator>();
    }

    private void Update()
    {
        if (countText == null) countText = GetComponentInChildren<Text>(true);
        if (img == null) img = GetComponent<Image>();
        if (animator == null) animator = GetComponent<Animator>();

        if (itemCount == 1 && countText.gameObject.activeSelf) countText.gameObject.SetActive(false);
        else if (itemCount < 1 && countText.gameObject.activeSelf) countText.gameObject.SetActive(false);
        else if (itemCount > 1 && !countText.gameObject.activeSelf)
        {
            countText.text = itemCount.ToString();
            countText.gameObject.SetActive(true);
        }

        if (!itemCount.ToString().Equals(countText.text))
        {
            countText.text = itemCount.ToString();
        }
    }

    public void SelectedItem()
    {
        if (_Item == null)
        {
        }
        else if (_Item.Data == null)
        {
        }
        else if (_Item.Data.ID.Equals(string.Empty))
        {
        }
        else
        {
            SoundManager.instance.StartAudio(new string[1] { "Click_0" },  VOLUME_TYPE.EFFECT);
            Inventorys.Instance.SelectedItem(_Item);
        }
    }

}
