﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Inventorys : MonoBehaviour
{
    public static Inventorys Instance;
    public ItemSlot[] slots;
    public Image selectedImg;
    public Text selectText;
    public Transform BuffParant;
    public Image[] QuickSlots;


    [Header("Coin")]
    public Text goldbarText;
    public int goldbar = 0;
    public Text glowingSkullText;
    public int glowindskull = 0;
    public int glowindskull2 = 0;
    public Text keyText;
    public int key = 0;
    public Text reinforceText;
    public int reinforcePoint = 0;
    public Image rightCollTimeImage;
    public Animator rightCollTImeAnim;
    public Animator Q_CollTimeAnim;
    public Image Q_CollTimeImage;

    public Text CollTimeText_Q;
    public Text CollTimeText_RB;
    [Header("Predator")]
    public GameObject predatorObj;
    public Text CollTimeText_Predator;
    public Animator predatorCollTimeAnim;
    public Image predatorCollTimeImage;

    [Header("Buff")]
    List<GameObject> BuffList = new List<GameObject>();

    [Header("GetItem")]
    public GameObject getItemObj;
    public Image getItemImage;
    public Text getItemText;

    [Header("ItemCheck")]
    public List<ItemInfo> shopOpenItems = new List<ItemInfo>();
    public List<ItemInfo> DiscardedItem = new List<ItemInfo>();
    public List<String> DiscardedItemString = new List<String>();
    public GameObject ItemGuideObj;
    public bool isGuid = false;
    public GameObject DefenceGuage_LB;
    public GameObject DefenceGuage_RB;
    public GameObject DefenceGuage_Q;
    LocalizeData lData;

    private Sprite nowSelectedItemSprite;
    private string nowSelectedItemId;
    [SerializeField] private Font defaultFont;
    [Header("ICON TEXT")]
    [SerializeField] private Image[] StateImages;
    [SerializeField] private Text[] StateValues;
    [SerializeField] private Sprite[] stateIcons;


    private void Awake()
    {
        if (Instance == null) Instance = this;
    }
    private void Start()
    {
        for(int i=0; i< slots.Length; i++)
        {
            if (slots[i]._Item == null) slots[i].SetItem(null);
        }
        int t = SaveAndLoadManager.instance.LoadItem("glowing_skull");
        if (t == -1) glowindskull = 0;
        else glowindskull = t;

        lData = Array.Find(SheetManager.Instance.LocalDB.dataArray, item => item.ID.Equals("ui_reinforcecouting"));
    }
    private void Update()
    {
        if (!goldbar.ToString().Equals(goldbarText.text)) goldbarText.text = goldbar.ToString();
        if (PDG.Dungeon.instance.isBossRush)
        {
            if (!glowindskull2.ToString().Equals(glowingSkullText.text)) glowingSkullText.text = glowindskull2.ToString();
        }
        else
        {
            if (!glowindskull.ToString().Equals(glowingSkullText.text)) glowingSkullText.text = glowindskull.ToString();
        }
        if (!key.ToString().Equals(keyText.text)) keyText.text = key.ToString();

        if(LocalizeManager.lang == LANGUAGE_SHIFTRICK.ENG)
        {
            if (!(lData.ENG + reinforcePoint.ToString()).Equals(reinforceText.text)) reinforceText.text = (lData.ENG + " : " + reinforcePoint.ToString());
        }
        else if (LocalizeManager.lang == LANGUAGE_SHIFTRICK.KOR)
        {
            if (!(lData.KOR + reinforcePoint.ToString()).Equals(reinforceText.text)) reinforceText.text = (lData.KOR + " : " + reinforcePoint.ToString());
        }
        else if (LocalizeManager.lang == LANGUAGE_SHIFTRICK.CHN_S)
        {
            if (!(lData.CHNS + reinforcePoint.ToString()).Equals(reinforceText.text)) reinforceText.text = (lData.CHNS + " : " + reinforcePoint.ToString());
        }
        else if (LocalizeManager.lang == LANGUAGE_SHIFTRICK.CHN_T)
        {
            if (!(lData.CHNT + reinforcePoint.ToString()).Equals(reinforceText.text)) reinforceText.text = (lData.CHNT + " : " + reinforcePoint.ToString());
        }
        else if (LocalizeManager.lang == LANGUAGE_SHIFTRICK.JA)
        {
            if (!(lData.JA + reinforcePoint.ToString()).Equals(reinforceText.text)) reinforceText.text = (lData.JA + " : " + reinforcePoint.ToString());
        }
        else if (LocalizeManager.lang == LANGUAGE_SHIFTRICK.RU)
        {
            if (!(lData.RU + reinforcePoint.ToString()).Equals(reinforceText.text)) reinforceText.text = (lData.RU + " : " + reinforcePoint.ToString());
        }

        for (int i = 0; i < BuffList.Count; i++) BuffList[i].transform.localScale = Vector3.one;

    }
    public ItemInfo GetRandomItem()
    {
        List<ItemInfo> tempList = new List<ItemInfo>();
        for (int i = 0; i < slots.Length; i++)
        {
            if (slots[i]._Item != null)
            {
                if (slots[i]._Item.Data.ID.Equals(string.Empty))
                {
                    tempList.Add(slots[i]._Item);
                }
            }
        }

        if (tempList.Count > 0) return tempList[UnityEngine.Random.Range(0, tempList.Count)];

        return null;
    }
    public void InventoryReset(bool bossReset = false)
    {
        if (PDG.Dungeon.instance.isBossRush && !bossReset) return;
        PDG.Player.bFirstSetting = false;
        goldbar = 0;
        key = 0;
        reinforcePoint = 0;
        PDG.Hero.isTeleport = false;
        for (int i = 0; i < slots.Length; i++)
        {
            if (slots[i] == null) continue;
            if (slots[i]._Item == null) continue;
            if (slots[i]._Item.Data == null) continue;

            if (!slots[i]._Item.Data.ID.Equals(string.Empty))
            {
                if(bossReset)
                {
                    for (int j = 0; j < slots[i].itemCount; j++)
                        PDG.Player.instance.DeleteItem(slots[i]._Item.Data);
                }
                DeleteBuff(Resources.Load<Sprite>("Item/" + slots[i]._Item.Data.Type1 + "/" + slots[i]._Item.Data.Type2 + "/" + slots[i]._Item.Data.ID));
            }
        }

        for (int i = 0; i < slots.Length; i++)
        {
            slots[i].SetItem(null);
        }
        ResultPanelScript.instance.eliteList.Clear();
    }
    public bool AddItem(ItemsData item, int itemCount, bool uiOn, bool isFirst)
    {
        ItemInfo info = new ItemInfo
        {
            Data = CopyItem(item)
        };

        return AddItem(info, itemCount, uiOn, isFirst);
    }
    public bool AddItem(ItemInfo item, int itemCount, bool uiOn, bool isFirst = false)
    {
        item.Data = CopyItem(item.Data);
        if (item.Data.Type1.Equals("Coin"))
        {
            SoundManager.instance.StartAudio(new string[1] { "Object/coin" }, VOLUME_TYPE.EFFECT);
            if (item.Data.Type2.Equals("Gold")) goldbar += itemCount;
            else if (item.Data.Type2.Equals("Skull"))
            {
                StateGroup.instance.nMutantCell++;
                glowindskull += itemCount;
                ResultPanelScript.instance.skullCount++;
                if (SteamStatsAndAchievements.instance != null) SteamStatsAndAchievements.instance.SettingStat("stat_glowing_total_count", itemCount);
                if (SteamStatsAndAchievements.instance != null) SteamStatsAndAchievements.instance.SettingStat("stage_glowing_max_count", glowindskull, true);
                SaveAndLoadManager.instance.SaveItem(new ItemSave
                {
                    _id = item.Data.ID,
                    _nCount = itemCount
                });
            }
            else if (item.Data.Type2.Equals("Key")) key += itemCount;
            else if (item.Data.Type2.Equals("Reinforce")) reinforcePoint++;
            return true;
        }
        if (item.Data.Type1.Equals("Material"))
        {
            if (item.Data.ID.Contains("CaseParts_"))
            {
                PDG.Dungeon.instance.isGetWarpPart = true;
                ShowGetItemUI(item.Data.Type2 + "_" + item.Data.ID, Resources.Load<Sprite>("Item/" + item.Data.Type1 + "/" + item.Data.Type2 + "/" + item.Data.ID));
                return true;
            }
            if (item.Data.ID.Equals("eyes_fire"))
            {
                ShowGetItemUI(item.Data.ID + "_cont", Resources.Load<Sprite>("Item/" + item.Data.Type1 + "/" + item.Data.Type2 + "/" + item.Data.ID));
            }
            SaveAndLoadManager.instance.SaveItem(new ItemSave
            {
                _id = item.Data.Type2 + "_" + item.Data.ID,
                _nCount = itemCount
            });
            if (SteamStatsAndAchievements.instance != null) SteamStatsAndAchievements.instance.SettingStat("stat_material_total_count");
            SoundManager.instance.StartAudio(new string[1] { "Object/coin" }, VOLUME_TYPE.EFFECT);
            bool tt = PDG.Dungeon.instance.MaterialList.ContainsKey(item.Data.Type2 + "_" + item.Data.ID);
            if (tt)
            {
                PDG.Dungeon.instance.MaterialList[item.Data.Type2 + "_" + item.Data.ID] += itemCount;
            }
            else
            {
                PDG.Dungeon.instance.MaterialList.Add(item.Data.Type2 + "_" + item.Data.ID, itemCount);
            }
            return true;
        }
        if (item.Data.Type1.Equals("Potion"))
        {
            SoundManager.instance.StartAudio(new string[1] { "Object/hpRecovery" }, VOLUME_TYPE.EFFECT);
            if (item.Data.ID.Equals("recovery_syringe_1"))
            {
                bool b = PDG.Player.instance.HpRecovery((int)item.Data.Buffamount[0]);
                if (!b)
                    TextOpenController.instance.ShowText(LocalizeManager.GetLocalize("recovery_full_hp"), Color.white, 24, PDG.Player.instance.gameObject, 2.0f, false);
                return b;
            }
            if (item.Data.ID.Equals("recovery_syringe_2"))
            {
                bool b = PDG.Player.instance.HpRecovery((int)item.Data.Buffamount[0]);
                if (!b)
                    TextOpenController.instance.ShowText(LocalizeManager.GetLocalize("recovery_full_hp"), Color.white, 24, PDG.Player.instance.gameObject, 2.0f, false);
                return b;
            }
            if (item.Data.ID.Equals("emergency_kit")) return PDG.Player.instance.HpRecovery((int)item.Data.Buffamount[0]);
            if (item.Data.ID.Equals("anger_control"))
            {
                ResultPanelScript.instance.Q_SkillCount += (int)item.Data.Buffamount[0];
            }
            if (item.Data.ID.Contains("stats_"))
            {
                PDG.Player.instance.SettingItem(item.Data);
            }
            return true;
        }
        if(item.Data.Type1.Equals("Weapon") || item.Data.ID.Equals("avoid_enhance") || item.Data.ID.Equals("rolling"))
        {
            List<AchievementData> savedatas = new List<AchievementData>();
            if (item.Data.ID.Equals("secret_lasergun"))
            {
                AchievementsData aData = System.Array.Find(SheetManager.Instance.AchievementsDB.dataArray, item2 => item2.ID.Equals("collection_0"));
                savedatas.Add(new AchievementData { _id = aData.ID, _cur = 1, _max = aData.MAX, _clear = false });
            }
            if (item.Data.ID.Equals("secret_blade"))
            {
                AchievementsData aData = System.Array.Find(SheetManager.Instance.AchievementsDB.dataArray, item2 => item2.ID.Equals("collection_1"));
                savedatas.Add(new AchievementData { _id = aData.ID, _cur = 1, _max = aData.MAX, _clear = false });
            }
            if (item.Data.ID.Equals("secret_watergun"))
            {
                AchievementsData aData = System.Array.Find(SheetManager.Instance.AchievementsDB.dataArray, item2 => item2.ID.Equals("collection_2"));
                savedatas.Add(new AchievementData { _id = aData.ID, _cur = 1, _max = aData.MAX, _clear = false });
            }
            SaveAndLoadManager.instance.SaveAchievements(savedatas.ToArray());
            SaveAndLoadManager.instance.SaveCollection(new CollectionData { _id = item.Data.ID, _isOpen = true, _isWeapon = true });
            bool checks = false;
            for (int i = 0; i < slots.Length; i++)
            {
                if (slots[i]._Item != null)
                {
                    if (slots[i]._Item.Data.Type3.Equals(item.Data.Type3))
                    {
                        ItemInfo info = slots[i]._Item;
                        DropItem(info, slots[i].itemCount, PDG.Dungeon.instance.isBossRush);

                        string id = slots[i]._Item.Data.ID;
                        if (id.Equals("energy_release_2") || id.Equals("frozen_nova_2") || id.Equals("frame_nova_2")) 
                        {
                            id = id.Substring(0, id.Length - 2);
                        }

                        DeleteBuff(Resources.Load<Sprite>("Item/" + slots[i]._Item.Data.Type1 + "/" + slots[i]._Item.Data.Type2 + "/" + id));
                        slots[i].itemCount = itemCount;
                        slots[i].SetItem(item);
                        AddBuff(Resources.Load<Sprite>("Item/" + slots[i]._Item.Data.Type1 + "/" + slots[i]._Item.Data.Type2 + "/" + slots[i]._Item.Data.ID), slots[i]._Item.Data);
                        //if (uiOn) ShowGetItemUI(slots[i]._Item.Data.ID, slots[i].img.sprite);
                        SelectedItem(item);
                        checks = true;
                        break;
                    }
                }
            }
            SlotSort();
            if (checks)
            {
                if (!isFirst)
                {
                    SoundManager.instance.StartAudio(new string[1] { "Object/coin" }, VOLUME_TYPE.EFFECT);
                }
                return checks;
            }
        }
        bool check = false;
        for (int i = 0; i < slots.Length; i++)
        {
            if (slots[i]._Item != null)
            {
                if(slots[i]._Item.Data.ID.Equals(item.Data.ID))
                {
                    slots[i].itemCount += itemCount;
                    for (int j = 0; j < BuffList.Count; j++)
                    {
                        if (BuffList[j].name.Equals(item.Data.ID)) 
                        {
                            if (BuffList[j].transform.childCount > 0)
                            {
                                BuffList[j].transform.GetChild(0).GetComponent<Text>().text = slots[i].itemCount.ToString();
                            }
                            else
                            {
                                GameObject newObj = new GameObject();
                                RectTransform rt = newObj.AddComponent<RectTransform>();
                                rt.anchorMin = new Vector2(1, 0);
                                rt.anchorMax = new Vector2(1, 0);
                                rt.pivot = new Vector2(1, 0);

                                Text countText = newObj.AddComponent<Text>();
                                countText.alignment = TextAnchor.LowerRight;
                                countText.color = Color.white;
                                countText.fontSize = 20;
                                countText.horizontalOverflow = HorizontalWrapMode.Overflow;
                                countText.verticalOverflow = VerticalWrapMode.Overflow;
                                countText.raycastTarget = false;
                                countText.text = slots[i].itemCount.ToString();
                                countText.font = defaultFont;
                                newObj.transform.SetParent(BuffList[j].transform);
                                rt.anchoredPosition3D = new Vector3(-10, 5, 0);
                            }
                        }
                    }
                    if (uiOn) ShowGetItemUI(slots[i]._Item.Data.ID, slots[i].img.sprite);
                    check = true;
                    break;
                }
            }
        }
        if (check)
        {
            if (!isFirst)
            {
                SoundManager.instance.StartAudio(new string[1] { "Object/coin" }, VOLUME_TYPE.EFFECT);
                //SaveAndLoadManager.instance.GameSave(slots);
            }
            return check;
        }
        for (int i = 0; i < slots.Length; i++)
        {
            if(slots[i]._Item == null)
            {
                slots[i].itemCount = itemCount;
                slots[i].SetItem(item);
                if (uiOn) ShowGetItemUI(slots[i]._Item.Data.ID, slots[i].img.sprite);
                AddBuff(Resources.Load<Sprite>("Item/" + slots[i]._Item.Data.Type1 + "/" + slots[i]._Item.Data.Type2 + "/" + slots[i]._Item.Data.ID), slots[i]._Item.Data);

                SelectedItem(item);
                check = true;
                break;
            }
            else if (slots[i]._Item != null)
            {
                if (slots[i]._Item.Data.ID.Equals(string.Empty))
                {
                    slots[i].itemCount = itemCount;
                    slots[i].SetItem(item);
                    if (uiOn) ShowGetItemUI(slots[i]._Item.Data.ID, slots[i].img.sprite);
                    AddBuff(Resources.Load<Sprite>("Item/" + slots[i]._Item.Data.Type1 + "/" + slots[i]._Item.Data.Type2 + "/" + slots[i]._Item.Data.ID), slots[i]._Item.Data);
                    SelectedItem(item);
                    check = true;
                    break;
                }
            }
        }
        if (check)
        {
            if (!isFirst)
            {
                SoundManager.instance.StartAudio(new string[1] { "Object/coin" }, VOLUME_TYPE.EFFECT);
                //SaveAndLoadManager.instance.GameSave(slots);
            }
            return check;
        }
        return false;
    }
    public bool DeleteItem(string _type, int _count)
    {
        if(_type.Equals("Health"))
        {
            if (PDG.Player.instance.plusHp + PDG.Player.maxHp <= _count) return false;
            PDG.Player.instance.plusHp -= _count;
            PDG.Dungeon.curseHealth += _count;
            PDG.Player.instance.HpRecovery(0);
            return true;
        }
        else if(_type.Equals("Attack"))
        {
            if (PDG.Player.instance.damagePlus - _count <= 0) return false;
            if (PDG.Player.instance.damagePlusNear - _count <= 0) return false;
            PDG.Player.instance.damagePlus -= _count;
            if (PDG.Player.instance.damagePlus < 0) PDG.Player.instance.damagePlus = 0;
            PDG.Player.instance.damagePlusNear -= _count;
            if (PDG.Player.instance.damagePlusNear < 0) PDG.Player.instance.damagePlusNear = 0;
            PDG.Player.instance.damageQPlus -= _count;
            if (PDG.Player.instance.damageQPlus < 0) PDG.Player.instance.damageQPlus = 0;
            PDG.Player.instance.damageQPlusNear -= _count;
            if (PDG.Player.instance.damageQPlusNear < 0) PDG.Player.instance.damageQPlusNear = 0;
            PDG.Player.instance.damageRightPlus -= _count;
            if (PDG.Player.instance.damageRightPlus < 0) PDG.Player.instance.damageRightPlus = 0;
            PDG.Player.instance.damageRightPlusNear -= _count;
            if (PDG.Player.instance.damageRightPlusNear < 0) PDG.Player.instance.damageRightPlusNear = 0;
            PDG.Dungeon.curseAttack += _count;
            return true;
        }

        List<ItemsData> itemList = new List<ItemsData>();
        List<int> slotNumList = new List<int>();
        ItemsData[] tempArray = new ItemsData[_count];
        int[] tempArray2 = new int[_count];
        for (int j = 0; j < _count; j++)
        {
            itemList.Clear();
            slotNumList.Clear();
            if (_type.Equals("Passive"))
            {
                for (int i = 0; i < slots.Length; i++)
                {
                    if (slots[i]._Item != null)
                    {
                        if (slots[i]._Item.Data.Type1.Equals("Weapon")) continue;
                        if (slots[i]._Item.Data.Type1.Equals("Material")) continue;
                        if (slots[i]._Item.Data.Type1.Equals("Coin")) continue;
                        if (slots[i]._Item.Data.ID.Contains("stats_shield")) continue;
                        if (slots[i]._Item.Data.ID.Equals("painkiller")) continue;
                        if (slots[i]._Item.Data.ID.Equals("rolling")) continue;
                        if (slots[i]._Item.Data.ID.Equals("avoid_enhance")) continue;
                        if (slots[i]._Item.Data.Type1.Equals(_type))
                        {
                            itemList.Add(slots[i]._Item.Data);
                            slotNumList.Add(i);
                        }
                    }
                }
            }
            else if (_type.Equals("Random"))
            {
                for (int i = 0; i < slots.Length; i++)
                {
                    if (slots[i]._Item != null)
                    {
                        if (slots[i]._Item.Data.Type1.Equals("Weapon")) continue;
                        if (slots[i]._Item.Data.Type1.Equals("Material")) continue;
                        if (slots[i]._Item.Data.Type1.Equals("Coin")) continue;
                        if (slots[i]._Item.Data.ID.Contains("stats_shield")) continue;
                        if (slots[i]._Item.Data.ID.Equals("painkiller")) continue;
                        if (slots[i]._Item.Data.ID.Equals("rolling")) continue;
                        if (slots[i]._Item.Data.ID.Equals("avoid_enhance")) continue;
                        itemList.Add(slots[i]._Item.Data);
                        slotNumList.Add(i);
                    }
                }
            }
            if (itemList.Count <= 0) return false;
            int t = UnityEngine.Random.Range(0, itemList.Count);
            tempArray[j] = itemList[t];
            tempArray2[j] = slotNumList[t];
        }
        for (int j = 0; j < _count; j++)
        {
            if(slots[tempArray2[j]].itemCount == 1)
            {
                PDG.Player.instance.DeleteItem(tempArray[j]);
                ItemInfo info = slots[tempArray2[j]]._Item;
                int t = shopOpenItems.FindIndex(items => items.Equals(info));
                if (t != -1)
                {
                    shopOpenItems.RemoveAt(t);
                }
                if (!info.Data.ID.Equals("glowing_skull"))
                {
                    DiscardedItem.Add(info);
                    DiscardedItemString.Add(info.Data.ID);
                }
                DeleteBuff(tempArray[j].ID);
                slots[tempArray2[j]].SetItem(null);
            }
            else
            {
                slots[tempArray2[j]].itemCount -= 1;
                PDG.Player.instance.DeleteItem(tempArray[j]);
            }
        }
        SlotSort();
        return true;
    }
    public ItemsData CopyItem(ItemsData _data)
    {
        return new ItemsData
        {
            Aim = _data.Aim,
            Armspeed = _data.Armspeed,
            Armwave = _data.Armwave,
            Auto = _data.Auto,
            Autoaim = _data.Autoaim,
            Bulletdelay = _data.Bulletdelay,
            Bulletsize = _data.Bulletsize,
            Bulletspeed = _data.Bulletspeed,
            Cont = _data.Cont,
            Damage = _data.Damage,
            Elemental = _data.Elemental,
            Elementaltime = _data.Elementaltime,
            Force = _data.Force,
            Grade = _data.Grade,
            ID = _data.ID,
            Idskill = _data.Idskill,
            Maxdropcount = _data.Maxdropcount,
            Multishoot = _data.Multishoot,
            Name = _data.Name,
            Needunlock = _data.Needunlock,
            Range = _data.Range,
            Shootcount = _data.Shootcount,
            Shootdelay = _data.Shootdelay,
            Type1 = _data.Type1,
            Type2 = _data.Type2,
            Type3 = _data.Type3,
            Type4 = _data.Type4,
            Buffamount = _data.Buffamount,
            Cooltimeopen = _data.Cooltimeopen,
            Maxattackcount = _data.Maxattackcount,
            Ticktime = _data.Ticktime,
            Critical = _data.Critical,
            Criticaldamage = _data.Criticaldamage,
            Damagenear = _data.Damagenear,
            Attacktype = _data.Attacktype,
            Isfixed = _data.Isfixed,
            Type = _data.Type
        };
    }
    public void ShowGetItemUI(string _name, Sprite _sprite, bool _isGreen = false, string _unlockId = "")
    {
        StartCoroutine(OpenItemUI(LocalizeManager.GetLocalize(_name), _sprite, _isGreen, _unlockId));
    }
    public  Coroutine tempCorotine = null;
    private List<Sprite> spriteList = new List<Sprite>();
    IEnumerator OpenItemUI(string _name, Sprite _sprite, bool _isGreen, string _unlockId)
    {
        while (true)
        {
            if (tempCorotine == null && spriteList.Find(item => item.Equals(_sprite)) == null && _sprite != null)
            {
                if (!_sprite.name.Contains("CaseParts_"))
                    spriteList.Add(_sprite);
                tempCorotine = StartCoroutine(OpenItemUI2(_name, _sprite, _isGreen, _unlockId));
                break;
            }
            yield return null;
        }
    }
    IEnumerator OpenItemUI2(string _name, Sprite _sprite, bool _isGreen, string _unlockId)
    {

        if (_isGreen && !_unlockId.Equals("")) 
        {
            _name = _name.Replace("##", LocalizeManager.GetLocalize(_unlockId));
        }

        SoundManager.instance.StartAudio(new string[1] { "GetItem" }, VOLUME_TYPE.EFFECT);
        getItemImage.sprite = _sprite;
        getItemText.text = _name;
        float y = -171.0f;
        if (_isGreen)
            getItemObj.GetComponent<Image>().color = Color.green;
        else
            getItemObj.GetComponent<Image>().color = Color.white;
        getItemObj.GetComponent<RectTransform>().anchoredPosition3D = new Vector3(0, y, 0);
        while(true)
        {
            y += 5;
            getItemObj.GetComponent<RectTransform>().anchoredPosition3D = new Vector3(0, y, 0);
            if (y > 20.0f) break;
            yield return new WaitForSecondsRealtime(Time.unscaledDeltaTime);
        }
        yield return new WaitForSecondsRealtime(3.0f);
        while (true)
        {
            y -= 5;
            getItemObj.GetComponent<RectTransform>().anchoredPosition3D = new Vector3(0, y, 0);
            if (y < -171.0f) break;
            yield return new WaitForSecondsRealtime(Time.unscaledDeltaTime);
        }
        tempCorotine = null;
    }
    public void DropItem(ItemInfo _data, int count, bool _isBossRush)
    {
        if (_isBossRush) return;
        GameObject obj = ObjManager.Call().GetObject("Item");
        if (obj != null)
        {
            Item _item = obj.GetComponent<Item>();
            _item._Item = _data;
            _item.dir = Vector2.zero;
            _item.itemCount = count;
            _item.rPow = 0;
            obj.transform.position = PDG.Player.instance.transform.position;
            obj.SetActive(true);
            _item.isBossRush = _isBossRush;
        }
    }
    public void DropItem(string _id, int count, string _type = "", bool _isMaterial = false)
    {
        GameObject obj = ObjManager.Call().GetObject("Item");
        if (obj != null)
        {
            Item _item = obj.GetComponent<Item>();
            if (_isMaterial)
            {
                _item._Item = new ItemInfo
                {
                    Data = new ItemsData
                    {
                        Type1 = "Material",
                        Type2 = _type,
                        ID = _id
                    }
                };
            }
            else
            {
                _item._Item = new ItemInfo
                {
                    Data = Array.Find(SheetManager.Instance.ItemsDB.dataArray, item => item.ID.Equals(_id))
                };
                if (!_item._Item.Data.ID.Equals("glowing_skull"))
                {
                    DiscardedItem.Add(_item._Item);
                    DiscardedItemString.Add(_item._Item.Data.ID);
                }
            }
            _item.dir = Vector2.zero;
            _item.itemCount = count;
            _item.rPow = 0;
            obj.transform.position = PDG.Player.instance.transform.position;
            obj.SetActive(true);
        }
    }
    public void AddBuff(Sprite _sprite, ItemsData _iData)
    {
        GameObject obj = new GameObject(_sprite.name);
        Image img = obj.AddComponent<Image>();
        img.sprite = _sprite;
        img.raycastTarget = false;
        RuntimeAnimatorController run = Resources.Load<RuntimeAnimatorController>("Item/" + _iData.Type1 + "/" + _iData.Type2 + "/" + _iData.ID + "/" + _iData.ID);
        if (run != null)
        {
            Animator animator = obj.AddComponent<Animator>();
            animator.updateMode = AnimatorUpdateMode.UnscaledTime;
            animator.runtimeAnimatorController = run;
        }

        obj.transform.SetParent(BuffParant);
        BuffList.Add(obj);
    }
    public void DeleteBuff(Sprite _sprite)
    {
        for (int i = 0; i < BuffList.Count; i++)
        {
            if(BuffList[i].GetComponent<Animator>() && BuffList[i].GetComponent<Animator>().enabled && BuffList[i].GetComponent<Animator>().runtimeAnimatorController.name == _sprite.name)
            {
                GameObject obj = BuffList[i];
                BuffList.RemoveAt(i);
                Destroy(obj);
                break;
            }
            else if (BuffList[i].GetComponent<Image>().sprite == _sprite)
            {
                GameObject obj = BuffList[i];
                BuffList.RemoveAt(i);
                Destroy(obj);
                break;
            }
        }
    }
    public void DeleteBuff(string _name)
    {
        for (int i = 0; i < BuffList.Count; i++)
        {
            if (BuffList[i].GetComponent<Image>().sprite.name == _name)
            {
                GameObject obj = BuffList[i];
                BuffList.RemoveAt(i);
                Destroy(obj);
                break;
            }
        }
    }
    public void SelectedItem(ItemInfo _item)
    {
        selectedImg.color = Color.white;
        selectedImg.sprite = Resources.Load<Sprite>("Item/" + _item.Data.Type1 + "/" + _item.Data.Type2 + "/" + _item.Data.ID);

        if (_item.Data.Grade.Equals("S") || _item.Data.Grade.Equals("G")) selectText.text = "<color=#ff3700>";
        else if (_item.Data.Grade.Equals("A")) selectText.text = "<color=#ffe500>";
        else if (_item.Data.Grade.Equals("B")) selectText.text = "<color=#00ffe0>";
        else if (_item.Data.Grade.Equals("C")) selectText.text = "<color=#1dff1d>";
        else if (_item.Data.Grade.Equals("D")) selectText.text = "<color=#999BFF>";
        else if (_item.Data.Grade.Equals("E")) selectText.text = "<color=#FFFFFF>";
        else if (_item.Data.Grade.Equals("F")) selectText.text = "<color=#A0A0A0>";

        if (PDG.Player.instance.isNovaReinforce && (_item.Data.ID.Equals("energy_release") || _item.Data.ID.Equals("frozen_nova") || _item.Data.ID.Equals("frame_nova")))
            selectText.text += LocalizeManager.GetLocalize(_item.Data.ID + "_2") + "</color>\n\n" + LocalizeManager.GetLocalize(_item.Data.ID + "_2" + "_cont");
        else
        {
            if (_item.Data.ID.Equals("q_change"))
            {
                selectText.text += LocalizeManager.GetLocalize(_item.Data.ID) + "</color>\n\n" + LocalizeManager.GetLocalize(_item.Data.ID + "_cont") + "\n";
                selectText.text += LocalizeManager.GetLocalize("bodyremodeling_q_type_" + PDG.Dungeon.instance.nQChange);
            }
            else
                selectText.text += LocalizeManager.GetLocalize(_item.Data.ID) + "</color>\n\n" + LocalizeManager.GetLocalize(_item.Data.ID + "_cont");
        }

        nowSelectedItemSprite = selectedImg.sprite;
        nowSelectedItemId = _item.Data.ID;
        ItemInfoEnterText(nowSelectedItemId);
    }
    public void SelectedItem(string _id, Sprite _sprite)
    {
        nowSelectedItemSprite = _sprite;
        nowSelectedItemId = _id;
        ItemsData iData = Array.Find(SheetManager.Instance.ItemsDB.dataArray, item => item.ID.Equals(_id));
        if (iData.Grade.Equals("S") || iData.Grade.Equals("G")) selectText.text = "<color=#ff3700>";
        else if (iData.Grade.Equals("A")) selectText.text = "<color=#ffe500>";
        else if (iData.Grade.Equals("B")) selectText.text = "<color=#00ffe0>";
        else if (iData.Grade.Equals("C")) selectText.text = "<color=#1dff1d>";
        else if (iData.Grade.Equals("D")) selectText.text = "<color=#999BFF>";
        else if (iData.Grade.Equals("E")) selectText.text = "<color=#FFFFFF>";
        else if (iData.Grade.Equals("F")) selectText.text = "<color=#efefef>";

        SoundManager.instance.StartAudio(new string[1] { "Click_0" }, VOLUME_TYPE.EFFECT);
        selectedImg.color = Color.white;
        selectedImg.sprite = _sprite;
        if (PDG.Player.instance.isNovaReinforce && (_id.Equals("energy_release") || _id.Equals("frozen_nova") || _id.Equals("frame_nova")))
            selectText.text += LocalizeManager.GetLocalize(_id + "_2") + "</color>\n\n" + LocalizeManager.GetLocalize(_id + "_2" + "_cont");
        else
            selectText.text += LocalizeManager.GetLocalize(_id) + "</color>\n\n" + LocalizeManager.GetLocalize(_id + "_cont");
        ItemInfoEnterText(nowSelectedItemId);
    }
    void ItemInfoEnterText(string _id)
    {
        string tempID = _id;
        int ttt = tempID.LastIndexOf('_');
        if (ttt != -1 && ttt == tempID.Length - 2)
            tempID = tempID.Substring(0, tempID.Length - 2);
        int pDam = 0;
        int pCri = 0;
        int pCriD = 0;
        int pRange = 0;
        int pAim = 0;
        int pMulti = 0;
        int pShot = 0;
        int pShotD = 0;
        ManufacturesData loadData = SaveAndLoadManager.instance.LoadManufacture(tempID, PDG.Dungeon.instance.isBossRush);
        if (!loadData.id.Equals(string.Empty) || !loadData.id.Equals(""))
        {
            for (int i = 0; i < loadData.stateid.Count; i++)
            {
                if (loadData.stateid[i].ToUpper().Equals("Attack".ToUpper()))
                    pDam = loadData.statevalue[i];
                else if (loadData.stateid[i].ToUpper().Equals("Critical".ToUpper()))
                    pCri = loadData.statevalue[i];
                else if (loadData.stateid[i].ToUpper().Equals("CriticalDamage".ToUpper()))
                    pCriD = loadData.statevalue[i];
                else if (loadData.stateid[i].ToUpper().Equals("Range".ToUpper()))
                    pRange = loadData.statevalue[i];
                else if (loadData.stateid[i].ToUpper().Equals("Aim".ToUpper()))
                    pAim = loadData.statevalue[i];
                else if (loadData.stateid[i].ToUpper().Equals("MultiShoot".ToUpper()))
                    pMulti = loadData.statevalue[i];
                else if (loadData.stateid[i].ToUpper().Equals("ShootCount".ToUpper()))
                    pShot = loadData.statevalue[i];
                else if (loadData.stateid[i].ToUpper().Equals("ShootDelay".ToUpper()))
                    pShotD = loadData.statevalue[i];
            }
        }

        int iDataIndex = Array.FindIndex(SheetManager.Instance.ItemsDB.dataArray, item => item.ID.Equals(_id));
        if (iDataIndex != -1) 
        {
            ItemsData iData = SheetManager.Instance.ItemsDB.dataArray[iDataIndex];
            if (iData.ID.Contains("q_change") || iData.ID.Contains("cell_shield"))
            {
                StateValues[0].text = "-";
                StateValues[1].text = "-";
                StateValues[2].text = "-";
                StateValues[3].text = "-";
                StateValues[4].text = "-";
                StateValues[5].text = "-";
                StateValues[6].text = "-";
                StateValues[7].text = "-";
            }
            else if (iData.Type1.Equals("Weapon"))
            {
                if (iData.Attacktype.Equals("M"))
                {
                    StateImages[0].sprite = Array.Find(stateIcons, item => item.name.Equals("item_icon_attack_m"));
                    if (pDam > 0)
                    {
                        StateValues[0].color = Color.green;
                        StateValues[0].text = (iData.Damagenear[0] + pDam) + " - " + (iData.Damagenear[1] + pDam);
                    }
                    else
                    {
                        StateValues[0].color = Color.white;
                        StateValues[0].text = iData.Damagenear[0] + " - " + iData.Damagenear[1];
                    }
                }
                else
                {
                    StateImages[0].sprite = Array.Find(stateIcons, item => item.name.Equals("item_icon_attack_l"));
                    if (pDam > 0)
                    {
                        StateValues[0].color = Color.green;
                        StateValues[0].text = (iData.Damage[0] + pDam) + " - " + (iData.Damage[1] + pDam);
                    }
                    else
                    {
                        StateValues[0].color = Color.white;
                        StateValues[0].text = iData.Damage[0] + " - " + iData.Damage[1];
                    }
                }

                if (pCri > 0)
                {
                    StateValues[1].color = Color.green;
                    StateValues[1].text = (iData.Critical + pCri).ToString();
                }
                else
                {
                    StateValues[1].color = Color.white;
                    StateValues[1].text = iData.Critical.ToString();
                }

                if (pCriD > 0)
                {
                    StateValues[2].color = Color.green;
                    StateValues[2].text = (iData.Criticaldamage + pCriD).ToString();
                }
                else
                {
                    StateValues[2].color = Color.white;
                    StateValues[2].text = iData.Criticaldamage.ToString();
                }

                if (pRange > 0)
                {
                    StateValues[3].color = Color.green;
                    StateValues[3].text = (iData.Range + pRange).ToString();
                }
                else
                {
                    StateValues[3].color = Color.white;
                    StateValues[3].text = iData.Range.ToString();
                }

                if (pAim > 0)
                {
                    StateValues[4].color = Color.green;
                    StateValues[4].text = (iData.Aim + pAim).ToString();
                }
                else
                {
                    StateValues[4].color = Color.white;
                    StateValues[4].text = iData.Aim.ToString();
                }

                if (pMulti > 0)
                {
                    StateValues[5].color = Color.green;
                    StateValues[5].text = (iData.Multishoot + pMulti).ToString();
                }
                else
                {
                    StateValues[5].color = Color.white;
                    StateValues[5].text = iData.Multishoot.ToString();
                }

                if (pShot > 0)
                {
                    StateValues[6].color = Color.green;
                    StateValues[6].text = (iData.Shootcount + pShot).ToString();
                }
                else
                {
                    StateValues[6].color = Color.white;
                    StateValues[6].text = iData.Shootcount.ToString();
                }

                if (pShotD > 0)
                {
                    StateValues[7].color = Color.green;
                    StateValues[7].text = (iData.Shootdelay + pShotD).ToString();
                }
                else
                {
                    StateValues[7].color = Color.white;
                    StateValues[7].text = iData.Shootdelay.ToString();
                }
            }
            else if (iData.Type1.Equals("Passive"))
            {
                StateValues[0].text = "-";
                StateValues[1].text = "-";
                StateValues[2].text = "-";
                StateValues[3].text = "-";
                StateValues[4].text = "-";
                StateValues[5].text = "-";
                StateValues[6].text = "-";
                StateValues[7].text = "-";
                if (iData.ID.Contains("blasting_fire")) StateValues[5].text = iData.Buffamount[0].ToString();
                if (iData.ID.Contains("continuous_shooting")) StateValues[6].text = iData.Buffamount[0].ToString();
                if (iData.ID.Contains("nerve_enhancer_arm")) StateValues[0].text = iData.Buffamount[1].ToString();
                if (iData.ID.Contains("nerve_enhancer_spine")) StateValues[7].text = iData.Buffamount[0].ToString();
                if (iData.ID.Contains("nerve_enhancer_leg")) StateValues[0].text = iData.Buffamount[1].ToString();
                if (iData.ID.Contains("nerve_enhancer_head")) StateValues[4].text = iData.Buffamount[0].ToString();
                if (iData.ID.Contains("muscle_enhancer_arm")) StateValues[0].text = iData.Buffamount[0].ToString();
                if (iData.ID.Contains("nerve_enhancer_eyes")) StateValues[3].text = iData.Buffamount[0].ToString();
                if (iData.ID.Contains("electric_cell")) StateValues[0].text = iData.Buffamount[1].ToString();
                if (iData.ID.Contains("dex_fire"))
                {
                    StateValues[4].text = iData.Buffamount[1].ToString();
                    StateValues[0].text = iData.Buffamount[2].ToString();
                    StateValues[1].text = iData.Buffamount[3].ToString();
                }
                if (iData.ID.Contains("increase_cri_damage")) StateValues[1].text = iData.Buffamount[0].ToString();
                if (iData.ID.Contains("attack_m_manual"))
                {
                    StateImages[0].sprite = Array.Find(stateIcons, item => item.name.Equals("item_icon_attack_m"));
                    StateValues[0].text = iData.Buffamount[0].ToString();
                }
                if (iData.ID.Contains("attack_r_manual"))
                {
                    StateImages[0].sprite = Array.Find(stateIcons, item => item.name.Equals("item_icon_attack_l"));
                    StateValues[0].text = iData.Buffamount[0].ToString();
                }
            }
            else
            {
                StateValues[0].text = "-";
                StateValues[1].text = "-";
                StateValues[2].text = "-";
                StateValues[3].text = "-";
                StateValues[4].text = "-";
                StateValues[5].text = "-";
                StateValues[6].text = "-";
                StateValues[7].text = "-";
            }
        }


    }
    public void LocaliChange()
    {
        selectedImg.color = Color.white;
        selectedImg.sprite = nowSelectedItemSprite;
        selectText.text = LocalizeManager.GetLocalize(nowSelectedItemId) + "\n\n" + LocalizeManager.GetLocalize(nowSelectedItemId + "_cont");
    }
    public bool UnlockAndMaxCountCheck(string _id)
    {
        ItemsData iData = Array.Find(SheetManager.Instance.ItemsDB.dataArray, item => item.ID.Equals(_id));
        if (iData.Type1.Equals("Material")) return false; // 재료는 나오지마라

        // 레벨체크
        bool levelMax = false;
        string str = _id;
        int tt = str.LastIndexOf('_');
        int tLevel = 0;
        if (tt != -1 && tt == str.Length - 2)
        {
            tLevel = int.Parse(str.Substring(str.Length - 1, 1));
            str = str.Substring(0, str.Length - 1);
            tLevel++;
            str += tLevel.ToString();
            int tIndex = Array.FindIndex(SheetManager.Instance.ItemsDB.dataArray, item => item.ID.Equals(str));
            if (tIndex == -1)
            {
                levelMax = true;
            }
        }
        else
        {
            str += "_1";
            int tIndex = Array.FindIndex(SheetManager.Instance.ItemsDB.dataArray, item => item.ID.Equals(str));
            if (tIndex == -1)
            {
                levelMax = true;
            }
        }
        if (!levelMax)
        {
            int loadUnlock = SaveAndLoadManager.instance.LoadUnlock(str);
            if (loadUnlock >= 1)
            {
                //이건 낮은 레벨
                return false;
            }
        }

        int nowCount = 0;
        // 슬롯에 아이템이 있으면 현재 카운트를 알아옴
        for (int i = 0; i < slots.Length; i++)
        {
            if (slots[i]._Item != null)
            {
                if (slots[i]._Item.Data.ID.Equals(_id))
                {
                    nowCount = slots[i].itemCount;
                }
            }
        }
        for (int i = 0; i < shopOpenItems.Count; i++)
        {
            if (shopOpenItems[i].Data.ID.Equals(_id))
            {
                nowCount += 1;
            }
        }
        for (int i = 0; i < DiscardedItem.Count; i++)
        {
            if (DiscardedItem[i].Data.ID.Equals(_id))
            {
                nowCount += 1;
            }
        }
        if (PDG.Dungeon.instance.isTestMode2)
        {
            for (int i = 0; i < PDG.Dungeon.instance.TestModeItemLIst.Count; i++)
            {
                if (PDG.Dungeon.instance.TestModeItemLIst[i].Equals(_id))
                {
                    nowCount += 1;
                }
            }
        }
        //한계 돌파
        if (nowCount >= iData.Maxdropcount) return false;
        bool isUnlock = false;
        //언락 확인
        if(iData.Needunlock)
        {
            UnlockListsData uData = Array.Find(SheetManager.Instance.UnlockListsDB.dataArray, item => item.Itemid.Equals(_id));
            int loadunlockCount = SaveAndLoadManager.instance.LoadUnlock(_id);
            if (uData == null) return false;
            if (loadunlockCount < uData.Unlockcount) isUnlock = true;
        }
        //언락 되지못한 아이템
        if (isUnlock) return false;
        return true;
    }
    public void BuffItemReSetting(ItemsData item)
    {
        for (int i = 0; i < slots.Length; i++)
        {
            if (slots[i]._Item != null)
            {
                if (slots[i]._Item.Data.Type1.Equals("Weapon")) continue;
                if (slots[i]._Item.Data.ID.Equals(item.Type3)) continue;
                if (!slots[i]._Item.Data.ID.Equals(string.Empty))
                {
                    PDG.Player.instance.SettingItem(slots[i]._Item.Data);
                }
            }
        }
    }
    public void BuffItemReSetting()
    {
        for (int i = 0; i < slots.Length; i++)
        {
            if (slots[i]._Item != null)
            {
                if (!slots[i]._Item.Data.ID.Equals(string.Empty))
                {
                    for (int j = 0; j < slots[i].itemCount; j++)
                        PDG.Player.instance.SettingItem(slots[i]._Item.Data);
                }
            }
        }
    }
    public int miniGameBulletCount = 10;
    public int miniGameBulletUnCount = 0;
    Coroutine guageCoroutine1 = null;
    Coroutine guageCoroutine2 = null;
    Coroutine guageCoroutine3 = null;
    public void GuageShaker(int type)
    {
        if (type == 0)
        {
            if (guageCoroutine1 == null) guageCoroutine1 = StartCoroutine(GuageShakerCoroutine(type));
        }
        if (type == 1)
        {
            if (guageCoroutine2 == null) guageCoroutine2 = StartCoroutine(GuageShakerCoroutine(type));
        }
        if (type == 2)
        {
            if (guageCoroutine3 == null) guageCoroutine3 = StartCoroutine(GuageShakerCoroutine(type));
        }
    }
    public int GetItemCount(string _id)
    {
        for (int i = 0; i < slots.Length; i++)
        {
            if (slots[i]._Item != null)
            {
                if (slots[i]._Item.Data.ID.Equals(_id))
                {
                    return slots[i].itemCount;
                }
            }
        }
        return 0;
    }
    IEnumerator GuageShakerCoroutine(int type)
    {
        Vector3 pos;
        if (type == 0)
        {
            pos = DefenceGuage_LB.transform.position;
            for (int i = 0; i < 10; i++)
            {
                DefenceGuage_LB.transform.position = new Vector3(pos.x + UnityEngine.Random.Range(-5, 5), pos.y + UnityEngine.Random.Range(-5, 5), 0);
                yield return new WaitForSeconds(Time.deltaTime);
            }
            DefenceGuage_LB.transform.position = pos;
            guageCoroutine1 = null;
        }
        else if (type == 1)
        {
            pos = DefenceGuage_RB.transform.position;
            for (int i = 0; i < 10; i++)
            {
                DefenceGuage_RB.transform.position = new Vector3(pos.x + UnityEngine.Random.Range(-5, 5), pos.y + UnityEngine.Random.Range(-5, 5), 0);
                yield return new WaitForSeconds(Time.deltaTime);
            }
            DefenceGuage_RB.transform.position = pos;
            guageCoroutine2 = null;
        }
        else if (type == 2)
        {
            pos = DefenceGuage_Q.transform.position;
            for (int i = 0; i < 10; i++)
            {
                DefenceGuage_Q.transform.position = new Vector3(pos.x + UnityEngine.Random.Range(-5, 5), pos.y + UnityEngine.Random.Range(-5, 5), 0);
                yield return new WaitForSeconds(Time.deltaTime);
            }
            DefenceGuage_Q.transform.position = pos;
            guageCoroutine3 = null;
        }
    }
    public void ItemRetire(string _id)
    {
        for (int i = 0; i < slots.Length; i++)
        {
            if (slots[i]._Item != null)
            {
                if (slots[i]._Item.Data.ID.Equals(_id))
                {
                    ItemInfo info = slots[i]._Item;
                    int t = shopOpenItems.FindIndex(items => items.Equals(info));
                    if (t != -1)
                    {
                        shopOpenItems.RemoveAt(t);
                    }
                    DiscardedItem.Add(info);
                    DiscardedItemString.Add(info.Data.ID);
                    DeleteBuff(Resources.Load<Sprite>("Item/" + slots[i]._Item.Data.Type1 + "/" + slots[i]._Item.Data.Type2 + "/" + slots[i]._Item.Data.ID));
                    slots[i].SetItem(null);
                    break;
                }
            }
        }
        SlotSort();
    }
    IEnumerator OpentheDoor()
    {
        GameObject.Find("Canvas").transform.Find("TutorialText").gameObject.SetActive(true);
        GameObject.Find("Canvas").transform.Find("TutorialText").GetComponent<Text>().text = LocalizeManager.GetLocalize("warp_open_title");
        yield return new WaitForSeconds(5.0f);
        if (GameObject.Find("Canvas").transform.Find("TutorialText").GetComponent<Text>().text == LocalizeManager.GetLocalize("warp_open_title"))
        {
            GameObject.Find("Canvas").transform.Find("TutorialText").gameObject.SetActive(false);
        }
    }
    public void SlotSort()
    {
        for (int i = 0; i < slots.Length - 1; i++)
        {
            if (slots[i]._Item != null && slots[i]._Item.Data != null) continue;
            for (int j = i; j < slots.Length; j++)
            {
                ItemInfo t = slots[i]._Item;
                int c = slots[i].itemCount;
                if (slots[j]._Item != null)
                {
                    slots[i]._Item = slots[j]._Item;
                    slots[i].itemCount = slots[j].itemCount;

                    slots[j].itemCount = c;
                    slots[j]._Item = t;
                    slots[i].SetItem(slots[i]._Item);
                    slots[j].SetItem(slots[j]._Item);
                    break;
                }
            }
        }
    }
}
