﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Entrance : MonoBehaviour
{
    public string stateName = string.Empty;
    public GameObject animObj;
    public GameObject entranceQuestionPanel;
    public Button YesBtn;
    public Button NoBtn;
    public Text entranceQuestionText;
    private float moveSpeed = 1500.0f;
    Coroutine endCoroutine;
    [SerializeField] private bool isBossRushExit = false;
    [SerializeField] private bool isBossRushEntrance = false;
    [SerializeField] private GameObject AjitPrefabs;
    private void Start()
    {
        if (entranceQuestionPanel == null) entranceQuestionPanel = GameObject.Find("Canvas").transform.Find("QuestionPanel").gameObject;
        if (entranceQuestionText == null) entranceQuestionText = entranceQuestionPanel.transform.Find("Text").GetComponent<Text>();
        if (YesBtn == null) YesBtn = entranceQuestionPanel.transform.Find("BtnPanel").Find("YesBtn").GetComponent<Button>();
        if (NoBtn == null) NoBtn = entranceQuestionPanel.transform.Find("BtnPanel").Find("NoBtn").GetComponent<Button>();
    }
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(transform.position, 4);
    }
    private void Update()
    {
        RaycastHit2D hit = Physics2D.CapsuleCast(transform.position, new Vector2(4, 2.5f), CapsuleDirection2D.Horizontal, 0, Vector2.zero, 0, 1 << 9);
        if (hit)
        {
            PDG.Dungeon.instance.newObjectSelectedObj = gameObject;
            Question();
        }
        else
        {
            if (endCoroutine == null && entranceQuestionPanel.activeSelf && PDG.Dungeon.instance.newObjectSelectedObj == gameObject)
            {
                PDG.Player.instance.isCameraOff = false;
                StopAllCoroutines();
                endCoroutine = StartCoroutine(EndQuestionEvent());
            }
        }

        if (entranceQuestionPanel.activeSelf && PDG.Dungeon.instance.newObjectSelectedObj == gameObject)
        {
            if ((Input.GetKeyDown(KeyCode.Escape) || Input.GetKeyDown(KeyCode.Backspace) || PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_CANCLE], true)) && PDG.Dungeon.instance.closeCoolTime > 0.2f)
            {
                PDG.Dungeon.instance.closeCoolTime = 0.0f;
                PDG.Player.instance.isCameraOff = false;
                StopAllCoroutines();
                StartCoroutine(EndQuestionEvent());
                SoundManager.instance.Click();
            }

            if ((Input.GetKeyDown(KeyCode.Return) || PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ACTIVE], true) || PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_CONFIRM], true)) && PDG.Dungeon.instance.closeCoolTime > 0.2f) 
            {
                PDG.Dungeon.instance.closeCoolTime = 0.0f;
                SoundManager.instance.StartAudio(new string[1] { "Object/active_F" }, VOLUME_TYPE.EFFECT);
                PDG.Player.instance.isCameraOff = false;
                if (isBossRushEntrance)
                {
                    PDG.Dungeon.instance.newObjectSelectedObj = null;
                    PDG.Dungeon.instance.newObjectSelected = false;
                    PDG.Dungeon.instance.fBossRushTime = 0.0f;
                    GameObject obj = Instantiate(AjitPrefabs);
                    obj.transform.position = new Vector3(22.5f, 22.5f, 0);
                    PDG.Player.instance.transform.position = new Vector3(22.5f, 22.5f, 0);
                    SaveAndLoadManager.instance.SaveItem(new ItemSave { _id = "glowing_skull2", _nCount = 200 }, true);
                    SaveAndLoadManager.instance.DeleteItemBossRush();
                    Inventorys.Instance.glowindskull2 = 200;
                    PDG.Dungeon.instance.isBossRush = true;
                    entranceQuestionPanel.SetActive(false);
                    PDG.Dungeon.instance.StartCoroutine(PDG.Dungeon.instance.BossRush());
                    Destroy(transform.root.gameObject);
                }
                else if (!isBossRushExit)
                {
                    entranceQuestionPanel.SetActive(false);
                    PDG.Dungeon.instance.StateCreate(stateName, entranceQuestionPanel, animObj);
                }
                else
                {
                    PDG.Dungeon.instance.isBossRush = false;
                    entranceQuestionPanel.SetActive(false);
                    SaveAndLoadManager.instance.DeleteManufacture(1);
                    PDG.Dungeon.instance.GoAjit();
                }
            }
        }
    }
    private void Question()
    {
        if (!entranceQuestionPanel.activeSelf)
        {
            if (PDG.Dungeon.instance.isBossRush)
            {
                string t = LocalizeManager.GetLocalize("bossrush_exit");
                entranceQuestionText.text = t;
            }
            else if (isBossRushEntrance)
            {
                string t = LocalizeManager.GetLocalize(stateName);
                entranceQuestionText.text = t;
            }
            else
            {
                LocalizeData localdata = System.Array.Find(SheetManager.Instance.LocalDB.dataArray, item => item.ID.Equals("stage_title_" + PDG.Dungeon.instance.stageRan[PDG.Dungeon.instance.StageLevel]));
                LocalizeData localdata2 = System.Array.Find(SheetManager.Instance.LocalDB.dataArray, item => item.ID.Equals("stage_entrance"));
                string statename = string.Empty;
                string statename2 = string.Empty;
                if (LocalizeManager.lang == LANGUAGE_SHIFTRICK.ENG) statename = localdata.ENG;
                else if (LocalizeManager.lang == LANGUAGE_SHIFTRICK.KOR) statename = localdata.KOR;
                else if (LocalizeManager.lang == LANGUAGE_SHIFTRICK.CHN_S) statename = localdata.CHNS;
                else if (LocalizeManager.lang == LANGUAGE_SHIFTRICK.CHN_T) statename = localdata.CHNT;
                else if (LocalizeManager.lang == LANGUAGE_SHIFTRICK.JA) statename = localdata.JA;
                else if (LocalizeManager.lang == LANGUAGE_SHIFTRICK.RU) statename = localdata.RU;

                if (LocalizeManager.lang == LANGUAGE_SHIFTRICK.ENG) statename2 = localdata2.ENG;
                else if (LocalizeManager.lang == LANGUAGE_SHIFTRICK.KOR) statename2 = localdata2.KOR;
                else if (LocalizeManager.lang == LANGUAGE_SHIFTRICK.CHN_S) statename2 = localdata2.CHNS;
                else if (LocalizeManager.lang == LANGUAGE_SHIFTRICK.CHN_T) statename2 = localdata2.CHNT;
                else if (LocalizeManager.lang == LANGUAGE_SHIFTRICK.JA) statename2 = localdata2.JA;
                else if (LocalizeManager.lang == LANGUAGE_SHIFTRICK.RU) statename2 = localdata2.RU;

                entranceQuestionText.text = statename + statename2;
            }
            entranceQuestionPanel.SetActive(true);
            StopAllCoroutines();
            endCoroutine = null;
            StartCoroutine(StartQuestionEvent());
            YesBtn.onClick.RemoveAllListeners();
            YesBtn.onClick.AddListener(() => {
                PDG.Player.instance.isCameraOff = false;
                if (isBossRushEntrance)
                {
                    SaveAndLoadManager.instance.DeleteManufacture(1);
                    PDG.Dungeon.instance.newObjectSelectedObj = null;
                    PDG.Dungeon.instance.newObjectSelected = false;
                    PDG.Dungeon.instance.fBossRushTime = 0.0f;
                    GameObject obj = Instantiate(AjitPrefabs);
                    obj.transform.position = new Vector3(22.5f, 22.5f, 0);
                    PDG.Player.instance.transform.position = new Vector3(22.5f, 22.5f, 0);
                    SaveAndLoadManager.instance.SaveItem(new ItemSave { _id = "glowing_skull2", _nCount = 200 }, true);
                    SaveAndLoadManager.instance.DeleteItemBossRush();
                    Inventorys.Instance.glowindskull2 = 200;
                    PDG.Dungeon.instance.isBossRush = true;
                    entranceQuestionPanel.SetActive(false);
                    PDG.Dungeon.instance.StartCoroutine(PDG.Dungeon.instance.BossRush());
                    Destroy(transform.root.gameObject);
                }
                else if (!isBossRushExit)
                {
                    entranceQuestionPanel.SetActive(false);
                    PDG.Dungeon.instance.StateCreate(stateName, entranceQuestionPanel, animObj);
                }
                else
                {
                    entranceQuestionPanel.SetActive(false);
                    PDG.Dungeon.instance.GoAjit();
                }
            });
            NoBtn.onClick.RemoveAllListeners();
            NoBtn.onClick.AddListener(() => {
                PDG.Player.instance.isCameraOff = false;
                StopAllCoroutines();
                endCoroutine = StartCoroutine(EndQuestionEvent());
                SoundManager.instance.Click();
            });
        }
    }
    IEnumerator StartQuestionEvent()
    {
        RectTransform rt = entranceQuestionPanel.GetComponent<RectTransform>();
        float y = 500.0f;
        while (true)
        {
            rt.anchoredPosition3D = new Vector3(0, y, 0);
            y -= Time.deltaTime * moveSpeed;
            if (y <= 50.0f) break;
            yield return new WaitForSeconds(Time.deltaTime);
        }
        y = 50.0f;
        rt.anchoredPosition3D = new Vector3(0, y, 0);
        while (true)
        {
            rt.anchoredPosition3D = new Vector3(0, y, 0);
            y += Time.deltaTime * (moveSpeed / 4);
            if (y >= 70.0f) break;
            yield return new WaitForSeconds(Time.deltaTime);
        }
        y = 70.0f;
        rt.anchoredPosition3D = new Vector3(0, y, 0);
        while (true)
        {
            rt.anchoredPosition3D = new Vector3(0, y, 0);
            y -= Time.deltaTime * (moveSpeed / 4);
            if (y <= 50.0f) break;
            yield return new WaitForSeconds(Time.deltaTime);
        }
        y = 50.0f;
        rt.anchoredPosition3D = new Vector3(0, y, 0);
        while (true)
        {
            rt.anchoredPosition3D = new Vector3(0, y, 0);
            y += Time.deltaTime * (moveSpeed / 4);
            if (y >= 60.0f) break;
            yield return new WaitForSeconds(Time.deltaTime);
        }
        y = 60.0f;
        rt.anchoredPosition3D = new Vector3(0, y, 0);
        while (true)
        {
            rt.anchoredPosition3D = new Vector3(0, y, 0);
            y -= Time.deltaTime * (moveSpeed / 4);
            if (y <= 50.0f) break;
            yield return new WaitForSeconds(Time.deltaTime);
        }
        y = 50.0f;
        rt.anchoredPosition3D = new Vector3(0, y, 0);
        endCoroutine = null;
    }
    IEnumerator EndQuestionEvent()
    {
        RectTransform rt = entranceQuestionPanel.GetComponent<RectTransform>();
        float y = 50.0f;
        rt.anchoredPosition3D = new Vector3(0, y, 0);
        while (true)
        {
            rt.anchoredPosition3D = new Vector3(0, y, 0);
            y += Time.deltaTime * (moveSpeed / 2);
            if (y >= 500.0f) break;
            yield return new WaitForSeconds(Time.deltaTime);
        }
        y = 500.0f;
        rt.anchoredPosition3D = new Vector3(0, y, 0);
        endCoroutine = null;
        entranceQuestionPanel.SetActive(false);
    }
}
