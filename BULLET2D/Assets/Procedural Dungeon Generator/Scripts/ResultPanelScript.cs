﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ResultPanelScript : MonoBehaviour
{
    [Header("Text")]
    [SerializeField] private Text killText;
    [SerializeField] private Text areaText;
    [SerializeField] private Text timeText;
    [SerializeField] private Text goldText;
    [SerializeField] private Text skullText;
    [SerializeField] private Text dieText;
    [SerializeField] private Text elitText;
    [SerializeField] private Text chargeguageText;
    [SerializeField] private Image chargeguageImage;
    [Header("Variable")]
    public int killCount;
    public int skullCount = 0;
    public float chargeGuage = 0;
    public int chargeGuageMax = 100;
    public int Q_SkillCount = 0;
    public string areaName;
    public float time;
    public GameObject openObj;
    public GameObject UIObj;
    public Image[] slot;
    public Image[] matslot;
    public float defeatAmount = 20;
    [SerializeField] private GameObject titleObj;
    [SerializeField] private GameObject[] openObjs;
    [SerializeField] private bool isTestButton = false;
    public bool isReset = false;
    public static ResultPanelScript instance;
    [Header("ItemSelect")]
    public GameObject ItemSelectObj;
    public GameObject ItemSelectSlotItem;
    public Transform ItemSelectTransform;
    List<GameObject> ItemselectList = new List<GameObject>();
    public bool isSelectItem = false;
    public Text selectItemText;
    public GameObject selectPopupObj;
    public ItemInfo nowSelectItem = null;
    public GameObject crossHiarObj;
    [Header("Item Info")]
    public GameObject ItemInfoObj;
    public Image itemInfoImage;
    public Text itemNameText;
    public Text itemContText;
    public List<string> eliteList = new List<string>();
    public List<int> bossList = new List<int>();

    [SerializeField] private Text titleText;
    [SerializeField] private Text titleText2;

    public bool isWin = false;
    [SerializeField] private GameObject[] escObjs;
    [SerializeField] private SetLanguage escLang;

    [SerializeField] private Sprite[] dungeonSprites;
    [SerializeField] private Sprite[] bossSPrites;
    [SerializeField] private RectTransform characterObj;
    [SerializeField] private bool isMute = false;
    [SerializeField] private Image[] bossImages;
    [SerializeField] private Sprite[] bossSimbol;
    [SerializeField] private Transform loadingBarObj;
    [SerializeField] private Sprite failSprite;
    public List<GameObject> stageStepObj = new List<GameObject>();

    private void Awake()
    {
        if (instance == null) instance = this;
    }
    private void Update()
    {
        if(isTestButton)
        {
            isTestButton = false;
            titleObj.GetComponent<RectTransform>().anchoredPosition3D = new Vector3(0, 0, 0);
            openObj.SetActive(false);
            titleObj.SetActive(false);
            ShowResult("죽어", false);
        }

        time += Time.deltaTime;
        if(openObj.activeSelf)
        {
            if (isWin)
            {
                for (int i = 0; i < escObjs.Length; i++) 
                {
                    escObjs[i].SetActive(false);
                }
                escLang._id = "ui_close";
            }
            else
            {
                escLang._id = "ui_restart";
            }
            if ((Input.GetKeyDown(KeyCode.Space)) || PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_CONFIRM], true) && PDG.Dungeon.instance.closeCoolTime > 0.2f)
            {
                PDG.Dungeon.instance.closeCoolTime = 0.0f;
                if(PDG.Dungeon.instance.isBossRush)
                {
                    PDG.Player.curHp = 6;
                    UIObj.SetActive(true);
                    GameObject.Find("Canvas").transform.Find("DiePanel").gameObject.SetActive(false);
                    openObj.SetActive(false);
                    titleObj.GetComponent<RectTransform>().anchoredPosition3D = new Vector3(0, 0, 0);
                    titleObj.SetActive(false);
                    PDG.Dungeon.instance.StageLevel = 0;
                    PDG.Dungeon.isRebuildDevice = false;
                    GetComponent<Image>().enabled = false;
                    GoReStart(0);
                }
                else if(!isWin)
                {
                    PDG.Player.curHp = 6;
                    UIObj.SetActive(true);
                    GameObject.Find("Canvas").transform.Find("DiePanel").gameObject.SetActive(false);
                    openObj.SetActive(false);
                    titleObj.GetComponent<RectTransform>().anchoredPosition3D = new Vector3(0, 0, 0);
                    titleObj.SetActive(false);
                    PDG.Dungeon.instance.StageLevel = 0;
                    PDG.Dungeon.isRebuildDevice = false;
                    GetComponent<Image>().enabled = false;
                    GoReStart(0);
                }
                //else
                //{
                //    Time.timeScale = 1;
                //    Inventorys.Instance.reinforcePoint++;
                //    PDG.Dungeon.instance.isStageBoss = false;
                //    PDG.Dungeon.instance.StageLevel++;
                //    titleObj.GetComponent<RectTransform>().anchoredPosition3D = new Vector3(0, 0, 0);
                //    titleObj.SetActive(false);
                //    openObj.SetActive(false);
                //    GetComponent<Image>().enabled = false;
                //    PDG.Dungeon.instance.StateCreate("", null, null);
                //}
            }
            if ((Input.GetKeyDown(KeyCode.Escape) || Input.GetKeyDown(KeyCode.Backspace) || PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_CANCLE], true)) && PDG.Dungeon.instance.closeCoolTime > 0.2f) 
            {
                PDG.Dungeon.instance.closeCoolTime = 0.0f;
                if (PDG.Dungeon.instance.isBossRush)
                {
                    PDG.Player.curHp = 6;
                    UIObj.SetActive(true);
                    GameObject.Find("Canvas").transform.Find("DiePanel").gameObject.SetActive(false);
                    openObj.SetActive(false);
                    titleObj.GetComponent<RectTransform>().anchoredPosition3D = new Vector3(0, 0, 0);
                    titleObj.SetActive(false);
                    PDG.Dungeon.instance.StageLevel = 0;
                    PDG.Dungeon.isRebuildDevice = false;
                    GetComponent<Image>().enabled = false;
                    GoReStart(0);
                }
                else if (!isWin)
                {
                    UIObj.SetActive(true);
                    GameObject.Find("Canvas").transform.Find("DiePanel").gameObject.SetActive(false);
                    openObj.SetActive(false);
                    titleObj.GetComponent<RectTransform>().anchoredPosition3D = new Vector3(0, 0, 0);
                    titleObj.SetActive(false);
                    GetComponent<Image>().enabled = false;
                    PDG.Dungeon.instance.closeCoolTime = 0.0f;
                    PDG.Dungeon.instance.StageLevel = 0;
                    PDG.Dungeon.isRebuildDevice = false;
                    GoReStart(1);
                }
                else
                {
                
                    UIObj.SetActive(true);
                    GameObject.Find("Canvas").transform.Find("DiePanel").gameObject.SetActive(false);
                    openObj.SetActive(false);
                    titleObj.GetComponent<RectTransform>().anchoredPosition3D = new Vector3(0, 0, 0);
                    titleObj.SetActive(false);
                    GetComponent<Image>().enabled = false;
                    PDG.Dungeon.instance.closeCoolTime = 0.0f;
                    Inventorys.Instance.reinforcePoint++;
                    Time.timeScale = 1;
                    //PDG.Dungeon.instance.isStageBoss = false;
                    //PDG.Dungeon.instance.StageLevel++;
                    //PDG.Dungeon.instance.GoTumblbug();
                }
            }
        }
        if(PDG.Player.instance != null)
        {
            if (Q_SkillCount <= (PDG.Player.instance.Qskill.Equals("spiny_cell_firing_1") ? 1 : 0)) chargeGuage += Time.deltaTime * 20;
        }

        chargeguageImage.fillAmount = (float)chargeGuage / (float)chargeGuageMax;
        if(chargeGuage >= chargeGuageMax)
        {
            chargeGuage -= chargeGuageMax;
            Q_SkillCount++;
        }
        chargeguageText.text = Q_SkillCount.ToString();
    }
    private void GoReStart(int _type)
    {
        int maxLevel = 0;
        for (int ii = 0; ii < 4; ii++)
        {
            ChoiseStateSave saveData = SaveAndLoadManager.instance.LoadChoiseState("ChoiseState" + ii);
            if (saveData._id.Equals("none")) continue;
            if (saveData._type != null)
            {
                StatsData sData = StateUpgradeController.instance.AllList[saveData._type].Find(item => item.Steps == saveData._level);
                if (sData.Sum)
                {
                    for (int j = 0; j < StateUpgradeController.instance.AllList[saveData._type].Count; j++)
                    {
                        StatsData sData2 = StateUpgradeController.instance.AllList[saveData._type][j];
                        if (sData2.Steps > saveData._level) continue;
                        if (sData.ID.Equals("stats_dimention2"))
                        {
                            maxLevel = sData.Steps;
                        }
                        if (sData.ID.Equals("stats_dimention3"))
                        {
                            maxLevel = sData.Steps;
                        }
                    }
                }
            }
        }
        ItemInfo getItem = null;
        if (maxLevel == 2 && isReset)
        {
            getItem = Inventorys.Instance.GetRandomItem();
        }
        else if(maxLevel == 3 && isReset)
        {
            StartCoroutine(ItemSelected(_type));
            return;
        }

        if (isReset)
        {
            Inventorys.Instance.InventoryReset();
            PlayerBuffController.instance.BuffReset(_type);
        }
        isReset = false;

        if(getItem != null)
        {
            if(!getItem.Data.ID.Equals(string.Empty))
            {
                Inventorys.Instance.AddItem(getItem, 1, true);
            }
        }
        Time.timeScale = 1;

        if (isMute)
        {
            SoundManager.isMute = false;
            isMute = false;
        }

        if (_type == 0)
        {
            PDG.Dungeon.curseHealth = 0;
            PDG.Dungeon.curseAttack = 0;
            time = 0.0f;
            PDG.Dungeon.instance.MaterialList.Clear();
            PDG.Dungeon.instance.StageLevel = 0;
            PDG.Dungeon.isRebuildDevice = false;
            PDG.Dungeon.instance.isStageBoss = false;
            PDG.Dungeon.instance.nowStageStep = 0;
            PDG.Dungeon.instance.stageRan = new int[PDG.Dungeon.instance.maxStage];
            PDG.Dungeon.instance.stageBossRan = new int[PDG.Dungeon.instance.maxStage];
            GameObject.Find("Canvas").transform.Find("LoadingPanel").Find("NewLoading").Find("character").GetComponent<RectTransform>().anchoredPosition3D = new Vector3(-770, 75);
            for (int i = 0; i < PDG.Dungeon.instance.stageRan.Length; i++)
            {
                while (true)
                {
                    PDG.Dungeon.instance.stageRan[i] = Random.Range(0, PDG.Dungeon.instance.stageRan.Length + 1);
                    if (i == 0 && PDG.Dungeon.instance.prevStartRan != -1 && PDG.Dungeon.instance.prevStartRan == PDG.Dungeon.instance.stageRan[i])
                    {
                        continue;
                    }
                    if (i == 0) PDG.Dungeon.instance.prevStartRan = PDG.Dungeon.instance.stageRan[i];
                    bool c = false;
                    for (int j = 0; j < i; j++)
                    {
                        if (PDG.Dungeon.instance.stageRan[i] == PDG.Dungeon.instance.stageRan[j]) c = true;
                    }
                    if (!c) break;
                }
            }
            for (int i = 0; i < PDG.Dungeon.instance.stageBossRan.Length; i++)
            {
                while (true)
                {
                    PDG.Dungeon.instance.stageBossRan[i] = Random.Range(0, PDG.Dungeon.instance.stageBossRan.Length);
                    if (i == 0 && PDG.Dungeon.instance.prevStartRanBoss != -1 && PDG.Dungeon.instance.prevStartRanBoss == PDG.Dungeon.instance.stageBossRan[i])
                    {
                        continue;
                    }
                    if (i == 0) PDG.Dungeon.instance.prevStartRanBoss = PDG.Dungeon.instance.stageBossRan[i];
                    bool c = false;
                    for (int j = 0; j < i; j++)
                    {
                        if (PDG.Dungeon.instance.stageBossRan[i] == PDG.Dungeon.instance.stageBossRan[j]) c = true;
                    }
                    if (!c) break;
                }
            }
            for (int i = 0; i < PDG.Dungeon.instance.stageStepObj.Count; i++)
            {
                PDG.Dungeon.instance.stageStepObj[i].GetComponent<Image>().sprite = PDG.Dungeon.instance.stageDefaultSprite;
            }

            if (PDG.Dungeon.instance.isBossRush)
            {
                PDG.Dungeon.instance.Clear();
                PDG.Dungeon.instance.GoAjit2(false);
            }
            else
                PDG.Dungeon.instance.Create();
        }
        else
        {
            PDG.Dungeon.instance.GoAjit();
        }
    }
    IEnumerator ItemSelected(int _type)
    {
        crossHiarObj.SetActive(true);
        for (int i = 0; i < ItemselectList.Count; i++)
            Destroy(ItemselectList[i]);
        ItemselectList.Clear();

        ItemSelectObj.SetActive(true);
        ItemSelectObj.GetComponent<Image>().enabled = true;
        List<ItemInfo> tempList = new List<ItemInfo>();
        for (int i = 0; i < Inventorys.Instance.slots.Length; i++)
        {
            if (Inventorys.Instance.slots[i]._Item != null)
            {
                if (!Inventorys.Instance.slots[i]._Item.Data.ID.Equals(string.Empty))
                {
                    tempList.Add(Inventorys.Instance.slots[i]._Item);
                }
            }
        }
        for (int i = 0; i < tempList.Count; i++)
        {
            GameObject obj = Instantiate(ItemSelectSlotItem, ItemSelectTransform);
            obj.transform.SetParent(ItemSelectTransform);
            ItemInfo info = tempList[i];
            obj.GetComponent<ResultItemSelect>().item = info;
            obj.transform.Find("Icon").GetComponent<Image>().sprite = Resources.Load<Sprite>("Item/" + tempList[i].Data.Type1 + "/" + tempList[i].Data.Type2 + "/" + tempList[i].Data.ID);
            ItemselectList.Add(obj);
        }
        while (!isSelectItem)
        {
            yield return null;
        }

        if (isReset)
        {
            Inventorys.Instance.InventoryReset();
            PlayerBuffController.instance.BuffReset(_type);
        }
        isReset = false;

        if (nowSelectItem != null)
        {
            if (!nowSelectItem.Data.ID.Equals(string.Empty))
            {
                Inventorys.Instance.AddItem(nowSelectItem, 1, true);
            }
        }
        Time.timeScale = 1;
        if (_type == 0)
            PDG.Dungeon.instance.Create();
        else
            PDG.Dungeon.instance.GoAjit();
    }
    public void ClickItem(GameObject _obj, ItemInfo info)
    {
        for (int i = 0; i < ItemselectList.Count; i++)
        {
            if(ItemselectList[i] == _obj)
            {
                ItemselectList[i].transform.Find("Image").gameObject.SetActive(true);
                ItemselectList[i].GetComponent<ResultItemSelect>().isSelect = true;
                ItemselectList[i].GetComponent<ResultItemSelect>().infoObj = ItemInfoObj;
                InfoSetting(info);
            }
            else
            {
                ItemselectList[i].transform.Find("Image").gameObject.SetActive(false);
                ItemselectList[i].GetComponent<ResultItemSelect>().isSelect = false;
            }
        }
    }
    public void InfoSetting(ItemInfo _item)
    {
        ItemInfoObj.SetActive(true);
        Sprite sprite = Resources.Load<Sprite>("Item/" + _item.Data.Type1 + "/" + _item.Data.Type2 + "/" + _item.Data.ID);
        if (sprite == null) itemInfoImage.color = new Color(0, 0, 0, 0);
        else
        {
            itemInfoImage.color = Color.white;
            itemInfoImage.sprite = sprite;
        }

        itemNameText.text = LocalizeManager.GetLocalize(_item.Data.ID);
        itemContText.text = LocalizeManager.GetLocalize(_item.Data.ID + "_cont");
    }
    public void SelectItem(ItemInfo _item)
    {
        string t = LocalizeManager.GetLocalize("result_itemselect");
        t = t.Replace("##", LocalizeManager.GetLocalize(_item.Data.ID));
        selectItemText.text = t;
        nowSelectItem = _item;
        selectPopupObj.SetActive(true);
    }
    public void SelectComplete()
    {
        isSelectItem = true;
        selectPopupObj.SetActive(false);
        crossHiarObj.SetActive(false);
        ItemSelectObj.GetComponent<Image>().enabled = false;
        ItemSelectObj.SetActive(false);
    }
    public void ShowResult(string _whyDie, bool _isWin)
    {
        Image spake = GameObject.Find("Canvas").transform.Find("Spake").GetComponent<Image>();
        spake.color = new Color(1, 1, 1, 0);
        if (!_isWin)
        {
            if (!SaveAndLoadManager.instance.GameDelete()) Debug.LogError("DELETE FAIL");
        }
        if (!SoundManager.isMute)
        {
            SoundManager.isMute = true;
            isMute = true;
        }
        GameObject.Find("Canvas").transform.Find("PlayerTimer").gameObject.SetActive(false);
        GameObject.Find("Canvas").transform.Find("PlayerTimerText").gameObject.SetActive(false);
        GameObject.Find("Canvas").transform.Find("TutorialText").gameObject.SetActive(false);
        GameObject.Find("Canvas").transform.Find("TutorialText2").gameObject.SetActive(false);
        GameObject.Find("Canvas").transform.Find("TutorialText3").gameObject.SetActive(false);
        GameObject.Find("Canvas").transform.Find("TutorialText4").gameObject.SetActive(false);
        GameObject.Find("Canvas").transform.Find("MapGuide").gameObject.SetActive(false);
        GameObject.Find("Canvas").transform.Find("Timer").gameObject.SetActive(false);
        GameObject.Find("Canvas").transform.Find("GrapGuage").gameObject.SetActive(false);

        SoundManager.instance.StopAllSound();
        isWin = _isWin;
        GetComponent<Image>().enabled = true;
        dieText.text = _whyDie;
        killText.text = killCount.ToString();
        skullText.text = skullCount.ToString();
        areaText.text = areaName;
        goldText.text = Inventorys.Instance.goldbar.ToString();
        timeText.text = FloatToTime(Mathf.FloorToInt(time));
        elitText.text = eliteList.Count.ToString();
        for (int i = 0; i < bossImages.Length; i++) bossImages[i].gameObject.SetActive(false);
        for (int i = 0; i < bossList.Count; i++)
        {
            bossImages[i].sprite = bossSimbol[bossList[i]];
            bossImages[i].gameObject.SetActive(true);
        }
        for (int i = 0; i < slot.Length; i++) slot[i].color = new Color(1, 1, 1, 0);
        for (int i = 0; i < matslot.Length; i++) matslot[i].color = new Color(1, 1, 1, 0);
        for (int i = 0; i < Inventorys.Instance.slots.Length; i++)
        {
            if (Inventorys.Instance.slots[i] == null) continue;
            if (Inventorys.Instance.slots[i]._Item == null) continue;
            if (Inventorys.Instance.slots[i]._Item.Data != null)
            {
                if (!Inventorys.Instance.slots[i]._Item.Data.ID.Equals(string.Empty))
                {
                    if (i > slot.Length) break;
                    slot[i].sprite = Inventorys.Instance.slots[i].img.sprite;
                    slot[i].color = new Color(1, 1, 1, 1);
                }
            }
        }
        SettingSafe();


        if (!isWin)
        {
            for(int i=0; i< stageStepObj.Count; i++)
            {
                Destroy(stageStepObj[i]);
            }
            stageStepObj.Clear();

            float t_maxStage = (4 * PDG.Dungeon.instance.stageCount) + (4 * PDG.Dungeon.instance.bossroomCount);
            int t_count = 0;
            for (int total_stage = 0; total_stage < 4; total_stage++)
            {
                for (int i = 0; i < PDG.Dungeon.instance.stageCount; i++)
                {
                    GameObject obj = new GameObject();
                    obj.name = "Stage_" + total_stage + "_normal" + i;
                    Image tImage = obj.AddComponent<Image>();
                    tImage.sprite = PDG.Dungeon.instance.stageDefaultSprite;
                    obj.transform.SetParent(loadingBarObj);
                    obj.transform.localScale = Vector3.one;
                    obj.GetComponent<RectTransform>().anchoredPosition3D = new Vector3(((1300f / t_maxStage) * t_count) - 650f, 0, 0);
                    obj.GetComponent<RectTransform>().sizeDelta = new Vector2(52, 52);
                    stageStepObj.Add(obj);
                    t_count++;

                    GameObject subObj = new GameObject();
                    subObj.name = "StageFail";
                    Image tImage2 = subObj.AddComponent<Image>();
                    tImage2.sprite = failSprite;
                    subObj.transform.SetParent(obj.transform);
                    subObj.transform.localScale = Vector3.one;
                    subObj.transform.GetComponent<RectTransform>().sizeDelta = new Vector2(80, 80);
                    subObj.transform.GetComponent<RectTransform>().anchoredPosition3D = Vector3.zero;
                    subObj.SetActive(false);
                }
                for (int i = 0; i < PDG.Dungeon.instance.bossroomCount; i++)
                {
                    GameObject obj = new GameObject();
                    obj.name = "Stage_" + total_stage + "_boss" + i;
                    Image tImage = obj.AddComponent<Image>();
                    tImage.sprite = PDG.Dungeon.instance.stageDefaultSprite;
                    obj.transform.SetParent(loadingBarObj);
                    obj.transform.localScale = Vector3.one;
                    obj.GetComponent<RectTransform>().anchoredPosition3D = new Vector3(((1300f / t_maxStage) * t_count) - 650f, 0, 0);
                    obj.GetComponent<RectTransform>().sizeDelta = new Vector2(78, 78);
                    stageStepObj.Add(obj);
                    t_count++;

                    GameObject subObj = new GameObject();
                    subObj.name = "StageFail";
                    Image tImage2 = subObj.AddComponent<Image>();
                    tImage2.sprite = failSprite;
                    subObj.transform.SetParent(obj.transform);
                    subObj.transform.localScale = Vector3.one;
                    subObj.transform.GetComponent<RectTransform>().sizeDelta = new Vector2(80, 80);
                    subObj.transform.GetComponent<RectTransform>().anchoredPosition3D = Vector3.zero;
                    subObj.SetActive(false);
                }
            }
            int n = 0;
            for (int i = 0; i < stageStepObj.Count; i++)
            {
                stageStepObj[i].GetComponent<Image>().sprite = PDG.Dungeon.instance.stageDefaultSprite;
            }
            for (int i = 0; i < stageStepObj.Count; i++)
            {
                string tName = stageStepObj[i].name;
                n = (PDG.Dungeon.instance.StageLevel * (PDG.Dungeon.instance.stageCount + 1)) + PDG.Dungeon.instance.nowStageStep;
                if (n == i)
                    stageStepObj[i].transform.GetChild(0).gameObject.SetActive(true);
                else
                    stageStepObj[i].transform.GetChild(0).gameObject.SetActive(false);
                if (i <= n)
                {
                    if (tName.Contains("boss"))
                    {
                        stageStepObj[i].GetComponent<Image>().sprite = bossSPrites[PDG.Dungeon.instance.stageBossRan[i / (PDG.Dungeon.instance.stageCount + 1)]];
                    }
                    else
                    {
                        stageStepObj[i].GetComponent<Image>().sprite = dungeonSprites[PDG.Dungeon.instance.stageRan[i / (PDG.Dungeon.instance.stageCount + 1)]];
                    }
                }
                else
                    break;
            }
        }
        StartCoroutine(ResultViewOpen(_isWin));
    }
    IEnumerator CharacterMove()
    {
        int level = PDG.Dungeon.instance.StageLevel;
        int stageCount = PDG.Dungeon.instance.stageCount;
        int nowStageStep = PDG.Dungeon.instance.nowStageStep;
        int n = (level * (stageCount + 1)) + nowStageStep;
        float x = -725;
        characterObj.anchoredPosition3D = new Vector3(x, -70.0f);
        while (true)
        {
            x += Time.unscaledDeltaTime * 200;
            characterObj.anchoredPosition3D = new Vector3(x, characterObj.anchoredPosition3D.y);
            if (x >= stageStepObj[n].GetComponent<RectTransform>().anchoredPosition3D.x)
            {
                characterObj.anchoredPosition3D = new Vector3(stageStepObj[n].GetComponent<RectTransform>().anchoredPosition3D.x, characterObj.anchoredPosition3D.y);
                break;
            }
            yield return new WaitForSecondsRealtime(Time.unscaledDeltaTime);
        }
    }
    IEnumerator ResultViewOpen(bool _isWin)
    {
        Time.timeScale = 0;
        if (_isWin)
        {
            titleText.color = Color.green;
            titleText2.color = Color.green;
            titleText.text = LocalizeManager.GetLocalize("ui_win");
            titleText2.text = LocalizeManager.GetLocalize("ui_win");
        }
        else
        {
            titleText.color = Color.red;
            titleText2.color = Color.red;
            titleText.text = LocalizeManager.GetLocalize("ui_defeat");
            titleText2.text = LocalizeManager.GetLocalize("ui_defeat");
        }
        UIObj.SetActive(false);
        titleObj.SetActive(true);
        yield return new WaitForSecondsRealtime(2.0f);
        float y = 0.0f;
        while(true)
        {
            titleObj.GetComponent<RectTransform>().anchoredPosition3D = new Vector3(0, y, 0);
            y += defeatAmount;
            if(y >= 400)
            {
                titleObj.GetComponent<RectTransform>().anchoredPosition3D = new Vector3(0, 400, 0);
                break;
            }
            yield return new WaitForSecondsRealtime(Time.unscaledDeltaTime);
        }
        titleObj.SetActive(false);
        openObj.SetActive(true);
        float a = 0.0f;
        while (true)
        {
            openObj.GetComponent<Image>().color = new Color(1, 1, 1, a);
            a += 0.1f;
            if (a >= 1.0f)
            {
                openObj.GetComponent<Image>().color = new Color(1, 1, 1, 1);
                break;
            }
            yield return new WaitForSecondsRealtime(Time.unscaledDeltaTime);
        }
        for (int i = 0; i < openObjs.Length; i++) openObjs[i].SetActive(true);
        StartCoroutine(CharacterMove());
    }
    private string FloatToTime(int _time)
    {
        if (_time == 0) return "0S";

        int day = 0;
        int hour = 0;
        int minute = 0;
        int secend = 0;
        int newTime = _time;

        while (newTime >= 86400)
        {
            newTime -= 86400;
            day++;
        }

        while (newTime >= 3600)
        {
            newTime -= 3600;
            hour++;
        }

        while (newTime >= 60)
        {
            newTime -= 60;
            minute++;
        }

        secend = newTime;

        return (day > 0 ? day + "D " : string.Empty) + (hour > 0 ? hour + "H " : string.Empty) + (minute > 0 ? minute + "M " : string.Empty) + (secend > 0 ? secend + "S" : string.Empty);
    }
    public void SettingSafe()
    {
        try
        {
            int cc = 0;
            for (int i = 0; i < PDG.Dungeon.instance.materialTypes.Length; i++)
            {
                Sprite[] materialSprites = Resources.LoadAll<Sprite>("Item/Material/" + PDG.Dungeon.instance.materialTypes[i]);
                for (int j = 0; j < materialSprites.Length; j++)
                {
                    bool tt = PDG.Dungeon.instance.MaterialList.ContainsKey(PDG.Dungeon.instance.materialTypes[i] + "_" + materialSprites[j].name);
                    if (tt)
                    {
                        Sprite s = Resources.Load<Sprite>("Item/Material/" + PDG.Dungeon.instance.materialTypes[i] + "/" + materialSprites[j].name);
                        matslot[cc].color = new Color32(255, 255, 255, 255);
                        matslot[cc].sprite = s;
                        matslot[cc].transform.Find("Text").GetComponent<Text>().text = PDG.Dungeon.instance.MaterialList[PDG.Dungeon.instance.materialTypes[i] + "_" + materialSprites[j].name].ToString();
                        matslot[cc].transform.localScale = Vector3.one;
                        string n = PDG.Dungeon.instance.materialTypes[i] + "_" + materialSprites[j].name;
                        cc++;
                    }
                    if (cc >= matslot.Length) break;
                }
            }

            if (PDG.Dungeon.instance.isGetWarpPart)
            {
                if (cc >= matslot.Length) return;
                Sprite s = Resources.Load<Sprite>("Item/Material/warp/CaseParts_" + PDG.Dungeon.instance.warpType);
                matslot[cc].color = new Color32(255, 255, 255, 255);
                matslot[cc].sprite = s;
                matslot[cc].transform.Find("Text").GetComponent<Text>().text = "1";
                matslot[cc].transform.localScale = Vector3.one;
                cc++;
            }

            for (int i = cc; i < matslot.Length; i++)
            {
                matslot[i].color = new Color32(255, 255, 255, 0);
                matslot[i].transform.Find("Text").GetComponent<Text>().text = string.Empty;
            }
        }
        catch(System.Exception e)
        {
            TextOpenController.instance.ShowText(e.Message, Color.white, 24, PDG.Player.instance.gameObject, 5.0f);
        }
    }
}
