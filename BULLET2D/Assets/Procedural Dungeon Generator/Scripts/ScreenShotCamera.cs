﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenShotCamera : MonoBehaviour
{
    public GameObject target;
    private void Update()
    {
        if (target == null) return;
        Vector3 pos = target.transform.position;
        pos.z = -15;
        transform.position = pos;
    }
}
