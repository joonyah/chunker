﻿using PDG;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwordThrow : MonoBehaviour
{
    public bool isRotate = false;
    public float destroyDistance = 10.0f;
    public float _angle;
    public GameObject ownerObj;
    public string nowWeaponName;
    [SerializeField] private float rotateSpeed = 2.0f;
    private float plusAngle = 0.0f;
    private int bounsCount = 0;
    float bounsDelayTime = 0.15f;
    private void OnEnable()
    {
        bounsCount = 0;
    }
    void Update()
    {
        if (bounsDelayTime > 0)
        {
            bounsDelayTime -= Time.deltaTime;
            return;
        }

        if (ownerObj)
        {
            if (Vector3.Distance(ownerObj.transform.position, transform.position) > destroyDistance)
            {
                DestroyEffect(null);
            }
        }

        if(isRotate)
        {
            plusAngle += Time.deltaTime * rotateSpeed;
            transform.localEulerAngles = new Vector3(0, 0, _angle + plusAngle);
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (bounsDelayTime > 0)
        {
            return;
        }
        if (collision.gameObject.layer.Equals(23))
        {
            if (collision.transform.parent && collision.transform.parent.gameObject.GetComponent<TentacleController>()) collision.transform.parent.gameObject.GetComponent<TentacleController>().SetDamage(1);
            if (collision.gameObject.GetComponent<ChestHit>())
            {
                collision.gameObject.GetComponent<ChestHit>().SetDamage(1);
                DestroyEffect(collision);
            }
            else if (collision.gameObject.GetComponent<DrumHit>())
            {
                collision.gameObject.GetComponent<DrumHit>().SetDamage(1);
                DestroyEffect(collision);
            }
        }
        if (collision.gameObject.layer.Equals(12))
        {
            Vector3 dir2 = collision.gameObject.transform.position - PDG.Player.instance.transform.position;
            dir2.Normalize();
            if (collision.gameObject.GetComponent<MOB.Monster>()) collision.gameObject.GetComponent<MOB.Monster>().SetDamage((Player.instance.isLeftNear ? Player.instance.damageNear : Player.instance.damage), -dir2, 2, Player.instance.left_elemetanl, Player.instance.left_elemnetalTime);
            if (Player.instance.isStrategic_02) Player.instance.isStrategic_02 = false;
            if (PDG.Player.instance.isLeftNear && PDG.Player.instance.isCritical)
            {
                GameObject criEffect = ObjManager.Call().GetObject("CriticalHitEffect");
                criEffect.transform.position = collision.transform.position;
                criEffect.SetActive(true);
            }
            DestroyEffect(collision);
        }
        if (collision.gameObject.layer.Equals(24))
        {
            if (collision.gameObject.GetComponent<MOB.BossMonster_Six_Apear>()) collision.gameObject.GetComponent<MOB.BossMonster_Six_Apear>().SetDamage(PDG.Player.instance.damage);
            if (collision.gameObject.GetComponent<Boss_STG>()) collision.gameObject.GetComponent<Boss_STG>().SetDamage(Player.instance.isLeftNear ? Player.instance.damageNear : Player.instance.damage, collision.transform.position);
            if (collision.gameObject.GetComponent<Boss_NPC>()) collision.gameObject.GetComponent<Boss_NPC>().SetDamage(Player.instance.isLeftNear ? Player.instance.damageNear : Player.instance.damage, collision.transform.position);
            if (collision.gameObject.GetComponent<Boss_Golem>()) collision.gameObject.GetComponent<Boss_Golem>().SetDamage(Player.instance.isLeftNear ? Player.instance.damageNear : Player.instance.damage, collision.transform.position);
            if (collision.gameObject.GetComponent<Boss_Stone>()) collision.gameObject.GetComponent<Boss_Stone>().SetDamage(Player.instance.isLeftNear ? Player.instance.damageNear : Player.instance.damage, collision.transform.position, false);
            if (Player.instance.isStrategic_02) Player.instance.isStrategic_02 = false;
            if (PDG.Player.instance.isLeftNear && PDG.Player.instance.isCritical)
            {
                GameObject criEffect = ObjManager.Call().GetObject("CriticalHitEffect");
                criEffect.transform.position = collision.transform.position;
                criEffect.SetActive(true);
            }
            DestroyEffect(collision);
        }
        if (collision.gameObject.layer.Equals(8))
        {
            DestroyEffect(collision);
        }
    }
    public void DestroyEffect(Collider2D coll)
    {
        GameObject obj = ObjManager.Call().GetObject("SwordForceDestroy");
        obj.transform.position = transform.position;
        obj.transform.localEulerAngles = new Vector3(0, 0, _angle);
        obj.GetComponent<SwordEffect>()._weaponName = nowWeaponName;
        obj.GetComponent<Animator>().Rebind();
        obj.GetComponent<SwordEffect>().isAttack = false;
        obj.SetActive(true);
        if (Player.instance.isBulletBounce && coll != null)
        {
            if (bounsCount >= PDG.Player.instance.BounsCount) gameObject.SetActive(false);
            bounsDelayTime = 0.15f;
            Vector3 dir = coll.transform.position - transform.position;
            dir.Normalize();
            float angle = GetReflectAngle(coll.transform.position - dir, coll.transform.position, dir);
            GetComponent<Rigidbody2D>().velocity = Vector2.zero;
            dir = GetPosition(transform.position, angle) - transform.position;
            dir.Normalize();
            GetComponent<Rigidbody2D>().AddForce(dir * Player.instance.forcePow, ForceMode2D.Impulse);
            transform.localEulerAngles = new Vector3(0, 0, angle);
            bounsCount++;
        }
        else
            gameObject.SetActive(false);
    }
    #region -- Tool --
    float GetAngle(Vector3 vStart, Vector3 vEnd)
    {
        Vector3 v = vEnd - vStart;

        return Mathf.Atan2(v.y, v.x) * Mathf.Rad2Deg;
    }
    Vector3 GetPosition(Vector3 vStart, float angle)
    {
        float x = Mathf.Cos(Mathf.Deg2Rad * angle) + vStart.x;
        float y = Mathf.Sin(Mathf.Deg2Rad * angle) + vStart.y;

        return new Vector3(x, y, 0);
    }
    float GetReflectAngle(Vector3 _hitPoint, Vector3 _hitPosition, Vector3 _dir)
    {
        Vector3 _dir2 = _hitPoint - _hitPosition;
        _dir2.Normalize();

        if (_dir.x == 0 && _dir.y > 0)       // 위 
        {
            return 270.0f;
        }
        else if (_dir.x == 0 && _dir.y < 0)  // 아래
        {
            return 90.0f;
        }
        else if (_dir.x > 0 && _dir.y == 0)  // 오른쪽
        {
            return 180.0f;
        }
        else if (_dir.x < 0 && _dir.y == 0)  // 왼쪽
        {
            return 0.0f;
        }
        else if (_dir.x > 0 && _dir.y > 0)  // 오른쪽 위 
        {
            if (_dir2.x > 0)
            {
                float zz = GetAngle(_hitPoint, transform.position);
                float f = zz - 180.0f; // 입사각
                return 360.0f - f; // 반사각
            }
            else if (_dir2.y > 0)
            {
                float zz = GetAngle(_hitPoint, transform.position);
                float f = 270.0f - zz; // 입사각
                return 90.0f + f; // 반사각
            }
            else
            {
                float hitAngle = GetAngle(_hitPosition, _hitPoint);
                if (hitAngle > 225.0f)
                {
                    float zz = GetAngle(_hitPoint, transform.position);
                    float f = zz - 180.0f; // 입사각
                    return 360.0f - f; // 반사각
                }
                else if (hitAngle < 225.0f)
                {
                    float zz = GetAngle(_hitPoint, transform.position);
                    float f = 270.0f - zz; // 입사각
                    return 90.0f + f; // 반사각
                }
                else
                {
                    return 225.0f;
                }

            }
        }
        else if (_dir.x < 0 && _dir.y > 0)   // 왼쪽 위
        {
            if (_dir2.x < 0)
            {
                float zz = GetAngle(_hitPoint, transform.position);
                float f = 360.0f - zz; // 입사각
                return 180.0f + f; // 반사각
            }
            else if (_dir2.y > 0)
            {
                float zz = GetAngle(_hitPoint, transform.position);
                float f = zz - 270.0f; // 입사각
                return f; // 반사각
            }
            else
            {
                float hitAngle = GetAngle(_hitPosition, _hitPoint);
                if (hitAngle > 315.0f)
                {
                    float zz = GetAngle(_hitPoint, transform.position);
                    float f = zz - 270.0f; // 입사각
                    return f; // 반사각
                }
                else if (hitAngle < 315.0f)
                {
                    float zz = GetAngle(_hitPoint, transform.position);
                    float f = 360.0f - zz; // 입사각
                    return 180.0f + f; // 반사각
                }
                else
                {
                    return 315.0f;
                }

            }
        }
        else if (_dir.x < 0 && _dir.y < 0)   // 왼쪽 아래
        {
            if (_dir2.x < 0)
            {
                float zz = GetAngle(_hitPoint, transform.position);
                float f = zz; // 입사각
                return 180.0f - f; // 반사각
            }
            else if (_dir2.y < 0)
            {
                float zz = GetAngle(_hitPoint, transform.position);
                float f = 90.0f - zz; // 입사각
                return 270.0f + f; // 반사각
            }
            else
            {
                float hitAngle = GetAngle(_hitPosition, _hitPoint);
                if (hitAngle > 45.0f)
                {
                    float zz = GetAngle(_hitPoint, transform.position);
                    float f = zz; // 입사각
                    return 180.0f - f; // 반사각
                }
                else if (hitAngle < 45.0f)
                {
                    float zz = GetAngle(_hitPoint, transform.position);
                    float f = 90.0f - zz; // 입사각
                    return 270.0f + f; // 반사각
                }
                else
                {
                    return 45.0f;
                }

            }
        }
        else if (_dir.x > 0 && _dir.y < 0)   // 오른쪽 아래
        {
            if (_dir2.x > 0)
            {
                float zz = GetAngle(_hitPoint, transform.position);
                float f = 180.0f - zz; // 입사각
                return f; // 반사각
            }
            else if (_dir2.y < 0)
            {
                float zz = GetAngle(_hitPoint, transform.position);
                float f = zz - 90.0f; // 입사각
                return 270.0f - f; // 반사각
            }
            else
            {
                float hitAngle = GetAngle(_hitPosition, _hitPoint);
                if (hitAngle > 135.0f)
                {
                    float zz = GetAngle(_hitPoint, transform.position);
                    float f = zz - 90.0f; // 입사각
                    return 270.0f - f; // 반사각
                }
                else if (hitAngle < 135.0f)
                {
                    float zz = GetAngle(_hitPoint, transform.position);
                    float f = 180.0f - zz; // 입사각
                    return f; // 반사각
                }
                else
                {
                    return 135.0f;
                }
            }
        }
        else
        {
            return 0.0f;
        }
    }
    #endregion
}
