﻿using MOB;
using PDG;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwingAttackController : MonoBehaviour
{
    [SerializeField] private float hitTime = 0.25f;
    [SerializeField] private List<GameObject> objList = new List<GameObject>();
    [SerializeField] private List<float> timeList = new List<float>();
    [SerializeField] private GameObject hitEffect;
    [SerializeField] private GameObject criticalHitEffect;
    private void Update()
    {
        for (int i = 0; i < objList.Count; i++)
        {
            if (objList[i] == null)
            {
                objList.RemoveAt(i);
                timeList.RemoveAt(i);
                i--;
                continue;
            }
            else if (objList[i].GetComponent<MOB.MonsterArggro>())
            {
                if (objList[i].GetComponent<MOB.MonsterArggro>()._monster.isDie)
                {
                    objList.RemoveAt(i);
                    timeList.RemoveAt(i);
                    i--;
                    continue;
                }
            }
            else if (objList[i].GetComponent<MOB.Monster>())
            {
                if (objList[i].GetComponent<MOB.Monster>().isDie)
                {
                    objList.RemoveAt(i);
                    timeList.RemoveAt(i);
                    i--;
                    continue;
                }
            }
            timeList[i] += Time.deltaTime;
        }
    }
    private void OnTriggerStay2D(Collider2D collision)
    {
        int index = objList.FindIndex(item => item.Equals(collision.gameObject));
        if (index != -1)
        {
            if (hitTime > timeList[index]) return;
            else timeList[index] = 0.0f;
        }
        else
        {
            objList.Add(collision.gameObject);
            timeList.Add(0.0f);
        }
        if (collision.gameObject.layer.Equals(23))
        {
            if (collision.transform.parent && collision.transform.parent.GetComponent<TentacleController>()) collision.transform.parent.GetComponent<TentacleController>().SetDamage(1);
            if (collision.gameObject.GetComponent<ChestHit>())
            {
                collision.gameObject.GetComponent<ChestHit>().SetDamage(1);
                GameObject obj = ObjManager.Call().GetObject("Sword_Hit");
                obj.transform.position = collision.transform.position;
                obj.GetComponent<Animator>().Rebind();
                obj.SetActive(true);
                {
                    int ran = Random.Range(0, 3);
                    if (ran == 0)
                    {
                        GameObject obj2 = ObjManager.Call().GetObject("Hit_sprite4");
                        obj2.transform.position = collision.transform.position;
                        obj2.GetComponent<SwordHitEffectController>().animSpeed = 1.0f;
                        obj2.GetComponent<SwordHitEffectController>().isPenetrate = false;
                        obj2.GetComponent<SwordHitEffectController>().isAttack = false;
                        obj2.transform.localScale = Vector3.one;
                        obj2.transform.localEulerAngles = Vector2.zero;
                        obj2.GetComponent<Animator>().Rebind();
                        obj2.SetActive(true);
                    }
                    else if (ran == 1)
                    {
                        GameObject obj2 = ObjManager.Call().GetObject("Sword_Hit_1");
                        if (obj2 != null)
                        {
                            obj2.transform.position = collision.transform.position;
                            obj2.GetComponent<Animator>().Rebind();
                            obj2.SetActive(true);
                        }
                    }
                    else
                    {
                        GameObject obj2 = ObjManager.Call().GetObject("Sword_Hit_2");
                        if (obj2 != null)
                        {
                            obj2.transform.position = collision.transform.position;
                            obj2.GetComponent<Animator>().Rebind();
                            obj2.SetActive(true);
                        }
                    }
                }

                if (hitEffect)
                {
                    GameObject hitObj = Instantiate(hitEffect);
                    hitObj.transform.position = collision.transform.position;
                    Destroy(hitObj, 2f);
                }
            }
            else if (collision.gameObject.GetComponent<DrumHit>())
            {
                collision.gameObject.GetComponent<DrumHit>().SetDamage(1);
                GameObject obj = ObjManager.Call().GetObject("Sword_Hit");
                obj.transform.position = collision.transform.position;
                obj.GetComponent<Animator>().Rebind();
                obj.SetActive(true);
                {
                    int ran = Random.Range(0, 3);
                    if (ran == 0)
                    {
                        GameObject obj2 = ObjManager.Call().GetObject("Hit_sprite4");
                        obj2.transform.position = collision.transform.position;
                        obj2.GetComponent<SwordHitEffectController>().animSpeed = 1.0f;
                        obj2.GetComponent<SwordHitEffectController>().isPenetrate = false;
                        obj2.GetComponent<SwordHitEffectController>().isAttack = false;
                        obj2.transform.localScale = Vector3.one;
                        obj2.transform.localEulerAngles = Vector2.zero;
                        obj2.GetComponent<Animator>().Rebind();
                        obj2.SetActive(true);
                    }
                    else if (ran == 1)
                    {
                        GameObject obj2 = ObjManager.Call().GetObject("Sword_Hit_1");
                        if (obj2 != null)
                        {
                            obj2.transform.position = collision.transform.position;
                            obj2.GetComponent<Animator>().Rebind();
                            obj2.SetActive(true);
                        }
                    }
                    else
                    {
                        GameObject obj2 = ObjManager.Call().GetObject("Sword_Hit_2");
                        if (obj2 != null)
                        {
                            obj2.transform.position = collision.transform.position;
                            obj2.GetComponent<Animator>().Rebind();
                            obj2.SetActive(true);
                        }
                    }
                }

                if (hitEffect)
                {
                    GameObject hitObj = Instantiate(hitEffect);
                    hitObj.transform.position = collision.transform.position;
                    Destroy(hitObj, 2f);
                }
            }
            else if (collision.gameObject.GetComponent<DestroyObjects>())
            {
                collision.gameObject.GetComponent<DestroyObjects>().DestroyObject((collision.gameObject.transform.position - PDG.Player.instance.transform.position).normalized);
                GameObject obj = ObjManager.Call().GetObject("Sword_Hit");
                obj.transform.position = collision.transform.position;
                obj.GetComponent<Animator>().Rebind();
                obj.SetActive(true);
                {
                    int ran = Random.Range(0, 3);
                    if (ran == 0)
                    {
                        GameObject obj2 = ObjManager.Call().GetObject("Hit_sprite4");
                        obj2.transform.position = collision.transform.position;
                        obj2.GetComponent<SwordHitEffectController>().animSpeed = 1.0f;
                        obj2.GetComponent<SwordHitEffectController>().isPenetrate = false;
                        obj2.GetComponent<SwordHitEffectController>().isAttack = false;
                        obj2.transform.localScale = Vector3.one;
                        obj2.transform.localEulerAngles = Vector2.zero;
                        obj2.GetComponent<Animator>().Rebind();
                        obj2.SetActive(true);
                    }
                    else if (ran == 1)
                    {
                        GameObject obj2 = ObjManager.Call().GetObject("Sword_Hit_1");
                        if (obj2 != null)
                        {
                            obj2.transform.position = collision.transform.position;
                            obj2.GetComponent<Animator>().Rebind();
                            obj2.SetActive(true);
                        }
                    }
                    else
                    {
                        GameObject obj2 = ObjManager.Call().GetObject("Sword_Hit_2");
                        if (obj2 != null)
                        {
                            obj2.transform.position = collision.transform.position;
                            obj2.GetComponent<Animator>().Rebind();
                            obj2.SetActive(true);
                        }
                    }
                }

                if (hitEffect)
                {
                    GameObject hitObj = Instantiate(hitEffect);
                    hitObj.transform.position = collision.transform.position;
                    Destroy(hitObj, 2f);
                }
            }
        }
        if (collision.gameObject.layer.Equals(24))
        {
            if (collision.gameObject.GetComponent<MOB.BossMonster_Six_Apear>()) collision.gameObject.GetComponent<MOB.BossMonster_Six_Apear>().SetDamage(PDG.Player.instance.damageNear);
            if (collision.gameObject.GetComponent<Boss_STG>()) collision.gameObject.GetComponent<Boss_STG>().SetDamage(Player.instance.isLeftNear ? Player.instance.damageNear : Player.instance.damage, collision.transform.position);
            if (collision.gameObject.GetComponent<Boss_NPC>()) collision.gameObject.GetComponent<Boss_NPC>().SetDamage(Player.instance.isLeftNear ? Player.instance.damageNear : Player.instance.damage, collision.transform.position);
            if (collision.gameObject.GetComponent<Boss_Golem>()) collision.gameObject.GetComponent<Boss_Golem>().SetDamage(Player.instance.isLeftNear ? Player.instance.damageNear : Player.instance.damage, collision.transform.position);
            if (collision.gameObject.GetComponent<Boss_Stone>()) collision.gameObject.GetComponent<Boss_Stone>().SetDamage(Player.instance.isLeftNear ? Player.instance.damageNear : Player.instance.damage, collision.transform.position, false);
            if (Player.instance.isStrategic_02) Player.instance.isStrategic_02 = false;
            GameObject obj = ObjManager.Call().GetObject("Sword_Hit");
            obj.transform.position = collision.transform.position;
            obj.GetComponent<Animator>().Rebind();
            obj.SetActive(true);
            {
                int ran = Random.Range(0, 3);
                if (ran == 0)
                {
                    if (collision != null)
                    {
                        GameObject obj2 = ObjManager.Call().GetObject("Hit_sprite4");
                        obj2.transform.position = collision.transform.position;
                        obj2.GetComponent<SwordHitEffectController>().animSpeed = 1.0f;
                        obj2.GetComponent<SwordHitEffectController>().isPenetrate = false;
                        obj2.GetComponent<SwordHitEffectController>().isAttack = false;
                        obj2.transform.localScale = Vector3.one;
                        obj2.transform.localEulerAngles = Vector2.zero;
                        obj2.GetComponent<Animator>().Rebind();
                        obj2.SetActive(true);
                    }
                }
                else if (ran == 1)
                {
                    if (collision != null)
                    {
                        GameObject obj2 = ObjManager.Call().GetObject("Sword_Hit_1");
                        if (obj2 != null)
                        {
                            obj2.transform.position = collision.transform.position;
                            obj2.GetComponent<Animator>().Rebind();
                            obj2.SetActive(true);
                        }
                    }
                }
                else
                {
                    if (collision != null)
                    {
                        GameObject obj2 = ObjManager.Call().GetObject("Sword_Hit_2");
                        if (obj2 != null)
                        {
                            obj2.transform.position = collision.transform.position;
                            obj2.GetComponent<Animator>().Rebind();
                            obj2.SetActive(true);
                        }
                    }
                }
            }

            if (hitEffect && !Player.instance.isCritical)
            {
                GameObject hitObj = Instantiate(hitEffect);
                hitObj.transform.position = collision.transform.position;
                Destroy(hitObj, 2f);
            }

            if (PDG.Player.instance.isLeftNear && PDG.Player.instance.isCritical)
            {
                GameObject criEffect = ObjManager.Call().GetObject("CriticalHitEffect");
                criEffect.transform.position = collision.transform.position;
                criEffect.SetActive(true);
            }
        }
        if (collision.gameObject.layer.Equals(12))
        {
            Vector3 dir2 = collision.gameObject.transform.position - PDG.Player.instance.transform.position;
            dir2.Normalize();

            collision.gameObject.GetComponent<MOB.Monster>().SetDamage(PDG.Player.instance.isLeftNear ? PDG.Player.instance.damageNear : PDG.Player.instance.damage, -dir2, 2, Player.instance.left_elemetanl, Player.instance.left_elemnetalTime);
            if (Player.instance.isStrategic_02) Player.instance.isStrategic_02 = false;
            GameObject obj = ObjManager.Call().GetObject("Sword_Hit");
            obj.transform.position = collision.transform.position;
            obj.GetComponent<Animator>().Rebind();
            obj.SetActive(true);
            {
                int ran = Random.Range(0, 3);
                if (ran == 0)
                {
                    GameObject obj2 = ObjManager.Call().GetObject("Hit_sprite4");
                    obj2.transform.position = collision.transform.position;
                    obj2.transform.localScale = Vector3.one;
                    obj2.transform.localEulerAngles = Vector2.zero;
                    obj2.GetComponent<SwordHitEffectController>()._effctName = "Hit_" + ran;
                    obj2.GetComponent<SwordHitEffectController>().animSpeed = 1.0f;
                    obj2.GetComponent<SwordHitEffectController>().isPenetrate = false;
                    obj2.GetComponent<SwordHitEffectController>().isAttack = false;
                    obj2.GetComponent<Animator>().Rebind();
                    obj2.SetActive(true);
                }
                else if (ran == 1)
                {
                    GameObject obj2 = ObjManager.Call().GetObject("Hit_sprite4");
                    obj2.transform.position = collision.transform.position;
                    obj2.transform.localScale = Vector3.one;
                    obj2.transform.localEulerAngles = Vector2.zero;
                    obj2.GetComponent<SwordHitEffectController>()._effctName = "Hit_" + ran;
                    obj2.GetComponent<SwordHitEffectController>().animSpeed = 1.0f;
                    obj2.GetComponent<SwordHitEffectController>().isPenetrate = false;
                    obj2.GetComponent<SwordHitEffectController>().isAttack = false;
                    obj2.GetComponent<Animator>().Rebind();
                    obj2.SetActive(true);
                }
                else
                {
                    GameObject obj2 = ObjManager.Call().GetObject("Hit_sprite4");
                    obj2.transform.position = collision.transform.position;
                    obj2.transform.localScale = Vector3.one;
                    obj2.transform.localEulerAngles = Vector2.zero;
                    obj2.GetComponent<SwordHitEffectController>()._effctName = "Hit_" + ran;
                    obj2.GetComponent<SwordHitEffectController>().animSpeed = 1.0f;
                    obj2.GetComponent<SwordHitEffectController>().isPenetrate = false;
                    obj2.GetComponent<SwordHitEffectController>().isAttack = false;
                    obj2.GetComponent<Animator>().Rebind();
                    obj2.SetActive(true);
                }
            }

            if (hitEffect && !Player.instance.isCritical)
            {
                GameObject hitObj = Instantiate(hitEffect);
                hitObj.transform.position = collision.transform.position;
                Destroy(hitObj, 2f);
            }

            if (PDG.Player.instance.isLeftNear && PDG.Player.instance.isCritical)
            {
                GameObject criEffect = ObjManager.Call().GetObject("CriticalHitEffect");
                criEffect.transform.position = collision.transform.position;
                criEffect.SetActive(true);
            }
        }
    }
}
