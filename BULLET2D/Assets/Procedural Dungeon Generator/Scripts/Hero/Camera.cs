﻿using UnityEngine;

namespace PDG
{
	public class Camera : MonoBehaviour
	{
		public float smoothTime = 0.3F;
		public float zOffSet = -5f;

        public Transform target;

		private Vector2 velocity = Vector2.zero;
        private Vector2 weaponDir = Vector2.zero;
        private Hero _hero;
        public float CameraMoveAmount = 2.0f;
        public float moveDis = 15.5f;
        public int bossIndex = -1;
        public bool isTestScreenClear = false;
        private void Update()
		{
			if (target == null) return;
            if (_hero == null) _hero = target.GetComponent<Hero>();
            if(isTestScreenClear)
            {
                isTestScreenClear = false;
                ScreenClear();
            }
            Vector2 targetPosition = target.TransformPoint(new Vector2());
            if (_hero != null)
            {
                if (_hero.isWeaponMode && !Player.instance.isCameraOff && !Dungeon.instance.isJoystrick)
                {
                    Vector3 crossPos = _hero.WeaponsCrossHair.transform.position;
                    crossPos.z = zOffSet;
                    crossPos = UnityEngine.Camera.main.ScreenToWorldPoint(crossPos);

                    float dis = Vector3.Distance(transform.position, crossPos);

                    if (dis > moveDis)
                    {
                        weaponDir = crossPos - transform.position;
                        weaponDir.Normalize();
                    }
                    else weaponDir = Vector2.zero;
                }
                else weaponDir = Vector2.zero;
            }
            else weaponDir = Vector2.zero;
            targetPosition.y += 0.5f;
            targetPosition += (weaponDir * CameraMoveAmount);
            transform.position = Vector2.SmoothDamp(transform.position, targetPosition, ref velocity, smoothTime);
			transform.position = new Vector3(transform.position.x, transform.position.y, zOffSet);


            if (Dungeon.instance.nowRoomType.Equals("boss") && !Dungeon.instance.isBossRush)
            {
                if (bossIndex == -1) bossIndex = Dungeon.instance.RoomInfo.FindIndex(item => item.rooMType.Equals("bossstage"));
                else
                {
                    Roominfos info = Dungeon.instance.RoomInfo[bossIndex];
                    int x = info.x;
                    int y = info.y;
                    int w = info.width;
                    int h = info.height;

                    x = x + (w / 2);
                    y = y + (h / 2);
                    // x축 16
                    // y축 9.5

                    Vector3 newPos = transform.position;
                    float minY = 0.0f;
                    float maxY = 0.0f;
                    int nowBoss = Dungeon.instance.stageBossRan[Dungeon.instance.isStageBoss ? Dungeon.instance.StageLevel : (Dungeon.instance.StageLevel > 0 ? Dungeon.instance.StageLevel - 1 : Dungeon.instance.StageLevel)];
                    if (nowBoss == 0)
                    {
                        minY = 10.0f;
                        maxY = 9.5f;
                    }
                    if (nowBoss == 1)
                    {
                        minY = 1;
                        maxY = 3.5f;
                    }
                    if (nowBoss == 2)
                    {
                        minY = 12;
                        maxY = 12.5f;
                    }
                    if (nowBoss == 3)
                    {
                        minY = 13;
                        maxY = 12.5f;
                    }
                    if (newPos.y > (y + maxY)) newPos.y = y + maxY;
                    if (newPos.y < (y - minY)) newPos.y = y - minY;
                    transform.position = newPos;
                }
            }
            else if (transform.position.x < 0 && !Dungeon.instance.isBossRush)
            {
                Vector3 newPos = transform.position;
                if (newPos.y >= -8) newPos.y = -8.0f;
                else if (newPos.y <= -37.0f) newPos.y = -37.0f;
                transform.position = newPos;
            }
        }
        public void ScreenClear()
        {
            UbhObjectPool.instance.ReleaseAllBullet();
            RaycastHit2D[] hits = Physics2D.BoxCastAll(transform.position, new Vector2(40, 40), 0, Vector2.zero, 0, 1 << 13 | 1 << 17 | 1 << 25 | 1 << 12);
            for (int i = 0; i < hits.Length; i++)
            {
                if(hits[i].collider.transform.parent)
                {
                    if (hits[i].collider.transform.root.gameObject.layer.Equals(9)) continue;
                }
                if(hits[i].collider.gameObject.layer.Equals(12))
                {
                    if (hits[i].collider.gameObject.name.Contains("bossSpores") || hits[i].collider.gameObject.name.Contains("IceBullet"))
                        Destroy(hits[i].collider.gameObject);
                    else
                        continue;
                }
                if(hits[i].collider.gameObject.name.Contains("Clone"))
                {
                    Destroy(hits[i].collider.gameObject);
                }
                else if(hits[i].collider.transform.parent && hits[i].collider.transform.parent.name.Equals("ObjectManager"))
                {
                    hits[i].collider.gameObject.SetActive(false);
                }
                else
                {
                    Destroy(hits[i].collider.gameObject);
                }
            }
        }
	}
}