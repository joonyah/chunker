﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwordSubController : MonoBehaviour
{
    [SerializeField] private Sprite[] sprites;
    public SpriteRenderer sr;
    public SwordController sc;
    public void SwordEnd()
    {
        sr.sprite = System.Array.Find(sprites, item => item.name.Equals("giant_blade_weapon_" + sc.AttackType));
        if(transform.Find("GameObject"))
        {
            transform.Find("GameObject").gameObject.SetActive(false);
        }
    }

    public void SwordScaleChange(int t)
    {
        if(sr.flipY)
        {
            transform.Find("GameObject").localScale = new Vector3(-1, -1, 1);
        }
        else
        {
            if(t == 0)
                transform.Find("GameObject").localScale = new Vector3(-1, 1, 1);
            else
                transform.Find("GameObject").localScale = new Vector3(1, -1, 1);
        }
    }
}
