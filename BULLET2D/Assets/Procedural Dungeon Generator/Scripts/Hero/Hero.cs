﻿using MoreMountains.FeedbacksForThirdParty;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace PDG
{
    public enum Direction
    {
        None = 0, Up = 1, Down = 2, Right = 3, Left = 4, RightUp = 5, LeftUp = 6, RightDown = 7, LeftDown = 8
    }

    public class Hero : MonoBehaviour
    {
        public float speed = 3f;
        public float bonusSpeed = 1.2f;
        public LayerMask obstacles;
        bool isHole = false;
        [HideInInspector] public Animator animator;
        //[HideInInspector] public Vector2 targetPosition;
        private Collider2D _coll;

        //public Direction direction = Direction.None;
        //public Direction directionWeapons = Direction.None;

        private bool isMoveChange = false;

        [Header("Avoid Variables")]
        [HideInInspector] public float avoidTime = 1.0f;
        [HideInInspector] public float avoidTimeNow = 1.0f;
        public static float avoidSpeed = 1.0f;
        public float avoidSpeedPlus = 1.5f;
        [Header("Weapons")]
        public bool isWeaponMode = false;
        public GameObject WeaponsSlot;
        public Transform WeaponsFireTransform;
        public GameObject WeaponsCrossHair;
        [SerializeField] private float angle = 0.0f;
        public float zOffSet;

        [Header("Effect")]
        [SerializeField] private float moveEffectTime = 3.0f;
        [SerializeField] private float moveEffectTimeNow = 0.0f;
        [SerializeField] private float moveEffectTimeDelay = 1.0f;
        [SerializeField] private float moveEffectTimeDelayNow = 1.0f;
        [SerializeField] private Vector2[] RunEffectPositions;
        [SerializeField] private Vector2 RunEffectPositionNow;
        [SerializeField] private GameObject RunEffectObj;
        [SerializeField] private ParticleSystem Footsteps;

        [Header("Teleport")]
        public static bool isTeleport = false;
        [SerializeField] private ParticleSystem DustCircle;
        [SerializeField] private float teleportSpeed = 4.0f;
        [SerializeField] private float teleportEffectDelay = 0.1f;
        public float teleportEffectDestroyTime = 0.2f;
        public bool isTeleportUse = false;
        public bool isBack = false;
        public float backTime = 0.1f;
        public GameObject hitEffect;
        private void Awake()
        {
            animator = GetComponent<Animator>();

            //targetPosition = transform.position;

            zOffSet = FindObjectOfType<Camera>().zOffSet;
        }
        private void Start()
        {
            if (WeaponsCrossHair == null) WeaponsCrossHair = GameObject.Find("Canvas").transform.Find("CrossHair").gameObject;
            GameObject obj = GameObject.Find("HeroLight");
            if (obj != null)
            {
                obj.transform.SetParent(this.transform);
                obj.transform.localPosition = Vector3.zero;
                obj.transform.localScale = Vector3.one;
            }
            GetComponent<SpriteRenderer>().sortingLayerName = "Player";
            _coll = GetComponent<Collider2D>();
        }
        private void Update()
        {
            if (CustomManager.instance != null && CustomManager.instance.isOpen) return;
            if (GetComponent<Player>()) if (GetComponent<Player>().isDie) return;
            if (Dialogue.Instance != null && Dialogue.Instance.isCommnet) return;

            if(WeaponsFireTransform == null)
            {
                if (transform.Find("Gun").transform.Find("Gun").Find("Fireposition"))
                    WeaponsFireTransform = transform.Find("Gun").transform.Find("Gun").Find("Fireposition");
                else
                {
                    GameObject newFirePos = new GameObject();
                    CircleCollider2D c = newFirePos.AddComponent<CircleCollider2D>();
                    c.isTrigger = true;
                    c.radius = 0.05f;
                    newFirePos.layer = 17;
                    newFirePos.name = "Fireposition";
                    newFirePos.transform.SetParent(transform.Find("Gun").transform.Find("Gun"));
                    newFirePos.transform.SetSiblingIndex(3);
                    newFirePos.transform.localScale = Vector3.one;
                    newFirePos.transform.localEulerAngles = Vector3.zero;
                    newFirePos.transform.localPosition = new Vector3(1, 0.03f, 0);
                    WeaponsFireTransform = newFirePos.transform;
                }
            }

            if(isBack)
            {
                backTime -= Time.deltaTime;
                if (backTime <= 0.0f) isBack = false;
            }
            MoveEffect();
            Avoid();
            NewCollisionCheck();
            int mask = 1 << 21;
            RaycastHit2D hit = Physics2D.CircleCast(transform.position, 0.2f, Vector2.zero, 1, mask);
            if (!hit && !isHole)
            {
                ShopBuyInfo.instance.HideInfo();
                Dungeon.instance.isSelect = false;
            }
            else
            {
                Dungeon.instance.isSelect = true;
            }
            mask = 1 << 22;
            hit = Physics2D.CircleCast(transform.position, 1.5f, Vector2.zero, 1, mask);
            if (hit)
            {
                if (!hit.collider.GetComponent<Chest>().isOpen)
                {
                    Dungeon.instance.isSelect = true;
                    GameObject pKey = GameObject.Find("Canvas").transform.Find("PressKey").gameObject;
                    pKey.SetActive(true);
                    hit.collider.GetComponent<Chest>().isSelected = true;
                    pKey.transform.position = UnityEngine.Camera.main.WorldToScreenPoint(hit.collider.transform.position);
                }
            }
            mask = 1 << 23;
            hit = Physics2D.CircleCast(transform.position, 0.2f, Vector2.zero, 1, mask);
            if (hit)
            {
                if (hit.collider.GetComponent<DestroyObjects>())
                {
                    Vector3 dir = hit.collider.transform.position - Player.instance.transform.position;
                    dir.Normalize();
                    hit.collider.GetComponent<DestroyObjects>().DestroyObject(dir);
                }
                if (hit.collider.GetComponent<ObjectFragments>())
                {
                    hit.collider.GetComponent<ObjectFragments>().ReDestroyObject();
                }
            }
        }
        private void NewCollisionCheck()
        {
            RaycastHit2D hit;
            GameObject pKey = GameObject.Find("Canvas").transform.Find("PressKey").gameObject;
            if (!Dungeon.instance.isSelect) pKey.SetActive(false);
            Dungeon.instance.isSelect = false;
            isHole = false;

            Vector3 hitCollisionSize = new Vector3(0.5f, 0.5f);
            if (Player.instance.isRollingTransform) hitCollisionSize = new Vector3(1.5f, 1.5f);

            hit = Physics2D.BoxCast(transform.position, hitCollisionSize, 0, GetDirection(PlayerCombat.pl), 0, obstacles);
            if (hit && (PlayerCombat.instance.coll.enabled || PlayerCombat.instance.cColl.enabled))
            {
                if (hit.collider.tag.Equals("Enemy") && !Player.instance.isHit)
                {
                    if (hit.collider.GetComponent<MOB.Monster>() && hit.collider.GetComponent<MOB.Monster>().isMiMicWait)
                    {
                        return;
                    }

                    if (Player.instance.isRollingTransform)
                    {
                        Vector3 dir = hit.collider.transform.position - Player.instance.transform.position;
                        dir.Normalize();
                        if(hit.collider.GetComponent<MOB.Monster>())
                        {
                            hit.collider.GetComponent<MOB.Monster>().SetDamage((Player.instance.isQNear ? Player.instance.damageQNear : Player.instance.damageQ), -dir, 2, Player.instance.q_elemetanl, Player.instance.q_elemnetalTime);

                            if (PDG.Player.instance.isQNear && PDG.Player.instance.isCriticalQ)
                            {
                                GameObject criEffect = ObjManager.Call().GetObject("CriticalHitEffect");
                                criEffect.transform.position = hit.point;
                                criEffect.SetActive(true);
                            }
                        }
                        else if (hit.collider.GetComponent<MOB.BossMonster_Six_Apear>())
                        {
                            hit.collider.GetComponent<MOB.BossMonster_Six_Apear>().SetDamage(Player.instance.damageQ);
                            if (PDG.Player.instance.isQNear && PDG.Player.instance.isCriticalQ)
                            {
                                GameObject criEffect = ObjManager.Call().GetObject("CriticalHitEffect");
                                criEffect.transform.position = hit.point;
                                criEffect.SetActive(true);
                            }
                        }
                        else if (hit.collider.GetComponent<Boss_STG>())
                        {
                            hit.collider.GetComponent<Boss_STG>().SetDamage(Player.instance.isQNear ? Player.instance.damageQNear : Player.instance.damageQ, hit.point);
                            if (PDG.Player.instance.isQNear && PDG.Player.instance.isCriticalQ)
                            {
                                GameObject criEffect = ObjManager.Call().GetObject("CriticalHitEffect");
                                criEffect.transform.position = hit.point;
                                criEffect.SetActive(true);
                            }
                        }
                        else if (hit.collider.GetComponent<Boss_NPC>())
                        {
                            hit.collider.GetComponent<Boss_NPC>().SetDamage(Player.instance.isQNear ? Player.instance.damageQNear : Player.instance.damageQ, hit.point);
                            if (PDG.Player.instance.isQNear && PDG.Player.instance.isCriticalQ)
                            {
                                GameObject criEffect = ObjManager.Call().GetObject("CriticalHitEffect");
                                criEffect.transform.position = hit.point;
                                criEffect.SetActive(true);
                            }
                        }
                        else if (hit.collider.GetComponent<Boss_Golem>())
                        {
                            hit.collider.GetComponent<Boss_Golem>().SetDamage(Player.instance.isQNear ? Player.instance.damageQNear : Player.instance.damageQ, hit.point);
                            if (PDG.Player.instance.isQNear && PDG.Player.instance.isCriticalQ)
                            {
                                GameObject criEffect = ObjManager.Call().GetObject("CriticalHitEffect");
                                criEffect.transform.position = hit.point;
                                criEffect.SetActive(true);
                            }
                        }
                        else if (hit.collider.GetComponent<Boss_Stone>())
                        {
                            hit.collider.GetComponent<Boss_Stone>().SetDamage(Player.instance.isQNear ? Player.instance.damageQNear : Player.instance.damageQ, hit.point, true);
                            if (PDG.Player.instance.isQNear && PDG.Player.instance.isCriticalQ)
                            {
                                GameObject criEffect = ObjManager.Call().GetObject("CriticalHitEffect");
                                criEffect.transform.position = hit.point;
                                criEffect.SetActive(true);
                            }
                        }

                        CameraShaker._instance.StartShake(0.05f, 0.05f, 0.05f);
                        SoundManager.instance.StartAudio(new string[1] { "Character/rollingHit_" + Random.Range(0, 3) }, VOLUME_TYPE.EFFECT);
                        GameObject obj_hit = ObjManager.Call().GetObject("Hit_sprite4");
                        obj_hit.transform.position = hit.collider.transform.position;
                        obj_hit.transform.localScale = Vector3.one;
                        obj_hit.transform.localEulerAngles = Vector2.zero;
                        obj_hit.GetComponent<SwordHitEffectController>()._effctName = "rollingAttack_" + Random.Range(0, 3);
                        obj_hit.GetComponent<SwordHitEffectController>().animSpeed = 1.0f;
                        obj_hit.GetComponent<SwordHitEffectController>().isPenetrate = false;
                        obj_hit.GetComponent<SwordHitEffectController>().isAttack = false;
                        obj_hit.GetComponent<Animator>().Rebind();
                        obj_hit.SetActive(true);

                    }

                    Vector3 backDir = transform.position - hit.collider.transform.position;
                    backDir.Normalize();
                    RaycastHit2D hit2 = Physics2D.Raycast(transform.position + backDir, backDir, 1, 1 << 8);
                    if (!hit2) transform.Translate(backDir * 20.0f * Time.deltaTime);
                    isBack = true;
                    backTime = 0.1f;
                    if (hit.collider.GetComponent<MOB.Monster>())
                    {
                        string tId = hit.collider.GetComponent<MOB.Monster>().mobData.ID;
                        if (tId.Contains("_t")) tId = tId.Substring(0, tId.Length - 3);
                        hit.collider.GetComponent<MOB.Monster>().Knock(gameObject);
                        GetComponent<Player>().SetDamage(1, LocalizeManager.GetLocalize("monster_" + tId), 
                            hit.collider.GetComponent<MOB.Monster>().isBoss ? DEATH_TYPE.DEATH_SUB_BOSS  : (hit.collider.GetComponent<MOB.Monster>().isUnique ? DEATH_TYPE.DEATH_UNIQUE : DEATH_TYPE.DEATH_NORMAL), tId);
                    }
                    else if (hit.collider.GetComponent<MOB.BossMonster_Six_Apear>())
                    {
                        string tId = hit.collider.GetComponent<MOB.BossMonster_Six_Apear>().mobData.ID;
                        if (tId.Contains("_t")) tId = tId.Substring(0, tId.Length - 3);
                        GetComponent<Player>().SetDamage(1, LocalizeManager.GetLocalize("monster_" + tId), DEATH_TYPE.DEATH_BOSS, "octopus_king");
                    }
                    else if (hit.collider.GetComponent<Boss_STG>())
                    {
                        string tId = hit.collider.GetComponent<Boss_STG>().mobData.ID;
                        if (tId.Contains("_t")) tId = tId.Substring(0, tId.Length - 3);
                        GetComponent<Player>().SetDamage(1, LocalizeManager.GetLocalize("monster_" + tId), DEATH_TYPE.DEATH_BOSS, "boss_stg");
                    }
                    else if (hit.collider.GetComponent<Boss_NPC>())
                    {
                        string tId = hit.collider.GetComponent<Boss_NPC>().mobData.ID;
                        if (tId.Contains("_t")) tId = tId.Substring(0, tId.Length - 3);
                        GetComponent<Player>().SetDamage(1, LocalizeManager.GetLocalize("monster_" + tId), DEATH_TYPE.DEATH_BOSS, "boss_npc");
                    }
                    else if (hit.collider.GetComponent<Boss_Golem>())
                    {
                        string tId = hit.collider.GetComponent<Boss_Golem>().mobData.ID;
                        if (tId.Contains("_t")) tId = tId.Substring(0, tId.Length - 3);
                        GetComponent<Player>().SetDamage(1, LocalizeManager.GetLocalize("monster_" + tId), DEATH_TYPE.DEATH_BOSS, "boss_golem");
                    }
                    else if (hit.collider.GetComponent<Boss_Stone>())
                    {
                        string tId = hit.collider.GetComponent<Boss_Stone>().mobData.ID;
                        if (tId.Contains("_t")) tId = tId.Substring(0, tId.Length - 3);
                        GetComponent<Player>().SetDamage(1, LocalizeManager.GetLocalize("monster_" + tId), DEATH_TYPE.DEATH_BOSS, "boss_stone");
                    }
                    GameObject obj2 = Instantiate(hitEffect);
                    obj2.transform.position = transform.position;
                    Destroy(obj2, 2f);
                }
                //else if (hit.collider.tag.Equals("Warp") && !Dungeon.instance.newObjectSelected)
                //{
                //    isHole = true;
                //    if (hit.collider.GetComponent<Exit>())
                //    {
                //        hit.collider.GetComponent<Exit>().isSelected = true;
                //        hit.collider.GetComponent<Exit>().AdjacentActive();
                //        Dungeon.instance.newObjectSelected = true;
                //        Dungeon.instance.newObjectSelectedObj = hit.collider.gameObject;
                //    }
                //}
                else if (hit.collider.tag.Equals("NPC"))
                {
                    if (hit.collider.GetComponent<NPC>())
                    {
                        hit.collider.GetComponent<NPC>().isSelected = true;
                        Dungeon.instance.isSelect = true;
                        isHole = true;
                    }
                }
            }
        }
        #region -- Coroutine --
        #endregion
        #region -- Move Method --
        private void MoveEffect()
        {
            if (!animator.GetBool("Idle"))
            {
                Flip();
                moveEffectTimeNow += Time.deltaTime;
                if (moveEffectTimeNow > moveEffectTime)
                {
                    moveEffectTimeDelayNow += Time.deltaTime;
                    if (moveEffectTimeDelayNow >= moveEffectTimeDelay)
                    {
                        moveEffectTimeDelayNow = 0.0f;
                        Vector2 plusPosition = RunEffectPositionNow;
                        if (transform.localScale.x < 0) plusPosition.x *= -1;
                        GameObject obj = Instantiate(RunEffectObj, (Vector2)transform.position + plusPosition, Quaternion.identity, null);
                        Destroy(obj, 1.0f);
                    }
                }
            }
            else
            {
                moveEffectTimeNow = 0.0f;
                moveEffectTimeDelayNow = moveEffectTimeDelay;
            }
        }
        private void Avoid()
        {
            avoidTimeNow += Time.deltaTime;
            if (avoidTimeNow > (avoidTime / 2))
            {
                avoidSpeed = 1.0f;
            }
            if (avoidTimeNow > avoidTime && isTeleport)
            {
                if (isWeaponMode) WeaponsSlot.SetActive(true);
                if (!_coll.enabled) _coll.enabled = true;
            }
            if ((PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_SLIDE], true) || Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_SLIDE], true)) && avoidTimeNow > avoidTime && isTeleport && !Player.instance._ArmController.isGrab && !PDG.Player.instance.isRollingTransform && !PDG.Player.instance.isMiniGame
                && (!PDG.Dungeon.instance.nowRoomType.Equals("boss_waiting_2") || !PDG.Dungeon.instance.isStageBoss))
            {
                isTeleportUse = true;
                avoidTimeNow = 0.0f;
                StartCoroutine(TeleportCoroutine());
                avoidSpeed = teleportSpeed;
                if (isWeaponMode) WeaponsSlot.SetActive(false);
                if (_coll.enabled) _coll.enabled = false;
            }
        }
        public void WeaponOnOff(bool changeValue)
        {
            if (WeaponsCrossHair == null) WeaponsCrossHair = GameObject.Find("Canvas").transform.Find("CrossHair").gameObject;
            isWeaponMode = changeValue;
            WeaponsSlot.SetActive(isWeaponMode);
            WeaponsCrossHair.SetActive(isWeaponMode);
            //animator.SetTrigger("WeaponsTrigger");
            //animator.SetBool("isWeapons", isWeaponMode);
        }
        private void WeaponsControl()
        {
            //if (isWeaponMode)
            //{
            //    if (WeaponsCrossHair == null) WeaponsCrossHair = GameObject.Find("Canvas").transform.Find("CrossHair").gameObject;
            //
            //    WeaponsCrossHair.transform.position = Input.mousePosition;
            //
            //
            //    Vector3 thisPos = transform.position;
            //    thisPos.y += 0.75f;
            //
            //    Vector3 firePos = WeaponsFireTransform.position;
            //    Vector3 crossPos = WeaponsCrossHair.transform.position;
            //    crossPos.z = zOffSet;
            //    crossPos = UnityEngine.Camera.main.ScreenToWorldPoint(crossPos);
            //    firePos.z = crossPos.z = 0;
            //    angle = GetAngle(firePos, crossPos);
            //    while (angle > 360.0f) angle -= 360.0f;
            //    while (angle < 0) angle += 360.0f;
            //
            //    float dis = Vector3.Distance(transform.position, crossPos);
            //    if (dis <= MinRange)
            //    {
            //        Vector3 dir = crossPos - thisPos;
            //        dir.Normalize();
            //        Vector3 newPos = thisPos + dir * MinRange;
            //        WeaponsCrossHair.transform.position = UnityEngine.Camera.main.WorldToScreenPoint(newPos);
            //        firePos = WeaponsFireTransform.position;
            //        crossPos = WeaponsCrossHair.transform.position;
            //        crossPos.z = zOffSet;
            //        crossPos = UnityEngine.Camera.main.ScreenToWorldPoint(crossPos);
            //        firePos.z = crossPos.z = 0;
            //        angle = GetAngle(firePos, crossPos);
            //        while (angle > 360.0f) angle -= 360.0f;
            //        while (angle < 0) angle += 360.0f;
            //    }
            //
            //    float angle2 = GetAngle(thisPos, crossPos);
            //    while (angle2 > 360.0f) angle2 -= 360.0f;
            //    while (angle2 < 0) angle2 += 360.0f;
            //
            //    if ((angle2 >= 0.0f && angle2 <= 22.5f) || (angle2 > 337.5f && angle2 <= 360.0f)) directionWeapons = Direction.Right;
            //    else if (angle2 > 22.5f && angle2 <= 67.5f) directionWeapons = Direction.RightUp;
            //    else if (angle2 > 67.5f && angle2 <= 112.5f) directionWeapons = Direction.Up;
            //    else if (angle2 > 112.5f && angle2 <= 157.5f) directionWeapons = Direction.LeftUp;
            //    else if (angle2 > 157.5f && angle2 <= 202.5f) directionWeapons = Direction.Left;
            //    else if (angle2 > 202.5f && angle2 <= 247.5f) directionWeapons = Direction.LeftDown;
            //    else if (angle2 > 247.5f && angle2 <= 292.5f) directionWeapons = Direction.Down;
            //    else if (angle2 > 292.5f && angle2 <= 337.5f) directionWeapons = Direction.RightDown;
            //    else directionWeapons = Direction.None;
            //
            //    if (directionWeapons == Direction.LeftUp || directionWeapons == Direction.RightUp || directionWeapons == Direction.Up)
            //    {
            //        if (WeaponsSlot.GetComponent<SpriteRenderer>()) WeaponsSlot.GetComponent<SpriteRenderer>().sortingOrder = 2;
            //    }
            //    else
            //    {
            //        if (WeaponsSlot.GetComponent<SpriteRenderer>()) WeaponsSlot.GetComponent<SpriteRenderer>().sortingOrder = 4;
            //    }
            //    animator.SetInteger("WeaponDirection", (int)directionWeapons);
            //}
        }
        private void Move(Vector2 axisDirection)
        {
            //if (animator.GetBool("isMove")) animator.SetInteger("Direction", (int)direction);
            if (axisDirection != Vector2.zero/* && targetPosition == (Vector2)transform.position*/)
            {
                animator.SetBool("isMove", true);
                if (axisDirection.x > 0 && axisDirection.y > 0) ChangeDirection(Direction.RightUp);
                else if (axisDirection.x > 0 && axisDirection.y < 0) ChangeDirection(Direction.RightDown);
                else if (axisDirection.x == 0 && axisDirection.y < 0) ChangeDirection(Direction.Down);
                else if (axisDirection.x == 0 && axisDirection.y > 0) ChangeDirection(Direction.Up);
                else if (axisDirection.x < 0 && axisDirection.y < 0) ChangeDirection(Direction.LeftDown);
                else if (axisDirection.x < 0 && axisDirection.y > 0) ChangeDirection(Direction.LeftUp);
                else if (axisDirection.x > 0 && axisDirection.y == 0) ChangeDirection(Direction.Right);
                else if (axisDirection.x < 0 && axisDirection.y == 0) ChangeDirection(Direction.Left);
                else ChangeDirection(Direction.None);
            }
            if (axisDirection == Vector2.zero) //if ((Vector2)transform.position == targetPosition)
            {
                if (Footsteps.isPlaying) Footsteps.Stop();
                animator.SetBool("isMove", false);
                ChangeDirection(Direction.None);
            }

            if (!Footsteps.isPlaying) Footsteps.Play();
            float t = speed * avoidSpeed * ((moveEffectTimeNow >= moveEffectTime) ? bonusSpeed : 1.0f);
            if (t > 7) t = 7;
            //RaycastHit2D hit = Physics2D.Raycast(transform.position, axisDirection, 2, 1 << 8 | 1 << 29);
            //if (!hit)
            //{
            //    transform.position = Vector2.MoveTowards(transform.position, transform.position + (Vector3)axisDirection, t * Time.deltaTime);
            //}
        }
        public void ChangeDirection(Direction dir)
        {
            //direction = dir;

            if (CheckCollision)
            {
                return;
            }
            //targetPosition += GetDirection(direction);
        }
        private bool CheckCollision
        {
            get
            {
                //RaycastHit2D hit;
                //GameObject pKey = GameObject.Find("Canvas").transform.Find("PressKey").gameObject;
                //if (!Dungeon.instance.isSelect) pKey.SetActive(false);
                //Dungeon.instance.isWarp = false;
                //Dungeon.instance.isSelect = false;
                //
                //hit = Physics2D.BoxCast(transform.position, new Vector2(0.4f, 0.4f), 0, GetDirection(direction), 1, obstacles);
                //if (hit)
                //{
                //    if (hit.collider.tag.Equals("Enemy"))
                //    {
                //        hit.collider.GetComponent<MOB.Monster>().Knock(gameObject);
                //        GetComponent<Player>().SetDamage(1);
                //        return false;
                //    }
                //    if (hit.collider.tag.Equals("Warp"))
                //    {
                //        if (hit.collider.GetComponent<Exit>()) hit.collider.GetComponent<Exit>().isSelected = true;
                //        if (hit.collider.GetComponent<Exit>()) hit.collider.GetComponent<Exit>().AdjacentActive();
                //        if(hit.collider.name.Contains("boss_hole"))
                //        {
                //            pKey.SetActive(true);
                //            pKey.transform.position = UnityEngine.Camera.main.WorldToScreenPoint(hit.transform.position);
                //            if(Input.GetKeyDown(KeyCode.F))
                //            {
                //                Debug.Log("CLEAR");
                //                Dungeon.instance.GoAjit();
                //            }
                //        }
                //        Dungeon.instance.isWarp = true;
                //        return false;
                //    }
                //    if (hit.collider.tag.Equals("Door"))
                //    {
                //        return !hit.collider.GetComponent<Door>().isClear;
                //    }
                //    if (hit.collider.tag.Equals("NPC"))
                //    {
                //        if (hit.collider.GetComponent<NPC>())
                //        {
                //            hit.collider.GetComponent<NPC>().isSelected = true;
                //            Dungeon.instance.isSelect = true;
                //        }
                //        else if (hit.collider.GetComponent<Entrance>())
                //        {
                //            hit.collider.GetComponent<Entrance>().Question();
                //        }
                //        return false;
                //    }
                //}
                return true;
                //return hit.collider != null;
            }
        }
        private void OnDrawGizmos()
        {
            //Gizmos.color = Color.blue;
            //Gizmos.DrawWireCube(transform.position + ((Vector3)GetDirection(direction) * 1), new Vector2(0.4f, 0.4f));
        }
        private Vector2 GetDirection(PlayerLookDirection dir)
        {
            Vector2 v = Vector2.zero;

            switch (dir)
            {
                case PlayerLookDirection.Up: v = Vector2.up; break;
                case PlayerLookDirection.Down: v = Vector2.down; break;
                case PlayerLookDirection.Right: v = Vector2.right; break;
                case PlayerLookDirection.Left: v = Vector2.left; break;
                case PlayerLookDirection.UpRight: v = new Vector2(0.5f, 0.5f); break;
                case PlayerLookDirection.UpLeft: v = new Vector2(-0.5f, 0.5f); break;
                case PlayerLookDirection.DownRight: v = new Vector2(0.5f, -0.5f); break;
                case PlayerLookDirection.DownLeft: v = new Vector2(-0.5f, -0.5f); break;
                default: v = Vector2.zero; break;

            }
            return v;
        }
        private void Flip()
        {
            switch (PlayerCombat.pl)
            {
                case PlayerLookDirection.DownRight:
                    {
                        RunEffectPositionNow = RunEffectPositions[0];
                        break;
                    }
                case PlayerLookDirection.UpRight:
                    {
                        RunEffectPositionNow = RunEffectPositions[1];
                        break;
                    }
                case PlayerLookDirection.UpLeft:
                    {
                        RunEffectPositionNow = RunEffectPositions[1];
                        break;
                    }
                case PlayerLookDirection.DownLeft:
                    {
                        RunEffectPositionNow = RunEffectPositions[0];
                        break;
                    }
                case PlayerLookDirection.Up:
                    {
                        WeaponsSlot.transform.localScale = new Vector3(1, 1, 1);
                        break;
                    }
                case PlayerLookDirection.Down:
                    {
                        WeaponsSlot.transform.localScale = new Vector3(1, 1, 1);
                        break;
                    }
                case PlayerLookDirection.Left:
                    {
                        RunEffectPositionNow = RunEffectPositions[0];
                        break;
                    }
                case PlayerLookDirection.Right:
                    {
                        RunEffectPositionNow = RunEffectPositions[0];
                        break;
                    }
            }
        }
        IEnumerator TeleportCoroutine()
        {
            SoundManager.instance.StartAudio(new string[1] { "Character/teleport" }, VOLUME_TYPE.EFFECT);
            if (!DustCircle.isPlaying) DustCircle.Play();
            GetComponent<SpriteRenderer>().enabled = false;
            if (!Dungeon.headType.Equals(string.Empty) && Player.instance.helmetObj.activeSelf) Player.instance.helmetObj.SetActive(false);
            CameraShaker._instance.StartShake(0.1f, 0.02f, 0.1f);
            GameObject.Find("Camera").GetComponent<MMLensDistortionShaker>().StartShaking();
            for (int i = 0; i < 4; i++)
            {
                Vector3 pos = transform.position;
                GameObject obj = Instantiate(Resources.Load<GameObject>("Effect/Teleport"), pos, Quaternion.identity, null);
                Destroy(obj, teleportEffectDestroyTime);
                yield return new WaitForSeconds(teleportEffectDelay);
            }
            yield return new WaitForSeconds(teleportEffectDelay);
            yield return new WaitForSeconds(teleportEffectDelay);
            GetComponent<SpriteRenderer>().enabled = true;
            if (!Dungeon.headType.Equals(string.Empty) && !Player.instance.helmetObj.activeSelf) Player.instance.helmetObj.SetActive(true);
            if (isWeaponMode) WeaponsSlot.SetActive(true);
            if (!_coll.enabled) _coll.enabled = true;
            if (DustCircle.isPlaying) DustCircle.Stop();
            WeaponsSlot.transform.parent.gameObject.SetActive(true);
            isTeleportUse = false;
        }
        #endregion
        #region -- Tool --
        public static float GetAngle(Vector3 vStart, Vector3 vEnd)
        {
            Vector3 v = vEnd - vStart;

            return Mathf.Atan2(v.y, v.x) * Mathf.Rad2Deg;
        }
        public static Vector3 GetPosition(Vector3 vStart, float angle)
        {
            float x = Mathf.Cos(Mathf.Deg2Rad * angle) + vStart.x;
            float y = Mathf.Sin(Mathf.Deg2Rad * angle) + vStart.y;

            return new Vector3(x, y, 0);
        }
        #endregion
    }
}