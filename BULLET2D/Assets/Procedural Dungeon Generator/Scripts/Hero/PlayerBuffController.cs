﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBuffController : MonoBehaviour
{
    public static PlayerBuffController instance;

    public static List<string> buffList = new List<string>();
    public List<string> buffListInspector;
    private void Awake()
    {
        if (instance == null) instance = this;
        buffListInspector = buffList;
    }

    public void AddBuff(string _id)
    {
        buffList.Add(_id);
        if (_id.Equals("survival_01"))
        {
            PDG.Player.instance.MaxHpChange(2);
        }
        //if (_id.Equals("offensive_01"))
        //{
        //    PDG.Player.instance.fInvincibleTime = 3.0f;
        //    PDG.Player.instance.fPlusDamageTime = 8.0f;
        //    PDG.Player.instance.plusDamageAmound = 5;
        //}
    }
    public void DeleteBuff(string _id)
    {
        int t = buffList.FindIndex(item => item.Equals(_id));
        if (t != -1) 
        {
            buffList.RemoveAt(t);

            if (_id.Equals("survival_01"))
            {
                PDG.Player.instance.MaxHpChange(-2);
            }
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="_type"> 0 빠른시작, 1 아지트로 </param>
    public void BuffReset(int _type)
    {
        if (_type == 0)
        {
            for (int i = 0; i < buffList.Count; i++)
            {
                if (buffList[i].Equals("offensive_01")) continue;
                if (buffList[i].Equals("offensive_02")) continue;
                if (buffList[i].Equals("offensive_03")) continue;
                if (buffList[i].Equals("strategic_02")) continue;
                if (buffList[i].Equals("strategic_03")) continue;
                if (buffList[i].Equals("strategic_04")) continue;
                buffList.RemoveAt(i);
                i--;
            }
            if (buffList.Count == 0) buffList.Clear();
        }
        else
            buffList.Clear();
    }
}
