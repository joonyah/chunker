﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArmController : MonoBehaviour
{
    public int isLeft = -1;
    public bool isGrab = false;
    public MOB.Monster grabMonster = null;
    public bool isAttack = false;
    public float AxisSpeed = 5.0f;
    public int ran = 0;
    private void Update()
    {
        if(PDG.Player.instance.isArmLeftAttack)
        {
            if (ran == 0) transform.Rotate(Vector3.forward, Time.deltaTime * AxisSpeed);
            else transform.Rotate(-Vector3.forward, Time.deltaTime * AxisSpeed);
        }
        else
        {
            ran = Random.Range(0, 2);
            transform.localEulerAngles = new Vector3(0, 0, 0);
        }
    }
}
