﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct TagGunAnimControl
{
    public RuntimeAnimatorController anim;
    public RuntimeAnimatorController fireAnim;
    public string spriteName;
    public Vector3 firePos;
    public Vector3 fireEffectPos;
}
public class GunChangeController : MonoBehaviour
{
    public TagGunAnimControl[] GunAnimators;
    public Transform FirePostionTransform;
    public Transform FireEffectPostionTransform;
    public SpriteRenderer sr;
    public Animator animator;
    [SerializeField] private Sprite[] armSprites;
    [SerializeField] private int nowSelectedNumber = -1;
    public GameObject FireEffectObj;
    public bool isRight = false;
    [SerializeField] private GameObject EneryChargeObj;
    [SerializeField] private Vector3[] cannonPosition;
    int cannoneNum = 0;
    [SerializeField] private ParticleSystem chargeGlow;
    [SerializeField] private Vector3[] ElectricFireEndPosition_level_0;
    [SerializeField] private Vector3[] ElectricFireEndPosition_level_1;
    [SerializeField] private Vector3[] ElectricFireEndPosition_level_2;
    public GameObject RocketCanonFireEffect;
    [SerializeField] private SpriteOutline sOutline;
    private void Awake()
    {
        sr = GetComponent<SpriteRenderer>();
        animator = GetComponent<Animator>();
    }
    public void GunChange(string _name)
    {
        nowSelectedNumber = System.Array.FindIndex(GunAnimators, item => item.spriteName.Equals(_name));
        if (nowSelectedNumber != -1)
        {
            sr.enabled = true;
            animator.enabled = true;
            animator.runtimeAnimatorController = GunAnimators[nowSelectedNumber].anim;

            if (FirePostionTransform == null) FirePostionTransform = transform.Find("Fireposition");
            FirePostionTransform.localPosition = GunAnimators[nowSelectedNumber].firePos;
            FireEffectPostionTransform.localPosition = GunAnimators[nowSelectedNumber].fireEffectPos;
        }
        else
        {
            animator.enabled = false;
            if (_name.Contains("fanatic_dagger")) _name = PDG.Player.instance.nowWeaponName;
            sr.sprite = System.Array.Find(armSprites, item => item.name.Equals(_name));
            if (sr.sprite == null) sr.sprite = System.Array.Find(armSprites, item => item.name.Equals(_name + "_weapon"));
            if (sr.sprite == null) sr.enabled = false;

            if (FirePostionTransform == null) FirePostionTransform = transform.Find("Fireposition");
            FirePostionTransform.localPosition = Vector3.zero;
            FireEffectPostionTransform.localPosition = Vector3.zero;
        }
        sr.flipX = false;

        if (PDG.Player.instance.nowWeaponName.Contains("multiple_cellstem") || PDG.Player.instance.nowWeaponName.Contains("electric_cellstem") || PDG.Player.instance.nowWeaponName.Contains("energy_arrow"))
        {
            StartCoroutine(OutlineSetting());
        }
        else
        {
            sOutline.Clear();
        }

    }
    public void ArmChange(string _name)
    {
        if (_name.Equals("dark_cells")) transform.Find("dark_cells").gameObject.SetActive(true);
        else transform.Find("dark_cells").gameObject.SetActive(false);

        RuntimeAnimatorController run = Resources.Load<RuntimeAnimatorController>("Animator/Weapon/" + _name + "/" + _name);
        if (run == null) animator.enabled = false;
        else
        {
            animator.enabled = true;
            animator.runtimeAnimatorController = run;
        }
        sr.sprite = System.Array.Find(armSprites, item => item.name.Equals(_name));
        if (sr.sprite == null) sr.sprite = System.Array.Find(armSprites, item => item.name.ToUpper().Equals((_name + "_weapon").ToUpper()));
        if (_name.Contains("harpoon_tentacles")) sr.flipX = true;
        else sr.flipX = false;


        if (sr != null && sr.sprite != null && sr.sprite.name.Contains("pixelfire_small"))
        {
            ParticleSystem p1 = FireEffectObj.transform.Find("Fire").GetComponent<ParticleSystem>();
            ParticleSystem p2 = FireEffectObj.transform.Find("Fire").Find("Spark01").GetComponent<ParticleSystem>();
            ParticleSystem p3 = FireEffectObj.transform.Find("Fire").Find("Spark02").GetComponent<ParticleSystem>();

            ParticleSystem.MainModule pm1 = p1.main;
            ParticleSystemGradientMode mode1 = pm1.startColor.mode;
            mode1 = ParticleSystemGradientMode.TwoColors;
            Color32 color1_1 = pm1.startColor.colorMin;
            Color32 color1_2 = pm1.startColor.colorMax;
            color1_1 = new Color32(236, 133, 66, 255);
            color1_2 = new Color32(255, 79, 0, 255);
            ParticleSystem.MinMaxGradient pc1 = new ParticleSystem.MinMaxGradient(color1_1, color1_2);
            pm1.startColor = pc1;

            ParticleSystem.MainModule pm2 = p2.main;
            ParticleSystemGradientMode mode2 = pm2.startColor.mode;
            mode2 = ParticleSystemGradientMode.TwoColors;
            Color32 color2_1 = pm2.startColor.colorMin;
            Color32 color2_2 = pm2.startColor.colorMax;
            color2_1 = new Color32(236, 133, 66, 255);
            color2_2 = new Color32(255, 79, 0, 255);
            ParticleSystem.MinMaxGradient pc2 = new ParticleSystem.MinMaxGradient(color2_1, color2_2);
            pm2.startColor = pc2;

            ParticleSystem.MainModule pm3 = p3.main;
            ParticleSystemGradientMode mode3 = pm3.startColor.mode;
            mode3 = ParticleSystemGradientMode.TwoColors;
            Color32 color3_1 = pm3.startColor.colorMin;
            Color32 color3_2 = pm3.startColor.colorMax;
            color3_1 = new Color32(236, 133, 66, 255);
            color3_2 = new Color32(255, 79, 0, 255);
            ParticleSystem.MinMaxGradient pc3 = new ParticleSystem.MinMaxGradient(color3_1, color3_2);
            pm3.startColor = pc3;

            FireEffectObj.SetActive(true);
        }
        else if (sr != null && sr.sprite != null && sr.sprite.name.Contains("cold_cell_wave"))
        {
            ParticleSystem p1 = FireEffectObj.transform.Find("Fire").GetComponent<ParticleSystem>();
            ParticleSystem p2 = FireEffectObj.transform.Find("Fire").Find("Spark01").GetComponent<ParticleSystem>();
            ParticleSystem p3 = FireEffectObj.transform.Find("Fire").Find("Spark02").GetComponent<ParticleSystem>();

            ParticleSystem.MainModule pm1 = p1.main;
            ParticleSystemGradientMode mode1 = pm1.startColor.mode;
            mode1 = ParticleSystemGradientMode.TwoColors;
            Color32 color1_1 = pm1.startColor.colorMin;
            Color32 color1_2 = pm1.startColor.colorMax;
            color1_1 = new Color32(66, 166, 236, 255);
            color1_2 = new Color32(0, 31, 255, 255);
            ParticleSystem.MinMaxGradient pc1 = new ParticleSystem.MinMaxGradient(color1_1, color1_2);
            pm1.startColor = pc1;

            ParticleSystem.MainModule pm2 = p2.main;
            ParticleSystemGradientMode mode2 = pm2.startColor.mode;
            mode2 = ParticleSystemGradientMode.TwoColors;
            Color32 color2_1 = pm2.startColor.colorMin;
            Color32 color2_2 = pm2.startColor.colorMax;
            color2_1 = new Color32(66, 166, 236, 255);
            color2_2 = new Color32(0, 31, 255, 255);
            ParticleSystem.MinMaxGradient pc2 = new ParticleSystem.MinMaxGradient(color2_1, color2_2);
            pm2.startColor = pc2;

            ParticleSystem.MainModule pm3 = p3.main;
            ParticleSystemGradientMode mode3 = pm3.startColor.mode;
            mode3 = ParticleSystemGradientMode.TwoColors;
            Color32 color3_1 = pm3.startColor.colorMin;
            Color32 color3_2 = pm3.startColor.colorMax;
            color3_1 = new Color32(66, 166, 236, 255);
            color3_2 = new Color32(0, 31, 255, 255);
            ParticleSystem.MinMaxGradient pc3 = new ParticleSystem.MinMaxGradient(color3_1, color3_2);
            pm3.startColor = pc3;

            FireEffectObj.SetActive(true);
        }
        else FireEffectObj.SetActive(false);

        if (sr != null && sr.sprite != null && sr.sprite.name.Contains("ice_spear"))
        {
            transform.Find("Ice_spear_effect").gameObject.SetActive(true);
            transform.Find("volatile").gameObject.SetActive(false);
        }
        else if (_name.Contains("volatile"))
        {
            transform.Find("volatile").gameObject.SetActive(true);
            transform.Find("Ice_spear_effect").gameObject.SetActive(false);
        }
        else
        {
            transform.Find("Ice_spear_effect").gameObject.SetActive(false);
            transform.Find("volatile").gameObject.SetActive(false);
        }

        if (FirePostionTransform == null) FirePostionTransform = transform.Find("Fireposition");

        if (_name.Contains("elastic_cell_weapon"))
            FirePostionTransform.localPosition = new Vector3(0.65f, 0.0f);
        else
            FirePostionTransform.localPosition = Vector3.zero;

        sOutline.Clear();
        //if (PDG.Player.instance.nowWeaponName.Contains("multiple_cellstem") || PDG.Player.instance.nowWeaponName.Contains("electric_cellstem") || PDG.Player.instance.nowWeaponName.Contains("energy_arrow"))
        //{
        //    try
        //    {
        //        if (GetComponent<SpriteRenderer>().sprite != null)
        //            sOutline.Regenerate();
        //    }
        //    catch (System.Exception e)
        //    {
        //
        //    }
        //}
    }
    IEnumerator OutlineSetting()
    {
        yield return null;
    }
    public void FireEffect(float angle)
    {
        if (sr.sprite.name.Contains("harpoon_tentacles")) return;
        if (sr.sprite.name.Contains("rocket_cannon")) return;
        if (sr.sprite.name.Contains("pixelfire_small")) return;
        if (sr.sprite.name.Contains("volatile")) return;
        if (sr.sprite.name.Contains("elastic_cell_weapon")) return;
        if (sr.sprite.name.Contains("old")) return;
        if (nowSelectedNumber == -1) return;
        if (animator.runtimeAnimatorController != null && animator.runtimeAnimatorController.name.Contains("balkan")) return;
        if (animator.runtimeAnimatorController != null && animator.runtimeAnimatorController.name.Contains("energy_arrow")) return;

        GameObject fireEffectObj = ObjManager.Call().GetObject("Bullet_Fire");
        if (fireEffectObj != null)
        {
            fireEffectObj.transform.localEulerAngles = new Vector3(0, 0, angle);
            fireEffectObj.transform.position = FireEffectPostionTransform.position;
            fireEffectObj.GetComponent<BulletAnimationController>().Change(GunAnimators[nowSelectedNumber].fireAnim);
            fireEffectObj.SetActive(true);
        }
    }
    public void AnimatorFire()
    {
        PDG.Player.instance.fireCoroutine = PDG.Player.instance.StartCoroutine(PDG.Player.instance.Fire());
    }
    public void EneryCharge()
    {
        EneryChargeObj.SetActive(true);
    }
    public void EneryChargeEnd()
    {
        EneryChargeObj.SetActive(false);
    }
    public void CannonFire()
    {
        StartCoroutine(Fire2());
    }
    public void ElectricFire(int level)
    {
        StartCoroutine(Fire(level));
    }
    IEnumerator Fire(int level)
    {
        for (int i = 0; i < PDG.Player.instance.shootCount + PDG.Player.instance.plusShootCount; i++)
        {
            for (int j = 0; j < (PDG.Player.instance.multiShoot + PDG.Player.instance.plusMultiShoot); j++)
            {
                float angleangle = 0.0f;
                if ((PDG.Player.instance.multiShoot + PDG.Player.instance.plusMultiShoot) == 1) angleangle = 0.0f;
                else
                {
                    int v = (PDG.Player.instance.multiShoot + PDG.Player.instance.plusMultiShoot) / 2;
                    v *= 15;
                    angleangle = -v + (15 * ((PDG.Player.instance.multiShoot + PDG.Player.instance.plusMultiShoot) % 2 == 0 ? j + 1 : j));
                }
                float randomAngle = Random.Range(-PDG.Player.instance.aimAngle, PDG.Player.instance.aimAngle);

                for (int k = 0; k <
                    (level == 0 ?   ElectricFireEndPosition_level_0.Length :
                    (level == 1 ?   ElectricFireEndPosition_level_1.Length :
                                    ElectricFireEndPosition_level_2.Length)); k++)
                {
                    GameObject obj = ObjManager.Call().GetObject("electricBullet");
                    obj.GetComponent<PlayerAttackController>().destroyDistance = PDG.Player.instance.bulletRange;
                    obj.GetComponent<PlayerAttackController>().firePosition = transform.position;

                    FirePostionTransform.localPosition = new Vector3(0.2f, 0);
                    Vector3 firePos = FirePostionTransform.position;
                    FirePostionTransform.localPosition = (level == 0 ? ElectricFireEndPosition_level_0[k] : (level == 1 ? ElectricFireEndPosition_level_1[k] : ElectricFireEndPosition_level_2[k]));
                    Vector3 crossPos = FirePostionTransform.position;
                    firePos.z = crossPos.z = 0;
                    Vector3 dir = crossPos - firePos;
                    dir.Normalize();
                    obj.transform.position = crossPos;
                    obj.SetActive(true);
                    obj.GetComponent<PlayerAttackController>().RigidBodyFire(dir, PDG.Player.instance.bulletSpeed);
                }
            }
            yield return new WaitForSeconds(PDG.Player.instance.bulletDelay);
        }
        sr.enabled = true;
    }
    IEnumerator Fire2()
    {
        for (int i = 0; i < PDG.Player.instance.shootCount + PDG.Player.instance.plusShootCount; i++)
        {
            for (int j = 0; j < (PDG.Player.instance.multiShoot + PDG.Player.instance.plusMultiShoot); j++)
            {
                float angleangle = 0.0f;
                if ((PDG.Player.instance.multiShoot + PDG.Player.instance.plusMultiShoot) == 1) angleangle = 0.0f;
                else
                {
                    int v = (PDG.Player.instance.multiShoot + PDG.Player.instance.plusMultiShoot) / 2;
                    v *= 15;
                    angleangle = -v + (15 * ((PDG.Player.instance.multiShoot + PDG.Player.instance.plusMultiShoot) % 2 == 0 ? j + 1 : j));
                }
                float randomAngle = Random.Range(-PDG.Player.instance.aimAngle, PDG.Player.instance.aimAngle);

                FirePostionTransform.localPosition = cannonPosition[cannoneNum];
                GameObject obj = ObjManager.Call().GetObject("biological_cannon");
                obj.transform.position = FirePostionTransform.position;
                Vector3 firePos = FirePostionTransform.position;
                Vector3 crossPos = PDG.Player.GetPosition(firePos, gun_simulation.rot);
                firePos.z = crossPos.z = 0;
                Vector3 dir = crossPos - firePos;
                dir.Normalize();
                obj.transform.eulerAngles = new Vector3(-90.0f, 0, 180.0f);
                obj.GetComponent<PlayerAttackController>().firePosition = FirePostionTransform.position;
                obj.GetComponent<PlayerAttackController>().destroyDistance = PDG.Player.instance.bulletRange;
                obj.GetComponent<PlayerAttackController>().weaponName = PDG.Player.instance.nowWeaponName;
                obj.SetActive(true);
                obj.GetComponent<PlayerAttackController>().RigidBodyFire(dir, PDG.Player.instance.bulletSpeed);
            }
            yield return new WaitForSeconds(PDG.Player.instance.bulletDelay);
        }
        cannoneNum++;
        if (cannoneNum >= cannonPosition.Length) cannoneNum = 0;
        sr.enabled = true;
    }
    private void Update()
    {
        if (EneryChargeObj.activeSelf)
        {
            float t = PDG.Player.instance.eneryTime;
            if (t < 1.0f)
            {
                ParticleSystem.MainModule pm = chargeGlow.main;
                ParticleSystem.MinMaxCurve m = new ParticleSystem.MinMaxCurve(1, 1);
                pm.startSize = m;
            }
            else if (t < 3.0f)
            {
                ParticleSystem.MainModule pm = chargeGlow.main;
                ParticleSystem.MinMaxCurve m = new ParticleSystem.MinMaxCurve(4, 4);
                pm.startSize = m;
            }
            else 
            {
                ParticleSystem.MainModule pm = chargeGlow.main;
                ParticleSystem.MinMaxCurve m = new ParticleSystem.MinMaxCurve(6, 6);
                pm.startSize = m;
            }
        }
    }
}
