﻿using MoreMountains.Feedbacks;
using MoreMountains.FeedbacksForThirdParty;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.UI;

public enum DEATH_TYPE { DEATH_NORMAL, DEATH_UNIQUE, DEATH_SUB_BOSS, DEATH_BOSS, DEATH_POSION, DEATH_FIRE, DEATH_FALL, DEATH_MAX};

namespace PDG
{
    [RequireComponent(typeof(Hero))]
    public class Player : MonoBehaviour
    {
        [SerializeField] private Vector2 boxCollSize;
        [SerializeField] private int curHpInspertor;
        public static Player instance;
        public static bool bFirstSetting = false;
        public bool isScouter = false;
        public bool isBulletBounce = false;
        public int BounsCount = 0;
        public bool isLaserBounce = false;
        public int BounsLaserCount = 0;

        public float damage = 5.0f;
        public float damageNear = 5.0f;
        public int nowBuffTowerBuff = -1;
        public int[] damageOrigin = new int[2];
        public int[] damageNearOrigin = new int[2];
        public float damagePlus = 0.0f;
        public float damagePlusNear;
        public float criticalPer = 0.0f;
        public float criticalDam = 0.0f;
        public float criticalPerOrigin = 0.0f;
        public float criticalDamOrigin = 0.0f;
        public float criticalPerPlus = 0.0f;
        public float criticalDamPlus = 0.0f;
        public bool isCritical = false;
        public bool isLeftNear = false;
        public bool isLeftFixed = false;

        public float damageMin;
        public float damageMax;
        public float damageRightMin;
        public float damageRightMax;
        public float damageQMin;
        public float damageQMax;

        public float damageNearMin;
        public float damageNearMax;
        public float damageNearRightMin;
        public float damageNearRightMax;
        public float damageNearQMin;
        public float damageNearQMax;

        public float damageRight = 5.0f;
        public float damageRightNear = 5.0f;
        public int[] damageRightOrigin = new int[2];
        public int[] damageRightNearOrigin = new int[2];
        public float damageRightPlus = 0.0f;
        public float damageRightPlusNear;
        public float criticalRightPer = 0.0f;
        public float criticalRightDam = 0.0f;
        public float criticalRightPerOrigin = 0.0f;
        public float criticalRightDamOrigin = 0.0f;
        public float criticalRightPerPlus = 0.0f;
        public float criticalRightDamPlus = 0.0f;
        public bool isCriticalRight = false;
        public bool isRightNear = false;
        public bool isRightFixed = false;

        public float damageQ = 5.0f;
        public float damageQNear = 5.0f;
        public int[] damageQOrigin = new int[2];
        public int[] damageQNearOrigin = new int[2];
        public float damageQPlus = 0.0f;
        public float damageQPlusNear;
        public float criticalQPer = 0.0f;
        public float criticalQDam = 0.0f;
        public float criticalQPerOrigin = 0.0f;
        public float criticalQDamOrigin = 0.0f;
        public float criticalQPerPlus = 0.0f;
        public float criticalQDamPlus = 0.0f;
        public bool isCriticalQ = false;
        public bool isQNear = false;
        public bool isQFixed = false;

        public bool isDie = false;
        public bool isHit = false;
        public Hero hero;
        private Animator weaponAnim;

        [Header("FireDelay")]
        [SerializeField] private float fireDelay = 0.25f;
        [SerializeField] private float fireDelayOrigin = 0.0f;
        [SerializeField] private float fireDelayNow = 0.25f;
        [SerializeField] private float fireDelayRightOrigin = 0.25f;
        [SerializeField] private float fireDelayRight = 0.25f;
        [SerializeField] private float fireDelayRightNow = 0.25f;

        public float bulletSpeed = 10.0f;
        private bool isWall = false;

        [Header("HpAndShield")]
        public static int maxHp = 6;
        private static int maxShield = 54;
        public static int curHp = 6;
        private static int curShield = 0;
        public Image[] HpBar;
        public Image[] ShieldBar;
        private Sprite hp100;
        private Sprite hp50;
        private Sprite hp50_2;
        private Sprite shield100;
        private Sprite shield50;
        private Sprite hpEmpty100;
        private Sprite hpEmpty50;
        private MMLensDistortionShaker Shaker;
        public Sprite hpOn;
        public Sprite hpOff;

        [Header("Gun Control")]
        public GunChangeController gunControl;
        public float aimAngle = 0.0f;
        public float aimAngleOrigin = 0.0f;
        public int shootCount = 1;
        public int multiShoot = 1;
        public float bulletDelay = 0.0f;
        public float knockback = 1.0f;
        public float bulletSize = 1;
        public float bulletRange = 1;
        public float bulletRangeRight = 1;
        public float bulletRangeOrigin = 1;
        public float bulletRightRangeOrigin = 1;
        public bool isSpiral = false;
        public string bulletName = string.Empty;
        public string itemType = string.Empty;
        public string itemTypeRight = string.Empty;
        public string itemTypeQ = string.Empty;

        [Header("Sword Control")]
        public SwordController swordControl;
        public float forcePow = 5.0f;
        public string nowWeaponName = string.Empty;
        public string nowRightWeaponName = string.Empty;

        [Header("Arm Control")]
        public ArmController _ArmController;
        public Transform[] ArmPos;
        [SerializeField] private bool isArm = false;
        [SerializeField] private long pushCheck = 0;
        [SerializeField] private long pushCheck2 = 0;

        [SerializeField] private long pushCheck3 = 0;
        [SerializeField] private long pushCheck4 = 0;

        [Header("Feedbacks")]
        public MMFeedbacks WeaponUsedMMFeedback;

        [Header("자동 늘리기")]
        [SerializeField] private int t = 0;
        public Transform ArmParent;
        [SerializeField] private List<GameObject> ArmList = new List<GameObject>(); // Pooling 할 것들
        public GameObject armPrefabs;
        [SerializeField] private int armCount = 10;
        [SerializeField] private float armAmountX = 0.5f;
        [Header("ArmDelay")]
        [SerializeField] private float armDelay = 0.25f;
        [SerializeField] private float armDelayNow = 0.25f;
        [SerializeField] private float armWaveDelay = 0.001f;
        [SerializeField] private float armDelay2 = 0.25f;
        [SerializeField] private float armDelayNow2 = 0.25f;
        [SerializeField] private float armWaveAmount = 0.1f;
        private bool isWave = false;
        private bool isWaveRight = false;
        [SerializeField] private Sprite[] armPieceSprites;
        [SerializeField] private Sprite[] armPieceSprites_right;
        [SerializeField] private float pullPow = 50.0f;
        [SerializeField] private int frequencyCountLeft = 2;
        [SerializeField] private int spreadCountLeft = 2;
        [SerializeField] private int frequencyCountRight = 2;
        [SerializeField] private int spreadCountRight = 2;
        public bool isArmCollision = false;

        [Header("Shield")]
        public GameObject ShieldObj;
        public bool isShield = false;

        [Header("ItemSlot")]
        public Sprite noneSprite;

        [Header("AddArm")]
        [SerializeField] private bool isAddArm = false;
        [SerializeField] private float addArmRadius = 5.0f;
        private Coroutine AddArmCorutine_1 = null;
        public int AddCountMax = 10;
        public int AddCountNow = 0;
        [SerializeField] Vector3 armStartVector;

        [Header("Q SKill")]
        public bool isUseQ = false;
        public string qSkillName = string.Empty;
        public QSkillController Qskill;
        public GameObject PlayerShield;

        [Header("Why Die")]
        public string whyDie = string.Empty;
        public float grabTime = 5.0f;
        public bool isDecrease = false;

        Coroutine rightCoroutine = null;
        Coroutine leftCoroutine = null;

        [Header("BuffControl")]
        public float fInvincibleTime_offensive_01 = 0.0f;
        public float fInvincibleTime_strategic_04 = 0.0f;

        public float fPlusDamageTime_offensive01 = 0.0f;
        public float fPlusDamageTime_offensive02 = 0.0f;
        public float fPlusDamageTime_offensive04 = 0.0f;

        public float fPlusDamage_offensive01 = 0.0f;
        public float fPlusDamage_offensive02 = 0.0f;
        public float fPlusDamage_offensive04 = 0.0f;

        public int nBuff_offensive03_count = 0;
        public int nBuff_offensive03_countMax = 5;
        public bool isStrategic_02 = false;
        public bool isSurvival_02 = false;
        public Pet _pet;
        public GameObject petObj;
        public GameObject helmetObj;
        public bool isTutorial = false;
        public float[] ran2 = null;
        float biologicalAttackTime = 0.0f;
        Vector3 endPos;
        public bool isCameraOff;
        public float fBuffPlusTime = 0.0f;

        [Header("TransformRolling")]
        public GameObject rollingTransformObj;
        public bool isRollingTransform = false; // 글로벌
        public bool isRollingItem = false; //아이템 획득여부 확인용
        public BoxCollider2D bColl;
        public CircleCollider2D cColl;
        public BoxCollider2D bbColl;
        public CircleCollider2D bcColl;
        public float rollingTime;

        [SerializeField] private float rightCollTime = 6.0f;
        private float rightCollTimeOrigin = 6.0f;
        public float rightCollTimeNow = 6.0f;
        public bool isRightCooltime = false;

        public GameObject Q_SkillBodyEffect;

        [Header("Elemental Attack")]
        public bool isIceAttack = false;
        public ELEMENTAL_TYPE left_elemetanl;
        public ELEMENTAL_TYPE right_elemetanl;
        public ELEMENTAL_TYPE q_elemetanl;
        public ELEMENTAL_TYPE space_elemetanl;
        public float left_elemnetalTime = 0.0f;
        public float right_elemnetalTime = 0.0f;
        public float q_elemnetalTime = 0.0f;
        public float space_elemnetalTime = 0.0f;

        private bool isGunFixedPos = false;
        private float giant_blade_comboTime = 0.0f;
        private bool isGiant_blade_combo = false;
        private int nGiant_blade_count = 0;

        [SerializeField] private float rightCombo_time = 0.0f;
        [SerializeField] private bool isRightCombo = false;
        [SerializeField] private int rightCombo_count = 0;
        public bool isMiniGame = false;

        [Header("StateUpgrade")]
        public int plusHp = 0;
        public float plusAim = 0.0f;
        public float plusAvoidSpeed = 0.0f;
        public float plusMoveSpeed = 0.0f;
        public float plusStateDamage = 0;
        public float plusAttackSpeed = 0.0f;
        public float plusAttackRange = 0.0f;
        public float plusQAmount = 0.0f;
        public float plusCollTime = 0.0f;
        public bool isDeath_Level_1 = false;
        public bool isDeath_Level_2 = false;
        public bool isDeath_Level_3 = false;
        public int isDeath_Level_3_Count = 0;
        public bool isGold_Level_1 = false;
        public bool isGold_Level_2 = false;
        public bool isGold_Level_3 = false;
        public bool isTeleporyObjectSpawn = false;
        public float plusBulletSpeed = 0.0f;
        public int plusShootCount = 0;
        public int plusMultiShoot = 0;
        public Image[] RightComboGlass;
        private float shieldTime = 0.0f;
        float leftColltime = 0.3f;
        float leftColltimeNow = 0.3f;
        public float tickTime = 0.0f;
        public float tickTimeSpecial = 0.0f;
        public float tickTimeR = 0.0f;
        public float tickTimeQ = 0.0f;

        float dieTime = 3.0f;
        bool isResurrection = false;
        float hitTIme = 1.0f;

        public float Q_CollTime = 20.0f;
        public float Q_CollTimeNow = 20.0f;
        public bool isQ_CollTime = false;

        public float left_defence_gauge_max = 10.0f;
        public float left_defence_gauge = 10.0f;
        public float right_defence_gauge_max = 10.0f;
        public float right_defence_gauge = 10.0f;
        public float q_defence_gauge_max = 10.0f;
        public float q_defence_gauge = 10.0f;

        public float attackSpeed_boss_stg_scythe = 0.0f;

        public bool isArmLeftAttack = false;

        [Header("Swing")]
        public GameObject SwingCollision;
        public Transform SwingFirePosition;
        public float SwingFireDelay = 1.0f;

        [SerializeField] private GameObject DroneObj;
        public GameObject IceElemental;
        public bool isLock = false;
        public bool isFreeze = false;
        public bool isIceEmun = false;
        public bool isFireEmun = false;
        public bool isPoisonEmun = false;
        public bool isGuideArrow = false;
        float guideDelayNow;
        float guideDelay;

        bool isColdFire = false;
        float coldDelay = 0.5f;
        float coldDelayNow = 0.0f;
        float iceReTime = 0.0f;
        [SerializeField] private GameObject coldObj;
        GameObject instanceColdObj;
        float coldCheckTime = 6.0f;

        [SerializeField] private LineRenderer AmingLine;
        [SerializeField] private GameObject bulletTargetObj;
        [SerializeField] private GameObject bulletTargetInLineObj;

        float bulletTargetX = -36.0f;

        float pushTime = 0.0f;
        public float eneryTime = 0.0f;

        [SerializeField] private GameObject multipleBombingObj;

        [Header("Dash")]
        [SerializeField] private GameObject DashStartEffect;
        [SerializeField] private GameObject DashEndEffect;
        [SerializeField] private GameObject DasingEffect;
        public bool isDash = false;

        [Header("volatile")]
        private float volatileBuffTime = 10.0f;
        private float volatileBuffTimeNow = 10.0f;
        private float volatileBuffTimeColl = 5.0f;
        [SerializeField] private GameObject volatileBuffObj;
        public int volatileBuffType = 0;
        [SerializeField] private ParticleSystem volatileBuffParticle;
        [SerializeField] private Color volatileBuffColor_1;
        [SerializeField] private Color volatileBuffColor_2;
        [SerializeField] private Color volatileBuffColor_3;
        [SerializeField] private Color volatileBuffColor_4;
        [SerializeField] private Color volatileBuffColor_5;
        bool isCollOpen = false;
        bool isSpecialAttack = false;
        public bool isSubWeapon = false;
        [SerializeField] private GameObject FireArmObj;
        [SerializeField] private GameObject FireArmAttackObj;
        [SerializeField] private GameObject FireArmAttackPrefabs;
        [SerializeField] private float pTime = 10.0f;
        [SerializeField] private float pTimeOrigin = 10.0f;
        [SerializeField] private float pSpawnTime = 0.0f;
        [SerializeField] private Sprite bTargetNormal;
        [SerializeField] private Sprite bTargetOneCenter;
        public GameObject pTimer;
        public GameObject pTimerText;

        public float buffTowerBuffTime_0 = 0.0f;
        public float buffTowerBuffTime_1 = 0.0f;
        public float buffTowerBuffTime_2 = 0.0f;
        public float buffTowerBuffTime_3 = 0.0f;
        public bool isBufftowerBuff1 = false;
        public bool isBufftowerBuff3 = false;

        public bool isFreeze2;
        private float FreezeTime = 0.0f;
        private float rightInTime = 0.0f;
        private GameObject grapGuage;
        [SerializeField] private GameObject LaserExplosionGreen;
        [SerializeField] private PlayerCollisionController[] multiple_cellstemCollision;
        [SerializeField] private PlayerCollisionController[] electric_cellstemCollision;
        public Coroutine fireCoroutine = null;
        [SerializeField] private int maxAttackCountL;
        [SerializeField] private int maxAttackCountQ;
        [SerializeField] private int maxAttackCountR;
        [SerializeField] private GameObject windCutter_1Effect;
        [SerializeField] private GameObject secret_lasergun_FireEffect;
        [SerializeField] private GameObject NovaAttackObj;
        long rightAttackCount = 0;
        public bool isNovaReinforce = false;
        GameObject SecretBladeObj;
        public bool isBiologicalDrone = false;
        [SerializeField] private Material defaultMaterial;
        float qSpecialTime = 0.0f;
        float qSpecialTimeMAX = 10;
        private void Awake()
        {
            if (instance == null) instance = this;
            if (instance != this) Destroy(gameObject);
            hero = GetComponent<Hero>();
            RightComboGlass = GameObject.Find("Canvas").transform.Find("UI").Find("SlotPanel").Find("RB").Find("CountSimbol").GetComponentsInChildren<Image>(true);
            curHpInspertor = curHp;
        }
        private void Start()
        {
            //_pet.gameObject.SetActive(false);
            maxHp = 6;
            plusHp = 0;
            plusAim = 0;
            plusAttackRange = 0;
            plusAttackSpeed = 0;
            plusAvoidSpeed = 0;
            plusCollTime = 0;
            plusMoveSpeed = 0;
            plusQAmount = 0;
            plusStateDamage = 1.0f;
            curShield = 0;
            plusBulletSpeed = 0;
            plusShootCount = 0;
            plusMultiShoot = 0;
            tickTime = 0.0f;
            tickTimeSpecial = 0.0f;
            tickTimeR = 0.0f;
            tickTimeQ = 0.0f;
            damagePlus = 0;
            damagePlusNear = 0;
            damageQPlus = 0;
            damageQPlusNear = 0;
            damageRightPlus = 0;
            damageRightPlusNear = 0;
            criticalPerPlus = 0;
            criticalQPerPlus = 0;
            criticalRightPerPlus = 0;
            criticalDamPlus = 0;
            criticalQDamPlus = 0;
            criticalRightDamPlus = 0;
            isScouter = false;

            hp100 = Resources.Load<Sprite>("HpMp/health_100");
            hp50 = Resources.Load<Sprite>("HpMp/health_50");
            hp50_2 = Resources.Load<Sprite>("HpMp/health_50_2");
            shield100 = Resources.Load<Sprite>("HpMp/shield_100");
            shield50 = Resources.Load<Sprite>("HpMp/shield_50");
            hpEmpty100 = Resources.Load<Sprite>("HpMp/empty_100");
            hpEmpty50 = Resources.Load<Sprite>("HpMp/empty_50");
            
            HpBar = GameObject.Find("Canvas").transform.Find("HpShieldPanel").Find("HpPanel").GetComponentsInChildren<Image>(true);
            for (int i = 0; i < HpBar.Length; i++) HpBar[i].gameObject.SetActive(false);
            ShieldBar = GameObject.Find("Canvas").transform.Find("HpShieldPanel").Find("ShieldPanel").GetComponentsInChildren<Image>(true);
            bulletTargetObj = GameObject.Find("Canvas").transform.Find("BulletTarget").gameObject;
            bulletTargetInLineObj = GameObject.Find("Canvas").transform.Find("BulletTarget").Find("BulletTarget_Inline").gameObject;
            Shaker = GameObject.Find("Camera").GetComponent<MMLensDistortionShaker>();
            pTimer = GameObject.Find("Canvas").transform.Find("PlayerTimer").gameObject;
            pTimerText = GameObject.Find("Canvas").transform.Find("PlayerTimerText").gameObject;
            grapGuage = GameObject.Find("Canvas").transform.Find("GrapGuage").gameObject;

            if (!isTutorial)
            {
                if (Dungeon.isStageClear && !Dungeon.instance.isTestMode2)
                {
                    Dungeon.isStageClear = false;
                }
                else if(SaveAndLoadManager.isLoad)
                {
                    SaveAndLoadManager.isLoad = false;
                    bFirstSetting = true;
                    GameSaveData[] loadData = SaveAndLoadManager.instance.GameLoad();
                    for (int i = 0; i < loadData.Length; i++)
                    {
                        if (loadData[i].id.Contains("savedata")) continue;
                        ItemsData iData = System.Array.Find(SheetManager.Instance.ItemsDB.dataArray, item => item.ID.Equals(loadData[i].id));
                        ItemInfo info = new ItemInfo { Data = Inventorys.Instance.CopyItem(iData) };
                        Inventorys.Instance.DiscardedItem.Add(info);
                        Inventorys.Instance.DiscardedItemString.Add(info.Data.ID);
                        Inventorys.Instance.AddItem(info, loadData[i].count, false);
                    }
                }
                else if (!Dungeon.instance.isBossRush)
                {
                    // 시작 아이템 레벨 체크
                    string startLeft = string.Empty;
                    if (Dungeon.instance.isStartGun)
                    {
                        int tLeft = SaveAndLoadManager.instance.LoadUnlock("start_gun_1");
                        int tLeft2 = SaveAndLoadManager.instance.LoadUnlock("start_gun_2");
                        if (tLeft2 == 0 && tLeft == 0) startLeft = "start_gun";
                        else if (tLeft2 == 1) startLeft = "start_gun_2";
                        else startLeft = "start_gun_1";
                    }
                    else
                    {
                        int tLeft = SaveAndLoadManager.instance.LoadUnlock("tentacle_arm_1");
                        if (tLeft == 0) startLeft = "tentacle_arm";
                        else startLeft = "tentacle_arm_1";
                    }

                    int tRight = SaveAndLoadManager.instance.LoadUnlock("catching_tentacle_1");
                    string startRight = string.Empty;
                    if (tRight == 0) startRight = "catching_tentacle";
                    else startRight = "catching_tentacle_1";

                    string startQ = string.Empty;
                    if (Dungeon.instance.nQChange == 0)
                    {
                        int tQ = SaveAndLoadManager.instance.LoadUnlock("spiny_cell_firing_1");
                        if (tQ == 0) startQ = "spiny_cell_firing";
                        else startQ = "spiny_cell_firing_1";
                    }
                    else
                    {
                        startQ = "q_change";
                    }

                    ItemsData Item_left = System.Array.Find(SheetManager.Instance.ItemsDB.dataArray, item => item.ID.Equals(startLeft));
                    ItemsData Item_right = System.Array.Find(SheetManager.Instance.ItemsDB.dataArray, item => item.ID.Equals(startRight));
                    ItemsData Item_Q = System.Array.Find(SheetManager.Instance.ItemsDB.dataArray, item => item.ID.Equals(startQ));
                    ItemsData Item_SPACE = System.Array.Find(SheetManager.Instance.ItemsDB.dataArray, item => item.ID.Equals("rolling"));


                    if (!bFirstSetting)
                    {
                        bFirstSetting = true;
                        ItemInfo info = new ItemInfo { Data = Inventorys.Instance.CopyItem(Item_left) };
                        Inventorys.Instance.DiscardedItem.Add(info);
                        Inventorys.Instance.DiscardedItemString.Add(info.Data.ID);
                        ItemInfo info2 = new ItemInfo { Data = Inventorys.Instance.CopyItem(Item_right) };
                        Inventorys.Instance.DiscardedItem.Add(info2);
                        Inventorys.Instance.DiscardedItemString.Add(info2.Data.ID);
                        ItemInfo info3 = new ItemInfo { Data = Inventorys.Instance.CopyItem(Item_Q) };
                        Inventorys.Instance.DiscardedItem.Add(info3);
                        Inventorys.Instance.DiscardedItemString.Add(info3.Data.ID);
                        ItemInfo info4 = new ItemInfo { Data = Inventorys.Instance.CopyItem(Item_SPACE) };
                        Inventorys.Instance.DiscardedItem.Add(info4);
                        Inventorys.Instance.DiscardedItemString.Add(info4.Data.ID);
                        Inventorys.Instance.AddItem(Item_left, 1, false, true);
                        Inventorys.Instance.AddItem(Item_right, 1, false, true);
                        Inventorys.Instance.AddItem(Item_Q, 1, false, true);
                        Inventorys.Instance.AddItem(Item_SPACE, 1, false, true);
                    }
                }
            }
            else
            {
                bFirstSetting = true;
                if (Dungeon.isStageClear && !Dungeon.instance.isTestMode2)
                {
                    Inventorys.Instance.BuffItemReSetting();
                    Dungeon.isStageClear = false;
                    hero.WeaponsSlot = gunControl.gameObject;
                }
                else
                {
                    curHp = 1;
                    hero.WeaponsSlot = gunControl.gameObject;
                }
            }

            if (!Dungeon.isSelectHead) helmetObj.SetActive(false);
            else helmetObj.SetActive(true);

            hero.WeaponsSlot = gunControl.gameObject;
            hero.WeaponsSlot.SetActive(false);
            hero.WeaponOnOff(true);
            ShieldSetting();
            Inventorys.Instance.BuffItemReSetting();

            damagePlus -= Dungeon.curseAttack;
            damagePlusNear -= Dungeon.curseAttack;
            damageRightPlus -= Dungeon.curseAttack;
            damageRightPlusNear -= Dungeon.curseAttack;
            damageQPlus -= Dungeon.curseAttack;
            damageQPlusNear -= Dungeon.curseAttack;
            plusHp -= Dungeon.curseHealth;
            plusHp -= Dungeon.curseHealth2;

            damagePlus -= Dungeon.curseAttack2;
            damagePlusNear -= Dungeon.curseAttack2;
            damageRightPlus -= Dungeon.curseAttack2;
            damageRightPlusNear -= Dungeon.curseAttack2;
            damageQPlus -= Dungeon.curseAttack2;
            damageQPlusNear -= Dungeon.curseAttack2;
            HpRecovery(0);

        }
        public void DeleteItem(ItemsData item)
        {
            if (item.ID.Equals("fourleaf_clover")) Dungeon.instance.triggerSkullDropAmount = 0;
            else if (item.ID.Equals("fourleaf_clover_gold")) Dungeon.instance.triggerKeyDropAmount = 0;
            else if (item.ID.Equals("nerve_enhancer_arm"))
            {
                bulletSpeed -= item.Buffamount[0];

                damagePlus -= (int)item.Buffamount[1];
                if (damagePlus < 0) damagePlus = 0;
                damagePlusNear -= (int)item.Buffamount[1];
                if (damagePlusNear < 0) damagePlusNear = 0;
                damageQPlus -= (int)item.Buffamount[1];
                if (damageQPlus < 0) damageQPlus = 0;
                damageQPlusNear -= (int)item.Buffamount[1];
                if (damageQPlusNear < 0) damageQPlusNear = 0;
                damageRightPlus -= (int)item.Buffamount[1];
                if (damageRightPlus < 0) damageRightPlus = 0;
                damageRightPlusNear -= (int)item.Buffamount[1];
                if (damageRightPlusNear < 0) damageRightPlusNear = 0;
            }
            else if (item.ID.Equals("nerve_enhancer_spine"))
            {
                leftColltime += 0.1f;
                attackSpeed_boss_stg_scythe -= item.Buffamount[0];
                plusAttackSpeed -= item.Buffamount[0];
            }
            else if (item.ID.Equals("nerve_enhancer_leg"))
            {
                hero.avoidSpeedPlus -= item.Buffamount[0];

                damagePlus -= (int)item.Buffamount[1];
                if (damagePlus < 0) damagePlus = 0;
                damagePlusNear -= (int)item.Buffamount[1];
                if (damagePlusNear < 0) damagePlusNear = 0;
                damageQPlus -= (int)item.Buffamount[1];
                if (damageQPlus < 0) damageQPlus = 0;
                damageQPlusNear -= (int)item.Buffamount[1];
                if (damageQPlusNear < 0) damageQPlusNear = 0;
                damageRightPlus -= (int)item.Buffamount[1];
                if (damageRightPlus < 0) damageRightPlus = 0;
                damageRightPlusNear -= (int)item.Buffamount[1];
                if (damageRightPlusNear < 0) damageRightPlusNear = 0;
            }
            else if (item.ID.Equals("nerve_enhancer_head"))
            {
                aimAngleOrigin += item.Buffamount[0];
            }
            else if (item.ID.Equals("muscle_enhancer_arm"))
            {
                damagePlus -= item.Buffamount[0];
                if (damagePlus < 0) damagePlus = 0;
                damagePlusNear -= item.Buffamount[0];
                if (damagePlusNear < 0) damagePlusNear = 0;
                damageQPlus -= item.Buffamount[0];
                if (damageQPlus < 0) damageQPlus = 0;
                damageQPlusNear -= item.Buffamount[0];
                if (damageQPlusNear < 0) damageQPlusNear = 0;
                damageRightPlus -= item.Buffamount[0];
                if (damageRightPlus < 0) damageRightPlus = 0;
                damageRightPlusNear -= item.Buffamount[0];
                if (damageRightPlusNear < 0) damageRightPlusNear = 0;
            }
            else if (item.ID.Equals("muscle_enhancer_leg")) GetComponent<PlayerCombat>().moveSpeed -= item.Buffamount[0];
            else if (item.ID.Equals("muscle_enhancer_body") || item.ID.Equals("strengthen_stamina"))
            {
                plusHp -= (int)item.Buffamount[0];
                HpSetting();
            }
            else if (item.ID.Equals("nerve_enhancer_eyes"))
            {
                bulletRangeOrigin -= item.Buffamount[0];
                bulletRightRangeOrigin -= item.Buffamount[0];
            }
            else if (item.ID.Equals("blasting_fire"))
            {
                plusMultiShoot -= (int)item.Buffamount[0];
                ran2 = new float[multiShoot + plusMultiShoot];
                for (int i = 0; i < ran2.Length; i++) ran2[i] = Random.Range(-aimAngle, aimAngle);
                isDecrease = true;
                t = 0;
                for (int i = 0; i < ArmList.Count; i++) ArmList[i].SetActive(false);
            }
            else if (item.ID.Equals("scouter"))
            {
                isScouter = false;
            }
            else if (item.ID.Equals("continuous_shooting"))
            {
                plusShootCount -= (int)item.Buffamount[0];
            }
            else if (item.ID.Equals("electric_cell"))
            {
                tickTimeSpecial = 0.0f;
                isAddArm = false;
                damagePlus -= (int)item.Buffamount[1];
                if (damagePlus < 0) damagePlus = 0;
                damagePlusNear -= (int)item.Buffamount[1];
                if (damagePlusNear < 0) damagePlusNear = 0;
                damageQPlus -= (int)item.Buffamount[1];
                if (damageQPlus < 0) damageQPlus = 0;
                damageQPlusNear -= (int)item.Buffamount[1];
                if (damageQPlusNear < 0) damageQPlusNear = 0;
                damageRightPlus -= (int)item.Buffamount[1];
                if (damageRightPlus < 0) damageRightPlus = 0;
                damageRightPlusNear -= (int)item.Buffamount[1];
                if (damageRightPlusNear < 0) damageRightPlusNear = 0;
            }
            else if (item.ID.Equals("dex_fire"))
            {
                plusBulletSpeed -= item.Buffamount[0];
                aimAngleOrigin += item.Buffamount[1];
                damagePlus -= (int)item.Buffamount[2];
                if (damagePlus < 0) damagePlus = 0;
                damagePlusNear -= (int)item.Buffamount[2];
                if (damagePlusNear < 0) damagePlusNear = 0;
                damageQPlus -= (int)item.Buffamount[2];
                if (damageQPlus < 0) damageQPlus = 0;
                damageQPlusNear -= (int)item.Buffamount[2];
                if (damageQPlusNear < 0) damageQPlusNear = 0;
                damageRightPlus -= (int)item.Buffamount[2];
                if (damageRightPlus < 0) damageRightPlus = 0;
                damageRightPlusNear -= (int)item.Buffamount[2];
                if (damageRightPlusNear < 0) damageRightPlusNear = 0;
                criticalPerPlus -= item.Buffamount[3];
                criticalQPerPlus -= item.Buffamount[3];
                criticalRightPerPlus -= item.Buffamount[3];
            }
            else if (item.ID.Equals("increase_cri_damage"))
            {
                criticalDamPlus -= item.Buffamount[0];
                criticalQDamPlus -= item.Buffamount[0];
                criticalRightDamPlus -= item.Buffamount[0];
            }
            else if (item.ID.Equals("attack_m_manual"))
            {
                damagePlusNear -= item.Buffamount[0];
                damageQPlusNear -= item.Buffamount[0];
                damageRightPlusNear -= item.Buffamount[0];
            }
            else if (item.ID.Equals("attack_r_manual"))
            {
                damagePlus -= item.Buffamount[0];
                damageQPlus -= item.Buffamount[0];
                damageRightPlus -= item.Buffamount[0];
            }
            else if (item.ID.Equals("nova_booster"))
            {
                isNovaReinforce = false;
                if (nowRightWeaponName.Contains("energy_release") || nowRightWeaponName.Contains("frozen_nova") || nowRightWeaponName.Contains("frame_nova"))
                {
                    string prevItemID = nowRightWeaponName.Substring(0, nowRightWeaponName.Length - 2);
                    ItemsData iData = System.Array.Find(SheetManager.Instance.ItemsDB.dataArray, reinforceItem => reinforceItem.ID.Equals(prevItemID));
                    if (iData != null) SettingItem(iData);
                    NovaAttackObj = Resources.Load<GameObject>("NovaPrefabs/" + iData.ID);
                }
            }
            else if (item.ID.Equals("spiral_shot")) isSpiral = false;
            else if (item.ID.Contains("creature_piko"))
            {
                if (_pet) Destroy(_pet.gameObject);
            }
            else if (item.ID.Equals("icecloak")) isIceEmun = false;
            else if (item.ID.Equals("firecloak")) isFireEmun = false;
            else if (item.ID.Equals("poisoncloak")) isPoisonEmun = false;
            else if (item.ID.Equals("guidebullet")) isGuideArrow = false;
            else if (item.ID.Contains("biological_drone")) isBiologicalDrone = false;
            else if (item.ID.Equals("prism"))
            {
                isLaserBounce = false;
                BounsLaserCount--;
                if (BounsLaserCount < 0) BounsLaserCount = 0;
            }
            else if (item.ID.Equals("spectroscope"))
            {
                isBulletBounce = false;
                BounsCount--;
                if (BounsCount < 0) BounsCount = 0;
            }
            else if (item.ID.Equals("amplifier_5"))
            {
                fBuffPlusTime -= item.Buffamount[0];
                int t = SaveAndLoadManager.instance.LoadUnlock(item.ID + "_Custom");
                if (t == 0)
                {
                    Inventorys.Instance.ShowGetItemUI("get_helmet", Resources.Load<Sprite>("Head/" + item.ID + "/" + item.ID));
                    SaveAndLoadManager.instance.SaveUnlock(item.ID + "_Custom", 1);
                }
            }
            else if (item.ID.Equals("auxiliary_aura_5"))
            {
                right_defence_gauge_max -= item.Buffamount[0];
                GetComponent<SpriteRenderer>().material = defaultMaterial;
            }
            else if (item.ID.Contains("stats_"))
            {
                if (item.ID.Equals("stats_r_speed1")) plusAvoidSpeed = 0;
                else if (item.ID.Equals("stats_r_speed2")) plusCollTime -= 0.3f;
                else if (item.ID.Equals("stats_r_speed3")) plusCollTime -= 0.6f;
                else if (item.ID.Equals("stats_death1")) isDeath_Level_1 = false;
                else if (item.ID.Equals("stats_death2")) isDeath_Level_2 = false;
                else if (item.ID.Equals("stats_death3"))
                {
                    isDeath_Level_3 = false;
                    isDeath_Level_3_Count = 0;
                }
                else if (item.ID.Equals("stats_gold1")) isGold_Level_1 = false;
                else if (item.ID.Equals("stats_gold2")) isGold_Level_2 = false;
                else if (item.ID.Equals("stats_gold3")) isGold_Level_3 = false;
                else if (item.ID.Equals("stats_health1")) plusHp -= 2;
                else if (item.ID.Equals("stats_health2")) plusHp -= 4;
                else if (item.ID.Equals("stats_health3")) plusHp -= 6;
                else if (item.ID.Equals("stats_insight1")) plusStateDamage -= 1.2f;
                else if (item.ID.Equals("stats_insight2")) plusStateDamage -= 1.4f;
                else if (item.ID.Equals("stats_insight3")) plusStateDamage -= 1.6f;
                else if (item.ID.Equals("stats_aim1")) plusAim -= 0.2f;
                else if (item.ID.Equals("stats_aim2")) plusAim -= 0.4f;
                else if (item.ID.Equals("stats_aim3")) plusAim -= 0.6f;
                else if (item.ID.Equals("stats_m_speed1")) plusMoveSpeed -= 0.2f;
                else if (item.ID.Equals("stats_m_speed2")) plusMoveSpeed -= 0.3f;
                else if (item.ID.Equals("stats_m_speed3")) plusMoveSpeed -= 0.5f;
                else if (item.ID.Equals("stats_a_speed1")) plusAttackSpeed -= 0.5f;
                else if (item.ID.Equals("stats_a_speed2")) plusAttackSpeed -= 1.0f;
                else if (item.ID.Equals("stats_a_speed3")) plusAttackSpeed -= 1.5f;
                else if (item.ID.Equals("stats_delay1")) plusAttackRange -= 0.5f;
                else if (item.ID.Equals("stats_delay2")) plusAttackRange -= 0.7f;
                else if (item.ID.Equals("stats_delay3")) plusAttackRange -= 1.0f;
                else if (item.ID.Equals("stats_q1")) plusQAmount -= 0.2f;
                else if (item.ID.Equals("stats_q2")) plusQAmount -= 0.4f;
                else if (item.ID.Equals("stats_q3")) plusQAmount -= 0.5f;
            }
        }
        public void SettingItem(ItemsData item)
        {
            if (item.ID.Equals("avoid_enhance"))
            {
                Hero.isTeleport = true;
                Inventorys.Instance.QuickSlots[3].sprite = Resources.Load<Sprite>("Item/" + item.Type1 + "/" + item.Type2 + "/" + item.ID);
                RuntimeAnimatorController run = Resources.Load<RuntimeAnimatorController>("Item/" + item.Type1 + "/" + item.Type2 + "/" + item.ID + "/" + item.ID);
                if (run == null)
                {
                    Inventorys.Instance.QuickSlots[3].GetComponent<Animator>().enabled = false;
                }
                else
                {
                    Inventorys.Instance.QuickSlots[3].GetComponent<Animator>().runtimeAnimatorController = run;
                    Inventorys.Instance.QuickSlots[3].GetComponent<Animator>().enabled = true;
                }

            }
            else if (item.ID.Equals("rolling"))
            {
                Hero.isTeleport = false;
                Inventorys.Instance.QuickSlots[3].sprite = Resources.Load<Sprite>("Item/" + item.Type1 + "/" + item.Type2 + "/" + item.ID);
                RuntimeAnimatorController run = Resources.Load<RuntimeAnimatorController>("Item/" + item.Type1 + "/" + item.Type2 + "/" + item.ID + "/" + item.ID);
                if (run == null)
                {
                    Inventorys.Instance.QuickSlots[3].GetComponent<Animator>().enabled = false;
                }
                else
                {
                    Inventorys.Instance.QuickSlots[3].GetComponent<Animator>().runtimeAnimatorController = run;
                    Inventorys.Instance.QuickSlots[3].GetComponent<Animator>().enabled = true;
                }
            }
            else if (item.ID.Equals("fourleaf_clover")) Dungeon.instance.triggerSkullDropAmount = item.Buffamount[0];
            else if (item.ID.Equals("fourleaf_clover_gold")) Dungeon.instance.triggerKeyDropAmount = item.Buffamount[0];
            else if (item.ID.Equals("nerve_enhancer_arm"))
            {
                bulletSpeed += item.Buffamount[0];

                damagePlus += (int)item.Buffamount[1];
                damagePlusNear += (int)item.Buffamount[1];
                damageQPlus += (int)item.Buffamount[1];
                damageQPlusNear += (int)item.Buffamount[1];
                damageRightPlus += (int)item.Buffamount[1];
                damageRightPlusNear += (int)item.Buffamount[1];
            }
            else if (item.ID.Equals("nerve_enhancer_spine"))
            {
                leftColltime -= 0.1f;
                attackSpeed_boss_stg_scythe += item.Buffamount[0];
                plusAttackSpeed += item.Buffamount[0];
            }
            else if (item.ID.Equals("nerve_enhancer_leg"))
            {
                hero.avoidSpeedPlus += item.Buffamount[0];

                damagePlus += (int)item.Buffamount[1];
                damagePlusNear += (int)item.Buffamount[1];
                damageQPlus += (int)item.Buffamount[1];
                damageQPlusNear += (int)item.Buffamount[1];
                damageRightPlus += (int)item.Buffamount[1];
                damageRightPlusNear += (int)item.Buffamount[1];

            }
            else if (item.ID.Equals("nerve_enhancer_head"))
            {
                if (!Dungeon.isSelectHead) Dungeon.headType = item.ID;
                helmetObj.SetActive(true);
                aimAngleOrigin -= item.Buffamount[0];
                int t = SaveAndLoadManager.instance.LoadUnlock(item.ID + "_Custom");
                if (t == 0)
                {
                    Inventorys.Instance.ShowGetItemUI("get_helmet", Resources.Load<Sprite>("Head/" + item.ID + "/" + item.ID));
                    SaveAndLoadManager.instance.SaveUnlock(item.ID + "_Custom", 1);
                }
            }
            else if (item.ID.Equals("muscle_enhancer_arm"))
            {
                if (!Dungeon.isSelectHead) Dungeon.headType = item.ID;
                helmetObj.SetActive(true);

                damagePlus += item.Buffamount[0];
                damagePlusNear += item.Buffamount[0];
                damageRightPlus += item.Buffamount[0];
                damageRightPlusNear += item.Buffamount[0];
                damageQPlus += item.Buffamount[0];
                damageQPlusNear += item.Buffamount[0];
                int t = SaveAndLoadManager.instance.LoadUnlock(item.ID + "_Custom");
                if (t == 0)
                {
                    Inventorys.Instance.ShowGetItemUI("get_helmet", Resources.Load<Sprite>("Head/" + item.ID + "/" + item.ID));
                    SaveAndLoadManager.instance.SaveUnlock(item.ID + "_Custom", 1);
                }
            }
            else if (item.ID.Equals("muscle_enhancer_leg")) GetComponent<PlayerCombat>().moveSpeed += item.Buffamount[0];
            else if (item.ID.Equals("muscle_enhancer_body") || item.ID.Equals("strengthen_stamina"))
            {
                if (!Dungeon.isSelectHead) Dungeon.headType = item.ID;
                helmetObj.SetActive(true);

                plusHp += (int)item.Buffamount[0];
                HpSetting();
                int t = SaveAndLoadManager.instance.LoadUnlock(item.ID + "_Custom");
                if (t == 0)
                {
                    Inventorys.Instance.ShowGetItemUI("get_helmet", Resources.Load<Sprite>("Head/" + item.ID + "/" + item.ID));
                    SaveAndLoadManager.instance.SaveUnlock(item.ID + "_Custom", 1);
                }
            }
            else if (item.ID.Equals("painkiller"))
            {
                if (!Dungeon.isSelectHead) Dungeon.headType = item.ID;
                helmetObj.SetActive(true);
                ShieldRecovery((int)item.Buffamount[0]);
                int t = SaveAndLoadManager.instance.LoadUnlock(item.ID + "_Custom");
                if (t == 0)
                {
                    Inventorys.Instance.ShowGetItemUI("get_helmet", Resources.Load<Sprite>("Head/" + item.ID + "/" + item.ID));
                    SaveAndLoadManager.instance.SaveUnlock(item.ID + "_Custom", 1);
                }
            }
            else if (item.ID.Equals("nerve_enhancer_eyes"))
            {
                if (!Dungeon.isSelectHead) Dungeon.headType = item.ID;
                helmetObj.SetActive(true);
                bulletRangeOrigin += item.Buffamount[0];
                bulletRightRangeOrigin += item.Buffamount[0];
                int t = SaveAndLoadManager.instance.LoadUnlock(item.ID + "_Custom");
                if (t == 0)
                {
                    Inventorys.Instance.ShowGetItemUI("get_helmet", Resources.Load<Sprite>("Head/" + item.ID + "/" + item.ID));
                    SaveAndLoadManager.instance.SaveUnlock(item.ID + "_Custom", 1);
                }
            }
            else if (item.ID.Equals("blasting_fire"))
            {
                if (!Dungeon.isSelectHead) Dungeon.headType = item.ID;
                helmetObj.SetActive(true);
                plusMultiShoot += (int)item.Buffamount[0];
                ran2 = new float[multiShoot + plusMultiShoot];
                for (int i = 0; i < ran2.Length; i++)
                    ran2[i] = Random.Range(-aimAngle, aimAngle);
                isDecrease = true;
                t = 0;
                for (int i = 0; i < ArmList.Count; i++) ArmList[i].SetActive(false);
                int tt = SaveAndLoadManager.instance.LoadUnlock(item.ID + "_Custom");
                if (tt == 0)
                {
                    Inventorys.Instance.ShowGetItemUI("get_helmet", Resources.Load<Sprite>("Head/" + item.ID + "/" + item.ID));
                    SaveAndLoadManager.instance.SaveUnlock(item.ID + "_Custom", 1);
                }
            }
            else if (item.ID.Equals("scouter"))
            {
                if (!Dungeon.isSelectHead) Dungeon.headType = item.ID;
                helmetObj.SetActive(true);
                isScouter = true;
                int tt = SaveAndLoadManager.instance.LoadUnlock(item.ID + "_Custom");
                if (tt == 0)
                {
                    Inventorys.Instance.ShowGetItemUI("get_helmet", Resources.Load<Sprite>("Head/" + item.ID + "/" + item.ID));
                    SaveAndLoadManager.instance.SaveUnlock(item.ID + "_Custom", 1);
                }
            }
            else if (item.ID.Equals("continuous_shooting"))
            {
                if (!Dungeon.isSelectHead) Dungeon.headType = item.ID;
                helmetObj.SetActive(true);
                plusShootCount += (int)item.Buffamount[0];
                int t = SaveAndLoadManager.instance.LoadUnlock(item.ID + "_Custom");
                if (t == 0)
                {
                    Inventorys.Instance.ShowGetItemUI("get_helmet", Resources.Load<Sprite>("Head/" + item.ID + "/" + item.ID));
                    SaveAndLoadManager.instance.SaveUnlock(item.ID + "_Custom", 1);
                }

            }
            else if (item.ID.Equals("electric_cell"))
            {
                isAddArm = true;
                tickTimeSpecial = item.Ticktime;
                damagePlus += (int)item.Buffamount[1];
                damagePlusNear += (int)item.Buffamount[1];
                damageQPlus += (int)item.Buffamount[1];
                damageQPlusNear += (int)item.Buffamount[1];
                damageRightPlus += (int)item.Buffamount[1];
                damageRightPlusNear += (int)item.Buffamount[1];
            }
            else if (item.ID.Equals("dex_fire"))
            {
                if (!Dungeon.isSelectHead) Dungeon.headType = item.ID;
                helmetObj.SetActive(true);

                plusBulletSpeed += item.Buffamount[0];
                aimAngleOrigin -= item.Buffamount[1];
                damagePlus += (int)item.Buffamount[2];
                damagePlusNear += (int)item.Buffamount[2];
                damageQPlus += (int)item.Buffamount[2];
                damageQPlusNear += (int)item.Buffamount[2];
                damageRightPlus += (int)item.Buffamount[2];
                damageRightPlusNear += (int)item.Buffamount[2];
                criticalPerPlus += item.Buffamount[3];
                criticalQPerPlus += item.Buffamount[3];
                criticalRightPerPlus += item.Buffamount[3];
                int t = SaveAndLoadManager.instance.LoadUnlock(item.ID + "_Custom");
                if (t == 0)
                {
                    Inventorys.Instance.ShowGetItemUI("get_helmet", Resources.Load<Sprite>("Head/" + item.ID + "/" + item.ID));
                    SaveAndLoadManager.instance.SaveUnlock(item.ID + "_Custom", 1);
                }
            }
            else if (item.ID.Equals("increase_cri_damage"))
            {
                if (!Dungeon.isSelectHead) Dungeon.headType = item.ID;
                helmetObj.SetActive(true);
                criticalDamPlus += item.Buffamount[0];
                criticalQDamPlus += item.Buffamount[0];
                criticalRightDamPlus += item.Buffamount[0];
                int t = SaveAndLoadManager.instance.LoadUnlock(item.ID + "_Custom");
                if (t == 0)
                {
                    Inventorys.Instance.ShowGetItemUI("get_helmet", Resources.Load<Sprite>("Head/" + item.ID + "/" + item.ID));
                    SaveAndLoadManager.instance.SaveUnlock(item.ID + "_Custom", 1);
                }
            }
            else if (item.ID.Equals("attack_m_manual"))
            {
                if (!Dungeon.isSelectHead) Dungeon.headType = item.ID;
                helmetObj.SetActive(true);
                damagePlusNear += item.Buffamount[0];
                damageQPlusNear += item.Buffamount[0];
                damageRightPlusNear += item.Buffamount[0];
                int t = SaveAndLoadManager.instance.LoadUnlock(item.ID + "_Custom");
                if (t == 0)
                {
                    Inventorys.Instance.ShowGetItemUI("get_helmet", Resources.Load<Sprite>("Head/" + item.ID + "/" + item.ID));
                    SaveAndLoadManager.instance.SaveUnlock(item.ID + "_Custom", 1);
                }
            }
            else if (item.ID.Equals("attack_r_manual"))
            {
                if (!Dungeon.isSelectHead) Dungeon.headType = item.ID;
                helmetObj.SetActive(true);
                damagePlus += item.Buffamount[0];
                damageQPlus += item.Buffamount[0];
                damageRightPlus += item.Buffamount[0];
                int t = SaveAndLoadManager.instance.LoadUnlock(item.ID + "_Custom");
                if (t == 0)
                {
                    Inventorys.Instance.ShowGetItemUI("get_helmet", Resources.Load<Sprite>("Head/" + item.ID + "/" + item.ID));
                    SaveAndLoadManager.instance.SaveUnlock(item.ID + "_Custom", 1);
                }
            }
            else if (item.ID.Equals("nova_booster"))
            {
                isNovaReinforce = true;
                if (nowRightWeaponName.Contains("energy_release") || nowRightWeaponName.Contains("frozen_nova") || nowRightWeaponName.Contains("frame_nova"))
                {
                    ItemsData iData = System.Array.Find(SheetManager.Instance.ItemsDB.dataArray, reinforceItem => reinforceItem.ID.Equals(nowRightWeaponName + "_2"));
                    if (iData != null) SettingItem(iData);
                    NovaAttackObj = Resources.Load<GameObject>("NovaPrefabs/" + iData.ID);
                }
            }
            else if (item.ID.Equals("spiral_shot")) isSpiral = true;
            else if (item.ID.Contains("spiny_cell_firing")) isRollingItem = false;
            else if (item.ID.Equals("rolling_thorns")) isRollingItem = true;
            else if (item.ID.Contains("creature_piko"))
            {
                GameObject _petObj = Instantiate(petObj, null);
                Vector3 anglePosition = GetPosition(transform.position, Random.Range(0, 180));
                Vector3 dir = anglePosition - transform.position;
                dir.Normalize();
                _petObj.transform.position = transform.position + (dir * Random.Range(2, 4));
                _pet = _petObj.GetComponent<Pet>();
            }
            else if (item.ID.Equals("icecloak")) isIceEmun = true;
            else if (item.ID.Equals("firecloak")) isFireEmun = true;
            else if (item.ID.Equals("poisoncloak")) isPoisonEmun = true;
            else if (item.ID.Equals("guidebullet")) isGuideArrow = true;
            else if (item.ID.Contains("biological_drone"))
            {
                isBiologicalDrone = true;
                SoundManager.instance.StartAudio(new string[1] { "Weapon/" + nowRightWeaponName }, VOLUME_TYPE.EFFECT);
                for (int i = 0; i < 3; i++)
                {
                    GameObject obj = Instantiate(DroneObj);
                    obj.transform.position = GetPosition(transform.position, 40 * i + 30);
                    obj.GetComponent<DroneController>().moveAngle = 40 * i + 20;
                }
            }
            else if (item.ID.Equals("prism"))
            {
                isLaserBounce = true;
                BounsLaserCount++;
            }
            else if (item.ID.Equals("spectroscope"))
            {
                isBulletBounce = true;
                BounsCount++;
            }
            else if (item.ID.Equals("amplifier_5"))
            {
                if (!Dungeon.isSelectHead) Dungeon.headType = item.ID;
                helmetObj.SetActive(true);
                fBuffPlusTime += item.Buffamount[0];
                int t = SaveAndLoadManager.instance.LoadUnlock(item.ID + "_Custom");
                if (t == 0)
                {
                    Inventorys.Instance.ShowGetItemUI("get_helmet", Resources.Load<Sprite>("Head/" + item.ID + "/" + item.ID));
                    SaveAndLoadManager.instance.SaveUnlock(item.ID + "_Custom", 1);
                }
            }
            else if (item.ID.Equals("auxiliary_aura_5"))
            {
                right_defence_gauge_max += item.Buffamount[0];
                GetComponent<SpriteRenderer>().material = Resources.Load<Material>("Item/Material/" + item.ID);
            }
            else if (item.ID.Contains("stats_"))
            {
                if (item.ID.Equals("stats_r_speed1")) plusAvoidSpeed = 0.3f;
                else if (item.ID.Equals("stats_r_speed2")) plusCollTime += 0.3f;
                else if (item.ID.Equals("stats_r_speed3")) plusCollTime += 0.6f;
                else if (item.ID.Equals("stats_death1")) isDeath_Level_1 = true;
                else if (item.ID.Equals("stats_death2")) isDeath_Level_2 = true;
                else if (item.ID.Equals("stats_death3"))
                {
                    isDeath_Level_3 = true;
                    isDeath_Level_3_Count = 0;
                }
                else if (item.ID.Equals("stats_gold1")) isGold_Level_1 = true;
                else if (item.ID.Equals("stats_gold2")) isGold_Level_2 = true;
                else if (item.ID.Equals("stats_gold3")) isGold_Level_3 = true;
                else if (item.ID.Equals("stats_health1")) plusHp += 2;
                else if (item.ID.Equals("stats_health2")) plusHp += 4;
                else if (item.ID.Equals("stats_health3")) plusHp += 6;
                else if (item.ID.Equals("stats_insight1")) plusStateDamage += 1.2f;
                else if (item.ID.Equals("stats_insight2")) plusStateDamage += 1.4f;
                else if (item.ID.Equals("stats_insight3")) plusStateDamage += 1.6f;
                else if (item.ID.Equals("stats_aim1")) plusAim += 0.2f;
                else if (item.ID.Equals("stats_aim2")) plusAim += 0.4f;
                else if (item.ID.Equals("stats_aim3")) plusAim += 0.6f;
                else if (item.ID.Equals("stats_m_speed1")) plusMoveSpeed += 0.2f;
                else if (item.ID.Equals("stats_m_speed2")) plusMoveSpeed += 0.3f;
                else if (item.ID.Equals("stats_m_speed3")) plusMoveSpeed += 0.5f;
                else if (item.ID.Equals("stats_a_speed1")) plusAttackSpeed += 0.5f;
                else if (item.ID.Equals("stats_a_speed2")) plusAttackSpeed += 1.0f;
                else if (item.ID.Equals("stats_a_speed3")) plusAttackSpeed += 1.5f;
                else if (item.ID.Equals("stats_shield1")) curShield += 1;
                else if (item.ID.Equals("stats_shield2")) curShield += 2;
                else if (item.ID.Equals("stats_shield3")) curShield += 3;
                else if (item.ID.Equals("stats_delay1")) plusAttackRange += 0.5f;
                else if (item.ID.Equals("stats_delay2")) plusAttackRange += 0.7f;
                else if (item.ID.Equals("stats_delay3")) plusAttackRange += 1.0f;
                else if (item.ID.Equals("stats_q1")) plusQAmount += 0.2f;
                else if (item.ID.Equals("stats_q2")) plusQAmount += 0.4f;
                else if (item.ID.Equals("stats_q3")) plusQAmount += 0.5f;
            }
            if (item.Type1.Equals("Weapon"))
            {
                _ArmController.isLeft = -1;
                if (fireCoroutine != null)
                {
                    StopCoroutine(fireCoroutine);
                    fireCoroutine = null;
                }
                PlayerCombat.ps = PlayerState.Idle;

                string tempID = item.ID;
                int ttt = tempID.LastIndexOf('_');
                if (ttt != -1 && ttt == tempID.Length - 2)
                    tempID = tempID.Substring(0, tempID.Length - 2);
                int pDam = 0;
                int pCri = 0;
                int pCriD = 0;
                int pRange = 0;
                int pAim = 0;
                int pMulti = 0;
                int pShot = 0;
                int pShotD = 0;
                ManufacturesData loadData = SaveAndLoadManager.instance.LoadManufacture(tempID, PDG.Dungeon.instance.isBossRush);
                if (!loadData.id.Equals(string.Empty) || !loadData.id.Equals(""))
                {
                    for (int i = 0; i < loadData.stateid.Count; i++)
                    {
                        if (loadData.stateid[i].ToUpper().Equals("Attack".ToUpper()))
                            pDam = loadData.statevalue[i];
                        else if (loadData.stateid[i].ToUpper().Equals("Critical".ToUpper()))
                            pCri = loadData.statevalue[i];
                        else if (loadData.stateid[i].ToUpper().Equals("CriticalDamage".ToUpper()))
                            pCriD = loadData.statevalue[i];
                        else if (loadData.stateid[i].ToUpper().Equals("Range".ToUpper()))
                            pRange = loadData.statevalue[i];
                        else if (loadData.stateid[i].ToUpper().Equals("Aim".ToUpper()))
                            pAim = loadData.statevalue[i];
                        else if (loadData.stateid[i].ToUpper().Equals("MultiShoot".ToUpper()))
                            pMulti = loadData.statevalue[i];
                        else if (loadData.stateid[i].ToUpper().Equals("ShootCount".ToUpper()))
                            pShot = loadData.statevalue[i];
                        else if (loadData.stateid[i].ToUpper().Equals("ShootDelay".ToUpper()))
                            pShotD = loadData.statevalue[i];
                    }
                }

                if (item.Type3.Equals("Main"))
                {
                    if (transform.Find("GreenFatLaserBeam").gameObject.activeSelf) transform.Find("GreenFatLaserBeam").gameObject.SetActive(false);
                    if (transform.Find("GreenFatLaserEnd").gameObject.activeSelf) transform.Find("GreenFatLaserEnd").gameObject.SetActive(false);
                    if (transform.Find("GreenFatLaserStart").gameObject.activeSelf) transform.Find("GreenFatLaserStart").gameObject.SetActive(false);
                    if (weaponAnim != null && weaponAnim.runtimeAnimatorController != null) weaponAnim.SetBool("Attack", false);
                    t = 0;
                    giant_blade_comboTime = 0.0f;
                    isGiant_blade_combo = false;
                    nGiant_blade_count = 0;
                    for (int i = 0; i < ArmList.Count; i++)
                    {
                        ArmList[i].SetActive(false);
                    }
                    if (weaponAnim != null && weaponAnim.runtimeAnimatorController != null) weaponAnim.SetBool("Attack", false);
                    if (ran2 != null) 
                    {
                        for (int i = 0; i < ran2.Length; i++) ran2[i] = Random.Range(-aimAngle, aimAngle);
                    }
                    isDecrease = false;
                    AddCountNow = 0;
                    _ArmController.isLeft = -1;
                    gunControl.sr.enabled = true;
                    isArmCollision = false;
                    _ArmController.isAttack = false;

                    if (itemType.Equals("Sword"))
                    {
                        hero.WeaponsSlot = swordControl.gameObject;
                        weaponAnim = swordControl.animator;
                        if (!item.Type2.Equals("Special")) nowWeaponName = item.ID;
                        gunControl.gameObject.SetActive(false);
                        swordControl.gameObject.SetActive(true);
                    }
                    else
                    {
                        hero.WeaponsSlot = gunControl.gameObject;
                        weaponAnim = hero.WeaponsSlot.GetComponent<Animator>();
                        if (!item.Type2.Equals("Special")) nowWeaponName = item.ID;
                        swordControl.gameObject.SetActive(false);
                        gunControl.gameObject.SetActive(true);
                    }

                    if (nowWeaponName.Equals("secret_blade"))
                        SecretBladeObj = Resources.Load<GameObject>("NovaPrefabs/secret_blade");
                    else if (nowWeaponName.Equals("fire_blade"))
                        SecretBladeObj = Resources.Load<GameObject>("NovaPrefabs/fire_blade");


                    itemType = item.Type2;
                    if (item.Type2.Equals("Gun"))
                    {
                        gunControl.gameObject.SetActive(true);
                        string str = item.ID;
                        int t = str.LastIndexOf('_');
                        if (t != -1 && t == str.Length - 2)
                        {
                            str = str.Substring(0, str.Length - 2);
                        }
                        gunControl.GunChange(str);
                        if (item.ID.Contains("fanatic_dagger")) isGunFixedPos = true;
                        else isGunFixedPos = false;
                    }
                    else if (item.Type2.Equals("Arm"))
                    {
                        spreadCountLeft = item.Armspeed[0];
                        frequencyCountLeft = item.Armspeed[1];
                        string str = item.ID;
                        int t = str.LastIndexOf('_');
                        if (t != -1 && t == str.Length - 2)
                        {
                            str = str.Substring(0, str.Length - 2);
                        }
                        gunControl.ArmChange(str);
                        //armPieceSprites = Resources.LoadAll<Sprite>("Effect/Weapons/" + item.ID);
                    }
                    else if (item.Type2.Equals("Sword"))
                    {
                        swordControl.ChangeSword(item.ID);
                    }

                    if (item.Type2.Equals("Special"))
                    {
                        swordControl.gameObject.SetActive(false);
                        gunControl.gameObject.SetActive(true);
                        isSubWeapon = true;
                        if (item.ID.Equals("fireArm"))
                        {
                            gunControl.GetComponent<SpriteOutline>().Clear();
                            gunControl.sr.enabled = false;
                            FireArmObj.SetActive(true);
                            pTimeOrigin = pTime = 10.0f;
                            pTimer.gameObject.SetActive(true);
                        }
                    }
                    else
                    {
                        isSubWeapon = false;
                    }

                    isCollOpen = item.Cooltimeopen;
                    isWave = item.Armwave;
                    Inventorys.Instance.QuickSlots[1].sprite = Resources.Load<Sprite>("Item/" + item.Type1 + "/" + item.Type2 + "/" + item.ID);

                    RuntimeAnimatorController run = Resources.Load<RuntimeAnimatorController>("Item/" + item.Type1 + "/" + item.Type2 + "/" + item.ID + "/" + item.ID);
                    if (run == null)
                    {
                        Inventorys.Instance.QuickSlots[1].GetComponent<Animator>().enabled = false;
                    }
                    else
                    {
                        Inventorys.Instance.QuickSlots[1].GetComponent<Animator>().runtimeAnimatorController = run;
                        Inventorys.Instance.QuickSlots[1].GetComponent<Animator>().enabled = true;
                    }

                    maxAttackCountL = item.Maxattackcount;
                    bulletSpeed = item.Bulletspeed;
                    aimAngleOrigin = item.Aim + pAim;
                    shootCount = item.Shootcount + pShot;
                    multiShoot = item.Multishoot + pMulti;
                    bulletDelay = item.Bulletdelay;
                    knockback = item.Force;
                    bulletSize = item.Bulletsize;
                    bulletRangeOrigin = item.Range + pRange;
                    bulletName = item.Idskill;
                    fireDelayNow = fireDelayOrigin = (item.Shootdelay - ((float)pShotD / 100.0f));
                    damageOrigin = new int[item.Damage.Length];
                    for (int i = 0; i < damageOrigin.Length; i++) damageOrigin[i] = item.Damage[i];
                    for (int i = 0; i < damageOrigin.Length; i++) damageOrigin[i] += pDam;
                    damageNearOrigin = new int[item.Damagenear.Length];
                    for (int i = 0; i < damageOrigin.Length; i++) damageNearOrigin[i] = item.Damagenear[i];
                    for (int i = 0; i < damageNearOrigin.Length; i++) damageNearOrigin[i] += pDam;
                    criticalPerOrigin = item.Critical + pCri;
                    criticalDamOrigin = item.Criticaldamage + pCriD;
                    itemType = item.Type2;
                    tickTime = item.Ticktime;
                    isLeftFixed = item.Isfixed;
                    isLeftNear = item.Attacktype.Equals("M") ? true : false;

                    if (item.ID.Contains("multiple_cellstem"))
                    {
                        for (int i = 0; i < multiple_cellstemCollision.Length; i++)
                        {
                            multiple_cellstemCollision[i].hitTime = tickTime;
                        }
                    }
                    if (item.ID.Contains("electric_cellstem"))
                    {
                        for (int i = 0; i < electric_cellstemCollision.Length; i++)
                        {
                            electric_cellstemCollision[i].hitTime = tickTime;
                        }
                    }
                    
                    //Inventorys.Instance.BuffItemReSetting(item);
                    AmingLine.gameObject.SetActive(false);
                    gunControl.transform.Find("electric_cellstem_effect").gameObject.SetActive(false);
                    if (!item.Elemental.Equals(string.Empty)) left_elemnetalTime = item.Elementaltime;
                    if (item.Elemental.Equals("ICE")) left_elemetanl = ELEMENTAL_TYPE.ICE;
                    if (item.ID.Contains("dna_mo_blade"))
                    {
                        Inventorys.Instance.DefenceGuage_LB.SetActive(true);
                        Inventorys.Instance.DefenceGuage_LB.GetComponent<Image>().color = Color.white;
                        Inventorys.Instance.DefenceGuage_LB.transform.Find("Image").GetComponent<Image>().color = Color.white;
                    }
                    else if (item.ID.Contains("cold_cell_wave"))
                    {
                        Inventorys.Instance.DefenceGuage_LB.SetActive(true);
                        Inventorys.Instance.DefenceGuage_LB.GetComponent<Image>().color = Color.white;
                        Inventorys.Instance.DefenceGuage_LB.transform.Find("Image").GetComponent<Image>().color = Color.white;
                    }
                    else Inventorys.Instance.DefenceGuage_LB.SetActive(false);

                    if (item.ID.Contains("elastic_cell_weapon")) AmingLine.gameObject.SetActive(true);
                    else if (item.ID.Equals("electric_cellstem")) gunControl.transform.Find("electric_cellstem_effect").gameObject.SetActive(true);
                }
                if (item.Type3.Equals("Sub"))
                {
                    if (item.ID.Equals("energy_release") || item.ID.Equals("frozen_nova") || item.ID.Equals("frame_nova"))
                    {
                        if (isNovaReinforce)
                            item = System.Array.Find(SheetManager.Instance.ItemsDB.dataArray, reinforceItem => reinforceItem.ID.Equals(item.ID + "_2"));
                    }
                    maxAttackCountR = item.Maxattackcount;
                    nowRightWeaponName = item.ID;
                    itemTypeRight = item.Type2;
                    damageRightOrigin = new int[item.Damage.Length];
                    for (int i = 0; i < damageRightOrigin.Length; i++) damageRightOrigin[i] = item.Damage[i];
                    for (int i = 0; i < damageRightOrigin.Length; i++) damageRightOrigin[i] += pDam;
                    damageRightNearOrigin = new int[item.Damagenear.Length];
                    for (int i = 0; i < damageRightNearOrigin.Length; i++) damageRightNearOrigin[i] = item.Damagenear[i];
                    for (int i = 0; i < damageRightNearOrigin.Length; i++) damageRightNearOrigin[i] += pDam;
                    criticalRightPerOrigin = item.Critical + pCri;
                    criticalRightDamOrigin = item.Criticaldamage + pCriD;
                    bulletRightRangeOrigin = item.Range + pRange;
                    isRightFixed = item.Isfixed;
                    fireDelayRightNow = fireDelayRightOrigin = (item.Shootdelay - ((float)pShotD / 100.0f));
                    tickTimeR = item.Ticktime;
                    isRightNear = item.Attacktype.Equals("M") ? true : false;
                    if (item.Type2.Equals("Grab"))
                    {
                        spreadCountRight = item.Armspeed[0];
                        frequencyCountRight = item.Armspeed[1];
                    }
                    isWaveRight = item.Armwave;
                    Inventorys.Instance.QuickSlots[2].sprite = Resources.Load<Sprite>("Item/" + item.Type1 + "/" + item.Type2 + "/" + item.ID);
                    RuntimeAnimatorController run = Resources.Load<RuntimeAnimatorController>("Item/" + item.Type1 + "/" + item.Type2 + "/" + item.ID + "/" + item.ID);
                    if (run == null)
                    {
                        Inventorys.Instance.QuickSlots[2].GetComponent<Animator>().enabled = false;
                    }
                    else
                    {
                        Inventorys.Instance.QuickSlots[2].GetComponent<Animator>().runtimeAnimatorController = run;
                        Inventorys.Instance.QuickSlots[2].GetComponent<Animator>().enabled = true;
                    }
                    if (gunControl.transform.Find("living_arrow_weapon").GetComponent<Animator>().GetBool("isAttack"))
                        gunControl.transform.Find("living_arrow_weapon").GetComponent<Animator>().SetBool("isAttack", false);
                    if (swordControl.transform.Find("living_arrow_weapon").GetComponent<Animator>().GetBool("isAttack"))
                        swordControl.transform.Find("living_arrow_weapon").GetComponent<Animator>().SetBool("isAttack", false);
                    gunControl.transform.Find("living_arrow_weapon").gameObject.SetActive(false);

                    if (!item.Elemental.Equals(string.Empty)) right_elemnetalTime = item.Elementaltime;
                    else right_elemnetalTime = 0.0f;
                    if (item.Elemental.Equals("ICE")) right_elemetanl = ELEMENTAL_TYPE.ICE;
                    else right_elemetanl = ELEMENTAL_TYPE.NONE;

                    if (item.ID.Contains("rushing_ice")) rightCollTimeOrigin = 4.0f;
                    else if (item.ID.Contains("explosive_cell")) rightCollTimeOrigin = 1.0f;
                    else if (item.ID.Equals("dash")) rightCollTimeOrigin = 5.0f;
                    else rightCollTimeOrigin = 6.0f;

                    isRightCooltime = false;
                    Inventorys.Instance.rightCollTimeImage.fillAmount = 0;
                    Inventorys.Instance.rightCollTimeImage.transform.parent.Find("CollTimeImageP").gameObject.SetActive(false);
                    Inventorys.Instance.CollTimeText_RB.text = string.Empty;
                    gunControl.transform.Find("living_arrow_weapon").Find("multi_canon").gameObject.SetActive(false);
                    if (item.ID.Equals("cell_shield"))
                    {
                        Inventorys.Instance.DefenceGuage_RB.SetActive(true);
                        Inventorys.Instance.DefenceGuage_RB.GetComponent<Image>().color = Color.white;
                        Inventorys.Instance.DefenceGuage_RB.transform.Find("Image").GetComponent<Image>().color = Color.white;
                    }
                    else if (item.ID.Contains("living_arrow") || item.ID.Contains("secret_lasergun") || item.ID.Contains("secret_watergun") || item.ID.Contains("rocket_punch") || item.ID.Contains("multi_canon"))
                    {
                        Inventorys.Instance.DefenceGuage_RB.SetActive(true);
                        Inventorys.Instance.DefenceGuage_RB.GetComponent<Image>().color = Color.white;
                        Inventorys.Instance.DefenceGuage_RB.transform.Find("Image").GetComponent<Image>().color = Color.white;
                        if (gunControl.transform.Find("living_arrow_weapon")) gunControl.transform.Find("living_arrow_weapon").GetComponent<RightWeaponChange>().SettingAnimator(item.ID);
                        if (swordControl.transform.Find("living_arrow_weapon")) swordControl.transform.Find("living_arrow_weapon").GetComponent<RightWeaponChange>().SettingAnimator(item.ID);
                    }
                    else if (item.ID.Contains("lasergun_recycle"))
                    {
                        Inventorys.Instance.DefenceGuage_RB.SetActive(false);
                        if (gunControl.transform.Find("living_arrow_weapon")) gunControl.transform.Find("living_arrow_weapon").GetComponent<RightWeaponChange>().SettingAnimator(item.ID);
                        if (swordControl.transform.Find("living_arrow_weapon")) swordControl.transform.Find("living_arrow_weapon").GetComponent<RightWeaponChange>().SettingAnimator(item.ID);
                    }
                    else if(item.ID.Contains("energy_release") || item.ID.Contains("frozen_nova") || item.ID.Contains("frame_nova"))
                    {
                        NovaAttackObj = Resources.Load<GameObject>("NovaPrefabs/" + item.ID);
                        Inventorys.Instance.DefenceGuage_RB.SetActive(true);
                        Inventorys.Instance.DefenceGuage_RB.GetComponent<Image>().color = Color.white;
                        Inventorys.Instance.DefenceGuage_RB.transform.Find("Image").GetComponent<Image>().color = Color.white;
                    }
                    else Inventorys.Instance.DefenceGuage_RB.SetActive(false);
                }
                if (item.Type3.Equals("Bomb"))
                {
                    maxAttackCountQ = item.Maxattackcount;
                    itemTypeQ = item.Type2;

                    string str = item.ID;
                    int t = str.LastIndexOf('_');
                    if (t != -1 && t == str.Length - 2)
                    {
                        str = str.Substring(0, str.Length - 2);
                    }
                    qSkillName = str;
                    damageQOrigin = new int[item.Damage.Length];
                    for (int i = 0; i < damageQOrigin.Length; i++) damageQOrigin[i] = item.Damage[i];
                    for (int i = 0; i < damageQOrigin.Length; i++) damageQOrigin[i] += pDam;
                    tickTimeQ = item.Ticktime;
                    damageQNearOrigin = new int[item.Damagenear.Length];
                    for (int i = 0; i < damageQNearOrigin.Length; i++) damageQNearOrigin[i] = item.Damagenear[i];
                    for (int i = 0; i < damageQNearOrigin.Length; i++) damageQNearOrigin[i] += pDam;
                    criticalQPerOrigin = item.Critical +pCri;
                    criticalQDamOrigin = item.Criticaldamage+pCriD;
                    isQNear = item.Attacktype.Equals("M") ? true : false;
                    isQFixed = item.Isfixed;
                    Inventorys.Instance.QuickSlots[0].sprite = Resources.Load<Sprite>("Item/" + item.Type1 + "/" + item.Type2 + "/" + item.ID);
                    RuntimeAnimatorController run = Resources.Load<RuntimeAnimatorController>("Item/" + item.Type1 + "/" + item.Type2 + "/" + item.ID + "/" + item.ID);
                    if (run == null)
                    {
                        Inventorys.Instance.QuickSlots[0].GetComponent<Animator>().enabled = false;
                    }
                    else
                    {
                        Inventorys.Instance.QuickSlots[0].GetComponent<Animator>().runtimeAnimatorController = run;
                        Inventorys.Instance.QuickSlots[0].GetComponent<Animator>().enabled = true;
                    }

                    if (!item.Elemental.Equals(string.Empty)) q_elemnetalTime = item.Elementaltime;
                    if (item.Elemental.Equals("ICE")) q_elemetanl = ELEMENTAL_TYPE.ICE;

                    if (qSkillName.Contains("multiple_bombing"))
                        Q_CollTime = 6.0f;
                    else if (qSkillName.Equals("rolling_thorns"))
                        Q_CollTime = 20.0f;
                    else if (qSkillName.Equals("deep_impact"))
                        Q_CollTime = 1;
                    else if (qSkillName.Equals("q_change"))
                        Q_CollTime = 10;

                    Inventorys.Instance.Q_CollTimeImage.fillAmount = 0;
                    Inventorys.Instance.Q_CollTimeImage.transform.parent.Find("CollTimeImageP").gameObject.SetActive(false);
                    Inventorys.Instance.CollTimeText_Q.text = string.Empty;
                    isQ_CollTime = false;
                    Inventorys.Instance.DefenceGuage_Q.SetActive(false);
                }
            }
        }
        private void FixedUpdate()
        {
            if(!isDie)
            {
                if ((nowRightWeaponName.Contains("secret_lasergun") || nowRightWeaponName.Contains("multi_canon") ||
                    nowRightWeaponName.Contains("secret_watergun") || nowRightWeaponName.Contains("rocket_punch")) && !isSubWeapon)
                    right_defence_gauge += Time.fixedDeltaTime * 2;
                else if (nowRightWeaponName.Contains("living_arrow"))
                    right_defence_gauge += Time.fixedDeltaTime * 2;
                else if (nowRightWeaponName.Contains("energy_release") || nowRightWeaponName.Contains("frozen_nova") || nowRightWeaponName.Contains("frame_nova")) right_defence_gauge += Time.fixedDeltaTime * 1.5f;

                if (!isShield && !isRollingTransform && !hero.isTeleportUse && PlayerCombat.ps != PlayerState.Health && PlayerCombat.ps != PlayerState.Predators && PlayerCombat.ps != PlayerState.DeepImpact && !isSubWeapon && !Dialogue.Instance.isCommnet)
                {
                    if (itemTypeRight.Equals("Arm")) ArmRightAttack();
                }
            }
        }
        // Update is called once per frame
        void Update()
        {
            if (Dungeon.isEliteSpawnWait)
            {
                if (weaponAnim != null && weaponAnim.runtimeAnimatorController != null) weaponAnim.SetBool("Attack", false);
                if (transform.Find("GreenFatLaserBeam").gameObject.activeSelf) transform.Find("GreenFatLaserBeam").gameObject.SetActive(false);
                if (transform.Find("GreenFatLaserStart").gameObject.activeSelf) transform.Find("GreenFatLaserStart").gameObject.SetActive(false);
                if (transform.Find("GreenFatLaserEnd").gameObject.activeSelf) transform.Find("GreenFatLaserEnd").gameObject.SetActive(false);
                if (pTimerText.activeSelf) pTimerText.SetActive(false);
                t = 0;
                pushCheck = pushCheck2 = pushCheck3 = pushCheck4 = 0;
                for (int i = 0; i < ArmList.Count; i++) ArmList[i].SetActive(false);
                return;
            }
            if (CustomManager.instance != null && CustomManager.instance.isOpen) return;
            if (iceReTime > 0.0f)
            {
                iceReTime -= Time.deltaTime;
            }
            #region -- BUFF --
            if (buffTowerBuffTime_0 > 0.0f)
            {
                if (transform.Find("RegenerateGreen") && !transform.Find("RegenerateGreen").gameObject.activeSelf) transform.Find("RegenerateGreen").gameObject.SetActive(true);
                buffTowerBuffTime_0 -= Time.deltaTime;
            }
            else if (buffTowerBuffTime_0 <= 0.0f)
            {
                if (transform.Find("RegenerateGreen") && transform.Find("RegenerateGreen").gameObject.activeSelf) transform.Find("RegenerateGreen").gameObject.SetActive(false);
                transform.Find("buffTower_0").gameObject.SetActive(false);
            }
            if (buffTowerBuffTime_1 > 0.0f)
            {
                if (transform.Find("RegenerateRed") && !transform.Find("RegenerateRed").gameObject.activeSelf) transform.Find("RegenerateRed").gameObject.SetActive(true);
                buffTowerBuffTime_1 -= Time.deltaTime;
            }
            else if (buffTowerBuffTime_1 <= 0.0f)
            {
                if (transform.Find("RegenerateRed") && transform.Find("RegenerateRed").gameObject.activeSelf) transform.Find("RegenerateRed").gameObject.SetActive(false);
                transform.Find("buffTower_1").gameObject.SetActive(false);
            }
            if (buffTowerBuffTime_2 > 0.0f)
            {
                if (transform.Find("RegenerateBlue") && !transform.Find("RegenerateBlue").gameObject.activeSelf) transform.Find("RegenerateBlue").gameObject.SetActive(true);
                buffTowerBuffTime_2 -= Time.deltaTime;
            }
            else if (buffTowerBuffTime_2 <= 0.0f)
            {
                if (transform.Find("RegenerateBlue") && transform.Find("RegenerateBlue").gameObject.activeSelf) transform.Find("RegenerateBlue").gameObject.SetActive(false);
                transform.Find("buffTower_2").gameObject.SetActive(false);
            }
            if (buffTowerBuffTime_3 > 0.0f)
            {
                if (transform.Find("RegenerateYellow") && !transform.Find("RegenerateYellow").gameObject.activeSelf) transform.Find("RegenerateYellow").gameObject.SetActive(true);
                buffTowerBuffTime_3 -= Time.deltaTime;
            }
            else if (buffTowerBuffTime_3 <= 0.0f)
            {
                transform.Find("buffTower_3").gameObject.SetActive(false);
                if (transform.Find("RegenerateYellow") && transform.Find("RegenerateYellow").gameObject.activeSelf) transform.Find("RegenerateYellow").gameObject.SetActive(false);
            }
            if (buffTowerBuffTime_0 > 0.0f || buffTowerBuffTime_1 > 0.0f || buffTowerBuffTime_2 > 0.0f || buffTowerBuffTime_3 > 0.0f)
            {

                float t = float.MinValue;
                if (buffTowerBuffTime_0 > t) t = buffTowerBuffTime_0;
                if (buffTowerBuffTime_1 > t) t = buffTowerBuffTime_1;
                if (buffTowerBuffTime_2 > t) t = buffTowerBuffTime_2;
                if (buffTowerBuffTime_3 > t) t = buffTowerBuffTime_3;

                if (!pTimerText.activeSelf) pTimerText.SetActive(true);
                if (pTimerText.activeSelf)
                {
                    Vector3 p = transform.position;
                    p.y += 1.5f;
                    pTimerText.transform.position = UnityEngine.Camera.main.WorldToScreenPoint(p);
                    pTimerText.GetComponent<Text>().text = t.ToString("F0");
                }
            }
            else
            {
                if (pTimerText.activeSelf) pTimerText.SetActive(false);
            }
            if (isBufftowerBuff1 && buffTowerBuffTime_1 <= 0.0f)
            {
                damagePlus -= 10.0f;
                if (damagePlus < 0) damagePlus = 0;
                damagePlusNear -= 10.0f;
                if (damagePlusNear < 0) damagePlusNear = 0;
                damageQPlus -= 10.0f;
                if (damageQPlus < 0) damageQPlus = 0;
                damageQPlusNear -= 10.0f;
                if (damageQPlusNear < 0) damageQPlusNear = 0;
                damageRightPlus -= 10.0f;
                if (damageRightPlus < 0) damageRightPlus = 0;
                damageRightPlusNear -= 10.0f;
                if (damageRightPlusNear < 0) damageRightPlusNear = 0;
                isBufftowerBuff1 = false;
            }
            if (isBufftowerBuff3  && buffTowerBuffTime_3 <= 0.0f)
            {
                plusAttackSpeed -= 1.0f;
                isBufftowerBuff3 = false;
            }
            if (Dungeon.isRebuildDevice)
            {
                if (transform.Find("NovaRed") && !transform.Find("NovaRed").gameObject.activeSelf) transform.Find("NovaRed").gameObject.SetActive(true);
            }
            else
            {
                if (transform.Find("NovaRed") && transform.Find("NovaRed").gameObject.activeSelf) transform.Find("NovaRed").gameObject.SetActive(false);
            }

            if (transform.Find("BoostPadRed") && transform.Find("BoostPadRed").gameObject.activeSelf) transform.Find("BoostPadRed").gameObject.SetActive(false);
            if (fInvincibleTime_offensive_01 > 0.0f) fInvincibleTime_offensive_01 -= Time.deltaTime;
            if (fInvincibleTime_strategic_04 > 0.0f)
            {
                if (transform.Find("BoostPadRed") && !transform.Find("BoostPadRed").gameObject.activeSelf) transform.Find("BoostPadRed").gameObject.SetActive(true);
                fInvincibleTime_strategic_04 -= Time.deltaTime;
            }
            if (fPlusDamageTime_offensive01 > 0.0f)
            {
                if (transform.Find("BoostPadRed") && !transform.Find("BoostPadRed").gameObject.activeSelf) transform.Find("BoostPadRed").gameObject.SetActive(true);
                fPlusDamageTime_offensive01 -= Time.deltaTime;
            }
            else fPlusDamage_offensive01 = 0.0f;
            if (fPlusDamageTime_offensive02 > 0.0f)
            {
                if (transform.Find("BoostPadRed") && !transform.Find("BoostPadRed").gameObject.activeSelf) transform.Find("BoostPadRed").gameObject.SetActive(true);
                fPlusDamageTime_offensive02 -= Time.deltaTime;
            }
            else fPlusDamage_offensive02 = 0.0f;
            if (fPlusDamageTime_offensive04 > 0.0f) fPlusDamageTime_offensive04 -= Time.deltaTime;
            else fPlusDamage_offensive04 = 0.0f;
            if (isStrategic_02)
            {
                if (transform.Find("BoostPadRed") && !transform.Find("BoostPadRed").gameObject.activeSelf) transform.Find("BoostPadRed").gameObject.SetActive(true);
            }
            if(qSpecialTime > 0.0f)
            {
                if (!GameObject.Find("Canvas").transform.Find("UI").Find("Charge").Find("Image").Find("QChange").gameObject.activeSelf)
                {
                    GameObject.Find("Canvas").transform.Find("UI").Find("Charge").Find("Image").Find("QChange").gameObject.SetActive(true);
                    GameObject.Find("Canvas").transform.Find("UI").Find("Charge").Find("Image").Find("QChange").GetComponent<Image>().color = Color.white;
                    GameObject.Find("Canvas").transform.Find("UI").Find("Charge").Find("Image").Find("QChange").Find("QChange").GetComponent<Image>().color = Color.white;
                }
                qSpecialTime -= Time.deltaTime;
                GameObject.Find("Canvas").transform.Find("UI").Find("Charge").Find("Image").Find("QChange").Find("QChange").GetComponent<Image>().fillAmount = qSpecialTime / qSpecialTimeMAX;
                if (qSpecialTime <= 0.0f)
                {
                    if (Dungeon.instance.nQChange == 2) plusMultiShoot -= 1;
                    if (GameObject.Find("Canvas").transform.Find("UI").Find("Charge").Find("Image").Find("QChange").gameObject.activeSelf)
                    {
                        GameObject.Find("Canvas").transform.Find("UI").Find("Charge").Find("Image").Find("QChange").gameObject.SetActive(false);
                    }
                }
            }
            if (damageOrigin != null && damageOrigin.Length > 0)
            {
                damage = ((Random.Range(damageOrigin[0], damageOrigin[1] + 1) + damagePlus + (Dungeon.isRebuildDevice ? 3 : 0) + fPlusDamage_offensive02 + fPlusDamage_offensive01 + fPlusDamage_offensive04) * plusStateDamage);
                damageMin = ((damageOrigin[0] + damagePlus + (Dungeon.isRebuildDevice ? 3 : 0) + fPlusDamage_offensive02 + fPlusDamage_offensive01 + fPlusDamage_offensive04) * plusStateDamage);
                damageMax = ((damageOrigin[1] + damagePlus + (Dungeon.isRebuildDevice ? 3 : 0) + fPlusDamage_offensive02 + fPlusDamage_offensive01 + fPlusDamage_offensive04) * plusStateDamage);
                if (isLeftFixed)
                {
                    damage = (damage / 10) + Random.Range(damageOrigin[0], damageOrigin[1] + 1);
                    damageMin = (damageMin / 10) + damageOrigin[0];
                    damageMax = (damageMax / 10) + damageOrigin[1];
                }
                if (qSpecialTime > 0.0f && Dungeon.instance.nQChange == 1)
                {
                    damage *= 2;
                    damageMin *= 2;
                    damageMax *= 2;
                }
                if (damage < 1) damage = 1;
                if (damageMin < 1) damageMin = 1;
                if (damageMax < 1) damageMax = 1;
                if (isStrategic_02)
                {
                    damage *= 2;
                    damageMin *= 2;
                    damageMax *= 2;
                }
            }
            if (damageNearOrigin != null && damageNearOrigin.Length > 0)
            {
                criticalPer = criticalPerOrigin + criticalPerPlus;
                if (qSpecialTime > 0.0f && Dungeon.instance.nQChange == 4) criticalPer = 1;
                criticalDam = criticalDamOrigin + criticalDamPlus;
                if (criticalPer >= 1.0f) criticalPer = 1;
                if (GetCritical(Mathf.FloorToInt(criticalPer * 100)))
                {
                    isCritical = true;
                    damageNear = (((Random.Range(damageNearOrigin[0], damageNearOrigin[1] + 1) + damagePlusNear + (Dungeon.isRebuildDevice ? 3 : 0) + fPlusDamage_offensive02 + fPlusDamage_offensive01 + fPlusDamage_offensive04) * criticalDam) * plusStateDamage);
                    if (isLeftFixed) damageNear = (damageNear / 10) + Random.Range(damageNearOrigin[0], damageNearOrigin[1] + 1);
                    if (qSpecialTime > 0.0f && Dungeon.instance.nQChange == 1) damageNear *= 2;
                    if (qSpecialTime > 0.0f && Dungeon.instance.nQChange == 3) damageNear *= 3;
                }
                else
                {
                    isCritical = false;
                    damageNear = ((Random.Range(damageNearOrigin[0], damageNearOrigin[1] + 1) + damagePlusNear + (Dungeon.isRebuildDevice ? 3 : 0) + fPlusDamage_offensive02 + fPlusDamage_offensive01 + fPlusDamage_offensive04) * plusStateDamage);
                    if (isLeftFixed) damageNear = (damageNear / 10) + Random.Range(damageNearOrigin[0], damageNearOrigin[1] + 1);
                    if (qSpecialTime > 0.0f && Dungeon.instance.nQChange == 1) damageNear *= 2;
                }
                damageNearMin = ((damageNearOrigin[0] + damagePlusNear + (Dungeon.isRebuildDevice ? 3 : 0) + fPlusDamage_offensive02 + fPlusDamage_offensive01 + fPlusDamage_offensive04) * plusStateDamage);
                damageNearMax = ((damageNearOrigin[1] + damagePlusNear + (Dungeon.isRebuildDevice ? 3 : 0) + fPlusDamage_offensive02 + fPlusDamage_offensive01 + fPlusDamage_offensive04) * plusStateDamage);
                if (isLeftFixed)
                {
                    damageNearMin = (damageNearMin / 10) + damageNearOrigin[0];
                    damageNearMax = (damageNearMax / 10) + damageNearOrigin[1];
                }
                if (qSpecialTime > 0.0f && Dungeon.instance.nQChange == 1)
                {
                    damageNearMin *= 2;
                    damageNearMax *= 2;
                }
                if (damageNear < 1) damageNear = 1;
                if (damageNearMin < 1) damageNearMin = 1;
                if (damageNearMax < 1) damageNearMax = 1;
                if (isStrategic_02)
                {
                    damageNear *= 2;
                    damageNearMin *= 2;
                    damageNearMax *= 2;
                }
            }
            if (damageRightOrigin != null && damageRightOrigin.Length > 0)
            {
                damageRight = ((Random.Range(damageRightOrigin[0], damageRightOrigin[1] + 1) + damageRightPlus + (Dungeon.isRebuildDevice ? 3 : 0) + fPlusDamage_offensive02 + fPlusDamage_offensive01 + fPlusDamage_offensive04) * plusStateDamage);
                damageRightMin = ((damageRightOrigin[0] + damageRightPlus + (Dungeon.isRebuildDevice ? 3 : 0) + fPlusDamage_offensive02 + fPlusDamage_offensive01 + fPlusDamage_offensive04) * plusStateDamage);
                damageRightMax = ((damageRightOrigin[1] + damageRightPlus + (Dungeon.isRebuildDevice ? 3 : 0) + fPlusDamage_offensive02 + fPlusDamage_offensive01 + fPlusDamage_offensive04) * plusStateDamage);
                if (isRightFixed)
                {
                    damageRight = (damageRight / 10) + Random.Range(damageRightOrigin[0], damageRightOrigin[1] + 1);
                    damageRightMin = (damageRightMin / 10) + damageRightOrigin[0];
                    damageRightMax = (damageRightMax / 10) + damageRightOrigin[1];
                }
                if (qSpecialTime > 0.0f && Dungeon.instance.nQChange == 1)
                {
                    damageRight *= 2;
                    damageRightMin *= 2;
                    damageRightMax *= 2;
                }
                if (damageRight < 1) damageRight = 1;
                if (damageRightMin < 1) damageRightMin = 1;
                if (damageRightMax < 1) damageRightMax = 1;
                if (isStrategic_02)
                {
                    damageRight *= 2;
                    damageRightMin *= 2;
                    damageRightMax *= 2;
                }
            }
            if (damageRightNearOrigin != null && damageRightNearOrigin.Length > 0)
            {
                criticalRightPer = criticalRightPerOrigin + criticalRightPerPlus;
                if (qSpecialTime > 0.0f && Dungeon.instance.nQChange == 4) criticalRightPer = 1;
                criticalRightDam = criticalRightDamOrigin + criticalRightDamPlus;
                if (criticalRightPer >= 1.0f) criticalRightPer = 1;
                if (GetCritical(Mathf.FloorToInt(criticalRightPer * 100)))
                {
                    isCriticalRight = true;
                    damageRightNear = (((Random.Range(damageRightNearOrigin[0], damageRightNearOrigin[1] + 1) + damageRightPlusNear + (Dungeon.isRebuildDevice ? 3 : 0) + fPlusDamage_offensive02 + fPlusDamage_offensive01 + fPlusDamage_offensive04) * criticalRightDam) * plusStateDamage);
                    if (isRightFixed) damageRightNear = (damageRightNear / 10) + Random.Range(damageRightOrigin[0], damageRightOrigin[1] + 1);
                    if (qSpecialTime > 0.0f && Dungeon.instance.nQChange == 1) damageRightNear *= 2;
                    if (qSpecialTime > 0.0f && Dungeon.instance.nQChange == 3) damageRightNear *= 3;
                }
                else
                {
                    isCriticalRight = false;
                    damageRightNear = ((Random.Range(damageRightNearOrigin[0], damageRightNearOrigin[1] + 1) + damageRightPlusNear + (Dungeon.isRebuildDevice ? 3 : 0) + fPlusDamage_offensive02 + fPlusDamage_offensive01 + fPlusDamage_offensive04) * plusStateDamage);
                    if (isRightFixed) damageRightNear = (damageRightNear / 10) + Random.Range(damageRightNearOrigin[0], damageRightNearOrigin[1] + 1);
                    if (qSpecialTime > 0.0f && Dungeon.instance.nQChange == 1) damageRightNear *= 2;
                }
                damageNearRightMin = ((damageRightNearOrigin[0] + damageRightPlusNear + (Dungeon.isRebuildDevice ? 3 : 0) + fPlusDamage_offensive02 + fPlusDamage_offensive01 + fPlusDamage_offensive04) * plusStateDamage);
                damageNearRightMax = ((damageRightNearOrigin[1] + damageRightPlusNear + (Dungeon.isRebuildDevice ? 3 : 0) + fPlusDamage_offensive02 + fPlusDamage_offensive01 + fPlusDamage_offensive04) * plusStateDamage);
                if (isRightFixed)
                {
                    damageNearRightMin = (damageNearRightMin / 10) + damageRightNearOrigin[0];
                    damageNearRightMax = (damageNearRightMax / 10) + damageRightNearOrigin[1];
                }
                if (qSpecialTime > 0.0f && Dungeon.instance.nQChange == 1)
                {
                    damageNearRightMin *= 2;
                    damageNearRightMax *= 2;
                }
                if (damageRightNear < 1) damageRightNear = 1;
                if (damageNearRightMin < 1) damageNearRightMin = 1;
                if (damageNearRightMax < 1) damageNearRightMax = 1;
                if (isStrategic_02)
                {
                    damageRightNear *= 2;
                    damageNearRightMin *= 2;
                    damageNearRightMax *= 2;
                }
            }
            if (damageQOrigin != null && damageQOrigin.Length > 0)
            {
                damageQ = ((Random.Range(damageQOrigin[0], damageQOrigin[1] + 1) + damageQPlus + (Dungeon.isRebuildDevice ? 3 : 0) + fPlusDamage_offensive02 + fPlusDamage_offensive01 + fPlusDamage_offensive04) * plusStateDamage);
                damageQMin = ((damageQOrigin[0] + damageQPlus + (Dungeon.isRebuildDevice ? 3 : 0) + fPlusDamage_offensive02 + fPlusDamage_offensive01 + fPlusDamage_offensive04) * plusStateDamage);
                damageQMax = ((damageQOrigin[1] + damageQPlus + (Dungeon.isRebuildDevice ? 3 : 0) + fPlusDamage_offensive02 + fPlusDamage_offensive01 + fPlusDamage_offensive04) * plusStateDamage);
                if (isQFixed)
                {
                    damageQ = (damageQ / 10) + Random.Range(damageQOrigin[0], damageQOrigin[1] + 1);
                    damageQMin = (damageQMin / 10) + damageQOrigin[0];
                    damageQMax = (damageQMax / 10) + damageQOrigin[1];
                }
                if (qSpecialTime > 0.0f && Dungeon.instance.nQChange == 1)
                {
                    damageQ *= 2;
                    damageQMin *= 2;
                    damageQMax *= 2;
                }
                if (damageQ < 1) damageQ = 1;
                if (damageQMin < 1) damageQMin = 1;
                if (damageQMax < 1) damageQMax = 1;
                if (isStrategic_02)
                {
                    damageQ *= 2;
                    damageQMin *= 2;
                    damageQMax *= 2;
                }
            }
            if (damageQNearOrigin != null && damageQNearOrigin.Length > 0)
            {
                criticalQPer = criticalPerOrigin + criticalPerPlus;
                if (qSpecialTime > 0.0f && Dungeon.instance.nQChange == 4) criticalQPer = 1;
                criticalQDam = criticalQDamOrigin + criticalQDamPlus;
                if (criticalQPer >= 1.0f) criticalQPer = 1;
                if (GetCritical(Mathf.FloorToInt(criticalQPer * 100)))
                {
                    isCriticalQ = true;
                    damageQNear = (((Random.Range(damageQNearOrigin[0], damageQNearOrigin[1] + 1) + damageQPlusNear + (Dungeon.isRebuildDevice ? 3 : 0) + fPlusDamage_offensive02 + fPlusDamage_offensive01 + fPlusDamage_offensive04) * criticalQDam) * plusStateDamage);
                    if (isQFixed) damageQNear = (damageQNear / 10) + Random.Range(damageQNearOrigin[0], damageQNearOrigin[1] + 1);
                    if (qSpecialTime > 0.0f && Dungeon.instance.nQChange == 1) damageQNear *= 2;
                    if (qSpecialTime > 0.0f && Dungeon.instance.nQChange == 3) damageQNear *= 3;
                }
                else
                {
                    isCriticalQ = false;
                    damageQNear = ((Random.Range(damageQNearOrigin[0], damageQNearOrigin[1] + 1) + damageQPlusNear + (Dungeon.isRebuildDevice ? 3 : 0) + fPlusDamage_offensive02 + fPlusDamage_offensive01 + fPlusDamage_offensive04) * plusStateDamage);
                    if (isQFixed) damageQNear = (damageQNear / 10) + Random.Range(damageQNearOrigin[0], damageQNearOrigin[1] + 1);
                    if (qSpecialTime > 0.0f && Dungeon.instance.nQChange == 1) damageQNear *= 2;
                }
                damageNearQMin = ((damageQNearOrigin[0] + damageQPlusNear + (Dungeon.isRebuildDevice ? 3 : 0) + fPlusDamage_offensive02 + fPlusDamage_offensive01 + fPlusDamage_offensive04) * plusStateDamage);
                damageNearQMax = ((damageQNearOrigin[1] + damageQPlusNear + (Dungeon.isRebuildDevice ? 3 : 0) + fPlusDamage_offensive02 + fPlusDamage_offensive01 + fPlusDamage_offensive04) * plusStateDamage);
                if (isQFixed)
                {
                    damageNearQMin = (damageNearQMin / 10) + damageQNearOrigin[0];
                    damageNearQMax = (damageNearQMax / 10) + damageQNearOrigin[1];
                }
                if (qSpecialTime > 0.0f && Dungeon.instance.nQChange == 1)
                {
                    damageNearQMin *= 2;
                    damageNearQMax *= 2;
                }
                if (damageQNear < 1) damageQNear = 1;
                if (damageNearQMin < 1) damageNearQMin = 1;
                if (damageNearQMax < 1) damageNearQMax = 1;
                if (isStrategic_02)
                {
                    damageQNear *= 2;
                    damageNearQMin *= 2;
                    damageNearQMax *= 2;
                }
            }
            if (nBuff_offensive03_count >= nBuff_offensive03_countMax)
            {
                nBuff_offensive03_count = 0;
                transform.Find("HealMiniGreen").gameObject.SetActive(true);
                transform.Find("HealMiniGreen").GetComponent<ParticleSystem>().Play(true);
                HpRecovery(1);
            }

            aimAngle = aimAngleOrigin - (aimAngleOrigin * plusAim);
            if (aimAngle < 0) aimAngle = 0;

            bulletRange = bulletRangeOrigin + (bulletRangeOrigin * plusAttackRange);
            bulletRangeRight = bulletRightRangeOrigin + (bulletRightRangeOrigin * plusAttackRange);
            if (bulletRange > 25) bulletRange = 25;
            if (bulletRangeRight > 25) bulletRangeRight = 25;

            fireDelay = fireDelayOrigin / (1 + plusAttackSpeed);
            if (fireDelay < 0) fireDelay = 0;

            fireDelayRight = fireDelayRightOrigin / (1 + plusAttackSpeed);
            if (fireDelayRight < 0) fireDelayRight = 0;

            rightCollTime = rightCollTimeOrigin - (rightCollTime * plusCollTime);
            if (rightCollTime < 0) rightCollTime = 0;

            shieldTime += Time.deltaTime;
            if(isDeath_Level_3)
            {
                if(shieldTime > 60 && isDeath_Level_3_Count < 2)
                {
                    shieldTime = 0.0f;
                    ShieldRecovery(1);
                    isDeath_Level_3_Count++;
                }
            }


            if(isFreeze2)
            {
                FreezeTime -= Time.deltaTime;
                GetComponent<SpriteRenderer>().color = new Color32(0, 100, 255, 255);
                if (FreezeTime <= 0.0f)
                {
                    isFreeze2 = false;
                    GetComponent<SpriteRenderer>().color = new Color32(255, 255, 255, 255);
                }
            }
            if (isResurrection) dieTime -= Time.deltaTime;
            if (isResurrection && dieTime <= 0.0f && PlayerCombat.ps != PlayerState.Health)
            {
                SoundManager.instance.StartAudio(new string[1] { "Object/healthtower" }, VOLUME_TYPE.AMBIENT);
                GetComponent<Animator>().SetBool("Die", false);
                dieTime = 2.0f;
                PlayerCombat.ps = PlayerState.Health;
            }
            if (isResurrection && dieTime <= 0.0f && PlayerCombat.ps == PlayerState.Health)
            {
                isResurrection = false;
                PlayerCombat.ps = PlayerState.Idle;
                HpRecovery(1);
                SoundManager.instance.StopAudio(new string[1] { "Object/healthtower" });
                SoundManager.instance.StartAudio(new string[1] { "Character/QSkill" }, VOLUME_TYPE.EFFECT);
                isUseQ = true;
                Qskill.gameObject.SetActive(true);
                Qskill.StartQ("spiny_cell_firing");
                StartCoroutine(QShield());

                GameObject obj2 = ObjManager.Call().GetObject("Hit_sprite7");
                obj2.transform.position = transform.position;
                obj2.transform.localScale = Vector3.one;
                obj2.transform.localEulerAngles = Vector3.zero;
                obj2.GetComponent<SwordHitEffectController>()._effctName = "qSkill";
                obj2.GetComponent<SwordHitEffectController>().animSpeed = 1.0f;
                obj2.GetComponent<Animator>().Rebind();
                obj2.SetActive(true);
                StartCoroutine(HitAction());
            }
            #endregion
            if (isHit && !isDie)
            {
                if (isDeath_Level_1) hitTIme -= Time.deltaTime / 2;
                else hitTIme -= Time.deltaTime;
                if (hitTIme <= 0.0f) isHit = false;
            }
            if (isDie)
            {
                AmingLine.gameObject.SetActive(false);
                bulletTargetObj.SetActive(false);
            }
            else
            {
                if (rightInTime > 0.0f) rightInTime -= Time.deltaTime;
                if (AmingLine.gameObject.activeSelf)
                {
                    Vector3 pp = UnityEngine.Camera.main.ScreenToWorldPoint(Input.mousePosition);
                    pp.z = 0.0f;
                    Vector3 _dir = pp - transform.position;
                    _dir.Normalize();
                    RaycastHit2D hit = Physics2D.Raycast(transform.position, _dir, bulletRange, 1 << 8 | 1 << 23 | 1 << 12 | 1 << 24);

                    Vector3 ePos = transform.position + (_dir * bulletRange);
                    if (hit) ePos = hit.point;

                    AmingLine.SetPosition(0, transform.position + (_dir));
                    AmingLine.SetPosition(1, ePos);
                }
                if (bulletTargetObj.activeSelf)
                {
                    Vector3 p_0 = transform.position;
                    p_0.y += 0.5f;
                    Vector3 p = UnityEngine.Camera.main.WorldToScreenPoint(p_0);
                    bulletTargetObj.transform.position = p;
                    bulletTargetX += (Time.deltaTime * 72) / fireDelay;
                    if (bulletTargetX > 36.0f)
                    {
                        bulletTargetObj.SetActive(false);
                    }
                    bulletTargetInLineObj.GetComponent<RectTransform>().anchoredPosition3D = new Vector3(bulletTargetX, 0.0f, 0);

                    if ((PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ATTACK], true) || Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_ATTACK], true)) && nowWeaponName.Contains("elastic_cell_weapon") && isSpecialAttack) 
                    {
                        isSpecialAttack = false;
                        if (bulletTargetX > -9.0f && bulletTargetX < 6.0f)
                        {
                            TextOpenController.instance.ShowText("GOOD!", Color.white, 24);
                            SpecialFire();
                        }
                        else
                        {
                            TextOpenController.instance.ShowText("MISS!", Color.red, 24);
                        }
                    }
                }
                volatileBuffTimeNow += Time.deltaTime;
                volatileBuffTimeColl -= Time.deltaTime;
                if (volatileBuffTimeColl <= 0.0f)
                {
                    volatileBuffObj.SetActive(false);
                    volatileBuffType = 0;
                }
                guideDelayNow += Time.deltaTime;
                guideDelay = Time.deltaTime * 10;
                #region -- Arm Group Layer --
                if (PlayerCombat.pl == PlayerLookDirection.Up || PlayerCombat.pl == PlayerLookDirection.UpLeft || PlayerCombat.pl == PlayerLookDirection.UpRight) ArmParent.GetComponent<SortingGroup>().sortingOrder = 2;
                else ArmParent.GetComponent<SortingGroup>().sortingOrder = 4;
                #endregion
                if ((ResultPanelScript.instance.Q_SkillCount > 0 || qSpecialTime > 0) && !hero.isTeleportUse && PlayerCombat.ps != PlayerState.Avoid )
                {
                    Q_SkillBodyEffect.SetActive(true);
                    if (qSpecialTime > 0)
                    {
                        if (Dungeon.instance.nQChange == 1)
                        {
                            Q_SkillBodyEffect.transform.Find("RoundFireBlue").gameObject.SetActive(false);
                            Q_SkillBodyEffect.transform.Find("RoundFireRed").gameObject.SetActive(true);
                            Q_SkillBodyEffect.transform.Find("RoundFireGreen").gameObject.SetActive(false);
                            Q_SkillBodyEffect.transform.Find("RoundFirePurple").gameObject.SetActive(false);
                            Q_SkillBodyEffect.transform.Find("RoundFireYellow").gameObject.SetActive(false);
                            //Q_SkillBodyEffect.transform.Find("RoundFireRed").GetComponent<ParticleSystem>().Play();
                        }
                        if (Dungeon.instance.nQChange == 2)
                        {
                            Q_SkillBodyEffect.transform.Find("RoundFireBlue").gameObject.SetActive(false);
                            Q_SkillBodyEffect.transform.Find("RoundFireRed").gameObject.SetActive(false);
                            Q_SkillBodyEffect.transform.Find("RoundFireGreen").gameObject.SetActive(true);
                            Q_SkillBodyEffect.transform.Find("RoundFirePurple").gameObject.SetActive(false);
                            Q_SkillBodyEffect.transform.Find("RoundFireYellow").gameObject.SetActive(false);
                            //Q_SkillBodyEffect.transform.Find("RoundFireGreen").GetComponent<ParticleSystem>().Play();
                        }
                        if (Dungeon.instance.nQChange == 3)
                        {
                            Q_SkillBodyEffect.transform.Find("RoundFireBlue").gameObject.SetActive(false);
                            Q_SkillBodyEffect.transform.Find("RoundFireRed").gameObject.SetActive(false);
                            Q_SkillBodyEffect.transform.Find("RoundFireGreen").gameObject.SetActive(false);
                            Q_SkillBodyEffect.transform.Find("RoundFirePurple").gameObject.SetActive(true);
                            Q_SkillBodyEffect.transform.Find("RoundFireYellow").gameObject.SetActive(false);
                            //Q_SkillBodyEffect.transform.Find("RoundFirePurple").GetComponent<ParticleSystem>().Play();
                        }
                        if (Dungeon.instance.nQChange == 4)
                        {
                            Q_SkillBodyEffect.transform.Find("RoundFireBlue").gameObject.SetActive(false);
                            Q_SkillBodyEffect.transform.Find("RoundFireRed").gameObject.SetActive(false);
                            Q_SkillBodyEffect.transform.Find("RoundFireGreen").gameObject.SetActive(false);
                            Q_SkillBodyEffect.transform.Find("RoundFirePurple").gameObject.SetActive(false);
                            Q_SkillBodyEffect.transform.Find("RoundFireYellow").gameObject.SetActive(true);
                            //Q_SkillBodyEffect.transform.Find("RoundFireYellow").GetComponent<ParticleSystem>().Play();
                        }
                    }
                    else
                    {
                        Q_SkillBodyEffect.transform.Find("RoundFireRed").gameObject.SetActive(false);
                        Q_SkillBodyEffect.transform.Find("RoundFireGreen").gameObject.SetActive(false);
                        Q_SkillBodyEffect.transform.Find("RoundFirePurple").gameObject.SetActive(false);
                        Q_SkillBodyEffect.transform.Find("RoundFireYellow").gameObject.SetActive(false);
                        Q_SkillBodyEffect.transform.Find("RoundFireBlue").gameObject.SetActive(true);
                        //Q_SkillBodyEffect.transform.Find("RoundFireBlue").GetComponent<ParticleSystem>().Play();
                    }
                }
                else Q_SkillBodyEffect.SetActive(false);
                if (rollingTime > 0.0f)
                {
                    rollingTime -= Time.deltaTime;

                }
                if (isRollingTransform && rollingTime < 0.0f)
                {
                    rollingTime = 0.0f;
                    StartCoroutine(TransformSpake(false));
                }
                if(isQ_CollTime)
                {
                    int c = Mathf.FloorToInt(Q_CollTime - Q_CollTimeNow);
                    if (c > 0) Inventorys.Instance.CollTimeText_Q.text = c.ToString();
                    else Inventorys.Instance.CollTimeText_Q.text = string.Empty;
                    Q_CollTimeNow += Time.deltaTime;
                    if (Q_CollTimeNow >= Q_CollTime) 
                    {
                        isQ_CollTime = false;
                        Inventorys.Instance.Q_CollTimeImage.transform.parent.Find("CollTimeImageP").gameObject.SetActive(false);
                        Inventorys.Instance.Q_CollTimeAnim.Rebind();
                    }
                    Inventorys.Instance.Q_CollTimeImage.fillAmount = 1.0f - (Q_CollTimeNow / Q_CollTime);
                }
                if (pTimer.activeSelf)
                {
                    FireArmObj.GetComponent<SpriteRenderer>().flipX = gunControl.sr.flipX;
                    FireArmObj.GetComponent<SpriteRenderer>().flipY = gunControl.sr.flipY;
                    Vector3 p = transform.position;
                    p.x -= 0.05f;
                    p.y += 0.8f;
                    pTimer.transform.position = UnityEngine.Camera.main.WorldToScreenPoint(p);

                    float timerFillAmount = 1.0f - ((pTimeOrigin - pTime) / pTimeOrigin);
                    pTimer.transform.Find("Image").GetComponent<Image>().fillAmount = timerFillAmount;

                    if(Input.GetKeyDown(KeyCode.C))
                    {
                        isSubWeapon = false;
                    }

                    if (timerFillAmount <= 0.0f || !isSubWeapon)
                    {
                        gunControl.sr.enabled = true;
                        FireArmObj.SetActive(false);
                        pTimer.SetActive(false);
                        ItemsData iData = System.Array.Find(SheetManager.Instance.ItemsDB.dataArray, item => item.ID.Equals(nowWeaponName));
                        SettingItem(iData);
                    }
                }
                if (nowWeaponName.Contains("cold_cell_wave") && !isSubWeapon) left_defence_gauge += Time.deltaTime * 2;
                else left_defence_gauge += Time.deltaTime;
                if ((nowRightWeaponName.Contains("living_arrow") || nowRightWeaponName.Contains("secret_lasergun") || nowRightWeaponName.Contains("multi_canon") ||
                    nowRightWeaponName.Contains("secret_watergun") || nowRightWeaponName.Contains("energy_release") ||
                    nowRightWeaponName.Contains("frozen_nova") || nowRightWeaponName.Contains("frame_nova") ||
                    nowRightWeaponName.Contains("rocket_punch")) && !isSubWeapon) ;
                else right_defence_gauge += Time.deltaTime;
                q_defence_gauge += Time.deltaTime;
                if (left_defence_gauge > left_defence_gauge_max) left_defence_gauge = left_defence_gauge_max;
                if (right_defence_gauge > right_defence_gauge_max) right_defence_gauge = right_defence_gauge_max;
                if (q_defence_gauge > q_defence_gauge_max) q_defence_gauge = q_defence_gauge_max;
                Inventorys.Instance.DefenceGuage_LB.transform.Find("Image").GetComponent<Image>().fillAmount = left_defence_gauge / left_defence_gauge_max;
                Inventorys.Instance.DefenceGuage_RB.transform.Find("Image").GetComponent<Image>().fillAmount = right_defence_gauge / right_defence_gauge_max;
                Inventorys.Instance.DefenceGuage_Q.transform.Find("Image").GetComponent<Image>().fillAmount = q_defence_gauge / q_defence_gauge_max;

                fireDelayNow += Time.deltaTime;
                fireDelayRightNow += Time.deltaTime;
                armDelayNow += Time.deltaTime;
                armDelayNow2 += Time.deltaTime;
                leftColltimeNow += Time.deltaTime;

                if (nowRightWeaponName.Contains("wind_cutter"))
                {
                    for (int i = 0; i < RightComboGlass.Length; i++) RightComboGlass[i].gameObject.SetActive(false);
                    for (int i = 0; i < 3 - rightCombo_count; i++)
                    {
                        RightComboGlass[i].color = Color.white;
                        RightComboGlass[i].gameObject.SetActive(true);
                    }
                }
                else
                {
                    for (int i = 0; i < RightComboGlass.Length; i++) RightComboGlass[i].gameObject.SetActive(false);
                }
                if (nowRightWeaponName.Contains("wind_cutter") && isRightCombo)
                {
                    rightCombo_time += Time.deltaTime;
                    if (rightCombo_count >= 3)
                    {
                        rightCollTimeNow = 0.0f;
                        isRightCombo = false;
                        rightCombo_count = 0;
                        isRightCooltime = true;
                        Inventorys.Instance.rightCollTimeImage.transform.parent.Find("CollTimeImageP").gameObject.SetActive(true);
                        if (itemType.Equals("Gun") || itemType.Equals("Arm"))
                            hero.WeaponsSlot.GetComponent<SpriteRenderer>().enabled = true;
                        else
                        {
                            if (hero.WeaponsSlot.transform.GetChild(0).GetComponent<SpriteRenderer>())
                                hero.WeaponsSlot.transform.GetChild(0).GetComponent<SpriteRenderer>().enabled = true;
                        }
                    }
                }
                if (nowWeaponName.Contains("giant_blade") && isGiant_blade_combo && !isSubWeapon)
                {
                    giant_blade_comboTime += Time.deltaTime;
                    if (giant_blade_comboTime > 0.5f)
                    {
                        isGiant_blade_combo = false;
                        nGiant_blade_count = 0;
                    }
                }
                if (nowWeaponName.Equals("electric_cellstem") && isGiant_blade_combo && !isSubWeapon)
                {
                    giant_blade_comboTime += Time.deltaTime;
                    if (giant_blade_comboTime > 0.5f)
                    {
                        isGiant_blade_combo = false;
                        nGiant_blade_count = 0;
                    }
                }
                if (isRightCooltime)
                {
                    int c = Mathf.FloorToInt(rightCollTime - rightCollTimeNow);
                    if (c > 0) Inventorys.Instance.CollTimeText_RB.text = c.ToString();
                    else Inventorys.Instance.CollTimeText_RB.text = string.Empty;
                    rightCollTimeNow += Time.deltaTime;
                    if (rightCollTimeNow >= rightCollTime)
                    {
                        if (nowRightWeaponName.Contains("wind_cutter"))
                        {
                            if (rightCombo_count == 0)
                            {
                                isRightCooltime = false;
                                Inventorys.Instance.rightCollTimeImage.transform.parent.Find("CollTimeImageP").gameObject.SetActive(false);
                                Inventorys.Instance.rightCollTImeAnim.Rebind();
                            }
                        }
                        else
                        {
                            Inventorys.Instance.rightCollTimeImage.transform.parent.Find("CollTimeImageP").gameObject.SetActive(false);
                            isRightCooltime = false;
                            Inventorys.Instance.rightCollTImeAnim.Rebind();
                        }
                    }
                    Inventorys.Instance.rightCollTimeImage.fillAmount = 1.0f - (rightCollTimeNow / rightCollTime);
                }
                if (!nowWeaponName.Contains("biological_cannon")) gunControl.EneryChargeEnd();
                if (isMiniGame) return;
                if (!isShield && !isRollingTransform && !hero.isTeleportUse && PlayerCombat.ps != PlayerState.Health && PlayerCombat.ps != PlayerState.Predators && PlayerCombat.ps != PlayerState.DeepImpact && !isSubWeapon && !Dialogue.Instance.isCommnet)
                {
                    if (itemType.Equals("Arm")) Sting();
                    if (itemType.Equals("Gun")) GunFire();
                    if (itemType.Equals("Sword")) SwordAttack();
                    if (itemTypeRight.Equals("Grab")) Grab();
                    if (itemTypeRight.Equals("Sword")) SwordAttackRight();
                    if (itemTypeRight.Equals("Gun")) GunFireRight();
                    if (itemTypeRight.Equals("Spawn")) SpawnRight();
                    if (itemTypeRight.Equals("Dash")) DashAttack();
                    if ((PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ATTACK], false) || Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_ATTACK], false)) && hero.isWeaponMode && !isFreeze && !hero.animator.GetBool("Avoid") && !isUseQ && !UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject())
                    {
                        if (isGuideArrow || buffTowerBuffTime_0 > 0.0f) GuideArrowFire();
                    }
                }
                else if(Dialogue.Instance.isCommnet)
                {
                    if (weaponAnim != null && weaponAnim.runtimeAnimatorController != null) weaponAnim.SetBool("Attack", false);
                    if (transform.Find("GreenFatLaserBeam").gameObject.activeSelf) transform.Find("GreenFatLaserBeam").gameObject.SetActive(false);
                    if (transform.Find("GreenFatLaserStart").gameObject.activeSelf) transform.Find("GreenFatLaserStart").gameObject.SetActive(false);
                    if (transform.Find("GreenFatLaserEnd").gameObject.activeSelf) transform.Find("GreenFatLaserEnd").gameObject.SetActive(false);
                    for (int i = 0; i < ArmList.Count; i++) ArmList[i].SetActive(false);
                }
                else if (isSubWeapon)
                {
                    if ((PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ATTACK], false) || Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_ATTACK], false)) && hero.isWeaponMode && pTime >= 0.0f && !isFreeze && !hero.animator.GetBool("Avoid") && !isUseQ && !UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject()) 
                    {
                        pushCheck++;
                        pTime -= Time.deltaTime;
                        pSpawnTime += Time.deltaTime;
                    }
                    if (pushCheck == pushCheck2)
                    {
                        pushCheck = pushCheck2 = 0;
                    }
                    else
                    {
                        if (pSpawnTime >= 0.5f)
                        {
                            pSpawnTime = 0.0f;
                            GameObject obj = Instantiate(FireArmAttackPrefabs);
                            obj.transform.position = FireArmAttackObj.transform.position;
                            obj.transform.GetComponent<PlayerAttackController>().hitTime = tickTime;
                            obj.transform.GetComponent<PlayerAttackController>().vDir = (GetPosition(transform.position, gun_simulation.rot) - transform.position).normalized;
                            obj.transform.GetComponent<PlayerAttackController>().plusAngle = gun_simulation.rot;
                            obj.transform.eulerAngles = new Vector3(-gun_simulation.rot, 90, 0);
                            Destroy(obj, 5.0f);
                        }
                    }
                    pushCheck2 = pushCheck;
                }
                if (hero.isTeleportUse)
                {
                    ShieldObj.SetActive(false);
                    hero.WeaponsSlot.SetActive(false);
                }
                else if (isShield) 
                {
                    hero.WeaponsSlot.SetActive(false);
                }
                if (!itemTypeQ.Equals(string.Empty) && !isRollingTransform) UseQskill();
                if (itemTypeRight.Equals("Shield") && !isRollingTransform) Shield();
                if (_ArmController.isGrab && !isRollingTransform)
                {
                    if (!grapGuage.activeSelf) grapGuage.SetActive(true);

                    if (grapGuage.activeSelf)
                    {
                        Vector3 pos = transform.position;
                        pos.y += 1.0f;
                        grapGuage.transform.position = UnityEngine.Camera.main.WorldToScreenPoint(pos);
                        grapGuage.transform.Find("Guage").GetComponent<Image>().fillAmount = grabTime / 3f;
                    }
                    grabTime -= Time.deltaTime;
                    if (grabTime <= 0.0f)
                    {
                        grapGuage.SetActive(false);
                        _ArmController.grabMonster.UnGrab2();
                        _ArmController.grabMonster = null;
                        _ArmController.isGrab = false;
                        grabTime = 5.0f;
                    }
                }
                if (curShield <= 0 && helmetObj.activeSelf)
                {
                    if(Dungeon.headType.Equals("painkiller") && !Dungeon.isSelectHead)
                    {
                        helmetObj.SetActive(false);
                        Inventorys.Instance.DeleteBuff("painkiller");
                    }
                }
            }
        }
        private void LateUpdate()
        {
            if (PlayerCombat.ps != PlayerState.Swing) helmetObj.GetComponent<SpriteRenderer>().flipX = GetComponent<SpriteRenderer>().flipX;
        }
        private void DashAttack()
        {
            if ((PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ATTACK2], false) || Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_ATTACK2], false)) && hero.isWeaponMode && !isArmCollision && !_ArmController.isGrab && !isUseQ && !hero.animator.GetBool("Avoid") &&
                !UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject() && !isRightCooltime && !isFreeze)
            {
                Inventorys.Instance.rightCollTimeImage.transform.parent.Find("CollTimeImageP").gameObject.SetActive(true);
                isRightCooltime = true;
                rightCollTimeNow = 0.0f;
                StartCoroutine(Dasing());
            }
        }
        IEnumerator Dasing()
        {
            GameObject ingObj = Instantiate(DasingEffect);
            GameObject startEffect = Instantiate(DashStartEffect);
            startEffect.transform.position = transform.position;
            Destroy(startEffect, 2.0f);
            float fff = 0.0f;
            Vector3 firePos = transform.position;
            Vector3 crossPos = GetPosition(firePos, gun_simulation.rot);
            firePos.z = crossPos.z = 0;
            Vector3 dir = crossPos - firePos;
            dir.Normalize();
            fInvincibleTime_offensive_01 = 0.5f;
            isDash = true;
            while (true)
            {
                RaycastHit2D hit = Physics2D.Raycast(transform.position, dir, 1, 1 << 8);
                if (hit) break;
                transform.Translate(dir * 25 * Time.deltaTime);
                Vector3 p = transform.position;
                ingObj.transform.position = p + dir;
                RaycastHit2D[] hits = Physics2D.CapsuleCastAll(transform.position + new Vector3(-0.2f, 0), new Vector2(0.5f, 3), CapsuleDirection2D.Vertical, GetAngle(transform.position, transform.position + dir), Vector2.zero, 0, 1 << 12);
                if (hits.Length > 0)
                {
                    for (int i = 0; i < hits.Length; i++)
                    {
                        if (hits[i].collider.GetComponent<MOB.Monster>())
                        {
                            Vector3 dir2 = transform.position - (Vector3)hits[i].point;
                            dir2.Normalize();
                            hits[i].collider.GetComponent<MOB.Monster>().SetDamage((isRightNear ? damageRightNear : damageRight), dir2, 5, ELEMENTAL_TYPE.NONE, 0.0f);
                            if (isRightNear && isCriticalRight)
                            {
                                GameObject criEffect = ObjManager.Call().GetObject("CriticalHitEffect");
                                criEffect.transform.position = hits[i].point;
                                criEffect.SetActive(true);
                            }
                        }
                    }
                    break;
                }


                fff += Time.deltaTime;
                if (fff > 0.4f) break;
                yield return new WaitForSeconds(Time.deltaTime);
            }
            isDash = false;
            fInvincibleTime_offensive_01 = 0.5f;
            Destroy(ingObj);
            GameObject endEffect = Instantiate(DashEndEffect);
            endEffect.transform.position = transform.position;
            Destroy(endEffect, 2.0f);
        }
        private void SpawnRight()
        {
            if ((PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ATTACK2], false) || Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_ATTACK2], false)) && hero.isWeaponMode && !isArmCollision && !_ArmController.isGrab && !isUseQ && !hero.animator.GetBool("Avoid") &&
                !UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject() && !isRightCooltime && !isFreeze)
            {
                isRightCooltime = true;
                Inventorys.Instance.rightCollTimeImage.transform.parent.Find("CollTimeImageP").gameObject.SetActive(true);
                rightCollTimeNow = 0.0f;
                SoundManager.instance.StartAudio(new string[1] { "Weapon/" + nowRightWeaponName }, VOLUME_TYPE.EFFECT);
                for (int i = 0; i < 3; i++)
                {
                    GameObject obj = Instantiate(DroneObj);
                    obj.transform.position = GetPosition(transform.position, 40 * i + 30);
                    obj.GetComponent<DroneController>().moveAngle = 40 * i + 20;
                }
            }
        }
        private void GunFire()
        {
            if (swordControl.gameObject.activeSelf) swordControl.gameObject.SetActive(false);
            if (!gunControl.gameObject.activeSelf && !isShield && rightAttackCount == 0)
            {
                hero.WeaponsSlot = gunControl.gameObject;
                gunControl.gameObject.SetActive(true);
            }
            if ((PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ATTACK], false) || Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_ATTACK], false)) && hero.isWeaponMode && fireDelayNow > fireDelay && !isUseQ && !hero.animator.GetBool("Avoid") && !UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject() && !isFreeze)
            {
                if (isCollOpen)
                {
                    isSpecialAttack = true;
                    bulletTargetObj.GetComponent<Image>().sprite = bTargetNormal;
                    bulletTargetObj.SetActive(true);
                    bulletTargetX = -36.0f;
                }
                else
                {
                    bulletTargetObj.SetActive(false);
                }

                Vector3 pos = transform.position;
                pos.y -= 1.75f;
                weaponAnim.SetTrigger("Attack");
                fireCoroutine = StartCoroutine(Fire());
                fireDelayNow = 0.0f;
                if (_pet != null) _pet.attackTimer = 5.0f;
            }
        }
        private void GunFireRight()
        {
            if(nowRightWeaponName.Contains("lasergun_recycle"))
            {
                if ((PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ATTACK2], false) || Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_ATTACK2], false)) && hero.isWeaponMode && !isArmCollision && !_ArmController.isGrab && !isUseQ && !hero.animator.GetBool("Avoid") &&
                    !UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject() && !isRightCooltime && !isFreeze)
                {
                    if (!gunControl.transform.Find("living_arrow_weapon").gameObject.activeSelf) gunControl.transform.Find("living_arrow_weapon").gameObject.SetActive(true);
                    if (!swordControl.transform.Find("living_arrow_weapon").gameObject.activeSelf) swordControl.transform.Find("living_arrow_weapon").gameObject.SetActive(true);
                    if (fireDelayRightNow > fireDelayRight)
                    {
                        rightAttackCount++;
                        StartCoroutine(LaserGunRecycle(rightAttackCount));
                        fireDelayRightNow = 0.0f;
                    }
                    pushCheck3++;
                    if (_pet != null) _pet.attackTimer = 5.0f;
                    _ArmController.isLeft = 0;
                }
                if (pushCheck3 == pushCheck4 && pushCheck3 != 0)
                {
                    pushCheck3 = pushCheck4 = 0;
                    _ArmController.isLeft = -1;
                    if (gunControl.transform.Find("living_arrow_weapon").gameObject.activeSelf) gunControl.transform.Find("living_arrow_weapon").gameObject.SetActive(false);
                    if (swordControl.transform.Find("living_arrow_weapon").gameObject.activeSelf) swordControl.transform.Find("living_arrow_weapon").gameObject.SetActive(false);
                }
                else if (pushCheck3 != 0)
                {
                    if (transform.Find("GreenFatLaserBeam").gameObject.activeSelf) transform.Find("GreenFatLaserBeam").gameObject.SetActive(false);
                    if (transform.Find("GreenFatLaserEnd").gameObject.activeSelf) transform.Find("GreenFatLaserEnd").gameObject.SetActive(false);
                    if (transform.Find("GreenFatLaserStart").gameObject.activeSelf) transform.Find("GreenFatLaserStart").gameObject.SetActive(false);
                    if (weaponAnim != null && weaponAnim.runtimeAnimatorController != null) weaponAnim.SetBool("Attack", false);
                    t = 0;
                    for (int i = 0; i < ArmList.Count; i++) ArmList[i].SetActive(false);
                    gunControl.sr.enabled = false;
                    swordControl.swordObj.GetComponent<SpriteRenderer>().enabled = false;
                }
                pushCheck4 = pushCheck3;
            }
            else
            {
                if ((PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ATTACK2], false) || Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_ATTACK2], false)) && hero.isWeaponMode && !isArmCollision && !_ArmController.isGrab && !isUseQ && !hero.animator.GetBool("Avoid") &&
                    !UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject() && !isRightCooltime && !isFreeze)
                {
                    isRightCooltime = true;
                    Inventorys.Instance.rightCollTimeImage.transform.parent.Find("CollTimeImageP").gameObject.SetActive(true);
                    rightCollTimeNow = 0.0f;
                    SoundManager.instance.StartAudio(new string[1] { "Weapon/" + nowRightWeaponName }, VOLUME_TYPE.EFFECT);
                    StartCoroutine(GunFireRightAttack());
                }
            }
        }
        int lasergunc = 0;
        IEnumerator LaserGunRecycle(long _rightAttackCount)
        {
            if ((Dungeon.instance.isBossRush ? Inventorys.Instance.glowindskull2 : Inventorys.Instance.glowindskull) > 0)
            {
                lasergunc++;
                if (lasergunc >= 4)
                {
                    lasergunc = 0;

                    if (Dungeon.instance.isBossRush)
                        Inventorys.Instance.glowindskull2 -= 1;
                    else
                        Inventorys.Instance.glowindskull -= 1;

                    SaveAndLoadManager.instance.SaveItem(new ItemSave
                    {

                        _id = (Dungeon.instance.isBossRush ? "glowing_skull2" : "glowing_skull"),
                        _nCount = -1
                    });
                }
                for (int i = 0; i < 6; i++)
                {
                    Vector3 firePos = transform.position;
                    Vector3 crossPos = GetPosition(firePos, gun_simulation.rot + Random.Range(-5, 5));
                    firePos.z = crossPos.z = 0;
                    Vector3 dir = crossPos - firePos;
                    dir.Normalize();
                    GameObject obj = ObjManager.Call().GetObject("lasergun_recycle");
                    obj.transform.position = transform.position + (dir * 1.25f);
                    obj.GetComponent<SciFiBeamStatic>().dir = dir;
                    obj.SetActive(true);
                    yield return new WaitForSeconds(0.05f);
                }
            }
            else
            {
                TextOpenController.instance.ShowText(LocalizeManager.GetLocalize("InsufficientUsingItem"), Color.red, 18, gameObject, 1, false);
            }
            if (rightAttackCount == _rightAttackCount)
            {
                if (gunControl.transform.Find("living_arrow_weapon").gameObject.activeSelf) gunControl.transform.Find("living_arrow_weapon").gameObject.SetActive(false);
                if (swordControl.transform.Find("living_arrow_weapon").gameObject.activeSelf) swordControl.transform.Find("living_arrow_weapon").gameObject.SetActive(false);
                gunControl.sr.enabled = true;
                rightAttackCount = 0;
            }
        }
        IEnumerator GunFireRightAttack()
        {
            Vector3 startPos = transform.position;
            Vector3 endPos = UnityEngine.Camera.main.ScreenToWorldPoint(hero.WeaponsCrossHair.transform.position);
            GameObject shootObj = Instantiate(Resources.Load<GameObject>("Weapons/" + nowRightWeaponName + "/" + nowRightWeaponName + "_bomb"));
            shootObj.GetComponent<White_ant_bomb>().Throw(startPos, endPos, true, string.Empty);
            yield return new WaitForSeconds(0.5f);
        }
        private void Sting()
        {
            if (swordControl.gameObject.activeSelf) swordControl.gameObject.SetActive(false);
            if (!gunControl.gameObject.activeSelf && !isShield && rightAttackCount == 0) 
            {
                hero.WeaponsSlot = gunControl.gameObject;
                gunControl.gameObject.SetActive(true);
            }
            if (_ArmController.isLeft == 0) return;

            if (nowWeaponName.Contains("biological_energy"))
            {
                biologicalAttackTime -= Time.deltaTime;
                if (biologicalAttackTime < 0.0f)
                {
                    _ArmController.isAttack = false;
                    biologicalAttackTime = 0.25f;
                    biologicalAttackTime /= (1 + plusAttackSpeed);
                }
                if ((PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ATTACK], false) || Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_ATTACK], false)) && hero.isWeaponMode && !_ArmController.isGrab && !isUseQ && !hero.animator.GetBool("Avoid") && !UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject() && !isFreeze)
                {
                    if (isCollOpen)
                    {
                        bulletTargetObj.GetComponent<Image>().sprite = bTargetNormal;
                        isSpecialAttack = true;
                        bulletTargetObj.SetActive(true);
                        bulletTargetX = -36.0f;
                    }
                    else
                    {
                        bulletTargetObj.SetActive(false);
                    }
                    _ArmController.isLeft = 1;
                    if (weaponAnim != null && weaponAnim.runtimeAnimatorController != null) weaponAnim.SetBool("Attack", true);
                    Vector3 firePos = gunControl.transform.position;
                    Vector3 crossPos = UnityEngine.Camera.main.ScreenToWorldPoint(hero.WeaponsCrossHair.transform.position);
                    firePos.z = crossPos.z = 0;
                    Vector3 dir = crossPos - firePos;
                    dir.Normalize();

                    int mask = 1 << 8 | 1 << 12 | 1 << 22 | 1 << 24 | 1 << 23;
                    Vector3 tPos = firePos + (dir * 1.25F);
                    RaycastHit2D hit = Physics2D.Raycast(tPos, dir, bulletRange, mask);
                    if (hit)
                    {
                        endPos = hit.point + (Vector2)dir * 0.5f;
                    }
                    else endPos = tPos + (dir * bulletRange);
                    Vector3 pos = firePos + (dir * (armAmountX * t)) + (dir * 1.25F);
                    float dis = Vector3.Distance(firePos, pos);
                    float dis2 = Vector3.Distance(firePos, endPos);
                    LineRenderer lineRender = transform.Find("GreenFatLaserBeam").GetComponent<LineRenderer>();
                    if (!lineRender.gameObject.activeSelf) lineRender.gameObject.SetActive(true);
                    lineRender.positionCount = 2;
                    lineRender.SetPosition(0, tPos);
                    transform.Find("GreenFatLaserStart").position = tPos;
                    transform.Find("GreenFatLaserEnd").position = endPos;
                    lineRender.SetPosition(1, endPos);

                    if (dis2 > dis)
                    {
                        t++;
                    }
                    else
                    {
                        if (!_ArmController.isAttack)
                        {
                            _ArmController.isAttack = true;
                            if (hit && hit.collider != null && hit.collider.GetComponent<MOB.Monster>())
                            {
                                SoundManager.instance.StartAudio(new string[1] { "Weapon/biological_energy" }, VOLUME_TYPE.EFFECT);
                                Vector3 dir2 = hit.collider.transform.position - transform.position;
                                dir2.Normalize();
                                hit.collider.GetComponent<MOB.Monster>().SetDamage(isLeftNear ? damageNear : damage, -dir2, 2, left_elemetanl, left_elemnetalTime);

                                if (isLeftNear && isCritical)
                                {
                                    GameObject criEffect = ObjManager.Call().GetObject("CriticalHitEffect");
                                    criEffect.transform.position = hit.point;
                                    criEffect.SetActive(true);
                                }
                                GameObject obj2 = Instantiate(LaserExplosionGreen);
                                obj2.transform.position = endPos;
                                Destroy(obj2, 2f);
                            }
                            if (hit && hit.collider != null && hit.collider.gameObject.layer.Equals(23))
                            {
                                SoundManager.instance.StartAudio(new string[1] { "Weapon/biological_energy" }, VOLUME_TYPE.EFFECT);
                                if (hit.collider.gameObject.name.Contains("puddle")) return;
                                if (hit.collider.gameObject.name.Contains("grass_2")) return;
                                if (hit.collider.gameObject.name.Contains("door")) return;

                                if (hit.collider.transform.parent && hit.collider.transform.parent.GetComponent<TentacleController>())
                                {
                                    hit.collider.transform.parent.GetComponent<TentacleController>().SetDamage(1);
                                    GameObject obj2 = Instantiate(LaserExplosionGreen);
                                    obj2.transform.position = endPos;
                                    Destroy(obj2, 2f);
                                }
                                Vector3 dir2 = hit.collider.gameObject.transform.position - transform.position;
                                dir2.Normalize();
                                if (hit.collider.gameObject.GetComponent<DestroyObjects>())
                                {
                                    hit.collider.gameObject.GetComponent<DestroyObjects>().DestroyObject(dir2);
                                    GameObject obj2 = Instantiate(LaserExplosionGreen);
                                    obj2.transform.position = endPos;
                                    Destroy(obj2, 2f);
                                }
                                if (hit.collider.gameObject.GetComponent<ChestHit>() && !hit.collider.gameObject.GetComponent<ChestHit>().isOpenBefore)
                                {
                                    hit.collider.gameObject.GetComponent<ChestHit>().SetDamage(1);
                                    GameObject obj2 = Instantiate(LaserExplosionGreen);
                                    obj2.transform.position = endPos;
                                    Destroy(obj2, 2f);
                                }
                                if (hit.collider.gameObject.GetComponent<DrumHit>())
                                {
                                    hit.collider.gameObject.GetComponent<DrumHit>().SetDamage(1);
                                    GameObject obj2 = Instantiate(LaserExplosionGreen);
                                    obj2.transform.position = endPos;
                                    Destroy(obj2, 2f);
                                }
                            }
                            if (hit && hit.collider != null && hit.collider.gameObject.layer.Equals(24))
                            {
                                SoundManager.instance.StartAudio(new string[1] { "Weapon/biological_energy" }, VOLUME_TYPE.EFFECT);
                                if (hit.collider.gameObject.GetComponent<Boss_STG>()) hit.collider.gameObject.GetComponent<Boss_STG>().SetDamage(isLeftNear ? damageNear : damage, hit.point);
                                if (hit.collider.gameObject.GetComponent<Boss_NPC>()) hit.collider.gameObject.GetComponent<Boss_NPC>().SetDamage(isLeftNear ? damageNear : damage, hit.point);
                                if (hit.collider.gameObject.GetComponent<Boss_Golem>()) hit.collider.gameObject.GetComponent<Boss_Golem>().SetDamage(isLeftNear ? damageNear : damage, hit.point);
                                if (hit.collider.gameObject.GetComponent<Boss_Stone>()) hit.collider.gameObject.GetComponent<Boss_Stone>().SetDamage(isLeftNear ? damageNear : damage, hit.point, false);
                                if (isStrategic_02) instance.isStrategic_02 = false;

                                if (isLeftNear && isCritical)
                                {
                                    GameObject criEffect = ObjManager.Call().GetObject("CriticalHitEffect");
                                    criEffect.transform.position = hit.point;
                                    criEffect.SetActive(true);
                                }
                                GameObject obj2 = Instantiate(LaserExplosionGreen);
                                obj2.transform.position = endPos;
                                Destroy(obj2, 2f);
                            }
                        }
                    }
                    pushCheck++;
                    if (_pet != null) _pet.attackTimer = 5.0f;
                }
                if (pushCheck == pushCheck2)
                {
                    t = 0;
                    _ArmController.isLeft = -1;
                    pushCheck = pushCheck2 = 0;
                    if (weaponAnim != null && weaponAnim.runtimeAnimatorController != null) weaponAnim.SetBool("Attack", false);
                    if (transform.Find("GreenFatLaserBeam").gameObject.activeSelf) transform.Find("GreenFatLaserBeam").gameObject.SetActive(false);
                    if (transform.Find("GreenFatLaserStart").gameObject.activeSelf) transform.Find("GreenFatLaserStart").gameObject.SetActive(false);
                    if (transform.Find("GreenFatLaserEnd").gameObject.activeSelf) transform.Find("GreenFatLaserEnd").gameObject.SetActive(false);
                }
                else
                {
                    if (!transform.Find("GreenFatLaserStart").gameObject.activeSelf) transform.Find("GreenFatLaserStart").gameObject.SetActive(true);
                    if (!transform.Find("GreenFatLaserEnd").gameObject.activeSelf) transform.Find("GreenFatLaserEnd").gameObject.SetActive(true);
                }
                pushCheck2 = pushCheck;
            }
            else if (nowWeaponName.Contains("multiple_cellstem"))
            {
                if ((PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ATTACK], false) || Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_ATTACK], false)) && hero.isWeaponMode && !hero.animator.GetBool("Avoid") && !isArmCollision && !_ArmController.isGrab && !isUseQ &&
                    !UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject() && leftColltimeNow > leftColltime && !isFreeze)
                {
                    if (isCollOpen)
                    {
                        bulletTargetObj.GetComponent<Image>().sprite = bTargetNormal;
                        isSpecialAttack = true;
                        bulletTargetObj.SetActive(true);
                        bulletTargetX = -36.0f;
                    }
                    else
                    {
                        bulletTargetObj.SetActive(false);
                    }
                    _ArmController.isLeft = 1;


                    if (!gunControl.GetComponent<Animator>().GetBool("Trigger_" + (multiShoot + plusMultiShoot > 3 ? 3 : multiShoot + plusMultiShoot)))
                    {
                        int nShoot = 1;
                        nShoot = (multiShoot + plusMultiShoot > 3 ? 3 : multiShoot + plusMultiShoot);
                        if(nShoot == 1)
                        {
                            gunControl.GetComponent<Animator>().SetBool("Trigger_1", true);
                            gunControl.GetComponent<Animator>().SetBool("Trigger_2", false);
                            gunControl.GetComponent<Animator>().SetBool("Trigger_3", false);
                        }
                        else if (nShoot == 2)
                        {
                            gunControl.GetComponent<Animator>().SetBool("Trigger_1", false);
                            gunControl.GetComponent<Animator>().SetBool("Trigger_2", true);
                            gunControl.GetComponent<Animator>().SetBool("Trigger_3", false);
                        }
                        else 
                        {
                            gunControl.GetComponent<Animator>().SetBool("Trigger_1", false);
                            gunControl.GetComponent<Animator>().SetBool("Trigger_2", false);
                            gunControl.GetComponent<Animator>().SetBool("Trigger_3", true);
                        }
                        gunControl.GetComponent<Animator>().SetTrigger("Attack");
                        gunControl.GetComponent<AudioSource>().Play();
                        //if (gunControl.GetComponent<AudioSource>() && !gunControl.GetComponent<AudioSource>().isPlaying)
                        //{
                        //    gunControl.GetComponent<AudioSource>().Play();
                        //}
                    }
                    pushCheck++;
                }

                if (pushCheck == pushCheck2)
                {
                    gunControl.GetComponent<Animator>().SetBool("Trigger_" + (multiShoot + plusMultiShoot > 3 ? 3 : multiShoot + plusMultiShoot), false);
                    _ArmController.isLeft = -1;
                    _ArmController.isAttack = false;
                    isArmCollision = false;
                }
                pushCheck2 = pushCheck;
            }
            else if (nowWeaponName.Contains("harpoon_tentacles") || nowWeaponName.Contains("electric_cellmass") || nowWeaponName.Contains("pixelfire_small") || nowWeaponName.Equals("Ice_spear") || nowWeaponName.Equals("volatile") || nowWeaponName.Equals("dark_cells"))
            {
                if ((PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ATTACK], false) || Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_ATTACK], false)) && hero.isWeaponMode && (volatileBuffType == 3 ? fireDelayNow > 0.1f : fireDelayNow > fireDelay) && !isUseQ && !hero.animator.GetBool("Avoid") && !UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject() && !isFreeze)
                {
                    if (isCollOpen)
                    {
                        bulletTargetObj.GetComponent<Image>().sprite = bTargetNormal;
                        isSpecialAttack = true;
                        bulletTargetObj.SetActive(true);
                        bulletTargetX = -36.0f;
                    }
                    else
                    {
                        bulletTargetObj.SetActive(false);
                    }
                    fireCoroutine = StartCoroutine(Fire());
                    fireDelayNow = 0.0f;
                    if (_pet != null) _pet.attackTimer = 5.0f;
                }
            }
            else if (nowWeaponName.Contains("elastic_cell_weapon"))
            {
                if ((PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ATTACK], false) || Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_ATTACK], false)) && hero.isWeaponMode && fireDelayNow > fireDelay && !isUseQ && !hero.animator.GetBool("Avoid") && !UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject() && !isFreeze)
                {
                    if (isCollOpen)
                    {
                        bulletTargetObj.GetComponent<Image>().sprite = bTargetOneCenter;
                        isSpecialAttack = true;
                        bulletTargetObj.SetActive(true);
                        bulletTargetX = -36.0f;
                    }
                    else
                    {
                        bulletTargetObj.SetActive(false);
                    }
                    fireCoroutine = StartCoroutine(Fire());
                    fireDelayNow = 0.0f;
                    if (_pet != null) _pet.attackTimer = 5.0f;
                }
            }
            else if (nowWeaponName.Equals("electric_cellstem"))
            {
                if ((PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ATTACK], false) || Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_ATTACK], false)) && hero.isWeaponMode && !isFreeze && (fireDelayNow > fireDelay || (isGiant_blade_combo && giant_blade_comboTime > 0.3f && nGiant_blade_count < 3)) && !hero.animator.GetBool("Avoid") && !isUseQ && !UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject())
                {
                    if (isCollOpen)
                    {
                        bulletTargetObj.GetComponent<Image>().sprite = bTargetNormal;
                        isSpecialAttack = true;
                        bulletTargetObj.SetActive(true);
                        bulletTargetX = -36.0f;
                    }
                    else
                    {
                        bulletTargetObj.SetActive(false);
                    }
                    SoundManager.instance.StartAudio(new string[1] { "Weapon/" + nowWeaponName }, VOLUME_TYPE.EFFECT);
                    _ArmController.isAttack = false;
                    fireDelayNow = 0.0f;
                    giant_blade_comboTime = 0.0f;
                    isGiant_blade_combo = true;
                    gunControl.animator.SetTrigger("AttackTrigger");
                    gunControl.animator.SetInteger("Combo", nGiant_blade_count);
                    if (nGiant_blade_count > 3) nGiant_blade_count = 0;
                    nGiant_blade_count++;
                }
            }
            else if (nowWeaponName.Contains("biological_cannon"))
            {
                if ((PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ATTACK], false) || Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_ATTACK], false)) && hero.isWeaponMode && fireDelayNow > fireDelay && !hero.animator.GetBool("Avoid") && !UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject() && !isFreeze) 
                {
                    if (isCollOpen)
                    {
                        bulletTargetObj.GetComponent<Image>().sprite = bTargetNormal;
                        isSpecialAttack = true;
                        bulletTargetObj.SetActive(true);
                        bulletTargetX = -36.0f;
                    }
                    else
                    {
                        bulletTargetObj.SetActive(false);
                    }
                    pushTime += Time.deltaTime;
                    pushCheck++;
                    if (!isColdFire)
                    {
                        gunControl.animator.SetTrigger("AttackTrigger");
                        isColdFire = true;
                    }
                    if (_pet != null) _pet.attackTimer = 5.0f;
                }

                if (pushTime > 0.25f)
                {
                    eneryTime += Time.deltaTime;
                    if (!gunControl.animator.GetBool("isCombine")) gunControl.animator.SetBool("isCombine", true);
                }

                if (pushCheck == pushCheck2)
                {
                    gunControl.EneryChargeEnd();
                    if (isColdFire)
                    {
                        pushTime = 0.0f;
                        fireDelayNow = 0.0f;
                        if (gunControl.animator.GetBool("isCombine"))
                        {
                            int level = 0;
                            gunControl.animator.SetBool("isCombine", false);
                            GameObject obj = ObjManager.Call().GetObject("EneryChargeBall");
                            obj.transform.position = transform.position;
                            Vector3 firePos = hero.WeaponsFireTransform.position;
                            Vector3 crossPos = GetPosition(firePos, gun_simulation.rot);
                            firePos.z = crossPos.z = 0;
                            Vector3 dir = crossPos - firePos;
                            dir.Normalize();
                            if (eneryTime < 1.0f)
                            {
                                obj.transform.localScale = new Vector3(1, 1, 1);
                                level = 1;
                            }
                            else if (eneryTime < 3.0f)
                            {
                                obj.transform.localScale = new Vector3(2, 2, 1);
                                level = 2;
                            }
                            else
                            {
                                obj.transform.localScale = new Vector3(3, 3, 1);
                                level = 3;
                            }
                            obj.GetComponent<PlayerAttackController>().attackLevel = level;
                            obj.GetComponent<PlayerAttackController>().firePosition = transform.position;
                            obj.GetComponent<PlayerAttackController>().destroyDistance = bulletRange;
                            obj.SetActive(true);
                            obj.GetComponent<PlayerAttackController>().RigidBodyFire(dir, bulletSpeed);
                        }
                        eneryTime = 0.0f;
                        isColdFire = false;
                    }
                }

                pushCheck2 = pushCheck;
            }
            else if (nowWeaponName.Contains("cold_cell_wave"))
            {
                if ((PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ATTACK], false) || Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_ATTACK], false)) && hero.isWeaponMode && fireDelayNow > fireDelay && !isUseQ && !hero.animator.GetBool("Avoid") && !UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject() && !isFreeze)
                {
                    if (isCollOpen)
                    {
                        bulletTargetObj.GetComponent<Image>().sprite = bTargetNormal;
                        isSpecialAttack = true;
                        bulletTargetObj.SetActive(true);
                        bulletTargetX = -36.0f;
                    }
                    else
                    {
                        bulletTargetObj.SetActive(false);
                    }
                    if (leftColltimeNow > leftColltime)
                    {
                        if (!isColdFire)
                        {
                            fireCoroutine = StartCoroutine(Fire());
                        }
                        isColdFire = true;
                        coldDelayNow += Time.deltaTime;
                        coldCheckTime -= Time.deltaTime;
                        pushCheck++;
                    }
                    else
                    {
                        if (!isColdFire)
                        {
                            fireCoroutine = StartCoroutine(Fire());
                        }
                        fireDelayNow = 0.0f;
                    }
                    if (_pet != null) _pet.attackTimer = 5.0f;
                }
                if (pushCheck == pushCheck2 || left_defence_gauge < 1.0f)
                {
                    if (isColdFire)
                    {
                        pushCheck = pushCheck2 = 0;
                        isColdFire = false;
                        fireDelayNow = 0.0f;
                        if (instanceColdObj != null)
                        {
                            ParticleSystem p1 = instanceColdObj.transform.GetChild(0).GetComponent<ParticleSystem>();
                            ParticleSystem p2 = p1.transform.GetChild(0).GetComponent<ParticleSystem>();
                            ParticleSystem.MainModule pm1 = p1.main;
                            pm1.loop = false;
                            ParticleSystem.MainModule pm2 = p2.main;
                            pm2.loop = false;
                            if (coldCheckTime < 0.0f)
                            {
                                Destroy(instanceColdObj, 0.5f);
                            }
                            else
                            {
                                Destroy(instanceColdObj, 0.5f);
                            }
                        }
                    }
                }
                else
                {
                    if (left_defence_gauge > 2.0f && coldDelayNow > coldDelay)
                    {
                        if (instanceColdObj == null)
                        {
                            coldCheckTime = 5.0f;
                            instanceColdObj = Instantiate(coldObj);
                            instanceColdObj.GetComponent<PlayerAttackController>().hitTime = tickTime;
                        }
                        coldDelayNow = 0.0f;
                        left_defence_gauge -= 2.0f;
                    }
                }
                if (instanceColdObj != null && isColdFire)
                {
                    Vector3 p = UnityEngine.Camera.main.ScreenToWorldPoint(PlayerCombat.instance.aim.transform.position);
                    p.z = 0;
                    instanceColdObj.transform.position = p;
                }
                pushCheck2 = pushCheck;
            }
            else if (nowWeaponName.Contains("tracking_energy_gun"))
            {
                if ((PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ATTACK], false) || Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_ATTACK], false)) && hero.isWeaponMode && fireDelayNow > fireDelay && !isUseQ && !hero.animator.GetBool("Avoid") && !UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject() && !isFreeze)
                {
                    if (isCollOpen)
                    {
                        bulletTargetObj.GetComponent<Image>().sprite = bTargetNormal;
                        isSpecialAttack = true;
                        bulletTargetObj.SetActive(true);
                        bulletTargetX = -36.0f;
                    }
                    else
                    {
                        bulletTargetObj.SetActive(false);
                    }
                    gunControl.animator.SetTrigger("AttackTrigger");
                    fireDelayNow = 0.0f;
                    if (_pet != null) _pet.attackTimer = 5.0f;
                }
            }
            else
            {
                if (ran2 == null || ran2.Length == 0)
                {
                    ran2 = new float[multiShoot + plusMultiShoot];
                    for (int i = 0; i < ran2.Length; i++)
                        ran2[i] = Random.Range(-aimAngle, aimAngle);
                }
                if ((PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ATTACK], false) || Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_ATTACK], false)) && hero.isWeaponMode && !hero.animator.GetBool("Avoid") && !isArmCollision && !_ArmController.isGrab && !isUseQ &&
                    !UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject() && leftColltimeNow > leftColltime && !isFreeze)
                {
                    if (isCollOpen)
                    {
                        bulletTargetObj.GetComponent<Image>().sprite = bTargetNormal;
                        isSpecialAttack = true;
                        bulletTargetObj.SetActive(true);
                        bulletTargetX = -36.0f;
                    }
                    else
                    {
                        bulletTargetObj.SetActive(false);
                    }
                    isArmLeftAttack = true;
                }

                if (isArmLeftAttack && !isArmCollision && !_ArmController.isGrab && !isUseQ)
                {
                    _ArmController.isLeft = 1;
                    gunControl.sr.enabled = false;
                    if (fireDelayNow > fireDelay)
                    {
                        if (t < armCount)
                        {
                            if (t == 0) SoundManager.instance.StartAudio(new string[1] { "Weapon/tentacle_arm" }, VOLUME_TYPE.EFFECT);
                            for (int i = 0; i < Mathf.CeilToInt(spreadCountLeft * (1 + plusAttackSpeed)); i++)
                            {
                                for (int m = 0; m < multiShoot + plusMultiShoot; m++)
                                {
                                    float plusX = 0.0f;
                                    float plusY = 0.0f;
                                    GameObject obj = GetArmPoolingObject();
                                    int ran = Random.Range(0, 2);
                                    if (ran < 1 && AddCountNow < AddCountMax && isAddArm)
                                    {
                                        obj.GetComponent<ArmPiece>().isAddArm = true;
                                        AddCountNow++;
                                    }
                                    else obj.GetComponent<ArmPiece>().isAddArm = false;
                                    obj.SetActive(true);

                                    float angleangle = 0.0f;
                                    if ((multiShoot + plusMultiShoot) == 1) angleangle = 0.0f;
                                    else
                                    {
                                        int v = (multiShoot + plusMultiShoot) / 2;
                                        v *= 10;
                                        angleangle = -v + (10 * ((multiShoot + plusMultiShoot) % 2 == 0 ? m + 1 : m));
                                    }
                                    if (ran2 == null || ran2.Length != (multiShoot + plusMultiShoot)) 
                                    {
                                        ran2 = new float[multiShoot + plusMultiShoot];
                                        for (int kk = 0; kk < ran2.Length; kk++)
                                            ran2[kk] = Random.Range(-aimAngle, aimAngle);
                                    }
                                    angleangle += ran2[m];
                                    Vector3 pos = GetPosition(new Vector3(plusX, plusY, 0), angleangle);
                                    Vector3 dir = pos - (new Vector3(plusX, plusY, 0));
                                    dir.Normalize();
                                    obj.transform.localPosition = armStartVector + dir * (armAmountX * t);
                                    obj.transform.localEulerAngles = new Vector3(0, 0, angleangle);
                                    obj.transform.localScale = Vector3.one;
                                    obj.GetComponent<ArmPiece>().enabled = true;
                                    obj.GetComponent<SpriteRenderer>().sortingOrder = (armCount * (multiShoot + plusMultiShoot)) - (t + m);
                                }
                                t++;
                            }
                            for (int i = ArmList.Count - 1; i >= 0; i--)
                            {
                                if (ArmList[i].activeSelf)
                                {
                                    ArmList[i].GetComponent<SpriteRenderer>().sprite = armPieceSprites[Random.Range(1, armPieceSprites.Length - 1)];
                                }
                            }
                        }
                        else
                            isArmLeftAttack = false;
                        fireDelayNow = 0.0f;
                    }
                    if (t < armCount && !isDecrease) pushCheck++;
                    if (_pet != null) _pet.attackTimer = 5.0f;
                }
                else
                    isArmLeftAttack = false;

                if (pushCheck == pushCheck2)
                {
                    if (t != 0)
                    {
                        leftColltimeNow = 0.0f;
                        isDecrease = true;
                        fireDelayNow = 10.0f;
                        if (armDelayNow > armDelay)
                        {
                            for (int i = 0; i < Mathf.CeilToInt(frequencyCountLeft * (1 + plusAttackSpeed)); i++)
                            {
                                if (t - 1 < 0) continue;
                                if (_ArmController.isGrab) _ArmController.grabMonster.tGrab = ArmList[t - 1].transform;
                                for (int m = 0; m < (multiShoot + plusMultiShoot); m++)
                                {
                                    int index = t * (multiShoot + plusMultiShoot);
                                    if (ArmList[index - 1 - m] == null)
                                    {
                                        ArmList.RemoveAt(index - 1 - m);
                                        i--;
                                        continue;
                                    }
                                    ArmList[index - 1 - m].SetActive(false);
                                }
                                if (t > 0) t--;
                            }
                            armDelayNow = 0.0f;
                        }
                    }
                    if (t == 0)
                    {
                        for (int i = 0; i < ran2.Length; i++) ran2[i] = Random.Range(-aimAngle, aimAngle);
                        isDecrease = false;
                        AddCountNow = 0;
                        _ArmController.isLeft = -1;
                        if (rightAttackCount == 0) 
                        gunControl.sr.enabled = true;
                        isArmCollision = false;
                        _ArmController.isAttack = false;
                        if (_ArmController.isGrab) _ArmController.grabMonster.tGrab = ArmParent;
                        isArmLeftAttack = false;
                    }
                }
                pushCheck2 = pushCheck;
            }
        }
        private void OnDrawGizmos()
        {
            Gizmos.color = Color.red;
            Gizmos.DrawWireCube(transform.position, boxCollSize);
        }
        private void Grab()
        {
            if (_ArmController.isLeft == 1) return;
            if ((PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ATTACK2], false) || Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_ATTACK2], false)) && hero.isWeaponMode && !isArmCollision && !_ArmController.isGrab && !isUseQ &&
                !hero.animator.GetBool("Avoid") && !UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject() && !isRightCooltime && !isFreeze)
            {
                isArmLeftAttack = false;
                if (swordControl.gameObject.activeSelf) swordControl.gameObject.SetActive(false);
                if (!gunControl.gameObject.activeSelf && rightAttackCount == 0)
                {
                    hero.WeaponsSlot = gunControl.gameObject;
                    gunControl.gameObject.SetActive(true);
                }
                _ArmController.isLeft = 0;
                gunControl.sr.enabled = false;
                if (fireDelayRightNow > fireDelayRight)
                {
                    if (t < armCount)
                    {
                        if (t == 0) SoundManager.instance.StartAudio(new string[1] { "Weapon/catching_tentacle" }, VOLUME_TYPE.EFFECT);
                        for (int i = 0; i < Mathf.CeilToInt(spreadCountRight * (1 + plusAttackSpeed)); i++)
                        {
                            GameObject obj = GetArmPoolingObject();
                            obj.SetActive(true);
                            obj.transform.localPosition = new Vector3(armAmountX * t, 0, 0) + armStartVector;
                            obj.transform.localEulerAngles = Vector3.zero;
                            obj.transform.localScale = Vector3.one;
                            obj.GetComponent<ArmPiece>().enabled = true;
                            obj.GetComponent<SpriteRenderer>().sortingOrder = armCount - t;
                            t++;
                        }
                        bool first = false;
                        for (int i = ArmList.Count - 1; i >= 0; i--)
                        {
                            if (ArmList[i].activeSelf)
                            {
                                if (!first)
                                {
                                    first = true;
                                    ArmList[i].GetComponent<SpriteRenderer>().sprite = armPieceSprites_right[armPieceSprites_right.Length - 1];
                                }
                                else if (i == 0)
                                {
                                    ArmList[i].GetComponent<SpriteRenderer>().sprite = armPieceSprites_right[0];
                                }
                                else
                                {
                                    ArmList[i].GetComponent<SpriteRenderer>().sprite = armPieceSprites_right[Random.Range(1, armPieceSprites_right.Length - 1)];
                                }
                            }
                        }
                    }
                    fireDelayRightNow = 0.0f;
                }

                if (t < armCount && !isDecrease)
                    pushCheck++;
                if (_pet != null) _pet.attackTimer = 5.0f;
            }
            else if ((PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ATTACK2], true) || Input.GetKeyDown(KeyCode.Mouse0) || 
                Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_ATTACK], true) || Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_ATTACK2], true)) && _ArmController.isGrab) 
            {
                SoundManager.instance.StartAudio(new string[1] { "Weapon/catching_tentacle_pull" }, VOLUME_TYPE.EFFECT);
                Vector3 firePos = hero.WeaponsFireTransform.position;
                Vector3 crossPos = GetPosition(firePos, gun_simulation.rot);
                firePos.z = crossPos.z = 0;
                Vector3 dir = crossPos - firePos;
                dir.Normalize();
                _ArmController.grabMonster.rigid.AddForce(dir * pullPow, ForceMode2D.Impulse);
                _ArmController.grabMonster.UnGrab();
                _ArmController.isGrab = false;
                grapGuage.SetActive(false);
            }

            if (pushCheck == pushCheck2)
            {
                if (t != 0)
                {
                    isRightCooltime = true;
                    Inventorys.Instance.rightCollTimeImage.transform.parent.Find("CollTimeImageP").gameObject.SetActive(true);
                    rightCollTimeNow = 0.0f;
                    isDecrease = true;
                    if (rightAttackCount == 0)
                        gunControl.sr.enabled = true;
                    if (armDelayNow > armDelay)
                    {
                        for (int i = 0; i < Mathf.CeilToInt(frequencyCountRight * (1 + plusAttackSpeed)); i++)
                        {
                            if (t - 1 < 0) continue;
                            if (_ArmController.isGrab) _ArmController.grabMonster.tGrab = ArmList[t - 1].transform;
                            ArmList[t - 1].SetActive(false);
                            if (t > 0) t--;
                        }
                        armDelayNow = 0.0f;
                    }
                }
                if (t == 0)
                {
                    isDecrease = false;
                    _ArmController.isLeft = -1;
                    isArmCollision = false;
                    if (_ArmController.isGrab) _ArmController.grabMonster.tGrab = ArmParent;

                    if (itemType.Equals("Sword"))
                    {
                        hero.WeaponsSlot = swordControl.gameObject;
                        weaponAnim = swordControl.animator;
                        gunControl.gameObject.SetActive(false);
                    }
                    else
                    {
                        hero.WeaponsSlot = gunControl.gameObject;
                        weaponAnim = hero.WeaponsSlot.GetComponent<Animator>();
                        swordControl.gameObject.SetActive(false);
                    }
                }
            }
            if (armDelayNow2 > armDelay2 && isWaveRight)
            {
                for (int i = 0; i < ArmList.Count; i++)
                {
                    if (ArmList[i].activeSelf)
                    {
                        ArmList[i].transform.localPosition = new Vector3(ArmList[i].transform.localPosition.x, Random.Range(-armWaveAmount, armWaveAmount), 0);
                    }
                }
                armDelayNow2 = 0.0f;
            }
            pushCheck2 = pushCheck;
        }
        private void Shield()
        {
            if ((PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ATTACK2], false) || Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_ATTACK2], false)) && hero.isWeaponMode && !isArmCollision && !_ArmController.isGrab && _ArmController.isLeft == -1 && !isUseQ && !hero.animator.GetBool("Avoid") && !UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject())
            {
                if (!isShield)
                {
                    ShieldObj.SetActive(true);
                    isShield = true;
                    ShieldObj.GetComponent<Animator>().SetTrigger("open");
                }
                if (right_defence_gauge >= 1.0f) ShieldObj.GetComponent<BoxCollider2D>().enabled = true;
                else ShieldObj.GetComponent<BoxCollider2D>().enabled = false;
                pushCheck++;
                if (_pet != null) _pet.attackTimer = 5.0f;
            }

            if (pushCheck == pushCheck2 )
            {
                //END
                if (isShield)
                {
                    isShield = false;
                    ShieldObj.GetComponent<BoxCollider2D>().enabled = false;
                    ShieldObj.GetComponent<Animator>().SetTrigger("close");
                }
                pushCheck = pushCheck2 = 0;
            }
            pushCheck2 = pushCheck;
        }
        private void UseQskill()
        {
            if ((PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_SKILL], true) || Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_SKILL], true)) && hero.isWeaponMode && !isArmCollision && !_ArmController.isGrab && !isUseQ && ResultPanelScript.instance.Q_SkillCount > 0 && 
                (qSkillName.Contains("spiny_cell_firing") || !hero.animator.GetBool("Avoid"))&& !isQ_CollTime && !UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject())
            {
                if (PlayerBuffController.buffList.FindIndex(item => item.Equals("strategic_02")) != -1 && !isStrategic_02) isStrategic_02 = true;
                if (!isTutorial)
                {
                    ResultPanelScript.instance.Q_SkillCount--;
                    if (Dungeon.instance.nowRoomType.Equals("normal")) StateGroup.instance.nChanrgingSkillsNormal++;
                    else StateGroup.instance.nChanrgingSkillsBoss++;
                }
                if (qSkillName.Contains("multiple_bombing"))
                {
                    isQ_CollTime = true;
                    Q_CollTimeNow = 0.0f;
                    Inventorys.Instance.Q_CollTimeImage.transform.parent.Find("CollTimeImageP").gameObject.SetActive(true);
                    StartCoroutine(multipleBombing());
                }
                else if (qSkillName.Contains("deep_impact"))
                {
                    isQ_CollTime = true;
                    Q_CollTimeNow = 0.0f;
                    StartCoroutine(DeepImpact());
                }
                else if (qSkillName.Contains("q_change"))
                {
                    isQ_CollTime = true;
                    Q_CollTimeNow = 0.0f;
                    StartCoroutine(ChangeQActive());
                }
                else if (isRollingItem && !isRollingTransform)
                {
                    isQ_CollTime = true;
                    Q_CollTimeNow = 0.0f;
                    Inventorys.Instance.Q_CollTimeImage.transform.parent.Find("CollTimeImageP").gameObject.SetActive(true);
                    rollingTime = 5.0f;
                    StartCoroutine(TransformSpake(true));
                }
                else
                {
                    SoundManager.instance.StartAudio(new string[1] { "Character/QSkill" }, VOLUME_TYPE.EFFECT);
                    isUseQ = true;
                    Qskill.gameObject.SetActive(true);
                    Qskill.StartQ(qSkillName);
                    StartCoroutine(QShield());

                    GameObject obj2 = ObjManager.Call().GetObject("Hit_sprite7");
                    obj2.transform.position = transform.position;
                    obj2.transform.localScale = Vector3.one;
                    obj2.transform.localEulerAngles = Vector3.zero;
                    obj2.GetComponent<SwordHitEffectController>()._effctName = "qSkill";
                    obj2.GetComponent<SwordHitEffectController>().animSpeed = 1.0f;
                    obj2.GetComponent<Animator>().Rebind();
                    obj2.SetActive(true);
                }
                if (_pet != null) _pet.attackTimer = 5.0f;
            }
        }
        IEnumerator ChangeQActive()
        {
            if (Dungeon.instance.nQChange == 1) qSpecialTimeMAX = qSpecialTime = 10;
            if (Dungeon.instance.nQChange == 2)
            {
                plusMultiShoot += 1;
                qSpecialTimeMAX = qSpecialTime = 10; 
            }
            if (Dungeon.instance.nQChange == 3) qSpecialTimeMAX = qSpecialTime = 10;
            if (Dungeon.instance.nQChange == 4) qSpecialTimeMAX = qSpecialTime = 10;
            SoundManager.instance.StartAudio(new string[1] { "BuffEffectSound" }, VOLUME_TYPE.EFFECT);
            yield return null;
        }
        IEnumerator DeepImpact()
        {
            isHit = true;
            hitTIme = 10.0f;
            PlayerCombat.ps = PlayerState.DeepImpact;
            GameObject HitEffect = Resources.Load<GameObject>("Weapons/DeepImpactHit");
            GameObject HitEffect2 = Resources.Load<GameObject>("Weapons/DeepImpactHit2");
            Vector3 hitEffectSize = new Vector3(0.6f, 0.6f, 0.6f);
            yield return new WaitForSeconds(0.5f);
            StartCoroutine(QShield());
            GameObject obj33 = Instantiate(Resources.Load<GameObject>("Weapons/DeepImpact"));
            obj33.transform.position = transform.position;
            Destroy(obj33, 1f);
            RaycastHit2D[] hits = Physics2D.BoxCastAll(transform.position, boxCollSize, 0, Vector2.zero, 0, 1 << 12 | 1 << 23 | 1 << 24);
            if (hits.Length > 0)
            {
                for (int i = 0; i < hits.Length; i++)
                {
                    RaycastHit2D hit = hits[i];

                    if (hit.collider.gameObject.layer.Equals(12))
                    {
                        Vector3 dir = hit.collider.transform.position - transform.position;
                        dir.Normalize();

                        if (hit.collider.GetComponent<MOB.Monster>())
                        {
                            hit.collider.GetComponent<MOB.Monster>().SetDamage(damage, -dir, 1, ELEMENTAL_TYPE.NONE, 0.0f);
                            if (HitEffect)
                            {
                                GameObject obj = Instantiate(HitEffect);
                                obj.transform.position = hit.point;
                                obj.transform.localScale = hitEffectSize;
                                Destroy(obj, 2f);
                            }
                            if (HitEffect2)
                            {
                                GameObject obj = Instantiate(HitEffect2);
                                obj.transform.position = hit.point;
                                obj.transform.localScale = hitEffectSize;
                                Destroy(obj, 2f);
                            }
                        }
                    }
                    else if (hit.collider.gameObject.layer.Equals(24))
                    {
                        if (hit.collider.GetComponent<MOB.BossMonster_Six_Apear>()) hit.collider.GetComponent<MOB.BossMonster_Six_Apear>().SetDamage(damage);
                        if (hit.collider.GetComponent<Boss_STG>()) hit.collider.GetComponent<Boss_STG>().SetDamage(damage, hit.point);
                        if (hit.collider.GetComponent<Boss_NPC>()) hit.collider.GetComponent<Boss_NPC>().SetDamage(damage, hit.point);
                        if (hit.collider.GetComponent<Boss_Golem>()) hit.collider.GetComponent<Boss_Golem>().SetDamage(damage, hit.point);
                        if (hit.collider.GetComponent<Boss_Stone>()) hit.collider.GetComponent<Boss_Stone>().SetDamage(damage, hit.point, false);
                        if (Player.instance.isStrategic_02) Player.instance.isStrategic_02 = false;
                    }
                    else if (hit.collider.gameObject.layer.Equals(23))
                    {
                        if (hit.collider.transform.parent && hit.collider.transform.parent.GetComponent<TentacleController>())
                        {
                            hit.collider.transform.parent.GetComponent<TentacleController>().SetDamage(1);
                            if (HitEffect)
                            {
                                GameObject obj = Instantiate(HitEffect);
                                obj.transform.position = hit.point;
                                obj.transform.localScale = hitEffectSize;
                                Destroy(obj, 2f);
                            }
                            if (HitEffect2)
                            {
                                GameObject obj = Instantiate(HitEffect2);
                                obj.transform.position = hit.point;
                                obj.transform.localScale = hitEffectSize;
                                Destroy(obj, 2f);
                            }
                        }
                        Vector3 dir = hit.collider.gameObject.transform.position - PDG.Player.instance.transform.position;
                        dir.Normalize();
                        if (hit.collider.gameObject.GetComponent<DestroyObjects>())
                        {
                            hit.collider.gameObject.GetComponent<DestroyObjects>().DestroyObject(dir);
                            if (HitEffect)
                            {
                                GameObject obj = Instantiate(HitEffect);
                                obj.transform.position = hit.point;
                                obj.transform.localScale = hitEffectSize;
                                Destroy(obj, 2f);
                            }
                            if (HitEffect2)
                            {
                                GameObject obj = Instantiate(HitEffect2);
                                obj.transform.position = hit.point;
                                obj.transform.localScale = hitEffectSize;
                                Destroy(obj, 2f);
                            }
                        }
                        if (hit.collider.gameObject.GetComponent<ChestHit>() && !hit.collider.gameObject.GetComponent<ChestHit>().isOpenBefore)
                        {
                            hit.collider.gameObject.GetComponent<ChestHit>().SetDamage(1);
                            if (HitEffect)
                            {
                                GameObject obj = Instantiate(HitEffect);
                                obj.transform.position = hit.point;
                                obj.transform.localScale = hitEffectSize;
                                Destroy(obj, 2f);
                            }
                            if (HitEffect2)
                            {
                                GameObject obj = Instantiate(HitEffect2);
                                obj.transform.position = hit.point;
                                obj.transform.localScale = hitEffectSize;
                                Destroy(obj, 2f);
                            }
                        }
                        if (hit.collider.gameObject.GetComponent<DrumHit>())
                        {
                            hit.collider.gameObject.GetComponent<DrumHit>().SetDamage(1);
                            if (HitEffect)
                            {
                                GameObject obj = Instantiate(HitEffect);
                                obj.transform.position = hit.point;
                                obj.transform.localScale = hitEffectSize;
                                Destroy(obj, 2f);
                            }
                            if (HitEffect2)
                            {
                                GameObject obj = Instantiate(HitEffect2);
                                obj.transform.position = hit.point;
                                obj.transform.localScale = hitEffectSize;
                                Destroy(obj, 2f);
                            }
                        }
                    }
                }
            }
            yield return new WaitForSeconds(0.5f);
            hits = Physics2D.BoxCastAll(transform.position, boxCollSize, 0, Vector2.zero, 0, 1 << 12 | 1 << 23 | 1 << 24);
            if (hits.Length > 0)
            {
                for (int i = 0; i < hits.Length; i++)
                {
                    RaycastHit2D hit = hits[i];

                    if (hit.collider.gameObject.layer.Equals(12))
                    {
                        Vector3 dir = hit.collider.transform.position - transform.position;
                        dir.Normalize();

                        if (hit.collider.GetComponent<MOB.Monster>())
                        {
                            hit.collider.GetComponent<MOB.Monster>().SetDamage(damage, -dir, 1, ELEMENTAL_TYPE.NONE, 0.0f);
                            if (HitEffect)
                            {
                                GameObject obj = Instantiate(HitEffect);
                                obj.transform.position = hit.point;
                                obj.transform.localScale = hitEffectSize;
                                Destroy(obj, 2f);
                            }
                            if (HitEffect2)
                            {
                                GameObject obj = Instantiate(HitEffect2);
                                obj.transform.position = hit.point;
                                obj.transform.localScale = hitEffectSize;
                                Destroy(obj, 2f);
                            }
                        }
                    }
                    else if (hit.collider.gameObject.layer.Equals(24))
                    {
                        if (hit.collider.GetComponent<MOB.BossMonster_Six_Apear>()) hit.collider.GetComponent<MOB.BossMonster_Six_Apear>().SetDamage(damage);
                        if (hit.collider.GetComponent<Boss_STG>()) hit.collider.GetComponent<Boss_STG>().SetDamage(damage, hit.point);
                        if (hit.collider.GetComponent<Boss_NPC>()) hit.collider.GetComponent<Boss_NPC>().SetDamage(damage, hit.point);
                        if (hit.collider.GetComponent<Boss_Golem>()) hit.collider.GetComponent<Boss_Golem>().SetDamage(damage, hit.point);
                        if (hit.collider.GetComponent<Boss_Stone>()) hit.collider.GetComponent<Boss_Stone>().SetDamage(damage, hit.point, false);
                        if (Player.instance.isStrategic_02) Player.instance.isStrategic_02 = false;
                    }
                    else if (hit.collider.gameObject.layer.Equals(23))
                    {
                        if (hit.collider.transform.parent && hit.collider.transform.parent.GetComponent<TentacleController>())
                        {
                            hit.collider.transform.parent.GetComponent<TentacleController>().SetDamage(1);
                            if (HitEffect)
                            {
                                GameObject obj = Instantiate(HitEffect);
                                obj.transform.position = hit.point;
                                obj.transform.localScale = hitEffectSize;
                                Destroy(obj, 2f);
                            }
                            if (HitEffect2)
                            {
                                GameObject obj = Instantiate(HitEffect2);
                                obj.transform.position = hit.point;
                                obj.transform.localScale = hitEffectSize;
                                Destroy(obj, 2f);
                            }
                        }
                        Vector3 dir = hit.collider.gameObject.transform.position - PDG.Player.instance.transform.position;
                        dir.Normalize();
                        if (hit.collider.gameObject.GetComponent<DestroyObjects>())
                        {
                            hit.collider.gameObject.GetComponent<DestroyObjects>().DestroyObject(dir);
                            if (HitEffect)
                            {
                                GameObject obj = Instantiate(HitEffect);
                                obj.transform.position = hit.point;
                                obj.transform.localScale = hitEffectSize;
                                Destroy(obj, 2f);
                            }
                            if (HitEffect2)
                            {
                                GameObject obj = Instantiate(HitEffect2);
                                obj.transform.position = hit.point;
                                obj.transform.localScale = hitEffectSize;
                                Destroy(obj, 2f);
                            }
                        }
                        if (hit.collider.gameObject.GetComponent<ChestHit>() && !hit.collider.gameObject.GetComponent<ChestHit>().isOpenBefore)
                        {
                            hit.collider.gameObject.GetComponent<ChestHit>().SetDamage(1);
                            if (HitEffect)
                            {
                                GameObject obj = Instantiate(HitEffect);
                                obj.transform.position = hit.point;
                                obj.transform.localScale = hitEffectSize;
                                Destroy(obj, 2f);
                            }
                            if (HitEffect2)
                            {
                                GameObject obj = Instantiate(HitEffect2);
                                obj.transform.position = hit.point;
                                obj.transform.localScale = hitEffectSize;
                                Destroy(obj, 2f);
                            }
                        }
                        if (hit.collider.gameObject.GetComponent<DrumHit>())
                        {
                            hit.collider.gameObject.GetComponent<DrumHit>().SetDamage(1);
                            if (HitEffect)
                            {
                                GameObject obj = Instantiate(HitEffect);
                                obj.transform.position = hit.point;
                                obj.transform.localScale = hitEffectSize;
                                Destroy(obj, 2f);
                            }
                            if (HitEffect2)
                            {
                                GameObject obj = Instantiate(HitEffect2);
                                obj.transform.position = hit.point;
                                obj.transform.localScale = hitEffectSize;
                                Destroy(obj, 2f);
                            }
                        }
                    }
                }
            }
            PlayerCombat.ps = PlayerState.Idle;
            yield return new WaitForSeconds(0.5f);
            hitTIme = 0.0f;
            isHit = false;
        }
        IEnumerator multipleBombing()
        {
            GameObject obj = Instantiate(multipleBombingObj, null);
            obj.transform.position = transform.position;
            obj.GetComponent<PlayerAttackController>().hitTime = tickTimeQ;
            ParticleSystem p = obj.transform.Find("Missile_Up").GetComponent<ParticleSystem>();
            float ttt = 0.0f;
            while (true)
            {
                ttt += Time.deltaTime;
                if (!p.isPlaying && ttt < 5.0f) p.Play();
                else if (!p.isPlaying && ttt >= 5.0f) break;
                yield return new WaitForSeconds(Time.deltaTime);
            }
            Destroy(obj, 2.0f);
        }
        IEnumerator TransformSpake(bool isOn)
        {
            if (isOn) SoundManager.instance.StartAudio(new string[1] { "Character/transform_start" }, VOLUME_TYPE.EFFECT);
            else SoundManager.instance.StartAudio(new string[1] { "Character/transform_end" }, VOLUME_TYPE.EFFECT);
            CameraShaker._instance.StartShake(0.1f, 0.01f, 0.5f);
            Image spake = GameObject.Find("Canvas").transform.Find("Spake").GetComponent<Image>();
            float a = 0.0f;
            while (true)
            {
                a += Time.deltaTime * 5;
                spake.color = new Color(1, 1, 1, a);
                if (a > 1.0f) break;
                yield return new WaitForSeconds(Time.deltaTime);
            }
            spake.color = new Color(1, 1, 1, 1);

            rollingTransformObj.SetActive(isOn);

            a = 1.0f;
            while (true)
            {
                a -= Time.deltaTime * 5;
                spake.color = new Color(1, 1, 1, a);
                if (a <= 0.0f) break;
                yield return new WaitForSeconds(Time.deltaTime);
            }
            spake.color = new Color(1, 1, 1, 0);

            isRollingTransform = isOn;
            bColl.enabled = !isOn;
            bbColl.enabled = !isOn;
            cColl.enabled = isOn;
            bcColl.enabled = isOn;

            if (!isOn) SoundManager.instance.StopAudio(new string[1] { "Character/rollingTransformMoving3" });
        }
        private void SwordAttack()
        {
            if (_ArmController.isLeft == 0) return;
            if (!swordControl.gameObject.activeSelf && !isShield)
            {
                hero.WeaponsSlot = swordControl.gameObject;
                swordControl.gameObject.SetActive(true);
            }
            if (gunControl.gameObject.activeSelf) gunControl.gameObject.SetActive(false);
            if ((PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ATTACK], false) || Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_ATTACK], false)) && hero.isWeaponMode && !isFreeze && (fireDelayNow > fireDelay || (isGiant_blade_combo && giant_blade_comboTime > 0.3f && nGiant_blade_count < 3)) && !hero.animator.GetBool("Avoid") && !isUseQ && !UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject())
            {
                swordControl.sr.enabled = true;
                swordControl.AttackType++;
                fireDelayNow = 0.0f;
                if (isCollOpen)
                {
                    bulletTargetObj.GetComponent<Image>().sprite = bTargetNormal;
                    isSpecialAttack = true;
                    bulletTargetObj.SetActive(true);
                    bulletTargetX = -36.0f;
                }
                else
                {
                    bulletTargetObj.SetActive(false);
                }

                if (nowWeaponName.Contains("giant_scythe"))
                {
                    CameraShaker._instance.StartShake(0.01f, 0.01f, 0.1f);
                    SoundManager.instance.StartAudio(new string[1] { "Weapon/" + nowWeaponName }, VOLUME_TYPE.EFFECT);
                    StartCoroutine(Cythe());
                }
                else if (nowWeaponName.Contains("giant_blade"))
                {
                    CameraShaker._instance.StartShake(0.01f, 0.01f, 0.1f);
                    giant_blade_comboTime = 0.0f;
                    isGiant_blade_combo = true;
                    nGiant_blade_count++;

                    SoundManager.instance.StartAudio(new string[1] { "Weapon/" + nowWeaponName }, VOLUME_TYPE.EFFECT);
                    swordControl.animator.SetTrigger("AttackTrigger");
                    StartCoroutine(GiantBladeShot());

                }
                else if (swordControl.isThrow)
                {
                    CameraShaker._instance.StartShake(0.01f, 0.01f, 0.1f);
                    SoundManager.instance.StartAudio(new string[1] { "Weapon/cell_dagger" }, VOLUME_TYPE.EFFECT);
                    StartCoroutine(SwordThrow());
                }
                else if(nowWeaponName.Contains("boss_stg_scythe"))
                {
                    SwingFireDelay -= Time.deltaTime;
                    if (SwingFireDelay <= 0.0f)
                    {
                        SwingFireDelay = 0.2f;
                        StartCoroutine(SwordEffectCoroutine(true));
                        SoundManager.instance.StartAudio(new string[1] { "blade_whip" }, VOLUME_TYPE.EFFECT);
                    }
                    float s = (1 + attackSpeed_boss_stg_scythe) + ((1 + attackSpeed_boss_stg_scythe) * plusAttackSpeed);
                    GetComponent<Animator>().SetFloat("AnimSpeed", s);
                    if (!SwingCollision.activeSelf) SwingCollision.SetActive(true);
                    swordControl.gameObject.SetActive(false);
                    PlayerCombat.ps = PlayerState.Swing;
                    pushCheck3++;
                }
                else if (nowWeaponName.Contains("secret_blade"))
                {
                    CameraShaker._instance.StartShake(0.01f, 0.01f, 0.1f);
                    swordControl.animator.SetTrigger("AttackTrigger");
                    StartCoroutine(SecretBladeAttack());
                    SoundManager.instance.StartAudio(new string[1] { nowWeaponName }, VOLUME_TYPE.EFFECT);
                }
                else if (nowWeaponName.Contains("fire_blade"))
                {
                    CameraShaker._instance.StartShake(0.01f, 0.01f, 0.1f);
                    swordControl.animator.SetTrigger("AttackTrigger");
                    StartCoroutine(SecretBladeAttack());
                    SoundManager.instance.StartAudio(new string[1] { nowWeaponName }, VOLUME_TYPE.EFFECT);
                }
                else if (nowWeaponName.Contains("old_sword"))
                {
                    CameraShaker._instance.StartShake(0.01f, 0.01f, 0.1f);
                    swordControl.animator.SetTrigger("AttackTrigger");
                    SoundManager.instance.StartAudio(new string[1] { "blade_whip" }, VOLUME_TYPE.EFFECT);
                }
                else
                {
                    CameraShaker._instance.StartShake(0.01f, 0.01f, 0.1f);
                    swordControl.animator.SetTrigger("Attack");
                    SoundManager.instance.StartAudio(new string[1] { "blade_whip" }, VOLUME_TYPE.EFFECT);
                    StartCoroutine(SwordEffectCoroutine(false));
                }
                if (_pet != null) _pet.attackTimer = 5.0f;
            }

            if(pushCheck3 == pushCheck4 && PlayerCombat.ps == PlayerState.Swing)
            {
                if (SwingCollision.activeSelf) SwingCollision.SetActive(false);
                pushCheck3 = pushCheck4 = 0;
                PlayerCombat.ps = PlayerState.Idle;
                PlayerCombat.isCompulsionMove = false;
                helmetObj.GetComponent<SpriteRenderer>().enabled = true;
            }
            pushCheck4 = pushCheck3;
        }
        IEnumerator GiantBladeShot()
        {
            for (int i = 0; i < shootCount + plusShootCount; i++)
            {
                for (int j = 0; j < (multiShoot + plusMultiShoot); j++)
                {
                    float angleangle = 0.0f;
                    if ((multiShoot + plusMultiShoot) == 1) angleangle = 0.0f;
                    else
                    {
                        int v = (multiShoot + plusMultiShoot) / 2;
                        v *= 10;
                        angleangle = -v + 10 * ((multiShoot + plusMultiShoot) % 2 == 0 ? j + 1 : j);
                    }
                    SoundManager.instance.StartAudio(new string[1] { nowWeaponName }, VOLUME_TYPE.EFFECT);
                    float randomAngle = Random.Range(-aimAngle, aimAngle);
                    Vector3 firePos = transform.position;
                    Vector3 crossPos = GetPosition(firePos, gun_simulation.rot + randomAngle + angleangle);
                    firePos.z = crossPos.z = 0;
                    Vector3 dir = crossPos - firePos;
                    dir.Normalize();
                    int ran = Random.Range(1, 3);
                    GameObject obj2 = ObjManager.Call().GetObject("Hit_sprite4");
                    obj2.transform.position = transform.position + (dir * 2.5f);
                    obj2.GetComponent<SwordHitEffectController>()._effctName = "giant_blade_" + ran;
                    obj2.GetComponent<SwordHitEffectController>().animSpeed = 1.0f;
                    obj2.GetComponent<SwordHitEffectController>().attackRadius = ran == 2 ? 1.3f : 3;
                    obj2.GetComponent<SwordHitEffectController>().isPenetrate = true;
                    obj2.GetComponent<SwordHitEffectController>().isAttack = true;
                    obj2.GetComponent<SwordHitEffectController>().nRange = bulletRange;
                    obj2.GetComponent<SwordHitEffectController>().sPos = transform.position;
                    obj2.GetComponent<SwordHitEffectController>().hitTime = tickTime;
                    obj2.GetComponent<Animator>().Rebind();
                    obj2.transform.localScale = Vector3.one;
                    obj2.transform.localEulerAngles = new Vector3(0, 0, GetAngle(firePos, crossPos));
                    obj2.SetActive(true);
                    obj2.GetComponent<Rigidbody2D>().AddForce(dir * (bulletSpeed + plusBulletSpeed), ForceMode2D.Impulse);
                }
                yield return new WaitForSeconds(bulletDelay);
            }
        }
        private void SwordAttackRight()
        {
            int mask = 1 << 8 | 1 << 12 | 1 << 24 | 1 << 23 | 1 << 29;
            Vector3 firePos = transform.position;
            Vector3 crossPos = UnityEngine.Camera.main.ScreenToWorldPoint(hero.WeaponsCrossHair.transform.position);
            firePos.z = crossPos.z = 0;
            Vector3 dir = crossPos - firePos;
            dir.Normalize();
            endPos = transform.position + (dir * 10);
            RaycastHit2D[] hit = Physics2D.BoxCastAll(transform.position, new Vector2(1.5f, 1), GetAngle(transform.position, endPos), dir, 10, mask);
            if (hit.Length != 0)
            {
                for (int i = hit.Length - 1; i >= 0; i--)
                {
                    if (hit[i].collider.gameObject.layer.Equals(8) || hit[i].collider.gameObject.layer.Equals(29))
                        endPos = hit[i].transform.position;
                }
            }
            if (nowRightWeaponName.Contains("wind_cutter"))
            {
                if ((PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ATTACK2], false) || Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_ATTACK2], false)) && hero.isWeaponMode && !isArmCollision && !_ArmController.isGrab && !isUseQ && !hero.animator.GetBool("Avoid") && !isFreeze &&
                    (!PDG.Dungeon.instance.nowRoomType.Equals("boss_waiting_2") || !PDG.Dungeon.instance.isStageBoss) &&
                    !UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject() && ((rightCombo_count == 0 && !isRightCooltime) || (isRightCombo && rightCombo_time > 0.3f && rightCombo_count < 3)))
                {
                    if (itemType.Equals("Gun") || itemType.Equals("Arm"))
                        hero.WeaponsSlot.GetComponent<SpriteRenderer>().enabled = false;
                    else
                    {
                        if (hero.WeaponsSlot.transform.GetChild(0).GetComponent<SpriteRenderer>())
                            hero.WeaponsSlot.transform.GetChild(0).GetComponent<SpriteRenderer>().enabled = false;
                    }
                    rightCombo_time = 0.0f;
                    isRightCombo = true;
                    rightCombo_count++;

                    SoundManager.instance.StartAudio(new string[1] { "Character/rushing_ice" }, VOLUME_TYPE.EFFECT);
                    firePos = transform.position;
                    crossPos = UnityEngine.Camera.main.ScreenToWorldPoint(hero.WeaponsCrossHair.transform.position);
                    firePos.z = crossPos.z = 0;
                    dir = crossPos - firePos;
                    dir.Normalize();
                    endPos = transform.position + (dir * 10);
                    hit = Physics2D.BoxCastAll(transform.position + dir, new Vector2(0.5f, 0.5f), GetAngle(transform.position, endPos), dir, 10, mask);
                    if (hit.Length != 0)
                    {
                        for (int i = 0; i < hit.Length; i++)
                        {
                            if (hit[i].collider.gameObject.layer.Equals(8) || hit[i].collider.gameObject.layer.Equals(29))
                            {
                                endPos = (Vector3)hit[i].point - (dir * 1.42f);
                                break;
                            }
                            else if (hit[i].collider.gameObject.layer.Equals(12))
                            {
                                GameObject obj4 = ObjManager.Call().GetObject("Hit_sprite4");
                                obj4.transform.position = transform.position;
                                obj4.transform.localScale = Vector3.one;
                                obj4.GetComponent<SwordHitEffectController>()._effctName = "Hit_" + Random.Range(0, 3);
                                obj4.GetComponent<SwordHitEffectController>().animSpeed = 1.0f;
                                obj4.GetComponent<Animator>().Rebind();
                                obj4.SetActive(true);

                                Vector3 dir2 = hit[i].collider.transform.position - transform.position;
                                dir2.Normalize();
                                hit[i].collider.GetComponent<MOB.Monster>().SetDamage((isRightNear ? damageRightNear : damageRight), -dir2, 2, right_elemetanl, right_elemnetalTime);
                                SoundManager.instance.StartAudio(new string[1] { "Character/rushing_ice_hit" }, VOLUME_TYPE.EFFECT);

                                if (isRightNear && isCriticalRight) 
                                {
                                    GameObject criEffect = ObjManager.Call().GetObject("CriticalHitEffect");
                                    criEffect.transform.position = hit[i].point;
                                    criEffect.SetActive(true);
                                }
                            }
                            else if (hit[i].collider.gameObject.layer.Equals(24))
                            {
                                GameObject obj4 = ObjManager.Call().GetObject("Hit_sprite4");
                                obj4.transform.position = transform.position;
                                obj4.transform.localScale = Vector3.one;
                                obj4.GetComponent<SwordHitEffectController>()._effctName = "Hit_" + Random.Range(0, 3);
                                obj4.GetComponent<SwordHitEffectController>().animSpeed = 1.0f;
                                obj4.GetComponent<Animator>().Rebind();
                                obj4.SetActive(true);
                                if (hit[i].collider.GetComponent<MOB.BossMonster_Six_Apear>()) hit[i].collider.GetComponent<MOB.BossMonster_Six_Apear>().SetDamage(damageRight);
                                if (hit[i].collider.GetComponent<Boss_STG>()) hit[i].collider.GetComponent<Boss_STG>().SetDamage(isRightNear ? damageRightNear : damageRight, hit[i].point);
                                if (hit[i].collider.GetComponent<Boss_NPC>()) hit[i].collider.GetComponent<Boss_NPC>().SetDamage(isRightNear ? damageRightNear : damageRight, hit[i].point);
                                if (hit[i].collider.GetComponent<Boss_Golem>()) hit[i].collider.GetComponent<Boss_Golem>().SetDamage(isRightNear ? damageRightNear : damageRight, hit[i].point);
                                if (hit[i].collider.GetComponent<Boss_Stone>()) hit[i].collider.GetComponent<Boss_Stone>().SetDamage(isRightNear ? damageRightNear : damageRight, hit[i].point, false);
                                SoundManager.instance.StartAudio(new string[1] { "Character/rushing_ice_hit" }, VOLUME_TYPE.EFFECT);
                                if (isRightNear && isCriticalRight) 
                                {
                                    GameObject criEffect = ObjManager.Call().GetObject("CriticalHitEffect");
                                    criEffect.transform.position = hit[i].point;
                                    criEffect.SetActive(true);
                                }
                            }
                            else if (hit[i].collider.gameObject.layer.Equals(23))
                            {
                                if (hit[i].collider.transform.parent && hit[i].collider.transform.parent.GetComponent<TentacleController>())
                                {
                                    hit[i].collider.transform.parent.GetComponent<TentacleController>().SetDamage(1);

                                    GameObject obj4 = ObjManager.Call().GetObject("Hit_sprite4");
                                    obj4.transform.position = transform.position;
                                    obj4.transform.localScale = Vector3.one;
                                    obj4.GetComponent<SwordHitEffectController>()._effctName = "Hit_" + Random.Range(0, 3);
                                    obj4.GetComponent<SwordHitEffectController>().animSpeed = 1.0f;
                                    obj4.GetComponent<Animator>().Rebind();
                                    obj4.SetActive(true);
                                }
                                Vector3 dir2 = hit[i].collider.gameObject.transform.position - transform.position;
                                dir2.Normalize();
                                if (hit[i].collider.gameObject.GetComponent<DestroyObjects>())
                                {
                                    hit[i].collider.gameObject.GetComponent<DestroyObjects>().DestroyObject(dir2);
                                    GameObject obj4 = ObjManager.Call().GetObject("Hit_sprite4");
                                    obj4.transform.position = transform.position;
                                    obj4.transform.localScale = Vector3.one;
                                    obj4.GetComponent<SwordHitEffectController>()._effctName = "Hit_" + Random.Range(0, 3);
                                    obj4.GetComponent<SwordHitEffectController>().animSpeed = 1.0f;
                                    obj4.GetComponent<Animator>().Rebind();
                                    obj4.SetActive(true);
                                }
                                if (hit[i].collider.gameObject.GetComponent<ChestHit>() && !hit[i].collider.gameObject.GetComponent<ChestHit>().isOpenBefore)
                                {
                                    hit[i].collider.gameObject.GetComponent<ChestHit>().SetDamage(1);
                                    GameObject obj4 = ObjManager.Call().GetObject("Hit_sprite4");
                                    obj4.transform.position = transform.position;
                                    obj4.transform.localScale = Vector3.one;
                                    obj4.GetComponent<SwordHitEffectController>()._effctName = "Hit_" + Random.Range(0, 3);
                                    obj4.GetComponent<SwordHitEffectController>().animSpeed = 1.0f;
                                    obj4.GetComponent<Animator>().Rebind();
                                    obj4.SetActive(true);
                                }
                                if (hit[i].collider.gameObject.GetComponent<DrumHit>())
                                {
                                    hit[i].collider.gameObject.GetComponent<DrumHit>().SetDamage(1);
                                    GameObject obj4 = ObjManager.Call().GetObject("Hit_sprite4");
                                    obj4.transform.position = transform.position;
                                    obj4.transform.localScale = Vector3.one;
                                    obj4.GetComponent<SwordHitEffectController>()._effctName = "Hit_" + Random.Range(0, 3);
                                    obj4.GetComponent<SwordHitEffectController>().animSpeed = 1.0f;
                                    obj4.GetComponent<Animator>().Rebind();
                                    obj4.SetActive(true);
                                }

                            }
                        }
                    }
                    GameObject obj3 = ObjManager.Call().GetObject("Hit_sprite7");
                    obj3.transform.position = transform.position;
                    obj3.transform.localScale = Vector3.one;
                    obj3.GetComponent<SwordHitEffectController>()._effctName = "wind_cutter";
                    obj3.GetComponent<SwordHitEffectController>().animSpeed = 0.5f;
                    obj3.GetComponent<Animator>().Rebind();
                    obj3.SetActive(true);
                    obj3.GetComponent<SwordHitEffectController>().isPlayerTarget = true;

                    Vector3 centerPos = (endPos - transform.position) / 2;
                    centerPos = transform.position + centerPos;
                    GameObject obj2 = ObjManager.Call().GetObject("Hit_sprite7");
                    obj2.transform.position = centerPos;
                    obj2.transform.localScale = Vector3.one;
                    obj2.transform.localEulerAngles = new Vector3(0, 0, GetAngle(transform.position, endPos));
                    obj2.GetComponent<SwordHitEffectController>()._effctName = "wind_cutter2";
                    obj2.GetComponent<SwordHitEffectController>().animSpeed = 1.0f;
                    obj2.GetComponent<Animator>().Rebind();
                    obj2.SetActive(true);
                    transform.position = endPos;
                    if (nowRightWeaponName.Equals("wind_cutter_1")) 
                    {
                        GameObject obj123 = Instantiate(windCutter_1Effect);
                        obj123.transform.position = transform.position;
                        obj123.GetComponent<SingleAttackController>().SettingDamage(isRightNear ? damageRightNear : damageRight, true);
                        Destroy(obj123, 2f);
                    }
                    rightInTime = 1.0f;
                    if (itemType.Equals("Gun") || itemType.Equals("Arm"))
                        hero.WeaponsSlot.GetComponent<SpriteRenderer>().enabled = true;
                }
            }
            else
            {
                if ((PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ATTACK2], false) || Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_ATTACK2], false)) && hero.isWeaponMode && !isArmCollision && !_ArmController.isGrab && !isUseQ && !isFreeze
                    && (!PDG.Dungeon.instance.nowRoomType.Equals("boss_waiting_2") || !PDG.Dungeon.instance.isStageBoss) 
                    && !hero.animator.GetBool("Avoid") && !UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject() && !isRightCooltime)
                {
                    SoundManager.instance.StartAudio(new string[1] { "Character/rushing_ice" }, VOLUME_TYPE.EFFECT);
                    firePos = transform.position;
                    crossPos = UnityEngine.Camera.main.ScreenToWorldPoint(hero.WeaponsCrossHair.transform.position);
                    firePos.z = crossPos.z = 0;
                    dir = crossPos - firePos;
                    dir.Normalize();
                    endPos = transform.position + (dir * 10);
                    hit = Physics2D.BoxCastAll(transform.position + dir, new Vector2(1.5f, 0.5f), GetAngle(transform.position, endPos), dir, 10, mask);
                    if (hit.Length != 0)
                    {
                        for (int i = 0; i < hit.Length; i++)
                        {
                            if (hit[i].collider.gameObject.layer.Equals(8) || hit[i].collider.gameObject.layer.Equals(29))
                            {
                                endPos = (Vector3)hit[i].point - (dir * 1.42f);
                                break;
                            }
                            else if (hit[i].collider.gameObject.layer.Equals(12))
                            {
                                GameObject obj3 = ObjManager.Call().GetObject("Hit_sprite9");
                                obj3.transform.position = hit[i].collider.transform.position;
                                obj3.transform.localScale = Vector3.one;
                                obj3.transform.localEulerAngles = new Vector3(0, 0, 0);
                                obj3.GetComponent<SwordHitEffectController>()._effctName = "rushing_ice_hit";
                                obj3.GetComponent<SwordHitEffectController>().animSpeed = 1.0f;
                                obj3.GetComponent<Animator>().Rebind();
                                obj3.SetActive(true);

                                Vector3 dir2 = hit[i].collider.transform.position - transform.position;
                                dir2.Normalize();

                                hit[i].collider.GetComponent<MOB.Monster>().SetDamage((isRightNear ? damageRightNear : damageRight), -dir2, 2, right_elemetanl, right_elemnetalTime);
                                SoundManager.instance.StartAudio(new string[1] { "Character/rushing_ice_hit" }, VOLUME_TYPE.EFFECT);
                                if (isRightNear && isCriticalRight)
                                {
                                    GameObject criEffect = ObjManager.Call().GetObject("CriticalHitEffect");
                                    criEffect.transform.position = hit[i].point;
                                    criEffect.SetActive(true);
                                }
                            }
                            else if (hit[i].collider.gameObject.layer.Equals(23))
                            {
                                if (hit[i].collider.transform.parent && hit[i].collider.transform.parent.GetComponent<TentacleController>())
                                {
                                    hit[i].collider.transform.parent.GetComponent<TentacleController>().SetDamage(1);
                                    GameObject obj3 = ObjManager.Call().GetObject("Hit_sprite9");
                                    obj3.transform.position = hit[i].collider.transform.position;
                                    obj3.transform.localScale = Vector3.one;
                                    obj3.transform.localEulerAngles = new Vector3(0, 0, 0);
                                    obj3.GetComponent<SwordHitEffectController>()._effctName = "rushing_ice_hit";
                                    obj3.GetComponent<SwordHitEffectController>().animSpeed = 1.0f;
                                    obj3.GetComponent<Animator>().Rebind();
                                    obj3.SetActive(true);
                                }
                                Vector3 dir2 = hit[i].collider.gameObject.transform.position - transform.position;
                                dir2.Normalize();
                                if (hit[i].collider.gameObject.GetComponent<DestroyObjects>())
                                {
                                    hit[i].collider.gameObject.GetComponent<DestroyObjects>().DestroyObject(dir2);
                                    GameObject obj3 = ObjManager.Call().GetObject("Hit_sprite9");
                                    obj3.transform.position = hit[i].collider.transform.position;
                                    obj3.transform.localScale = Vector3.one;
                                    obj3.transform.localEulerAngles = new Vector3(0, 0, 0);
                                    obj3.GetComponent<SwordHitEffectController>()._effctName = "rushing_ice_hit";
                                    obj3.GetComponent<SwordHitEffectController>().animSpeed = 1.0f;
                                    obj3.GetComponent<Animator>().Rebind();
                                    obj3.SetActive(true);
                                }
                                if (hit[i].collider.gameObject.GetComponent<ChestHit>() && !hit[i].collider.gameObject.GetComponent<ChestHit>().isOpenBefore)
                                {
                                    hit[i].collider.gameObject.GetComponent<ChestHit>().SetDamage(1);
                                    GameObject obj3 = ObjManager.Call().GetObject("Hit_sprite9");
                                    obj3.transform.position = hit[i].collider.transform.position;
                                    obj3.transform.localScale = Vector3.one;
                                    obj3.transform.localEulerAngles = new Vector3(0, 0, 0);
                                    obj3.GetComponent<SwordHitEffectController>()._effctName = "rushing_ice_hit";
                                    obj3.GetComponent<SwordHitEffectController>().animSpeed = 1.0f;
                                    obj3.GetComponent<Animator>().Rebind();
                                    obj3.SetActive(true);
                                }
                                if (hit[i].collider.gameObject.GetComponent<DrumHit>())
                                {
                                    hit[i].collider.gameObject.GetComponent<DrumHit>().SetDamage(1);
                                    GameObject obj3 = ObjManager.Call().GetObject("Hit_sprite9");
                                    obj3.transform.position = hit[i].collider.transform.position;
                                    obj3.transform.localScale = Vector3.one;
                                    obj3.transform.localEulerAngles = new Vector3(0, 0, 0);
                                    obj3.GetComponent<SwordHitEffectController>()._effctName = "rushing_ice_hit";
                                    obj3.GetComponent<SwordHitEffectController>().animSpeed = 1.0f;
                                    obj3.GetComponent<Animator>().Rebind();
                                    obj3.SetActive(true);
                                }

                            }
                            else if (hit[i].collider.gameObject.layer.Equals(24))
                            {
                                GameObject obj3 = ObjManager.Call().GetObject("Hit_sprite9");
                                obj3.transform.position = hit[i].collider.transform.position;
                                obj3.transform.localScale = Vector3.one;
                                obj3.transform.localEulerAngles = new Vector3(0, 0, 0);
                                obj3.GetComponent<SwordHitEffectController>()._effctName = "rushing_ice_hit";
                                obj3.GetComponent<SwordHitEffectController>().animSpeed = 1.0f;
                                obj3.GetComponent<Animator>().Rebind();
                                obj3.SetActive(true);
                                if (hit[i].collider.GetComponent<MOB.BossMonster_Six_Apear>()) hit[i].collider.GetComponent<MOB.BossMonster_Six_Apear>().SetDamage(damageRight);
                                if (hit[i].collider.GetComponent<Boss_STG>()) hit[i].collider.GetComponent<Boss_STG>().SetDamage(isRightNear ? damageRightNear : damageRight, hit[i].point);
                                if (hit[i].collider.GetComponent<Boss_NPC>()) hit[i].collider.GetComponent<Boss_NPC>().SetDamage(isRightNear ? damageRightNear : damageRight, hit[i].point);
                                if (hit[i].collider.GetComponent<Boss_Golem>()) hit[i].collider.GetComponent<Boss_Golem>().SetDamage(isRightNear ? damageRightNear : damageRight, hit[i].point);
                                if (hit[i].collider.GetComponent<Boss_Stone>()) hit[i].collider.GetComponent<Boss_Stone>().SetDamage(isRightNear ? damageRightNear : damageRight, hit[i].point, false);
                                SoundManager.instance.StartAudio(new string[1] { "Character/rushing_ice_hit" }, VOLUME_TYPE.EFFECT);

                                if (isRightNear && isCriticalRight)
                                {
                                    GameObject criEffect = ObjManager.Call().GetObject("CriticalHitEffect");
                                    criEffect.transform.position = hit[i].point;
                                    criEffect.SetActive(true);
                                }
                            }
                        }
                    }
                    Vector3 centerPos = (endPos - transform.position) / 2;
                    centerPos = transform.position + centerPos;
                    GameObject obj2 = ObjManager.Call().GetObject("Hit_sprite7");
                    obj2.transform.position = centerPos;
                    obj2.transform.localScale = Vector3.one;
                    obj2.transform.localEulerAngles = new Vector3(0, 0, GetAngle(transform.position, endPos));
                    obj2.GetComponent<SwordHitEffectController>()._effctName = "rushing_ice";
                    obj2.GetComponent<SwordHitEffectController>().animSpeed = 1.0f;
                    obj2.GetComponent<Animator>().Rebind();
                    obj2.SetActive(true);
                    isRightCooltime = true;
                    Inventorys.Instance.rightCollTimeImage.transform.parent.Find("CollTimeImageP").gameObject.SetActive(true);
                    rightCollTimeNow = 0.0f;
                    transform.position = endPos;
                    rightInTime = 1.0f;
                }
            }
        }
        private void ArmRightAttack()
        {
            if (nowRightWeaponName.Contains("living_arrow") || nowRightWeaponName.Contains("secret_lasergun") || nowRightWeaponName.Contains("secret_watergun") || nowRightWeaponName.Contains("rocket_punch") || nowRightWeaponName.Contains("multi_canon"))
            {
                if ((PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ATTACK2], false) || Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_ATTACK2], false)) && hero.isWeaponMode && !isUseQ && !hero.animator.GetBool("Avoid") &&
                    (pushCheck3 == 0 ? right_defence_gauge > 4 : right_defence_gauge > 0.1f) && !UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject() && !isFreeze)
                {
                    if (!gunControl.transform.Find("living_arrow_weapon").gameObject.activeSelf) gunControl.transform.Find("living_arrow_weapon").gameObject.SetActive(true);
                    if (!swordControl.transform.Find("living_arrow_weapon").gameObject.activeSelf) swordControl.transform.Find("living_arrow_weapon").gameObject.SetActive(true);
                    if (isCollOpen)
                    {
                        bulletTargetObj.GetComponent<Image>().sprite = bTargetNormal;
                        isSpecialAttack = true;
                        bulletTargetObj.SetActive(true);
                        bulletTargetX = -36.0f;
                    }
                    else
                    {
                        bulletTargetObj.SetActive(false);
                    }

                    if (fireDelayRightNow > fireDelayRight)
                    {
                        rightAttackCount++;
                        StartCoroutine(FireRight(rightAttackCount));
                        fireDelayRightNow = 0.0f;
                    }
                    pushCheck3++;
                    if (nowRightWeaponName.Contains("rocket_punch"))
                        right_defence_gauge -= 0.1f;
                    else if (nowRightWeaponName.Contains("living_arrow"))
                        right_defence_gauge -= 0.22f;
                    else
                        right_defence_gauge -= 0.3f;
                    if (_pet != null) _pet.attackTimer = 5.0f;
                    _ArmController.isLeft = 0;
                }
                if (pushCheck3 == pushCheck4 && pushCheck3 != 0)
                {
                    pushCheck3 = pushCheck4 = 0;
                    _ArmController.isLeft = -1;

                    if (nowRightWeaponName.Contains("rocket_punch") || nowRightWeaponName.Contains("multi_canon"))
                    {

                    }
                    else
                    {
                        if (gunControl.transform.Find("living_arrow_weapon").gameObject.activeSelf) gunControl.transform.Find("living_arrow_weapon").gameObject.SetActive(false);
                        if (swordControl.transform.Find("living_arrow_weapon").gameObject.activeSelf) swordControl.transform.Find("living_arrow_weapon").gameObject.SetActive(false);
                    }
                }
                else if (pushCheck3 != 0)
                {
                    if (transform.Find("GreenFatLaserBeam").gameObject.activeSelf) transform.Find("GreenFatLaserBeam").gameObject.SetActive(false);
                    if (transform.Find("GreenFatLaserEnd").gameObject.activeSelf) transform.Find("GreenFatLaserEnd").gameObject.SetActive(false);
                    if (transform.Find("GreenFatLaserStart").gameObject.activeSelf) transform.Find("GreenFatLaserStart").gameObject.SetActive(false);
                    if (weaponAnim != null && weaponAnim.runtimeAnimatorController != null) weaponAnim.SetBool("Attack", false);
                    t = 0;
                    for (int i = 0; i < ArmList.Count; i++) ArmList[i].SetActive(false);
                    gunControl.sr.enabled = false;
                    swordControl.swordObj.GetComponent<SpriteRenderer>().enabled = false;
                }
                pushCheck4 = pushCheck3;
            }
            else if (nowRightWeaponName.Contains("energy_release") || nowRightWeaponName.Contains("frame_nova") || nowRightWeaponName.Contains("frozen_nova"))
            {
                if ((PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ATTACK2], false) || Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_ATTACK2], false)) && hero.isWeaponMode && !isUseQ && !hero.animator.GetBool("Avoid") &&
                    right_defence_gauge >= 3.0f && fireDelayRightNow > fireDelayRight && !UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject() && !isFreeze)
                {
                    if (isCollOpen)
                    {
                        bulletTargetObj.GetComponent<Image>().sprite = bTargetNormal;
                        isSpecialAttack = true;
                        bulletTargetObj.SetActive(true);
                        bulletTargetX = -36.0f;
                    }
                    else
                    {
                        bulletTargetObj.SetActive(false);
                    }

                    GameObject obj = Instantiate(NovaAttackObj);
                    obj.GetComponent<PlayerAttackController>().hitTime = tickTimeR;
                    obj.transform.position = transform.position;
                    Destroy(obj, (nowRightWeaponName.Contains("energy_release") ? 0.89f : (nowRightWeaponName.Contains("_2") ? 0.89f : 0.3f)));

                    fireDelayRightNow = 0.0f;
                    right_defence_gauge -= (nowRightWeaponName.Contains("frozen_nova") ? 1.5f : 3.0f);
                    if (_pet != null) _pet.attackTimer = 5.0f;
                }
            }
        }
        IEnumerator SecretBladeAttack()
        {
            for (int i = 0; i < (nowWeaponName.Equals("secret_blade") ? 3 : 2); i++) 
            {
                GameObject obj = Instantiate(SecretBladeObj);

                Vector3 firePos = hero.WeaponsFireTransform.position;
                Vector3 crossPos = GetPosition(firePos, gun_simulation.rot);
                firePos.z = crossPos.z = 0;
                Vector3 dir = crossPos - firePos;
                dir.Normalize();

                obj.transform.position = firePos + (dir * 3);
                obj.transform.localEulerAngles = new Vector3(Random.Range(0, 180), Random.Range(0, 180), Random.Range(0, 180));
                Destroy(obj, 1f);
                yield return new WaitForSeconds(0.25f);
            }
        }
        IEnumerator Cythe()
        {
            float randomAngle = Random.Range(-aimAngle, aimAngle);
            Vector3 firePos = transform.position;
            firePos.y += 0.75f;
            Vector3 crossPos = UnityEngine.Camera.main.ScreenToWorldPoint(hero.WeaponsCrossHair.transform.position);
            firePos.z = crossPos.z = 0;
            Vector3 dir = crossPos - firePos;
            dir.Normalize();
            float angle = GetAngle(firePos, crossPos);
            GameObject obj = ObjManager.Call().GetObject("SwordEffect2");
            obj.transform.position = firePos + (dir * 1.5f);
            obj.GetComponent<SwordEffect>().ownerObj = gameObject;
            obj.transform.localEulerAngles = new Vector3(0, 0, angle);
            obj.GetComponent<SwordEffect>()._angle = angle;
            obj.GetComponent<SwordEffect>().fSize = 2.5f;
            obj.GetComponent<SwordEffect>().destroyDistance = bulletRange;
            obj.GetComponent<SwordEffect>()._weaponName = nowWeaponName;
            obj.GetComponent<SwordEffect>().isAttack = true;
            obj.SetActive(true);
            PlayerCombat.isCompulsionMove = true;
            Vector3 pos = transform.position;
            for (int i = 0; i < 3; i++)
            {
                SoundManager.instance.StartAudio(new string[1] { "Weapon/" + nowWeaponName }, VOLUME_TYPE.EFFECT);
                GameObject obj2 = ObjManager.Call().GetObject("Hit_sprite4");
                obj2.transform.position = pos;
                obj2.GetComponent<SwordHitEffectController>()._effctName = "Hit_" + UnityEngine.Random.Range(3, 6);
                obj2.GetComponent<SwordHitEffectController>().animSpeed = 1.0f;
                obj2.GetComponent<SwordHitEffectController>().isPenetrate = false;
                obj2.GetComponent<SwordHitEffectController>().isAttack = true;
                obj2.GetComponent<SwordHitEffectController>().attackRadius = 4;
                obj2.GetComponent<SwordHitEffectController>().sPos = transform.position;
                obj2.GetComponent<Animator>().Rebind();
                obj2.transform.localScale = Vector3.one;
                obj2.transform.localEulerAngles = Vector2.zero;
                obj2.SetActive(true);
                yield return new WaitForSeconds(0.3f);
            }
        }
        IEnumerator SwordThrow()
        {
            swordControl.sr.enabled = false;
            float randomAngle = Random.Range(-aimAngle, aimAngle);
            Vector3 firePos = transform.position;
            firePos.y += 0.75f;
            Vector3 crossPos = UnityEngine.Camera.main.ScreenToWorldPoint(hero.WeaponsCrossHair.transform.position);
            firePos.z = crossPos.z = 0;
            Vector3 dir = crossPos - firePos;
            dir.Normalize();
            float angle = GetAngle(firePos, crossPos);
            for (int i = 0; i < shootCount + plusShootCount; i++)
            {
                for (int j = 0; j < (multiShoot + plusMultiShoot); j++)
                {
                    float angleangle = 0.0f;
                    if ((multiShoot + plusMultiShoot) == 1) angleangle = 0.0f;
                    else
                    {
                        int v = (multiShoot + plusMultiShoot) / 2;
                        v *= 10;
                        angleangle = -v + (10 * ((multiShoot + plusMultiShoot) % 2 == 0 ? j + 1 : j));
                    }
                    GameObject obj = ObjManager.Call().GetObject("SwordThrow");
                    obj.transform.position = firePos + (dir * 1.5f);
                    obj.transform.localEulerAngles = new Vector3(0, 0, angle + randomAngle + angleangle);
                    obj.GetComponent<SwordThrow>().ownerObj = gameObject;
                    obj.GetComponent<SwordThrow>()._angle = angle + randomAngle + angleangle;
                    obj.GetComponent<SwordThrow>().destroyDistance = bulletRange;
                    obj.GetComponent<SwordThrow>().nowWeaponName = nowWeaponName;
                    obj.GetComponent<SwordThrow>().isRotate = true;
                    obj.SetActive(true);
                    dir = GetPosition(obj.transform.position, angle + randomAngle + angleangle) - obj.transform.position;
                    dir.Normalize();
                    obj.GetComponent<Rigidbody2D>().AddForce(dir * forcePow, ForceMode2D.Impulse);
                    StartCoroutine(CameraQuake(0.05f, 0.001f));
                }
                yield return new WaitForSeconds(bulletDelay);
            }
            yield return new WaitForSeconds(fireDelay / 2);
            swordControl.sr.enabled = true;
        }
        IEnumerator SwordEffectCoroutine(bool isSwing)
        {
            float randomAngle = Random.Range(-aimAngle, aimAngle);
            Vector3 firePos = (isSwing ? SwingFirePosition.position : transform.position);
            firePos.y += 0.75f;
            Vector3 crossPos = UnityEngine.Camera.main.ScreenToWorldPoint(hero.WeaponsCrossHair.transform.position);
            firePos.z = crossPos.z = 0;
            Vector3 dir = (isSwing ? firePos - transform.position : crossPos - firePos);
            dir.Normalize();

            float angle = (isSwing ? GetAngle(transform.position, firePos) : GetAngle(firePos, crossPos));

            GameObject obj = null;
            if (!isSwing)
            {
                obj = ObjManager.Call().GetObject("SwordEffect");
                obj.transform.position = firePos + (dir * 1.5f);
                obj.transform.localEulerAngles = new Vector3(0, 0, angle);
                obj.GetComponent<SwordEffect>().ownerObj = null;
                obj.GetComponent<SwordEffect>()._angle = angle;
                obj.GetComponent<SwordEffect>().destroyDistance = bulletRange;
                obj.GetComponent<SwordEffect>().fSize = 2.5f;
                obj.GetComponent<SwordEffect>()._weaponName = nowWeaponName;
                obj.GetComponent<SwordEffect>().isAttack = true;
                obj.SetActive(true);
            }

            for (int i = 0; i < shootCount + plusShootCount; i++)
            {
                for (int j = 0; j < (multiShoot + plusMultiShoot); j++)
                {
                    float angleangle = 0.0f;
                    if ((multiShoot + plusMultiShoot) == 1) angleangle = 0.0f;
                    else
                    {
                        int v = (multiShoot + plusMultiShoot) / 2;
                        v *= 10;
                        angleangle = -v + (10 * ((multiShoot + plusMultiShoot) % 2 == 0 ? j + 1 : j));
                    }
                    GameObject obj2 = ObjManager.Call().GetObject("SwordForce");
                    obj2.transform.position = firePos + (dir * (isSwing ? 1.5f : 4f));
                    obj2.transform.localEulerAngles = new Vector3(0, 0, angle + randomAngle + angleangle);
                    obj2.GetComponent<SwordEffect>().ownerObj = gameObject;
                    obj2.GetComponent<SwordEffect>().firepos = transform.position;
                    obj2.GetComponent<SwordEffect>()._angle = angle + randomAngle + angleangle;
                    obj2.GetComponent<SwordEffect>()._weaponName = nowWeaponName;
                    obj2.GetComponent<SwordEffect>().fSize = 0.55f;
                    obj2.GetComponent<SwordEffect>().destroyDistance = bulletRange * (isSwing ? 1 : 2);
                    obj2.GetComponent<SwordEffect>().isAttack = true;
                    obj2.SetActive(true);
                    dir = GetPosition(obj2.transform.position, angle + randomAngle + angleangle) - obj2.transform.position;
                    dir.Normalize();
                    obj2.GetComponent<Rigidbody2D>().AddForce(dir * (isSwing ? 12 : forcePow), ForceMode2D.Impulse);
                    //StartCoroutine(CameraQuake(0.05f, 0.001f));
                }
                yield return new WaitForSeconds(bulletDelay);
            }
            yield return new WaitForSeconds(1.0f);
            if (!isSwing) obj.SetActive(false);
        }
        IEnumerator QShield()
        {
            isHit = true;
            hitTIme = 3.0f;
            PlayerShield.SetActive(true);
            yield return new WaitForSeconds(3.0f);
            PlayerShield.SetActive(false);
            isHit = false;
        }
        private GameObject GetArmPoolingObject()
        {
            for (int i = 0; i < ArmList.Count; i++)
            {
                if (!ArmList[i].activeSelf) return ArmList[i];
            }
            GameObject obj = Instantiate(armPrefabs);
            obj.transform.SetParent(ArmParent);
            ArmList.Add(obj);
            return obj;
           
        }
        private void SpecialFire()
        {
            StartCoroutine(SpecialFireCoroutine());
        }
        IEnumerator SpecialFireCoroutine()
        {
            GameObject obj2 = ObjManager.Call().GetObject("elastic_cell_weapon_fireEffect");
            obj2.transform.position = gunControl.FirePostionTransform.position;
            obj2.SetActive(true);

            gunControl.animator.SetTrigger("AttackTrigger");
            for (int i = 0; i < shootCount + plusShootCount; i++)
            {
                SoundManager.instance.StartAudio(new string[1] { "Weapon/" + nowWeaponName }, VOLUME_TYPE.EFFECT);
                for (int j = 0; j < multiShoot + plusMultiShoot; j++)
                {
                    float angleangle = 0.0f;
                    if (multiShoot + plusMultiShoot == 1) angleangle = 0.0f;
                    else
                    {
                        int r = Random.Range(8, 12);
                        int v = (multiShoot + plusMultiShoot) / 2;
                        v *= r;
                        angleangle = -v + (r * ((multiShoot + plusMultiShoot) % 2 == 0 ? j + 1 : j));
                    }
                    SoundManager.instance.StartAudio(new string[1] { nowRightWeaponName }, VOLUME_TYPE.EFFECT);
                    float randomAngle = Random.Range(-aimAngle, aimAngle);

                    GameObject obj = ObjManager.Call().GetObject("elastic_cell_weapon_bullet_special");
                    Vector3 firePos = hero.WeaponsFireTransform.position;
                    Vector3 crossPos = GetPosition(firePos, gun_simulation.rot + randomAngle + angleangle);
                    firePos.z = crossPos.z = 0;
                    Vector3 dir = crossPos - firePos;
                    dir.Normalize();
                    obj.transform.position = firePos + (dir * 1.05f);
                    obj.SetActive(true);
                    obj.GetComponent<PlayerAttackController>().firePosition = transform.position;
                    obj.GetComponent<PlayerAttackController>().destroyDistance = bulletRange;
                    obj.GetComponent<PlayerAttackController>().RigidBodyFire(dir, bulletSpeed);
                    StartCoroutine(CameraQuake(0.05f, 0.001f));
                }
                yield return new WaitForSeconds(bulletDelay);
            }
        }
        public IEnumerator FireRight(long _rightAttackCount)
        {
            if (nowRightWeaponName.Contains("rocket_punch") || nowRightWeaponName.Contains("multi_canon"))
            {
                if (!gunControl.transform.Find("living_arrow_weapon").GetComponent<Animator>().GetBool("isAttack"))
                {
                    SoundManager.instance.StartAudio(new string[1] { "Weapon/" + nowRightWeaponName }, VOLUME_TYPE.EFFECT);
                    gunControl.transform.Find("living_arrow_weapon").GetComponent<Animator>().SetBool("isAttack", true);
                }
                if (!swordControl.transform.Find("living_arrow_weapon").GetComponent<Animator>().GetBool("isAttack"))
                {
                    SoundManager.instance.StartAudio(new string[1] { "Weapon/" + nowRightWeaponName }, VOLUME_TYPE.EFFECT);
                    swordControl.transform.Find("living_arrow_weapon").GetComponent<Animator>().SetBool("isAttack", true);
                }
                if(nowRightWeaponName.Contains("multi_canon"))
                    yield return new WaitForSeconds(0.5f);
                else
                    yield return new WaitForSeconds(0.15f);
            }
            else
                SoundManager.instance.StartAudio(new string[1] { "Weapon/" + nowRightWeaponName }, VOLUME_TYPE.EFFECT);

            ItemsData rightData = System.Array.Find(SheetManager.Instance.ItemsDB.dataArray, item => item.ID.Equals(nowRightWeaponName));

            string tempID = rightData.ID;
            int ttt = tempID.LastIndexOf('_');
            if (ttt != -1 && ttt == tempID.Length - 2)
                tempID = tempID.Substring(0, tempID.Length - 2);

            int pDam = 0;
            int pCri = 0;
            int pCriD = 0;
            int pRange = 0;
            int pAim = 0;
            int pMulti = 0;
            int pShot = 0;
            int pShotD = 0;
            ManufacturesData loadData = SaveAndLoadManager.instance.LoadManufacture(tempID, PDG.Dungeon.instance.isBossRush);
            if (!loadData.id.Equals(string.Empty) || !loadData.id.Equals(""))
            {
                for (int i = 0; i < loadData.stateid.Count; i++)
                {
                    if (loadData.stateid[i].ToUpper().Equals("Attack".ToUpper()))
                        pDam = loadData.statevalue[i];
                    else if (loadData.stateid[i].ToUpper().Equals("Critical".ToUpper()))
                        pCri = loadData.statevalue[i];
                    else if (loadData.stateid[i].ToUpper().Equals("CriticalDamage".ToUpper()))
                        pCriD = loadData.statevalue[i];
                    else if (loadData.stateid[i].ToUpper().Equals("Range".ToUpper()))
                        pRange = loadData.statevalue[i];
                    else if (loadData.stateid[i].ToUpper().Equals("Aim".ToUpper()))
                        pAim = loadData.statevalue[i];
                    else if (loadData.stateid[i].ToUpper().Equals("MultiShoot".ToUpper()))
                        pMulti = loadData.statevalue[i];
                    else if (loadData.stateid[i].ToUpper().Equals("ShootCount".ToUpper()))
                        pShot = loadData.statevalue[i];
                    else if (loadData.stateid[i].ToUpper().Equals("ShootDelay".ToUpper()))
                        pShotD = loadData.statevalue[i];
                }
            }
            Vector3 fixedPosition = hero.WeaponsFireTransform.position;
            float fixedAngle = gun_simulation.rot;

            float aim = rightData.Aim - pAim;
            float bDelay = rightData.Bulletdelay;
            if (aim <= 0) aim = 0;
            if (bDelay <= 0) bDelay = 0;
            for (int i = 0; i < rightData.Shootcount + plusShootCount + pShot; i++)
            {
                SoundManager.instance.StartAudio(new string[1] { "Weapon/" + nowRightWeaponName + "_bullet" }, VOLUME_TYPE.EFFECT);
                for (int j = 0; j < rightData.Multishoot + plusMultiShoot + pMulti; j++)
                {
                    float angleangle = 0.0f;
                    if (rightData.Multishoot + plusMultiShoot  + pMulti == 1) angleangle = 0.0f;
                    else
                    {
                        int r = Random.Range(8, 12);
                        int v = (rightData.Multishoot + plusMultiShoot+ pMulti) / 2;
                        v *= r;
                        angleangle = -v + (r * ((rightData.Multishoot + plusMultiShoot+ pMulti) % 2 == 0 ? j + 1 : j));
                    }
                    SoundManager.instance.StartAudio(new string[1] { nowRightWeaponName }, VOLUME_TYPE.EFFECT);
                    float randomAngle = Random.Range(-aim, aim);
                    if (nowRightWeaponName.Contains("living_arrow"))
                    {
                        GameObject obj = ObjManager.Call().GetObject("Bullet");
                        obj.GetComponent<BulletController>().isMonster = false;
                        obj.GetComponent<BulletController>().firePosition = transform.position;
                        obj.GetComponent<BulletController>().bulletType = nowRightWeaponName;
                        obj.GetComponent<BulletController>().fSpeed = rightData.Bulletspeed + plusBulletSpeed;
                        obj.GetComponent<BulletController>().fAngle = gun_simulation.rot + randomAngle + angleangle + 45f;
                        obj.GetComponent<BulletController>().fRange = rightData.Range + pRange;
                        obj.GetComponent<BulletController>().isPenetrate = true;
                        obj.GetComponent<BulletController>().isRightAttack = true;
                        Vector3 firePos = transform.position;
                        Vector3 crossPos = GetPosition(firePos, (isGunFixedPos ? fixedAngle : gun_simulation.rot) + randomAngle + angleangle);
                        firePos.z = crossPos.z = 0;
                        Vector3 dir = crossPos - firePos;
                        dir.Normalize();
                        obj.GetComponent<BulletController>().dir = dir;
                        obj.transform.position = firePos + (dir * 1.05f);
                        obj.SetActive(true);
                        obj.GetComponent<BulletController>().BulletFire();
                        StartCoroutine(CameraQuake(0.05f, 0.001f));
                    }
                    else if (nowRightWeaponName.Contains("secret_lasergun"))
                    {
                        GameObject obj = ObjManager.Call().GetObject("Bullet");
                        obj.GetComponent<BulletController>().isMonster = false;
                        obj.GetComponent<BulletController>().firePosition = transform.position;
                        obj.GetComponent<BulletController>().bulletType = nowRightWeaponName;
                        obj.GetComponent<BulletController>().fSpeed = rightData.Bulletspeed + plusBulletSpeed;
                        obj.GetComponent<BulletController>().fAngle = gun_simulation.rot + randomAngle + angleangle;
                        obj.GetComponent<BulletController>().fRange = rightData.Range + pRange;
                        obj.GetComponent<BulletController>().isPenetrate = false;
                        obj.GetComponent<BulletController>().isRightAttack = true;
                        Vector3 firePos = transform.position;
                        Vector3 crossPos = GetPosition(firePos, (isGunFixedPos ? fixedAngle : gun_simulation.rot) + randomAngle + angleangle);
                        firePos.z = crossPos.z = 0;
                        Vector3 dir = crossPos - firePos;
                        dir.Normalize();
                        obj.GetComponent<BulletController>().dir = dir;
                        obj.transform.position = firePos + (dir * 1.05f);
                        obj.SetActive(true);
                        obj.GetComponent<BulletController>().BulletFire();
                        StartCoroutine(CameraQuake(0.05f, 0.001f));
                    }
                    else if (nowRightWeaponName.Contains("secret_watergun"))
                    {
                        GameObject obj = ObjManager.Call().GetObject("Bullet");
                        obj.GetComponent<BulletController>().isMonster = false;
                        obj.GetComponent<BulletController>().firePosition = transform.position;
                        obj.GetComponent<BulletController>().bulletType = nowRightWeaponName;
                        obj.GetComponent<BulletController>().fSpeed = rightData.Bulletspeed + plusBulletSpeed;
                        obj.GetComponent<BulletController>().fAngle = gun_simulation.rot;
                        obj.GetComponent<BulletController>().fRange = rightData.Range + pRange;
                        obj.GetComponent<BulletController>().isPenetrate = false;
                        obj.GetComponent<BulletController>().isRightAttack = true;
                        Vector3 firePos = transform.position;
                        Vector3 crossPos = GetPosition(firePos, gun_simulation.rot);
                        firePos.z = crossPos.z = 0;
                        Vector3 dir = crossPos - firePos;
                        dir.Normalize();
                        obj.GetComponent<BulletController>().dir = dir;
                        obj.transform.position = firePos + (dir * 1.05f);
                        obj.SetActive(true);
                        obj.GetComponent<BulletController>().BulletFire();
                        StartCoroutine(CameraQuake(0.05f, 0.001f));
                    }
                    else if (nowRightWeaponName.Contains("rocket_punch"))
                    {
                        GameObject obj = ObjManager.Call().GetObject("rocket_punch");
                        Vector3 firePos = transform.position;
                        Vector3 crossPos = GetPosition(firePos, gun_simulation.rot);
                        firePos.z = crossPos.z = 0;
                        Vector3 dir = crossPos - firePos;
                        dir.Normalize();
                        obj.transform.position = firePos + (dir * 1.05f);
                        obj.GetComponent<PlayerAttackController>().firePosition = firePos + (dir * 1.05f);
                        obj.GetComponent<PlayerAttackController>().weaponName = nowRightWeaponName;
                        obj.SetActive(true);
                        obj.GetComponent<PlayerAttackController>().RigidBodyFire(dir, bulletRangeRight);
                        StartCoroutine(CameraQuake(0.05f, 0.001f));
                    }
                    else if (nowRightWeaponName.Contains("multi_canon"))
                    {
                        if (gunControl.transform.Find("living_arrow_weapon").Find(nowRightWeaponName))
                        {
                            if (!gunControl.transform.Find("living_arrow_weapon").Find(nowRightWeaponName).gameObject.activeSelf)
                            {
                                gunControl.transform.Find("living_arrow_weapon").Find(nowRightWeaponName).gameObject.SetActive(true);
                                gunControl.transform.Find("living_arrow_weapon").Find(nowRightWeaponName).GetComponent<MultiCanonShotController>().enabled = true;
                            }
                        }
                        if (swordControl.transform.Find("living_arrow_weapon").Find(nowRightWeaponName))
                        {
                            if (!swordControl.transform.Find("living_arrow_weapon").Find(nowRightWeaponName).gameObject.activeSelf)
                            {
                                swordControl.transform.Find("living_arrow_weapon").Find(nowRightWeaponName).gameObject.SetActive(true);
                                swordControl.transform.Find("living_arrow_weapon").Find(nowRightWeaponName).GetComponent<MultiCanonShotController>().enabled = true;
                            }
                        }
                    }
                    else
                    {
                        GameObject obj = ObjManager.Call().GetObject("Bullet");
                        obj.GetComponent<BulletController>().isMonster = false;
                        obj.GetComponent<BulletController>().firePosition = transform.position;
                        obj.GetComponent<BulletController>().bulletType = bulletName;
                        obj.GetComponent<BulletController>().fSpeed = bulletSpeed + plusBulletSpeed;
                        obj.GetComponent<BulletController>().fAngle = gun_simulation.rot + randomAngle + angleangle;
                        obj.GetComponent<BulletController>().fRange = bulletRange + pRange;
                        obj.GetComponent<BulletController>().isRightAttack = true;
                        if (nowWeaponName.Contains("energy_arrow") || nowWeaponName.Contains("start_gun_2")) obj.GetComponent<BulletController>().isPenetrate = true;
                        else obj.GetComponent<BulletController>().isPenetrate = false;
                        Vector3 firePos = (isGunFixedPos ? fixedPosition : hero.WeaponsFireTransform.position);
                        Vector3 crossPos = GetPosition(firePos, (isGunFixedPos ? fixedAngle : gun_simulation.rot) + randomAngle + angleangle);
                        firePos.z = crossPos.z = 0;
                        Vector3 dir = crossPos - firePos;
                        dir.Normalize();
                        obj.GetComponent<BulletController>().dir = dir;
                        obj.transform.position = firePos + (dir * 1.05f);
                        obj.SetActive(true);
                        obj.GetComponent<BulletController>().BulletFire();
                        if (!nowWeaponName.Contains("fanatic_dagger")) gunControl.FireEffect(gun_simulation.rot);
                        StartCoroutine(CameraQuake(0.05f, 0.001f));
                        if (nowWeaponName.Contains("fanatic_dagger")) SoundManager.instance.StartAudio(new string[1] { "Character/fanatic_dagger" }, VOLUME_TYPE.EFFECT);
                    }
                }
                yield return new WaitForSeconds(bDelay);
            }
            if (rightAttackCount == _rightAttackCount)
            {
                if (nowRightWeaponName.Contains("rocket_punch"))
                {
                    SoundManager.instance.StartAudio(new string[1] { "Weapon/" + nowRightWeaponName }, VOLUME_TYPE.EFFECT);
                    if (gunControl.transform.Find("living_arrow_weapon").GetComponent<Animator>().GetBool("isAttack")) gunControl.transform.Find("living_arrow_weapon").GetComponent<Animator>().SetBool("isAttack", false);
                    if (swordControl.transform.Find("living_arrow_weapon").GetComponent<Animator>().GetBool("isAttack")) swordControl.transform.Find("living_arrow_weapon").GetComponent<Animator>().SetBool("isAttack", false);
                    yield return new WaitForSeconds(0.15f);
                }
                if (nowRightWeaponName.Contains("multi_canon"))
                {
                    if (gunControl.transform.Find("living_arrow_weapon").Find(nowRightWeaponName))
                    {
                        if (gunControl.transform.Find("living_arrow_weapon").Find(nowRightWeaponName).gameObject.activeSelf)
                        {
                            gunControl.transform.Find("living_arrow_weapon").Find(nowRightWeaponName).gameObject.SetActive(false);
                            gunControl.transform.Find("living_arrow_weapon").Find(nowRightWeaponName).GetComponent<MultiCanonShotController>().enabled = false;
                        }
                    }
                    if (swordControl.transform.Find("living_arrow_weapon").Find(nowRightWeaponName))
                    {
                        if (swordControl.transform.Find("living_arrow_weapon").Find(nowRightWeaponName).gameObject.activeSelf)
                        {
                            swordControl.transform.Find("living_arrow_weapon").Find(nowRightWeaponName).gameObject.SetActive(false);
                            swordControl.transform.Find("living_arrow_weapon").Find(nowRightWeaponName).GetComponent<MultiCanonShotController>().enabled = false;
                        }
                    }
                }
                if (gunControl.transform.Find("living_arrow_weapon").gameObject.activeSelf) gunControl.transform.Find("living_arrow_weapon").gameObject.SetActive(false);
                if (swordControl.transform.Find("living_arrow_weapon").gameObject.activeSelf) swordControl.transform.Find("living_arrow_weapon").gameObject.SetActive(false);
                gunControl.sr.enabled = true;
                rightAttackCount = 0;
            }
        }
        public IEnumerator Fire()
        {
            Vector3 fixedPosition = hero.WeaponsFireTransform.position;
            gunControl.transform.Find("volatile_fire").gameObject.SetActive(false);
            if (nowWeaponName.Contains("elastic_cell_weapon"))
            {
                gunControl.animator.SetTrigger("AttackTrigger");
            }
            if (nowWeaponName.Equals("volatile"))
            {
                if (!gunControl.transform.Find("volatile_fire").gameObject.activeSelf) gunControl.transform.Find("volatile_fire").gameObject.SetActive(true);
                gunControl.transform.Find("volatile_fire").Find("FX_Atk_PKQ").GetComponent<ParticleSystem>().Play();
                if (volatileBuffTimeNow > volatileBuffTime)
                {
                    volatileBuffType = Random.Range(1, 5);
                    volatileBuffTimeNow = 0.0f;
                    volatileBuffTimeColl = 5.0f;
                    ParticleSystem.MainModule main = volatileBuffParticle.main;
                    ParticleSystem.MinMaxGradient c;
                    if (volatileBuffType == 1) c = new ParticleSystem.MinMaxGradient(volatileBuffColor_1);
                    else if (volatileBuffType == 2) c = new ParticleSystem.MinMaxGradient(volatileBuffColor_2);
                    else if (volatileBuffType == 3) c = new ParticleSystem.MinMaxGradient(volatileBuffColor_3);
                    else if (volatileBuffType == 4) c = new ParticleSystem.MinMaxGradient(volatileBuffColor_4);
                    else c = new ParticleSystem.MinMaxGradient(volatileBuffColor_5);
                    main.startColor = c;
                    volatileBuffParticle.gameObject.SetActive(true);
                }
            }
            if(nowWeaponName.Contains("rocket_cannon"))
            {
                GameObject obj = Instantiate(gunControl.RocketCanonFireEffect);
                obj.transform.position = gunControl.FireEffectPostionTransform.position;
                obj.transform.localEulerAngles = new Vector3(gun_simulation.rot, -90, 0);
                Destroy(obj, 1f);
            }
            if (nowWeaponName.Contains("old_gun"))
            {
                GameObject fObj = Instantiate(Resources.Load<GameObject>("Item/Weapon/Gun/old_gun/fireeffect"));
                fObj.transform.position = fixedPosition;
                fObj.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
                fObj.transform.localEulerAngles = new Vector3(-gun_simulation.rot + 180, 90, 0);
                Destroy(fObj, 2f);
            }
            if (nowWeaponName.Contains("old_shotgun"))
            {
                GameObject fObj = Instantiate(Resources.Load<GameObject>("Item/Weapon/Gun/old_shotgun/fireeffect"));
                fObj.transform.position = fixedPosition;
                fObj.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
                fObj.transform.localEulerAngles = new Vector3(-gun_simulation.rot + 180, 90, 0);
                Destroy(fObj, 2f);
            }
            float fixedAngle = gun_simulation.rot;
            if (nowWeaponName.Contains("fanatic_dagger"))
            {
                GameObject obj2 = ObjManager.Call().GetObject("Hit_sprite9");
                obj2.transform.position = transform.position;
                obj2.transform.localScale = Vector3.one;
                obj2.transform.localEulerAngles = new Vector3(0, 0, fixedAngle);
                obj2.GetComponent<SwordHitEffectController>()._effctName = "fanatic_dagger";
                obj2.GetComponent<SwordHitEffectController>().animSpeed = 0.5f;
                obj2.GetComponent<Animator>().Rebind();
                obj2.SetActive(true);
                obj2.GetComponent<SwordHitEffectController>().isPlayerTarget = true;
                gunControl.sr.enabled = false;
            }
            if (nowWeaponName.Equals("Ice_spear")) gunControl.sr.enabled = false;
            if (nowWeaponName.Contains("harpoon_tentacles")) gunControl.sr.enabled = false;
            for (int i = 0; i < shootCount + plusShootCount; i++)
            {
                SoundManager.instance.StartAudio(new string[1] { "Weapon/" + nowWeaponName }, VOLUME_TYPE.EFFECT);
                for (int j = 0; j < (volatileBuffType == 4 ? (multiShoot + plusMultiShoot) + 2 : (multiShoot + plusMultiShoot)); j++)
                {
                    float angleangle = 0.0f;
                    if ((volatileBuffType == 4 ? (multiShoot + plusMultiShoot) + 2 : (multiShoot + plusMultiShoot)) == 1) angleangle = 0.0f;
                    else
                    {
                        int v = (multiShoot + plusMultiShoot) / 2;
                        v *= (nowWeaponName.Contains("harpoon_tentacles") ? 15 : (nowWeaponName.Contains("harpoon_tentacles") ? 15 : 10));
                        angleangle = -v + ((nowWeaponName.Contains("harpoon_tentacles") ? 15 : (nowWeaponName.Contains("harpoon_tentacles") ? 15 : 10)) * ((multiShoot + plusMultiShoot) % 2 == 0 ? j + 1 : j));
                    }
                    SoundManager.instance.StartAudio(new string[1] { nowWeaponName }, VOLUME_TYPE.EFFECT);
                    float randomAngle = Random.Range(-aimAngle, aimAngle);
                    if (nowWeaponName.Contains("electric_cellmass"))
                    {
                        GameObject obj = ObjManager.Call().GetObject("electric_cellmass");
                        obj.GetComponent<CellmassController>().firePosition = transform.position;
                        obj.GetComponent<CellmassController>().fireDelay = tickTime;
                        obj.GetComponent<CellmassController>().maxAttackCount = maxAttackCountL;
                        obj.GetComponent<CellmassController>().fSpeed = bulletSpeed + plusBulletSpeed;
                        obj.GetComponent<CellmassController>().fAngle = gun_simulation.rot + randomAngle + angleangle;
                        obj.GetComponent<CellmassController>().fRange = bulletRange;
                        Vector3 firePos = (isGunFixedPos ? fixedPosition : hero.WeaponsFireTransform.position);
                        Vector3 crossPos = GetPosition(firePos, (isGunFixedPos ? fixedAngle : gun_simulation.rot) + randomAngle + angleangle);
                        firePos.z = crossPos.z = 0;
                        Vector3 dir = crossPos - firePos;
                        dir.Normalize();
                        obj.GetComponent<CellmassController>().dir = dir;
                        obj.transform.position = firePos + (dir * 1.05f);
                        obj.SetActive(true);
                        obj.GetComponent<CellmassController>().BulletFire();
                        StartCoroutine(CameraQuake(0.05f, 0.001f));
                    }
                    else if (nowWeaponName.Contains("pixelfire_small") || nowWeaponName.Contains("cold_cell_wave"))
                    {
                        GameObject obj = ObjManager.Call().GetObject("pixelfire_small");
                        obj.GetComponent<PixelFireBulletController>().firePosition = transform.position;
                        obj.GetComponent<PixelFireBulletController>().fSpeed = bulletSpeed + plusBulletSpeed;
                        obj.GetComponent<PixelFireBulletController>().fAngle = gun_simulation.rot + randomAngle + angleangle;
                        obj.GetComponent<PixelFireBulletController>().fRange = bulletRange;
                        obj.GetComponent<PixelFireBulletController>().bulletType = nowWeaponName;
                        Vector3 firePos = (isGunFixedPos ? fixedPosition : hero.WeaponsFireTransform.position);
                        Vector3 crossPos = GetPosition(firePos, (isGunFixedPos ? fixedAngle : gun_simulation.rot) + randomAngle + angleangle);
                        firePos.z = crossPos.z = 0;
                        Vector3 dir = crossPos - firePos;
                        dir.Normalize();
                        obj.GetComponent<PixelFireBulletController>().dir = dir;
                        obj.transform.position = firePos + (dir * 1.05f);
                        obj.SetActive(true);
                        obj.GetComponent<PixelFireBulletController>().BulletFire();
                        StartCoroutine(CameraQuake(0.05f, 0.001f));
                    }
                    else if (nowWeaponName.Contains("tracking_energy_gun"))
                    {
                        GameObject obj = ObjManager.Call().GetObject("tracking_energy_gun_bullet");
                        obj.GetComponent<TrackingEneryBulletController>().firePosition = transform.position;
                        obj.GetComponent<TrackingEneryBulletController>().fSpeed = bulletSpeed + plusBulletSpeed;
                        obj.GetComponent<TrackingEneryBulletController>().fAngle = gun_simulation.rot + randomAngle + angleangle;
                        obj.GetComponent<TrackingEneryBulletController>().fRange = bulletRange;
                        Vector3 firePos = (isGunFixedPos ? fixedPosition : hero.WeaponsFireTransform.position);
                        Vector3 crossPos = GetPosition(firePos, (isGunFixedPos ? fixedAngle : gun_simulation.rot) + randomAngle + angleangle);
                        firePos.z = crossPos.z = 0;
                        Vector3 dir = crossPos - firePos;
                        dir.Normalize();
                        obj.GetComponent<TrackingEneryBulletController>().dir = dir;
                        obj.transform.position = firePos + (dir * 1.05f);
                        obj.SetActive(true);
                        obj.GetComponent<TrackingEneryBulletController>().BulletFire();
                        StartCoroutine(CameraQuake(0.05f, 0.001f));
                    }
                    else if (nowWeaponName.Contains("elastic_cell_weapon"))
                    {
                        GameObject obj = ObjManager.Call().GetObject("elastic_cell_weapon_bullet");
                        obj.GetComponent<BulletController>().firePosition = transform.position;
                        obj.GetComponent<BulletController>().fSpeed = bulletSpeed + plusBulletSpeed;
                        obj.GetComponent<BulletController>().fAngle = gun_simulation.rot + randomAngle + angleangle;
                        obj.GetComponent<BulletController>().fRange = bulletRange;
                        obj.GetComponent<BulletController>().bulletType = nowWeaponName;
                        Vector3 firePos = (isGunFixedPos ? fixedPosition : transform.position);
                        Vector3 crossPos = GetPosition(firePos, (isGunFixedPos ? fixedAngle : gun_simulation.rot) + randomAngle + angleangle);
                        firePos.z = crossPos.z = 0;
                        Vector3 dir = crossPos - firePos;
                        dir.Normalize();
                        obj.GetComponent<BulletController>().dir = dir;
                        obj.transform.position = firePos;
                        obj.SetActive(true);
                        obj.GetComponent<BulletController>().BulletFire();
                        StartCoroutine(CameraQuake(0.05f, 0.001f));
                    }
                    else if (nowWeaponName.Equals("Ice_spear"))
                    {
                        GameObject obj = ObjManager.Call().GetObject("IceSpear");
                        obj.GetComponent<PlayerAttackController>().firePosition = transform.position;
                        obj.GetComponent<PlayerAttackController>().destroyDistance = bulletRange;
                        obj.GetComponent<PlayerAttackController>().weaponName = nowWeaponName;
                        Vector3 firePos = (isGunFixedPos ? fixedPosition : hero.WeaponsFireTransform.position);
                        Vector3 crossPos = GetPosition(firePos, (isGunFixedPos ? fixedAngle : gun_simulation.rot) + randomAngle + angleangle);
                        firePos.z = crossPos.z = 0;
                        Vector3 dir = crossPos - firePos;
                        dir.Normalize();
                        obj.transform.position = firePos + (dir * 1.05f);
                        obj.transform.eulerAngles = new Vector3(0, 0, gun_simulation.rot + randomAngle + angleangle);
                        obj.GetComponent<PlayerAttackController>().plusAngle = gun_simulation.rot + randomAngle + angleangle;
                        obj.SetActive(true);
                        obj.GetComponent<PlayerAttackController>().RigidBodyFire(dir, bulletSpeed);
                        StartCoroutine(CameraQuake(0.05f, 0.001f));
                    }
                    else if (nowWeaponName.Equals("dark_cells"))
                    {
                        GameObject obj = ObjManager.Call().GetObject("dark_cells_fire");
                        obj.GetComponent<PlayerAttackController>().firePosition = transform.position;
                        obj.GetComponent<PlayerAttackController>().destroyDistance = bulletRange;
                        obj.GetComponent<PlayerAttackController>().weaponName = nowWeaponName;
                        Vector3 firePos = (isGunFixedPos ? fixedPosition : hero.WeaponsFireTransform.position);
                        Vector3 crossPos = GetPosition(firePos, (isGunFixedPos ? fixedAngle : gun_simulation.rot) + randomAngle + angleangle);
                        firePos.z = crossPos.z = 0;
                        Vector3 dir = crossPos - firePos;
                        dir.Normalize();
                        obj.transform.position = firePos + (dir * 1.05f);
                        obj.transform.eulerAngles = new Vector3(0, 0, gun_simulation.rot + randomAngle + angleangle);
                        obj.GetComponent<PlayerAttackController>().plusAngle = gun_simulation.rot + randomAngle + angleangle;
                        obj.SetActive(true);
                        obj.GetComponent<PlayerAttackController>().RigidBodyFire(dir, bulletSpeed);
                        StartCoroutine(CameraQuake(0.05f, 0.001f));
                    }
                    else if (nowWeaponName.Equals("volatile"))
                    {
                        GameObject obj = ObjManager.Call().GetObject("volatile");
                        obj.GetComponent<PlayerAttackController>().firePosition = transform.position;
                        obj.GetComponent<PlayerAttackController>().destroyDistance = (volatileBuffType == 5 ? bulletRange * 2 : bulletRange);
                        obj.GetComponent<PlayerAttackController>().weaponName = nowWeaponName;
                        if (volatileBuffType == 1) obj.GetComponent<PlayerAttackController>().attackLevel = 3;
                        else obj.GetComponent<PlayerAttackController>().attackLevel = 0;
                        Vector3 firePos = (isGunFixedPos ? fixedPosition : hero.WeaponsFireTransform.position);
                        Vector3 crossPos = GetPosition(firePos, (isGunFixedPos ? fixedAngle : gun_simulation.rot) + randomAngle + angleangle);
                        firePos.z = crossPos.z = 0;
                        Vector3 dir = crossPos - firePos;
                        dir.Normalize();
                        obj.transform.position = firePos + (dir * 1.05f);
                        obj.transform.eulerAngles = new Vector3(0, 0, gun_simulation.rot + randomAngle + angleangle);
                        obj.SetActive(true);
                        obj.GetComponent<PlayerAttackController>().RigidBodyFire(dir, (volatileBuffType == 5 ? bulletSpeed * 2.0f : bulletSpeed));
                        StartCoroutine(CameraQuake(0.05f, 0.001f));
                    }
                    else if (nowWeaponName.Equals("rocket_cannon"))
                    {
                        GameObject obj = ObjManager.Call().GetObject("rocket_cannon");
                        obj.GetComponent<PlayerAttackController>().firePosition = transform.position;
                        obj.GetComponent<PlayerAttackController>().destroyDistance = (volatileBuffType == 5 ? bulletRange * 2 : bulletRange);
                        obj.GetComponent<PlayerAttackController>().weaponName = nowWeaponName;
                        if (volatileBuffType == 1) obj.GetComponent<PlayerAttackController>().attackLevel = 3;
                        else obj.GetComponent<PlayerAttackController>().attackLevel = 0;
                        Vector3 firePos = (isGunFixedPos ? fixedPosition : hero.WeaponsFireTransform.position);
                        Vector3 crossPos = GetPosition(firePos, (isGunFixedPos ? fixedAngle : gun_simulation.rot) + randomAngle + angleangle);
                        firePos.z = crossPos.z = 0;
                        Vector3 dir = crossPos - firePos;
                        dir.Normalize();
                        obj.transform.position = firePos + (dir * 1.05f);
                        obj.transform.eulerAngles = new Vector3(0, 0, gun_simulation.rot + randomAngle + angleangle);
                        obj.SetActive(true);
                        obj.GetComponent<PlayerAttackController>().RigidBodyFire(dir, (volatileBuffType == 5 ? bulletSpeed * 2.0f : bulletSpeed));
                        StartCoroutine(CameraQuake(0.05f, 0.001f));
                    }
                    else if (nowWeaponName.Contains("old_gun"))
                    {
                        GameObject obj = ObjManager.Call().GetObject("old_gun");
                        obj.GetComponent<PlayerAttackController>().firePosition = transform.position;
                        obj.GetComponent<PlayerAttackController>().destroyDistance = (volatileBuffType == 5 ? bulletRange * 2 : bulletRange);
                        obj.GetComponent<PlayerAttackController>().weaponName = nowWeaponName;
                        if (volatileBuffType == 1) obj.GetComponent<PlayerAttackController>().attackLevel = 3;
                        else obj.GetComponent<PlayerAttackController>().attackLevel = 0;
                        Vector3 firePos = (isGunFixedPos ? fixedPosition : hero.WeaponsFireTransform.position);
                        Vector3 crossPos = GetPosition(firePos, (isGunFixedPos ? fixedAngle : gun_simulation.rot) + randomAngle + angleangle);
                        firePos.z = crossPos.z = 0;
                        Vector3 dir = crossPos - firePos;
                        dir.Normalize();
                        obj.transform.position = firePos + (dir * 1.05f);
                        obj.transform.eulerAngles = new Vector3(0, 0, gun_simulation.rot + randomAngle + angleangle);
                        obj.SetActive(true);
                        obj.GetComponent<PlayerAttackController>().RigidBodyFire(dir, (volatileBuffType == 5 ? bulletSpeed * 2.0f : bulletSpeed));
                        StartCoroutine(CameraQuake(0.05f, 0.001f));
                    }
                    else if (nowWeaponName.Contains("old_shotgun"))
                    {
                        GameObject obj = ObjManager.Call().GetObject("old_shotgun");
                        obj.GetComponent<PlayerAttackController>().firePosition = transform.position;
                        obj.GetComponent<PlayerAttackController>().destroyDistance = (volatileBuffType == 5 ? bulletRange * 2 : bulletRange);
                        obj.GetComponent<PlayerAttackController>().weaponName = nowWeaponName;
                        if (volatileBuffType == 1) obj.GetComponent<PlayerAttackController>().attackLevel = 3;
                        else obj.GetComponent<PlayerAttackController>().attackLevel = 0;
                        Vector3 firePos = (isGunFixedPos ? fixedPosition : hero.WeaponsFireTransform.position);
                        Vector3 crossPos = GetPosition(firePos, (isGunFixedPos ? fixedAngle : gun_simulation.rot) + randomAngle + angleangle);
                        firePos.z = crossPos.z = 0;
                        Vector3 dir = crossPos - firePos;
                        dir.Normalize();
                        obj.transform.position = firePos + (dir * 1.05f);
                        obj.transform.eulerAngles = new Vector3(0, 0, gun_simulation.rot + randomAngle + angleangle);
                        obj.SetActive(true);
                        obj.GetComponent<PlayerAttackController>().RigidBodyFire(dir, (volatileBuffType == 5 ? bulletSpeed * 2.0f : bulletSpeed));
                        StartCoroutine(CameraQuake(0.05f, 0.001f));
                    }
                    else
                    {
                        GameObject obj = ObjManager.Call().GetObject("Bullet");
                        obj.GetComponent<BulletController>().isMonster = false;
                        obj.GetComponent<BulletController>().firePosition = transform.position;
                        obj.GetComponent<BulletController>().bulletType = bulletName;
                        obj.GetComponent<BulletController>().fSpeed = bulletSpeed + plusBulletSpeed;
                        obj.GetComponent<BulletController>().fAngle = gun_simulation.rot + randomAngle + angleangle;
                        obj.GetComponent<BulletController>().fRange = bulletRange;
                        obj.GetComponent<BulletController>().isRightAttack = false;
                        if (nowWeaponName.Contains("energy_arrow") || nowWeaponName.Contains("start_gun_2")) obj.GetComponent<BulletController>().isPenetrate = true;
                        else obj.GetComponent<BulletController>().isPenetrate = false;
                        Vector3 firePos = (isGunFixedPos ? fixedPosition : hero.WeaponsFireTransform.position);
                        Vector3 crossPos = GetPosition(firePos, (isGunFixedPos ? fixedAngle : gun_simulation.rot) + randomAngle + angleangle);
                        firePos.z = crossPos.z = 0;
                        Vector3 dir = crossPos - firePos;
                        dir.Normalize();
                        obj.GetComponent<BulletController>().dir = dir;
                        obj.transform.position = firePos + (dir * 1.05f);
                        obj.SetActive(true);
                        obj.GetComponent<BulletController>().BulletFire();
                        if (!nowWeaponName.Contains("fanatic_dagger")) gunControl.FireEffect(gun_simulation.rot);
                        StartCoroutine(CameraQuake(0.05f, 0.001f));
                        if (nowWeaponName.Contains("fanatic_dagger")) SoundManager.instance.StartAudio(new string[1] { "Character/fanatic_dagger" }, VOLUME_TYPE.EFFECT);
                    }
                }
                yield return new WaitForSeconds(bulletDelay);
            }
            if (nowWeaponName.Equals("Ice_spear"))
                yield return new WaitForSeconds(0.3f);
            if (nowWeaponName.Contains("old_shotgun"))
            {
                yield return new WaitForSeconds(0.1f);
                SoundManager.instance.StartAudio(new string[1] { "Weapon/old_shotgun2" }, VOLUME_TYPE.EFFECT);
            }

            gunControl.sr.enabled = true;
        }
        public void SetDamage(int _damage, string _why, DEATH_TYPE _dType, string _mobID, ELEMENTAL_TYPE _eType = ELEMENTAL_TYPE.NONE)
        {
            if (isHit) return;
            if (!isHit)
            {
                CameraShaker._instance.StartShake(0.2f, 0.02f, 0.2f);
                isHit = true;
                hitTIme = 1.0f;
                StartCoroutine(HitAction());
            }
            if (Dungeon.isEliteSpawnWait) return;
            if (rightInTime > 0.0f) return;
            if (isRollingTransform) return;
            if (fInvincibleTime_offensive_01 > 0.0f) return;
            if (buffTowerBuffTime_2 > 0.0f) return;
            if (isDie) return;

            if (Dungeon.instance.newObjectSelectedObj != null && Dungeon.instance.newObjectSelectedObj.GetComponent<HpRecoveryController>()) Dungeon.instance.newObjectSelectedObj.GetComponent<HpRecoveryController>().isUsing = false;
            if (Dungeon.instance.newObjectSelectedObj != null && Dungeon.instance.newObjectSelectedObj.GetComponent<UpgradeController>()) Dungeon.instance.newObjectSelectedObj.GetComponent<UpgradeController>().isUsing = false;
            if (Dungeon.instance.newObjectSelectedObj != null && Dungeon.instance.newObjectSelectedObj.GetComponent<DungeonSpecialObject>()) Dungeon.instance.newObjectSelectedObj.GetComponent<DungeonSpecialObject>().isUsing = false;
            if (Dungeon.instance.newObjectSelectedObj != null && Dungeon.instance.newObjectSelectedObj.GetComponent<NPC>()) Dungeon.instance.newObjectSelectedObj.GetComponent<NPC>().isUsing = false;
            if (Dungeon.instance.newObjectSelectedObj != null && Dungeon.instance.newObjectSelectedObj.GetComponent<NewShopController>()) Dungeon.instance.newObjectSelectedObj.GetComponent<NewShopController>().isUse = false;
            if (Dungeon.instance.newObjectSelectedObj != null && Dungeon.instance.newObjectSelectedObj.GetComponent<MOB.Monster>())  Dungeon.instance.newObjectSelectedObj.GetComponent<MOB.Monster>().isUsing = false;

            if (Dungeon.instance.nowRoomType.Equals("normal")) StateGroup.instance.nShotByHunter++;
            else StateGroup.instance.nShotByBoss++;
            if (_dType == DEATH_TYPE.DEATH_SUB_BOSS) StateGroup.instance.subShotCount++;
            if(_dType == DEATH_TYPE.DEATH_POSION && _mobID.Equals("slime_bright")) StateGroup.instance.subShotCount++;

            //속성 데미지
            if (_eType == ELEMENTAL_TYPE.ICE)
            {
                if (isIceEmun) return;
                if (IceElemental != null && !IceElemental.activeSelf)
                {
                    IceElemental.GetComponent<ElementalController>().elementalTime = 2.0f;
                    IceElemental.GetComponent<ElementalController>().eType = _eType;
                    isFreeze = true;
                    IceElemental.SetActive(true);
                    return;
                }
            }
            if (_eType == ELEMENTAL_TYPE.FREEZE)
            {
                if (isIceEmun) return;
                FreezeTime = 6.0f;
                isFreeze2 = true;
                return;
            }
            if (_eType == ELEMENTAL_TYPE.FIRE)
            {
                if (isFireEmun) return;
            }
            if(_eType == ELEMENTAL_TYPE.POISON)
            {
                if (isPoisonEmun) return;
            }

            ScreenShot.instance.Screenshot(gameObject);
            SoundManager.instance.StartAudio(new string[1] { "Character/ShieldOff" }, VOLUME_TYPE.EFFECT);
            if (curShield > 0)
            {
                if (curShield > _damage) curShield -= _damage;
                else
                {
                    _damage -= curShield;
                    curHp -= _damage;
                    curShield = 0;
                }

                GameObject obj2 = ObjManager.Call().GetObject("Hit_sprite7");
                obj2.transform.position = transform.position;
                obj2.transform.localScale = Vector3.one;
                obj2.transform.localEulerAngles = Vector3.zero;
                obj2.GetComponent<SwordHitEffectController>()._effctName = "ShieldOff";
                obj2.GetComponent<SwordHitEffectController>().animSpeed = 0.7f;
                obj2.GetComponent<Animator>().Rebind();
                obj2.SetActive(true);
            }
            else
            {
                curHp -= _damage;
            }
            whyDie = _why;
            HpSetting();
            ShieldSetting();
            if (curHp <= 0)
            {
                //if (Dungeon.instance.isBossRush)
                //{
                //    SaveAndLoadManager.isLoad = true;
                //    SaveAndLoadManager.instance.GameSave(Inventorys.Instance.slots);
                //}
                if (transform.Find("GreenFatLaserBeam").gameObject.activeSelf) transform.Find("GreenFatLaserBeam").gameObject.SetActive(false);
                if (transform.Find("GreenFatLaserEnd").gameObject.activeSelf) transform.Find("GreenFatLaserEnd").gameObject.SetActive(false);
                if (transform.Find("GreenFatLaserStart").gameObject.activeSelf) transform.Find("GreenFatLaserStart").gameObject.SetActive(false);

                if (isDeath_Level_2)
                {
                    curHp = 2;
                    HpSetting();
                    return;
                }
                if (isTutorial)
                {
                    curHp = 1;
                    return;
                }
                if (PlayerBuffController.buffList.FindIndex(item => item.Equals("survival_02")) != -1 && !isSurvival_02)
                {
                    GetComponent<Animator>().SetBool("Die", true);
                    GetComponent<Animator>().SetTrigger("DieTrigger");
                    isResurrection = true;
                    isSurvival_02 = true;
                    isHit = true;
                    return;
                }

                if (SteamStatsAndAchievements.instance != null)
                {
                    SteamStatsAndAchievements.instance.SettingStat("stat_death_total_count");

                    int deathCount = SteamStatsAndAchievements.instance.GettingStat("stat_death_total_count");
                    AchievementsData aData_0 = System.Array.Find(SheetManager.Instance.AchievementsDB.dataArray, item => item.ID.Equals("indomitablewill_0"));
                    AchievementsData aData_1 = System.Array.Find(SheetManager.Instance.AchievementsDB.dataArray, item => item.ID.Equals("indomitablewill_1"));
                    AchievementsData aData_2 = System.Array.Find(SheetManager.Instance.AchievementsDB.dataArray, item => item.ID.Equals("indomitablewill_2"));
                    AchievementsData aData_3 = System.Array.Find(SheetManager.Instance.AchievementsDB.dataArray, item => item.ID.Equals("indomitablewill_3"));
                    AchievementsData aData_4 = System.Array.Find(SheetManager.Instance.AchievementsDB.dataArray, item => item.ID.Equals("indomitablewill_4"));

                    SaveAndLoadManager.instance.SaveAchievements(new AchievementData[5] {
                      new AchievementData { _id = aData_0.ID, _cur = deathCount, _max = aData_0.MAX, _clear = false },
                      new AchievementData { _id = aData_1.ID, _cur = deathCount, _max = aData_1.MAX, _clear = false },
                      new AchievementData { _id = aData_2.ID, _cur = deathCount, _max = aData_2.MAX, _clear = false },
                      new AchievementData { _id = aData_3.ID, _cur = deathCount, _max = aData_3.MAX, _clear = false },
                      new AchievementData { _id = aData_4.ID, _cur = deathCount, _max = aData_4.MAX, _clear = false }
                    });
                }

                switch (_dType)
                {
                    case DEATH_TYPE.DEATH_NORMAL:
                        if (SteamStatsAndAchievements.instance != null) SteamStatsAndAchievements.instance.SettingStat("stat_death_normal_count");
                        break;
                    case DEATH_TYPE.DEATH_UNIQUE:
                        if (SteamStatsAndAchievements.instance != null) SteamStatsAndAchievements.instance.SettingStat("stat_death_normal_count");
                        break;
                    case DEATH_TYPE.DEATH_SUB_BOSS:
                        if (SteamStatsAndAchievements.instance != null) SteamStatsAndAchievements.instance.SettingStat("stat_death_unique_count");
                        break;
                    case DEATH_TYPE.DEATH_BOSS:
                        if (SteamStatsAndAchievements.instance != null) SteamStatsAndAchievements.instance.SettingStat("stat_death_boss_count");
                        break;
                    case DEATH_TYPE.DEATH_FALL:
                        if (SteamStatsAndAchievements.instance != null) SteamStatsAndAchievements.instance.SettingStat("stat_death_fall_count");
                        break;
                    case DEATH_TYPE.DEATH_POSION:
                        if (SteamStatsAndAchievements.instance != null) SteamStatsAndAchievements.instance.SettingStat("stat_death_posion_count");
                        break;
                    case DEATH_TYPE.DEATH_FIRE:
                        if (SteamStatsAndAchievements.instance != null) SteamStatsAndAchievements.instance.SettingStat("stat_death_fire_count");
                        break;
                }

                if (!_mobID.Equals(string.Empty))
                {
                    string tId = _mobID;
                    if (tId.Contains("_t")) tId = tId.Substring(0, tId.Length - 3);
                    int tempIndex = System.Array.FindIndex(SheetManager.Instance.MonsterDB.dataArray, item => item.ID.Equals(tId));
                    if (tempIndex != -1 && SteamStatsAndAchievements.instance != null) SteamStatsAndAchievements.instance.SettingStat(tId + "_death");
                }

                curHp = 0;
                isDie = true;
                hero.isWeaponMode = false;
                hero.WeaponsSlot.SetActive(false);
                hero.WeaponsCrossHair.SetActive(false);
                hero.animator.SetBool("isWeapons", false);
                Dungeon.instance.MonsterClear();

                TextOpenController.instance.TextClose(Color.white);
                Dungeon.instance.newObjectSelected = false;
                Dungeon.instance.newObjectSelectedObj = null;
                SaveAndLoadManager.instance.DeleteManufacture(PDG.Dungeon.instance.isBossRush ? 1 : 0);
                MakeController.instance.DicMakeCount.Clear();
                SoundManager.instance.StartAudio(new string[1] { "Character/die" }, VOLUME_TYPE.EFFECT);
                StartCoroutine(DieAction());
            }
            if (PlayerBuffController.buffList.FindIndex(item => item.Equals("offensive_01")) != -1)
            {
                fInvincibleTime_offensive_01 = 1.0f;
                fPlusDamageTime_offensive01 = 1.0f;
                fPlusDamage_offensive01 = 5;
            }
        }
        IEnumerator HitAction()
        {
            CameraShaker._instance.StartShake(0.1f, Time.deltaTime, 0.2f);
            Shaker.StartShaking();
            GetComponent<SpriteRenderer>().color = new Color32(255, 105, 105, 255);
            yield return new WaitForSeconds(0.05f);
            GetComponent<SpriteRenderer>().color = Color.white;
            for (int i = 0; i < (isDeath_Level_1 ? 19 : 9); i++)
            {
                GetComponent<SpriteRenderer>().enabled = !GetComponent<SpriteRenderer>().enabled;
                if (!isSubWeapon) transform.Find("Gun").Find("Gun").GetComponent<SpriteRenderer>().enabled = !transform.Find("Gun").Find("Gun").GetComponent<SpriteRenderer>().enabled;
                //Q_SkillBodyEffect.GetComponent<SpriteRenderer>().enabled = !Q_SkillBodyEffect.GetComponent<SpriteRenderer>().enabled;
                if (helmetObj.activeSelf) helmetObj.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, (i % 2 == 0 ? 0 : 1));
                yield return new WaitForSeconds(0.1f);
            }
            GetComponent<SpriteRenderer>().enabled = true;
            if (!isSubWeapon) transform.Find("Gun").Find("Gun").GetComponent<SpriteRenderer>().enabled = true;
            //Q_SkillBodyEffect.GetComponent<SpriteRenderer>().enabled = true;
            if (helmetObj.activeSelf) helmetObj.GetComponent<SpriteRenderer>().color = Color.white;
            yield return new WaitForSeconds(0.05f);
        }
        public void ExitCharacterOpen()
        {
            rightInTime = 2.0f;
            GetComponent<SpriteRenderer>().enabled = true;
            transform.Find("Gun").Find("Gun").GetComponent<SpriteRenderer>().enabled = true;
            //Q_SkillBodyEffect.GetComponent<SpriteRenderer>().enabled = true;
            if (helmetObj.activeSelf) helmetObj.GetComponent<SpriteRenderer>().color = Color.white;
        }
        IEnumerator DieAction()
        {
            Time.timeScale = 0.1f;
            GetComponent<Animator>().SetTrigger("DieTrigger");
            GetComponent<Animator>().SetBool("Die", true);
            yield return new WaitForSeconds(0.3f);
            Time.timeScale = 1.0f;
            //GetComponent<SpriteRenderer>().enabled = false;
            GameObject.Find("Canvas").transform.Find("DiePanel").Find("Anim").GetComponent<Animator>().Rebind();
            GameObject.Find("Canvas").transform.Find("DiePanel").gameObject.SetActive(true);
            yield return new WaitForSeconds(2.5f);
            ResultPanelScript.instance.isReset = true;
            ResultPanelScript.instance.ShowResult(whyDie, false);
        }
        IEnumerator CameraQuake(float _amount, float _delay)
        {
            if (Dungeon.instance.nCameraShakerLevel != 0 && !CameraShaker._instance.isDontMoveCamera)
            {
                _amount = (PDG.Dungeon.instance.nCameraShakerLevel / 2) * 0.05f;
                if (_amount < 0.25f) _amount = 0.03f;
                float t = 0.0f;
                for (int i = 0; i < 5; i++)
                {
                    UnityEngine.Camera.main.transform.position += new Vector3(Random.Range(-_amount, _amount), Random.Range(-_amount, _amount), 0);
                    yield return new WaitForSeconds(Time.deltaTime);
                }
            }
        }
        public bool HpRecovery(int amount)
        {
            float maxCount = (maxHp + plusHp);
            if (maxCount < 1) maxCount = 1;
            if (curHp == maxCount)
            {
                HpSetting();
                return false;
            }
            curHp += amount; 
            if (curHp > maxCount) curHp = (maxHp + plusHp);
            curHpInspertor = curHp;
            HpSetting();
            return true; 
        }
        public bool ShieldRecovery(int amount)
        {
            curShield += amount;
            ShieldSetting();
            return true;
        }
        private void HpSetting()
        {
            float maxCount = (maxHp + plusHp);
            if (maxCount < 1) maxCount = 1;
            for (int i = 0; i < Mathf.CeilToInt(maxCount / 2); i++)
            {
                if (i >= HpBar.Length) break;
                if (!HpBar[i].gameObject.activeSelf)
                {
                    HpBar[i].GetComponent<Image>().color = Color.white;
                    HpBar[i].gameObject.SetActive(true);
                }
                for (int j = 0; j < 2; j++)
                {
                    if((i * 2 + j) < curHp)
                    {
                        if(j == 0)
                        {
                            HpBar[i].sprite = hp50;
                            if ((i * 2 + j) + 1 >= maxCount)
                            {
                                HpBar[i].sprite = hp50_2;
                            }
                        }
                        else
                        {
                            HpBar[i].sprite = hp100;
                        }
                    }
                    else if ((i * 2 + j) > curHp)
                    {
                        if (j == 0)
                        {
                            HpBar[i].sprite = hpEmpty50;
                        }
                        else
                        {
                            if ((i * 2 + j) >= maxCount)
                            {
                                HpBar[i].sprite = hpEmpty50;
                                break;
                            }
                            else
                                HpBar[i].sprite = hpEmpty100;
                        }
                    }
                }
            }
            if (Mathf.CeilToInt(maxCount / 2) < HpBar.Length)
            {
                for (int i = Mathf.CeilToInt(maxCount / 2); i < HpBar.Length; i++)
                {
                    if (i >= HpBar.Length) break;
                    if (HpBar[i].gameObject.activeSelf) HpBar[i].gameObject.SetActive(false);
                }
            }
        }
        private void ShieldSetting()
        {
            for (int i = 0; i < Mathf.CeilToInt((float)curShield / 2); i++)
            {
                if (i >= ShieldBar.Length) break;
                if (!ShieldBar[i].gameObject.activeSelf)
                {
                    ShieldBar[i].color = Color.white;
                    ShieldBar[i].gameObject.SetActive(true);
                }

                for (int j = 0; j < 2; j++)
                {
                    if ((i * 2 + j) < curShield)
                    {
                        if (j == 0)
                        {
                            ShieldBar[i].sprite = shield50;
                        }
                        else
                        {
                            ShieldBar[i].sprite = shield100;
                        }
                    }
                    else if ((i * 2 + j) > curShield)
                    {
                        ShieldBar[i].sprite = hpEmpty100;
                    }
                }
            }
            if (Mathf.CeilToInt((float)curShield / 2) < ShieldBar.Length)
            {
                for (int i = Mathf.CeilToInt((float)curShield / 2); i < ShieldBar.Length; i++)
                {
                    if (i >= ShieldBar.Length) break;
                    if (ShieldBar[i].gameObject.activeSelf) ShieldBar[i].gameObject.SetActive(false);
                }
            }
        }
        private bool CheckCollision
        {
            get
            {
                Dungeon dun = Dungeon.instance;
                Vector3Int pos = new Vector3Int(Mathf.FloorToInt(hero.WeaponsFireTransform.position.x), Mathf.FloorToInt(hero.WeaponsFireTransform.position.y), 0);
                if (hero.transform.localScale.x > 0) pos.x += 1;
                return dun.Find(pos.x, pos.y, null) || dun.Find(pos.x, pos.y, dun.wall_side_mid_right) || dun.Find(pos.x, pos.y, dun.wall_side_mid_left);
            }
        }
        public void MaxHpChange(int _amount)
        {
            maxHp += _amount;
            HpSetting();
        }
        private void GuideArrowFire()
        {
            int r = Random.Range(0, 100);
            if (r >= 45 && r < 55 && guideDelayNow > guideDelay) 
            {
                guideDelayNow = 0.0f;

                RaycastHit2D[] hit = Physics2D.BoxCastAll(transform.position, new Vector2(15, 10), 0, Vector2.zero, 0, 1 << 12 | 1 << 24);
                if (hit.Length > 0)
                {
                    GameObject obj = ObjManager.Call().GetObject("GuideBullet");
                    if (obj)
                    {
                        obj.transform.position = transform.position;

                        if (obj.GetComponent<GuideBulletController>())
                        {
                            obj.GetComponent<GuideBulletController>().target = hit[Random.Range(0, hit.Length)].collider.gameObject;
                        }
                        //obj.GetComponent<GuideBulletController>().endPos = hit[Random.Range(0, hit.Length)].collider.transform.position;
                        obj.SetActive(true);
                        if (obj && obj.GetComponent<GuideBulletController>())
                            obj.GetComponent<GuideBulletController>().BulletFire();
                    }
                }
            }
        }
        #region -- Tool --
        public static float GetAngle(Vector3 vStart, Vector3 vEnd)
        {
            Vector3 v = vEnd - vStart;

            return Mathf.Atan2(v.y, v.x) * Mathf.Rad2Deg;
        }
        public static Vector3 GetPosition(Vector3 vStart, float angle)
        {
            float x = Mathf.Cos(Mathf.Deg2Rad * angle) + vStart.x;
            float y = Mathf.Sin(Mathf.Deg2Rad * angle) + vStart.y;

            return new Vector3(x, y, 0);
        }
        private bool GetCritical(int max)
        {
                int[] per = new int[100];
                for (int i = 0; i<per.Length; i++) per[i] = 0;

                int t = 0;
            while (true)
            {
                int ran = Random.Range(0, 100);
                if (per[ran] == 0)
                {
                    per[ran] = 1;
                    t++;
                }
                if (t >= max) break;
            }

            int ran2 = Random.Range(0, 100);
            return per[ran2] == 0 ? false : true;
        }
        #endregion
    }
}
