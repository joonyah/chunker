﻿using PDG;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QSkillController : MonoBehaviour
{
    [SerializeField] private PDG.Player _player;
    [SerializeField] private RuntimeAnimatorController[] anims;
    [SerializeField] private Animator animator;
    [SerializeField] private SpriteRenderer sr;
    public string nowQSkillName;

    private void Awake()
    {
        animator = GetComponent<Animator>();
        sr = GetComponent<SpriteRenderer>();
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void StartQ(string _name)
    {
        CameraShaker._instance.StartShake(0.2f, 0.02f, 0.2f);
        animator.runtimeAnimatorController = System.Array.Find(anims, item => item.name.Equals(_name));
        animator.Rebind();
    }

    public void EndQ()
    {
        gameObject.SetActive(false);
        _player.isUseQ = false;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.layer.Equals(12))
        {
            Vector3 dir2 = collision.gameObject.transform.position - PDG.Player.instance.transform.position;
            dir2.Normalize();
            collision.gameObject.GetComponent<MOB.Monster>().SetDamage(PDG.Player.instance.isQNear ? PDG.Player.instance.damageQNear : PDG.Player.instance.damageQ, -dir2, 2, Player.instance.q_elemetanl, Player.instance.q_elemnetalTime, true);
            if (PDG.Player.instance.isQNear && PDG.Player.instance.isCriticalQ)
            {
                GameObject criEffect = ObjManager.Call().GetObject("CriticalHitEffect");
                criEffect.transform.position = collision.transform.position;
                criEffect.SetActive(true);
            }
        }
        if (collision.gameObject.layer.Equals(24))
        {
            if (collision.gameObject.GetComponent<MOB.BossMonster_Six_Apear>()) collision.gameObject.GetComponent<MOB.BossMonster_Six_Apear>().SetDamage(Player.instance.damageQ);
            if (collision.gameObject.GetComponent<Boss_STG>()) collision.gameObject.GetComponent<Boss_STG>().SetDamage(Player.instance.isQNear ? Player.instance.damageQNear : Player.instance.damageQ, collision.transform.position);
            if (collision.gameObject.GetComponent<Boss_NPC>()) collision.gameObject.GetComponent<Boss_NPC>().SetDamage(Player.instance.isQNear ? Player.instance.damageQNear : Player.instance.damageQ, collision.transform.position);
            if (collision.gameObject.GetComponent<Boss_Golem>()) collision.gameObject.GetComponent<Boss_Golem>().SetDamage(Player.instance.isQNear ? Player.instance.damageQNear : Player.instance.damageQ, collision.transform.position);
            if (collision.gameObject.GetComponent<Boss_Stone>()) collision.gameObject.GetComponent<Boss_Stone>().SetDamage(Player.instance.isQNear ? Player.instance.damageQNear : Player.instance.damageQ, collision.transform.position, true);

            if (PDG.Player.instance.isQNear && PDG.Player.instance.isCriticalQ)
            {
                GameObject criEffect = ObjManager.Call().GetObject("CriticalHitEffect");
                criEffect.transform.position = collision.transform.position;
                criEffect.SetActive(true);
            }
        }
        if (collision.gameObject.layer.Equals(23))
        {
            if (collision.gameObject.name.Contains("puddle")) return;
            if (collision.gameObject.name.Contains("grass_2")) return;
            if (collision.gameObject.name.Contains("door")) return;
            Vector3 dir = collision.gameObject.transform.position - PDG.Player.instance.transform.position;
            dir.Normalize();
            if (collision.gameObject.transform.parent && collision.gameObject.transform.parent.GetComponent<TentacleController>())
            {
                collision.gameObject.transform.parent.GetComponent<TentacleController>().SetDamage(1);
            }
            if (collision.gameObject.GetComponent<DestroyObjects>())
            {
                collision.gameObject.GetComponent<DestroyObjects>().DestroyObject(dir);
            }
            if (collision.gameObject.GetComponent<ChestHit>() && !collision.gameObject.GetComponent<ChestHit>().isOpenBefore)
            {
                collision.gameObject.GetComponent<ChestHit>().SetDamage(1);
            }
            if (collision.gameObject.GetComponent<DrumHit>())
            {
                collision.gameObject.GetComponent<DrumHit>().SetDamage(1);
            }
        }
    }
}
