﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraShaker : MonoBehaviour
{
    public static CameraShaker _instance;
    public bool TestShakeButton;
    public bool isDontMoveCamera = false;
    public float _x;
    public float _y;
    public float _delay;
    public float _time;

    private bool isStart = false;
    private Camera _camera;

    private void Awake()
    {
        if (_instance == null) _instance = this;
         _camera = Camera.main;
    }

    private void Update()
    {
        if (TestShakeButton)
        {
            TestShakeButton = false;
            StartCoroutine(Shaker());
        }
    }

    public void StartShake(float _amount, float delay, float time, bool ff = false)
    {
        if (PDG.Dungeon.instance.nCameraShakerLevel == 0 && !ff) return;
        if (isDontMoveCamera) return;
        if (ff)
            _x = _y = _amount;
        else
            _x = _y = (PDG.Dungeon.instance.nCameraShakerLevel / 2) * 0.05f;
        _delay = delay;
        _time = time;
        TestShakeButton = true;
    }

    IEnumerator Shaker()
    {
        if (!isStart && !isDontMoveCamera)
        {
            isStart = true;
            Vector3 pos = transform.position;
            float _t = 0.0f;
            while (true)
            {
                if (PDG.Player.instance != null && PDG.Player.instance.isCameraOff)
                {
                    pos = PDG.Player.instance.transform.position;
                    pos.z = -15;
                }
                transform.position = new Vector3(pos.x + (Random.Range(-_x, _x)), pos.y + (Random.Range(-_y, _y)), -15);
                _t += Time.deltaTime;
                if (_t >= _time) break;
                yield return new WaitForSeconds(_delay);
            }
            transform.position = pos;
            isStart = false;
        }
    }
}
