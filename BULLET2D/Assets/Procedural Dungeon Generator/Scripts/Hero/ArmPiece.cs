﻿using MOB;
using PDG;
using SplatterSystem;
using SplatterSystem.TopDown;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArmPiece : MonoBehaviour
{
    ArmController armController;
    public bool isAddArm = false;
    private Coroutine AddArmCorutine_1 = null;
    List<GameObject> addList = new List<GameObject>();

    [SerializeField] private GameObject[] Points;
    [SerializeField] private GameObject bezierObj;
    [SerializeField] private GameObject bezierDestroyObj;

    private void OnEnable()
    {
        armController = Player.instance._ArmController;
    }
    private void OnDisable()
    {
        for (int i = 0; i < addList.Count; i++)
            addList[i].SetActive(false);
        AddArmCorutine_1 = null;
    }
    private void OnDrawGizmos()
    {
        if (isAddArm)
        {
            Gizmos.color = Color.red;
            Gizmos.DrawWireSphere(transform.position, 5);
        }
    }
    private void Update()
    {
        if (isAddArm && Player.instance.nowWeaponName.Contains("tentacle_arm") && !Player.instance.isSubWeapon && armController.isLeft == 1)
        {
            if (!bezierObj.activeSelf) bezierObj.SetActive(true);
            int mask = 1 << 12 | 1 << 24 | 1 << 23;
            RaycastHit2D[] hits = Physics2D.CircleCastAll(transform.position, 5, Vector2.zero, 1, mask);
            if (hits.Length > 0)
            {
                RaycastHit2D hit = hits[Random.Range(0, hits.Length)];
                if (hit)
                {
                    if (hit.collider.gameObject.GetComponent<ChestHit>() && hit.collider.gameObject.GetComponent<ChestHit>().isOpenBefore)
                    {
                        Points[1].transform.localPosition = Vector3.zero;
                        Points[2].transform.localPosition = Vector3.zero;
                        Points[3].transform.localPosition = Vector3.zero;
                    }
                    else
                    {
                        float angle = GetAngle(transform.position, hit.collider.transform.position);
                        Vector3 pos1 = Vector3.Lerp(transform.position, hit.collider.transform.position, Random.Range(0.0f, 1.0f));
                        Vector3 pos2 = Vector3.Lerp(transform.position, hit.collider.transform.position, Random.Range(0.0f, 1.0f));
                        Points[1].transform.position = GetPosition(pos1, angle + (Random.Range(-90, 90)));
                        Points[2].transform.position = GetPosition(pos2, angle + (Random.Range(-90, 90)));
                        Points[3].transform.position = hit.collider.transform.position;
                    }

                    int index = -1;
                    index = Dungeon.instance.publicListObj.FindIndex(item => item.Equals(hit.collider.gameObject));
                    if (index != -1)
                    {
                        if (Player.instance.tickTimeSpecial > Dungeon.instance.publicListTime[index]) return;
                        else Dungeon.instance.publicListTime[index] = 0.0f;
                    }
                    else
                    {
                        Dungeon.instance.publicListObj.Add(hit.collider.gameObject);
                        Dungeon.instance.publicListTime.Add(0.0f);
                    }

                    Vector3 dir = hit.collider.transform.position - Player.instance.transform.position;
                    dir.Normalize();
                    if (hit.collider.GetComponent<Monster>())
                    {
                        hit.collider.GetComponent<Monster>().SetDamage(Player.instance.damage, -dir, 2, ELEMENTAL_TYPE.NONE, 0.0f);
                        if (bezierDestroyObj)
                        {
                            GameObject dObj = Instantiate(bezierDestroyObj);
                            dObj.transform.position = hit.point;
                            Destroy(dObj, 2f);
                        }
                    }
                    if (hit.collider.GetComponent<BossMonster_Six_Apear>())
                    {
                        hit.collider.GetComponent<BossMonster_Six_Apear>().SetDamage(1);
                        if (bezierDestroyObj)
                        {
                            GameObject dObj = Instantiate(bezierDestroyObj);
                            dObj.transform.position = hit.point;
                            Destroy(dObj, 2f);
                        }
                    }
                    if (hit.collider.GetComponent<Boss_STG>())
                    {
                        hit.collider.GetComponent<Boss_STG>().SetDamage(Player.instance.damage, hit.point);
                        if (bezierDestroyObj)
                        {
                            GameObject dObj = Instantiate(bezierDestroyObj);
                            dObj.transform.position = hit.point;
                            Destroy(dObj, 2f);
                        }
                    }
                    if (hit.collider.GetComponent<Boss_NPC>())
                    {
                        hit.collider.GetComponent<Boss_NPC>().SetDamage(Player.instance.damage, hit.point);
                        if (bezierDestroyObj)
                        {
                            GameObject dObj = Instantiate(bezierDestroyObj);
                            dObj.transform.position = hit.point;
                            Destroy(dObj, 2f);
                        }
                    }
                    if (hit.collider.GetComponent<Boss_Stone>())
                    {
                        hit.collider.GetComponent<Boss_Stone>().SetDamage(Player.instance.damage, hit.point, false);
                        if (bezierDestroyObj)
                        {
                            GameObject dObj = Instantiate(bezierDestroyObj);
                            dObj.transform.position = hit.point;
                            Destroy(dObj, 2f);
                        }
                    }
                    if (hit.collider.GetComponent<Boss_Golem>())
                    {
                        hit.collider.GetComponent<Boss_Golem>().SetDamage(Player.instance.damage, hit.point);
                        if (bezierDestroyObj)
                        {
                            GameObject dObj = Instantiate(bezierDestroyObj);
                            dObj.transform.position = hit.point;
                            Destroy(dObj, 2f);
                        }
                    }
                    if (hit.collider.gameObject.layer.Equals(23))
                    {
                        if (hit.collider.gameObject.name.Contains("puddle")) return;
                        if (hit.collider.gameObject.name.Contains("grass_2")) return;
                        if (hit.collider.gameObject.name.Contains("door")) return;

                        if (hit.collider.transform.parent && hit.collider.transform.parent.GetComponent<TentacleController>())
                        {
                            hit.collider.transform.parent.GetComponent<TentacleController>().SetDamage(1);
                            if (bezierDestroyObj)
                            {
                                GameObject dObj = Instantiate(bezierDestroyObj);
                                dObj.transform.position = hit.point;
                                Destroy(dObj, 2f);
                            }
                        }
                        Vector3 dir2 = hit.collider.gameObject.transform.position - Player.instance.transform.position;
                        dir2.Normalize();
                        if (hit.collider.gameObject.GetComponent<DestroyObjects>())
                        {
                            hit.collider.gameObject.GetComponent<DestroyObjects>().DestroyObject(dir2);
                            if (bezierDestroyObj)
                            {
                                GameObject dObj = Instantiate(bezierDestroyObj);
                                dObj.transform.position = hit.point;
                                Destroy(dObj, 2f);
                            }
                        }
                        if (hit.collider.gameObject.GetComponent<ChestHit>() && !hit.collider.gameObject.GetComponent<ChestHit>().isOpenBefore)
                        {
                            hit.collider.gameObject.GetComponent<ChestHit>().SetDamage(1);
                            if (bezierDestroyObj)
                            {
                                GameObject dObj = Instantiate(bezierDestroyObj);
                                dObj.transform.position = hit.point;
                                Destroy(dObj, 2f);
                            }
                        }
                        if (hit.collider.gameObject.GetComponent<DrumHit>())
                        {
                            hit.collider.gameObject.GetComponent<DrumHit>().SetDamage(1);
                            if (bezierDestroyObj)
                            {
                                GameObject dObj = Instantiate(bezierDestroyObj);
                                dObj.transform.position = hit.point;
                                Destroy(dObj, 2f);
                            }
                        }
                    }
                }
            }
            else
            {
                Points[1].transform.localPosition = Vector3.zero;
                Points[2].transform.localPosition = Vector3.zero;
                Points[3].transform.localPosition = Vector3.zero;
            }
        }
        else
        {
            if (bezierObj.activeSelf) bezierObj.SetActive(false);
        }
    }
    IEnumerator AddArmStart(Transform start, Transform target)
    {
        int count = 9999;
        float f = 0.0f;
        while (true)
        {
            Vector3 sPos = start.position;
            Vector3 ePos = target.position;
            Vector3 lPos = Vector3.Lerp(sPos, ePos, f);
            GameObject obj = GetArmPoolingObject();
            obj.SetActive(true);
            obj.transform.position = lPos;
            obj.transform.localEulerAngles = new Vector3(0, 0, GetAngle(obj.transform.position, ePos));
            obj.transform.localScale = Vector3.one;
            obj.GetComponent<SpriteRenderer>().sortingOrder = count--;
            f += Time.deltaTime * 10;
            if (f > 1.0f) break;
            yield return null;
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {

        int index = -1;
        index = Dungeon.instance.publicListObj2.FindIndex(item => item.Equals(collision.gameObject));
        if (index != -1)
        {
            if (Player.instance.tickTime > Dungeon.instance.publicListTime2[index]) return;
            else Dungeon.instance.publicListTime2[index] = 0.0f;
        }
        else
        {
            Dungeon.instance.publicListObj2.Add(collision.gameObject);
            Dungeon.instance.publicListTime2.Add(0.0f);
        }

        if (collision.gameObject.layer.Equals(23))
        {
            if (collision.gameObject.name.Contains("puddle")) return;
            if (collision.gameObject.name.Contains("grass_2")) return;
            if (collision.gameObject.name.Contains("door")) return;

            if (collision.transform.parent && collision.transform.parent.GetComponent<TentacleController>())
            {
                collision.transform.parent.GetComponent<TentacleController>().SetDamage(1);
            }
            Vector3 dir = collision.gameObject.transform.position - Player.instance.transform.position;
            dir.Normalize();
            if (collision.gameObject.GetComponent<DestroyObjects>())
            {
                collision.gameObject.GetComponent<DestroyObjects>().DestroyObject(dir);

                GameObject obj3 = ObjManager.Call().GetObject("tentacle_arm");
                obj3.transform.position = collision.transform.position;
                obj3.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
                obj3.transform.localEulerAngles = new Vector3(-90, 0, 0);
                obj3.SetActive(true);
            }
            if (collision.gameObject.GetComponent<ChestHit>() && !collision.gameObject.GetComponent<ChestHit>().isOpenBefore)
            {
                collision.gameObject.GetComponent<ChestHit>().SetDamage(1);
                Player.instance.isArmCollision = true;

                GameObject obj3 = ObjManager.Call().GetObject("tentacle_arm");
                obj3.transform.position = collision.transform.position;
                obj3.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
                obj3.transform.localEulerAngles = new Vector3(-90, 0, 0);
                obj3.SetActive(true);
            }
            if(collision.gameObject.GetComponent<DrumHit>())
            {
                collision.gameObject.GetComponent<DrumHit>().SetDamage(1);

                GameObject obj3 = ObjManager.Call().GetObject("tentacle_arm");
                obj3.transform.position = collision.transform.position;
                obj3.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
                obj3.transform.localEulerAngles = new Vector3(-90, 0, 0);
                obj3.SetActive(true);
            }
        }
        if (collision.gameObject.layer.Equals(24))
        {
            Player.instance.isArmCollision = true;
            if (collision.gameObject.GetComponent<BossMonster_Six_Apear>()) collision.gameObject.GetComponent<BossMonster_Six_Apear>().SetDamage(Player.instance.damage);
            if (collision.gameObject.GetComponent<Boss_STG>()) collision.gameObject.GetComponent<Boss_STG>().SetDamage(Player.instance.damage, collision.transform.position);
            if (collision.gameObject.GetComponent<Boss_NPC>()) collision.gameObject.GetComponent<Boss_NPC>().SetDamage(Player.instance.damage, collision.transform.position);
            if (collision.gameObject.GetComponent<Boss_Golem>()) collision.gameObject.GetComponent<Boss_Golem>().SetDamage(Player.instance.damage, collision.transform.position);
            if (collision.gameObject.GetComponent<Boss_Stone>()) collision.gameObject.GetComponent<Boss_Stone>().SetDamage(Player.instance.damage, collision.transform.position, false);
            if (Player.instance.isStrategic_02) Player.instance.isStrategic_02 = false;

            if (PDG.Player.instance.isLeftNear && PDG.Player.instance.isCritical)
            {
                GameObject criEffect = ObjManager.Call().GetObject("CriticalHitEffect");
                criEffect.transform.position = collision.transform.position;
                criEffect.SetActive(true);
            }
            GameObject obj3 = ObjManager.Call().GetObject("tentacle_arm");
            obj3.transform.position = collision.transform.position;
            obj3.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
            obj3.transform.localEulerAngles = new Vector3(-90, 0, 0);
            obj3.SetActive(true);
        }
        if (collision.gameObject.layer.Equals(12))
        {
            MOB.Monster mob = collision.gameObject.GetComponent<MOB.Monster>();
            if (armController.isLeft == 1)
            {
                if (!armController.isAttack && mob != null)
                {
                    Vector3 dir = mob.transform.position - Player.instance.transform.position;
                    dir.Normalize();
                    armController.isAttack = true;
                    mob.SetDamage((Player.instance.damage), -dir, 2, Player.instance.left_elemetanl, Player.instance.left_elemnetalTime);
                    if (Player.instance.isStrategic_02) Player.instance.isStrategic_02 = false;
                    Player.instance.isArmCollision = true;
                }
                GameObject obj3 = ObjManager.Call().GetObject("tentacle_arm");
                obj3.transform.position = collision.transform.position;
                obj3.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
                obj3.transform.localEulerAngles = new Vector3(-90, 0, 0);
                obj3.SetActive(true);
                if (PDG.Player.instance.isLeftNear && PDG.Player.instance.isCritical)
                {
                    GameObject criEffect = ObjManager.Call().GetObject("CriticalHitEffect");
                    criEffect.transform.position = collision.transform.position;
                    criEffect.SetActive(true);
                }
            }
            if (armController.isLeft == 0 && !armController.isGrab && !mob.isJump && !mob.isNoneInfo && !mob.isGoblin &&
                (Player.instance.nowRightWeaponName.Equals("catching_tentacle_1") ? true : !mob.isUnique) && !mob.isBoss && !mob.mobData.ID.Contains("ghost_hide"))
            {
                if (!mob.isMimic || !mob.isMiMicWait)
                {
                    if ((!mob.isTutorial || mob.tutoType == 1))
                    {
                        mob.Grab(collision.transform);
                        armController.grabMonster = mob;
                        armController.isGrab = true;
                    }
                    Player.instance.isArmCollision = true;
                    Player.instance.grabTime = 3.0f;

                    GameObject obj3 = ObjManager.Call().GetObject("tentacle_arm");
                    obj3.transform.position = collision.transform.position;
                    obj3.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
                    obj3.transform.localEulerAngles = new Vector3(-90, 0, 0);
                    obj3.SetActive(true);
                }
            }
            else if (armController.isLeft == 0 && !armController.isGrab && mob.isBoss)
            {
                if (!armController.isAttack)
                {
                    Vector3 dir = mob.transform.position - Player.instance.transform.position;
                    dir.Normalize();
                    armController.isAttack = true;
                    mob.SetDamage((Player.instance.isRightNear ? Player.instance.damageRightNear : Player.instance.damageRight), -dir, 2, Player.instance.right_elemetanl, Player.instance.right_elemnetalTime);
                    if (Player.instance.isStrategic_02) Player.instance.isStrategic_02 = false;
                    Player.instance.isArmCollision = true;
                    if (PDG.Player.instance.isRightNear && PDG.Player.instance.isCriticalRight)
                    {
                        GameObject criEffect = ObjManager.Call().GetObject("CriticalHitEffect");
                        criEffect.transform.position = collision.transform.position;
                        criEffect.SetActive(true);
                    }
                }
                GameObject obj3 = ObjManager.Call().GetObject("tentacle_arm");
                obj3.transform.position = collision.transform.position;
                obj3.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
                obj3.transform.localEulerAngles = new Vector3(-90, 0, 0);
                obj3.SetActive(true);
            }
        }
    }
    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.layer.Equals(8))
        {
            Player.instance.isArmCollision = true;
            Player.instance.isDecrease = true;
        }
    }
    private GameObject GetArmPoolingObject()
    {
        for (int i = 0; i < addList.Count; i++)
        {
            if (!addList[i].activeSelf) return addList[i];
        }
        GameObject obj = Instantiate(Player.instance.armPrefabs);
        obj.transform.SetParent(Player.instance.ArmParent);
        addList.Add(obj);
        return obj;

    }
    #region -- Tool --
    public static float GetAngle(Vector3 vStart, Vector3 vEnd)
    {
        Vector3 v = vEnd - vStart;

        return Mathf.Atan2(v.y, v.x) * Mathf.Rad2Deg;
    }
    public static Vector3 GetPosition(Vector3 vStart, float angle)
    {
        float x = Mathf.Cos(Mathf.Deg2Rad * angle) + vStart.x;
        float y = Mathf.Sin(Mathf.Deg2Rad * angle) + vStart.y;

        return new Vector3(x, y, 0);
    }
    #endregion
}
