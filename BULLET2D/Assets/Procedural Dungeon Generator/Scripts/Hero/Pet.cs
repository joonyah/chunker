﻿using PDG;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pet : MonoBehaviour
{
    public float searchDistance = 7.0f;
    public float attackTimer = 0.0f;
    private bool isAttack = false;

    private float attackDelay = 2.0f;
    private float attackDelayNow = 2.0f;
    public float fAroundSpeed = 50.0f;
    public float moveAngle = 0.0f;
    public float dis = 2.0f;

    private SpriteRenderer sr;

    private void Awake()
    {
        sr = GetComponent<SpriteRenderer>();
    }

    private void OnEnable()
    {
    }

    private void Start()
    {
        StartCoroutine(MoveCoroutine());
    }
    // Update is called once per frame
    void Update()
    {
        attackTimer -= Time.deltaTime;
        if (attackTimer <= 0.0f) isAttack = false;
        else isAttack = true;

        if(isAttack)
        {
            attackDelayNow += Time.deltaTime;

            int mask = 1 << 12 | 1 << 24;
            RaycastHit2D hit = Physics2D.CircleCast(PDG.Player.instance.transform.position, searchDistance, Vector2.zero, 0, mask);
            if(hit)
            {
                Vector3 mDir = hit.collider.transform.position - transform.position;
                mDir.Normalize();
                if(mDir.x < 0)
                {
                    sr.flipX = true;
                }
                else
                {
                    sr.flipX = false;
                }

                if (attackDelayNow > attackDelay)
                {
                    attackDelayNow = 0.0f;
                    Attack(hit.collider.gameObject);
                }
            }
        }
        //moveAngle += Time.deltaTime * fAroundSpeed;
        //if (moveAngle >= 360.0f) moveAngle -= 360.0f;
        
        //Vector3 anglePosition = GetPosition(_player.transform.position, moveAngle);
        //Vector3 dir = anglePosition - _player.transform.position;
        //dir.Normalize();
        //transform.position = _player.transform.position + (dir * dis);
    }
    IEnumerator MoveCoroutine()
    {
        float f = Random.Range(0.1f, 1.0f);
        bool isUp = Random.Range(0, 2) % 2 == 0 ? true : false;
        while (true)
        {
            if (Vector3.Distance(Player.instance.transform.position, transform.position) >= 3.0f)
            {
                Vector3 anglePosition = GetPosition(Player.instance.transform.position, moveAngle);
                Vector3 dir = anglePosition - Player.instance.transform.position;
                dir.Normalize();

                Vector3 startPos = transform.position;
                Vector3 endPos = Player.instance.transform.position + (dir * dis);

                float t = 0.0f;
                while (true)
                {
                    Vector3 lerp = Vector3.Lerp(startPos, endPos, t);
                    transform.position = lerp;
                    t += Time.deltaTime * 3;
                    if (t >= 1.0f)
                    {
                        dis = Random.Range(2, 5);
                        moveAngle = Random.Range(0, 180);
                        break;
                    }
                    yield return new WaitForSeconds(Time.deltaTime);
                }
            }
            else
            {
                Vector3 pos = transform.position;
                if (isUp)
                {
                    f += Time.deltaTime * 4;
                    pos.y += Time.deltaTime;
                    if (f > 1.0f)
                    {
                        f = 0.0f;
                        isUp = !isUp;
                    }
                }
                else
                {
                    f += Time.deltaTime * 4;
                    pos.y -= Time.deltaTime;
                    if (f > 1.0f)
                    {
                        f = 0.0f;
                        isUp = !isUp;
                    }
                }
                transform.position = pos;
            }
            yield return new WaitForSeconds(Time.deltaTime);
        }
    }
    private void Attack(GameObject targetObj)
    {
        float angle = GetAngle(transform.position, targetObj.transform.position);
        GameObject obj = ObjManager.Call().GetObject("Bullet");
        obj.GetComponent<BulletController>().isMonster = false;
        obj.GetComponent<BulletController>().firePosition = transform.position;
        obj.GetComponent<BulletController>().bulletType = "startGun_Bullet";
        obj.GetComponent<BulletController>().fSpeed = 6;
        obj.GetComponent<BulletController>().fAngle = angle;
        obj.GetComponent<BulletController>().fRange = 10;
        obj.GetComponent<BulletController>().isPenetrate = false;
        obj.GetComponent<BulletController>().isRightAttack = true;
        Vector3 firePos = transform.position;
        Vector3 crossPos = GetPosition(firePos, angle);
        firePos.z = crossPos.z = 0;
        Vector3 dir = crossPos - firePos;
        dir.Normalize();
        obj.GetComponent<BulletController>().dir = dir;
        obj.transform.position = firePos + (dir * 1.05f);
        obj.SetActive(true);
        obj.GetComponent<BulletController>().BulletFire();
    }
    #region -- Tool --
    public static float GetAngle(Vector3 vStart, Vector3 vEnd)
    {
        Vector3 v = vEnd - vStart;

        return Mathf.Atan2(v.y, v.x) * Mathf.Rad2Deg;
    }
    public static Vector3 GetPosition(Vector3 vStart, float angle)
    {
        float x = Mathf.Cos(Mathf.Deg2Rad * angle) + vStart.x;
        float y = Mathf.Sin(Mathf.Deg2Rad * angle) + vStart.y;

        return new Vector3(x, y, 0);
    }
    #endregion
}
