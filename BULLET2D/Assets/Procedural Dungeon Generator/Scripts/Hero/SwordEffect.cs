﻿using PDG;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct tagSpriteCheck
{
    public string _name;
    public Sprite _sprite;
}

public class SwordEffect : MonoBehaviour
{
    [SerializeField] private Animator animator;
    [SerializeField] private SpriteRenderer sr;
    [SerializeField] private string _spritePath;
    public GameObject ownerObj;
    public float destroyDistance = 10.0f;
    public float _angle;
    public string _weaponName;
    public Vector3 firepos;
    [SerializeField] private AudioSource _audio;
    AudioClip clip;
    public tagSpriteCheck[] checkList;
    private Sprite[] _sprites;
    public CircleCollider2D defenceColl;
    public bool isAttack = false;
    [SerializeField] private List<GameObject> objList = new List<GameObject>();
    public float fSize;
    [SerializeField] private GameObject hitEffect;
    [SerializeField] private GameObject criticalHitEffect;
    private int bounsCount = 0;
    float bounsDelayTime = 0.15f;
    private void OnEnable()
    {
        if (animator)
        {
            animator.Rebind();
            Load();
        }
        clip = Resources.Load<AudioClip>("Sound/Weapon/" + _weaponName + "_hit");
        if (_audio != null)
        {
            _audio.volume = (SoundManager.isMute || (PDG.Dungeon.instance == null ? false : PDG.Dungeon.instance.isPause) || (PDG.Player.instance == null ? false : PDG.Player.instance.isDie)) ? 0 : SoundManager.EffectVolume;
            if (clip != null) _audio.PlayOneShot(clip);
        }
        if (transform.Find("hit")) transform.Find("hit").gameObject.SetActive(false);
        if (_weaponName.Contains("cell_dagger") && transform.Find("hit")) transform.Find("hit").gameObject.SetActive(true);
        if(_weaponName.Contains("boss_stg_scythe") && transform.Find("boss_stg_scythe"))
        {
            sr.enabled = false;
            transform.Find("boss_stg_scythe").gameObject.SetActive(true);
        }
        else if(transform.Find("boss_stg_scythe"))
        {
            sr.enabled = true;
            transform.Find("boss_stg_scythe").gameObject.SetActive(false);
        }
        objList.Clear();
        bounsCount = 0;
    }
    public void InitSetting(string _name)
    {
        tagSpriteCheck t = System.Array.Find(checkList, item => item._name.Equals(_name));
        GetComponent<SpriteRenderer>().sprite = t._sprite;
        objList.Clear();
    }
    void Load()
    {
        var spritePath = "Effect/Weapons/" + _weaponName;
        if (!spritePath.Equals(_spritePath))
        {
            _spritePath = spritePath;
            _sprites = Resources.LoadAll<Sprite>(_spritePath);
        }
    }
    void LateUpdate()
    {
        if (animator == null) return;
        if (sr == null || sr.sprite == null)
            return;
        Load();
        var name = sr.sprite.name;
        var sprite = System.Array.Find(_sprites, item => item.name == name);
        if (sprite) sr.sprite = sprite;
    }
    public void AnimationEnd()
    {
        gameObject.SetActive(false);
        if (PlayerCombat.isCompulsionMove)
        {
            PlayerCombat.isCompulsionMove = false;
            PlayerCombat.instance.coll.enabled = true;
        }
    }
    private void Update()
    {
        if (bounsDelayTime > 0)
        {
            bounsDelayTime -= Time.deltaTime;
            return;
        }

        if (_weaponName.Contains("dna_mo_blade") && defenceColl != null)
        {
            if (Player.instance.left_defence_gauge >= 1.0f) defenceColl.enabled = true;
            else defenceColl.enabled = false;
        }

        if (ownerObj)
        {
            if (PlayerCombat.isCompulsionMove)
            {
                transform.position = ownerObj.transform.position;
            }
            else
            {
                if (Vector3.Distance(firepos, transform.position) > destroyDistance)
                {
                    if (_weaponName.Contains("boss_stg_scythe") && hitEffect)
                    {
                        GameObject hObj = Instantiate(hitEffect);
                        hObj.transform.position = transform.position;
                        Destroy(hObj, 2f);
                        gameObject.SetActive(false);
                    }
                    else if (_weaponName.Contains("dna_mo_blade") && defenceColl != null)
                    {
                        gameObject.SetActive(false);
                    }
                    else
                        DestroyEffect();
                }
            }
        }

        RaycastHit2D[] hits = Physics2D.CircleCastAll(transform.position, fSize, Vector2.zero, 0, 1 << 12 | 1 << 24 | 1 << 23 | 1 << 8);
        if (hits.Length > 0 &&  isAttack)
        {
            for (int i = 0; i < hits.Length; i++)
            {
                Collider2D collision = hits[i].collider;
                if (objList.FindIndex(item => item.Equals(collision.gameObject)) != -1)
                {
                    continue;
                }
                objList.Add(collision.gameObject);

                if (collision.gameObject.layer.Equals(12))
                {
                    Vector3 dir2 = collision.gameObject.transform.position - Player.instance.transform.position;
                    dir2.Normalize();
                    if (_weaponName.Contains("dna_mo_blade") && defenceColl != null)
                    {
                        collision.gameObject.GetComponent<MOB.Monster>().SetDamage(PDG.Player.instance.isLeftNear ? PDG.Player.instance.damageNear : PDG.Player.instance.damage, -dir2, 2, Player.instance.left_elemetanl, Player.instance.left_elemnetalTime);

                        if (hitEffect && !Player.instance.isCritical)
                        {
                            GameObject hitObj = Instantiate(hitEffect);
                            hitObj.transform.position = hits[i].point;
                            Destroy(hitObj, 2f);
                        }
                    }
                    else if ((_weaponName.Contains("dna_mo_blade") && defenceColl == null) || _weaponName.Contains("boss_stg_scythe")) 
                    {
                        collision.gameObject.GetComponent<MOB.Monster>().SetDamage(PDG.Player.instance.damageNear, -dir2, 2, Player.instance.left_elemetanl, Player.instance.left_elemnetalTime);
                    }
                    else
                    {
                        collision.gameObject.GetComponent<MOB.Monster>().SetDamage(PDG.Player.instance.isLeftNear ? PDG.Player.instance.damageNear : PDG.Player.instance.damage, -dir2, 2, Player.instance.left_elemetanl, Player.instance.left_elemnetalTime);
                    }

                    if (PDG.Player.instance.isLeftNear && PDG.Player.instance.isCritical) 
                    {
                        if ((_weaponName.Contains("dna_mo_blade") && defenceColl == null) || _weaponName.Contains("boss_stg_scythe"))
                        {

                        }
                        else
                        {
                            GameObject criEffect = ObjManager.Call().GetObject("CriticalHitEffect");
                            criEffect.transform.position = hits[i].point;
                            criEffect.SetActive(true);
                        }
                    }

                    if (Player.instance.isStrategic_02) Player.instance.isStrategic_02 = false;

                    if (_weaponName.Contains("boss_stg_scythe") && hitEffect)
                    {
                        GameObject hObj = Instantiate(hitEffect);
                        hObj.transform.position = hits[i].point;
                        Destroy(hObj, 2f);
                        gameObject.SetActive(false);
                    }
                    else
                    {
                        GameObject obj = ObjManager.Call().GetObject("Sword_Hit");
                        obj.transform.position = collision.transform.position;
                        obj.GetComponent<Animator>().Rebind();
                        obj.SetActive(true);
                        {
                            int ran = Random.Range(0, 3);
                            if (ran == 0)
                            {
                                GameObject obj2 = ObjManager.Call().GetObject("Hit_sprite4");
                                obj2.transform.position = collision.transform.position;
                                obj2.transform.localScale = Vector3.one;
                                obj2.transform.localEulerAngles = Vector2.zero;
                                obj2.GetComponent<SwordHitEffectController>()._effctName = "Hit_" + (_weaponName.Contains("giant_scythe") ? ran + 3 : ran);
                                obj2.GetComponent<SwordHitEffectController>().animSpeed = 1.0f;
                                obj2.GetComponent<SwordHitEffectController>().isPenetrate = false;
                                obj2.GetComponent<SwordHitEffectController>().isAttack = false;
                                obj2.GetComponent<Animator>().Rebind();
                                obj2.SetActive(true);
                            }
                            else if (ran == 1)
                            {
                                GameObject obj2 = ObjManager.Call().GetObject("Hit_sprite4");
                                obj2.transform.position = collision.transform.position;
                                obj2.transform.localScale = Vector3.one;
                                obj2.transform.localEulerAngles = Vector2.zero;
                                obj2.GetComponent<SwordHitEffectController>()._effctName = "Hit_" + (_weaponName.Contains("giant_scythe") ? ran + 3 : ran);
                                obj2.GetComponent<SwordHitEffectController>().animSpeed = 1.0f;
                                obj2.GetComponent<SwordHitEffectController>().isPenetrate = false;
                                obj2.GetComponent<SwordHitEffectController>().isAttack = false;
                                obj2.GetComponent<Animator>().Rebind();
                                obj2.SetActive(true);
                            }
                            else
                            {
                                GameObject obj2 = ObjManager.Call().GetObject("Hit_sprite4");
                                obj2.transform.position = collision.transform.position;
                                obj2.transform.localScale = Vector3.one;
                                obj2.transform.localEulerAngles = Vector2.zero;
                                obj2.GetComponent<SwordHitEffectController>()._effctName = "Hit_" + (_weaponName.Contains("giant_scythe") ? ran + 3 : ran);
                                obj2.GetComponent<SwordHitEffectController>().animSpeed = 1.0f;
                                obj2.GetComponent<SwordHitEffectController>().isPenetrate = false;
                                obj2.GetComponent<SwordHitEffectController>().isAttack = false;
                                obj2.GetComponent<Animator>().Rebind();
                                obj2.SetActive(true);
                            }
                        }
                    }
                }
                if (collision.gameObject.layer.Equals(23))
                {
                    if (collision.transform.parent && collision.transform.parent.GetComponent<TentacleController>())
                    {
                        collision.transform.parent.GetComponent<TentacleController>().SetDamage(1);

                        if (_weaponName.Contains("boss_stg_scythe") && hitEffect)
                        {
                            GameObject hObj = Instantiate(hitEffect);
                            hObj.transform.position = hits[i].point;
                            Destroy(hObj, 2f);
                            gameObject.SetActive(false);
                        }
                    }
                    if (collision.gameObject.GetComponent<ChestHit>())
                    {
                        collision.gameObject.GetComponent<ChestHit>().SetDamage(1);

                        if (_weaponName.Contains("boss_stg_scythe") && hitEffect)
                        {
                            GameObject hObj = Instantiate(hitEffect);
                            hObj.transform.position = hits[i].point;
                            Destroy(hObj, 2f);
                            gameObject.SetActive(false);
                        }
                        else
                        {
                            GameObject obj = ObjManager.Call().GetObject("Sword_Hit");
                            obj.transform.position = collision.transform.position;
                            obj.GetComponent<Animator>().Rebind();
                            obj.SetActive(true);
                            {
                                int ran = Random.Range(0, 3);
                                if (ran == 0)
                                {
                                    GameObject obj2 = ObjManager.Call().GetObject("Hit_sprite4");
                                    obj2.transform.position = collision.transform.position;
                                    obj2.GetComponent<SwordHitEffectController>().animSpeed = 1.0f;
                                    obj2.GetComponent<SwordHitEffectController>().isPenetrate = false;
                                    obj2.GetComponent<SwordHitEffectController>().isAttack = false;
                                    obj2.transform.localScale = Vector3.one;
                                    obj2.transform.localEulerAngles = Vector2.zero;
                                    obj2.GetComponent<Animator>().Rebind();
                                    obj2.SetActive(true);
                                }
                                else if (ran == 1)
                                {
                                    GameObject obj2 = ObjManager.Call().GetObject("Sword_Hit_1");
                                    if (obj2 != null)
                                    {
                                        obj2.transform.position = collision.transform.position;
                                        obj2.GetComponent<Animator>().Rebind();
                                        obj2.SetActive(true);
                                    }
                                }
                                else
                                {
                                    GameObject obj2 = ObjManager.Call().GetObject("Sword_Hit_2");
                                    if (obj2 != null)
                                    {
                                        obj2.transform.position = collision.transform.position;
                                        obj2.GetComponent<Animator>().Rebind();
                                        obj2.SetActive(true);
                                    }
                                }
                            }
                        }
                    }
                    else if (collision.gameObject.GetComponent<DrumHit>())
                    {
                        collision.gameObject.GetComponent<DrumHit>().SetDamage(1);

                        if (_weaponName.Contains("boss_stg_scythe") && hitEffect)
                        {
                            GameObject hObj = Instantiate(hitEffect);
                            hObj.transform.position = hits[i].point;
                            Destroy(hObj, 2f);
                            gameObject.SetActive(false);
                        }
                        else
                        {
                            GameObject obj = ObjManager.Call().GetObject("Sword_Hit");
                            obj.transform.position = collision.transform.position;
                            obj.GetComponent<Animator>().Rebind();
                            obj.SetActive(true);
                            {
                                int ran = Random.Range(0, 3);
                                if (ran == 0)
                                {
                                    GameObject obj2 = ObjManager.Call().GetObject("Hit_sprite4");
                                    obj2.transform.position = collision.transform.position;
                                    obj2.GetComponent<SwordHitEffectController>().animSpeed = 1.0f;
                                    obj2.GetComponent<SwordHitEffectController>().isPenetrate = false;
                                    obj2.GetComponent<SwordHitEffectController>().isAttack = false;
                                    obj2.transform.localScale = Vector3.one;
                                    obj2.transform.localEulerAngles = Vector2.zero;
                                    obj2.GetComponent<Animator>().Rebind();
                                    obj2.SetActive(true);
                                }
                                else if (ran == 1)
                                {
                                    GameObject obj2 = ObjManager.Call().GetObject("Sword_Hit_1");
                                    if (obj2 != null)
                                    {
                                        obj2.transform.position = collision.transform.position;
                                        obj2.GetComponent<Animator>().Rebind();
                                        obj2.SetActive(true);
                                    }
                                }
                                else
                                {
                                    GameObject obj2 = ObjManager.Call().GetObject("Sword_Hit_2");
                                    if (obj2 != null)
                                    {
                                        obj2.transform.position = collision.transform.position;
                                        obj2.GetComponent<Animator>().Rebind();
                                        obj2.SetActive(true);
                                    }
                                }
                            }
                        }
                    }
                }
                if (collision.gameObject.layer.Equals(24))
                {
                    if (_weaponName.Contains("dna_mo_blade") && defenceColl != null)
                    {
                        if (collision.gameObject.GetComponent<MOB.BossMonster_Six_Apear>()) collision.gameObject.GetComponent<MOB.BossMonster_Six_Apear>().SetDamage(Player.instance.damageNear);
                        if (collision.gameObject.GetComponent<Boss_STG>()) collision.gameObject.GetComponent<Boss_STG>().SetDamage(Player.instance.isLeftNear ? Player.instance.damageNear : Player.instance.damage, hits[i].point);
                        if (collision.gameObject.GetComponent<Boss_NPC>()) collision.gameObject.GetComponent<Boss_NPC>().SetDamage(Player.instance.isLeftNear ? Player.instance.damageNear : Player.instance.damage, hits[i].point);
                        if (collision.gameObject.GetComponent<Boss_Golem>()) collision.gameObject.GetComponent<Boss_Golem>().SetDamage(Player.instance.isLeftNear ? Player.instance.damageNear : Player.instance.damage, hits[i].point);
                        if (collision.gameObject.GetComponent<Boss_Stone>()) collision.gameObject.GetComponent<Boss_Stone>().SetDamage(Player.instance.isLeftNear ? Player.instance.damageNear : Player.instance.damage, hits[i].point, false);

                        if (hitEffect && !Player.instance.isCritical)
                        {
                            GameObject hitObj = Instantiate(hitEffect);
                            hitObj.transform.position = hits[i].point;
                            Destroy(hitObj, 2f);
                        }
                    }
                    else if ((_weaponName.Contains("dna_mo_blade") && defenceColl == null) || _weaponName.Contains("boss_stg_scythe"))
                    {
                        if (collision.gameObject.GetComponent<MOB.BossMonster_Six_Apear>()) collision.gameObject.GetComponent<MOB.BossMonster_Six_Apear>().SetDamage(Player.instance.damage);
                        if (collision.gameObject.GetComponent<Boss_STG>()) collision.gameObject.GetComponent<Boss_STG>().SetDamage(Player.instance.damage, hits[i].point);
                        if (collision.gameObject.GetComponent<Boss_NPC>()) collision.gameObject.GetComponent<Boss_NPC>().SetDamage(Player.instance.damage, hits[i].point);
                        if (collision.gameObject.GetComponent<Boss_Golem>()) collision.gameObject.GetComponent<Boss_Golem>().SetDamage(Player.instance.damage, hits[i].point);
                        if (collision.gameObject.GetComponent<Boss_Stone>()) collision.gameObject.GetComponent<Boss_Stone>().SetDamage(Player.instance.damage, hits[i].point, false);
                    }
                    else
                    {
                        if (collision.gameObject.GetComponent<MOB.BossMonster_Six_Apear>()) collision.gameObject.GetComponent<MOB.BossMonster_Six_Apear>().SetDamage(Player.instance.damage);
                        if (collision.gameObject.GetComponent<Boss_STG>()) collision.gameObject.GetComponent<Boss_STG>().SetDamage(Player.instance.isLeftNear ? Player.instance.damageNear : Player.instance.damage, hits[i].point);
                        if (collision.gameObject.GetComponent<Boss_NPC>()) collision.gameObject.GetComponent<Boss_NPC>().SetDamage(Player.instance.isLeftNear ? Player.instance.damageNear : Player.instance.damage, hits[i].point);
                        if (collision.gameObject.GetComponent<Boss_Golem>()) collision.gameObject.GetComponent<Boss_Golem>().SetDamage(Player.instance.isLeftNear ? Player.instance.damageNear : Player.instance.damage, hits[i].point);
                        if (collision.gameObject.GetComponent<Boss_Stone>()) collision.gameObject.GetComponent<Boss_Stone>().SetDamage(Player.instance.isLeftNear ? Player.instance.damageNear : Player.instance.damage, hits[i].point, false);
                    }

                    if (PDG.Player.instance.isLeftNear && PDG.Player.instance.isCritical)
                    {
                        if ((_weaponName.Contains("dna_mo_blade") && defenceColl == null) || _weaponName.Contains("boss_stg_scythe"))
                        {

                        }
                        else
                        {
                            GameObject criEffect = ObjManager.Call().GetObject("CriticalHitEffect");
                            criEffect.transform.position = hits[i].point;
                            criEffect.SetActive(true);
                        }
                    }

                    if (Player.instance.isStrategic_02) Player.instance.isStrategic_02 = false;

                    if (_weaponName.Contains("boss_stg_scythe") && hitEffect)
                    {
                        GameObject hObj = Instantiate(hitEffect);
                        hObj.transform.position = hits[i].point;
                        Destroy(hObj, 2f);
                        gameObject.SetActive(false);
                    }
                    else
                    {
                        GameObject obj = ObjManager.Call().GetObject("Sword_Hit");
                        obj.transform.position = collision.transform.position;
                        obj.GetComponent<Animator>().Rebind();
                        obj.SetActive(true);
                        {
                            int ran = Random.Range(0, 3);
                            if (ran == 0)
                            {
                                if (collision != null)
                                {
                                    GameObject obj2 = ObjManager.Call().GetObject("Hit_sprite4");
                                    obj2.transform.position = collision.transform.position;
                                    obj2.GetComponent<SwordHitEffectController>().animSpeed = 1.0f;
                                    obj2.GetComponent<SwordHitEffectController>().isPenetrate = false;
                                    obj2.GetComponent<SwordHitEffectController>().isAttack = false;
                                    obj2.transform.localScale = Vector3.one;
                                    obj2.transform.localEulerAngles = Vector2.zero;
                                    obj2.GetComponent<Animator>().Rebind();
                                    obj2.SetActive(true);
                                }
                            }
                            else if (ran == 1)
                            {
                                if (collision != null)
                                {
                                    GameObject obj2 = ObjManager.Call().GetObject("Sword_Hit_1");
                                    if (obj2 != null)
                                    {
                                        obj2.transform.position = collision.transform.position;
                                        obj2.GetComponent<Animator>().Rebind();
                                        obj2.SetActive(true);
                                    }
                                }
                            }
                            else
                            {
                                if (collision != null)
                                {
                                    GameObject obj2 = ObjManager.Call().GetObject("Sword_Hit_2");
                                    if (obj2 != null)
                                    {
                                        obj2.transform.position = collision.transform.position;
                                        obj2.GetComponent<Animator>().Rebind();
                                        obj2.SetActive(true);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            if (_weaponName.Contains("dna_mo_blade") && defenceColl != null)
            {
            }
            else if (Player.instance.isBulletBounce && _weaponName.Contains("dna_mo_blade") && defenceColl == null)
            {
                GameObject obj = ObjManager.Call().GetObject("SwordForceDestroy");
                obj.transform.position = transform.position;
                obj.transform.localEulerAngles = new Vector3(0, 0, _angle);
                obj.GetComponent<SwordEffect>().InitSetting(_weaponName);
                obj.GetComponent<SwordEffect>()._weaponName = _weaponName;
                obj.GetComponent<Animator>().Rebind();
                obj.GetComponent<SwordEffect>().isAttack = false;
                obj.SetActive(true);

                if (bounsCount >= PDG.Player.instance.BounsCount) gameObject.SetActive(false);

                bounsDelayTime = 0.15f;
                Vector3 dir = hits[0].collider.transform.position - transform.position;
                dir.Normalize();
                float angle = GetReflectAngle(hits[0].point, hits[0].collider.transform.position, dir);
                GetComponent<Rigidbody2D>().velocity = Vector2.zero;
                dir = GetPosition(transform.position, angle) - transform.position;
                dir.Normalize();
                GetComponent<Rigidbody2D>().AddForce(dir * Player.instance.forcePow, ForceMode2D.Impulse);
                transform.localEulerAngles = new Vector3(0, 0, angle);
                bounsCount++;
            }
            else
            {
                DestroyEffect();
            }
        }

    }
    public void DestroyEffect()
    {
        if (_weaponName.Contains("boss_stg_scythe") && hitEffect)
        {
            GameObject hObj = Instantiate(hitEffect);
            hObj.transform.position = transform.position;
            Destroy(hObj, 2f);
            gameObject.SetActive(false);
        }
        else
        {
            GameObject obj = ObjManager.Call().GetObject("SwordForceDestroy");
            obj.transform.position = transform.position;
            obj.transform.localEulerAngles = new Vector3(0, 0, _angle);
            obj.GetComponent<SwordEffect>().InitSetting(_weaponName);
            obj.GetComponent<SwordEffect>()._weaponName = _weaponName;
            obj.GetComponent<Animator>().Rebind();
            obj.GetComponent<SwordEffect>().isAttack = false;
            obj.SetActive(true);
        }
        gameObject.SetActive(false);
    }
    #region -- Tool --
    float GetAngle(Vector3 vStart, Vector3 vEnd)
    {
        Vector3 v = vEnd - vStart;

        return Mathf.Atan2(v.y, v.x) * Mathf.Rad2Deg;
    }
    Vector3 GetPosition(Vector3 vStart, float angle)
    {
        float x = Mathf.Cos(Mathf.Deg2Rad * angle) + vStart.x;
        float y = Mathf.Sin(Mathf.Deg2Rad * angle) + vStart.y;

        return new Vector3(x, y, 0);
    }
    float GetReflectAngle(Vector3 _hitPoint, Vector3 _hitPosition, Vector3 _dir)
    {
        Vector3 _dir2 = _hitPoint - _hitPosition;
        _dir2.Normalize();

        if (_dir.x == 0 && _dir.y > 0)       // 위 
        {
            return 270.0f;
        }
        else if (_dir.x == 0 && _dir.y < 0)  // 아래
        {
            return 90.0f;
        }
        else if (_dir.x > 0 && _dir.y == 0)  // 오른쪽
        {
            return 180.0f;
        }
        else if (_dir.x < 0 && _dir.y == 0)  // 왼쪽
        {
            return 0.0f;
        }
        else if (_dir.x > 0 && _dir.y > 0)  // 오른쪽 위 
        {
            if (_dir2.x > 0)
            {
                float zz = GetAngle(_hitPoint, transform.position);
                float f = zz - 180.0f; // 입사각
                return 360.0f - f; // 반사각
            }
            else if (_dir2.y > 0)
            {
                float zz = GetAngle(_hitPoint, transform.position);
                float f = 270.0f - zz; // 입사각
                return 90.0f + f; // 반사각
            }
            else
            {
                float hitAngle = GetAngle(_hitPosition, _hitPoint);
                if (hitAngle > 225.0f)
                {
                    float zz = GetAngle(_hitPoint, transform.position);
                    float f = zz - 180.0f; // 입사각
                    return 360.0f - f; // 반사각
                }
                else if (hitAngle < 225.0f)
                {
                    float zz = GetAngle(_hitPoint, transform.position);
                    float f = 270.0f - zz; // 입사각
                    return 90.0f + f; // 반사각
                }
                else
                {
                    return 225.0f;
                }

            }
        }
        else if (_dir.x < 0 && _dir.y > 0)   // 왼쪽 위
        {
            if (_dir2.x < 0)
            {
                float zz = GetAngle(_hitPoint, transform.position);
                float f = 360.0f - zz; // 입사각
                return 180.0f + f; // 반사각
            }
            else if (_dir2.y > 0)
            {
                float zz = GetAngle(_hitPoint, transform.position);
                float f = zz - 270.0f; // 입사각
                return f; // 반사각
            }
            else
            {
                float hitAngle = GetAngle(_hitPosition, _hitPoint);
                if (hitAngle > 315.0f)
                {
                    float zz = GetAngle(_hitPoint, transform.position);
                    float f = zz - 270.0f; // 입사각
                    return f; // 반사각
                }
                else if (hitAngle < 315.0f)
                {
                    float zz = GetAngle(_hitPoint, transform.position);
                    float f = 360.0f - zz; // 입사각
                    return 180.0f + f; // 반사각
                }
                else
                {
                    return 315.0f;
                }

            }
        }
        else if (_dir.x < 0 && _dir.y < 0)   // 왼쪽 아래
        {
            if (_dir2.x < 0)
            {
                float zz = GetAngle(_hitPoint, transform.position);
                float f = zz; // 입사각
                return 180.0f - f; // 반사각
            }
            else if (_dir2.y < 0)
            {
                float zz = GetAngle(_hitPoint, transform.position);
                float f = 90.0f - zz; // 입사각
                return 270.0f + f; // 반사각
            }
            else
            {
                float hitAngle = GetAngle(_hitPosition, _hitPoint);
                if (hitAngle > 45.0f)
                {
                    float zz = GetAngle(_hitPoint, transform.position);
                    float f = zz; // 입사각
                    return 180.0f - f; // 반사각
                }
                else if (hitAngle < 45.0f)
                {
                    float zz = GetAngle(_hitPoint, transform.position);
                    float f = 90.0f - zz; // 입사각
                    return 270.0f + f; // 반사각
                }
                else
                {
                    return 45.0f;
                }

            }
        }
        else if (_dir.x > 0 && _dir.y < 0)   // 오른쪽 아래
        {
            if (_dir2.x > 0)
            {
                float zz = GetAngle(_hitPoint, transform.position);
                float f = 180.0f - zz; // 입사각
                return f; // 반사각
            }
            else if (_dir2.y < 0)
            {
                float zz = GetAngle(_hitPoint, transform.position);
                float f = zz - 90.0f; // 입사각
                return 270.0f - f; // 반사각
            }
            else
            {
                float hitAngle = GetAngle(_hitPosition, _hitPoint);
                if (hitAngle > 135.0f)
                {
                    float zz = GetAngle(_hitPoint, transform.position);
                    float f = zz - 90.0f; // 입사각
                    return 270.0f - f; // 반사각
                }
                else if (hitAngle < 135.0f)
                {
                    float zz = GetAngle(_hitPoint, transform.position);
                    float f = 180.0f - zz; // 입사각
                    return f; // 반사각
                }
                else
                {
                    return 135.0f;
                }
            }
        }
        else
        {
            return 0.0f;
        }
    }
    #endregion
}
