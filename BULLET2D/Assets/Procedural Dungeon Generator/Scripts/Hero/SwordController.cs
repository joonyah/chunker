﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwordController : MonoBehaviour
{

    public int AttackType = 0;
    public Animator animator;
    public SpriteRenderer sr;
    public GameObject swordObj;
    [SerializeField] private Sprite[] sprites;
    public bool isThrow = false;
    public bool isAnim = false;
    string _spritePath;
    [SerializeField] private RuntimeAnimatorController[] _animators;
    [SerializeField] private RuntimeAnimatorController defaultAnim;
    private void OnEnable()
    {
        animator.SetInteger("AttackType", AttackType);
    }
    private void Start()
    {
    }
    void LateUpdate()
    {
        if (!PDG.Player.instance.nowWeaponName.Contains("dna_mo_blade")) return;
        if (sr == null || sr.sprite == null)
            return;
        var sprite = System.Array.Find(sprites, item => item.name == PDG.Player.instance.nowWeaponName + "_weapon");
        if (sprite) sr.sprite = sprite;
    }
    private void Update()
    {
        if(isThrow)
        {

        }
        else if(isAnim)
        {
            if (AttackType > 1) AttackType = 0;
            animator.SetInteger("AttackType", AttackType);
            if (AttackType == 0)
            {
                sr.sortingOrder = 4;
            }
            else
            {
                sr.sortingOrder = 2;
            }
        }
        else
        {
            if (AttackType > 1) AttackType = 0;
            animator.SetInteger("AttackType", AttackType);
            if (AttackType == 0)
            {
                swordObj.transform.localScale = new Vector3(-1, 1, 1);
                sr.sortingOrder = 2;
            }
            else
            {
                swordObj.transform.localScale = new Vector3(1, 1, 1);
                sr.sortingOrder = 4;
            }
        }
    }
    public void ChangeSword(string _name)
    {
        AttackType = 0;
        if (_name.Contains("cell_dagger")) isThrow = true;
        else isThrow = false;

        if (_name.Contains("giant_blade"))
        {
            isAnim = true;
            sr.flipX = true;
        }
        else if (_name.Contains("secret_blade"))
        {
            isAnim = true;
            sr.flipX = true;
        }
        else if (_name.Contains("fire_blade"))
        {
            isAnim = true;
            sr.flipX = true;
        }
        else if (_name.Contains("old_sword"))
        {
            isAnim = true;
            sr.flipX = true;
        }
        else if (_name.Contains("dna_mo_blade"))
        {
            isAnim = false;
            sr.flipX = false;
        }
        else
        {
            isAnim = false;
            sr.flipX = false;
        }
        if (isAnim)
        {
            animator.runtimeAnimatorController = System.Array.Find(_animators, item => item.name.Equals(_name + "_anim"));
            animator.enabled = true;
        }
        else
        {
            if (_name.Contains("giant_scythe")) animator.enabled = false;
            else if (_name.Contains("cell_dagger")) animator.enabled = false;
            else if (_name.Contains("boss_stg_scythe")) animator.enabled = false;
            else animator.enabled = true;
            animator.runtimeAnimatorController = defaultAnim;
        }

        if (_name.Contains("giant_blade"))
        {
            swordObj.transform.localScale = new Vector3(1, 1, 1);
            swordObj.transform.localEulerAngles = new Vector3(0, 0, -180);
        }
        else if (_name.Contains("dna_mo_blade"))
        {
            swordObj.transform.localScale = new Vector3(-1, 1, 1);
            swordObj.transform.localEulerAngles = new Vector3(0, 0, -180);
            animator.SetInteger("AttackType", AttackType);
            animator.SetTrigger("Attack");
        }
        else
        {
            swordObj.transform.localScale = new Vector3(-1, 1, 1);
            swordObj.transform.localEulerAngles = new Vector3(0, 0, 0);
        }

        sr.sprite = System.Array.Find(sprites, item => item.name.Equals(_name + "_weapon"));
    }
}
