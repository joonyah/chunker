﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class WorldMapMovement : MonoBehaviour
{
    public GameObject TeleportObj;
    public GameObject targetObj;
    public float miniMapSpeed = 5.0f;
    public float miniMapSpeed_joy = 5.0f;
    public Camera _camera;
    private long pushCheck = 0;
    private long pushCheck2 = 0;
    private bool isPush = false;

    private Vector3 pushPosition = Vector3.zero;
    private Vector3 targetOriginPosition = Vector3.zero;

    private void Update()
    {
        if (PDG.Dungeon.instance.isJoystrick)
        {
            float x = Input.GetAxis("Horizontal_joy");
            float y = Input.GetAxis("Vertical_joy");
            if (x != 0 || y != 0)
            {
                Vector3 nowPosition = new Vector3(x, y, 0);
                targetObj.transform.position += (nowPosition * miniMapSpeed_joy);
            }
        }
        else
        {
            if (Input.GetKey(KeyCode.Mouse0))
            {
                if (!isPush)
                {
                    isPush = true;
                    pushPosition = Input.mousePosition;
                    targetOriginPosition = targetObj.transform.position;
                }
                pushCheck++;
            }

            if (pushCheck == pushCheck2) isPush = false;
            pushCheck2 = pushCheck;

            if (isPush)
            {
                Vector3 nowPosition = Input.mousePosition;
                Vector3 differencePosition = nowPosition - pushPosition;
                targetObj.transform.position = targetOriginPosition - (differencePosition * miniMapSpeed);

            }
        }
    }
}
