﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TopImageLayerController : MonoBehaviour
{
    private SpriteRenderer sr;

    private void Awake()
    {
        sr = GetComponent<SpriteRenderer>();
    }

    private void Update()
    {
        if (CheckCollision == 0)
        {
            sr.sortingLayerName = "Background";
        }
        else if (CheckCollision == 1)
        {
            sr.sortingLayerName = "Floor";
        }
    }
    private int CheckCollision
    {
        get
        {
            RaycastHit2D hit;
            int mask = 1 << 15;
            hit = Physics2D.BoxCast(transform.position, new Vector2(0.4f, 0.4f), 0, Vector2.zero, 1, mask);
            if (hit.collider.gameObject.layer.Equals(15))
            {
                if (hit.collider.name.Equals("Up"))
                {
                    return 1;
                }
                if (hit.collider.name.Equals("Down"))
                {
                    return 0;
                }
            }

            return 0;
        }
    }
}
