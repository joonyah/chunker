﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class StateChoiseItem : MonoBehaviour, IPointerClickHandler
{
    public void OnPointerClick(PointerEventData eventData)
    {
        if (eventData.button == PointerEventData.InputButton.Left) StateUpgradeController.instance.ChoiseSkill(gameObject);
        else if (eventData.button == PointerEventData.InputButton.Right) StateUpgradeController.instance.ChoiseUnSelected(gameObject);
    }
}
