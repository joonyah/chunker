﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyObjects : MonoBehaviour
{
    public Sprite[] DestroySprites;
    public GameObject[] _DestroyObjects;

    public Sprite[] DefaultSprite;

    public float fPow = 3.0f;
    public bool isPush = false;
    private void OnValidate()
    {
        if(_DestroyObjects.Length == 0)
        {
            for (int i = 0; i < DestroySprites.Length; i++)
            {
                GameObject obj = new GameObject();
                SpriteRenderer sr = obj.AddComponent<SpriteRenderer>();
                sr.sprite = DestroySprites[i];
                obj.transform.localPosition = Vector3.zero;
                obj.transform.SetParent(transform);
            }
        }
    }
    private void Start()
    {
        GetComponent<SpriteRenderer>().sprite = DefaultSprite[Random.Range(0, DefaultSprite.Length)];
    }
    private void Update()
    {
        if(isPush)
        {

        }
    }
    public void DestroyObject(Vector3 pos)
    {
        if(GetComponent<SpriteRenderer>().enabled)
        {
            if (name.Contains("door")) SoundManager.instance.StartAudio(new string[1] { "Object/door" }, VOLUME_TYPE.AMBIENT);
            else if (name.Contains("grass_2")) SoundManager.instance.StartAudio(new string[1] { "Object/door" }, VOLUME_TYPE.AMBIENT);
            else if (name.Contains("puddle") || name.Contains("cloth_piece")) SoundManager.instance.StartAudio(new string[1] { "Object/puddle" }, VOLUME_TYPE.AMBIENT);
            else if (name.Contains("envelope")) SoundManager.instance.StartAudio(new string[1] { "Object/envelope" }, VOLUME_TYPE.AMBIENT);
        }

        if (isPush)
        {
            Rigidbody2D rigid = GetComponent<Rigidbody2D>();
            rigid.AddForce(pos * fPow, ForceMode2D.Impulse);
        }
        else
        {
            Destroy(GetComponent<Collider2D>());
            GetComponent<SpriteRenderer>().enabled = false;
            for (int i = 0; i < _DestroyObjects.Length; i++)
            {
                _DestroyObjects[i].SetActive(true);
                Rigidbody2D rigid = _DestroyObjects[i].GetComponent<Rigidbody2D>();
                if (rigid) rigid.AddForce(new Vector2(Random.Range(-1.0f, 1.0f), Random.Range(-1.0f, 1.5f)) * fPow, ForceMode2D.Impulse);
            }
        }
    }
}
