﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BladeTrap : MonoBehaviour
{
    [SerializeField] private Vector3 leftEndPos;
    [SerializeField] private Vector3 rightEndPos;
    [SerializeField] private float moveSpeed = 5.0f;
    [SerializeField] private bool isLeft = true;
    [SerializeField] private GameObject returnObj;

    private void Update()
    {
        Vector3 nowLocalPosition = transform.localPosition;
        if (isLeft)
        {
            if (nowLocalPosition.x > leftEndPos.x) transform.Translate(Vector3.left * moveSpeed * Time.deltaTime);
            else isLeft = false;
        }
        else
        {
            if (nowLocalPosition.x < rightEndPos.x) transform.Translate(Vector3.right * moveSpeed * Time.deltaTime);
            else isLeft = true;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.layer.Equals(9))
        {
            collision.gameObject.GetComponent<PDG.Player>().SetDamage(1, LocalizeManager.GetLocalize("killedby_trap_gear"), DEATH_TYPE.DEATH_FALL, string.Empty);
            collision.gameObject.transform.position = returnObj.transform.position;
        }
    }
}
