﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DrumHit : MonoBehaviour
{
    public bool isHit = false;
    [SerializeField] private float curHp = 5.0f;
    [SerializeField] private Animator animator;
    [SerializeField] private AudioSource audio;
    [SerializeField] private BoxCollider2D coll;
    [SerializeField] private BoxCollider2D collBack;
    [SerializeField] private SpriteRenderer sr;
    [SerializeField] private GameObject drumBombObj;
    private float hitTime = 0.5f;
    private void Awake()
    {
        animator = GetComponent<Animator>();
        audio = GetComponent<AudioSource>();
        coll = GetComponent<BoxCollider2D>();
        sr = GetComponent<SpriteRenderer>();
    }

    private void Update()
    {
        if(isHit)
        {
            hitTime -= Time.deltaTime;
            if(hitTime < 0.0f)
            {
                isHit = false;
                hitTime = 0.5f;
            }
        }
    }

    public void SetDamage(float _damage)
    {
        if (isHit) return;
        isHit = true;
        curHp -= _damage;

        float dis = Vector3.Distance(PDG.Player.instance.transform.position, transform.position);
        if (dis > 40) audio.volume = 0;
        if ( (SoundManager.isMute || (PDG.Dungeon.instance == null ? false : PDG.Dungeon.instance.isPause) || (PDG.Player.instance == null ? false : PDG.Player.instance.isDie)))
        {
            audio.volume = 0;
        }
        else
        {
            float tVolume =  (SoundManager.isMute || (PDG.Dungeon.instance == null ? false : PDG.Dungeon.instance.isPause) || (PDG.Player.instance == null ? false : PDG.Player.instance.isDie)) ? 0 : SoundManager.EffectVolume;
            float t = tVolume / 40;
            audio.volume = tVolume - (dis * t);
        }
        audio.Play();

        if (curHp <= 0.0f)
        {
            SoundManager.instance.StartAudio(new string[1] { "Object/DrumBomb" }, VOLUME_TYPE.EFFECT);
            animator.SetTrigger("bomb");
            coll.enabled = false;
            collBack.enabled = false;
            CameraShaker._instance.StartShake(1, Time.deltaTime, 0.2f);
        }
        else
        {
            animator.SetTrigger("hit");
        }
    }
        
    public void BombEnd()
    {
        sr.enabled = false;
        GameObject obj = Instantiate(drumBombObj);
        obj.GetComponent<DrumBomb>().isMonster = false;
        obj.GetComponent<DrumBomb>().InitSetting(1, transform.position, drumBombObj, false, false, 0.25f, string.Empty);
        Destroy(gameObject, 1.0f);
    }
}
