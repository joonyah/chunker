﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemCapsule : MonoBehaviour
{
    [SerializeField] private string itemID;
    [SerializeField] private Animator onoffAnim;
    AudioSource _audio;
    private bool isOpen = false;
    public int nowCount;
    UnlockListsData uData;
    public GameObject itemObj;
    private bool isUnlock = false;

    [SerializeField] private int num;
    public static bool isGetItem_0 = false;
    public static bool isGetItem_1 = false;
    public static bool isGetItem_2 = false;
    private void Awake()
    {
        _audio = GetComponent<AudioSource>();
        uData = Array.Find(SheetManager.Instance.UnlockListsDB.dataArray, item => item.Itemid.Equals(itemID));
        nowCount = SaveAndLoadManager.instance.LoadUnlock(itemID);
        if (nowCount == uData.Unlockcount && ((!isGetItem_0 && num == 0) || (!isGetItem_1 && num == 1) || (!isGetItem_2 && num == 2)))
        {
            isUnlock = true;
            Item _item = itemObj.GetComponent<Item>();
            _item._Item = new ItemInfo
            {
                Data = Array.Find(SheetManager.Instance.ItemsDB.dataArray, item => item.ID.Equals(itemID))
            };
            _item.dir = Vector2.zero;
            _item.itemCount = 1;
            _item.rPow = 0;
            _item.transform.localPosition = Vector3.zero;
            itemObj.SetActive(true);
        }
    }

    private void Update()
    {

        if (UnLock.Instance.ulList.FindIndex(item => item.nowUnlockID.Equals(itemID)) != -1 && !isUnlock)
        {
            nowCount = UnLock.Instance.ulList.Find(item => item.nowUnlockID.Equals(itemID)).nowCount;
            if (nowCount == uData.Unlockcount && ((!isGetItem_0 && num == 0) || (!isGetItem_1 && num == 1) || (!isGetItem_2 && num == 2)))
            {
                isUnlock = true;
                Item _item = itemObj.GetComponent<Item>();
                _item._Item = new ItemInfo
                {
                    Data = Array.Find(SheetManager.Instance.ItemsDB.dataArray, item => item.ID.Equals(itemID))
                };
                _item.dir = Vector2.zero;
                _item.itemCount = 1;
                _item.rPow = 0;
                _item.transform.localPosition = Vector3.zero;
                itemObj.SetActive(true);
            }
            else
            {
                itemObj.SetActive(false);
            }
        }

        RaycastHit2D hit = Physics2D.CircleCast(transform.position, 1, Vector2.zero, 0, 1 << 9);
        if (hit)
        {
            if (!isOpen && isUnlock)
            {
                isOpen = true;
                _audio.volume =  (SoundManager.isMute || (PDG.Dungeon.instance == null ? false : PDG.Dungeon.instance.isPause) || (PDG.Player.instance == null ? false : PDG.Player.instance.isDie)) ? 0 : SoundManager.EffectVolume;
                _audio.Play();
                onoffAnim.SetTrigger("open");
            }
        }
        else
        {
            if(isOpen)
            {
                isOpen = false;
                onoffAnim.SetTrigger("close");
                _audio.volume =  (SoundManager.isMute || (PDG.Dungeon.instance == null ? false : PDG.Dungeon.instance.isPause) || (PDG.Player.instance == null ? false : PDG.Player.instance.isDie)) ? 0 : SoundManager.EffectVolume;
                _audio.Play();
            }
        }
    }
}
