﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ResultItemSelect : MonoBehaviour, IPointerClickHandler
{
    public ItemInfo item;
    public bool isSelect = false;
    public GameObject infoObj;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(isSelect)
        {
            Vector3 pos = transform.position;
            pos.x += 64;
            pos.y -= 64;
            infoObj.transform.position = pos;
        }
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        if (eventData.button == PointerEventData.InputButton.Left)
        {
            ResultPanelScript.instance.ClickItem(gameObject, item);
        }
        else if (eventData.button == PointerEventData.InputButton.Right)
        {
            if (isSelect)
            {
                infoObj.SetActive(false);
                ResultPanelScript.instance.SelectItem(item);
            }
        }
    }
}
