﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ObjectTrap : MonoBehaviour
{
    [SerializeField] private bool isMove;
    [SerializeField] private bool isHorizontal;
    [SerializeField] private bool isLeftAndDown;
    [SerializeField] private Vector3 LeftAndDownPos;
    [SerializeField] private Vector3 RightAndUpPos;
    [SerializeField] private GameObject moveObject;
    [SerializeField] private float moveSpeed;
    public bool isStart;
    [SerializeField] private GameObject pressKey;
    [SerializeField] private GameObject pressKeyFire;

    [SerializeField] private bool isCharacterOn;
    [SerializeField] private bool isFire;
    [SerializeField] private ObjectTrap startObj;
    [SerializeField] private ObjectTrap targetObj;
    [SerializeField] private ObjectTrap[] startObjs;
    public Animator animator;
    [SerializeField] private Vector3 firePos;
    public bool isTarget;
    public int targetCount = 0;
    [SerializeField] private GameObject npcObj;
    public float fireDelay = 1.0f;
    public bool isEnd;
    float fireDelayNow = 0.0f;

    [SerializeField] private BoxCollider2D offCollision;
    [SerializeField] private EdgeCollider2D onCollision;
    [SerializeField] private Text countText;
    [SerializeField] private GameObject uiObj;

    private GameObject goodPanel;
    private GameObject missPanel;
    private static bool firstOn = false;
    Text keyText;
    void Start()
    {
        if (isCharacterOn)
        {
            pressKey = GameObject.Find("Canvas").transform.Find("PressKeySwitch").gameObject;
            countText = GameObject.Find("Canvas").transform.Find("MiniGameBulletCountingSlot").Find("Text").GetComponent<Text>();
            uiObj = GameObject.Find("Canvas").transform.Find("MiniGameBulletCountingSlot").gameObject;
        }
        if (isFire) pressKeyFire = GameObject.Find("Canvas").transform.Find("PressKeyFire").gameObject;

        goodPanel = GameObject.Find("Canvas").transform.Find("GoodPanel").gameObject;
        missPanel = GameObject.Find("Canvas").transform.Find("MissPanel").gameObject;
        keyText = GameObject.Find("Canvas").transform.Find("TutorialText").GetComponent<Text>();
    }
    // Update is called once per frame
    void Update()
    {
        if(isEnd)
        {
            goodPanel.SetActive(false);
            missPanel.SetActive(false);
        }

        Vector3 nowLocalPosition = moveObject == null ? Vector3.zero : moveObject.transform.localPosition;
        if (isHorizontal && isLeftAndDown && isStart)
        {
            if (nowLocalPosition.x > LeftAndDownPos.x)
            {
                moveObject.transform.Translate(Vector3.left * moveSpeed * Time.deltaTime);
                if (isCharacterOn) PDG.Player.instance.transform.Translate(Vector3.left * moveSpeed * Time.deltaTime);
            }
            else isLeftAndDown = false;
        }
        else if (isHorizontal && !isLeftAndDown && isStart)
        {
            if (nowLocalPosition.x < RightAndUpPos.x)
            {
                moveObject.transform.Translate(Vector3.right * moveSpeed * Time.deltaTime);
                if (isCharacterOn) PDG.Player.instance.transform.Translate(Vector3.right * moveSpeed * Time.deltaTime);
            }
            else isLeftAndDown = true;
        }
        else if (!isHorizontal && isLeftAndDown && isStart)
        {
            if (nowLocalPosition.y > LeftAndDownPos.y)
            {
                moveObject.transform.Translate(Vector3.down * moveSpeed * Time.deltaTime);
                if (isCharacterOn) PDG.Player.instance.transform.Translate(Vector3.down * moveSpeed * Time.deltaTime);
            }
            else isLeftAndDown = false;
        }
        else if (!isHorizontal && !isLeftAndDown && isStart)
        {
            if (nowLocalPosition.y < RightAndUpPos.y)
            {
                moveObject.transform.Translate(Vector3.up * moveSpeed * Time.deltaTime);
                if (isCharacterOn) PDG.Player.instance.transform.Translate(Vector3.up * moveSpeed * Time.deltaTime);
            }
            else isLeftAndDown = true;
        }
        if(isCharacterOn)
        {
            RaycastHit2D hit = Physics2D.BoxCast(transform.position, new Vector2(1.5f, 2.0f), 0, Vector2.up, 1, 1 << 9);
            if (hit && !isStart )
            {
                pressKey.transform.position = Camera.main.WorldToScreenPoint(transform.position);
                pressKey.SetActive(true);
                if ((PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ACTIVE], true) || PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_ACTIVE], true)) && PDG.Dungeon.instance.closeCoolTime > 0.2f) 
                {
                    PDG.Dungeon.instance.closeCoolTime = 0.0f;
                    if(firstOn && Inventorys.Instance.goldbar < 5)
                    {
                        KeyTextOpen();
                        return;
                    }

                    if (firstOn)
                    {
                        Inventorys.Instance.goldbar -= 5;
                    }

                    firstOn = true;
                    SoundManager.instance.StartAudio(new string[1] { "Object/switch" }, VOLUME_TYPE.EFFECT);
                    isStart = true;
                    GetComponent<SpriteRenderer>().flipX = true;
                    for (int i = 0; i < startObjs.Length; i++)
                    {
                        startObjs[i].isStart = true;
                        startObjs[i].isEnd = false;
                    }
                    PDG.Camera c = FindObjectOfType(typeof(PDG.Camera)) as PDG.Camera;
                    c.target = moveObject.transform.parent;
                    PDG.Player.instance.isMiniGame = true;

                    offCollision.enabled = false;
                    onCollision.enabled = true;
                    targetObj.targetCount = 0;
                    Inventorys.Instance.miniGameBulletCount = 10;
                    Inventorys.Instance.miniGameBulletUnCount = 0;
                    if (uiObj) uiObj.SetActive(true);
                    if (countText) countText.text = Inventorys.Instance.miniGameBulletCount.ToString();
                    PDG.Dungeon.instance.TutorialText.GetComponent<Text>().text = LocalizeManager.GetLocalize("helldog_guide2");
                }
            }
            else
            {
                if (pressKey != null) pressKey.SetActive(false);
            }

            if(((isStart && !hit) || isEnd || Inventorys.Instance.miniGameBulletUnCount >= 10) && PDG.Player.instance.isMiniGame)
            {
                if (Inventorys.Instance.miniGameBulletUnCount >= 10 && targetObj.targetCount < 2) 
                {
                    SoundManager.instance.StartAudio(new string[1] { "Object/YouLose" }, VOLUME_TYPE.EFFECT);
                }
                else
                {
                    SoundManager.instance.StartAudio(new string[1] { "Object/YouWin" }, VOLUME_TYPE.EFFECT);
                }
                for (int i = 0; i < startObjs.Length; i++) startObjs[i].isStart = false;
                GetComponent<SpriteRenderer>().flipX = false;
                PDG.Camera c = FindObjectOfType(typeof(PDG.Camera)) as PDG.Camera;
                c.target = PDG.Player.instance.transform;
                PDG.Player.instance.isMiniGame = false;
                PDG.Player.instance.transform.position = transform.parent.parent.position + new Vector3(0, -10.2f, 0);
                moveObject.transform.localPosition = new Vector3(-7, -5.95f);
                if (uiObj) uiObj.SetActive(false);
                offCollision.enabled = true;
                onCollision.enabled = false;
                PDG.Dungeon.instance.TutorialText.GetComponent<Text>().text = LocalizeManager.GetLocalize("helldog_guide");
            }
        }
        if(isFire)
        {
            RaycastHit2D hit = Physics2D.BoxCast(transform.position, new Vector2(1.5f, 4.0f), 0, Vector2.zero, 0, 1 << 9);
            if (hit && startObj != null && startObj.isStart)
            {
                Vector3 pos = Camera.main.WorldToScreenPoint(transform.position);
                pos.y -= 1;
                pos.x += 75;
                pressKeyFire.transform.position = pos;
                pressKeyFire.SetActive(true);

                fireDelayNow += Time.deltaTime;
                if(fireDelayNow > fireDelay)
                {
                    if ((Input.GetKey(KeyCode.Space) || PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_ATTACK], true)) && Inventorys.Instance.miniGameBulletCount > 0 && PDG.Dungeon.instance.closeCoolTime > 0.2f) 
                    {
                        PDG.Dungeon.instance.closeCoolTime = 0.0f;
                        Inventorys.Instance.miniGameBulletCount--;
                        SoundManager.instance.StartAudio(new string[1] { "Object/cannon_" + Random.Range(0, 2) }, VOLUME_TYPE.EFFECT);
                        if (animator != null) animator.SetTrigger("Attack");
                        fireDelayNow = 0.0f;
                        if (startObj.countText) startObj.countText.text = Inventorys.Instance.miniGameBulletCount.ToString();
                    }


                }
            }
            else
            {
                if (pressKeyFire != null) pressKeyFire.SetActive(false);
            }
        }
        if(isTarget)
        {
            if(targetCount >= 2)
            {
                for (int i = 0; i < startObjs.Length; i++) startObjs[i].isEnd = true;
                if (npcObj != null) npcObj.SetActive(true);
                Dialogue.Instance.CommnetSetting("hell_dog", 0, null);
                Dialogue.Instance.offObj = npcObj;
                isEnd = true;
                gameObject.SetActive(false);
                PDG.Dungeon.instance.TutorialText.SetActive(false);
            }
        }
    }
    private void KeyTextOpen()
    {
        StartCoroutine(KeyTextOpen2());
    }
    IEnumerator KeyTextOpen2()
    {
        keyText.text = LocalizeManager.GetLocalize("gold_lackText");
        keyText.gameObject.SetActive(true);
        yield return new WaitForSeconds(2.0f);
        keyText.gameObject.SetActive(false);
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(gameObject.layer.Equals(23) && collision.gameObject.layer.Equals(13) && collision.gameObject.GetComponent<BulletController>() && collision.gameObject.GetComponent<BulletController>().isMiniGame)
        {
            SoundManager.instance.StartAudio(new string[1] { "Object/target_" + Random.Range(0, 2) }, VOLUME_TYPE.EFFECT);
            if (animator != null) animator.SetTrigger("Attack");
            if (collision.gameObject.GetComponent<BulletController>()) collision.gameObject.SetActive(false);
            if (isTarget)
            {
                targetCount++;
                StartCoroutine(PanelOpen(true));
            }
            else
            {
                StartCoroutine(PanelOpen(false));
            }

            Inventorys.Instance.miniGameBulletUnCount++;
            GameObject obj = ObjManager.Call().GetObject("Bullet_Hit");
            obj.transform.position = transform.position;
            obj.GetComponent<SwordHitEffectController>()._isMonster = true;
            obj.GetComponent<SwordHitEffectController>()._effctName = "pistol_bullet";
            obj.GetComponent<Animator>().Rebind();
            obj.SetActive(true);
        }
    }
    public void Fire()
    {
        GameObject obj = ObjManager.Call().GetObject("Bullet");
        obj.GetComponent<BulletController>().isMonster = false;
        obj.GetComponent<BulletController>().firePosition = transform.position;
        obj.GetComponent<BulletController>().bulletType = "startGun_Bullet";
        obj.GetComponent<BulletController>().fSpeed = 4;
        obj.GetComponent<BulletController>().fAngle = 90;
        obj.GetComponent<BulletController>().fRange = 20;
        obj.GetComponent<BulletController>().isRightAttack = false;
        obj.GetComponent<BulletController>().isPenetrate = false;
        firePos = transform.position + new Vector3(0, 0.5f, 0);
        Vector3 crossPos = GetPosition(firePos,90);
        firePos.z = crossPos.z = 0;
        Vector3 dir = crossPos - firePos;
        dir.Normalize();
        obj.GetComponent<BulletController>().dir = dir;
        obj.transform.position = firePos + (dir * 1.05f);
        obj.SetActive(true);
        obj.GetComponent<BulletController>().BulletFire();
        obj.GetComponent<BulletController>().isMiniGame = true;
    }
    public IEnumerator PanelOpen(bool isSuccess)
    {
        GameObject obj;
        if (isSuccess) obj = goodPanel;
        else obj = missPanel;
        obj.SetActive(true);
        float t = 0.0f;
        while(t < 1.0f)
        {
            Vector3 pos = Camera.main.WorldToScreenPoint(transform.position);
            pos.y += 50;
            obj.transform.position = pos;
            t += Time.deltaTime;
            yield return new WaitForSeconds(Time.deltaTime);
        }
        obj.SetActive(false);
    }
    #region -- Tool --
    public static float GetAngle(Vector3 vStart, Vector3 vEnd)
    {
        Vector3 v = vEnd - vStart;

        return Mathf.Atan2(v.y, v.x) * Mathf.Rad2Deg;
    }
    public static Vector3 GetPosition(Vector3 vStart, float angle)
    {
        float x = Mathf.Cos(Mathf.Deg2Rad * angle) + vStart.x;
        float y = Mathf.Sin(Mathf.Deg2Rad * angle) + vStart.y;

        return new Vector3(x, y, 0);
    }
    #endregion
}
