﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Chest : MonoBehaviour
{
    private Animator animator;
    private AudioSource source;

    private bool mimic = false;
    [SerializeField] private bool opened = false;

    public string BoxGrade = string.Empty;
    public float startDis = 2.0f;
    public string[] itemName;

    public bool TestCloseBox = false;
    public Sprite noneSprite;
    public Sprite outlineSprite;
    public bool isSelected = false;
    public bool isOpen = false;

    public bool isStartSetting = false;
    public string nowStageName = string.Empty;
    Text keyText;
    Coroutine keyTextCoroutine = null;
    public GameObject minimapObj;
    private bool isopenGuide = false;
    [SerializeField] private bool isStartMap;

    private void Awake()
    {
        animator = GetComponent<Animator>();
        source = GetComponent<AudioSource>();
        mimic = Random.Range(0, 5) == 0 ? true : false;
    }
    private void Start()
    {
        keyText = GameObject.Find("Canvas").transform.Find("TutorialText").GetComponent<Text>();
        if (isStartSetting)
        {
            BoxSetting(nowStageName);
        }
        if (isStartMap && !PDG.Dungeon.instance.isTestMode) gameObject.SetActive(false);
    }
    IEnumerator ItemGuide(string _id)
    {
        Inventorys.Instance.ItemGuideObj.transform.Find("Text").GetComponent<SetLanguage>().InitSetting(_id);
        yield return new WaitForSeconds(0.2f);
        Time.timeScale = 0;
        SoundManager.instance.StartAudio(new string[1] { "pressanykey" }, VOLUME_TYPE.EFFECT);
        Inventorys.Instance.ItemGuideObj.SetActive(true);
        Inventorys.Instance.isGuid = true;
    }
    private void Update()
    {
        int mask = 1 << 9;
        RaycastHit2D hit = Physics2D.CircleCast(transform.position, 2.0f, Vector2.zero, 1, mask);
        if (!hit) isSelected = false;
        else
        {
            if (SaveAndLoadManager.instance.LoadItem("Chest_Guid") == -1 && !isopenGuide)
            {
                isopenGuide = true;
                StartCoroutine(ItemGuide("Chest_Guid"));
            }
        }
        if (isSelected && !isOpen)
        {
            if (!isOpen) animator.enabled = false;
            GetComponent<SpriteRenderer>().sprite = outlineSprite;
            if (/*Inventorys.Instance.key > 0 &&*/ PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ACTIVE], true))
            {
                isOpen = true;
                animator.enabled = true;
                Inventorys.Instance.key--;
                Open();
            }
            else if (Inventorys.Instance.key <= 0 && PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ACTIVE], true))
            {
                KeyTextOpen();
            }
        }
        else
        {
            GetComponent<SpriteRenderer>().sprite = noneSprite;
        }
        if (TestCloseBox)
        {
            TestCloseBox = false;
            opened = false;
            GetComponent<SpriteRenderer>().sprite = noneSprite;
        }
    }
    private void KeyTextOpen()
    {
        if (keyTextCoroutine == null) keyTextCoroutine = StartCoroutine(KeyTextOpen2());
    }
    IEnumerator KeyTextOpen2()
    {
        keyText.text = LocalizeManager.GetLocalize("key_lackText");
        keyText.gameObject.SetActive(true);
        yield return new WaitForSeconds(2.0f);
        keyText.gameObject.SetActive(false);
        keyTextCoroutine = null;
    }
    public void Open()
    {
        if (opened) return;

        opened = true;

        //source.Play();
        SoundManager.instance.StartAudio(new string[1] { "Object/chest" }, VOLUME_TYPE.EFFECT);

        if (mimic)
        {
            animator.Play("Open");
            //animator.Play("Mimic");
        }
        else
        {
            animator.Play("Open");

        }
    }
    public void BoxOpen()
    {
        Vector3 tPos = transform.position;
        tPos.y += 1.0f;
        for (int i = 0; i < itemName.Length; i++)
        {
            float angle = -90.0f;
            angle += (i * (360.0f / itemName.Length));
            Vector3 pos = GetPosition(tPos, angle);
            Vector3 dir = pos - tPos;
            dir.Normalize();
            GameObject obj = ObjManager.Call().GetObject("Item");
            if (obj != null)
            {
                Item _item = obj.GetComponent<Item>();
                _item._Item = new ItemInfo
                {
                    Data = System.Array.Find(SheetManager.Instance.ItemsDB.dataArray, item => item.ID.Equals(itemName[i]))
                };
                _item.dir = dir;
                _item.itemCount = 1;
                _item.rPow = 1;
                float startDis2 = 1.0f * (itemName.Length / 10.0f);
                float angle2 = angle += 90.0f;
                if (angle2 <= 180) startDis2 -= (angle2 / 150.0f);
                else startDis2 -= ((180.0f / 150.0f) + ((180.0f - angle2) / 150.0f));

                obj.transform.position = pos + (dir * startDis2);
                obj.SetActive(true);
            }
        }
        minimapObj.SetActive(false);
    }
    public void BoxSetting(string _nowStageName)
    {
        while(true)
        {
            ChestPerData data = System.Array.Find(SheetManager.Instance.ChestPerDB.dataArray, item => item.Stagelevel.Equals(PDG.Dungeon.instance.StageLevel));
            BoxGrade = data.Chest[SaveAndLoadManager.GetBoxGrade(data)];
            BoxGrade = BoxGrade.Substring(BoxGrade.Length - 1, 1);

            List<ItemsData> iDatas = System.Array.FindAll(SheetManager.Instance.ItemsDB.dataArray, item => item.Grade.ToUpper().Equals(BoxGrade.ToUpper())).ToList();
            for (int i = 0; i < iDatas.Count; i++)
            {
                Sprite s = Resources.Load<Sprite>("Item/" + iDatas[i].Type1 + "/" + iDatas[i].Type2 + "/" + iDatas[i].ID);
                if (s == null || !Inventorys.Instance.UnlockAndMaxCountCheck(iDatas[i].ID))
                {
                    iDatas.RemoveAt(i);
                    i--;
                    continue;
                }
            }
            if (iDatas.Count > 0)
            {
                int ran = Random.Range(0, iDatas.Count);
                itemName = new string[1];
                itemName[0] = iDatas[ran].ID;
                Inventorys.Instance.shopOpenItems.Add(new ItemInfo() { Data = Inventorys.Instance.CopyItem(iDatas[ran]) });
                break;
            }
        }
    }
    #region -- Tool --
    public static float GetAngle(Vector3 vStart, Vector3 vEnd)
    {
        Vector3 v = vEnd - vStart;

        return Mathf.Atan2(v.y, v.x) * Mathf.Rad2Deg;
    }
    public static Vector3 GetPosition(Vector3 vStart, float angle)
    {
        float x = Mathf.Cos(Mathf.Deg2Rad * angle) + vStart.x;
        float y = Mathf.Sin(Mathf.Deg2Rad * angle) + vStart.y;

        return new Vector3(x, y, 0);
    }
    #endregion
}