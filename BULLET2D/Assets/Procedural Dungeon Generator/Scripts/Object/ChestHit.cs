﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ChestHit : MonoBehaviour
{
    [SerializeField] private float maxHp = 10.0f;
    [SerializeField] private float curHp = 10.0f;
    [SerializeField] private Animator animator;
    public bool isHit = false;

    [Header("ITEM SETTING TEST")]
    public bool isStartSetting = false;
    public string nowStageName = string.Empty;
    [Header("ITEM SETTING")]
    public string BoxGrade = string.Empty;
    public float startDis = 2.0f;
    public string[] itemName;

    public bool isOpen = false;
    AudioSource audio;
    public GameObject minimapObj;
    private bool isopenGuide = false;

    [SerializeField] private bool isNotItem;
    [SerializeField] private float hitTime;

    public bool isOpenBefore = false;
    public GameObject DestroyObj;
    bool isSelect = false;
    [SerializeField] private bool isHitChest;
    [SerializeField] private RuntimeAnimatorController[] anims;
    [SerializeField] private int _type;

    [SerializeField] private GameObject controllerObj;
    private void Awake()
    {
        animator = GetComponent<Animator>();
        audio = GetComponent<AudioSource>();

    }

    // Start is called before the first frame update
    void Start()
    {
        if (isHitChest)
        {
            _type = UnityEngine.Random.Range(0, anims.Length);
            animator.runtimeAnimatorController = anims[_type];
            BoxSetting();
        }
    }
    IEnumerator ItemGuide(string _id)
    {
        Inventorys.Instance.ItemGuideObj.transform.Find("Text").GetComponent<SetLanguage>().InitSetting(_id);
        yield return new WaitForSeconds(0.2f);
        Time.timeScale = 0;
        SoundManager.instance.StartAudio(new string[1] { "pressanykey" }, VOLUME_TYPE.EFFECT);
        Inventorys.Instance.ItemGuideObj.SetActive(true);
        Inventorys.Instance.isGuid = true;
    }
    // Update is called once per frame
    void Update()
    {
        if (isStartSetting && !isNotItem)
        {
            isStartSetting = false;
            BoxSetting();
        }

        if (isNotItem)
        {
            if (hitTime > 0.0f) hitTime -= Time.deltaTime;
            if (hitTime <= 0.0f) isHit = false;
        }

        if (isOpenBefore)
        {
            RaycastHit2D hit = Physics2D.BoxCast(transform.position + new Vector3(-0.06f, 0), new Vector2(2, 3), 0, Vector2.zero, 0, 1 << 9);
            if (hit && !isSelect && !isOpen && !PDG.Dungeon.instance.newObjectSelected)
            {
                isSelect = true;
                PDG.Dungeon.instance.newObjectSelected = true;
                PDG.Dungeon.instance.newObjectSelectedObj = gameObject;
                controllerObj.SetActive(true);
                controllerObj.GetComponent<SpriteRenderer>().sprite = PDG.Dungeon.instance.isJoystrick ? PDG.Dungeon.GetJoyStrickKeySprite(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_ACTIVE]) : PDG.Dungeon.GetJoyStrickKeySprite(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ACTIVE]);
                TextOpenController.instance.ShowText(LocalizeManager.GetLocalize("ChestText"), Color.white, 24, controllerObj, 0.5f, true);
            }
            else if (!hit)
            {
                isSelect = false;
                if (PDG.Dungeon.instance.newObjectSelectedObj == gameObject)
                {
                    PDG.Dungeon.instance.newObjectSelectedObj = null;
                    PDG.Dungeon.instance.newObjectSelected = false;
                }
                if (!TextOpenController.instance._text.text.Equals(LocalizeManager.GetLocalize("chest_unlock_text")))
                {
                    controllerObj.SetActive(false);
                    if (TextOpenController.instance.targetObj == controllerObj)
                    {
                        TextOpenController.instance.TextClose(Color.white);
                    }
                }
            }
                
            if (isSelect)
            {
                if ((PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ACTIVE], true) || PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_ACTIVE], true)) && !isOpen && PDG.Dungeon.instance.closeCoolTime > 0.2f) 
                {
                    PDG.Dungeon.instance.closeCoolTime = 0.0f;
                    if (PDG.Dungeon.instance.newObjectSelectedObj == gameObject)
                    {
                        PDG.Dungeon.instance.newObjectSelectedObj = null;
                        PDG.Dungeon.instance.newObjectSelected = false;
                    }
                    if (!TextOpenController.instance._text.text.Equals(LocalizeManager.GetLocalize("chest_unlock_text")))
                    {
                        controllerObj.SetActive(false);
                        if (TextOpenController.instance.targetObj == controllerObj)
                        {
                            TextOpenController.instance.TextClose(Color.white);
                        }
                    }
                    StateGroup.instance.nChestOpened++;
                    int totalOpenCount = SaveAndLoadManager.instance.LoadAchievements("openedbox_5")._cur;
                    AchievementsData aData_0 = Array.Find(SheetManager.Instance.AchievementsDB.dataArray, item => item.ID.Equals("openedbox_0"));
                    AchievementsData aData_1 = Array.Find(SheetManager.Instance.AchievementsDB.dataArray, item => item.ID.Equals("openedbox_1"));
                    AchievementsData aData_2 = Array.Find(SheetManager.Instance.AchievementsDB.dataArray, item => item.ID.Equals("openedbox_2"));
                    AchievementsData aData_3 = Array.Find(SheetManager.Instance.AchievementsDB.dataArray, item => item.ID.Equals("openedbox_3"));
                    AchievementsData aData_4 = Array.Find(SheetManager.Instance.AchievementsDB.dataArray, item => item.ID.Equals("openedbox_4"));
                    AchievementsData aData_5 = Array.Find(SheetManager.Instance.AchievementsDB.dataArray, item => item.ID.Equals("openedbox_5"));

                    SaveAndLoadManager.instance.SaveAchievements(new AchievementData[6] {
                      new AchievementData { _id = aData_0.ID, _cur = totalOpenCount + 1, _max = aData_0.MAX, _clear = false },
                      new AchievementData { _id = aData_1.ID, _cur = totalOpenCount + 1, _max = aData_1.MAX, _clear = false },
                      new AchievementData { _id = aData_2.ID, _cur = totalOpenCount + 1, _max = aData_2.MAX, _clear = false },
                      new AchievementData { _id = aData_3.ID, _cur = totalOpenCount + 1, _max = aData_3.MAX, _clear = false },
                      new AchievementData { _id = aData_4.ID, _cur = totalOpenCount + 1, _max = aData_4.MAX, _clear = false },
                      new AchievementData { _id = aData_5.ID, _cur = totalOpenCount + 1, _max = aData_5.MAX, _clear = false }
                    });

                    animator.SetTrigger("Open");
                    isOpen = true;
                    BoxOpen();
                }
            }
        }
        else
        {
            if (controllerObj) controllerObj.SetActive(false);
        }
    }

    public void SetDamage(float v)
    {
        if (isOpenBefore) return;
        curHp -= 1;
        if (audio && audio.enabled && audio.clip != null) audio.volume =  (SoundManager.isMute || (PDG.Dungeon.instance == null ? false : PDG.Dungeon.instance.isPause) || (PDG.Player.instance == null ? false : PDG.Player.instance.isDie)) ? 0 : SoundManager.EffectVolume;
        if (audio && audio.enabled && audio.clip != null) audio.Play();
        if (isNotItem)
        {
            hitTime = 0.25f;
            if (curHp <= 0)
            {
                //if ((GetComponent<SpriteRenderer>().sprite.name.Equals("box_0") || GetComponent<SpriteRenderer>().sprite.name.Equals("box_2")) && UnityEngine.Random.Range(0, 100) < 20) 
                //{
                //    itemName = new string[1];
                //    if (UnityEngine.Random.Range(0, 2) == 0)
                //        itemName[0] = "recovery_syringe_1";
                //    else
                //        itemName[0] = "recovery_syringe_2";
                //    BoxOpen(true);
                //}

                GameObject obj = Instantiate(DestroyObj);
                obj.transform.position = transform.position;
                Destroy(obj, 2.0f);
                Destroy(gameObject);
            }
            return;
        }
        if (curHp <= 0)
        {
            isOpenBefore = true;
            SoundManager.instance.StartAudio(new string[1] { "Object/lockopen" }, VOLUME_TYPE.EFFECT);
            SoundManager.instance.StartAudio(new string[1] { "Object/lockopen" }, VOLUME_TYPE.EFFECT);
            TextOpenController.instance.ShowText(LocalizeManager.GetLocalize("chest_unlock_text"), Color.white, 24, controllerObj, 3, false);
            animator.SetTrigger("OpenBefore");
        }
        else if (!isHit)
        {
            isHit = true;
            animator.SetTrigger("Hit");
        }
    }
    public void HitEnd()
    {
        isHit = false;
    }
    public void BoxOpen(bool notKnockback = false)
    {
        if (SteamStatsAndAchievements.instance != null) SteamStatsAndAchievements.instance.SettingStat("stat_box_open_count");
        SoundManager.instance.StartAudio(new string[1] { "Object/chest2" }, VOLUME_TYPE.EFFECT);
        Vector3 tPos = transform.position;
        tPos.y += 1.0f;
        for (int i = 0; i < itemName.Length; i++)
        {
            float angle = -90.0f;
            angle += (i * (360.0f / itemName.Length));
            Vector3 pos = GetPosition(tPos, angle);
            Vector3 dir = pos - tPos;
            dir.Normalize();
            GameObject obj = ObjManager.Call().GetObject("Item");
            if (obj != null)
            {
                ItemInfo info = new ItemInfo { Data = Inventorys.Instance.CopyItem(Array.Find(SheetManager.Instance.ItemsDB.dataArray, item => item.ID.Equals(itemName[i]))) };
                int t = Inventorys.Instance.shopOpenItems.FindIndex(items => items.Equals(info));
                if (t != -1)
                {
                    Inventorys.Instance.shopOpenItems.RemoveAt(t);
                }
                Inventorys.Instance.DiscardedItem.Add(info);
                Inventorys.Instance.DiscardedItemString.Add(info.Data.ID);

                Item _item = obj.GetComponent<Item>();
                _item._Item = info;
                _item.dir = dir;
                _item.itemCount = 1;
                _item.rPow = 1;
                string tempID = _item._Item.Data.ID;
                int ttt = tempID.LastIndexOf('_');
                if (ttt != -1 && ttt == tempID.Length - 2)
                    tempID = tempID.Substring(0, tempID.Length - 2);
                SaveAndLoadManager.instance.SaveItem(new ItemSave { _id = "EquipmentSave_" + tempID, _nCount = 1 });
                float startDis2 = startDis;
                float angle2 = angle += 90.0f;
                if (angle2 <= 180) startDis2 -= (angle2 / 150.0f);
                else startDis2 -= ((180.0f / 150.0f) + ((180.0f - angle2) / 150.0f));
                if (notKnockback)
                    obj.transform.position = transform.position;
                else
                    obj.transform.position = pos + (dir * startDis2);
                obj.SetActive(true);
            }
        }
        if (minimapObj) minimapObj.SetActive(false);
    }
    public void BoxSetting()
    {
        while(true)
        {
            ChestPerData data = Array.Find(SheetManager.Instance.ChestPerDB.dataArray, item => item.Stagelevel.Equals(PDG.Dungeon.instance.StageLevel));
            int gradeNum = SaveAndLoadManager.GetBoxGrade(data);
            BoxGrade = data.Chest[gradeNum];
            BoxGrade = BoxGrade.Substring(BoxGrade.Length - 1, 1);
            List<ItemsData> iDatas = Array.FindAll(SheetManager.Instance.ItemsDB.dataArray, item => item.Grade.ToUpper().Equals(BoxGrade.ToUpper()) && (_type == 0 ? item.Type1.Equals("Weapon") : item.Type1.Equals("Passive"))).ToList();
            for (int i = 0; i < iDatas.Count; i++)
            {
                Sprite s = Resources.Load<Sprite>("Item/" + iDatas[i].Type1 + "/" + iDatas[i].Type2 + "/" + iDatas[i].ID);
                if (s == null || !Inventorys.Instance.UnlockAndMaxCountCheck(iDatas[i].ID))
                {
                    iDatas.RemoveAt(i);
                    i--;
                    continue;
                }
            }
            if (iDatas.Count > 0)
            {
                ItemsData iData = iDatas[UnityEngine.Random.Range(0, iDatas.Count)];
                itemName = new string[1];
                itemName[0] = iData.ID;
                Inventorys.Instance.shopOpenItems.Add(new ItemInfo() { Data = Inventorys.Instance.CopyItem(iData) });
                break;
            }
        }
    }
    #region -- Tool --
    public static float GetAngle(Vector3 vStart, Vector3 vEnd)
    {
        Vector3 v = vEnd - vStart;

        return Mathf.Atan2(v.y, v.x) * Mathf.Rad2Deg;
    }
    public static Vector3 GetPosition(Vector3 vStart, float angle)
    {
        float x = Mathf.Cos(Mathf.Deg2Rad * angle) + vStart.x;
        float y = Mathf.Sin(Mathf.Deg2Rad * angle) + vStart.y;

        return new Vector3(x, y, 0);
    }
    #endregion
}
