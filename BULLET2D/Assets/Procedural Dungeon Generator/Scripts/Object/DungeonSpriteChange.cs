﻿using PDG;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DungeonSpriteChange : MonoBehaviour
{

    [SerializeField] private string type;
    [SerializeField] private string stage;
    [SerializeField] private SpriteRenderer sr;
    [SerializeField] private Sprite sprite;
    [SerializeField] private bool isAnimator;
    [SerializeField] private Animator animator;

    [SerializeField] private GameObject lightObj;
    [SerializeField] private Sprite noneSprite;

    private Dungeon dungeon;
    private Matrix matrix;
    [SerializeField] private bool isWallTop;
    [SerializeField] private bool isWallSide;
    [SerializeField] private bool isGround;
    // Start is called before the first frame update
    void Start()
    {
        if (lightObj) lightObj.SetActive(false);

        dungeon = Dungeon.instance;
        matrix = dungeon.GetComponent<Matrix>();

        stage = PDG.Dungeon.instance.StageName;
        sr = GetComponent<SpriteRenderer>();
        noneSprite = Resources.Load<Sprite>("NoneSprite");
        sprite = Resources.Load<Sprite>("DungeonSprite/" + type + "/" + stage + "/" + type);
        if (sprite == null)
        {
            Sprite[] sprites = Resources.LoadAll<Sprite>("DungeonSprite/" + type + "/" + stage);
            if (sprites == null || sprites.Length == 0) sr.sprite = noneSprite;
            else
            {
                sr.enabled = true;
                sr.sprite = sprites[Random.Range(0, sprites.Length)];
            }
        }
        else
        {
            sr.enabled = true;
            sr.sprite = sprite;
        }

        if (isAnimator)
        {
            animator = GetComponent<Animator>();
            RuntimeAnimatorController run = Resources.Load<RuntimeAnimatorController>("DungeonSprite/" + type + "/" + stage + "/" + type);
            if (run == null)
                animator.enabled = false;
            else
                animator.runtimeAnimatorController = run;
        }

        if (stage.Equals("Dungeon_0") && type.Contains("wall_fountain_mid"))
        {
            if (lightObj) lightObj.SetActive(true);
        }


        if (stage.Equals("Dungeon_1") && type.Contains("wall_column_top") && sr.sprite.name.Equals("3"))
        {
            if (lightObj)
            {
                lightObj.SetActive(true);
            }

        }
        if (stage.Equals("Dungeon_3") && type.Contains("wall_column_top") && sr.sprite.name.Equals("6"))
        {
            if (lightObj)
            {
                lightObj.SetActive(true);
            }
        }
        if (stage.Equals("Dungeon_2") && type.Contains("wall_column_top") && sr.sprite.name.Equals("9"))
        {
            if (lightObj)
            {
                lightObj.SetActive(true);
            }
        }
        if (stage.Equals("Dungeon_2") && type.Contains("wall_column_top") && sr.sprite.name.Equals("10"))
        {
            if (lightObj)
            {
                lightObj.SetActive(true);
            }
        }
        if (stage.Equals("Dungeon_4") && type.Contains("wall_column_top") && sr.sprite.name.Equals("17"))
        {
            if (lightObj)
            {
                lightObj.SetActive(true);
            }
        }

        Vector3 p = transform.position;
        int x = Mathf.FloorToInt(p.x);
        int y = Mathf.FloorToInt(p.y);

        if (isWallSide)
        {
            if (Dungeon.instance.matrix.map[x, y - 1] == Dungeon.instance.wall_right)
            {
                sr.sortingLayerName = "Floor";
                sr.color = Color.red;
            }
            else if (Dungeon.instance.matrix.map[x, y - 1] == Dungeon.instance.wall_left)
            {
                sr.sortingLayerName = "Floor";
                sr.color = Color.blue;
            }
            else if (Dungeon.instance.board.map[x, y - 1] == Dungeon.instance.wall_side_front_left)
            {
                sr.sortingLayerName = "Floor";
                sr.color = Color.green;
            }
            else if (Dungeon.instance.board.map[x, y - 1] == Dungeon.instance.wall_side_front_right)
            {
                sr.sortingLayerName = "Floor";
                sr.color = Color.yellow;
            }
            else
            {
                sr.sortingLayerName = "Background";
            }
        }
        if(isWallTop)
        {
            if (Ground)
            {
                sr.sortingLayerName = "Floor";
                GetComponent<BoxCollider2D>().enabled = true;
                transform.Find("GameObject").GetComponent<BoxCollider2D>().enabled = true;
            }
            else
            {
                sr.sortingLayerName = "Background";
                GetComponent<BoxCollider2D>().enabled = false;
                transform.Find("GameObject").GetComponent<BoxCollider2D>().enabled = false;
            }
        }
        isGround = Ground;
        if (type.Equals("wall_column_base") && stage.Equals("Dungeon_3")) sr.color = Color.white;
        else if (type.Equals("wall_column_base") && stage.Equals("Dungeon_2")) sr.color = Color.white;
        else if (type.Equals("wall_column_base") && stage.Equals("Dungeon_0")) sr.color = Color.white;
        else if (type.Equals("wall_column_base") && stage.Equals("Dungeon_4")) sr.color = Color.white;
        else if (type.Equals("wall_column_base") && !stage.Equals("Dungeon_3")) sr.color = new Color32(181, 181, 181, 255);
    }
    private bool Ground
    {
        get
        {
            try
            {
                int x = (int)transform.position.x;
                int y = (int)transform.position.y;

                return matrix.map[x, y - 2] == dungeon.floor_1;
            }
            catch (System.Exception e)
            {
                Debug.LogError(e.Message);
                return false;
            }
        }
    }

    private void Update()
    {
        if (Player.instance == null) return;
        if (!isWallTop) return;
        if (isGround) return;
        float distance = Vector3.Distance(Player.instance.transform.position, transform.position);
        if (distance < 6)
        {
            sr.color = new Color(1, 1, 1, 0.5f);
        }
        else
        {
            sr.color = Color.white;
        }
    }
}
