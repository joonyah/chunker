﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
//[RequireComponent(typeof(PolygonCollider2D))]
public class ObjectFragments : MonoBehaviour
{
    [SerializeField] private SpriteRenderer sr;
    [SerializeField] private float timer = 1.0f;
    [SerializeField] private float timerNow = 0.0f;
    [SerializeField] private Rigidbody2D rigid;
    public bool isStart = false;
    private void Awake()
    {
        sr = GetComponent<SpriteRenderer>();
        rigid = GetComponent<Rigidbody2D>();
    }
    public void InitSetting(Sprite _sprite, float _time)
    {
        timer = _time;
        sr.sprite = _sprite;
        if (GetComponent<PolygonCollider2D>()) Destroy(GetComponent<PolygonCollider2D>());
        gameObject.AddComponent<PolygonCollider2D>();
    }
    private void OnEnable()
    {
        timerNow = 0.0f;
    }

    private void Update()
    {
        timerNow += Time.deltaTime;
        if(timerNow > timer && !isStart)
        {
            rigid.bodyType = RigidbodyType2D.Static;
            timerNow = 0.0f;
            isStart = true;
        }
    }

    public void ReDestroyObject()
    {
        if(transform.parent.name.Contains("puddle"))
        {
            if (GetComponent<PolygonCollider2D>()) Destroy(GetComponent<PolygonCollider2D>());
            return;
        }
        isStart = false;
        rigid.bodyType = RigidbodyType2D.Dynamic;
    }
}
