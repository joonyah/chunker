﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Trap : MonoBehaviour
{
    [SerializeField] private SpriteRenderer sr;
    [SerializeField] private Animator animator;
    [SerializeField] private BoxCollider2D coll;
    [SerializeField] private bool isSickle = false;
    [SerializeField] private bool isSpikes = false;
    [SerializeField] private bool isFall = false;
    public UInt64 count = 0;
    public UInt64 changeCount = 50;
    Vector3 rotateDir = Vector3.forward;
    float startDelay = 0.0f;

    [Header("튜토전용")]
    [SerializeField] private Vector3 returnPosition;
    [SerializeField] private GameObject parantObj;


    [SerializeField] private string trapName = string.Empty;
    AudioSource audio;

    private void Awake()
    {
        sr = GetComponent<SpriteRenderer>();
        animator = GetComponent<Animator>();
        coll = GetComponent<BoxCollider2D>();
        audio = GetComponent<AudioSource>();
        if(isSickle)
        {
            transform.localEulerAngles = new Vector3(0, 0, -60.0f);
            count = 0;
            startDelay = UnityEngine.Random.Range(0.0f, 2.0f);
        }
    }

    private void FixedUpdate()
    {
        startDelay -= Time.fixedDeltaTime;
        if(startDelay <= 0.0f)
        {
            startDelay = 0.0f;
            if (isSickle)
            {
                count++;
                if (count > changeCount)
                {
                    count = 0;
                    rotateDir *= -1;
                }
                transform.RotateAround(rotateDir, Time.fixedDeltaTime);
            }
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.layer.Equals(9))
        {
            if (collision.gameObject.layer.Equals(9))
            {
                if (isSickle)
                {
                    float z = transform.localEulerAngles.z;
                    if (z <= 20 && z >= -20)
                    {
                        if(!PDG.Player.instance.isHit && !PDG.Player.instance.isDie)
                        {
                            PDG.Player.instance.SetDamage(1, LocalizeManager.GetLocalize("killedby_trap_sickle"), DEATH_TYPE.DEATH_FALL, string.Empty);
                            GameObject obj2 = ObjManager.Call().GetObject("Hit_sprite7");
                            obj2.transform.position = PDG.Player.instance.transform.position;
                            obj2.transform.localScale = Vector3.one;
                            obj2.GetComponent<SwordHitEffectController>()._effctName = "playerblood";
                            obj2.GetComponent<SwordHitEffectController>().animSpeed = 1.0f;
                            obj2.GetComponent<Animator>().Rebind();
                            obj2.SetActive(true);
                            SoundManager.instance.StartAudio(new string[1] { "Object/Sickle_hit" }, VOLUME_TYPE.EFFECT);

                            Vector3 dir = PDG.Player.instance.transform.position - transform.position;
                            dir.Normalize();

                            RaycastHit2D hit2 = Physics2D.Raycast(PDG.Player.instance.hero.transform.position + dir, dir, 1, 1 << 8);
                            if (!hit2) PDG.Player.instance.hero.transform.Translate(dir * 50.0f * Time.deltaTime);
                            PDG.Player.instance.hero.isBack = true;
                            PDG.Player.instance.hero.backTime = 0.4f;
                        }
                        
                    }
                }
                else if (isFall)
                {

                }
                else if (isSpikes)
                {
                    PDG.Player.instance.SetDamage(1, LocalizeManager.GetLocalize("killedby_trap_tack"), DEATH_TYPE.DEATH_FALL, string.Empty);
                    collision.transform.position = parantObj.transform.position + returnPosition;
                }
                else
                {
                    if(!PDG.Player.instance.isHit)
                    {
                        GameObject obj2 = ObjManager.Call().GetObject("Hit_sprite12");
                        obj2.transform.position = collision.transform.position;
                        obj2.transform.localScale = Vector3.one;
                        obj2.GetComponent<SwordHitEffectController>()._effctName = "fire_hit";
                        obj2.GetComponent<SwordHitEffectController>().animSpeed = 1.0f;
                        obj2.GetComponent<Animator>().Rebind();
                        obj2.SetActive(true);
                        PDG.Player.instance.SetDamage(1, LocalizeManager.GetLocalize("killedby_trap_fire"), DEATH_TYPE.DEATH_FIRE, string.Empty);
                    }
                }
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.layer.Equals(9))
        {
            if (collision.gameObject.layer.Equals(9))
            {
                if (isFall)
                {
                    PlayerCombat.ps = PlayerState.Fall;
                    PlayerCombat.fallPosition = transform.position;
                }
            }
        }
    }

    public void AnimationStart()
    {
        coll.enabled = true;
        if(audio)
        {
            float dis = Vector3.Distance(PDG.Player.instance.transform.position, transform.position);
            if (dis > 5) audio.volume = 0;
            float tVolume =  (SoundManager.isMute || (PDG.Dungeon.instance == null ? false : PDG.Dungeon.instance.isPause) || (PDG.Player.instance == null ? false : PDG.Player.instance.isDie)) ? 0 : SoundManager.EffectVolume;
            float t = tVolume / 5;
            audio.volume = tVolume - (dis * t);
            audio.Play();
        }
    }

    public void AnimationEnd()
    {
        coll.enabled = false;
    }
}
