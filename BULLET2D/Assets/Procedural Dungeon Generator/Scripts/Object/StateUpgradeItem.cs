﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class StateUpgradeItem : MonoBehaviour, IPointerClickHandler
{
    //public Button _btn;
    public Image _mainImage;
    public string StatType = string.Empty;
    public int nowLevel = 0;
    public int maxLevel = 0;
    public GameObject selectedObj;
    public Sprite onSprite;
    public Sprite offSprite;
    public bool isSelected;
    public Image[] levelImages;
    public Text nameText;

    private void Start()
    {

        for (int i = 0; i < levelImages.Length; i++)
        {
            Button btn = levelImages[i].GetComponent<Button>();
            int level = i + 1;
            btn.onClick.AddListener(() => { StateUpgradeController.instance.ButtonClickEvnet(this, level); });
        }
    }
    public void FirstClick()
    {
        StateUpgradeController.instance.ButtonClickEvnet(this);
    }
    public void SecondClick()
    {
        if(nowLevel < maxLevel)
        {
            StateUpgradeController.instance.PopupOepn(this);
        }
    }
    private void Update()
    {
        for (int i = 0; i < levelImages.Length; i++)
        {
            if(i < nowLevel)
            {
                levelImages[i].sprite = onSprite;
            }
            else if(i >= maxLevel)
            {
                levelImages[i].gameObject.SetActive(false);
            }
            else
            {
                levelImages[i].sprite = offSprite;
            }
        }
    }
    public void OnPointerClick(PointerEventData eventData)
    {
        if (eventData.button == PointerEventData.InputButton.Left)
            FirstClick();
        else if (eventData.button == PointerEventData.InputButton.Right)
        {
            if (isSelected) SecondClick();
        }
    }
}
