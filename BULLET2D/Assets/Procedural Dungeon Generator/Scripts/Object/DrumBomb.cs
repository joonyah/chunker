﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DrumBomb : MonoBehaviour
{
    public string elementalName = string.Empty;
    public bool isMonster = false;
    public bool isPlayer = false;
    int _type = 0;
    AudioSource audio;
    int range = 15;
    [SerializeField] private bool isIce = false;
    [SerializeField] private bool isPoison = false;

    [SerializeField] private float hitTime = 0.25f;
    [SerializeField] private List<GameObject> objList = new List<GameObject>();
    [SerializeField] private List<float> timeList = new List<float>();
    [SerializeField] private GameObject hitEffect;
    string mobID;
    private void Awake()
    {
        audio = GetComponent<AudioSource>();
    }

    public void InitSetting(int type, Vector3 pos, GameObject _obj, bool _isMonster, bool _isIce, float _hitTime, string _mobID, bool noDestroy = false)
    {
        objList.Clear();
        timeList.Clear();
        hitTime = _hitTime;
        isIce = _isIce;
        isMonster = _isMonster;
        _type = type;
        mobID = _mobID;
        transform.position = pos;
        int next = type + 1;

        if (type == 0 && _isIce)
        {
            if (audio) audio.enabled = true;
            if (audio) audio.Play();
        }

        if (_type < 3)
        {
            GameObject obj;
            RaycastHit2D hit;
            Vector2 nPos = new Vector2(pos.x - 1, pos.y);
            hit = Physics2D.Raycast(nPos, Vector2.zero, 0, 1 << 23);
            if (!hit && (PDG.Dungeon.instance.nowRoomType.Equals("normal") ? PDG.Dungeon.instance.Find(Mathf.FloorToInt(nPos.x), Mathf.FloorToInt(nPos.y), PDG.Dungeon.instance.floor_1) : true))
            {
                obj = Instantiate(_obj);
                obj.GetComponent<DrumBomb>().InitSetting(next, nPos, _obj, _isMonster, _isIce, _hitTime, _mobID);
            }
            nPos = new Vector2(pos.x + 1, pos.y);
            hit = Physics2D.Raycast(nPos, Vector2.zero, 0, 1 << 23);
            if (!hit && (PDG.Dungeon.instance.nowRoomType.Equals("normal") ? PDG.Dungeon.instance.Find(Mathf.FloorToInt(nPos.x), Mathf.FloorToInt(nPos.y), PDG.Dungeon.instance.floor_1) : true))
            {
                obj = Instantiate(_obj);
                obj.GetComponent<DrumBomb>().InitSetting(next, nPos, _obj, _isMonster, _isIce, _hitTime, _mobID);
            }
            nPos = new Vector2(pos.x, pos.y + 1);
            hit = Physics2D.Raycast(nPos, Vector2.zero, 0, 1 << 23);
            if (!hit && (PDG.Dungeon.instance.nowRoomType.Equals("normal") ? PDG.Dungeon.instance.Find(Mathf.FloorToInt(nPos.x), Mathf.FloorToInt(nPos.y), PDG.Dungeon.instance.floor_1) : true))
            {
                obj = Instantiate(_obj);
                obj.GetComponent<DrumBomb>().InitSetting(next, nPos, _obj, _isMonster, _isIce, _hitTime, _mobID);
            }
            nPos = new Vector2(pos.x, pos.y - 1);
            hit = Physics2D.Raycast(nPos, Vector2.zero, 0, 1 << 23);
            if (!hit && (PDG.Dungeon.instance.nowRoomType.Equals("normal") ? PDG.Dungeon.instance.Find(Mathf.FloorToInt(nPos.x), Mathf.FloorToInt(nPos.y), PDG.Dungeon.instance.floor_1) : true))
            {
                obj = Instantiate(_obj);
                obj.GetComponent<DrumBomb>().InitSetting(next, nPos, _obj, _isMonster, _isIce, _hitTime, _mobID);
            }
        }
        if(GetComponent<Animator>())
        {
            AnimatorStateInfo _stateInfo = GetComponent<Animator>().GetCurrentAnimatorStateInfo(0);
            GetComponent<Animator>().Play(_stateInfo.fullPathHash, -1, Random.Range(0.0f, 1.0f));
        }
        if (Random.Range(0, 100) < 80 && !noDestroy)
        {
            Destroy(gameObject);
        }
        else
        {
            Destroy(gameObject, Random.Range(5, 8));
        }
    }
    public void InitSetting(Vector3 pos)
    {
        transform.position = pos;
        Destroy(gameObject, Random.Range(10, 15));
    }
    IEnumerator Fire(int type, Vector3 pos, GameObject _obj)
    {
        yield return new WaitForSeconds(0.5f);
    }

    private void Update()
    {
        if (PDG.Player.instance != null)
        {
            float dis = Vector3.Distance(PDG.Player.instance.transform.position, transform.position);
            if (dis > range && audio) audio.volume = 0;
            if ( (SoundManager.isMute || (PDG.Dungeon.instance == null ? false : PDG.Dungeon.instance.isPause) || (PDG.Player.instance == null ? false : PDG.Player.instance.isDie)))
            {
                if (audio) audio.volume = 0;
            }
            else
            {
                float tVolume = SoundManager.AmbientVolume;
                float t = tVolume / range;
                if (audio) audio.volume = tVolume - (dis * t);
            }
        }
        else
        {
            if (audio) audio.volume = 0;
        }


        for (int i = 0; i < objList.Count; i++)
        {
            if (objList[i] == null)
            {
                objList.RemoveAt(i);
                timeList.RemoveAt(i);
                i--;
                continue;
            }
            else if (objList[i].GetComponent<MOB.MonsterArggro>())
            {
                if (objList[i].GetComponent<MOB.MonsterArggro>()._monster.isDie)
                {
                    objList.RemoveAt(i);
                    timeList.RemoveAt(i);
                    i--;
                    continue;
                }
            }
            else if (objList[i].GetComponent<MOB.Monster>())
            {
                if (objList[i].GetComponent<MOB.Monster>().isDie)
                {
                    objList.RemoveAt(i);
                    timeList.RemoveAt(i);
                    i--;
                    continue;
                }
            }
            timeList[i] += Time.deltaTime;
        }

        RaycastHit2D[] hit;
        if (isMonster)
        {
            hit = Physics2D.CircleCastAll(transform.position, 0.2f, new Vector2(0, -1), 1, 1 << 9 | 1 << 23);
        }
        else if (isPlayer)
        {
            hit = Physics2D.CircleCastAll(transform.position, 0.2f, new Vector2(0, -1), 1, 1 << 12 | 1 << 24 | 1 << 23);
        }
        else
        {
            hit = Physics2D.CircleCastAll(transform.position, 0.2f, new Vector2(0, -1), 1, 1 << 12 | 1 << 9 | 1 << 24 | 1 << 23);
        }

        if (hit.Length > 0)
        {
            for (int i = 0; i < hit.Length; i++)
            {
                GameObject obj = hit[i].collider.gameObject;
                int index = objList.FindIndex(item => item.Equals(obj));
                if (index != -1)
                {
                    if (hitTime > timeList[index]) return;
                    else timeList[index] = 0.0f;
                }
                else
                {
                    objList.Add(obj);
                    timeList.Add(0.0f);
                }

                Collider2D coll = hit[i].collider;
                if (coll.gameObject.layer.Equals(9) && coll.GetComponent<PDG.Player>() && !coll.GetComponent<PDG.Player>().isHit)
                {
                    coll.GetComponent<PDG.Player>().SetDamage(1,
                        isIce ? LocalizeManager.GetLocalize("death_ice") : (isPoison ? LocalizeManager.GetLocalize("death_poison") : LocalizeManager.GetLocalize("death_fire")),
                        isPoison ? DEATH_TYPE.DEATH_POSION : (isIce ? DEATH_TYPE.DEATH_NORMAL : DEATH_TYPE.DEATH_FIRE),
                        mobID,
                        isIce ? ELEMENTAL_TYPE.FREEZE : (isPoison ? ELEMENTAL_TYPE.POISON : ELEMENTAL_TYPE.FIRE));
                    if (hitEffect) 
                    {
                        GameObject hitObj = Instantiate(hitEffect);
                        hitObj.transform.position = coll.transform.position;
                        Destroy(hitObj, 2f);
                    }
                    if (isIce) Destroy(gameObject);
                }
                else if(coll.gameObject.layer.Equals(12) && coll.GetComponent<MOB.Monster>())
                {
                    //if (isPlayer) coll.GetComponent<MOB.Monster>().SetDamage(PDG.Player.instance.damage / 2, Vector2.zero, 0, ELEMENTAL_TYPE.FIRE, 3.0f);
                    coll.GetComponent<MOB.Monster>().fDotTime = 3.0f;
                    coll.GetComponent<MOB.Monster>().nowElementalType = ELEMENTAL_TYPE.FIRE;
                    coll.GetComponent<MOB.Monster>().elementalName = elementalName;
                }
                else if (coll.gameObject.layer.Equals(24) && coll.GetComponent<MOB.BossMonster_Six_Apear>() && !coll.GetComponent<MOB.BossMonster_Six_Apear>().isHit)
                {
                    coll.GetComponent<MOB.BossMonster_Six_Apear>().SetDamage(1);
                }
                else if (coll.gameObject.layer.Equals(24) && coll.GetComponent<Boss_STG>() && !coll.GetComponent<Boss_STG>().isHit)
                {
                    coll.GetComponent<Boss_STG>().SetDamage(1, hit[i].point);
                }
                else if (coll.gameObject.layer.Equals(24) && coll.GetComponent<Boss_NPC>() && !coll.GetComponent<Boss_NPC>().isHit)
                {
                    coll.GetComponent<Boss_NPC>().SetDamage(1, hit[i].point);
                }
                else if (coll.gameObject.layer.Equals(24) && coll.GetComponent<Boss_Stone>() && !coll.GetComponent<Boss_Stone>().isHit)
                {
                    coll.GetComponent<Boss_Stone>().SetDamage(1, hit[i].point, false);
                }
                else if (coll.gameObject.layer.Equals(24) && coll.GetComponent<Boss_Golem>() && !coll.GetComponent<Boss_Golem>().isHit)
                {
                    coll.GetComponent<Boss_Golem>().SetDamage(1, hit[i].point);
                }
                else if(coll.gameObject.layer.Equals(23))
                {
                    if (coll.transform.parent && coll.transform.parent.GetComponent<TentacleController>()) coll.transform.parent.GetComponent<TentacleController>().SetDamage(1);
                    if (coll.GetComponent<ChestHit>() && !coll.GetComponent<ChestHit>().isHit) coll.GetComponent<ChestHit>().SetDamage(1);
                    if (coll.GetComponent<DrumHit>() && !coll.GetComponent<DrumHit>().isHit) coll.GetComponent<DrumHit>().SetDamage(1);
                }
            }
        }
    }
}
