﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletTrap : MonoBehaviour
{
    [SerializeField] private Vector3 dir;
    [SerializeField] private float moveSpeed;
    [SerializeField] private float delay;
    [SerializeField] private int amount;
    [SerializeField] private float totalDelay;
    [SerializeField] private bool isRandom;

    [SerializeField] private int RandomAmountMin;
    [SerializeField] private int RandomAmountMax;
    [SerializeField] private float RandomDelayMin;
    [SerializeField] private float RandomDelayMax;

    private void Start()
    {
        if(isRandom)
        {
            amount = Random.Range(RandomAmountMin, RandomAmountMax);
            delay = Random.Range(RandomDelayMin, RandomDelayMax);
        }

        StartCoroutine(BulletFire());    
    }

    IEnumerator BulletFire()
    {
        yield return new WaitForSeconds(Random.Range(0.0f, 2.0f));
        while (true)
        {
            yield return new WaitForSeconds(totalDelay);
            for (int i = 0; i < amount; i++)
            {
                GameObject obj = ObjManager.Call().GetObject("Bullet");
                obj.GetComponent<BulletController>().isMonster = true;
                obj.GetComponent<BulletController>().firePosition = transform.position;
                obj.GetComponent<BulletController>().monsterName = "killedby_trap_bullet";
                obj.GetComponent<BulletController>().bulletType = "Monster_Bullet";
                obj.GetComponent<BulletController>().fSpeed = moveSpeed;
                obj.GetComponent<BulletController>().fAngle = 0.0f;
                obj.GetComponent<BulletController>().fRange = 20;
                obj.GetComponent<BulletController>().isPenetrate = false;
                obj.GetComponent<BulletController>().isRightAttack = false;
                obj.GetComponent<BulletController>().dir = dir;
                obj.transform.position = transform.position;
                obj.SetActive(true);
                obj.GetComponent<BulletController>().BulletFire();
                yield return new WaitForSeconds(delay);
            }
        }


    }
}
