﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GolemPunchTrap : MonoBehaviour
{
    [SerializeField] private Vector3[] positions;
    [SerializeField] private float moveSpeed;
    [SerializeField] private float delayTime;
    [SerializeField] private SpriteRenderer shadowSr;
    [SerializeField] private GameObject returnObj;
    BoxCollider2D bColl;

    private void Start()
    {
        bColl = GetComponent<BoxCollider2D>();
        bColl.enabled = false;
        StartCoroutine(ShadowAction());
    }

    IEnumerator ShadowAction()
    {
        while(true)
        {
            yield return new WaitForSeconds(delayTime);
            Vector3 curPos = positions[Random.Range(0, positions.Length)];
            shadowSr.transform.localPosition = curPos;
            curPos.y += 20.0f;
            transform.localPosition = curPos;
            curPos.y -= 20.0f;
            float a = 0.0f;
            while (a < 1.0f)
            {
                a += Time.deltaTime;
                shadowSr.color = new Color(1, 1, 1, a);
                yield return new WaitForSeconds(Time.deltaTime / 2);
            }
            shadowSr.color = new Color(1, 1, 1, 1);
            yield return new WaitForSeconds(1.0f);
            a = 1.0f;
            while (a > 0.0f)
            {
                a -= Time.deltaTime;
                shadowSr.color = new Color(1, 1, 1, a);
                yield return new WaitForSeconds(Time.deltaTime / 2);
            }
            shadowSr.color = new Color(1, 1, 1, 0);
            yield return new WaitForSeconds(0.5f);

            while(true)
            {
                float y = curPos.y;
                y += 2.0f;
                if (y >= transform.localPosition.y) bColl.enabled = true;
                if (curPos.y > transform.localPosition.y)
                {
                    // 이펙트 
                    GameObject obj3 = ObjManager.Call().GetObject("Hit_sprite12");
                    Vector3 pos3 = transform.position;
                    obj3.transform.position = pos3;
                    obj3.transform.localScale = Vector3.one;
                    obj3.GetComponent<SwordHitEffectController>()._effctName = "PikoJumpDown2";
                    obj3.GetComponent<SwordHitEffectController>().animSpeed = 0.2f;
                    obj3.GetComponent<Animator>().Rebind();
                    obj3.SetActive(true);
                    CameraShaker._instance.StartShake(0.2f, Time.deltaTime, 0.3f);
                    SoundManager.instance.StartAudio(new string[1] { "Monster/golemPunch" }, VOLUME_TYPE.EFFECT);
                    break;
                }
                transform.Translate(Vector3.down * moveSpeed * Time.deltaTime);
                yield return new WaitForSeconds(Time.deltaTime);
            }
            transform.localPosition = curPos;
            bColl.enabled = false;
            yield return new WaitForSeconds(0.5f);
            curPos.y += 20.0f;
            transform.localPosition = curPos;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.layer.Equals(9))
        {
            collision.gameObject.GetComponent<PDG.Player>().SetDamage(1, LocalizeManager.GetLocalize("killedby_trap_golempunch"), DEATH_TYPE.DEATH_FALL, string.Empty);
            collision.gameObject.transform.position = returnObj.transform.position;
        }
    }
}
