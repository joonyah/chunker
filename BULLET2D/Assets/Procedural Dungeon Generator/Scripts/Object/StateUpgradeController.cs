﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StateUpgradeController : MonoBehaviour
{
    public static StateUpgradeController instance;
    public bool isOn = false;
    [SerializeField] private GameObject obj;
    [SerializeField] private Transform parentTransform;
    [SerializeField] private GameObject itemPrefabs;
    [SerializeField] private List<StateUpgradeItem> objList = new List<StateUpgradeItem>();
    public Dictionary<string, List<StatsData>> AllList = new Dictionary<string, List<StatsData>>();


    [Header("Content")]
    [SerializeField] private Text titleText;
    [SerializeField] private Text levelText;
    [SerializeField] private Text nowLevelContText;
    [SerializeField] private Text nextCoinText;
    [SerializeField] private Image nextCoinImg;
    [SerializeField] private Text nextLevelContText;
    [SerializeField] private Sprite[] coinSprites;
    [SerializeField] private Sprite[] itemSprites;

    [Header("POPUP")]
    [SerializeField] private GameObject popupObj;
    [SerializeField] private Text popupTitleText;
    [SerializeField] private Text popupCumsumerText;
    [SerializeField] private Image popupCoinImage;
    [SerializeField] private Button popupCancleBtn;
    [SerializeField] private Button popupApplyBtn;

    [Header("Choise")]
    [SerializeField] private GameObject[] ChoiseArray;
    [SerializeField] private StatsData[] ChoiseSelectArray;
    [SerializeField] private Text ChoiseTitleText;
    [SerializeField] private bool isChoising = false;
    [SerializeField] private int nChoiseNum = -1;
    [SerializeField] private GameObject ChoiseSavePopup;

    [Header("RESET")]
    [SerializeField] private GameObject resetObj;
    [SerializeField] private Text resetText;
    [SerializeField] private Text UnNeedText;

    [Header("Question")]
    [SerializeField] private GameObject QuestionPanel;
    [SerializeField] private Text QuestionText;
    [SerializeField] private Button YesBtn;
    [SerializeField] private Button NoBtn;
    [SerializeField] private GameObject backgroundImg;
    [Header("TEST")]
    public bool isTextBtn;

    private void Awake()
    {
        if (instance == null) instance = this;
    }
    private void Start()
    {
        ChoiseSelectArray = new StatsData[ChoiseArray.Length];
        AllList.Clear();
        StatsData[] allData = SheetManager.Instance.StatsDB.dataArray;
        for (int i = 0; i < allData.Length; i++)
        {
            if (AllList.ContainsKey(allData[i].Type))
            {
                AllList[allData[i].Type].Add(allData[i]);
            }
            else
            {
                AllList.Add(allData[i].Type, new List<StatsData>());
                AllList[allData[i].Type].Add(allData[i]);
            }
        }
    }
    private void Update()
    {
        if (isTextBtn)
        {
            isTextBtn = false;
            ShowUpgrade();
        }
        if (isOn)
        {
            if (Input.GetKeyDown(KeyCode.R) && !popupObj.activeSelf && !resetObj.activeSelf && !ChoiseSavePopup.activeSelf)
            {
                bool check = false;
                for (int i = 0; i < ChoiseSelectArray.Length; i++)
                {
                    ChoiseStateSave saveData = SaveAndLoadManager.instance.LoadChoiseState("ChoiseState" + i);
                    if (saveData._type != null)
                    {
                        check = true;
                        break;
                    }
                }
                if (check)
                {
                    int t = SaveAndLoadManager.instance.LoadItem("CHOISE_SKILL_RESET_POINT");
                    if (t == -1)
                    {
                        resetText.text = LocalizeManager.GetLocalize("stats_resetwarning") + "1";
                    }
                    else
                    {
                        resetText.text = LocalizeManager.GetLocalize("stats_resetwarning") + t;
                    }
                    resetObj.SetActive(true);
                }
            }
            if ((Input.GetKeyDown(KeyCode.Escape) || Input.GetKeyDown(KeyCode.Backspace)) && popupObj.activeSelf)
            {
                PDG.Dungeon.instance.closeCoolTime = 0.0f;
                SoundManager.instance.StartAudio(new string[1] { "ui_esc" }, VOLUME_TYPE.EFFECT);
                popupObj.SetActive(false);
            }
            else if ((Input.GetKeyDown(KeyCode.Escape) || Input.GetKeyDown(KeyCode.Backspace)) && ChoiseSavePopup.activeSelf)
            {
                PDG.Dungeon.instance.closeCoolTime = 0.0f;
                SoundManager.instance.StartAudio(new string[1] { "ui_esc" }, VOLUME_TYPE.EFFECT);
                ChoiseSavePopup.SetActive(false);
            }
            else if ((Input.GetKeyDown(KeyCode.Escape) || Input.GetKeyDown(KeyCode.Backspace)) && resetObj.activeSelf)
            {
                PDG.Dungeon.instance.closeCoolTime = 0.0f;
                SoundManager.instance.StartAudio(new string[1] { "ui_esc" }, VOLUME_TYPE.EFFECT);
                resetObj.SetActive(false);
            }
            else if ((Input.GetKeyDown(KeyCode.Escape) || Input.GetKeyDown(KeyCode.Backspace)) && !popupObj.activeSelf && !ChoiseSavePopup.activeSelf)
            {
                bool check2 = false;
                foreach (KeyValuePair<string, List<StatsData>> arr in AllList)
                {
                    int level = SaveAndLoadManager.instance.LoadItem("STAT_" + arr.Key);
                    if (level > 0)
                    {
                        check2 = true;
                        break;
                    }
                }

                bool check = false;
                for (int i = 0; i < ChoiseSelectArray.Length; i++)
                {
                    ChoiseStateSave saveData = SaveAndLoadManager.instance.LoadChoiseState("ChoiseState" + i);
                    if (saveData._type != null)
                    {
                        check = true;
                        break;
                    }
                }
                if (!check && check2) 
                {
                    YesBtn.onClick.RemoveAllListeners();
                    NoBtn.onClick.RemoveAllListeners();
                    backgroundImg.SetActive(true);
                    YesBtn.onClick.AddListener(() => {
                        QuestionPanel.SetActive(false);
                        CloseUpgrade();
                        backgroundImg.SetActive(false);
                    });
                    NoBtn.onClick.AddListener(() => {
                        QuestionPanel.SetActive(false);
                        backgroundImg.SetActive(false);
                    });

                    QuestionText.text = LocalizeManager.GetLocalize("StatUpgrade_Guide");
                    QuestionPanel.SetActive(true);
                    return;
                }
                else
                {
                    PDG.Dungeon.instance.closeCoolTime = 0.0f;
                    SoundManager.instance.StartAudio(new string[1] { "ui_esc" }, VOLUME_TYPE.EFFECT);
                    CloseUpgrade();
                }
            }
            if (Input.GetKeyDown(KeyCode.Mouse1) && isChoising)
            {
                isChoising = false;
                ChoiseTitleText.gameObject.SetActive(false);
                ChoiseTitleText.text = string.Empty;
                for (int i = 0; i < ChoiseArray.Length; i++)
                {
                    GameObject animObj = ChoiseArray[i].transform.Find("Image").gameObject;
                    animObj.SetActive(false);
                }
                nChoiseNum = -1;
            }
        }
    }
    public void ContentReset()
    {
        titleText.text = string.Empty;
        levelText.text = string.Empty;
        nowLevelContText.text = string.Empty;
        nextCoinText.text = string.Empty;
        nextLevelContText.text = string.Empty;
        nextCoinImg.color = new Color(0, 0, 0, 0);
    }
    public void ShowUpgrade()
    {
        ContentReset();
        for (int i = 0; i < objList.Count; i++)
        {
            if (objList[i] != null)
            {
                GameObject obj = objList[i].gameObject;
                objList.RemoveAt(i);
                Destroy(obj);
                i--;
            }
        }
        foreach (KeyValuePair<string, List<StatsData>> arr in AllList)
        {
            GameObject obj = Instantiate(itemPrefabs, parentTransform);
            obj.transform.SetParent(parentTransform);
            StateUpgradeItem item = obj.GetComponent<StateUpgradeItem>();
            item.StatType = arr.Key;
            item.maxLevel = arr.Value.Count;
            item._mainImage.sprite = Array.Find(itemSprites, item22 => item22.name.Equals("stats_" + arr.Key));
            item.nameText.text = LocalizeManager.GetLocalize(arr.Value[0].ID + "_name");
            item.nameText.color = Color.green;
            int level = SaveAndLoadManager.instance.LoadItem("STAT_" + arr.Key);
            if (level != -1) item.nowLevel = level;
            else item.nameText.color = Color.gray;
            //item.InitSetting();
            objList.Add(item);
        }
        obj.SetActive(true);
        GetComponent<Image>().enabled = true;
        isOn = true;
        isChoising = false;
        ChoiseSavePopup.SetActive(false);
        popupObj.SetActive(false);
        resetObj.SetActive(false);
        ChoiseTitleText.gameObject.SetActive(false);
        for (int i = 0; i < ChoiseArray.Length; i++)
        {
            GameObject animObj = ChoiseArray[i].transform.Find("Image").gameObject;
            Button btn = ChoiseArray[i].GetComponent<Button>();
            GameObject obj = ChoiseArray[i];
            btn.onClick.RemoveAllListeners();
            btn.onClick.AddListener(() => { ChoiseSkill(obj); });
            animObj.SetActive(false);

            ChoiseStateSave saveData = SaveAndLoadManager.instance.LoadChoiseState("ChoiseState" + i);
            if (saveData._type != null)
            {
                StatsData sData = AllList[saveData._type].Find(item => item.Steps == saveData._level);

                ChoiseSelectArray[i] = sData;
                Image img = ChoiseArray[i].transform.Find("Icon").GetComponent<Image>();
                img.sprite = Array.Find(itemSprites, item22 => item22.name.Equals("stats_" + saveData._type));
                img.color = Color.white;
            }
            else
            {
                ChoiseSelectArray[i] = null;
                Image img = ChoiseArray[i].transform.Find("Icon").GetComponent<Image>();
                img.color = new Color(0, 0, 0, 0);
            }
        }
        Time.timeScale = 0;
    }
    public void CloseUpgrade()
    {
        if (popupObj.activeSelf) popupObj.SetActive(false);
        if (ChoiseSavePopup.activeSelf) ChoiseSavePopup.SetActive(false);

        GetComponent<Image>().enabled = false;
        obj.SetActive(false);
        isOn = false;
        PDG.Dungeon.instance.closeCoolTime = 0.0f;
        Time.timeScale = 1;
    }
    public void ButtonClickEvnet(StateUpgradeItem _item)
    {
        SoundManager.instance.StartAudio(new string[1] { "Click_0" }, VOLUME_TYPE.EFFECT);
        for (int i = 0; i < objList.Count; i++)
        {
            if (objList[i] == _item)
            {
                objList[i].isSelected = true;
                objList[i].selectedObj.SetActive(true);
            }
            else
            {
                objList[i].isSelected = false;
                objList[i].selectedObj.SetActive(false);
            }
        }
        StatsData sData = AllList[_item.StatType].Find(item => item.Steps == _item.nowLevel);
        StatsData sDataNext = AllList[_item.StatType].Find(item => item.Steps == _item.nowLevel + 1);
        levelText.text = _item.nowLevel + "/" + _item.maxLevel;
        if (sData == null)
        {
            nowLevelContText.text = string.Empty;
        }
        else
        {
            titleText.text = LocalizeManager.GetLocalize(sData.ID + "_name");
            nowLevelContText.text = LocalizeManager.GetLocalize(sData.ID + "_cont");
        }
        if (sDataNext == null)
        {
            nextCoinText.text = LocalizeManager.GetLocalize("stats_maxlevelwarning");
            nextLevelContText.text = string.Empty;
            nextCoinImg.color = new Color(0, 0, 0, 0);
        }
        else
        {
            string colorText = "<color=#4B4B4B>";
            if (Inventorys.Instance.glowindskull < sDataNext.Count) colorText = "<color=#FF0000>";

            titleText.text = LocalizeManager.GetLocalize(sDataNext.ID + "_name");
            nextCoinText.text = "> " + LocalizeManager.GetLocalize("stats_nextlevelup") + " : " + (LocalizeManager.lang == LANGUAGE_SHIFTRICK.ENG ? "    " : "") + "## " + colorText + sDataNext.Count + "</color>";
            nextCoinImg.sprite = Array.Find(coinSprites, item => item.name.Equals(sDataNext.Consume));
            nextCoinImg.color = Color.white;
            nextLevelContText.text = LocalizeManager.GetLocalize(sDataNext.ID + "_cont");
        }

        if (isChoising)
        {
            if (_item.nowLevel == 0)
            {
                ChoiseTitleText.text = LocalizeManager.GetLocalize("stat_upgrade_unactive");
            }
            else
            {
                for (int i = 0; i < ChoiseSelectArray.Length; i++)
                {
                    if (ChoiseSelectArray[i] != null)
                    {
                        if (ChoiseSelectArray[i].Equals(sData)) 
                        {
                            Debug.Log(Array.Find(ChoiseSelectArray, item => item.Equals(sData)));
                            ChoiseTitleText.text = LocalizeManager.GetLocalize("stat_upgrade_selected");
                            return;
                        }
                    }
                }

                ChoiseTitleText.text = LocalizeManager.GetLocalize("stat_upgrade_select");
                ChoiseSelectArray[nChoiseNum] = sData;
                Image img = ChoiseArray[nChoiseNum].transform.Find("Icon").GetComponent<Image>();
                img.sprite = Array.Find(itemSprites, item22 => item22.name.Equals("stats_" + _item.StatType));
                img.color = Color.white;
                Text text = ChoiseArray[nChoiseNum].transform.Find("Text").GetComponent<Text>();
                text.gameObject.SetActive(false);
            }
        }
    }
    public void ButtonClickEvnet(StateUpgradeItem _item, int _level)
    {
        SoundManager.instance.StartAudio(new string[1] { "Click_0" }, VOLUME_TYPE.EFFECT);

        StatsData sData = AllList[_item.StatType].Find(item => item.Steps == _level);
        StatsData sDataNext = AllList[_item.StatType].Find(item => item.Steps == _level + 1);

        levelText.text = _level + "/" + _item.maxLevel;
        if (sData == null)
        {
            nowLevelContText.text = string.Empty;
        }
        else
        {
            titleText.text = LocalizeManager.GetLocalize(sData.ID + "_name");
            nowLevelContText.text = LocalizeManager.GetLocalize(sData.ID + "_cont");
        }
        if (sDataNext == null)
        {
            nextCoinText.text = LocalizeManager.GetLocalize("stats_maxlevelwarning");
            nextLevelContText.text = string.Empty;
            nextCoinImg.color = new Color(0, 0, 0, 0);
        }
        else
        {
            string colorText = "<color=#4B4B4B>";
            if (Inventorys.Instance.glowindskull < sDataNext.Count) colorText = "<color=#FF0000>";

            titleText.text = LocalizeManager.GetLocalize(sDataNext.ID + "_name");
            nextCoinText.text = "> " + LocalizeManager.GetLocalize("stats_nextlevelup") + " : " + (LocalizeManager.lang == LANGUAGE_SHIFTRICK.ENG ? "    " : "") + "## " + colorText + sDataNext.Count + "</color>";
            nextCoinImg.sprite = Array.Find(coinSprites, item => item.name.Equals(sDataNext.Consume));
            nextCoinImg.color = Color.white;
            nextLevelContText.text = LocalizeManager.GetLocalize(sDataNext.ID + "_cont");
        }

    }
    public void PopupOepn(StateUpgradeItem _item)
    {
        if (isChoising) return;

        SoundManager.instance.StartAudio(new string[1] { "Click_0" }, VOLUME_TYPE.EFFECT);
        StatsData sData = AllList[_item.StatType].Find(item => item.Steps == _item.nowLevel + 1);

        string colorText = "<color=#FFDA3B>";
        if (Inventorys.Instance.glowindskull < sData.Count) colorText = "<color=#FF0000>";

        popupTitleText.text = LocalizeManager.GetLocalize(sData.ID + "_name") + " " + (_item.nowLevel + 1) + LocalizeManager.GetLocalize("stats_levelup");
        popupCumsumerText.text = colorText + sData.Count + "</color>" + LocalizeManager.GetLocalize("stats_usecoin");
        popupCoinImage.sprite = Array.Find(coinSprites, item => item.name.Equals(sData.Consume));

        popupCancleBtn.onClick.RemoveAllListeners();
        popupCancleBtn.onClick.AddListener(() => { PopupClose(); });
        popupApplyBtn.onClick.RemoveAllListeners();
        popupApplyBtn.onClick.AddListener(() => { ApplyItemSave(_item); });
        popupObj.SetActive(true);
    }
    private void PopupClose()
    {
        SoundManager.instance.StartAudio(new string[1] { "Click_0" }, VOLUME_TYPE.EFFECT);
        popupTitleText.text = string.Empty;
        popupCumsumerText.text = string.Empty;
        UnNeedText.gameObject.SetActive(false);
        popupObj.SetActive(false);
    }
    private void ApplyItemSave(StateUpgradeItem _item)
    {
        StatsData sData = AllList[_item.StatType].Find(item => item.Steps == _item.nowLevel + 1);
        if (sData.Count <= Inventorys.Instance.glowindskull)
        {
            SoundManager.instance.StartAudio(new string[1] { "UI/statupgrade" }, VOLUME_TYPE.EFFECT);
            Inventorys.Instance.glowindskull -= sData.Count;
            SaveAndLoadManager.instance.SaveItem(new ItemSave
            {
                _id = sData.Consume,
                _nCount = Inventorys.Instance.glowindskull
            }, true);
            SaveAndLoadManager.instance.SaveItem(new ItemSave { _id = "STAT_" + _item.StatType, _nCount = _item.nowLevel + 1 }, true);
            _item.nowLevel++;


            StatsData sData2 = AllList[_item.StatType].Find(item => item.Steps == _item.nowLevel);
            StatsData sData2Next = AllList[_item.StatType].Find(item => item.Steps == _item.nowLevel + 1);

            levelText.text = _item.nowLevel + "/" + _item.maxLevel;
            if (sData2 == null)
            {
                nowLevelContText.text = string.Empty;
            }
            else
            {
                titleText.text = LocalizeManager.GetLocalize(sData2.ID + "_name");
                nowLevelContText.text = LocalizeManager.GetLocalize(sData2.ID + "_cont");
            }
            if (sData2Next == null)
            {
                nextCoinText.text = LocalizeManager.GetLocalize("stats_maxlevelwarning");
                nextLevelContText.text = string.Empty;
                nextCoinImg.color = new Color(0, 0, 0, 0);
            }
            else
            {
                titleText.text = LocalizeManager.GetLocalize(sData2Next.ID + "_name");
                nextCoinText.text = "> " + LocalizeManager.GetLocalize("stats_nextlevelup") + " : " + (LocalizeManager.lang == LANGUAGE_SHIFTRICK.ENG ? "    " : "") + "## " + sData2Next.Count;
                nextCoinImg.sprite = Array.Find(coinSprites, item => item.name.Equals(sData2Next.Consume));
                nextCoinImg.color = Color.white;
                nextLevelContText.text = LocalizeManager.GetLocalize(sData2Next.ID + "_cont");
            }

            PopupClose();
        }
        else
        {
            SoundManager.instance.StartAudio(new string[1] { "UI/skullunneed" }, VOLUME_TYPE.EFFECT);
            UnNeedText.gameObject.SetActive(true);
        }
    }
    public void ChoiseSkill(GameObject _obj)
    {
        SoundManager.instance.StartAudio(new string[1] { "Click_0" }, VOLUME_TYPE.EFFECT);
        int checkNum = -1;
        for (int i = 0; i < ChoiseArray.Length; i++)
        {
            if (ChoiseArray[i] == _obj)
            {
                checkNum = i;
                break;
            }
        }
        if (ChoiseSelectArray[checkNum] != null)
        {
            ChoiseStateSave saveData = SaveAndLoadManager.instance.LoadChoiseState("ChoiseState" + checkNum);
            if (saveData._type != null)
            {
                return;
            }
        }
        isChoising = true;
        ChoiseTitleText.gameObject.SetActive(true);
        ChoiseTitleText.text = LocalizeManager.GetLocalize("stat_upgrade_select");
        for (int i = 0; i < ChoiseArray.Length; i++)
        {
            GameObject animObj = ChoiseArray[i].transform.Find("Image").gameObject;
            //Button btn = ChoiseArray[i].GetComponent<Button>();
            GameObject obj = ChoiseArray[i];
            if (ChoiseArray[i] == _obj)
            {
                //btn.onClick.RemoveAllListeners();
                //btn.onClick.AddListener(() => { ChoiseUnSelected(obj); });
                animObj.SetActive(true);
                nChoiseNum = i;
            }
            else
            {
                //btn.onClick.RemoveAllListeners();
                //btn.onClick.AddListener(() => { ChoiseSkill(obj); });
                animObj.SetActive(false);
            }
        }
    }
    public void ChoiseUnSelected(GameObject _obj)
    {
        SoundManager.instance.StartAudio(new string[1] { "Click_0" }, VOLUME_TYPE.EFFECT);
        for (int i = 0; i < ChoiseArray.Length; i++)
        {
            GameObject animObj = ChoiseArray[i].transform.Find("Image").gameObject;
            if (ChoiseArray[i] == _obj)
            {
                Button btn = ChoiseArray[i].GetComponent<Button>();
                GameObject obj = ChoiseArray[i];
                btn.onClick.RemoveAllListeners();
                btn.onClick.AddListener(() => { ChoiseSkill(obj); });

                ChoiseSelectArray[i] = null;
                Image img = ChoiseArray[i].transform.Find("Icon").GetComponent<Image>();
                img.color = new Color(0, 0, 0, 0);
                Text text = ChoiseArray[i].transform.Find("Text").GetComponent<Text>();
                text.gameObject.SetActive(true);
            }
        }
    }
    public void SavePopup()
    {
        SoundManager.instance.StartAudio(new string[1] { "Click_0" }, VOLUME_TYPE.EFFECT);
        ChoiseSavePopup.SetActive(true);
    }
    public void ChoiseSkillSave()
    {
        SoundManager.instance.StartAudio(new string[1] { "Click_0" }, VOLUME_TYPE.EFFECT);
        for (int i = 0; i < ChoiseSelectArray.Length; i++)
        {
            SaveAndLoadManager.instance.SaveChoiseState(new ChoiseStateSave
            {
                _id = "ChoiseState" + i,
                _level = (ChoiseSelectArray[i] == null ? 0 : ChoiseSelectArray[i].Steps),
                _type = (ChoiseSelectArray[i]?.Type)
            });
        }
        ShowUpgrade();
    }
    public void ChoiseSkillCancle()
    {
        SoundManager.instance.StartAudio(new string[1] { "Click_0" }, VOLUME_TYPE.EFFECT);
        ChoiseSavePopup.SetActive(false);
    }
    public void ResetCancle()
    {
        UnNeedText.gameObject.SetActive(false);
        resetObj.SetActive(false);
    }
    public void ResetApply()
    {
        int t = SaveAndLoadManager.instance.LoadItem("CHOISE_SKILL_RESET_POINT");
        if (t == -1) t = 1;
        if(t <= Inventorys.Instance.glowindskull)
        {
            t += 3;
            if (t > 50) t = 50;

            SaveAndLoadManager.instance.SaveItem(new ItemSave
            {
                _id = "CHOISE_SKILL_RESET_POINT",
                _nCount = t
            }, true);
            Inventorys.Instance.glowindskull -= t;
            SaveAndLoadManager.instance.SaveItem(new ItemSave
            {
                _id = "glowing_skull",
                _nCount = Inventorys.Instance.glowindskull
            }, true);
            for (int i = 0; i < ChoiseSelectArray.Length; i++)
            {
                ChoiseSelectArray[i] = null;
                SaveAndLoadManager.instance.SaveChoiseState(new ChoiseStateSave
                {
                    _id = "ChoiseState" + i,
                    _level = (ChoiseSelectArray[i] == null ? 0 : ChoiseSelectArray[i].Steps),
                    _type = (ChoiseSelectArray[i]?.Type)
                });
            }
            resetObj.SetActive(false);
            UnNeedText.gameObject.SetActive(false);
            ShowUpgrade();
        }
        else
        {
            UnNeedText.gameObject.SetActive(true);
        }
    }
}
