﻿using PDG;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PortalControl : MonoBehaviour
{
    public bool isOpen = false;
    GameObject pKey = null;
    private bool isSelected = false;
    [SerializeField] private bool isReady = false;
    [SerializeField] private int targetNum = 0;
    bool allClose = true;
    [SerializeField] private Animator animator;
    [SerializeField] private Sprite noneSprite;
    [SerializeField] private Sprite defaultSprite;
    [SerializeField] private Sprite outlineSprite;
    public GameObject fadeObj;
    private float fadeTime = 0.1f;
    public Material defaultMat;
    public int orderLayer;

    private void Awake()
    {
        animator = GetComponent<Animator>();
        fadeObj = GameObject.Find("Canvas").transform.Find("fadeObj").gameObject;
        pKey = GameObject.Find("Canvas").transform.Find("PressKey").gameObject;
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (allClose)
        {
            for (int i = 0; i < Dungeon.instance.TeleportList.Count; i++)
            {
                PortalControl pCon = Dungeon.instance.TeleportList[i].GetComponent<PortalControl>();
                if (pCon == this) continue;
                if (pCon.isOpen)
                {
                    allClose = false;
                    targetNum = i;
                    break;
                }
            }
            return;
        }
        if (isReady)
        {
            GameObject lineObj;
            if (Dungeon.instance.teleportLineObj == null)
            {
                lineObj = new GameObject();
                lineObj.layer = 18;
                lineObj.AddComponent<LineRenderer>();
                Dungeon.instance.teleportLineObj = lineObj;
            }
            else
            {
                lineObj = Dungeon.instance.teleportLineObj;
            }
            Dungeon.instance.teleportLineObj.SetActive(true);
            LineRenderer render = lineObj.GetComponent<LineRenderer>();
            render.sortingOrder = orderLayer;
            render.material = defaultMat;
            render.SetPosition(0, transform.position);
            render.SetPosition(1, Dungeon.instance.TeleportList[targetNum].transform.position);

            if (Input.GetKeyDown(KeyCode.W))
            {
                bool check = false;
                for (int i = targetNum; i < Dungeon.instance.TeleportList.Count; i++)
                {
                    PortalControl pCon = Dungeon.instance.TeleportList[i].GetComponent<PortalControl>();
                    if (pCon == this || i == targetNum) continue;
                    if (pCon.isOpen)
                    {
                        check = true;
                        targetNum = i;
                        break;
                    }
                }
                if (!check)
                {
                    for (int i = 0; i < Dungeon.instance.TeleportList.Count; i++)
                    {
                        PortalControl pCon = Dungeon.instance.TeleportList[i].GetComponent<PortalControl>();
                        if (pCon == this || i == targetNum) continue;
                        if (pCon.isOpen)
                        {
                            targetNum = i;
                            break;
                        }
                    }
                }
            }
            if (Input.GetKeyDown(KeyCode.S))
            {
                bool check = false;
                for (int i = targetNum; i >= 0; i--)
                {
                    PortalControl pCon = Dungeon.instance.TeleportList[i].GetComponent<PortalControl>();
                    if (pCon == this || i == targetNum) continue;
                    if (pCon.isOpen)
                    {
                        check = true;
                        targetNum = i;
                        break;
                    }
                }
                if (!check)
                {
                    for (int i = Dungeon.instance.TeleportList.Count - 1; i >= 0; i++)
                    {
                        PortalControl pCon = Dungeon.instance.TeleportList[i].GetComponent<PortalControl>();
                        if (pCon == this || i == targetNum) continue;
                        if (pCon.isOpen)
                        {
                            targetNum = i;
                            break;
                        }
                    }
                }
            }
            if (Input.GetKeyDown(KeyCode.F))
            {
                Dungeon.instance.isSelect = false;
                pKey.SetActive(false);
                Dungeon.instance.WorldMapOnOff();
                StartCoroutine(Warp());
            }
            if ((Input.GetKeyDown(KeyCode.Escape) || Input.GetKeyDown(KeyCode.Backspace)))
            {
                Dungeon.instance.WorldMapOnOff();
                Dungeon.instance.teleportLineObj.SetActive(false);
                isReady = false;
                PDG.Dungeon.instance.closeCoolTime = 0.0f;
            }
            return;
        }

        RaycastHit2D hit = Physics2D.CircleCast(transform.position, 1.5f, Vector2.zero, 1, 1 << 9);
        if(hit && !animator.GetBool("Trigger"))
        {
            Dungeon.instance.isSelect = true;
            pKey.SetActive(true);
            pKey.transform.position = UnityEngine.Camera.main.WorldToScreenPoint(transform.position);
            GetComponent<SpriteRenderer>().sprite = outlineSprite;
            if (Input.GetKeyDown(KeyCode.F) && !isReady)
            {
                Dungeon.instance.WorldMapOnOff(true);
                isReady = true;
            }
        }
    }

    IEnumerator Warp()
    {
        yield return new WaitForSeconds(0.5f);
        Dungeon.instance.HeroObj.SetActive(false);
        animator.enabled = true;
        animator.SetTrigger("out");
        animator.SetBool("Trigger", true);
        yield return new WaitForSeconds(1.0f);
        isReady = false;
        Dungeon.instance.teleportLineObj.SetActive(false);
        Dungeon.instance.TeleportList[targetNum].GetComponent<SpriteRenderer>().sprite = noneSprite;
        Dungeon.instance.HeroObj.transform.position = Dungeon.instance.TeleportList[targetNum].transform.position;
        for (int i = 0; i < Dungeon.instance.RoomInfo.Count; i++)
        {
            if (Dungeon.instance.RoomInfo[i].roomID.Equals(Dungeon.instance.TeleportList[targetNum].name))
            {
                Dungeon.instance.RoomInsert(Dungeon.instance.RoomInfo[i].roomNum);
                break;
            }
        }
        yield return StartCoroutine(FadeInOut());
        yield return new WaitForSeconds(1.0f);
        animator.SetBool("Trigger", false);
        animator.enabled = false;
        animator.GetComponent<SpriteRenderer>().sprite = defaultSprite;
        Dungeon.instance.TeleportList[targetNum].GetComponent<PortalControl>().animator.enabled = true;
        Dungeon.instance.TeleportList[targetNum].GetComponent<PortalControl>().animator.SetTrigger("in");
        Dungeon.instance.TeleportList[targetNum].GetComponent<PortalControl>().animator.SetBool("Trigger", true);
        yield return new WaitForSeconds(1.0f);
        Dungeon.instance.HeroObj.SetActive(true);
        yield return new WaitForSeconds(0.5f);
        Dungeon.instance.TeleportList[targetNum].GetComponent<PortalControl>().animator.SetBool("Trigger", false);
        Dungeon.instance.TeleportList[targetNum].GetComponent<PortalControl>().animator.enabled = false;
        Dungeon.instance.TeleportList[targetNum].GetComponent<SpriteRenderer>().sprite = defaultSprite;

    }
    public IEnumerator FadeInOut()
    {
        fadeObj.SetActive(true);
        float a = 0.0f;
        while (a <= 1.0f)
        {
            fadeObj.GetComponent<Image>().color = new Color(0, 0, 0, a);
            a += Time.deltaTime / fadeTime;
            yield return new WaitForSeconds(Time.deltaTime);
        }
        fadeObj.GetComponent<Image>().color = new Color(0, 0, 0, 1);
        yield return new WaitForSeconds(1.0f);
        while (a >= 0.0f)
        {
            fadeObj.GetComponent<Image>().color = new Color(0, 0, 0, a);
            a -= Time.deltaTime / fadeTime;
            yield return new WaitForSeconds(Time.deltaTime);
        }
        fadeObj.GetComponent<Image>().color = new Color(0, 0, 0, 0);
        fadeObj.SetActive(false);
    }
}
