﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthTower : MonoBehaviour
{
    [SerializeField] private Animator animator;
    [SerializeField] private GameObject PressKeyHealth;
    [SerializeField] private float pushTime = 0.0f;
    [SerializeField] private bool isOpen = false;
    [SerializeField] private GameObject effectObj;
    long pushCheck = 0;
    long pushCheck2 = 0;

    bool isPush = false;
    bool ispKeyOn = false;

    public bool isTutorial = false;

    public GameObject minimapObj;
    [SerializeField] private GameObject collisionObj;
    private void Awake()
    {
        animator = GetComponent<Animator>();
        PressKeyHealth = GameObject.Find("Canvas").transform.Find("PressKeyHealth").gameObject;
        collisionObj.SetActive(true);
    }

    private void Update()
    {
        if (pushTime > 1.0f)
        {
            animator.SetBool("Off", true);
            effectObj.SetActive(false);
            isOpen = true;
            pushTime = 0.0f;
            StartCoroutine(SpakeCoroutine());
            if (PDG.Player.instance != null) PDG.Player.instance.HpRecovery(1);
            if(isTutorial)
            {
                GameObject.Find("Canvas").transform.Find("TutorialText").gameObject.SetActive(false);
                Dialogue.Instance.CommnetSetting("tutorial_first_map_npc_2", 0, null);
                gameObject.SetActive(false);
            }
            if (PDG.Dungeon.instance.newObjectSelectedObj == gameObject)
            {
                PDG.Dungeon.instance.newObjectSelectedObj = null;
                PDG.Dungeon.instance.newObjectSelected = false;
            }
            StateGroup.instance.nHealthTowerUse++;
            SoundManager.instance.StartAudio(new string[1] { "Object/healthtower_end" }, VOLUME_TYPE.AMBIENT);
            PDG.Player.instance.transform.Find("HealMiniGreen").gameObject.SetActive(true);
            PDG.Player.instance.transform.Find("HealMiniGreen").GetComponent<ParticleSystem>().Play(true);
            minimapObj.SetActive(false);
            collisionObj.SetActive(false);
        }

        RaycastHit2D hit = Physics2D.BoxCast(transform.position, new Vector2(2, 2.5f), 0, Vector2.zero, 0, 1 << 9);
        if (hit && !isOpen && (!PDG.Dungeon.instance.newObjectSelected || PDG.Dungeon.instance.newObjectSelectedObj == gameObject)) 
        {
            if (!PressKeyHealth.activeSelf)
            {
                ispKeyOn = true;
                PressKeyHealth.SetActive(true);
                if (PressKeyHealth.GetComponent<SpriteRenderer>()) PressKeyHealth.GetComponent<SpriteRenderer>().sprite = PDG.Dungeon.instance.isJoystrick ? PDG.Dungeon.GetJoyStrickKeySprite(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_ACTIVE]) : PDG.Dungeon.GetJoyStrickKeySprite(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ACTIVE]);
                else if (PressKeyHealth.GetComponent<Image>()) PressKeyHealth.GetComponent<Image>().sprite = PDG.Dungeon.instance.isJoystrick ? PDG.Dungeon.GetJoyStrickKeySprite(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_ACTIVE]) : PDG.Dungeon.GetJoyStrickKeySprite(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ACTIVE]);

                if (SaveAndLoadManager.instance.LoadItem("healthTowerGuide") == -1)
                {
                    StartCoroutine(ItemGuide("healthTowerGuide"));
                }
            }
            Vector3 pos = transform.position;
            pos.y += 1;
            PressKeyHealth.transform.position = Camera.main.WorldToScreenPoint(pos);
            if ((PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ACTIVE], false) || PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_ACTIVE], false))) 
            {
                if (!isPush)
                {
                    PDG.Dungeon.instance.newObjectSelectedObj = gameObject;
                    PDG.Dungeon.instance.newObjectSelected = true;
                    SoundManager.instance.StartAudio(new string[1] { "Object/healthtower" }, VOLUME_TYPE.AMBIENT);
                }
                isPush = true;
                pushTime += Time.deltaTime;
                animator.SetBool("On", true);
                PlayerCombat.ps = PlayerState.Health;
                pushCheck++;
                CameraShaker._instance.StartShake(0.15f, 0.05f, 0.1f);
            }
        }
        else
        {
            if (PDG.Dungeon.instance.newObjectSelectedObj == gameObject)
            {
                PDG.Dungeon.instance.newObjectSelectedObj = null;
                PDG.Dungeon.instance.newObjectSelected = false;
            }
            if (PressKeyHealth.activeSelf && ispKeyOn)
            {
                PressKeyHealth.SetActive(false);
                ispKeyOn = false;
            }
        }


        if (pushCheck == pushCheck2 && isPush)
        {
            isPush = false;
            pushTime = 0.0f;
            pushCheck = pushCheck2 = 0;
            animator.SetBool("On", false);
            PlayerCombat.ps = PlayerState.Idle;
            SoundManager.instance.StopAudio(new string[1] { "Object/healthtower" });
            if (PDG.Dungeon.instance.newObjectSelectedObj == gameObject)
            {
                PDG.Dungeon.instance.newObjectSelectedObj = null;
                PDG.Dungeon.instance.newObjectSelected = false;
            }
        }
        pushCheck2 = pushCheck;
    }

    IEnumerator ItemGuide(string _id)
    {
        Inventorys.Instance.ItemGuideObj.transform.Find("Text").GetComponent<SetLanguage>().InitSetting(_id);
        yield return new WaitForSeconds(0.2f);
        Time.timeScale = 0;
        SoundManager.instance.StartAudio(new string[1] { "pressanykey" }, VOLUME_TYPE.EFFECT);
        Inventorys.Instance.ItemGuideObj.SetActive(true);
        Inventorys.Instance.isGuid = true;
    }

    IEnumerator SpakeCoroutine()
    {
        float a = 0.0f;
        UnityEngine.UI.Image img = GameObject.Find("Canvas").transform.Find("Spake").GetComponent<UnityEngine.UI.Image>();
        while(true)
        {
            img.color = new Color(1, 1, 1, a);
            a += Time.deltaTime * 10; 
            if (a >= 1.0f) break;
            yield return null;
        }
        img.color = Color.white;
        while (true)
        {
            img.color = new Color(1, 1, 1, a);
            a -= Time.deltaTime * 10;
            if (a <= 0.0f) break;
            yield return null;
        }
        img.color = new Color(1, 1, 1, 0);
    }
}
