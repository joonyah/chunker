﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Shop : MonoBehaviour
{
    [SerializeField] private Transform[] ItemSettingPositions;
    public GameObject NPCObj;
    public bool TestShop = false;

    public List<string> itemList = new List<string>();
    private void Update()
    {
        if(TestShop)
        {
            ShopSetting("d01_merchantstage_01");
            TestShop = false;
        }
    }
    public void ShopSetting(string nowRoomId)
    {
        MerchantData[] mDatas = System.Array.FindAll(SheetManager.Instance.MerchantDB.dataArray, item => item.ID.Equals(nowRoomId));
        for (int i = 0; i < mDatas.Length; i++)
        {
            string[] firstCheckString;
            string selectedItemID = string.Empty;
            bool first = false;
            if (!mDatas[i].Itemcategories1.Equals(string.Empty)) 
            {
                if (!mDatas[i].Itemcategories2.Equals(string.Empty)) 
                {
                    if (!mDatas[i].Itemcategories3.Equals(string.Empty)) firstCheckString = new string[3];
                    else firstCheckString = new string[2];
                }
                else firstCheckString = new string[1];
            }
            else  firstCheckString = null;

            // 뒤에꺼 체크
            if (firstCheckString == null)
            {
                first = false;
                if (mDatas[i].Itemgrade.Length > 0)
                {
                    string grade = mDatas[i].Itemgrade[GetBoxGrade(mDatas[i].Gradeprobability)];

                    List<ItemsData> iDatas = System.Array.FindAll(SheetManager.Instance.ItemsDB.dataArray, item => item.Grade.ToUpper().Equals(grade.ToUpper())).ToList();
                    for (int j = 0; j < iDatas.Count; j++)
                    {
                        Sprite s = Resources.Load<Sprite>("Item/" + iDatas[j].Type1 + "/" + iDatas[j].Type2 + "/" + iDatas[j].ID);
                        if (s == null || !Inventorys.Instance.UnlockAndMaxCountCheck(iDatas[j].ID) || itemList.FindIndex(item => item.Equals(iDatas[j].ID)) != -1)  
                        {
                            iDatas.RemoveAt(j);
                            j--;
                            continue;
                        }
                    }
                    if (iDatas.Count > 0)
                    {
                        selectedItemID = iDatas[Random.Range(0, iDatas.Count)].ID;
                    }
                }
            }
            else
            {
                first = true;
                if (firstCheckString.Length == 1) firstCheckString[0] = mDatas[i].Itemcategories1;
                else if (firstCheckString.Length == 2)
                {
                    firstCheckString[0] = mDatas[i].Itemcategories1;
                    firstCheckString[1] = mDatas[i].Itemcategories2;
                }
                else
                {
                    firstCheckString[0] = mDatas[i].Itemcategories1;
                    firstCheckString[1] = mDatas[i].Itemcategories2;
                    firstCheckString[2] = mDatas[i].Itemcategories3;
                }
                selectedItemID = firstCheckString[Random.Range(0, firstCheckString.Length)];
            }
            if (selectedItemID == null) continue;

            if(!selectedItemID.Equals(string.Empty))
            {
                GameObject obj = ObjManager.Call().GetObject("Item");
                if (obj != null)
                {
                    Item _item = obj.GetComponent<Item>();
                    if (first)
                    {
                        List<ItemsData> iDatas = System.Array.FindAll(SheetManager.Instance.ItemsDB.dataArray, item => item.Type2.ToUpper().Equals(selectedItemID.ToUpper())).ToList();
                        for (int j = 0; j < iDatas.Count; j++)
                        {
                            Sprite s = Resources.Load<Sprite>("Item/" + iDatas[j].Type1 + "/" + iDatas[j].Type2 + "/" + iDatas[j].ID);
                            if (s == null || !Inventorys.Instance.UnlockAndMaxCountCheck(iDatas[j].ID) || itemList.FindIndex(item => item.Equals(iDatas[j].ID)) != -1)
                            {
                                iDatas.RemoveAt(j);
                                j--;
                                continue;
                            }
                        }
                        if (iDatas.Count > 0)
                        {
                            selectedItemID = iDatas[Random.Range(0, iDatas.Count)].ID;
                        }
                    }
                    if (selectedItemID.Equals(string.Empty)) continue;
                    _item._Item = new ItemInfo
                    {
                        Data = Inventorys.Instance.CopyItem(System.Array.Find(SheetManager.Instance.ItemsDB.dataArray, item => item.ID.Equals(selectedItemID)))
                    };
                    _item.dir = Vector2.zero;
                    _item.itemCount = mDatas[i].Itemcount;
                    _item.rPow = 0;
                    obj.transform.position = ItemSettingPositions[i].position;
                    obj.SetActive(true);
                    _item.isShop = true;
                    _item.NPCObj = NPCObj;
                    itemList.Add(selectedItemID);
                    Inventorys.Instance.shopOpenItems.Add(new ItemInfo() { Data = Inventorys.Instance.CopyItem(_item._Item.Data) });
                }
            }
        }
    }
    private int GetBoxGrade(float[] percent)
    {
        int[] per = new int[percent.Length];
        for (int i = 0; i < percent.Length; i++)
        {
            per[i] = Mathf.FloorToInt(percent[i] * 1000);
        }

        int[] perTotal = new int[1000];
        int t = 0;
        for (int i = 0; i < per.Length; i++)
        {
            for (int j = 0; j < per[i]; j++)
            {
                perTotal[t] = i;
                t++;
            }
        }

        return perTotal[Random.Range(0, 1000)];
    }
}
