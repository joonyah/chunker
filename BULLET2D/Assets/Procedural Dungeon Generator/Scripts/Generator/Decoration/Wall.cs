﻿using UnityEngine;

namespace PDG
{
    public class Wall : MonoBehaviour
    {
        [SerializeField] private Sprite[] holes;
        [SerializeField] private Sprite[] banners;
        private SpriteRenderer sr;

        private Dungeon dungeon;
        private Matrix matrix;
        [SerializeField] private GameObject minimap;
        [SerializeField] private GameObject IceStone;
        private bool isGround = false;
        [SerializeField] private GameObject[] opnerObjs;
        [SerializeField] private GameObject[] openPartsObjs;

        [SerializeField] private GameObject MercenaryDoorObj;

        SpriteRenderer sr2;
        private void Start()
        {

            string nowDungeon = Dungeon.instance.StageName;
            holes = Resources.LoadAll<Sprite>("DungeonSprite/Hole/" + nowDungeon);
            banners = Resources.LoadAll<Sprite>("DungeonSprite/Banners/" + nowDungeon);


            sr = GetComponent<SpriteRenderer>();
            sr.sprite = Resources.Load<Sprite>("DungeonSprite/wall_mid/" + nowDungeon + "/wall_mid");
            dungeon = Dungeon.instance;
            matrix = dungeon.GetComponent<Matrix>();
            int x = (int)transform.position.x;
            int y = (int)transform.position.y;

            Sprite tmpSprite;
            if (Ground && Random.Range(0, 10) == 0 && banners != null && banners.Length > 0)
            {
                sr.sprite = banners[Random.Range(0, banners.Length)];
            }
            else if (Random.Range(0, 8) == 0 && holes != null && holes.Length > 0) 
            {
                tmpSprite = holes[Random.Range(0, holes.Length)];

                if ((tmpSprite.name != "wall_goo") || (Ground && tmpSprite.name == "wall_goo"))
                {
                    sr.sprite = tmpSprite;
                }
            }
            isGround = Ground;
            if (!Ground)
            {
                minimap.SetActive(false);
                GetComponent<BoxCollider2D>().enabled = true;
                GetComponent<SpriteRenderer>().sortingLayerName = "Background";

                GameObject obj = new GameObject();
                obj.transform.SetParent(transform);
                sr2 = obj.AddComponent<SpriteRenderer>();
                sr2.sortingLayerName = "Default";
                sr2.sortingOrder = -5;
                Sprite[] decoration = Resources.LoadAll<Sprite>("DungeonSprite/Floor/" + nowDungeon);
                sr2.sprite = decoration[Random.Range(0, decoration.Length)];
                obj.transform.localPosition = Vector3.zero;

                if (nowDungeon.Equals("Dungeon_0")) sr2.color = Color.white;
                else if (nowDungeon.Equals("Dungeon_1")) sr2.color = new Color32(181, 181, 181, 255);

            }
            else
            {
                GetComponent<BoxCollider2D>().enabled = false;
                GetComponent<SpriteRenderer>().sortingLayerName = "Default";
                if (CheckObj(x, y, 2))
                {
                    if (!Dungeon.instance.isOpnerSetting && !Dungeon.instance.isSaveMap)
                    {
                        Dungeon.instance.isOpnerSetting = true;
                        int n = Dungeon.instance.warpType;
                        GameObject[] objs = System.Array.FindAll<GameObject>(opnerObjs, item => item.name.Contains("opner_" + n));
                        Dungeon.instance.board.map[x, y] = IceStone;
                        GameObject obj = Instantiate(objs[Random.Range(0, objs.Length)], new Vector3(x, y, 0), Quaternion.identity);
                        obj.transform.SetParent(transform);
                    }
                    else if (!Dungeon.instance.isOpnePartSetting && !Dungeon.instance.isSaveMap)
                    {
                        Dungeon.instance.isOpnePartSetting = true;
                        int n = Dungeon.instance.warpType;
                        GameObject[] objs = System.Array.FindAll<GameObject>(openPartsObjs, item => item.name.Contains("outer_" + n));
                        Dungeon.instance.board.map[x, y] = IceStone;
                        GameObject obj = Instantiate(objs[Random.Range(0, objs.Length)], new Vector3(x, y, 0), Quaternion.identity);
                        obj.transform.SetParent(transform);
                    }
                    else if (Dungeon.instance.isMercenary && !Dungeon.instance.isMercenarySpawn && !Dungeon.instance.isSaveMap)
                    {
                        Dungeon.instance.isMercenarySpawn = true;
                        Dungeon.instance.board.map[x, y] = IceStone;
                        GameObject obj = Instantiate(MercenaryDoorObj, new Vector3(x, y, 0), Quaternion.identity);
                        obj.transform.SetParent(transform);
                    }
                }
            }
        }

        private bool Ground
        {
            get
            {
                try
                {
                    int x = (int)transform.position.x;
                    int y = (int)transform.position.y;

                    return matrix.map[x, y - 1] == dungeon.floor_1;
                }
                catch (System.Exception e)
                {
                    Debug.LogError(e.Message);
                    return false;
                }
            }
        }

        private bool CheckObj(int x, int y, int rangeX)
        {
            for (int i = -rangeX; i <= rangeX; i++)
            {
                if (
                    Dungeon.instance.board.map[x + i, y + 1] == IceStone ||
                    Dungeon.instance.board.map[x + i, y - 0] == IceStone ||
                    Dungeon.instance.board.map[x + i, y - 1] == IceStone ||
                    Dungeon.instance.board.map[x + i, y - 2] == IceStone ||
                    Dungeon.instance.board.map[x + i, y - 3] == IceStone ||
                    Dungeon.instance.board.map[x + i, y + 1] == Dungeon.instance.chestPrefab ||
                    Dungeon.instance.board.map[x + i, y - 0] == Dungeon.instance.chestPrefab ||
                    Dungeon.instance.board.map[x + i, y - 1] == Dungeon.instance.chestPrefab ||
                    Dungeon.instance.board.map[x + i, y - 2] == Dungeon.instance.chestPrefab ||
                    Dungeon.instance.board.map[x + i, y - 3] == Dungeon.instance.chestPrefab ||
                    Dungeon.instance.board.map[x + i, y + 1] == Dungeon.instance.chestPrefab2 ||
                    Dungeon.instance.board.map[x + i, y - 0] == Dungeon.instance.chestPrefab2 ||
                    Dungeon.instance.board.map[x + i, y - 1] == Dungeon.instance.chestPrefab2 ||
                    Dungeon.instance.board.map[x + i, y - 2] == Dungeon.instance.chestPrefab2 ||
                    Dungeon.instance.board.map[x + i, y - 3] == Dungeon.instance.chestPrefab2 ||
                    Dungeon.instance.board.map[x + i, y + 1] == Dungeon.instance.healthPrefab ||
                    Dungeon.instance.board.map[x + i, y - 0] == Dungeon.instance.healthPrefab ||
                    Dungeon.instance.board.map[x + i, y - 1] == Dungeon.instance.healthPrefab ||
                    Dungeon.instance.board.map[x + i, y - 2] == Dungeon.instance.healthPrefab ||
                    Dungeon.instance.board.map[x + i, y - 3] == Dungeon.instance.healthPrefab ||
                    Dungeon.instance.matrix.map[x + i, y] == Dungeon.instance.wall_side_mid_left ||
                    Dungeon.instance.matrix.map[x + i, y] == Dungeon.instance.wall_side_mid_right ||
                    Dungeon.instance.matrix.map[x + i, y] == Dungeon.instance.wall_left ||
                    Dungeon.instance.matrix.map[x + i, y] == Dungeon.instance.wall_right ||
                    Dungeon.instance.matrix.map[x + i, y - 0] == null ||
                    Dungeon.instance.matrix.map[x + i, y - 1] == null ||
                    Dungeon.instance.matrix.map[x + i, y - 2] == null)
                {
                    return false;
                }
            }
            return true;
        }

        private void Update()
        {
            if (Player.instance == null) return;
            if (isGround) return;
            float distance = Vector3.Distance(Player.instance.transform.position, transform.position);
            if (distance < 6)
            {
                sr.color = new Color(1, 1, 1, 0.5f);
            }
            else
            {
                sr.color = Color.white;
            }
        }
    }
}