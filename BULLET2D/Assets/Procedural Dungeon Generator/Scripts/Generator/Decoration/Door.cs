﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Door : MonoBehaviour
{
    private Animator animator;
    private bool isStart = false;
    public bool isClear = false;

    [Header("NPC 구출 전용")]
    public SpriteRenderer changeSR;
    public Sprite unlockSprite;
    public BoxCollider2D unlockCollider;
    [SerializeField] private bool isLock = false;
    private bool isSelected = false;
    [SerializeField] private GameObject pKey;
    [SerializeField] private GameObject QuestionPanel;
    [SerializeField] private Button YesBtn;
    [SerializeField] private Button NoBtn;
    [SerializeField] private Text contentText;
    [SerializeField] private Text keyText;

    float tempTime = 0.0f;
    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
        pKey = GameObject.Find("Canvas").transform.Find("PressKey").gameObject;
        QuestionPanel = GameObject.Find("Canvas").transform.Find("QuestionPanel3").gameObject;
        YesBtn = QuestionPanel.transform.Find("BtnPanel").Find("YesBtn").GetComponent<Button>();
        NoBtn = QuestionPanel.transform.Find("BtnPanel").Find("NoBtn").GetComponent<Button>();
        contentText = QuestionPanel.transform.Find("Text").GetComponent<Text>();
        keyText = GameObject.Find("Canvas").transform.Find("TutorialText").GetComponent<Text>();
    }
    public void AnimStart()
    {
        if (isStart) return;
        isStart = true;
        animator.SetTrigger("open");
    }

    private void Update()
    {
        tempTime += Time.deltaTime;
        if (isLock)
        {
            if (isSelected)
            {
                GetComponent<SpriteRenderer>().enabled = true;
                PDG.Dungeon.instance.isSelect = true;
                pKey.SetActive(true);
                Vector3 pos = transform.position;
                pos.y += 1.0f;
                pKey.transform.position = Camera.main.WorldToScreenPoint(pos);

                if ((PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ACTIVE], true) || PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_ACTIVE], true)) && tempTime > 0.05f)
                {
                    SoundManager.instance.StartAudio(new string[1] { "Object/active_F" }, VOLUME_TYPE.EFFECT);
                    tempTime = 0.0f;
                    contentText.text = LocalizeManager.GetLocalize("Dog_UnlockText");
                    QuestionPanel.SetActive(true);
                    if(Dialogue.Instance.dialogueObj.activeSelf)
                    {
                        Dialogue.Instance.isCommnet = false;
                        Dialogue.Instance.dialogueObj.SetActive(false);
                        PDG.Dungeon.instance.isSelect = false;
                    }
                }
            }
            else
                GetComponent<SpriteRenderer>().enabled = false;
        }

        if(QuestionPanel.activeSelf)
        {
            NoBtn.onClick.RemoveAllListeners();
            NoBtn.onClick.AddListener(() => {
                PDG.Player.instance.isCameraOff = false;
                QuestionPanel.SetActive(false);
            });
            YesBtn.onClick.RemoveAllListeners();
            YesBtn.onClick.AddListener(() => {
                if (Inventorys.Instance.key >= 2)
                {
                    SoundManager.instance.StartAudio(new string[2] { "Click_0", "Monster/lockopen" }, VOLUME_TYPE.EFFECT);
                    Inventorys.Instance.key -= 2;
                    changeSR.sprite = unlockSprite;
                    unlockCollider.enabled = false;
                    QuestionPanel.SetActive(false);
                    PDG.Player.instance.isCameraOff = false;
                    gameObject.SetActive(false);
                }
                else
                {
                    PDG.Player.instance.isCameraOff = false;
                    QuestionPanel.SetActive(false);
                    KeyTextOpen();
                }
            });

            if ((PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ACTIVE], true) || PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_ACTIVE], true)) && tempTime > 0.05f && Inventorys.Instance.key >= 2)
            {
                tempTime = 0.0f;
                SoundManager.instance.StartAudio(new string[2] { "Click_0", "Monster/lockopen" }, VOLUME_TYPE.EFFECT);
                Inventorys.Instance.key -= 3;
                changeSR.sprite = unlockSprite;
                unlockCollider.enabled = false;
                QuestionPanel.SetActive(false);
                PDG.Player.instance.isCameraOff = false;
                gameObject.SetActive(false);
            }
            if ((Input.GetKeyDown(KeyCode.Escape) || PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_CANCLE], true)) && tempTime > 0.05f)
            {
                PDG.Player.instance.isCameraOff = false;
                QuestionPanel.SetActive(false);
            }
        }
    }
    private void KeyTextOpen()
    {
        StartCoroutine(KeyTextOpen2());
    }
    IEnumerator KeyTextOpen2()
    {
        keyText.text = LocalizeManager.GetLocalize("key_lackText");
        keyText.gameObject.SetActive(true);
        yield return new WaitForSeconds(2.0f);
        keyText.gameObject.SetActive(false);
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.layer.Equals(9) && isClear && !isLock) AnimStart();


        if (collision.gameObject.layer.Equals(9) && isLock)
        {
            isSelected = true;
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        isSelected = false;
    }
}
