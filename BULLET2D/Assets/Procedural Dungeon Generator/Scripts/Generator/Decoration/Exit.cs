﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PDG;
using MOB;
using UnityEngine.UI;

public class Exit : MonoBehaviour
{
    [SerializeField] private Sprite OffImage;
    [SerializeField] private Sprite ActiveOffImage;
    [SerializeField] private Sprite OnImage;
    [SerializeField] private Sprite ActiveOnImage;

    [SerializeField] private Sprite SimbolOff;
    [SerializeField] private Sprite SimbolOn;
    public SpriteRenderer sr;
    public SpriteRenderer srSimbol;
    private Dungeon _dungeon;
    private Animator WarpAnim;
    private Animator WarpExitAnim;
    //private Animator WarpMonsterSpawnAnim;
    [SerializeField] private GameObject controllerObj;

    public int nNum = -1;
    public bool isSelected = false;
    public bool isLock = true;
    public bool isNotMonster = false;
    public int roomNum;

    public bool isWarp = false;

    public GameObject fadeObj;
    private float fadeTime = 0.1f;

    public bool isTutorial;
    public bool isFirstTutorial;

    public bool isNormal = false;

    [SerializeField] private Sprite bossSimbol;
    [SerializeField] private Sprite bossNormalSimbol;
    [SerializeField] private Sprite merchantSimbol;
    [SerializeField] private Sprite npcSimbol;

    [SerializeField] private string typeComment;
    // Start is called before the first frame update
    void Start()
    {
        typeComment = "warp_door_type_close_" + Dungeon.instance.warpType;
        _dungeon = Dungeon.instance;
        fadeObj = GameObject.Find("Canvas").transform.Find("fadeObj").gameObject;
        if (GameObject.Find("Generator"))
        {
            if (GameObject.Find("Generator").transform.Find("WarpMove"))
            {
                if (GameObject.Find("Generator").transform.Find("WarpMove").GetComponent<Animator>())
                {
                    WarpAnim = GameObject.Find("Generator").transform.Find("WarpMove").GetComponent<Animator>();
                }
            }
        }
        if (GameObject.Find("Generator"))
        {
            if (GameObject.Find("Generator").transform.Find("WarpMoveExit"))
            {
                if (GameObject.Find("Generator").transform.Find("WarpMoveExit").GetComponent<Animator>())
                {
                    WarpExitAnim = GameObject.Find("Generator").transform.Find("WarpMoveExit").GetComponent<Animator>();
                }
            }
        }
        StartCoroutine(WarpWait());
    }

    // Update is called once per frame
    void Update()
    {
        if (!isTutorial && Dungeon.instance.RoomInfo[roomNum].rooMType.Equals("bossstage") && !Dungeon.instance.isStageBoss && !isNormal) srSimbol.gameObject.SetActive(false);
        if (!isLock)
        {
            sr.sprite = OnImage;
            if (!isTutorial && Dungeon.instance.RoomInfo[roomNum].rooMType.Equals("bossstage") && Dungeon.instance.isStageBoss) srSimbol.sprite = bossSimbol;
            else if (!isTutorial && Dungeon.instance.RoomInfo[roomNum].rooMType.Equals("bossstage") && !Dungeon.instance.isStageBoss) srSimbol.sprite = bossNormalSimbol;
            else if (!isTutorial && Dungeon.instance.RoomInfo[roomNum].rooMType.Contains("d01_normalend_0")) srSimbol.sprite = bossNormalSimbol;
            else if (!isTutorial && Dungeon.instance.RoomInfo[roomNum].rooMType.Equals("merchantstage")) srSimbol.sprite = merchantSimbol;
            else if (!isTutorial && Dungeon.instance.RoomInfo[roomNum].rooMType.Equals("hiddenstage")) srSimbol.sprite = npcSimbol;
            else srSimbol.sprite = SimbolOn;
        }

        RaycastHit2D hit = Physics2D.BoxCast(transform.position, Vector2.one, 0, Vector2.zero, 0, 1 << 9);
        if (hit && !Dungeon.instance.newObjectSelected)
        {
            isSelected = true;
            if (controllerObj) controllerObj.SetActive(true);
            if (controllerObj) controllerObj.GetComponent<SpriteRenderer>().sprite = PDG.Dungeon.instance.isJoystrick ? PDG.Dungeon.GetJoyStrickKeySprite(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_ACTIVE]) : PDG.Dungeon.GetJoyStrickKeySprite(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ACTIVE]);
            AdjacentActive();
            Dungeon.instance.newObjectSelected = true;
            Dungeon.instance.newObjectSelectedObj = gameObject;
        }
        else if (!hit)
        {
            if (isLock)
            {
                sr.sprite = OffImage;
                srSimbol.sprite = SimbolOff;
            }
            if (Dungeon.instance.newObjectSelectedObj == gameObject)
            {
                Dungeon.instance.newObjectSelected = false;
                Dungeon.instance.newObjectSelectedObj = null;
                isSelected = false;
                if (controllerObj) controllerObj.SetActive(false);
                AdjacentUnActive();
            }
        }

        if (isSelected && !isNotMonster)
        {
            if (isFirstTutorial && nNum == 1)
            {
                isFirstTutorial = false;
                TutorialEvent_stage_01 obj = FindObjectOfType(typeof(TutorialEvent_stage_01)) as TutorialEvent_stage_01;
                obj.exit = this;
                Dialogue.Instance.CommnetSetting("tutorial_first_map_npc_3", 0, null);
                GameObject.Find("Canvas").transform.Find("TutorialText").gameObject.SetActive(false);
            }
            else
            {
                if (isLock) sr.sprite = ActiveOffImage;
                else sr.sprite = ActiveOnImage;

                if (nNum == 1)
                {
                    if ((PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ACTIVE], true) || Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_ACTIVE], true)) && !isWarp && !isLock && Dungeon.instance.warpGlobalCooltimeNow >= Dungeon.instance.warpGlobalCooltime && !Dialogue.Instance.dialogueObj.activeSelf)
                    {
                        if (Dungeon.isEliteSpawn)
                        {
                            EliteGetOut();
                        }
                        Dungeon.instance.warpGlobalCooltimeNow = 0.0f;
                        SoundManager.instance.StartAudio(new string[1] { "warp_insert" }, VOLUME_TYPE.EFFECT);
                        isWarp = true;
                        GameObject.Find("Canvas").transform.Find("BulletTarget").gameObject.SetActive(false);
                        StartCoroutine(WarpStart());
                    }
                    else if ((PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ACTIVE], true) || Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_ACTIVE], true)) && !isWarp && isLock && Dungeon.instance.warpGlobalCooltimeNow >= Dungeon.instance.warpGlobalCooltime)
                    {
                        if (Dungeon.isEliteSpawn)
                        {
                            EliteGetOut();
                        }
                        if (!isTutorial && !Dialogue.Instance.dialogueObj.activeSelf)
                        {
                            SoundManager.instance.StartAudio(new string[1] { "Object/active_F" }, VOLUME_TYPE.EFFECT);
                            Dungeon.instance.warpGlobalCooltimeNow = 0.0f;
                            Player.instance.transform.Find("HealMiniGreen").gameObject.SetActive(false);
                            if (Dungeon.instance.RoomInfo[roomNum].rooMType.Equals("bossstage")) TextOpenController.instance.ShowText(LocalizeManager.GetLocalize(typeComment), Color.white, 24, gameObject, 2.0f, false);
                            else TextOpenController.instance.ShowText(LocalizeManager.GetLocalize("closeddoor"), Color.white, 24, gameObject, 2.0f, false);

                            if (controllerObj) controllerObj.SetActive(false);
                            Dungeon.instance.newObjectSelected = false;
                            Dungeon.instance.newObjectSelectedObj = null;
                        }
                    }
                }
            }
        }
        else if (isNotMonster)
        {
            sr.enabled = false;
            srSimbol.gameObject.SetActive(false);
        }
    }
    void EliteGetOut()
    {
        Dungeon.isEliteSpawn = false;
        GameObject.Find("Canvas").transform.Find("TutorialText2").gameObject.SetActive(false);
        NPC[] _npcs = FindObjectsOfType(typeof(NPC)) as NPC[];
        if (_npcs != null && _npcs.Length > 0)
        {
            for (int i = 0; i < _npcs.Length; i++)
            {
                if (_npcs[i].isMonsterSpawn)
                {
                    _npcs[i].isOFF = false;
                    _npcs[i].GetComponent<Animator>().SetTrigger("ReSet");
                    if (_npcs[i].transform.Find("elite_monster_minimap")) _npcs[i].transform.Find("elite_monster_minimap").gameObject.SetActive(true);
                    break;
                }
            }
            SoundManager.instance.StartAudio(new string[1] { "BGM/Dungeon_" + Dungeon.instance.stageRan[Dungeon.instance.StageLevel] }, VOLUME_TYPE.BGM, null, true);
        }
    }
    #region -- Coroutine --
    int t = 0;
    IEnumerator WarpWait()
    {
        if (controllerObj) controllerObj.SetActive(false);
        while (true)
        {
            if (Dungeon.instance.isOpenWarp && isLock && isNormal && Dungeon.instance.RoomInfo[roomNum].rooMType.Equals("bossstage"))
            {
                isLock = false;
                isWarp = false;
                SoundManager.instance.StartAudio(new string[1] { "warp_open" }, VOLUME_TYPE.EFFECT);
                GameObject.Find("Canvas").transform.Find("TutorialText").gameObject.SetActive(true);
                GameObject.Find("Canvas").transform.Find("TutorialText").GetComponent<Text>().text = LocalizeManager.GetLocalize("warp_open_title");
                AdjacentUnLock();
            }
            else if(!Dungeon.instance.isOpenWarp && !isLock && isNormal)
            {
                isLock = true;
                isWarp = false;
                SoundManager.instance.StartAudio(new string[1] { "warp_open" }, VOLUME_TYPE.EFFECT);
                GameObject.Find("Canvas").transform.Find("TutorialText").gameObject.SetActive(true);
                GameObject.Find("Canvas").transform.Find("TutorialText").GetComponent<Text>().text = LocalizeManager.GetLocalize("warp_close_title");
                AdjacentLock(false);
            }
            yield return null;
        }
    }
    IEnumerator WarpStart()
    {
        t = 0;
        _dungeon.HeroObj.SetActive(false);
        if (Player.instance.isSubWeapon) Player.instance.pTimer.SetActive(false);
        if (controllerObj) controllerObj.SetActive(false);
        Vector3 pos = transform.position;
        pos.x -= 0.5f;
        pos.y -= 0.5f;
        if (WarpAnim != null)
        {
            WarpAnim.transform.position = pos;
            WarpAnim.gameObject.SetActive(true);
            WarpAnim.Rebind();
        }
        yield return new WaitForSeconds(1.5f);
        if (isTutorial)
        {
            Dungeon.instance.Clear();
            Dungeon.instance.isSelect = false;
            Dungeon.instance.nowTutorialNum++;
            Dungeon.instance.GoTutorial();
        }
        else
        {
            yield return StartCoroutine(FadeInOut());
            if (_dungeon.RoomInfo[roomNum].rooMType.Equals("bossstage") && t == 0 && Dungeon.instance.isStageBoss)
            {
                if (Dungeon.instance.stageBossRan[Dungeon.instance.StageLevel] == 1)
                {
                    if (GameObject.Find("Canvas").transform.Find("MiniMap_OutLine").gameObject.activeSelf) GameObject.Find("Canvas").transform.Find("MiniMap_OutLine").gameObject.SetActive(false);
                }
            }
            WarpExitAnim.gameObject.SetActive(true);
            WarpExitAnim.Rebind();
            yield return new WaitForSeconds(1.5f);
            WarpExitAnim.gameObject.SetActive(false);
            _dungeon.HeroObj.SetActive(true);
            if (Player.instance.isSubWeapon) Player.instance.pTimer.SetActive(true);
            _dungeon.HeroObj.GetComponent<Hero>().animator.SetInteger("Direction", 3);
            if (!_dungeon.RoomInfo[roomNum].isClear)
            {
                Vector2Int newPos = new Vector2Int((int)transform.position.x, (int)transform.position.y);
                int type = -1;
                for (int i = 0; i < _dungeon.RoomInfo[roomNum].EntranceWarps.Length; i++)
                    if (_dungeon.RoomInfo[roomNum].EntranceWarps[i] == newPos) type = 1;
                for (int i = 0; i < _dungeon.RoomInfo[roomNum].ExitWarps.Length; i++)
                    if (_dungeon.RoomInfo[roomNum].ExitWarps[i] == newPos) type = 0;


                int mask = 1 << 19;
                Vector3 pos2;
                if (type == 0) pos2 = _dungeon.RoomInfo[roomNum].EntranceWarps[1];
                else pos2 = _dungeon.RoomInfo[roomNum].ExitWarps[1];
                Roominfos info = _dungeon.RoomInfo[roomNum];
                info.isClear = true;
                _dungeon.RoomInfo[roomNum] = info;
                RaycastHit2D hit = Physics2D.BoxCast(pos2, new Vector2(0.5f, 0.5f), 0, Vector2.zero, 0, mask);
                if (hit)
                {
                    if (hit.collider.GetComponent<Exit>())
                    {
                        hit.collider.GetComponent<Exit>().isWarp = false;
                        hit.collider.GetComponent<Exit>().isLock = false;
                        hit.collider.GetComponent<Exit>().AdjacentUnLock(true);
                        GameObject obj = new GameObject("WarpLine")
                        {
                            layer = 18
                        };

                        if (_dungeon.RoomInfo[roomNum].rooMType.Equals("bossstage"))
                        {
                            Vector2 endPos = Dungeon.instance.waitingRoomCenter;
                            if (Dungeon.instance.StageLevel == 0)
                                endPos.y -= 5;
                            else if (Dungeon.instance.StageLevel == 1)
                                endPos.y -= 18;
                        }
                        obj.SetActive(false);
                        Dungeon.instance.LineObjs.Add(obj);
                    }
                }
            }

            if (_dungeon.RoomInfo[roomNum].rooMType.Equals("bossstage") && t == 0)
            {
                if (GameObject.Find("Canvas").transform.Find("TutorialText").GetComponent<Text>().text == LocalizeManager.GetLocalize("warp_open_title"))
                {
                    GameObject.Find("Canvas").transform.Find("TutorialText").gameObject.SetActive(false);
                }
                GameObject.Find("Canvas").transform.Find("TutorialText2").gameObject.SetActive(false);
                GameObject.Find("Canvas").transform.Find("TutorialText3").gameObject.SetActive(false);

                if (Dungeon.instance.isStageBoss)
                {
                    if (Dungeon.instance.stageBossRan[Dungeon.instance.StageLevel] == 1)
                    {
                        SoundManager.instance.StartAudio(new string[1] { "BGM/bossWaiting_2" }, VOLUME_TYPE.BGM, null, true);
                    }
                    else
                        SoundManager.instance.StartAudio(new string[1] { "bossWaiting" }, VOLUME_TYPE.BGM, null, true);
                }
            }
            else
            {
                GameObject.Find("Canvas").transform.Find("TutorialText").gameObject.SetActive(false);
                GameObject.Find("Canvas").transform.Find("TutorialText2").gameObject.SetActive(false);
                GameObject.Find("Canvas").transform.Find("TutorialText3").gameObject.SetActive(false);
                if (_dungeon.RoomInfo[roomNum].rooMType.Equals("startstage") && t == 1)
                {
                    Dungeon.instance.StartCoroutine(Dungeon.instance.MapGuide());
                }
                SoundManager.instance.StartAudio(new string[1] { "BGM/Dungeon_" + Dungeon.instance.stageRan[Dungeon.instance.StageLevel] }, VOLUME_TYPE.BGM, null, true);
            }
        }
        isWarp = false;
        if (t == 1) Dungeon.instance.RoomExit();
    }
    public IEnumerator FadeInOut()
    {
        fadeObj.SetActive(true);
        float a = 0.0f;
        while (a <= 1.0f)
        {
            fadeObj.GetComponent<Image>().color = new Color(0, 0, 0, a);
            a += Time.deltaTime / fadeTime;
            yield return new WaitForSeconds(Time.deltaTime);
        }
        fadeObj.GetComponent<Image>().color = new Color(0, 0, 0, 1);

        // 이동부분
        if (WarpAnim != null) WarpAnim.gameObject.SetActive(false);
        Vector2Int newPos = new Vector2Int((int)transform.position.x, (int)transform.position.y);
        int type = -1;
        for (int i = 0; i < _dungeon.RoomInfo[roomNum].EntranceWarps.Length; i++)
            if (_dungeon.RoomInfo[roomNum].EntranceWarps[i] == newPos) type = 1;
        for (int i = 0; i < _dungeon.RoomInfo[roomNum].ExitWarps.Length; i++)
            if (_dungeon.RoomInfo[roomNum].ExitWarps[i] == newPos) type = 0;
        Vector3 endPos;
        if (type == 0)
            endPos = _dungeon.RoomInfo[roomNum].EntranceWarps[3];
        else
        {
            endPos = _dungeon.RoomInfo[roomNum].ExitWarps[3];
            Dungeon.instance.FogOff(roomNum);
        }
        endPos.x += 1;
        if (_dungeon.RoomInfo[roomNum].rooMType.Equals("bossstage") && Dungeon.instance.isStageBoss)
        {
            if (type == 0)
            {
                endPos = Dungeon.instance.waitingRoomCenter;
                if (Dungeon.instance.stageBossRan[Dungeon.instance.StageLevel] == 0)
                    endPos.y -= 5;
                else if (Dungeon.instance.stageBossRan[Dungeon.instance.StageLevel] == 1)
                    endPos.y -= 18;
                _dungeon.HeroObj.transform.position = endPos;
            }
        }
        _dungeon.HeroObj.transform.position = endPos;
        Vector2 minusPosition = new Vector2(0.5f, -0.5f);

        if (WarpExitAnim != null)
        {
            WarpExitAnim.transform.position = (type == 0 ? _dungeon.RoomInfo[roomNum].EntranceWarps[1] + minusPosition : _dungeon.RoomInfo[roomNum].ExitWarps[1] + minusPosition);
        }


        yield return new WaitForSeconds(1.0f);
        while (a >= 0.0f)
        {
            fadeObj.GetComponent<Image>().color = new Color(0, 0, 0, a);
            a -= Time.deltaTime / fadeTime;
            yield return new WaitForSeconds(Time.deltaTime);
        }
        fadeObj.GetComponent<Image>().color = new Color(0, 0, 0, 0);
        fadeObj.SetActive(false);
        
        if (type == 0) // 다시 돌아옴
        {
            if (!_dungeon.RoomInfo[roomNum].rooMType.Equals("bossstage"))
                Dungeon.instance.RoomInsert(roomNum);
            else
            {
                Dungeon.instance.RoomInsert(roomNum, true);
            }

            if(_dungeon.RoomInfo[roomNum].roomID.Equals("d01_hell_dog_01"))
            {
                _dungeon.TutorialText.GetComponent<Text>().text = LocalizeManager.GetLocalize("helldog_guide");
                _dungeon.TutorialText.SetActive(true);
            }
            else
            {
                _dungeon.TutorialText.SetActive(false);
            }
            if (_dungeon.RoomInfo[roomNum].rooMType.Equals("bossstage"))
                Dungeon.instance.nowRoomType = "boss_waiting_" + (Dungeon.instance.stageBossRan[Dungeon.instance.StageLevel] + 1);
            else
                Dungeon.instance.nowRoomType = _dungeon.RoomInfo[roomNum].rooMType;
        }
        else // 나감
        {
            _dungeon.TutorialText.SetActive(false);
            Dungeon.instance.nowRoomType = "normal";
        }
        t = type;
        PDG.Player.instance.ExitCharacterOpen();
    }
    IEnumerator WarpMonsterSpawnStart()
    {
        if (SaveAndLoadManager.instance.LoadItem("Exit_Guide") == -1)
        {
            StartCoroutine(ItemGuide("Exit_Guide"));
        }
        if (controllerObj) controllerObj.SetActive(false);
        bool[] ranBool = new bool[8];
        Dungeon.instance.maxWarpCount = 3;
        Dungeon.instance.curWarpCount = 0;
        for (int i = 0; i < 3; i++)
        {
            Vector3 pos = transform.position;
            int ran = Random.Range(0, 8);
            if (!ranBool[ran])
            {
                ranBool[ran] = true;
            }
            else
            {
                i--;
                continue;
            }
            switch (ran)
            {
                case 0:
                    pos.x -= 2;
                    break;
                case 1:
                    pos.x -= 2;
                    pos.y -= 1;
                    break;
                case 2:
                    pos.x -= 1;
                    pos.y -= 1;
                    break;
                case 3:
                    pos.y -= 1;
                    break;
                case 4:
                    pos.x += 1;
                    break;
                case 5:
                    pos.x += 1;
                    pos.y -= 1;
                    break;
                case 6:
                    pos.x -= 1;
                    pos.y += 1;
                    break;
                case 7:
                    pos.y += 1;
                    break;
            }

            List<MonstersData> StageMonster = new List<MonstersData>();
            for (int j = 0; j < SheetManager.Instance.MonsterDB.dataArray.Length; j++)
            {
                for (int k = 0; k < SheetManager.Instance.MonsterDB.dataArray[j].Targetstage.Length; k++)
                {
                    if (SheetManager.Instance.MonsterDB.dataArray[j].Targetstage[k].Equals(Dungeon.instance.StageName) && SheetManager.Instance.MonsterDB.dataArray[j].Type.Equals("normal") && !SheetManager.Instance.MonsterDB.dataArray[j].Animtype.Equals("Roaming"))
                    {
                        StageMonster.Add(SheetManager.Instance.MonsterDB.dataArray[j]);
                        break;
                    }
                }
            }
            MonstersData mobData = StageMonster[Random.Range(0, StageMonster.Count)];
            StageData sData = System.Array.Find(SheetManager.Instance.StageDB.dataArray, item => item.Type2.Equals("normalstage") && item.Type1.Equals(Dungeon.instance.StageName));
            GameObject obj = ObjManager.Call().GetObject("Monster");
            if (obj != null)
            {
                obj.transform.position = pos;
                obj.SetActive(true);
                obj.GetComponent<MonsterArggro>().InitSetting(_dungeon.HeroObj, mobData, Random.Range(10, 15), true, mobData.Animtype);
                obj.GetComponent<MonsterArggro>().SpawnEffectStart();

                if (mobData.ID.Contains("gslime"))
                {
                    for (int k = 0; k < 2; k++)
                    {
                        GameObject _obj = ObjManager.Call().GetObject("Monster");
                        _obj.transform.position = pos;
                        _obj.SetActive(true);
                        _obj.GetComponent<MonsterArggro>().InitSetting(_dungeon.HeroObj, mobData, Random.Range(10, 15), true, mobData.Animtype);
                        _obj.GetComponent<MonsterArggro>().SpawnEffectStart();
                    }
                }
            }
            yield return new WaitForSeconds(1.0f);
        }
        while (true)
        {
            int cur = Dungeon.instance.curWarpCount;
            int max = Dungeon.instance.maxWarpCount;

            if (cur >= max)
            {
                isLock = false;
                isWarp = false;
                AdjacentUnLock();
            }
            yield return null;
        }
    }
    IEnumerator ItemGuide(string _id)
    {
        Inventorys.Instance.ItemGuideObj.transform.Find("Text").GetComponent<SetLanguage>().InitSetting(_id);
        yield return new WaitForSeconds(0.2f);
        Time.timeScale = 0;
        SoundManager.instance.StartAudio(new string[1] { "pressanykey" }, VOLUME_TYPE.EFFECT);
        Inventorys.Instance.ItemGuideObj.SetActive(true);
        Inventorys.Instance.isGuid = true;
    }
    #endregion
    #region -- Tool --
    public void AdjacentUnLock(bool _isDoor = false)
    {
        Vector2Int pos = new Vector2Int((int)transform.position.x, (int)transform.position.y);
        Vector2Int[] changePos = new Vector2Int[3];
        if (nNum == 0)
        {
            changePos[0] = new Vector2Int(pos.x + 1, pos.y + 0);
            changePos[1] = new Vector2Int(pos.x + 0, pos.y - 1);
            changePos[2] = new Vector2Int(pos.x + 1, pos.y - 1);
        }
        else if (nNum == 1)
        {
            changePos[0] = new Vector2Int(pos.x - 1, pos.y + 0);
            changePos[1] = new Vector2Int(pos.x - 1, pos.y - 1);
            changePos[2] = new Vector2Int(pos.x + 0, pos.y - 1);
        }
        else if (nNum == 2)
        {
            changePos[0] = new Vector2Int(pos.x + 0, pos.y + 1);
            changePos[1] = new Vector2Int(pos.x + 1, pos.y + 1);
            changePos[2] = new Vector2Int(pos.x + 1, pos.y + 0);
        }
        else
        {
            changePos[0] = new Vector2Int(pos.x - 1, pos.y + 1);
            changePos[1] = new Vector2Int(pos.x + 0, pos.y + 1);
            changePos[2] = new Vector2Int(pos.x - 1, pos.y + 0);
        }
        int mask = 1 << 19;
        for (int i = 0; i < changePos.Length; i++)
        {
            RaycastHit2D hit = Physics2D.BoxCast(changePos[i], new Vector2(0.5f, 0.5f), 0, Vector2.zero, 0, mask);
            if (hit)
            {
                if (hit.collider.GetComponent<Exit>())
                {
                    hit.collider.GetComponent<Exit>().isLock = false;

                }
            }
        }

        if (_isDoor)
        {
            if (Dungeon.instance.RoomInfo[roomNum].doorObj != null)
            {
                for (int i = 0; i < Dungeon.instance.RoomInfo[roomNum].doorObj.Count; i++)
                {
                    if (Dungeon.instance.RoomInfo[roomNum].doorObj[i].GetComponent<Door>())
                        Dungeon.instance.RoomInfo[roomNum].doorObj[i].GetComponent<Door>().isClear = true;
                }
            }
        }
    }
    public void AdjacentLock(bool isNotMonster)
    {
        Vector2Int pos = new Vector2Int((int)transform.position.x, (int)transform.position.y);
        Vector2Int[] changePos = new Vector2Int[3];
        if (nNum == 0)
        {
            changePos[0] = new Vector2Int(pos.x + 1, pos.y + 0);
            changePos[1] = new Vector2Int(pos.x + 0, pos.y - 1);
            changePos[2] = new Vector2Int(pos.x + 1, pos.y - 1);
        }
        else if (nNum == 1)
        {
            changePos[0] = new Vector2Int(pos.x - 1, pos.y + 0);
            changePos[1] = new Vector2Int(pos.x - 1, pos.y - 1);
            changePos[2] = new Vector2Int(pos.x + 0, pos.y - 1);
        }
        else if (nNum == 2)
        {
            changePos[0] = new Vector2Int(pos.x + 0, pos.y + 1);
            changePos[1] = new Vector2Int(pos.x + 1, pos.y + 1);
            changePos[2] = new Vector2Int(pos.x + 1, pos.y + 0);
        }
        else
        {
            changePos[0] = new Vector2Int(pos.x - 1, pos.y + 1);
            changePos[1] = new Vector2Int(pos.x + 0, pos.y + 1);
            changePos[2] = new Vector2Int(pos.x - 1, pos.y + 0);
        }
        int mask = 1 << 19;
        for (int i = 0; i < changePos.Length; i++)
        {
            RaycastHit2D hit = Physics2D.BoxCast(changePos[i], new Vector2(0.5f, 0.5f), 0, Vector2.zero, 0, mask);
            if (hit)
            {
                if (hit.collider.GetComponent<Exit>())
                {
                    hit.collider.GetComponent<Exit>().isLock = true;
                    hit.collider.GetComponent<Exit>().isNotMonster = isNotMonster;

                }
            }
        }
    }
    public void AdjacentActive()
    {
        Vector2Int pos = new Vector2Int(Mathf.FloorToInt(transform.position.x), Mathf.FloorToInt(transform.position.y));
        Vector2Int[] changePos = new Vector2Int[3];
        if (nNum == 0)
        {
            changePos[0] = new Vector2Int(pos.x + 1, pos.y + 0);
            changePos[1] = new Vector2Int(pos.x + 0, pos.y - 1);
            changePos[2] = new Vector2Int(pos.x + 1, pos.y - 1);
        }
        else if (nNum == 1)
        {
            changePos[0] = new Vector2Int(pos.x - 1, pos.y + 0);
            changePos[1] = new Vector2Int(pos.x - 1, pos.y - 1);
            changePos[2] = new Vector2Int(pos.x + 0, pos.y - 1);
        }
        else if (nNum == 2)
        {
            changePos[0] = new Vector2Int(pos.x + 0, pos.y + 1);
            changePos[1] = new Vector2Int(pos.x + 1, pos.y + 1);
            changePos[2] = new Vector2Int(pos.x + 1, pos.y + 0);
        }
        else
        {
            changePos[0] = new Vector2Int(pos.x - 1, pos.y + 1);
            changePos[1] = new Vector2Int(pos.x + 0, pos.y + 1);
            changePos[2] = new Vector2Int(pos.x - 1, pos.y + 0);
        }

        int mask = 1 << 19;
        for (int i = 0; i < changePos.Length; i++)
        {
            RaycastHit2D hit = Physics2D.BoxCast(changePos[i], new Vector2(0.8f, 0.8f), 0, Vector2.zero, 0, mask);
            if (hit)
            {
                if (hit.collider.GetComponent<Exit>())
                {
                    if (hit.collider.GetComponent<Exit>().controllerObj) hit.collider.GetComponent<Exit>().controllerObj.SetActive(true);
                    hit.collider.GetComponent<Exit>().isSelected = true;
                }
            }
        }
    }
    public void AdjacentUnActive()
    {
        Vector2Int pos = new Vector2Int((int)transform.position.x, (int)transform.position.y);
        Vector2Int[] changePos = new Vector2Int[3];
        if (nNum == 0)
        {
            changePos[0] = new Vector2Int(pos.x + 1, pos.y + 0);
            changePos[1] = new Vector2Int(pos.x + 0, pos.y - 1);
            changePos[2] = new Vector2Int(pos.x + 1, pos.y - 1);
        }
        else if (nNum == 1)
        {
            changePos[0] = new Vector2Int(pos.x - 1, pos.y + 0);
            changePos[1] = new Vector2Int(pos.x - 1, pos.y - 1);
            changePos[2] = new Vector2Int(pos.x + 0, pos.y - 1);
        }
        else if (nNum == 2)
        {
            changePos[0] = new Vector2Int(pos.x + 0, pos.y + 1);
            changePos[1] = new Vector2Int(pos.x + 1, pos.y + 1);
            changePos[2] = new Vector2Int(pos.x + 1, pos.y + 0);
        }
        else
        {
            changePos[0] = new Vector2Int(pos.x - 1, pos.y + 1);
            changePos[1] = new Vector2Int(pos.x + 0, pos.y + 1);
            changePos[2] = new Vector2Int(pos.x - 1, pos.y + 0);
        }

        int mask = 1 << 19;
        for (int i = 0; i < changePos.Length; i++)
        {
            RaycastHit2D hit = Physics2D.BoxCast(changePos[i], new Vector2(0.5f, 0.5f), 0, Vector2.zero, 0, mask);
            if (hit)
            {
                if (hit.collider.GetComponent<Exit>())
                {
                    if (hit.collider.GetComponent<Exit>().controllerObj) hit.collider.GetComponent<Exit>().controllerObj.SetActive(false);
                    hit.collider.GetComponent<Exit>().isSelected = false;
                }
            }
        }
    }
    #endregion
}
