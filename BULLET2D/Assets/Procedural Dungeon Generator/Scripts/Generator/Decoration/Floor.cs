﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace PDG
{
	public class Floor : MonoBehaviour
    {
        [SerializeField] private Sprite[] decoration;
        [SerializeField] private Sprite[] boxRoomDeco;
        [SerializeField] private List<GameObject> floorList = new List<GameObject>();
        [SerializeField] private GameObject eleteObj;
        [SerializeField] private GameObject totemObj;
        private int roomIndex;
        [SerializeField] private GameObject IceStone;
        [SerializeField] private GameObject BuffTower;


        [SerializeField] private GameObject destroyObj_1;
        [SerializeField] private GameObject destroyObj_3;

        private void Start()
        {
            string nowDungeon = Dungeon.instance.StageName;
            decoration = Resources.LoadAll<Sprite>("DungeonSprite/Floor/" + nowDungeon);
            floorList = Resources.LoadAll<GameObject>("Object/Prefabs/" + nowDungeon).ToList();
            IceStone = Resources.Load("Object/Prefabs/IceStone") as GameObject;

            if (nowDungeon.Equals("Dungeon_0")) GetComponent<SpriteRenderer>().color = Color.white;
            else if (nowDungeon.Equals("Dungeon_1")) GetComponent<SpriteRenderer>().color = new Color32(181, 181, 181, 255);

            int x = (int)transform.position.x;
			int y = (int)transform.position.y;

            if (Dungeon.instance != null && !Dungeon.instance.isSaveMap)
            {
                if (Dungeon.instance.matrix.map[x, y] != null)
                {
                    if (Dungeon.instance.matrix.map[x, y] == Dungeon.instance.exit_0 ||
                        Dungeon.instance.matrix.map[x, y] == Dungeon.instance.exit_1 ||
                        Dungeon.instance.matrix.map[x, y] == Dungeon.instance.exit_2 ||
                        Dungeon.instance.matrix.map[x, y] == Dungeon.instance.exit_3)
                    {
                        GetComponent<SpriteRenderer>().sprite = decoration[Random.Range(0, decoration.Length)];
                        return;
                    }
                }
            }
            roomIndex = Dungeon.instance.GetRoomIndex((int)transform.position.x, (int)transform.position.y);
            if (roomIndex != -1 )
            {
                if((Dungeon.instance.RoomInfo[roomIndex].rooMType.Equals("Start") || Dungeon.instance.RoomInfo[roomIndex].rooMType.Equals("Shop")) && !Dungeon.instance.isSaveMap)
                {
                    GetComponent<SpriteRenderer>().sprite = decoration[Random.Range(0, decoration.Length)];
                }
                else if(Dungeon.instance.RoomInfo[roomIndex].rooMType.Equals("Box") && !Dungeon.instance.isSaveMap)
                {
                    GetComponent<SpriteRenderer>().sprite = boxRoomDeco[Random.Range(0, boxRoomDeco.Length)];
                }
                else
                {
                    bool ccc = false;
                    if (!Dungeon.instance.isEleteObjInstall && !Dungeon.instance.isSaveMap)
                    {
                        if (CheckObj(x, y, 3) && Dungeon.instance.matrix.map[x, y - 1] != Dungeon.instance.wall_top_mid && Dungeon.instance.matrix.map[x, y - 2] != Dungeon.instance.wall_top_mid
                            && Dungeon.instance.matrix.map[x - 1, y - 1] != Dungeon.instance.wall_corner_top_right && Dungeon.instance.matrix.map[x - 1, y - 1] != Dungeon.instance.wall_corner_top_left
                            && Dungeon.instance.matrix.map[x + 1, y - 1] != Dungeon.instance.wall_corner_top_right && Dungeon.instance.matrix.map[x + 1, y - 1] != Dungeon.instance.wall_corner_top_left
                            && Dungeon.instance.matrix.map[x - 1, y - 2] != Dungeon.instance.wall_corner_top_right && Dungeon.instance.matrix.map[x - 1, y - 2] != Dungeon.instance.wall_corner_top_left
                            && Dungeon.instance.matrix.map[x + 1, y - 2] != Dungeon.instance.wall_corner_top_right && Dungeon.instance.matrix.map[x + 1, y - 2] != Dungeon.instance.wall_corner_top_left
                            && Dungeon.instance.matrix.map[x - 1, y - 2] == Dungeon.instance.floor_1 && Dungeon.instance.matrix.map[x - 2, y - 2] == Dungeon.instance.floor_1 && Dungeon.instance.matrix.map[x - 3, y - 2] == Dungeon.instance.floor_1
                            && Dungeon.instance.matrix.map[x - 1, y - 1] == Dungeon.instance.floor_1 && Dungeon.instance.matrix.map[x - 2, y - 1] == Dungeon.instance.floor_1 && Dungeon.instance.matrix.map[x - 3, y - 1] == Dungeon.instance.floor_1
                            && Dungeon.instance.matrix.map[x - 1, y - 0] == Dungeon.instance.floor_1 && Dungeon.instance.matrix.map[x - 2, y - 0] == Dungeon.instance.floor_1 && Dungeon.instance.matrix.map[x - 3, y - 0] == Dungeon.instance.floor_1
                            && Dungeon.instance.matrix.map[x - 1, y + 1] == Dungeon.instance.floor_1 && Dungeon.instance.matrix.map[x - 2, y + 1] == Dungeon.instance.floor_1 && Dungeon.instance.matrix.map[x - 3, y + 1] == Dungeon.instance.floor_1
                            && Dungeon.instance.matrix.map[x - 1, y + 2] == Dungeon.instance.floor_1 && Dungeon.instance.matrix.map[x - 2, y + 2] == Dungeon.instance.floor_1 && Dungeon.instance.matrix.map[x - 3, y + 2] == Dungeon.instance.floor_1)
                        {
                            ccc = true;
                            Dungeon.instance.isEleteObjInstall = true;
                            Dungeon.instance.board.map[x, y] = IceStone;
                            GameObject obj = Instantiate(eleteObj, new Vector3(x, y, 0), Quaternion.identity);
                            obj.transform.SetParent(transform);
                            return;
                        }
                        GetComponent<SpriteRenderer>().sprite = decoration[Random.Range(0, decoration.Length)];
                    }
                    if (!Dungeon.instance.isShopFlag && !ccc && !Dungeon.instance.isSaveMap)
                    {
                        if (CheckObj(x, y, 3)
                            && Dungeon.instance.matrix.map[x - 2, y - 3] == Dungeon.instance.floor_1 && Dungeon.instance.board.map[x - 2, y - 3] == null
                            && Dungeon.instance.matrix.map[x - 0, y - 3] == Dungeon.instance.floor_1 && Dungeon.instance.board.map[x - 0, y - 3] == null
                            && Dungeon.instance.matrix.map[x + 2, y - 3] == Dungeon.instance.floor_1 && Dungeon.instance.board.map[x + 2, y - 3] == null
                            && Dungeon.instance.matrix.map[x - 2, y - 4] == Dungeon.instance.floor_1 && Dungeon.instance.board.map[x + 2, y - 3] == null
                            && Dungeon.instance.matrix.map[x + 0, y - 4] == Dungeon.instance.floor_1 && Dungeon.instance.board.map[x + 2, y - 3] == null
                            && Dungeon.instance.matrix.map[x + 2, y - 4] == Dungeon.instance.floor_1 && Dungeon.instance.board.map[x + 2, y - 3] == null
                            && Dungeon.instance.matrix.map[x - 5, y - 4] == Dungeon.instance.floor_1 && Dungeon.instance.board.map[x + 2, y - 3] == null
                            && Dungeon.instance.matrix.map[x + 5, y - 4] == Dungeon.instance.floor_1 && Dungeon.instance.board.map[x + 2, y - 3] == null
                            && Dungeon.instance.matrix.map[x - 5, y - 3] == Dungeon.instance.floor_1 && Dungeon.instance.board.map[x + 2, y - 3] == null
                            && Dungeon.instance.matrix.map[x + 5, y - 3] == Dungeon.instance.floor_1 && Dungeon.instance.board.map[x + 2, y - 3] == null)
                        {
                            ccc = true;
                            Dungeon.instance.isShopFlag = true;
                            GameObject obj = Instantiate(Resources.Load<GameObject>("Object/Prefabs/Shop"), new Vector3(x, y, 0), Quaternion.identity);
                            obj.transform.SetParent(transform);
                            Dungeon.instance.board.map[x, y] = IceStone;
                            return;
                        }
                    }
                    if(!Dungeon.instance.isTotemInstall && CheckObj(x, y, 3) && !ccc && Dungeon.instance.matrix.map[x, y + 1] == Dungeon.instance.wall_mid && !Dungeon.instance.isSaveMap)
                    {
                        Dungeon.instance.isTotemInstall = true;
                        int t = Random.Range(0, 100);
                        if (t > 20 && t <= 80)
                        {
                            Dungeon.instance.board.map[x, y] = IceStone;
                            Dungeon.instance.board.map[x, y - 1] = IceStone;
                            GameObject obj = Instantiate(totemObj, new Vector3(x, y, 0), Quaternion.identity);
                            obj.transform.SetParent(transform);
                        }
                        GetComponent<SpriteRenderer>().sprite = decoration[Random.Range(0, decoration.Length)];
                        return;
                    }
                    if (CheckObj(x, y, 3) && Dungeon.instance.matrix.map[x, y + 1] == Dungeon.instance.wall_mid && !Dungeon.instance.isSaveMap)
                    {
                        GameObject obj;
                        if (!Dungeon.instance.isDungeonComputer5)
                        {
                            if (Dungeon.instance.nDungeonComputer5Count > 3)
                                Dungeon.instance.isDungeonComputer5 = true;
                            else
                                Dungeon.instance.nDungeonComputer5Count++;
                            obj = Instantiate(Resources.Load<GameObject>("Floor/FireArm"), new Vector3(x, y, 0), Quaternion.identity);
                            obj.GetComponent<SpriteRenderer>().sortingOrder = 4;
                            obj.transform.SetParent(transform);
                            Dungeon.instance.board.map[x, y] = IceStone;
                            GetComponent<SpriteRenderer>().sprite = decoration[Random.Range(0, decoration.Length)];
                            return;
                        }
                    }
                    if (CheckObj(x, y, 4) && !Dungeon.instance.isSaveMap)
                    {
                        GameObject obj;
                        if (!Dungeon.instance.isBuffTower)
                        {
                            if (Dungeon.instance.nBuffTower > 1)
                                Dungeon.instance.isBuffTower = true;
                            else
                                Dungeon.instance.nBuffTower++;
                            obj = Instantiate(BuffTower, new Vector3(x, y, 0), Quaternion.identity);
                            obj.GetComponent<SpriteRenderer>().sortingOrder = 4;
                            obj.transform.SetParent(transform);
                            Dungeon.instance.board.map[x, y] = IceStone;
                            GetComponent<SpriteRenderer>().sprite = decoration[Random.Range(0, decoration.Length)];
                            return;
                        }
                    }
                    if (!ccc )
                    {
                        GetComponent<SpriteRenderer>().sprite = decoration[Random.Range(0, decoration.Length)];

                        int ran2 = Random.Range(0, floorList.Count + 100);
                        bool c = false;
                        if (nowDungeon.Equals("Dungeon_3") && CheckObj(x, y, 3) && !Dungeon.instance.isSaveMap)
                        {
                            GameObject obj;
                            if (!Dungeon.instance.isDungeonComputer && Dungeon.instance.matrix.map[x, y + 1] == Dungeon.instance.wall_mid)
                            {
                                Dungeon.instance.isDungeonComputer = true;
                                obj = Instantiate(Resources.Load<GameObject>("Floor/Dungeon_3_Computer"), new Vector3(x, y, 0), Quaternion.identity);
                                obj.GetComponent<SpriteRenderer>().sortingOrder = 4;
                                obj.transform.SetParent(transform);
                                Dungeon.instance.board.map[x, y] = IceStone;
                                return;
                            }
                            else if (!Dungeon.instance.isDungeonComputer2 && Dungeon.instance.matrix.map[x, y + 1] == Dungeon.instance.wall_mid)
                            {
                                Dungeon.instance.isDungeonComputer2 = true;
                                obj = Instantiate(Resources.Load<GameObject>("Floor/Dungeon_3_Computer2"), new Vector3(x, y, 0), Quaternion.identity);
                                obj.GetComponent<SpriteRenderer>().sortingOrder = 4;
                                obj.transform.SetParent(transform);
                                Dungeon.instance.board.map[x, y] = IceStone;
                                return;
                            }
                            else
                            {
                                if (ran2 < 46)
                                {
                                    obj = Instantiate(IceStone, new Vector3(x, y, 0), Quaternion.identity);
                                    Dungeon.instance.board.map[x, y] = IceStone;
                                    Sprite[] sprites = Resources.LoadAll<Sprite>("Object/Dungeon_3");
                                    obj.GetComponent<SpriteRenderer>().sprite = sprites[sprites.Length - 1];
                                    obj.GetComponent<ChestHit>().DestroyObj = destroyObj_1;
                                    obj.transform.SetParent(transform);
                                    obj.GetComponent<SpriteRenderer>().sortingOrder = -4;
                                    return;
                                }
                            }
                        }
                        if (nowDungeon.Equals("Dungeon_0") && ran2 < 4 && !c && CheckObj(x, y, 2) && !Dungeon.instance.isSaveMap)
                        {
                            GameObject obj = Instantiate(IceStone, new Vector3(x, y, 0), Quaternion.identity);
                            Sprite[] sprites = Resources.LoadAll<Sprite>("Object/Dungeon_0");
                            obj.GetComponent<SpriteRenderer>().sprite = sprites[Random.Range(0, sprites.Length - 1)];
                            obj.GetComponent<ChestHit>().DestroyObj = destroyObj_1;
                            obj.transform.SetParent(transform);
                            obj.GetComponent<SpriteRenderer>().sortingOrder = -4;
                            Dungeon.instance.board.map[x, y] = IceStone;
                            return;
                        }
                        if (nowDungeon.Equals("Dungeon_1") && ran2 < 6 && !c && CheckObj(x, y, 2) && !Dungeon.instance.isSaveMap)
                        {
                            GameObject obj = Instantiate(IceStone, new Vector3(x, y, 0), Quaternion.identity);
                            Sprite[] sprites = Resources.LoadAll<Sprite>("Object/Dungeon_1");
                            obj.GetComponent<SpriteRenderer>().sprite = sprites[Random.Range(2, sprites.Length - 1)];
                            obj.GetComponent<ChestHit>().DestroyObj = destroyObj_1;
                            obj.transform.SetParent(transform);
                            obj.GetComponent<SpriteRenderer>().sortingOrder = -4;
                            Dungeon.instance.board.map[x, y] = IceStone;
                            return;
                        }
                        if (nowDungeon.Equals("Dungeon_1") && CheckObj(x, y, 3) && !Dungeon.instance.isSaveMap)
                        {
                            GameObject obj;
                            if (!Dungeon.instance.isDungeonComputer && Dungeon.instance.matrix.map[x, y + 1] == Dungeon.instance.wall_mid)
                            {
                                Dungeon.instance.isDungeonComputer = true;
                                obj = Instantiate(Resources.Load<GameObject>("Floor/Dungeon_1_Computer"), new Vector3(x, y, 0), Quaternion.identity);
                                obj.GetComponent<SpriteRenderer>().sortingOrder = 4;
                                obj.transform.SetParent(transform);
                                Dungeon.instance.board.map[x, y] = IceStone;
                                return;
                            }
                            else if (!Dungeon.instance.isDungeonComputer2 && Dungeon.instance.matrix.map[x, y + 1] == Dungeon.instance.wall_mid)
                            {
                                Dungeon.instance.isDungeonComputer2 = true;
                                obj = Instantiate(Resources.Load<GameObject>("Floor/Dungeon_1_Computer2"), new Vector3(x, y, 0), Quaternion.identity);
                                obj.GetComponent<SpriteRenderer>().sortingOrder = 4;
                                obj.transform.SetParent(transform);
                                Dungeon.instance.board.map[x, y] = IceStone;
                                return;
                            }
                            else
                            {
                                if (ran2 < 46)
                                {
                                    obj = Instantiate(IceStone, new Vector3(x, y, 0), Quaternion.identity);
                                    Sprite[] sprites = Resources.LoadAll<Sprite>("Object/Dungeon_1");
                                    int tt = Random.Range(0, 3);
                                    obj.GetComponent<SpriteRenderer>().sprite = sprites[tt == 3 ? sprites.Length - 1 : tt];
                                    obj.GetComponent<ChestHit>().DestroyObj = destroyObj_1;
                                    obj.transform.SetParent(transform);
                                    obj.GetComponent<SpriteRenderer>().sortingOrder = -4;
                                    Dungeon.instance.board.map[x, y] = IceStone;
                                    return;
                                }
                            }
                        }
                        if ((nowDungeon.Equals("Dungeon_2") || nowDungeon.Equals("Dungeon_4")) && CheckObj(x, y, 3) && !Dungeon.instance.isSaveMap)
                        {
                            if (Dungeon.instance.matrix.map[x, y + 2] == Dungeon.instance.wall_mid)
                            {
                                GameObject obj;
                                if (!Dungeon.instance.isDungeonComputer)
                                {
                                    Dungeon.instance.isDungeonComputer = true;
                                    obj = Instantiate(Resources.Load<GameObject>("Floor/Dungeon_2_Cabinet"), new Vector3(x, y, 0), Quaternion.identity);
                                    obj.GetComponent<SpriteRenderer>().sortingOrder = 4;
                                    obj.transform.SetParent(transform);
                                    Dungeon.instance.board.map[x, y] = IceStone;
                                    return;
                                }
                                else if (!Dungeon.instance.isDungeonComputer2)
                                {
                                    Dungeon.instance.isDungeonComputer2 = true;
                                    obj = Instantiate(Resources.Load<GameObject>("Floor/Dungeon_2_Cabinet2"), new Vector3(x, y, 0), Quaternion.identity);
                                    obj.GetComponent<SpriteRenderer>().sortingOrder = 4;
                                    obj.transform.SetParent(transform);
                                    Dungeon.instance.board.map[x, y] = IceStone;
                                    return;
                                }
                                else if (!Dungeon.instance.isDungeonComputer3)
                                {
                                    Dungeon.instance.isDungeonComputer3 = true;
                                    obj = Instantiate(Resources.Load<GameObject>("Floor/Dungeon_2_Cabinet3"), new Vector3(x, y, 0), Quaternion.identity);
                                    obj.GetComponent<SpriteRenderer>().sortingOrder = 4;
                                    obj.transform.SetParent(transform);
                                    Dungeon.instance.board.map[x, y] = IceStone;
                                    return;
                                }
                                else if (!Dungeon.instance.isDungeonComputer4)
                                {
                                    Dungeon.instance.isDungeonComputer4 = true;
                                    obj = Instantiate(Resources.Load<GameObject>("Floor/Dungeon_2_Cabinet4"), new Vector3(x, y, 0), Quaternion.identity);
                                    obj.GetComponent<SpriteRenderer>().sortingOrder = 4;
                                    obj.transform.SetParent(transform);
                                    Dungeon.instance.board.map[x, y] = IceStone;
                                    return;
                                }
                            }
                        }
                        if (ran2 < floorList.Count && (floorList[ran2].name.Equals("door") || floorList[ran2].name.Equals("iron_piece") || floorList[ran2].name.Equals("object_grass")))
                        {
                            for (int i = -1; i <= 1; i++)
                            {
                                for (int j = -1; j <= 1; j++)
                                {
                                    if (!Dungeon.instance.Find(x + i, y + j, Dungeon.instance.floor_1))
                                    {
                                        c = true;
                                        break;
                                    }
                                }
                            }
                        }
                        if (ran2 < floorList.Count && floorList[ran2].name.Equals("object_grass2"))
                        {
                            for (int i = -3; i <= 3; i++)
                            {
                                for (int j = -2; j <= 2; j++)
                                {
                                    if (!Dungeon.instance.Find(x + i, y + j, Dungeon.instance.floor_1))
                                    {
                                        c = true;
                                        break;
                                    }
                                }
                            }
                        }
                        if (ran2 < floorList.Count && floorList[ran2].name.Equals("hit_drum"))
                        {
                            for (int i = -1; i <= 1; i++)
                            {
                                for (int j = -4; j <= 0; j++)
                                {
                                    if (!Dungeon.instance.Find(x + i, y + j, Dungeon.instance.floor_1))
                                    {
                                        c = true;
                                        break;
                                    }
                                }
                            }
                        }
                        if (ran2 < floorList.Count && !c && CheckObj(x, y, 1))
                        {
                            GameObject obj = Instantiate(floorList[ran2], new Vector3(x, y, 0), Quaternion.identity);
                            if (floorList[ran2].name.Equals("Spikes")) obj.GetComponent<Animator>().Play("Spikes", 0, Random.Range(0.0f, 1.0f));
                            if (floorList[ran2].name.Equals("hole_bomb")) obj.GetComponent<Animator>().Play("hole_bomb", 0, Random.Range(0.0f, 1.0f));
                            if (floorList[ran2].name.Equals("air_sickle")) obj.transform.position = new Vector3(x, y + 5, 0);
                            obj.transform.SetParent(transform);
                            obj.GetComponent<SpriteRenderer>().sortingOrder = -4;
                            Dungeon.instance.board.map[x, y] = IceStone;
                            return;
                        }
                        if (nowDungeon.Equals("Dungeon_3") && ran2 < 44 && !c && CheckObj(x, y, 3))
                        {
                            GameObject obj = Instantiate(IceStone, new Vector3(x, y, 0), Quaternion.identity);
                            Sprite[] sprites = Resources.LoadAll<Sprite>("Object/Dungeon_3");
                            obj.GetComponent<ChestHit>().DestroyObj = destroyObj_1;
                            obj.GetComponent<SpriteRenderer>().sprite = sprites[Random.Range(0, sprites.Length - 1)];
                            obj.transform.SetParent(transform);
                            obj.GetComponent<SpriteRenderer>().sortingOrder = -4;
                            Dungeon.instance.board.map[x, y] = IceStone;
                            return;
                        }
                        else if (nowDungeon.Equals("Dungeon_2") && ran2 < 44 && !c && CheckObj(x, y, 3))
                        {
                            GameObject obj = Instantiate(IceStone, new Vector3(x, y, 0), Quaternion.identity);
                            Sprite[] sprites = Resources.LoadAll<Sprite>("Object/Dungeon_2");
                            obj.GetComponent<ChestHit>().DestroyObj = destroyObj_1;
                            obj.GetComponent<SpriteRenderer>().sprite = sprites[Random.Range(0, sprites.Length - 1)];
                            obj.transform.SetParent(transform);
                            obj.GetComponent<SpriteRenderer>().sortingOrder = -4;
                            Dungeon.instance.board.map[x, y] = IceStone;
                            return;
                        }
                        else if (nowDungeon.Equals("Dungeon_4") && ran2 < 44 && !c && CheckObj(x, y, 3))
                        {
                            GameObject obj = Instantiate(IceStone, new Vector3(x, y, 0), Quaternion.identity);
                            Sprite[] sprites = Resources.LoadAll<Sprite>("Object/Dungeon_4");
                            obj.GetComponent<ChestHit>().DestroyObj = destroyObj_1;
                            obj.GetComponent<SpriteRenderer>().sprite = sprites[Random.Range(0, sprites.Length - 1)];
                            obj.transform.SetParent(transform);
                            obj.GetComponent<SpriteRenderer>().sortingOrder = -4;
                            Dungeon.instance.board.map[x, y] = IceStone;
                            return;
                        }
                    }
                }
            }
            else
            {
                GetComponent<SpriteRenderer>().sprite = decoration[Random.Range(0, decoration.Length)];
            }
        }
        private bool CheckObj(int x, int y, int rangeX)
        {
            for (int i = -rangeX; i <= rangeX; i++)
            {
                if (
                    Dungeon.instance.board.map[x + i, y + 1] == IceStone ||
                    Dungeon.instance.board.map[x + i, y - 0] == IceStone ||
                    Dungeon.instance.board.map[x + i, y - 1] == IceStone ||
                    Dungeon.instance.board.map[x + i, y - 2] == IceStone ||
                    Dungeon.instance.board.map[x + i, y - 3] == IceStone ||

                    Dungeon.instance.board.map[x + i, y + 1] == Dungeon.instance.chestPrefab ||
                    Dungeon.instance.board.map[x + i, y - 0] == Dungeon.instance.chestPrefab ||
                    Dungeon.instance.board.map[x + i, y - 1] == Dungeon.instance.chestPrefab ||
                    Dungeon.instance.board.map[x + i, y - 2] == Dungeon.instance.chestPrefab ||
                    Dungeon.instance.board.map[x + i, y - 3] == Dungeon.instance.chestPrefab ||

                    Dungeon.instance.board.map[x + i, y + 1] == Dungeon.instance.chestPrefab2 ||
                    Dungeon.instance.board.map[x + i, y - 0] == Dungeon.instance.chestPrefab2 ||
                    Dungeon.instance.board.map[x + i, y - 1] == Dungeon.instance.chestPrefab2 ||
                    Dungeon.instance.board.map[x + i, y - 2] == Dungeon.instance.chestPrefab2 ||
                    Dungeon.instance.board.map[x + i, y - 3] == Dungeon.instance.chestPrefab2 ||

                    Dungeon.instance.board.map[x + i, y + 1] == Dungeon.instance.healthPrefab ||
                    Dungeon.instance.board.map[x + i, y - 0] == Dungeon.instance.healthPrefab ||
                    Dungeon.instance.board.map[x + i, y - 1] == Dungeon.instance.healthPrefab ||
                    Dungeon.instance.board.map[x + i, y - 2] == Dungeon.instance.healthPrefab ||
                    Dungeon.instance.board.map[x + i, y - 3] == Dungeon.instance.healthPrefab ||

                    Dungeon.instance.matrix.map[x + i, y] == Dungeon.instance.wall_side_mid_left ||
                    Dungeon.instance.matrix.map[x + i, y] == Dungeon.instance.wall_side_mid_right ||
                    Dungeon.instance.matrix.map[x + i, y] == Dungeon.instance.wall_left ||
                    Dungeon.instance.matrix.map[x + i, y] == Dungeon.instance.wall_right ||

                    Dungeon.instance.matrix.map[x + i, y - 1] == Dungeon.instance.exit_0 ||
                    Dungeon.instance.matrix.map[x + i, y - 1] == Dungeon.instance.exit_1 ||
                    Dungeon.instance.matrix.map[x + i, y - 1] == Dungeon.instance.exit_2 ||
                    Dungeon.instance.matrix.map[x + i, y - 1] == Dungeon.instance.exit_3 ||

                    Dungeon.instance.matrix.map[x + i, y + 0] == Dungeon.instance.exit_0 ||
                    Dungeon.instance.matrix.map[x + i, y + 0] == Dungeon.instance.exit_1 ||
                    Dungeon.instance.matrix.map[x + i, y + 0] == Dungeon.instance.exit_2 ||
                    Dungeon.instance.matrix.map[x + i, y + 0] == Dungeon.instance.exit_3 ||

                    Dungeon.instance.matrix.map[x + i, y + 1] == Dungeon.instance.exit_0 ||
                    Dungeon.instance.matrix.map[x + i, y + 1] == Dungeon.instance.exit_1 ||
                    Dungeon.instance.matrix.map[x + i, y + 1] == Dungeon.instance.exit_2 ||
                    Dungeon.instance.matrix.map[x + i, y + 1] == Dungeon.instance.exit_3 ||

                    Dungeon.instance.matrix.map[x + i, y + 2] == Dungeon.instance.exit_0 ||
                    Dungeon.instance.matrix.map[x + i, y + 2] == Dungeon.instance.exit_1 ||
                    Dungeon.instance.matrix.map[x + i, y + 2] == Dungeon.instance.exit_2 ||
                    Dungeon.instance.matrix.map[x + i, y + 2] == Dungeon.instance.exit_3 ||

                    Dungeon.instance.matrix.map[x + i, y + 1] == null ||
                    Dungeon.instance.matrix.map[x + i, y - 0] == null ||
                    Dungeon.instance.matrix.map[x + i, y - 1] == null ||
                    Dungeon.instance.matrix.map[x + i, y - 2] == null) 
                {
                    return false;
                }
            }
            return true;
        }
        private void SettingObj(int x, int y, int rangeX, GameObject setObj)
        {
            for (int i = -rangeX; i <= rangeX; i++)
            {
                if (Dungeon.instance.board.map[x + i, y - 0] == null) Dungeon.instance.board.map[x + i, y - 0] = setObj;
                if (Dungeon.instance.board.map[x + i, y - 0] == null) Dungeon.instance.board.map[x + i, y - 1] = setObj;
                if (Dungeon.instance.board.map[x + i, y - 0] == null) Dungeon.instance.board.map[x + i, y - 2] = setObj;
                if (Dungeon.instance.board.map[x + i, y - 0] == null) Dungeon.instance.board.map[x + i, y - 3] = setObj;
                if (Dungeon.instance.board.map[x + i, y - 0] == null) Dungeon.instance.board.map[x + i, y - 4] = setObj;
                if (Dungeon.instance.board.map[x + i, y - 0] == null) Dungeon.instance.board.map[x + i, y - 5] = setObj;
            }
        }
    }
}
