﻿using MOB;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;
using UnityEngine.UI;

namespace PDG
{
    [System.Serializable]
    public struct Roominfos
    {
        public int x;
        public int y;
        public int width;
        public int height;
        public int roomNum;
        public string rooMType;
        public Vector2[] EntranceWarps;
        public Vector2[] ExitWarps;
        public bool isExitCheck;
        public bool isClear;
        public System.Collections.Generic.List<GameObject> doorObj;
        public System.Collections.Generic.List<GameObject> roomObjs;
        public string roomID;
    }
    [System.Serializable]
    public struct MapPiece
    {
        public string id;
        public GameObject obj;
        public int x;
        public int y;
        public string mapType;
        public Vector2[] doorPos;
        public Vector2[] portalPos;
        public Sprite _minimapSprite;
    }
    [System.Serializable]
    public struct TutorialMap
    {
        public GameObject obj;
        public Vector2 heroSpawnPos;
    }

    [RequireComponent(typeof(Matrix))]
    public class Dungeon : MonoBehaviour
    {
        public static bool isChoiceItem = false;
        public bool isDemo = false;
        public bool isTestMode;
        public bool isTestMode2;
        public bool isTestModeReset;
        public List<BossItemBoxController> TestBoxs = new List<BossItemBoxController>();
        public List<string> TestModeItemLIst = new List<string>();
        public bool isGoblinSpawn = false;
        public bool isMercenaryAjit = false;
        public bool isMercenary = false;
        public bool isMercenarySpawn = false;
        public static int curseHealth;
        public static int curseHealth2;
        public static int curseAttack;
        public static int curseAttack2;

        [SerializeField] private int _curseHealth;
        [SerializeField] private int _curseHealth2;
        [SerializeField] private int _curseAttack;
        [SerializeField] private int _curseAttack2;

        public static bool isRecycle;
        public static bool isPredators;
        public bool newObjectSelected = false;
        public GameObject newObjectSelectedObj = null;
        public float predatorCoolTime = 60.0f;
        public float predatorCoolTimeNow = 60.0f;
        public bool isPredatorCollTime = false;
        public static string headType;
        public static bool isSelectHead = false;
        public string _headType;
        public bool _isSelectHead = false;
        public bool isStartGun = false;
        public int nQChange = 0;

        public bool isEleteObjInstall = false;
        public bool isTotemInstall = false;
        public static bool isRebuildDevice = false;
        public string[] materialTypes;
        public static bool ItemSelected = false;
        public static int prevEliteNum;
        public static GameObject SelecingItem;
        #region Variables
        #region -- Map Variables --
        public string nowRoomType;
        public bool isEndingDoorOpen = false;
        public bool isBuffTower;
        public int nBuffTower = 0;
        public Material LineMat;
        public System.Collections.Generic.List<GameObject> LineObjs = new System.Collections.Generic.List<GameObject>();
        public enum GenerationMode { Horizontal, Vertical }

        public static Dungeon instance;
        public static bool isTutorialClear = false;
        public static bool isEliteSpawn = false;
        public static bool isEliteSpawnWait = false;

        public bool isDungeonComputer = false;
        public bool isDungeonComputer2 = false;
        public bool isDungeonComputer3 = false;
        public bool isDungeonComputer4 = false;
        public bool isDungeonComputer5 = false;
        public int nDungeonComputer5Count = 0;
        public bool isShopFlag = false;
        //public bool isBossRoomEntranceClear = false;
        //public Vector3 vecBossRoomEntrancePos;
        [Header("Map")]
        [SerializeField] [Range(0, 250)] public int width = 45;
        [SerializeField] [Range(0, 250)] public int height = 45;
        [SerializeField] private GenerationMode mode = GenerationMode.Horizontal;
        [SerializeField] private bool isWorldMap = false;
        [SerializeField] private GameObject miniMap;
        [SerializeField] private GameObject worldMap;
        [SerializeField] private UnityEngine.Camera worldMapCamera;
        [SerializeField] private float wheelSpeed = 5.0f;

        [Header("Rooms")]
        [SerializeField] private IntRange numRooms = new IntRange(13, 15);
        [SerializeField] private IntRange roomWidth = new IntRange(3, 7);
        [SerializeField] private IntRange roomHeight = new IntRange(3, 7);

        [Header("StartRoom")]
        [SerializeField] private IntRange startRoomWidth = new IntRange(3, 7);
        [SerializeField] private IntRange startRoomHeight = new IntRange(3, 7);

        [Header("endRoom")]
        [SerializeField] private IntRange endRoomWidth = new IntRange(3, 7);
        [SerializeField] private IntRange endRoomHeight = new IntRange(3, 7);


        [Header("BoxRoom")]
        [SerializeField] private IntRange BoxRoomWidth = new IntRange(3, 7);
        [SerializeField] private IntRange BoxRoomHeight = new IntRange(3, 7);
        [SerializeField] private int BoxRoomCount = 2;

        [Header("Chests")]
        [SerializeField] private int nowChests = 0;
        [SerializeField] private int minChests = 1;
        [SerializeField] private int maxChests = 3;
        [SerializeField] private int nowChests2 = 0;
        [SerializeField] private int minChests2 = 3;
        [SerializeField] private int maxChests2 = 4;
        [SerializeField] private int maxHealth = 6;
        public GameObject chestPrefab;
        public GameObject chestPrefab2;
        public GameObject healthPrefab;

        [Header("Characters")]
        [SerializeField] private GameObject hero;

        [Header("Monster")]
        public string StageName = "Dungeon_0";
        public int StageLevel = 0;
        public int nowStageStep = 0;
        public bool isStageBoss = false;

        [SerializeField] private IntRange MonsterSpawnCount = new IntRange(3, 7);
        public System.Collections.Generic.List<GameObject> SpawnMonsters = new System.Collections.Generic.List<GameObject>();

        [Header("Warp")]
        public bool isSelect = false;
        public Sprite[] WarpImages;

        [Header("Warp Quest")]
        public System.Collections.Generic.List<GameObject> WarpSpawnMonsters = new System.Collections.Generic.List<GameObject>();
        public int maxWarpCount = -1;
        public int curWarpCount = -1;

        [Header("Build")]
        [SerializeField] private GameObject shop;
        public System.Collections.Generic.List<GameObject> BuildSpawn = new System.Collections.Generic.List<GameObject>();

        [Header("Make Rooms")]
        public System.Collections.Generic.List<Roominfos> RoomInfo = new System.Collections.Generic.List<Roominfos>();
        [SerializeField] private System.Collections.Generic.List<GameObject> FogList = new System.Collections.Generic.List<GameObject>();
        [SerializeField] private System.Collections.Generic.List<GameObject> FixedRoomMiniMap = new System.Collections.Generic.List<GameObject>();

        [Header("Loading")]
        public Image LoadingBar;
        public Text LoadingText;
        public GameObject LoadingObj;

        [SerializeField] private int webCount = 1;
        private Coroutine CreateCoroutine = null;
        public Vector2[] ChestPosition;
        public Vector2[] HealthPosition;

        [HideInInspector] public GameObject floor_1;
        [HideInInspector] public GameObject floor_2;
        [HideInInspector] public GameObject wall_mid;
        public GameObject wall_left;
        public GameObject wall_right;
        public GameObject wall_side_front_left;
        public GameObject wall_side_front_right;
        public GameObject wall_top_mid;
        public GameObject wall_corner_top_left;
        public GameObject wall_corner_top_right;
        private GameObject wall_corner_bottom_left;
        private GameObject wall_corner_bottom_right;
        private GameObject wall_corner_bottom_1;
        private GameObject wall_corner_bottom_2;
        [HideInInspector] public GameObject wall_side_mid_left;
        [HideInInspector] public GameObject wall_side_mid_right;
        private GameObject wall_side_top_left;
        private GameObject wall_side_top_right;
        private GameObject wall_column_top;
        private GameObject wall_column_mid;
        [HideInInspector] public GameObject wall_column_base;
        private GameObject fountain_blue;
        private GameObject fountain_red;
        private GameObject exit;
        [HideInInspector] public GameObject exit_0;
        [HideInInspector] public GameObject exit_1;
        [HideInInspector] public GameObject exit_2;
        [HideInInspector] public GameObject exit_3;
        private GameObject start;
        private GameObject end;
        private GameObject door_horizontal;
        public bool isPause;
        private int rooms = 0;
        private int chestsCount = 0;
        private int chestsCount2 = 0;
        private int healthCount = 0;
        private Vector2[] pointsList;

        [Header("Matrix")]
        public Matrix matrix;
        public Matrix board;

        private Transform itemParent;

        [Header("A*")]
        public AstarPath Astar;
        [HideInInspector] public GameObject HeroObj = null;
        #endregion
        [Header("TitleSlogan")]
        public GameObject titleSloganObj;
        public float fadeTime = 1.0f;
        public float UIfadeTime = 0.2f;
        [Header("Option")]
        public Image[] CameraShakerLevelImage;
        public int nCameraShakerLevel;
        public GameObject SettingCanvas;
        public GameObject OptionCanvas;
        public GameObject GamePlayCanvas;
        public GameObject VideoCanvas;
        public GameObject AudioCanvas;
        public GameObject LanguageCanvas;
        public GameObject KeySettingCanvas;
        public int settingTrigger = 0;
        public bool isFullScreenMode = false;
        public bool isVsyncMode = false;
        private Coroutine optionChangeCoroutine = null;
        public Sprite[] crossHairSprites;
        public Image crossHairImage;
        public Image crossHairImage_UI;
        private int nowCrossHairNum = 4;
        private long pushCheck = 0;
        private long pushCheck2 = 0;

        [SerializeField] private Image[] SettingSelectImages;
        [SerializeField] private int nowSettingSelectImageNum;
        [SerializeField] private int optionOcount = 4;
        [SerializeField] private int optionGcount = 3;
        [SerializeField] private int optionVcount = 3;
        [SerializeField] private int optionAcount = 3;
        [SerializeField] private int optionLcount = 3;
        [SerializeField] private int optionKcount = 3;

        [Header("ScreenModeChange")]
        public Sprite _checkSprite;
        public Sprite _unCheckSprite;
        public Image full_Option;
        public Image window_Option;
        public Image vSync_Option;
        public Text ui_FreamText;

        [Header("Item Trigger")]
        public float triggerSkullDropAmount = 0;
        public float triggerKeyDropAmount = 0;

        [Header("Fixed Room")]
        public MapPiece[] _MapPiece;
        StageData[] stageDatas;
        public GameObject AjitObj;
        public GameObject eleteObj;

        [Header("Tutorial")]
        public TutorialMap[] TutorialMap;
        public int nowTutorialNum = 0;
        public bool isUseQ = false;
        public int TutorialCount = 0;
        public GameObject firstEventMap;


        [Header("Teleport")]
        public GameObject TeleportObj;
        public List<GameObject> TeleportList = new List<GameObject>();
        public GameObject teleportLineObj;
        public bool isOpenPortal = false;

        [Header("Waitroom")]
        public Vector2 waitingEntrance;
        public Vector2 waitingExit;
        public Vector2 waitingRoomCenter;

        [Header("Tumblbug")]
        public bool isTumblbug = false;
        public GameObject TumblbugCanvas;
        public GameObject Canvas;
        public bool isTumblbugEventEnd = false;
        public static bool isStageClear = false;
        public Image[] tumblbugImages;
        public Image[] tumblbugImages2;
        public Sprite[] tumblbugSprites;

        [Header("FirstEvent")]
        public SpriteRenderer firstEventNPC = null;
        public GameObject moveTarget;
        public float firstNPCmoveSpeed = 5.0f;
        public int TutorialQCount = 0;
        public bool isEscPause;
        public Image UI_EscPause;

        public float closeCoolTime = 0.0f;
        public GameObject minimapObj;

        public GameObject BatObj;
        public GameObject spawnBat;
        public GameObject TutorialText;

        bool isAjit = false;
        float guidTime = 1.0f;

        public float warpGlobalCooltime = 4.0f;
        public float warpGlobalCooltimeNow = 4.0f;
        [SerializeField] private GameObject QuestionPanel;

        [Header("Question")]
        public GameObject QuestionPanels;
        public Button QuestionYes;
        public Button QuestionNo;
        public Text QuestionText;
        public bool isQuit = false;
        public Text mapName;

        [Header("ColorOption")]
        [SerializeField] private Scrollbar brightScroll;
        [SerializeField] private Scrollbar contrastScroll;
        [SerializeField] private PostProcessVolume pVolume;
        [SerializeField] private ColorGrading pGrading;
        [SerializeField] private Vignette pVignette;
        public static bool LoadingClear = false;
        public System.Collections.Generic.List<GameObject> HpList = new System.Collections.Generic.List<GameObject>();
        [SerializeField] private GameObject EndingObj;

        [Header("Warp")]
        public int warpType;
        public int warpTypeMax = 2;
        public bool isGetWarpPart = false;
        public bool isOpenWarp = false;
        public bool isOpnerSetting = false;
        public bool isOpnePartSetting = false;
        DungeonNextController[] tempNext;

        GameObject[] qPanel;
        public Dictionary<string, int> MaterialList = new Dictionary<string, int>();
        public List<GameObject> publicListObj = new List<GameObject>();
        public List<float> publicListTime = new List<float>();
        public List<GameObject> publicListObj2 = new List<GameObject>();
        public List<float> publicListTime2 = new List<float>();

        [SerializeField] private GameObject[] WorldMapSimbols;
        public List<string> joystricks = new List<string>();
        public bool isJoystrick = false;
        [SerializeField] private Sprite[] joyKeySprites;
        [SerializeField] private bool isChangeKey = false;
        [SerializeField] private int nowKeySettingNum = 0;
        [SerializeField] private GameObject AnyKeyObj;
        [SerializeField] private GameObject JoystickPanel;
        [SerializeField] private GameObject KeyboardPanel;
        [SerializeField] private Text KeyChangeGuideText;

        [SerializeField] private GameObject GoblinPortalObj;
        [SerializeField] private GameObject GolineSpawnObj;

        [SerializeField] private GameObject MercenaryObj;

        [Header("StageSetting")]
        public int maxStage = 4;
        public int prevStartRan = -1;
        public int prevStartRanBoss = -1;
        public int[] stageRan;
        public int[] stageBossRan;
        public Sprite[] stageMarkSprites;
        public Sprite[] bossMarkSprites;
        public RectTransform LoadingCharacter;
        public int stageCount = 2;
        public int bossroomCount = 1;
        [SerializeField] private Transform loadingBarObj;
        public Sprite stageDefaultSprite;
        public List<GameObject> stageStepObj = new List<GameObject>();

        int demoSelect = 0;

        public Coroutine DamageCoroutine = null;
        public bool isBossRush = false;
        public float fBossRushTime = 0.0f;
        public bool isBossRushStart = false;
        [SerializeField] private GameObject AjitPrefabs;
        public List<GameObject> BossRushItemList = new List<GameObject>();

        public bool isSaveMap = false;
        #endregion Variables
        #region Methods
        private void Awake()
        {
            Application.targetFrameRate = 144;
            if (instance == null) instance = this;
            if (instance != this) Destroy(gameObject);

            floor_1 = Resources.Load("Floor/floor_1") as GameObject;
            floor_2 = Resources.Load("Floor/floor_2") as GameObject;
            wall_mid = Resources.Load("Wall/Base/wall_mid") as GameObject;
            wall_left = Resources.Load("Wall/Base/wall_left") as GameObject;
            wall_right = Resources.Load("Wall/Base/wall_right") as GameObject;
            wall_side_front_left = Resources.Load("Wall/Base/wall_side_front_left") as GameObject;
            wall_side_front_right = Resources.Load("Wall/Base/wall_side_front_right") as GameObject;
            wall_top_mid = Resources.Load("Wall/Base/wall_top_mid") as GameObject;
            wall_corner_top_left = Resources.Load("Wall/Base/wall_corner_top_left") as GameObject;
            wall_corner_top_right = Resources.Load("Wall/Base/wall_corner_top_right") as GameObject;
            wall_corner_bottom_left = Resources.Load("Wall/Base/wall_corner_bottom_left") as GameObject;
            wall_corner_bottom_right = Resources.Load("Wall/Base/wall_corner_bottom_right") as GameObject;
            wall_corner_bottom_1 = Resources.Load("Wall/Base/wall_corner_bottom_1") as GameObject;
            wall_corner_bottom_2 = Resources.Load("Wall/Base/wall_corner_bottom_2") as GameObject;
            wall_side_mid_left = Resources.Load("Wall/Base/wall_side_mid_left") as GameObject;
            wall_side_mid_right = Resources.Load("Wall/Base/wall_side_mid_right") as GameObject;
            wall_side_top_left = Resources.Load("Wall/Base/wall_side_top_left") as GameObject;
            wall_side_top_right = Resources.Load("Wall/Base/wall_side_top_right") as GameObject;
            wall_column_top = Resources.Load("Wall/Decoration/Column/wall_column_top") as GameObject;
            wall_column_mid = Resources.Load("Wall/Decoration/Column/wall_column_mid") as GameObject;
            wall_column_base = Resources.Load("Wall/Decoration/Column/wall_column_base") as GameObject;
            fountain_blue = Resources.Load("Wall/Decoration/Fountains/fountain_blue") as GameObject;
            fountain_red = Resources.Load("Wall/Decoration/Fountains/fountain_red") as GameObject;
            exit = Resources.Load("Floor/exit") as GameObject;
            exit_0 = Resources.Load("Floor/exit_0") as GameObject;
            exit_1 = Resources.Load("Floor/exit_1") as GameObject;
            exit_2 = Resources.Load("Floor/exit_2") as GameObject;
            exit_3 = Resources.Load("Floor/exit_3") as GameObject;
            start = Resources.Load("Floor/start") as GameObject;
            end = Resources.Load("Floor/end") as GameObject;
            door_horizontal = Resources.Load("Floor/door_horizontal") as GameObject;
        }
        private void Start()
        {
            qPanel = new GameObject[3];
            qPanel[0] = GameObject.Find("Canvas").transform.Find("QuestionPanel").gameObject;
            qPanel[1] = GameObject.Find("Canvas").transform.Find("QuestionPanel2").gameObject;
            qPanel[2] = GameObject.Find("Canvas").transform.Find("QuestionPanel3").gameObject;

            stageRan = new int[maxStage];
            stageBossRan = new int[maxStage];
            for (int i = 0; i < stageRan.Length; i++)
            {
                while (true)
                {
                    stageRan[i] = Random.Range(0, stageRan.Length + 1);
                    if (i == 0 && prevStartRan != -1 && prevStartRan == stageRan[i])
                    {
                        continue;
                    }
                    if (i == 0) prevStartRan = stageRan[i];
                    bool c = false;
                    for (int j = 0; j < i; j++)
                    {
                        if (stageRan[i] == stageRan[j]) c = true;
                    }
                    if (!c) break;
                }
            }
            for (int i = 0; i < stageBossRan.Length; i++)
            {
                while (true)
                {
                    stageBossRan[i] = Random.Range(0, stageBossRan.Length);
                    if (i == 0 && prevStartRanBoss != -1 && prevStartRanBoss == stageBossRan[i])
                    {
                        continue;
                    }
                    if (i == 0) prevStartRanBoss = stageBossRan[i];
                    bool c = false;
                    for (int j = 0; j < i; j++)
                    {
                        if (stageBossRan[i] == stageBossRan[j]) c = true;
                    }
                    if (!c) break;
                }
            }

            float t_maxStage = (4 * stageCount) + (4 * bossroomCount);
            int t_count = 0;
            for (int total_stage = 0; total_stage < 4; total_stage++)
            {
                for (int i = 0; i < stageCount; i++)
                {
                    GameObject obj = new GameObject();
                    obj.name = "Stage_" + total_stage + "_normal" + i;
                    Image tImage = obj.AddComponent<Image>();
                    tImage.sprite = stageDefaultSprite;
                    obj.transform.SetParent(loadingBarObj);
                    obj.transform.localScale = Vector3.one;
                    obj.GetComponent<RectTransform>().anchoredPosition3D = new Vector3(((1440 / t_maxStage) * t_count) - 720, 0, 0);
                    obj.GetComponent<RectTransform>().sizeDelta = new Vector2(52, 52);
                    stageStepObj.Add(obj);
                    t_count++;
                }
                for (int i = 0; i < bossroomCount; i++)
                {
                    GameObject obj = new GameObject();
                    obj.name = "Stage_" + total_stage + "_boss" + i;
                    Image tImage = obj.AddComponent<Image>();
                    tImage.sprite = stageDefaultSprite;
                    obj.transform.SetParent(loadingBarObj);
                    obj.transform.localScale = Vector3.one;
                    obj.GetComponent<RectTransform>().anchoredPosition3D = new Vector3(((1440 / t_maxStage) * t_count) - 720, 0, 0);
                    obj.GetComponent<RectTransform>().sizeDelta = new Vector2(78, 78);
                    stageStepObj.Add(obj);
                    t_count++;
                }
            }

            VolumeSave[] v = SaveAndLoadManager.instance.LoadVolume();
            if (v != null)
            {
                for (int i = 0; i < v.Length; i++)
                {
                    if (v[i]._type == 5) isFullScreenMode = (v[i]._value == 1 ? true : false);
                    else if (v[i]._type == 6) isTutorialClear = (v[i]._value == 1 ? true : false);
                    else if (v[i]._type == 7) isVsyncMode = (v[i]._value == 1 ? true : false);
                }
            }
            int camShakerLevel = SaveAndLoadManager.instance.LoadItem("OPTION_CAMERA_SHAKER");
            if (camShakerLevel == -1) nCameraShakerLevel = 0;
            else nCameraShakerLevel = camShakerLevel;
            for (int i = 0; i < CameraShakerLevelImage.Length; i++)
            {
                if (i == nCameraShakerLevel) CameraShakerLevelImage[i].sprite = _checkSprite;
                else CameraShakerLevelImage[i].sprite = _unCheckSprite;
            }
            if (isFullScreenMode)
            {
                full_Option.sprite = _checkSprite;
                window_Option.sprite = _unCheckSprite;
            }
            else
            {
                full_Option.sprite = _unCheckSprite;
                window_Option.sprite = _checkSprite;
            }
            if (isVsyncMode)
            {
                QualitySettings.vSyncCount = 1;
                vSync_Option.sprite = _checkSprite;
            }
            else
            {
                QualitySettings.vSyncCount = 0;
                vSync_Option.sprite = _unCheckSprite;
            }
            SettingCanvas = GameObject.Find("Canvas").transform.Find("Pause").gameObject;
            isRecycle = SaveAndLoadManager.instance.LoadUnlock("recycle") == 0 ? false : true;
            isPredators =/* true; //*/SaveAndLoadManager.instance.LoadUnlock("predators") == 0 ? false : true;
            if (isPredators)
            {
                if (Inventorys.Instance != null)
                    Inventorys.Instance.predatorObj.SetActive(true);
                else
                    GameObject.Find("Canvas").transform.Find("UI").Find("SlotPanel").Find("Predator").gameObject.SetActive(true);
            }
            nowCrossHairNum = SaveAndLoadManager.instance.LoadItem("UI_CrossHair");
            if (nowCrossHairNum != -1)
            {
                crossHairImage.sprite = crossHairSprites[nowCrossHairNum];
                crossHairImage_UI.sprite = crossHairSprites[nowCrossHairNum];
            }
            else
            {
                nowCrossHairNum = 0;
                crossHairImage.sprite = crossHairSprites[0];
                crossHairImage_UI.sprite = crossHairSprites[0];
            }

            int t = SaveAndLoadManager.instance.LoadItem("ESC_PAUSE");
            if (t == -1)
                isEscPause = true;
            else
                isEscPause = System.Convert.ToBoolean(t);

            int loadBright = SaveAndLoadManager.instance.LoadItem("UI_BRIGHT");
            int loadContrast = SaveAndLoadManager.instance.LoadItem("UI_CONTRAST");
            int loadFream = SaveAndLoadManager.instance.LoadItem("FREAM_RATE");
            if (loadFream == -1)
            {
                loadFream = 60;
                SaveAndLoadManager.instance.SaveItem(new ItemSave { _id = "FREAM_RATE", _nCount = 60 }, true);
            }
            Application.targetFrameRate = loadFream;
            float vvv = ((float)loadFream - 30f) / 90f;
            ui_FreamText.transform.parent.transform.Find("SelectedBar_Fream").Find("Scrollbar").GetComponent<Scrollbar>().value = vvv;
            if (isVsyncMode)
            {
                ui_FreamText.text = LocalizeManager.GetLocalize("ui_fream") + "\n" + LocalizeManager.GetLocalize("ui_vsync_use");
            }
            else
            {
                ui_FreamText.text = LocalizeManager.GetLocalize("ui_fream") + "\n(" + loadFream + "FPS)";
            }
            pVolume.profile.TryGetSettings(out pGrading);
            pVolume.profile.TryGetSettings(out pVignette);
            pVignette.smoothness.overrideState = true;
            pVignette.smoothness.value = 1.0f;
            if (loadBright == -1)
            {
                brightScroll.value = 0.5f;
                pGrading.brightness.value = 0;
            }
            else
            {
                pGrading.brightness.value = loadBright;
                float loadBright_2 = (float)loadBright / 100.0f / 2;
                loadBright_2 += 0.5f;
                brightScroll.value = loadBright_2;
            }

            if (loadContrast == -1)
            {
                contrastScroll.value = 0.5f;
                pGrading.contrast.value = 0;
            }
            else
            {
                pGrading.contrast.value = loadContrast;
                float loadContrast_2 = (float)loadContrast / 100.0f / 2;
                loadContrast_2 += 0.5f;
                contrastScroll.value = loadContrast_2;
            }
            if (isEscPause) UI_EscPause.sprite = _checkSprite;
            else UI_EscPause.sprite = _unCheckSprite;

            if (!SaveAndLoadManager.instance.LoadNPC("Mercenary_fighter"))
            {
                if (SteamStatsAndAchievements.instance != null)
                {
                    int deathCount = SteamStatsAndAchievements.instance.GettingStat("stat_death_total_count");
                    if ((deathCount > 0) && GetPercentege(0.8f) != -1)
                    {
                        isMercenary = true;
                    }
                    else
                    {
                        isMercenary = false;
                    }
                }
                else
                {
                    isMercenary = false;
                }
            }
            else
            {
                isMercenary = false;
            }

            if (isTutorialClear)
            {
                firstEventMap.SetActive(false);
                GoAjit();
            }
            else
            {
                GoTutorialFirstEvent();
            }
        }
        private void Update()
        {
            _curseHealth = curseHealth;
            _curseHealth2 = curseHealth2;
            _curseAttack = curseAttack;
            _curseAttack2 = curseAttack2;
            if (isPredators)
            {
                if (Inventorys.Instance != null)
                {
                    if (!Inventorys.Instance.predatorObj.activeSelf)
                        Inventorys.Instance.predatorObj.SetActive(true);
                }
                else
                {
                    if (!GameObject.Find("Canvas").transform.Find("UI").Find("SlotPanel").Find("Predator").gameObject.activeSelf)
                        GameObject.Find("Canvas").transform.Find("UI").Find("SlotPanel").Find("Predator").gameObject.SetActive(true);
                }
            }
            joystricks = Input.GetJoystickNames().ToList();
            for (int i = 0; i < joystricks.Count; i++)
            {
                if (joystricks[i].Equals(string.Empty))
                {
                    joystricks.RemoveAt(i);
                    i--;
                }
            }

            //컨트롤러 전환
            GetKeyDownCheck((keydown) =>
            {
                if (keydown)
                {
                    //키보드로 전환
                    isJoystrick = false;

                    JoystickPanel.SetActive(false);
                    KeyboardPanel.SetActive(true);
                }
                else
                {
                    //조이스틱 전환
                    if (joystricks.Count > 0)
                    {
                        isJoystrick = true;

                        JoystickPanel.SetActive(true);
                        KeyboardPanel.SetActive(false);
                    }
                }
            });
            //if (joystricks.Count > 0) isJoystrick = true;
            //else isJoystrick = false;
            //if (!isJoystrick)
            //{
            //    JoystickPanel.SetActive(false);
            //    KeyboardPanel.SetActive(true);
            //}
            //else
            //{
            //    JoystickPanel.SetActive(true);
            //    KeyboardPanel.SetActive(false);
            //}

            if (isMercenaryAjit)
            {
                if (nowRoomType.Equals("normal"))
                {
                    if (MercenaryObj)
                    {
                        if (!MercenaryObj.activeSelf)
                        {
                            MercenaryObj.transform.position = Player.instance.transform.position;
                            MercenaryObj.SetActive(true);
                        }
                    }
                }
                else
                {
                    if (MercenaryObj) MercenaryObj.SetActive(false);
                }
            }

            float x = Input.GetAxisRaw("Horizontal_joy");
            float y = Input.GetAxisRaw("Vertical_joy");

            _headType = headType;
            _isSelectHead = isSelectHead;
            Cursor.lockState = CursorLockMode.Confined;
            if (GameObject.Find("Canvas") && GameObject.Find("Canvas").transform.Find("ExitArrow") && GameObject.Find("Canvas").transform.Find("ExitArrow").gameObject.activeSelf && !isTestMode2)
            {
                if (tempNext == null) tempNext = FindObjectsOfType(typeof(DungeonNextController)) as DungeonNextController[];
                if (tempNext != null && tempNext.Length > 0)
                {
                    float dis = float.MaxValue;
                    int tInt = int.MinValue;
                    for (int i = 0; i < tempNext.Length; i++)
                    {
                        float dis2 = Vector3.Distance(tempNext[i].transform.position, PDG.Player.instance.transform.position);
                        if (dis2 < dis)
                        {
                            tInt = i;
                            dis = dis2;
                        }
                    }
                    float angle = Player.GetAngle(Player.instance.transform.position, tempNext[tInt].transform.position);
                    GameObject.Find("Canvas").transform.Find("ExitArrow").transform.eulerAngles = new Vector3(0, 0, angle);
                    Vector3 p = Player.instance.transform.position;
                    p.y += 1.2f;
                    GameObject.Find("Canvas").transform.Find("ExitArrow").transform.position = UnityEngine.Camera.main.WorldToScreenPoint(p);
                }
            }
            if (isPredatorCollTime)
            {
                int c = Mathf.FloorToInt(predatorCoolTime - predatorCoolTimeNow);
                if (c > 0) Inventorys.Instance.CollTimeText_Predator.text = c.ToString();
                else Inventorys.Instance.CollTimeText_Predator.text = string.Empty;
                predatorCoolTimeNow += Time.deltaTime;
                if (predatorCoolTimeNow >= predatorCoolTime)
                {
                    isPredatorCollTime = false;
                    Inventorys.Instance.predatorCollTimeImage.transform.parent.Find("CollTimeImageP").gameObject.SetActive(false);
                    Inventorys.Instance.predatorCollTimeAnim.Rebind();
                }
                Inventorys.Instance.predatorCollTimeImage.fillAmount = 1.0f - (predatorCoolTimeNow / predatorCoolTime);
            }
            warpGlobalCooltimeNow += Time.deltaTime;
            closeCoolTime += Time.unscaledDeltaTime;

            if (isPause || isTumblbug)
            {
                // Cursor visible
                Cursor.visible = true;
            }
            else
            {
                // Cursor visible
                Cursor.visible = false;
            }

            if (!isTutorialClear)
            {
                crossHairImage.transform.position = Input.mousePosition;
            }

            for (int i = 0; i < publicListObj.Count; i++)
            {
                if (publicListObj[i] == null)
                {
                    publicListObj.RemoveAt(i);
                    publicListTime.RemoveAt(i);
                    i--;
                    continue;
                }
                else if (publicListObj[i].GetComponent<MonsterArggro>())
                {
                    if (publicListObj[i].GetComponent<MonsterArggro>()._monster.isDie)
                    {
                        publicListObj.RemoveAt(i);
                        publicListTime.RemoveAt(i);
                        i--;
                        continue;
                    }
                }
                else if (publicListObj[i].GetComponent<Monster>())
                {
                    if (publicListObj[i].GetComponent<Monster>().isDie)
                    {
                        publicListObj.RemoveAt(i);
                        publicListTime.RemoveAt(i);
                        i--;
                        continue;
                    }
                }
                publicListTime[i] += Time.fixedDeltaTime;
            }


            for (int i = 0; i < publicListObj2.Count; i++)
            {
                if (publicListObj2[i] == null)
                {
                    publicListObj2.RemoveAt(i);
                    publicListTime2.RemoveAt(i);
                    i--;
                    continue;
                }
                else if (publicListObj2[i].GetComponent<MonsterArggro>())
                {
                    if (publicListObj2[i].GetComponent<MonsterArggro>()._monster.isDie)
                    {
                        publicListObj2.RemoveAt(i);
                        publicListTime2.RemoveAt(i);
                        i--;
                        continue;
                    }
                }
                else if (publicListObj2[i].GetComponent<Monster>())
                {
                    if (publicListObj2[i].GetComponent<Monster>().isDie)
                    {
                        publicListObj2.RemoveAt(i);
                        publicListTime2.RemoveAt(i);
                        i--;
                        continue;
                    }
                }
                publicListTime2[i] += Time.fixedDeltaTime;
            }
            for (int i = 0; i < qPanel.Length; i++)
            {
                if (qPanel[i].activeSelf)
                {
                    if (Player.instance != null) Player.instance.isCameraOff = true;
                }
            }

            if (TutorialQCount == 4 && !isUseQ)
            {
                if (!isUseQ) Dialogue.Instance.CommnetSetting("tutorial_third_map_npc_2", 0, null);
                isUseQ = true;
                GameObject.Find("Canvas").transform.Find("TutorialText").gameObject.SetActive(false);
            }

            if (isChangeKey)
            {
                if (isJoystrick)
                {
                    if (joystricks[0].Contains("DS4") || joystricks[0].Contains("PC") || joystricks[0].Contains("Wireless"))
                    {
                        float joyCrossX = Input.GetAxisRaw("PS4_crossX");
                        float joyCrossY = Input.GetAxisRaw("PS4_crossY");
                        if (joyCrossY > 0 && closeCoolTime > 0.2f)
                        {
                            if (SaveAndLoadManager.instance.SaveKey(KeyCode.Joystick6Button0, nowKeySettingNum, false))
                                KeyChangeGuideText.text = "";
                            else
                                KeyChangeGuideText.text = LocalizeManager.GetLocalize("ui_keysetting_overlabkey");
                            closeCoolTime = 0.0f;
                            isChangeKey = false;
                            AnyKeyObj.SetActive(false);
                            return;
                        }
                        if (joyCrossY < 0 && closeCoolTime > 0.2f)
                        {
                            if (SaveAndLoadManager.instance.SaveKey(KeyCode.Joystick6Button1, nowKeySettingNum, false))
                                KeyChangeGuideText.text = "";
                            else
                                KeyChangeGuideText.text = LocalizeManager.GetLocalize("ui_keysetting_overlabkey");
                            closeCoolTime = 0.0f;
                            isChangeKey = false;
                            AnyKeyObj.SetActive(false);
                            return;
                        }
                        if (joyCrossX > 0 && closeCoolTime > 0.2f)
                        {
                            if (SaveAndLoadManager.instance.SaveKey(KeyCode.Joystick6Button3, nowKeySettingNum, false))
                                KeyChangeGuideText.text = "";
                            else
                                KeyChangeGuideText.text = LocalizeManager.GetLocalize("ui_keysetting_overlabkey");
                            closeCoolTime = 0.0f;
                            isChangeKey = false;
                            AnyKeyObj.SetActive(false);
                            return;
                        }
                        if (joyCrossX < 0 && closeCoolTime > 0.2f)
                        {
                            if (SaveAndLoadManager.instance.SaveKey(KeyCode.Joystick6Button4, nowKeySettingNum, false))
                                KeyChangeGuideText.text = "";
                            else
                                KeyChangeGuideText.text = LocalizeManager.GetLocalize("ui_keysetting_overlabkey");
                            closeCoolTime = 0.0f;
                            isChangeKey = false;
                            AnyKeyObj.SetActive(false);
                            return;
                        }
                    }
                    else
                    {
                        float joyCrossX = Input.GetAxisRaw("Horizontal_cross");
                        float joyCrossY = Input.GetAxisRaw("Vertical_cross");
                        float JoyStrickTrigger = Input.GetAxisRaw("JoyStrickTrigger");

                        if (joyCrossY > 0 && closeCoolTime > 0.2f)
                        {
                            if (SaveAndLoadManager.instance.SaveKey(KeyCode.Joystick6Button0, nowKeySettingNum, false))
                                KeyChangeGuideText.text = "";
                            else
                                KeyChangeGuideText.text = LocalizeManager.GetLocalize("ui_keysetting_overlabkey");
                            closeCoolTime = 0.0f;
                            isChangeKey = false;
                            AnyKeyObj.SetActive(false);
                            return;
                        }
                        if (joyCrossY < 0 && closeCoolTime > 0.2f)
                        {
                            if (SaveAndLoadManager.instance.SaveKey(KeyCode.Joystick6Button1, nowKeySettingNum, false))
                                KeyChangeGuideText.text = "";
                            else
                                KeyChangeGuideText.text = LocalizeManager.GetLocalize("ui_keysetting_overlabkey");
                            closeCoolTime = 0.0f;
                            isChangeKey = false;
                            AnyKeyObj.SetActive(false);
                            return;
                        }
                        if (joyCrossX > 0 && closeCoolTime > 0.2f)
                        {
                            if (SaveAndLoadManager.instance.SaveKey(KeyCode.Joystick6Button3, nowKeySettingNum, false))
                                KeyChangeGuideText.text = "";
                            else
                                KeyChangeGuideText.text = LocalizeManager.GetLocalize("ui_keysetting_overlabkey");
                            closeCoolTime = 0.0f;
                            isChangeKey = false;
                            AnyKeyObj.SetActive(false);
                            return;
                        }
                        if (joyCrossX < 0 && closeCoolTime > 0.2f)
                        {
                            if (SaveAndLoadManager.instance.SaveKey(KeyCode.Joystick6Button4, nowKeySettingNum, false))
                                KeyChangeGuideText.text = "";
                            else
                                KeyChangeGuideText.text = LocalizeManager.GetLocalize("ui_keysetting_overlabkey");
                            closeCoolTime = 0.0f;
                            isChangeKey = false;
                            AnyKeyObj.SetActive(false);
                            return;
                        }
                        if (JoyStrickTrigger > 0 && closeCoolTime > 0.2f)
                        {
                            if (SaveAndLoadManager.instance.SaveKey(KeyCode.Joystick7Button0, nowKeySettingNum, false))
                                KeyChangeGuideText.text = "";
                            else
                                KeyChangeGuideText.text = LocalizeManager.GetLocalize("ui_keysetting_overlabkey");
                            closeCoolTime = 0.0f;
                            isChangeKey = false;
                            AnyKeyObj.SetActive(false);
                            return;
                        }
                        if (JoyStrickTrigger < 0 && closeCoolTime > 0.2f)
                        {
                            if (SaveAndLoadManager.instance.SaveKey(KeyCode.Joystick7Button1, nowKeySettingNum, false))
                                KeyChangeGuideText.text = "";
                            else
                                KeyChangeGuideText.text = LocalizeManager.GetLocalize("ui_keysetting_overlabkey");
                            closeCoolTime = 0.0f;
                            isChangeKey = false;
                            AnyKeyObj.SetActive(false);
                            return;
                        }
                    }
                    if (Input.anyKeyDown && closeCoolTime > 0.2f)
                    {
                        foreach (KeyCode _key in (KeyCode[])System.Enum.GetValues(typeof(KeyCode)))
                        {
                            if (_key.ToString().Contains("Joystick1")) continue;
                            if (_key.ToString().Contains("Joystick2")) continue;
                            if (_key.ToString().Contains("Joystick3")) continue;
                            if (_key.ToString().Contains("Joystick4")) continue;
                            if (_key.ToString().Contains("Joystick5")) continue;
                            if (Input.GetKeyDown(_key))
                            {
                                if (CheckingKeyChange(_key))
                                {
                                    if ((joystricks[0].Contains("DS4") || joystricks[0].Contains("PC") || joystricks[0].Contains("Wireless")) && _key == KeyCode.JoystickButton6)
                                    {
                                        if (SaveAndLoadManager.instance.SaveKey(KeyCode.Joystick7Button1, nowKeySettingNum, false))
                                            KeyChangeGuideText.text = "";
                                        else
                                            KeyChangeGuideText.text = LocalizeManager.GetLocalize("ui_keysetting_overlabkey");
                                    }
                                    else if ((joystricks[0].Contains("DS4") || joystricks[0].Contains("PC") || joystricks[0].Contains("Wireless")) && _key == KeyCode.JoystickButton7)
                                    {
                                        if (SaveAndLoadManager.instance.SaveKey(KeyCode.Joystick7Button0, nowKeySettingNum, false))
                                            KeyChangeGuideText.text = "";
                                        else
                                            KeyChangeGuideText.text = LocalizeManager.GetLocalize("ui_keysetting_overlabkey");
                                    }
                                    else
                                    {
                                        if (SaveAndLoadManager.instance.SaveKey(_key, nowKeySettingNum, false))
                                            KeyChangeGuideText.text = "";
                                        else
                                            KeyChangeGuideText.text = LocalizeManager.GetLocalize("ui_keysetting_overlabkey");
                                    }

                                    closeCoolTime = 0.0f;
                                    isChangeKey = false;
                                    AnyKeyObj.SetActive(false);
                                }
                                else
                                {
                                    closeCoolTime = 0.0f;
                                    KeyChangeGuideText.color = Color.red;
                                    KeyChangeGuideText.text = LocalizeManager.GetLocalize("ui_keysetting_unusablekey");
                                }
                            }
                        }
                    }
                }
                else
                {
                    if (Input.anyKeyDown && closeCoolTime > 0.2f)
                    {
                        foreach (KeyCode _key in (KeyCode[])System.Enum.GetValues(typeof(KeyCode)))
                        {
                            if (Input.GetKeyDown(KeyCode.Escape))
                            {
                                closeCoolTime = 0.0f;
                                isChangeKey = false;
                                AnyKeyObj.SetActive(false);
                                return;
                            }
                            if (Input.GetKeyDown(_key))
                            {
                                if (CheckingKeyChange(_key))
                                {
                                    if (SaveAndLoadManager.instance.SaveKey(_key, nowKeySettingNum, true))
                                        KeyChangeGuideText.text = "";
                                    else
                                        KeyChangeGuideText.text = LocalizeManager.GetLocalize("ui_keysetting_overlabkey");
                                    closeCoolTime = 0.0f;
                                    isChangeKey = false;
                                    AnyKeyObj.SetActive(false);
                                }
                                else
                                {
                                    closeCoolTime = 0.0f;
                                    KeyChangeGuideText.color = Color.red;
                                    KeyChangeGuideText.text = LocalizeManager.GetLocalize("ui_keysetting_unusablekey");
                                }
                            }
                        }
                    }
                }
                return;
            }


            if (Inventorys.Instance.isGuid)
            {
                guidTime -= Time.unscaledDeltaTime;
                if ((PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ACTIVE], true) || JoyStickKeyInputCheck(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_CONFIRM], true)) && guidTime <= 0.0f)
                {
                    SoundManager.instance.StartAudio(new string[1] { "pressanykey" }, VOLUME_TYPE.EFFECT);
                    SaveAndLoadManager.instance.SaveItem(new ItemSave { _id = Inventorys.Instance.ItemGuideObj.transform.Find("Text").GetComponent<SetLanguage>()._id, _nCount = 1 }, true);
                    Inventorys.Instance.isGuid = false;
                    Inventorys.Instance.ItemGuideObj.SetActive(false);
                    Time.timeScale = 1;
                }
            }
            if ((PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_MAP], true) || JoyStickKeyInputCheck(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_MAP], true)) && !isPause && !Inventorys.Instance.isGuid && !isOpenPortal && closeCoolTime > 0.2f)
            {
                closeCoolTime = 0.0f;
                GameObject.Find("Camera").transform.Find("WorldMap").transform.localPosition = Vector3.zero;
                SoundManager.instance.StartAudio(new string[1] { "minimap_open2" }, VOLUME_TYPE.EFFECT);
                WorldMapOnOff();
            }

            if ((Input.GetKeyDown(KeyCode.Escape) || Input.GetKeyDown(KeyCode.Backspace) ||
                (isJoystrick ? (joystricks[0].Contains("PC") || joystricks[0].Contains("DS4") || joystricks[0].Contains("Wireless") ? Input.GetKeyDown(KeyCode.JoystickButton9) : Input.GetKeyDown(KeyCode.JoystickButton7)) : false)
                || (isWorldMap ? JoyStickKeyInputCheck(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_CANCLE], true) : false))
                 && !StateUpgradeController.instance.isOn && closeCoolTime > 0.2f && !isOpenPortal && settingTrigger == 0 && !isTumblbug && !QuestionPanel.activeSelf && !QuestionPanels.activeSelf
                 && (Dialogue.Instance == null ? true : !Dialogue.Instance.isCommnet) && !Inventorys.Instance.isGuid
                 && (UnLock.Instance == null ? true : !UnLock.Instance.isOpen)
                 && (Player.instance == null ? true : !Player.instance.isDie)
                 && (ReinforceController.instance == null ? true : !ReinforceController.instance.isOpen)
                 && (CustomManager.instance == null ? true : !CustomManager.instance.isOpen)
                 && (MakeController.instance == null ? true : !MakeController.instance.isOpen)
                 && (SafeManager.instance == null ? true : !SafeManager.instance.isOpen)
                 && (SandMailManager.instance == null ? true : !SandMailManager.instance.isOpen)
                 && (CollectionManager.instance == null ? true : !CollectionManager.instance.isOpen)
                 && (MemorialManager.instance == null ? true : !MemorialManager.instance.isOpen)
                 && (DifficultyManager.instance == null ? true : !DifficultyManager.instance.isOpen)
                 && (BossRushListManager.instance == null ? true : !BossRushListManager.instance.isOpen)
                 && (StateGroup.instance == null ? true : !StateGroup.instance.isOpen)
                 && (EquipmentUpgradeManager.instance == null ? true : !EquipmentUpgradeManager.instance.isOpen)
                 && !(GameObject.Find("Canvas").transform.Find("Timer").gameObject.activeSelf)
                 && (BodyRemodelingManager.instance == null ? true : !BodyRemodelingManager.instance.isOpen))
            {
                closeCoolTime = 0.0f;
                if (!isWorldMap)
                {
                    for (int i = 0; i < tumblbugImages2.Length; i++)
                    {
                        tumblbugImages2[i].GetComponent<Button>().onClick.RemoveAllListeners();
                        int c = i;
                        tumblbugImages2[i].GetComponent<Button>().onClick.AddListener(() => { TumblbugButton(c); });
                    }
                    OpenPause();
                }
                else
                {
                    SoundManager.instance.StartAudio(new string[1] { "ui_esc" }, VOLUME_TYPE.EFFECT);
                    WorldMapOnOff();
                }
            }
            else if ((Input.GetKeyDown(KeyCode.Escape) || Input.GetKeyDown(KeyCode.Backspace) || JoyStickKeyInputCheck(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_CANCLE], true))
                && isPause && !isTumblbug && !QuestionPanel.activeSelf && !Inventorys.Instance.isGuid && !QuestionPanels.activeSelf && closeCoolTime > 0.2f) // 옵션중ESC
            {
                closeCoolTime = 0.0f;
                if (settingTrigger == 1) // Puase 창 닫기
                {
                    SoundManager.instance.StartAudio(new string[1] { "ui_esc" }, VOLUME_TYPE.EFFECT);
                    isPause = !isPause;
                    Canvas.transform.Find("UI").gameObject.SetActive(!isPause);
                    SettingCanvas.SetActive(isPause);
                    Time.timeScale = (isPause ? 0 : 1);
                    settingTrigger = 0;
                    OptionCanvas.SetActive(false);
                    GamePlayCanvas.SetActive(false);
                    VideoCanvas.SetActive(false);
                    AudioCanvas.SetActive(false);
                    LanguageCanvas.SetActive(false);
                    KeySettingCanvas.SetActive(false);
                }
                else if (settingTrigger == 2) // 옵션창 닫기
                {
                    SoundManager.instance.StartAudio(new string[1] { "ui_esc" }, VOLUME_TYPE.EFFECT);
                    for (int i = 0; i < SettingSelectImages.Length; i++)
                    {
                        SettingSelectImages[i].enabled = false;
                    }
                    OptionCanvas.SetActive(false);
                    settingTrigger = 1;
                    SettingSafe();
                }
                else if (settingTrigger == 3) // 옵션 세부 내용 닫기
                {
                    SoundManager.instance.StartAudio(new string[1] { "ui_esc" }, VOLUME_TYPE.EFFECT);
                    for (int i = 0; i < SettingSelectImages.Length; i++)
                    {
                        SettingSelectImages[i].enabled = false;
                    }
                    settingTrigger = 2;
                    GamePlayCanvas.SetActive(false);
                    nowSettingSelectImageNum = 0;
                    SelectBarChange(SettingSelectImages[nowSettingSelectImageNum]);
                    SettingSafe();
                }
                else if (settingTrigger == 4) // 옵션 세부 내용 닫기
                {
                    SoundManager.instance.StartAudio(new string[1] { "ui_esc" }, VOLUME_TYPE.EFFECT);
                    for (int i = 0; i < SettingSelectImages.Length; i++)
                    {
                        SettingSelectImages[i].enabled = false;
                    }
                    settingTrigger = 2;
                    VideoCanvas.SetActive(false);
                    nowSettingSelectImageNum = 1;
                    SelectBarChange(SettingSelectImages[nowSettingSelectImageNum]);
                    SettingSafe();
                }
                else if (settingTrigger == 5) // 옵션 세부 내용 닫기
                {
                    SoundManager.instance.StartAudio(new string[1] { "ui_esc" }, VOLUME_TYPE.EFFECT);
                    for (int i = 0; i < SettingSelectImages.Length; i++)
                    {
                        SettingSelectImages[i].enabled = false;
                    }
                    settingTrigger = 2;
                    AudioCanvas.SetActive(false);
                    nowSettingSelectImageNum = 2;
                    SelectBarChange(SettingSelectImages[nowSettingSelectImageNum]);
                    SettingSafe();
                }
                else if (settingTrigger == 6) // 옵션 세부 내용 닫기
                {
                    SoundManager.instance.StartAudio(new string[1] { "ui_esc" }, VOLUME_TYPE.EFFECT);
                    for (int i = 0; i < SettingSelectImages.Length; i++)
                    {
                        SettingSelectImages[i].enabled = false;
                    }
                    settingTrigger = 2;
                    LanguageCanvas.SetActive(false);
                    nowSettingSelectImageNum = 3;
                    SelectBarChange(SettingSelectImages[nowSettingSelectImageNum]);
                    SettingSafe();
                }
                else if (settingTrigger == 7) // 옵션 세부 내용 닫기
                {
                    SoundManager.instance.StartAudio(new string[1] { "ui_esc" }, VOLUME_TYPE.EFFECT);
                    for (int i = 0; i < SettingSelectImages.Length; i++)
                    {
                        SettingSelectImages[i].enabled = false;
                    }
                    settingTrigger = 2;
                    KeySettingCanvas.SetActive(false);
                    nowSettingSelectImageNum = 4;
                    SelectBarChange(SettingSelectImages[nowSettingSelectImageNum]);
                    SettingSafe();
                }
            }
            else if ((Input.GetKeyDown(KeyCode.Escape) || Input.GetKeyDown(KeyCode.Backspace) || JoyStickKeyInputCheck(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_CANCLE], true))
                && !Inventorys.Instance.isGuid && QuestionPanels.activeSelf && closeCoolTime > 0.2f)
            {
                closeCoolTime = 0.0f;
                PDG.Player.instance.isCameraOff = false;
                QuestionPanels.SetActive(false);
            }
            if ((Input.GetKeyDown(KeyCode.Return) || PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ACTIVE], true) || JoyStickKeyInputCheck(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_CONFIRM], true))
                && !Inventorys.Instance.isGuid && QuestionPanels.activeSelf && closeCoolTime > 0.2f)
            {
                closeCoolTime = 0.0f;
                if (isQuit)
                {
                    PDG.Player.instance.isCameraOff = false;
                    SoundManager.instance.StartAudio(new string[1] { "Click_0" }, VOLUME_TYPE.EFFECT);
                    Application.Quit();
                }
                else
                {
                    PDG.Player.instance.isCameraOff = false;
                    QuestionPanels.SetActive(false);
                    SoundManager.instance.StartAudio(new string[1] { "Click_0" }, VOLUME_TYPE.EFFECT);
                    isPause = !isPause;
                    GameObject.Find("Canvas").transform.Find("UI").gameObject.SetActive(!isPause);
                    SettingCanvas.SetActive(isPause);
                    Time.timeScale = (isPause ? 0 : 1);
                    settingTrigger = (isPause ? 1 : 0);
                    GoAjit();
                }
            }

            //설정창 키보드 조작
            if (isPause)
            {
                if (PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ARROW_LEFT], false) || Input.GetKey(KeyCode.LeftArrow) || x < 0)
                {
                    if (settingTrigger == 3 && nowSettingSelectImageNum == optionOcount && closeCoolTime > 0.1f)
                    {
                        SoundManager.instance.StartAudio(new string[1] { "Click_0" }, VOLUME_TYPE.EFFECT);
                        CrossHairChange(true);
                        closeCoolTime = 0.0f;
                    }
                    if (settingTrigger == 3 && nowSettingSelectImageNum == optionOcount + 1 && closeCoolTime > 0.1f)
                    {
                        SoundManager.instance.StartAudio(new string[1] { "Click_0" }, VOLUME_TYPE.EFFECT);
                        CameraShakerLevelChange(nCameraShakerLevel - 1);
                        closeCoolTime = 0.0f;
                    }
                    if (settingTrigger == 5 && nowSettingSelectImageNum == optionOcount + optionGcount + optionVcount + 1 && closeCoolTime > 0.1f)
                    {
                        SoundManager.instance.StartAudio(new string[1] { "Click_0" }, VOLUME_TYPE.EFFECT);
                        SoundManager.instance.VolumeChange(0, SoundManager.instance.mSlider.value - 0.05f);
                        closeCoolTime = 0.0f;
                    }
                    if (settingTrigger == 5 && nowSettingSelectImageNum == optionOcount + optionGcount + optionVcount + 2 && closeCoolTime > 0.1f)
                    {
                        SoundManager.instance.StartAudio(new string[1] { "Click_0" }, VOLUME_TYPE.EFFECT);
                        SoundManager.instance.VolumeChange(1, SoundManager.instance.bSlider.value - 0.05f);
                        closeCoolTime = 0.0f;
                    }
                    if (settingTrigger == 5 && nowSettingSelectImageNum == optionOcount + optionGcount + optionVcount + 3 && closeCoolTime > 0.1f)
                    {
                        SoundManager.instance.StartAudio(new string[1] { "Click_0" }, VOLUME_TYPE.EFFECT);
                        SoundManager.instance.VolumeChange(2, SoundManager.instance.eSlider.value - 0.05f);
                        closeCoolTime = 0.0f;
                    }
                    if (settingTrigger == 5 && nowSettingSelectImageNum == optionOcount + optionGcount + optionVcount + 4 && closeCoolTime > 0.1f)
                    {
                        SoundManager.instance.StartAudio(new string[1] { "Click_0" }, VOLUME_TYPE.EFFECT);
                        SoundManager.instance.VolumeChange(3, SoundManager.instance.aSlider.value - 0.05f);
                        closeCoolTime = 0.0f;
                    }
                    if (settingTrigger == 4 && nowSettingSelectImageNum == optionOcount + optionGcount + 3 && closeCoolTime > 0.1f)
                    {
                        SoundManager.instance.StartAudio(new string[1] { "Click_0" }, VOLUME_TYPE.EFFECT);
                        Scrollbar scroll = SettingSelectImages[nowSettingSelectImageNum].transform.Find("Scrollbar").GetComponent<Scrollbar>();
                        FreamChange(scroll, scroll.value - 0.05f);
                        closeCoolTime = 0.0f;
                    }
                    if (settingTrigger == 4 && nowSettingSelectImageNum == optionOcount + optionGcount + 4 && closeCoolTime > 0.1f)
                    {
                        SoundManager.instance.StartAudio(new string[1] { "Click_0" }, VOLUME_TYPE.EFFECT);
                        Scrollbar scroll = SettingSelectImages[nowSettingSelectImageNum].transform.Find("Scrollbar").GetComponent<Scrollbar>();
                        BrightnessChange(scroll, scroll.value - 0.05f);
                        closeCoolTime = 0.0f;
                    }
                    if (settingTrigger == 4 && nowSettingSelectImageNum == optionOcount + optionGcount + 5 && closeCoolTime > 0.1f)
                    {
                        SoundManager.instance.StartAudio(new string[1] { "Click_0" }, VOLUME_TYPE.EFFECT);
                        Scrollbar scroll = SettingSelectImages[nowSettingSelectImageNum].transform.Find("Scrollbar").GetComponent<Scrollbar>();
                        ContrastChange(scroll, scroll.value - 0.05f);
                        closeCoolTime = 0.0f;
                    }
                    if (settingTrigger == 4 && nowSettingSelectImageNum == optionOcount + optionGcount + 4)
                    {
                        pushCheck++;
                    }
                    if (settingTrigger == 4 && nowSettingSelectImageNum == optionOcount + optionGcount + 5)
                    {
                        pushCheck++;
                    }
                }
                if (PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ARROW_RIGHT], false) || Input.GetKey(KeyCode.RightArrow) || x > 0)
                {
                    if (settingTrigger == 3 && nowSettingSelectImageNum == optionOcount && closeCoolTime > 0.1f)
                    {
                        SoundManager.instance.StartAudio(new string[1] { "Click_0" }, VOLUME_TYPE.EFFECT);
                        CrossHairChange(false);
                        closeCoolTime = 0.0f;
                    }
                    if (settingTrigger == 3 && nowSettingSelectImageNum == optionOcount + 1 && closeCoolTime > 0.1f)
                    {
                        SoundManager.instance.StartAudio(new string[1] { "Click_0" }, VOLUME_TYPE.EFFECT);
                        CameraShakerLevelChange(nCameraShakerLevel + 1);
                        closeCoolTime = 0.0f;
                    }
                    if (settingTrigger == 5 && nowSettingSelectImageNum == optionOcount + optionGcount + optionVcount + 1 && closeCoolTime > 0.1f)
                    {
                        SoundManager.instance.StartAudio(new string[1] { "Click_0" }, VOLUME_TYPE.EFFECT);
                        SoundManager.instance.VolumeChange(0, SoundManager.instance.mSlider.value + 0.05f);
                        closeCoolTime = 0.0f;
                    }
                    if (settingTrigger == 5 && nowSettingSelectImageNum == optionOcount + optionGcount + optionVcount + 2 && closeCoolTime > 0.1f)
                    {
                        SoundManager.instance.StartAudio(new string[1] { "Click_0" }, VOLUME_TYPE.EFFECT);
                        SoundManager.instance.VolumeChange(1, SoundManager.instance.bSlider.value + 0.05f);
                        closeCoolTime = 0.0f;
                    }
                    if (settingTrigger == 5 && nowSettingSelectImageNum == optionOcount + optionGcount + optionVcount + 3 && closeCoolTime > 0.1f)
                    {
                        SoundManager.instance.StartAudio(new string[1] { "Click_0" }, VOLUME_TYPE.EFFECT);
                        SoundManager.instance.VolumeChange(2, SoundManager.instance.eSlider.value + 0.05f);
                        closeCoolTime = 0.0f;
                    }
                    if (settingTrigger == 5 && nowSettingSelectImageNum == optionOcount + optionGcount + optionVcount + 4 && closeCoolTime > 0.1f)
                    {
                        SoundManager.instance.StartAudio(new string[1] { "Click_0" }, VOLUME_TYPE.EFFECT);
                        SoundManager.instance.VolumeChange(3, SoundManager.instance.aSlider.value + 0.05f);
                        closeCoolTime = 0.0f;
                    }
                    if (settingTrigger == 4 && nowSettingSelectImageNum == optionOcount + optionGcount + 3 && closeCoolTime > 0.1f)
                    {
                        SoundManager.instance.StartAudio(new string[1] { "Click_0" }, VOLUME_TYPE.EFFECT);
                        Scrollbar scroll = SettingSelectImages[nowSettingSelectImageNum].transform.Find("Scrollbar").GetComponent<Scrollbar>();
                        FreamChange(scroll, scroll.value + 0.05f);
                        closeCoolTime = 0.0f;
                    }
                    if (settingTrigger == 4 && nowSettingSelectImageNum == optionOcount + optionGcount + 4 && closeCoolTime > 0.1f)
                    {
                        SoundManager.instance.StartAudio(new string[1] { "Click_0" }, VOLUME_TYPE.EFFECT);
                        Scrollbar scroll = SettingSelectImages[nowSettingSelectImageNum].transform.Find("Scrollbar").GetComponent<Scrollbar>();
                        BrightnessChange(scroll, scroll.value + 0.05f);
                        closeCoolTime = 0.0f;
                    }
                    if (settingTrigger == 4 && nowSettingSelectImageNum == optionOcount + optionGcount + 5 && closeCoolTime > 0.1f)
                    {
                        SoundManager.instance.StartAudio(new string[1] { "Click_0" }, VOLUME_TYPE.EFFECT);
                        Scrollbar scroll = SettingSelectImages[nowSettingSelectImageNum].transform.Find("Scrollbar").GetComponent<Scrollbar>();
                        ContrastChange(scroll, scroll.value + 0.05f);
                        closeCoolTime = 0.0f;
                    }
                    if (settingTrigger == 4 && nowSettingSelectImageNum == optionOcount + optionGcount + 4)
                    {
                        pushCheck++;
                    }
                    if (settingTrigger == 4 && nowSettingSelectImageNum == optionOcount + optionGcount + 5)
                    {
                        pushCheck++;
                    }
                }

                if (pushCheck == pushCheck2 && pushCheck != 0)
                {
                    pushCheck = pushCheck2 = 0;
                    ValueChangePointUp();
                }
                else if (pushCheck != pushCheck2)
                {
                    ValueChangePointDown();
                }
                pushCheck2 = pushCheck;

                if ((PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ARROW_UP], true) || Input.GetKeyDown(KeyCode.UpArrow) || y > 0) && closeCoolTime > 0.2f)
                {
                    closeCoolTime = 0.0f;
                    SoundManager.instance.StartAudio(new string[1] { "Click_0" }, VOLUME_TYPE.EFFECT);
                    if (settingTrigger == 2)
                    {
                        nowSettingSelectImageNum--;
                        if (nowSettingSelectImageNum < 0) nowSettingSelectImageNum = SettingSelectImages.Length - 1 - optionGcount - optionVcount - optionAcount - optionLcount - (optionKcount + 14);
                        SelectBarChange(SettingSelectImages[nowSettingSelectImageNum]);
                    }
                    if (settingTrigger == 3)
                    {
                        nowSettingSelectImageNum--;
                        if (nowSettingSelectImageNum < optionOcount) nowSettingSelectImageNum = SettingSelectImages.Length - 1 - optionVcount - optionAcount - optionLcount - (optionKcount + 14);
                        SelectBarChange(SettingSelectImages[nowSettingSelectImageNum]);
                    }
                    if (settingTrigger == 4)
                    {
                        nowSettingSelectImageNum--;
                        if (nowSettingSelectImageNum < optionOcount + optionGcount) nowSettingSelectImageNum = SettingSelectImages.Length - 1 - optionAcount - optionLcount - (optionKcount + 14);
                        SelectBarChange(SettingSelectImages[nowSettingSelectImageNum]);
                    }
                    if (settingTrigger == 5)
                    {
                        nowSettingSelectImageNum--;
                        if (nowSettingSelectImageNum < optionOcount + optionGcount + optionVcount) nowSettingSelectImageNum = SettingSelectImages.Length - 1 - optionLcount - (optionKcount + 14);
                        SelectBarChange(SettingSelectImages[nowSettingSelectImageNum]);
                    }
                    if (settingTrigger == 6)
                    {
                        nowSettingSelectImageNum--;
                        if (nowSettingSelectImageNum < optionOcount + optionGcount + optionVcount + optionAcount) nowSettingSelectImageNum = SettingSelectImages.Length - 1 - (optionKcount + 14);
                        SelectBarChange(SettingSelectImages[nowSettingSelectImageNum]);
                    }
                    if (settingTrigger == 7)
                    {
                        nowSettingSelectImageNum--;
                        if (nowSettingSelectImageNum < optionOcount + optionGcount + optionVcount + optionAcount + optionLcount)
                            nowSettingSelectImageNum = SettingSelectImages.Length - (isJoystrick ? 15 : 13);
                        if (!isJoystrick)
                            SelectBarChange(SettingSelectImages[nowSettingSelectImageNum + 12]);
                        else
                            SelectBarChange(SettingSelectImages[nowSettingSelectImageNum]);
                    }
                }
                if ((PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ARROW_DOWN], true) || Input.GetKeyDown(KeyCode.DownArrow) || y < 0) && closeCoolTime > 0.2f)
                {
                    closeCoolTime = 0.0f;
                    SoundManager.instance.StartAudio(new string[1] { "Click_0" }, VOLUME_TYPE.EFFECT);
                    if (settingTrigger == 2)
                    {
                        nowSettingSelectImageNum++;
                        if (nowSettingSelectImageNum >= SettingSelectImages.Length - optionGcount - optionVcount - optionAcount - optionLcount - (optionKcount + 14)) nowSettingSelectImageNum = 0;
                        SelectBarChange(SettingSelectImages[nowSettingSelectImageNum]);
                    }
                    if (settingTrigger == 3)
                    {
                        nowSettingSelectImageNum++;
                        if (nowSettingSelectImageNum >= SettingSelectImages.Length - optionVcount - optionAcount - optionLcount - (optionKcount + 14)) nowSettingSelectImageNum = optionOcount;
                        SelectBarChange(SettingSelectImages[nowSettingSelectImageNum]);
                    }
                    if (settingTrigger == 4)
                    {
                        nowSettingSelectImageNum++;
                        if (nowSettingSelectImageNum >= SettingSelectImages.Length - optionAcount - optionLcount - (optionKcount + 14)) nowSettingSelectImageNum = optionOcount + optionGcount;
                        SelectBarChange(SettingSelectImages[nowSettingSelectImageNum]);
                    }
                    if (settingTrigger == 5)
                    {
                        nowSettingSelectImageNum++;
                        if (nowSettingSelectImageNum >= SettingSelectImages.Length - optionLcount - (optionKcount + 14)) nowSettingSelectImageNum = optionOcount + optionGcount + optionVcount;
                        SelectBarChange(SettingSelectImages[nowSettingSelectImageNum]);
                    }
                    if (settingTrigger == 6)
                    {
                        nowSettingSelectImageNum++;
                        if (nowSettingSelectImageNum >= SettingSelectImages.Length - (optionKcount + 14)) nowSettingSelectImageNum = optionOcount + optionGcount + optionVcount + optionAcount;
                        SelectBarChange(SettingSelectImages[nowSettingSelectImageNum]);
                    }
                    if (settingTrigger == 7)
                    {
                        nowSettingSelectImageNum++;
                        if (nowSettingSelectImageNum >= SettingSelectImages.Length - (isJoystrick ? 14 : 12)) nowSettingSelectImageNum = optionOcount + optionGcount + optionVcount + optionAcount + optionLcount;
                        if (!isJoystrick)
                            SelectBarChange(SettingSelectImages[nowSettingSelectImageNum + 12]);
                        else
                            SelectBarChange(SettingSelectImages[nowSettingSelectImageNum]);
                    }
                }
                if (PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ACTIVE], true) || Input.GetKeyDown(KeyCode.Return) || JoyStickKeyInputCheck(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_CONFIRM], true))
                {
                    SoundManager.instance.StartAudio(new string[1] { "Click_0" }, VOLUME_TYPE.EFFECT);
                    if (settingTrigger == 2)
                    {
                        OptionMenuClick(nowSettingSelectImageNum + 3);
                        return;
                    }
                    if (settingTrigger == 3 && nowSettingSelectImageNum == optionOcount + 2)
                    {
                        OnOffPause();
                        return;
                    }
                    if (settingTrigger == 4 && nowSettingSelectImageNum == optionOcount + optionGcount)
                    {
                        ScreenModeChange(true);
                        return;
                    }
                    if (settingTrigger == 4 && nowSettingSelectImageNum == optionOcount + optionGcount + 1)
                    {
                        ScreenModeChange(false);
                        return;
                    }
                    if (settingTrigger == 4 && nowSettingSelectImageNum == optionOcount + optionGcount + 2)
                    {
                        VsyncModeChange(!isVsyncMode);
                        return;
                    }
                    if (settingTrigger == 5 && nowSettingSelectImageNum == optionOcount + optionGcount + optionVcount)
                    {
                        SoundManager.instance.MuteChange(SettingSelectImages[nowSettingSelectImageNum].transform.Find("Mute").Find("Image").GetComponent<Image>());
                        return;
                    }
                    if (settingTrigger == 6 && nowSettingSelectImageNum == optionOcount + optionGcount + optionVcount + optionAcount)
                    {
                        LocalizeManager.instance.LanguageChange(0);
                        return;
                    }
                    if (settingTrigger == 6 && nowSettingSelectImageNum == optionOcount + optionGcount + optionVcount + optionAcount + 1)
                    {
                        LocalizeManager.instance.LanguageChange(1);
                        return;
                    }
                    if (settingTrigger == 6 && nowSettingSelectImageNum == optionOcount + optionGcount + optionVcount + optionAcount + 2)
                    {
                        LocalizeManager.instance.LanguageChange(2);
                        return;
                    }
                    if (settingTrigger == 6 && nowSettingSelectImageNum == optionOcount + optionGcount + optionVcount + optionAcount + 3)
                    {
                        LocalizeManager.instance.LanguageChange(3);
                        return;
                    }
                    if (settingTrigger == 6 && nowSettingSelectImageNum == optionOcount + optionGcount + optionVcount + optionAcount + 4)
                    {
                        LocalizeManager.instance.LanguageChange(4);
                        return;
                    }
                    if (settingTrigger == 6 && nowSettingSelectImageNum == optionOcount + optionGcount + optionVcount + optionAcount + 5)
                    {
                        LocalizeManager.instance.LanguageChange(5);
                        return;
                    }
                    if (settingTrigger == 7)
                    {
                        for (int i = 0; i < (isJoystrick ? 12 : 14); i++)
                        {
                            if (nowSettingSelectImageNum == optionOcount + optionGcount + optionVcount + optionAcount + optionLcount + i)
                            {
                                KeySetting(i);
                            }
                        }
                        return;
                    }
                }
            }

            // 옵션
            if ((PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_OPTION], true) || JoyStickKeyInputCheck(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_OPTION], true)) && isPause && !isOpenPortal && !QuestionPanels.activeSelf && closeCoolTime > 0.2f)
            {
                closeCoolTime = 0.0f;
                nowSettingSelectImageNum = 0;
                SoundManager.instance.StartAudio(new string[1] { "Click_0" }, VOLUME_TYPE.EFFECT);
                for (int i = 0; i < SettingSelectImages.Length; i++)
                {
                    SettingSelectImages[i].enabled = false;
                }
                OptionCanvas.SetActive(true);
                settingTrigger = 2;
                SelectBarChange(SettingSelectImages[nowSettingSelectImageNum]);
            }
            // 게임종료
            if ((PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_QUIT], true) || JoyStickKeyInputCheck(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_QUIT], true)) && isPause && !isOpenPortal && !QuestionPanels.activeSelf && closeCoolTime > 0.2f)
            {
                closeCoolTime = 0.0f;
                OpenQuestion_Quit();
            }
            // 재시작
            if ((PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_RESTART], true) || JoyStickKeyInputCheck(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_RESTART], true)) && isPause && !isOpenPortal && !isAjit && !QuestionPanels.activeSelf && closeCoolTime > 0.2f)
            {
                closeCoolTime = 0.0f;
                OpenQuestion_Restart();
            }

            if (isTumblbug && isTumblbugEventEnd)
            {
                if ((PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ARROW_UP], true) || Input.GetKeyDown(KeyCode.UpArrow) || y > 0) && closeCoolTime > 0.2f)
                {
                    closeCoolTime = 0.0f;
                    demoSelect--;
                    if (demoSelect < 0) demoSelect = 1;
                    SoundManager.instance.StartAudio(new string[1] { "Click_0" }, VOLUME_TYPE.EFFECT);

                    if (demoSelect == 0)
                    {
                        tumblbugImages[0].transform.Find("Select").gameObject.SetActive(true);
                        tumblbugImages[1].transform.Find("Select").gameObject.SetActive(false);
                    }
                    else
                    {
                        tumblbugImages[0].transform.Find("Select").gameObject.SetActive(false);
                        tumblbugImages[1].transform.Find("Select").gameObject.SetActive(true);
                    }
                }
                if ((PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ARROW_DOWN], true) || Input.GetKeyDown(KeyCode.DownArrow) || y < 0) && closeCoolTime > 0.2f)
                {
                    closeCoolTime = 0.0f;
                    demoSelect++;
                    if (demoSelect > 1) demoSelect = 0;
                    SoundManager.instance.StartAudio(new string[1] { "Click_0" }, VOLUME_TYPE.EFFECT);

                    if (demoSelect == 0)
                    {
                        tumblbugImages[0].transform.Find("Select").gameObject.SetActive(true);
                        tumblbugImages[1].transform.Find("Select").gameObject.SetActive(false);
                    }
                    else
                    {
                        tumblbugImages[0].transform.Find("Select").gameObject.SetActive(false);
                        tumblbugImages[1].transform.Find("Select").gameObject.SetActive(true);
                    }
                }
                if (PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ACTIVE], true) || Input.GetKeyDown(KeyCode.Return) || JoyStickKeyInputCheck(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_CONFIRM], true))
                {
                    tumblbugImages[demoSelect].GetComponent<Button>().onClick.Invoke();
                }
            }
            // 텀블벅 아지트로
            if ((Input.GetKeyDown(KeyCode.Escape) || Input.GetKeyDown(KeyCode.Backspace)) && isTumblbug && isTumblbugEventEnd && !QuestionPanel.activeSelf)
            {
                closeCoolTime = 0.0f;
                SoundManager.instance.StartAudio(new string[1] { "Click_0" }, VOLUME_TYPE.EFFECT);
                isTumblbug = false;
                isTumblbugEventEnd = false;
                Time.timeScale = 1;
                Canvas.SetActive(true);
                TumblbugCanvas.SetActive(false);
                isStageBoss = false;
                isStageClear = false;
                nowStageStep = 0;
                StageLevel = 0;
                GoAjit();
            }
            // 텀블벅 재시작
            if (Input.GetKeyDown(KeyCode.Space) && isTumblbug && isTumblbugEventEnd)
            {
                SoundManager.instance.StartAudio(new string[1] { "Click_0" }, VOLUME_TYPE.EFFECT);
                isTumblbug = false;
                isTumblbugEventEnd = false;
                Time.timeScale = 1;
                Canvas.SetActive(true);
                TumblbugCanvas.SetActive(false);
                isStageBoss = false;
                isStageClear = false;
                nowStageStep = 0;
                StageLevel = 0;
                Create();
            }
            if (isWorldMap)
            {
                float wheel = 0.0f;
                if (isJoystrick)
                {
                    if (joystricks[0].Contains("PC") || joystricks[0].Contains("DS4") || joystricks[0].Contains("Wireless"))
                    {
                        if (Input.GetKey(KeyCode.JoystickButton6)) wheel -= 0.2f;
                        else if (Input.GetKey(KeyCode.JoystickButton7)) wheel += 0.2f;
                    }
                    else
                        wheel = Input.GetAxis("JoyStrickTrigger");
                }
                else wheel = Input.GetAxis("Mouse ScrollWheel");
                if ((wheel * wheelSpeed) != 0 && (worldMapCamera.orthographicSize > 10 || worldMapCamera.orthographicSize < 80))
                {
                    worldMapCamera.orthographicSize -= wheel * wheelSpeed;
                    if (worldMapCamera.orthographicSize < 10) worldMapCamera.orthographicSize = 10;
                    if (worldMapCamera.orthographicSize > 30) worldMapCamera.orthographicSize = 30;
                }
            }
        }
        #region Initialization
        public void GoTutorialFirstEvent()
        {
            StartCoroutine(TutorialFirstEventStart());
        }
        IEnumerator TutorialFirstEventStart()
        {
            Canvas.transform.Find("UI").gameObject.SetActive(false);
            Canvas.transform.Find("MiniMap_OutLine").gameObject.SetActive(false);
            Grain pGrain;
            pVolume.profile.TryGetSettings(out pGrain);
            pGrain.active = true;
            SoundManager.instance.StartAudio(new string[1] { "tutorial_bgm_2" }, VOLUME_TYPE.BGM, null, true);
            yield return new WaitForSecondsRealtime(2.0f);
            float a = 0.0f;
            while (a <= 1.0f)
            {
                a += Time.deltaTime / 2;
                firstEventNPC.color = new Color(1, 1, 1, a);
                yield return new WaitForSeconds(Time.deltaTime);
            }
            firstEventNPC.color = Color.white;
            SoundManager.instance.StartAudio(new string[1] { "walking-5" }, VOLUME_TYPE.EFFECT);
            while (true)
            {
                Vector3 dir = moveTarget.transform.position - firstEventNPC.transform.position;
                dir.Normalize();
                if (Vector3.Distance(moveTarget.transform.position, firstEventNPC.transform.position) < 1)
                {
                    firstEventNPC.transform.parent.GetComponent<TutorialEvent>().isStart = true;
                    Dialogue.Instance.CommnetSetting("tutorial_first_event_npc", 0, null);
                    break;
                }
                else
                {
                    firstEventNPC.transform.Translate(dir * firstNPCmoveSpeed * Time.deltaTime);
                }
                yield return new WaitForSecondsRealtime(Time.deltaTime);
            }
            SoundManager.instance.StopAudio(new string[1] { "walking-5" });
        }
        public void GoTumblbug()
        {
            Clear();
            isTumblbug = true;
            if (Inventorys.Instance.tempCorotine != null)
            {
                Inventorys.Instance.StopAllCoroutines();
                Inventorys.Instance.getItemObj.GetComponent<RectTransform>().anchoredPosition3D = new Vector3(0, -171, 0);
            }

            Time.timeScale = 0;
            GameObject.Find("Canvas").SetActive(false);
            TumblbugCanvas.SetActive(true);

            for (int i = 0; i < tumblbugImages.Length; i++)
            {
                tumblbugImages[i].GetComponent<Button>().onClick.RemoveAllListeners();
                int c = i;
                tumblbugImages[i].GetComponent<Button>().onClick.AddListener(() => { TumblbugButton(c); });
            }
            StartCoroutine(Tumblbug());
        }
        IEnumerator Tumblbug()
        {
            Image[] AllImages = TumblbugCanvas.GetComponentsInChildren<Image>(true);
            Text[] AllTexts = TumblbugCanvas.GetComponentsInChildren<Text>(true);
            for (int i = 0; i < AllImages.Length; i++)
            {
                AllImages[i].color = new Color(AllImages[i].color.r, AllImages[i].color.g, AllImages[i].color.b, 0);
            }
            for (int i = 0; i < AllTexts.Length; i++)
            {
                AllTexts[i].color = new Color(AllTexts[i].color.r, AllTexts[i].color.g, AllTexts[i].color.b, 0);
            }
            yield return new WaitForSecondsRealtime(3.0f);
            float a = 0.0f;
            while (a <= 1.0f)
            {
                for (int i = 0; i < AllImages.Length; i++)
                {
                    AllImages[i].color = new Color(AllImages[i].color.r, AllImages[i].color.g, AllImages[i].color.b, a);
                }
                for (int i = 0; i < AllTexts.Length; i++)
                {
                    AllTexts[i].color = new Color(AllTexts[i].color.r, AllTexts[i].color.g, AllTexts[i].color.b, a);
                }
                a += Time.unscaledDeltaTime;
                yield return new WaitForSecondsRealtime(Time.unscaledDeltaTime);
            }
            for (int i = 0; i < AllImages.Length; i++)
            {
                AllImages[i].color = new Color(AllImages[i].color.r, AllImages[i].color.g, AllImages[i].color.b, 1);
            }
            for (int i = 0; i < AllTexts.Length; i++)
            {
                AllTexts[i].color = new Color(AllTexts[i].color.r, AllTexts[i].color.g, AllTexts[i].color.b, 1);
            }
            isTumblbugEventEnd = true;
        }
        public void GoAjit()
        {
            curseHealth2 = 0;
            curseAttack2 = 0;
            newObjectSelectedObj = null;
            newObjectSelected = false;
            Grain pGrain;
            pVolume.profile.TryGetSettings(out pGrain);
            pGrain.active = false;
            nQChange = 0;
            isStartGun = false;
            SoundManager.instance.StartAudio(new string[1] { "BGM/Ajit" }, VOLUME_TYPE.BGM, null, true);
            UnityEngine.Camera.main.orthographicSize = 8;
            Canvas.transform.Find("PressKey").gameObject.SetActive(false);
            Canvas.transform.Find("PressKeyHealth").gameObject.SetActive(false);
            Canvas.transform.Find("PressKeySwitch").gameObject.SetActive(false);
            Canvas.transform.Find("PressKeyGuide").gameObject.SetActive(false);
            Canvas.transform.Find("PressKeyFire").gameObject.SetActive(false);
            StageName = "ajit_mapTitle";
            SoundManager.instance.StopAudio(new string[1] { "tutorial_bgm_2" });
            isAjit = true;
            if (!isStageClear || isTestMode2)
            {
                BodyRemodelingManager.instance.ReSetPoint(true);
                ResultPanelScript.instance.Q_SkillCount = 0;
                Inventorys.Instance.DiscardedItem.Clear();
                Inventorys.Instance.DiscardedItemString.Clear();
                GameObject.Find("Canvas").transform.Find("LoadingPanel").Find("NewLoading").Find("character").GetComponent<RectTransform>().anchoredPosition3D = new Vector3(-770, 75);
                stageRan = new int[maxStage];
                stageBossRan = new int[maxStage];
                for (int i = 0; i < stageRan.Length; i++)
                {
                    while (true)
                    {
                        stageRan[i] = Random.Range(0, stageRan.Length + 1);
                        if (i == 0 && prevStartRan != -1 && prevStartRan == stageRan[i])
                        {
                            continue;
                        }
                        if (i == 0) prevStartRan = stageRan[i];
                        bool c = false;
                        for (int j = 0; j < i; j++)
                        {
                            if (stageRan[i] == stageRan[j]) c = true;
                        }
                        if (!c) break;
                    }
                }
                for (int i = 0; i < stageBossRan.Length; i++)
                {
                    while (true)
                    {
                        stageBossRan[i] = Random.Range(0, stageBossRan.Length);
                        if (i == 0 && prevStartRanBoss != -1 && prevStartRanBoss == stageBossRan[i])
                        {
                            continue;
                        }
                        if (i == 0) prevStartRanBoss = stageBossRan[i];
                        bool c = false;
                        for (int j = 0; j < i; j++)
                        {
                            if (stageBossRan[i] == stageBossRan[j]) c = true;
                        }
                        if (!c) break;
                    }
                }
                for (int i = 0; i < stageStepObj.Count; i++)
                {
                    stageStepObj[i].GetComponent<Image>().sprite = stageDefaultSprite;
                }
                Inventorys.Instance.InventoryReset();
                if (PlayerBuffController.instance != null) PlayerBuffController.instance.BuffReset(1);
                if (isTestMode)
                {
                    ItemsData iData = System.Array.Find(SheetManager.Instance.ItemsDB.dataArray, item => item.ID.Equals("key_normal"));
                    Inventorys.Instance.AddItem(iData, 1, false, true);
                }
                else
                {
                    isStageBoss = false;
                    nowStageStep = 0;
                    StageLevel = 0;
                    isRebuildDevice = false;
                }
            }
            Canvas.transform.Find("UI").gameObject.SetActive(true);
            Conversation.Instance.StartEffect();
            for (int i = 0; i < WorldMapSimbols.Length; i++)
            {
                WorldMapSimbols[i].SetActive(false);
            }
            WorldMapSimbols[0].SetActive(true);
            WorldMapSimbols[6].SetActive(true);
            WorldMapSimbols[7].SetActive(true);
            WorldMapSimbols[8].SetActive(true);
            WorldMapSimbols[12].SetActive(true);
            WorldMapSimbols[13].SetActive(true);
            WorldMapSimbols[14].SetActive(true);
            WorldMapSimbols[21].SetActive(true);
            WorldMapSimbols[22].SetActive(true);
            StartCoroutine(FadeInOut());
            StartCoroutine(AjitSetting());
        }
        public int bossRushClearCount = 0;
        public void GoAjit2(bool __isClear)
        {
            BossRushItemList.Clear();
            if (__isClear)
            {
                bossRushClearCount++;
                SaveAndLoadManager.instance.SaveItem(new ItemSave { _id = "glowing_skull2", _nCount = 50 });
                int t = SaveAndLoadManager.instance.LoadItem("glowing_skull2");
                Inventorys.Instance.glowindskull2 = t;
            }
            curseHealth2 = 0;
            curseAttack2 = 0;
            newObjectSelectedObj = null;
            newObjectSelected = false;
            Grain pGrain;
            pVolume.profile.TryGetSettings(out pGrain);
            pGrain.active = false;
            nQChange = 0;
            isStartGun = false;
            SoundManager.instance.StartAudio(new string[1] { "BGM/Ajit" }, VOLUME_TYPE.BGM, null, true);
            UnityEngine.Camera.main.orthographicSize = 8;
            Canvas.transform.Find("PressKey").gameObject.SetActive(false);
            Canvas.transform.Find("PressKeyHealth").gameObject.SetActive(false);
            Canvas.transform.Find("PressKeySwitch").gameObject.SetActive(false);
            Canvas.transform.Find("PressKeyGuide").gameObject.SetActive(false);
            Canvas.transform.Find("PressKeyFire").gameObject.SetActive(false);
            StageName = "ajit_mapTitle";
            isAjit = true;
            if (!isStageClear || isTestMode2)
            {
                BodyRemodelingManager.instance.ReSetPoint(true);
                ResultPanelScript.instance.Q_SkillCount = 0;
                Inventorys.Instance.DiscardedItem.Clear();
                Inventorys.Instance.DiscardedItemString.Clear();
                GameObject.Find("Canvas").transform.Find("LoadingPanel").Find("NewLoading").Find("character").GetComponent<RectTransform>().anchoredPosition3D = new Vector3(-770, 75);
                stageRan = new int[maxStage];
                stageBossRan = new int[maxStage];
                for (int i = 0; i < stageRan.Length; i++)
                {
                    while (true)
                    {
                        stageRan[i] = Random.Range(0, stageRan.Length + 1);
                        if (i == 0 && prevStartRan != -1 && prevStartRan == stageRan[i])
                        {
                            continue;
                        }
                        if (i == 0) prevStartRan = stageRan[i];
                        bool c = false;
                        for (int j = 0; j < i; j++)
                        {
                            if (stageRan[i] == stageRan[j]) c = true;
                        }
                        if (!c) break;
                    }
                }
                for (int i = 0; i < stageBossRan.Length; i++)
                {
                    while (true)
                    {
                        stageBossRan[i] = Random.Range(0, stageBossRan.Length);
                        if (i == 0 && prevStartRanBoss != -1 && prevStartRanBoss == stageBossRan[i])
                        {
                            continue;
                        }
                        if (i == 0) prevStartRanBoss = stageBossRan[i];
                        bool c = false;
                        for (int j = 0; j < i; j++)
                        {
                            if (stageBossRan[i] == stageBossRan[j]) c = true;
                        }
                        if (!c) break;
                    }
                }
                for (int i = 0; i < stageStepObj.Count; i++)
                {
                    stageStepObj[i].GetComponent<Image>().sprite = stageDefaultSprite;
                }
                Inventorys.Instance.InventoryReset();
                if (PlayerBuffController.instance != null) PlayerBuffController.instance.BuffReset(1);
                if (isTestMode)
                {
                    ItemsData iData = System.Array.Find(SheetManager.Instance.ItemsDB.dataArray, item => item.ID.Equals("key_normal"));
                    Inventorys.Instance.AddItem(iData, 1, false, true);
                }
                else
                {
                    isStageBoss = false;
                    nowStageStep = 0;
                    StageLevel = 0;
                    isRebuildDevice = false;
                }
            }
            Canvas.transform.Find("UI").gameObject.SetActive(true);
            Conversation.Instance.StartEffect();
            for (int i = 0; i < WorldMapSimbols.Length; i++)
            {
                WorldMapSimbols[i].SetActive(false);
            }
            WorldMapSimbols[0].SetActive(true);
            WorldMapSimbols[6].SetActive(true);
            WorldMapSimbols[7].SetActive(true);
            WorldMapSimbols[8].SetActive(true);
            WorldMapSimbols[12].SetActive(true);
            WorldMapSimbols[13].SetActive(true);
            WorldMapSimbols[14].SetActive(true);
            WorldMapSimbols[21].SetActive(true);
            WorldMapSimbols[22].SetActive(true);
            StartCoroutine(FadeInOut());
            StartCoroutine(AjitSetting2());
        }
        IEnumerator AjitSetting2()
        {
            if (ResultPanelScript.instance != null) ResultPanelScript.instance.time = 0.0f;
            if (ResultPanelScript.instance != null) ResultPanelScript.instance.bossList.Clear();
            SaveAndLoadManager.instance.DeleteManufacture(1);
            MaterialList.Clear();
            Clear();
            if (HeroObj != null) Destroy(HeroObj);
            yield return null;
            GameObject h = Instantiate(hero, new Vector3(-22.5f, -22.5f - 0.25F, 0), Quaternion.identity);
            HeroObj = h;
            h.name = "Hero";
            HeroObj.GetComponent<Player>().isTutorial = false;

            isMercenaryAjit = false;
            Player.bFirstSetting = false;
            ItemCapsule.isGetItem_0 = false;
            ItemCapsule.isGetItem_1 = false;
            ItemCapsule.isGetItem_2 = false;

            Camera[] cameras = FindObjectsOfType<Camera>();
            for (int i = 0; i < cameras.Length; i++)
                cameras[i].target = h.transform;
            minimapObj.SetActive(false);
            TutorialText.SetActive(false);

            newObjectSelectedObj = null;
            newObjectSelected = false;
            GameObject obj = Instantiate(AjitPrefabs);
            obj.transform.position = new Vector3(22.5f, 22.5f, 0);
            PDG.Player.instance.transform.position = new Vector3(22.5f, 22.5f, 0);
            isBossRush = true;
            StartCoroutine(BossRush());
        }
        public void GoEnding()
        {

            if (SteamStatsAndAchievements.instance != null && SaveAndLoadManager.nDifficulty == 0) SteamStatsAndAchievements.instance.UnlockAchievementToPublic("Difficulty_Easy_Clear");
            if (SteamStatsAndAchievements.instance != null && SaveAndLoadManager.nDifficulty == 1) SteamStatsAndAchievements.instance.UnlockAchievementToPublic("Difficulty_Normal_Clear");
            if (SteamStatsAndAchievements.instance != null && SaveAndLoadManager.nDifficulty == 2) SteamStatsAndAchievements.instance.UnlockAchievementToPublic("Difficulty_Hard_Clear");
            if (SteamStatsAndAchievements.instance != null && SaveAndLoadManager.nDifficulty == 3) SteamStatsAndAchievements.instance.UnlockAchievementToPublic("Difficulty_Hell_Clear");

            UnityEngine.Camera.main.orthographicSize = 8;
            Canvas.transform.Find("PressKey").gameObject.SetActive(false);
            Canvas.transform.Find("PressKeyHealth").gameObject.SetActive(false);
            Canvas.transform.Find("PressKeySwitch").gameObject.SetActive(false);
            Canvas.transform.Find("PressKeyGuide").gameObject.SetActive(false);
            Canvas.transform.Find("PressKeyFire").gameObject.SetActive(false);
            StageName = "ending_title";
            SoundManager.instance.StopAudio(new string[1] { "tutorial_bgm_2" });
            Canvas.transform.Find("UI").gameObject.SetActive(true);
            Conversation.Instance.StartEffect();
            StartCoroutine(EndingSetting());
        }
        public void GoTutorial()
        {
            if (Canvas.transform.Find("UI")) Canvas.transform.Find("UI").gameObject.SetActive(true);
            if (Canvas.transform.Find("PressKey")) Canvas.transform.Find("PressKey").gameObject.SetActive(false);
            if (Canvas.transform.Find("PressKeyHealth")) Canvas.transform.Find("PressKeyHealth").gameObject.SetActive(false);
            if (Canvas.transform.Find("PressKeySwitch")) Canvas.transform.Find("PressKeySwitch").gameObject.SetActive(false);
            if (Canvas.transform.Find("PressKeyGuide")) Canvas.transform.Find("PressKeyGuide").gameObject.SetActive(false);
            if (Canvas.transform.Find("PressKeyFire")) Canvas.transform.Find("PressKeyFire").gameObject.SetActive(false);
            if (nowTutorialNum >= TutorialMap.Length)
            {
                if (SteamStatsAndAchievements.instance != null) SteamStatsAndAchievements.instance.UnlockAchievementToPublic("TutorialEnd");
                GoAjit();
                SaveAndLoadManager.instance.SaveVolume(new VolumeSave { _type = 6, _value = 1 });
            }
            else
            {
                StartCoroutine(TutorialSetting());
            }
        }
        IEnumerator AjitSetting()
        {
            if (ResultPanelScript.instance != null) ResultPanelScript.instance.time = 0.0f;
            if (ResultPanelScript.instance != null) ResultPanelScript.instance.bossList.Clear();
            MaterialList.Clear();
            Clear();
            if (HeroObj != null) Destroy(HeroObj);
            yield return null;
            bool isLoad = false;
            if (SaveAndLoadManager.isLoad)
            {
                isLoad = true;
                GameSaveData[] loadData = SaveAndLoadManager.instance.GameLoad();
                for (int i = 0; i < loadData.Length; i++)
                {
                    if (!loadData[i].id.Contains("savedata")) continue;
                    else if (loadData[i].id.Equals("savedata_nQChange")) nQChange = loadData[i].count;
                    else if (loadData[i].id.Equals("savedata_stage_level")) StageLevel = loadData[i].count;
                    else if (loadData[i].id.Equals("savedata_stage_step")) nowStageStep = loadData[i].count;
                    else if (loadData[i].id.Equals("savedata_stage_boss")) isStageBoss = loadData[i].count == 0 ? false : true;
                    else if (loadData[i].id.Equals("savedata_stage_rank_0") && loadData[i].count != -1) stageRan[0] = loadData[i].count;
                    else if (loadData[i].id.Equals("savedata_stage_rank_1") && loadData[i].count != -1) stageRan[1] = loadData[i].count;
                    else if (loadData[i].id.Equals("savedata_stage_rank_2") && loadData[i].count != -1) stageRan[2] = loadData[i].count;
                    else if (loadData[i].id.Equals("savedata_stage_rank_3") && loadData[i].count != -1) stageRan[3] = loadData[i].count;
                    else if (loadData[i].id.Equals("savedata_stage_boss_0") && loadData[i].count != -1) stageBossRan[0] = loadData[i].count;
                    else if (loadData[i].id.Equals("savedata_stage_boss_1") && loadData[i].count != -1) stageBossRan[1] = loadData[i].count;
                    else if (loadData[i].id.Equals("savedata_stage_boss_2") && loadData[i].count != -1) stageBossRan[2] = loadData[i].count;
                    else if (loadData[i].id.Equals("savedata_stage_boss_3") && loadData[i].count != -1) stageBossRan[3] = loadData[i].count;
                    else if (loadData[i].id.Equals("savedata_curseHealth") && loadData[i].count != 0) curseHealth = loadData[i].count;
                    else if (loadData[i].id.Equals("savedata_curseHealth2") && loadData[i].count != 0) curseHealth2 = loadData[i].count;
                    else if (loadData[i].id.Equals("savedata_curseAttack") && loadData[i].count != 0) curseAttack = loadData[i].count;
                    else if (loadData[i].id.Equals("savedata_curseAttack2") && loadData[i].count != 0) curseAttack2 = loadData[i].count;
                    else if (loadData[i].id.Equals("savedata_isMercenaryAjit") && loadData[i].count != -1) isMercenaryAjit = loadData[i].count == 0 ? false : true;
                    else if (loadData[i].id.Equals("savedata_specialID") && loadData[i].count != -1)
                    {
                        ReinforceData[] useData = System.Array.FindAll(SheetManager.Instance.ReinforceDB.dataArray, item => item.Useflag);
                        PlayerBuffController.instance.AddBuff(useData[loadData[i].count].ID);
                    }
                    else if (loadData[i].id.Equals("savedata_difficulty") && loadData[i].count != -1) SaveAndLoadManager.nDifficulty = loadData[i].count;
                }

                if (StageLevel > 3) StageLevel = 3;
            }
            GameObject h = Instantiate(hero, new Vector3(-22.5f, -22.5f - 0.25F, 0), Quaternion.identity);
            HeroObj = h;
            h.name = "Hero";
            HeroObj.GetComponent<Player>().isTutorial = false;
            if (isLoad)
                StateCreate("", null, null);
            else
            {
                if (!isStageClear || isTestMode2)
                {
                    curseHealth = 0;
                    curseAttack = 0;
                    Player.curHp = 6;
                    isMercenaryAjit = false;
                    Player.bFirstSetting = false;
                    ItemCapsule.isGetItem_0 = false;
                    ItemCapsule.isGetItem_1 = false;
                    ItemCapsule.isGetItem_2 = false;
                }
                Camera[] cameras = FindObjectsOfType<Camera>();
                for (int i = 0; i < cameras.Length; i++)
                    cameras[i].target = h.transform;
                GameObject obj = Instantiate(AjitObj, null);
                obj.transform.localPosition = new Vector3(-22.5f, -22.5f, 0);
                minimapObj.SetActive(false);
                TutorialText.SetActive(false);
                spawnBat = Instantiate(BatObj);

                if (SteamStatsAndAchievements.instance.IsUnlockAchievement("Difficulty_Easy_Clear") ||
                    SteamStatsAndAchievements.instance.IsUnlockAchievement("Difficulty_Normal_Clear") ||
                    SteamStatsAndAchievements.instance.IsUnlockAchievement("Difficulty_Hard_Clear") ||
                    SteamStatsAndAchievements.instance.IsUnlockAchievement("Difficulty_Hell_Clear"))
                {
                    obj.transform.Find("ajit2_open").gameObject.SetActive(true);
                }
                else
                {
                    obj.transform.Find("ajit2_open").gameObject.SetActive(false);
                }
            }
        }
        IEnumerator EndingSetting()
        {
            Clear();
            SoundManager.instance.StartAudio(new string[1] { "BGM/LastStage" }, VOLUME_TYPE.BGM, null, true);
            if (HeroObj != null) Destroy(HeroObj);
            yield return null;
            GameObject h = Instantiate(hero, new Vector3(-22.5f, -22.5f - 18.25F, 0), Quaternion.identity);
            HeroObj = h;
            h.name = "Hero";
            h.transform.Find("AreaDustBreezy").gameObject.SetActive(true);
            Camera[] cameras = FindObjectsOfType<Camera>();
            for (int i = 0; i < cameras.Length; i++) cameras[i].target = h.transform;
            yield return new WaitForSeconds(1.0f);
            GameObject obj = Instantiate(EndingObj, null);
            obj.transform.localPosition = new Vector3(-22.5f, -22.5f, 0);
            minimapObj.SetActive(false);
            TutorialText.SetActive(false);
        }
        IEnumerator TutorialSetting()
        {
            Grain pGrain;
            pVolume.profile.TryGetSettings(out pGrain);
            pGrain.active = false;
            isAjit = false;
            Clear();
            if (HeroObj != null) Destroy(HeroObj);
            yield return null;
            GameObject obj = Instantiate(TutorialMap[nowTutorialNum].obj, null);
            obj.transform.localPosition = new Vector3(-22.5f, -22.5f, 0);

            GameObject h = Instantiate(hero, obj.transform.position + (Vector3)TutorialMap[nowTutorialNum].heroSpawnPos, Quaternion.identity);
            HeroObj = h;
            h.name = "Hero";
            HeroObj.GetComponent<Player>().isTutorial = true;
            Camera[] cameras = FindObjectsOfType<Camera>();
            for (int i = 0; i < cameras.Length; i++)
                cameras[i].target = h.transform;

            Inventorys.Instance.DefenceGuage_LB.SetActive(false);
            Inventorys.Instance.DefenceGuage_RB.SetActive(false);
            Inventorys.Instance.DefenceGuage_Q.SetActive(false);
            SoundManager.instance.StartAudio(new string[1] { "BGM/Tutorial" }, VOLUME_TYPE.BGM, null, true);

        }
        public void StateCreate(string stateName, GameObject offObj, GameObject animObj)
        {
            SoundManager.instance.StartAudio(new string[2] { "Click_0", "dungeon_insert" }, VOLUME_TYPE.EFFECT);
            if (offObj) offObj.SetActive(false);
            StartCoroutine(StateCreateCoroutine(animObj, stateName));
        }
        IEnumerator StateCreateCoroutine(GameObject animObj, string _stateName)
        {
            if (Player.instance.isSubWeapon) Player.instance.pTimer.SetActive(false);
            HeroObj.SetActive(false);
            if (animObj)
            {
                animObj.SetActive(true);
                animObj.GetComponent<Animator>().Rebind();
            }
            yield return new WaitForSecondsRealtime(2.0f);
            SoundManager.instance.StopAudio(new string[1] { "dungeon_insert" });
            HeroObj.SetActive(true);
            if (Player.instance.isSubWeapon) Player.instance.pTimer.SetActive(true);
            if (_stateName.Equals("Tutorial")) GoTutorial();
            else Create();
        }
        public void DieCreate(GameObject obj)
        {
            obj.transform.Find("Button").GetComponent<Button>().interactable = false;
            obj.SetActive(false);
            Create();
        }
        public void Create()
        {
            if (CreateCoroutine != null)
            {
                StopCoroutine(CreateCoroutine);
                CreateCoroutine = null;
            }
            Clear();
            if (CreateCoroutine == null) CreateCoroutine = StartCoroutine(Loading());
        }
        IEnumerator Loading()
        {
            StateGroup.instance.ClearValue();
            while (true)
            {
                if (GameObject.Find("Canvas").transform.Find("New Game Object")) Destroy(GameObject.Find("Canvas").transform.Find("New Game Object").gameObject);
                else if (GameObject.Find("Canvas").transform.Find("Tutorial(Clone)")) Destroy(GameObject.Find("Canvas").transform.Find("Tutorial(Clone)").gameObject);
                else
                    break;
                yield return null;
            }

            TextOpenController.instance.TextClose(Color.white);
            SoundManager.instance.StopBGM();
            nDungeonComputer5Count = 0;
            isDungeonComputer = false;
            isDungeonComputer2 = false;
            isDungeonComputer3 = false;
            isDungeonComputer4 = false;
            isDungeonComputer5 = false;
            Canvas.transform.Find("PressKey").gameObject.SetActive(false);
            Canvas.transform.Find("PressKeyHealth").gameObject.SetActive(false);
            Canvas.transform.Find("PressKeySwitch").gameObject.SetActive(false);
            Canvas.transform.Find("PressKeyGuide").gameObject.SetActive(false);
            Canvas.transform.Find("PressKeyFire").gameObject.SetActive(false);
            LoadingClear = false;
            isAjit = false;
            LoadingBar.fillAmount = 0.0f;
            LoadingText.text = LocalizeManager.GetLocalize("loadingbar_text_0");
            if (!LoadingObj.activeSelf)
            {
                int n = 0;
                for (int i = 0; i < stageStepObj.Count; i++)
                {
                    stageStepObj[i].GetComponent<Image>().sprite = stageDefaultSprite;
                }
                for (int i = 0; i < stageStepObj.Count; i++)
                {
                    string tName = stageStepObj[i].name;
                    n = (StageLevel * (stageCount + 1)) + nowStageStep;
                    if (i <= n)
                    {
                        if (tName.Contains("boss"))
                        {
                            stageStepObj[i].GetComponent<Image>().sprite = bossMarkSprites[stageBossRan[i / (stageCount + 1)]];
                        }
                        else
                        {
                            stageStepObj[i].GetComponent<Image>().sprite = stageMarkSprites[stageRan[i / (stageCount + 1)]];
                        }
                    }
                    else
                        break;
                }
                LoadingObj.SetActive(true);
                Vector3 pos = stageStepObj[n].GetComponent<RectTransform>().anchoredPosition3D;
                float x = pos.x - LoadingCharacter.anchoredPosition3D.x;
                x /= 2.0f;
                while (true)
                {
                    LoadingCharacter.anchoredPosition3D = new Vector3(LoadingCharacter.anchoredPosition3D.x + 2, LoadingCharacter.anchoredPosition3D.y);
                    if (LoadingCharacter.anchoredPosition3D.x >= pos.x)
                    {
                        LoadingCharacter.anchoredPosition3D = new Vector3(pos.x, LoadingCharacter.anchoredPosition3D.y);
                        break;
                    }
                    LoadingBar.fillAmount += 0.2f / x;
                    //LoadingText.text = Mathf.Ceil(LoadingBar.fillAmount * 100) + "%";
                    yield return null;
                }
            }
            LoadingBar.fillAmount = 0.2f;
            LoadingText.text = LocalizeManager.GetLocalize("loadingbar_text_0");
            Clear();
            yield return null;
            GameObject map = new GameObject
            {
                name = "Dungeon"
            };
            itemParent = map.transform;
            Rooms();
            Connect();
            StageName = "Dungeon_" + stageRan[StageLevel];
            stageDatas = System.Array.FindAll(SheetManager.Instance.StageDB.dataArray, item => item.Type1.Equals(StageName));
            FixedPrefabsRooms();
            yield return null;
            LoadingBar.fillAmount = 0.3f;
            LoadingText.text = LocalizeManager.GetLocalize("loadingbar_text_1");

            Smooth();
            yield return null;
            LoadingBar.fillAmount = 0.4f;
            LoadingText.text = LocalizeManager.GetLocalize("loadingbar_text_2");

            Walls();
            yield return null;
            LoadingBar.fillAmount = 0.5f;
            LoadingText.text = LocalizeManager.GetLocalize("loadingbar_text_3");

            Structure();
            yield return null;
            LoadingBar.fillAmount = 0.6f;
            LoadingText.text = LocalizeManager.GetLocalize("loadingbar_text_4");
            Columns();
            LoadingBar.fillAmount = 0.7f;
            LoadingText.text = LocalizeManager.GetLocalize("loadingbar_text_5");

            Fountains();
            yield return null;
            Columns();
            LoadingBar.fillAmount = 0.8f;
            LoadingText.text = LocalizeManager.GetLocalize("loadingbar_text_6");

            if (!isSaveMap) Chests();
            yield return null;
            LoadingBar.fillAmount = 0.90f;
            LoadingText.text = LocalizeManager.GetLocalize("loadingbar_text_7");

            SpawnHero();
            yield return null;
            LoadingBar.fillAmount = 0.93f;
            LoadingText.text = LocalizeManager.GetLocalize("loadingbar_text_8");
            if (!isSaveMap) SpawnMonster();
            yield return null;
            LoadingBar.fillAmount = 0.95f;
            LoadingText.text = LocalizeManager.GetLocalize("loadingbar_text_9");

            Generate();
            yield return null;
            LoadingBar.fillAmount = 1.0f;
            LoadingText.text = LocalizeManager.GetLocalize("loadingbar_text_10");
            CreateCoroutine = null;
            Astar.Scan(Astar.graphs[0]);
            yield return new WaitForSecondsRealtime(1.5f);
            if (LoadingObj.activeSelf) LoadingObj.SetActive(false);
            if (ResultPanelScript.instance != null) ResultPanelScript.instance.areaName = LocalizeManager.GetLocalize("stage_title_" + stageRan[StageLevel]);
            minimapObj.SetActive(true);
            spawnBat = Instantiate(BatObj);
            LoadingClear = true;
            GameObject.Find("Canvas").transform.Find("TutorialText").gameObject.SetActive(true);
            GameObject.Find("Canvas").transform.Find("TutorialText").GetComponent<Text>().text = LocalizeManager.GetLocalize("tutorial_move");
            GameObject.Find("Canvas").transform.Find("MiniMap_OutLine").gameObject.SetActive(true);
            SoundManager.instance.StartAudio(new string[1] { "BGM/Dungeon_" + stageRan[StageLevel] }, VOLUME_TYPE.BGM, null, true);

            for (int i = 0; i < WorldMapSimbols.Length; i++)
            {
                WorldMapSimbols[i].SetActive(true);
            }
            WorldMapSimbols[6].SetActive(false);
            WorldMapSimbols[7].SetActive(false);
            WorldMapSimbols[8].SetActive(false);
            WorldMapSimbols[12].SetActive(false);
            WorldMapSimbols[13].SetActive(false);
            WorldMapSimbols[14].SetActive(false);
            WorldMapSimbols[21].SetActive(false);
            WorldMapSimbols[22].SetActive(false);

            if (SteamStatsAndAchievements.instance != null) SteamStatsAndAchievements.instance.SettingStat("stat_play_count");
            if (SteamStatsAndAchievements.instance != null) SteamStatsAndAchievements.instance.SettingStat("stat_stage_entrance_count_" + stageRan[StageLevel]);

            if (isMercenaryAjit)
            {
                MercenaryObj = Instantiate(Resources.Load<GameObject>("Mercenary/Mercenary"));
                MercenaryObj.SetActive(false);
            }
            StateGroup.instance.isClearTimeStart = true;
        }
        public IEnumerator BossRush()
        {
            SaveAndLoadManager.instance.DeleteManufacture(1);
            yield return new WaitForSeconds(0.2f);
            TextOpenController.instance.TextClose(Color.white);
            if (GameObject.Find("Canvas").transform.Find("TutorialText2").gameObject.activeSelf)
                GameObject.Find("Canvas").transform.Find("TutorialText2").gameObject.SetActive(false);
            while (true)
            {
                if (GameObject.Find("Canvas").transform.Find("New Game Object")) Destroy(GameObject.Find("Canvas").transform.Find("New Game Object").gameObject);
                else if (GameObject.Find("Canvas").transform.Find("Tutorial(Clone)")) Destroy(GameObject.Find("Canvas").transform.Find("Tutorial(Clone)").gameObject);
                else
                    break;
                yield return null;
            }
        }
        public void Clear()
        {
            if (GameObject.Find("Canvas").transform.Find("TutorialText2").gameObject.activeSelf)
                GameObject.Find("Canvas").transform.Find("TutorialText2").gameObject.SetActive(false);
            isGoblinSpawn = false;
            newObjectSelected = false;
            newObjectSelectedObj = null;
            publicListObj.Clear();
            publicListTime.Clear();
            publicListObj2.Clear();
            publicListTime2.Clear();
            tempNext = null;
            isBuffTower = false;
            nBuffTower = 0;
            isOpnerSetting = false;
            isOpnePartSetting = false;
            isOpenWarp = false;
            isGetWarpPart = false;
            warpType = Random.Range(0, warpTypeMax);
            isEliteSpawn = false;
            UnityEngine.Camera.main.orthographicSize = 8;
            isShopFlag = false;
            nowRoomType = string.Empty;
            isEleteObjInstall = false;
            isTotemInstall = false;
            Camera _camera = FindObjectOfType(typeof(Camera)) as Camera;
            _camera.bossIndex = -1;

            isEleteObjInstall = Random.Range(0, 100) >= 50 ? false : true;

            stageDatas = null;
            matrix = GetComponent<Matrix>();
            matrix.rows = width;
            matrix.columns = height;
            matrix.map = new GameObject[matrix.rows + 2, matrix.columns + 2];

            board = GameObject.Find("Board").GetComponent<Matrix>();
            board.rows = matrix.rows;
            board.columns = matrix.columns;
            board.map = new GameObject[board.rows + 2, board.columns + 2];

            rooms = Random.Range(numRooms.m_Min, numRooms.m_Max);
            chestsCount = 0;
            chestsCount2 = 0;
            healthCount = 0;
            HealthPosition = new Vector2[maxHealth];
            nowChests = Random.Range(minChests, maxChests);
            nowChests2 = Random.Range(minChests2, maxChests2);
            ChestPosition = new Vector2[nowChests + nowChests2];
            for (int x = 0; x < matrix.rows; x++)
            {
                for (int y = 0; y < matrix.columns; y++)
                {
                    matrix.map[x, y] = null;
                    board.map[x, y] = null;
                }
            }
            SpriteRenderer[] tiles = FindObjectsOfType<SpriteRenderer>();
            for (int i = 0; i < tiles.Length; i++)
            {
                if (tiles[i]) Destroy(tiles[i].gameObject);
            }
            for (int i = 0; i < RoomInfo.Count; i++)
            {
                if (RoomInfo[i].doorObj != null)
                {
                    for (int j = 0; j < RoomInfo[i].doorObj.Count; j++)
                    {
                        if (RoomInfo[i].doorObj[j] != null) Destroy(RoomInfo[i].doorObj[j].gameObject);
                    }
                }

            }
            for (int i = 0; i < SpawnMonsters.Count; i++)
            {
                SpawnMonsters[i].SetActive(false);
            }
            for (int i = 0; i < WarpSpawnMonsters.Count; i++)
            {
                WarpSpawnMonsters[i].SetActive(false);
            }
            for (int i = 0; i < BuildSpawn.Count; i++)
            {
                Destroy(BuildSpawn[i].gameObject);
            }
            for (int i = 0; i < FixedRoomMiniMap.Count; i++)
            {
                Destroy(FixedRoomMiniMap[i].gameObject);
            }
            for (int i = 0; i < TeleportList.Count; i++)
            {
                Destroy(TeleportList[i].gameObject);
            }
            for (int i = 0; i < HpList.Count; i++)
            {
                Destroy(HpList[i].gameObject);
            }
            HpList.Clear();
            TeleportList.Clear();
            FixedRoomMiniMap.Clear();
            RoomInfo.Clear();
            BuildSpawn.Clear();
            WarpSpawnMonsters.Clear();
            SpawnMonsters.Clear();
            Inventorys.Instance.shopOpenItems.Clear();
            for (int i = 0; i < FogList.Count; i++)
            {
                Destroy(FogList[i]);
            }
            FogList.Clear();
            if (teleportLineObj) Destroy(teleportLineObj);

            GameObject lastDungeon = GameObject.Find("Dungeon");
            if (lastDungeon != null) Destroy(lastDungeon);

        }
        private void Generate()
        {
            List<Vector2Int> fList = new List<Vector2Int>();
            List<Vector2Int> fList2 = new List<Vector2Int>();
            for (int x = 0; x < matrix.rows; x++)
            {
                for (int y = 0; y < matrix.columns; y++)
                {
                    //if (board.map[x, y] == chestPrefab)
                    //{
                    //    GameObject go = Instantiate(chestPrefab, new Vector3(x, y, 0), Quaternion.identity);
                    //    RoomObjInsert(x, y, go);
                    //    go.transform.SetParent(itemParent);
                    //    go.GetComponent<Chest>().BoxSetting(StageName);
                    //}
                    if (board.map[x, y] == chestPrefab2)
                    {
                        GameObject go = Instantiate(chestPrefab2, new Vector3(x, y, 0), Quaternion.identity);
                        RoomObjInsert(x, y, go);
                        go.transform.SetParent(itemParent);
                        //go.GetComponent<ChestHit>().BoxSetting(StageName);
                    }
                    if (board.map[x, y] == healthPrefab)
                    {
                        GameObject go = Instantiate(healthPrefab, new Vector3(x, y, 0), Quaternion.identity);
                        RoomObjInsert(x, y, go);
                        go.transform.SetParent(itemParent);
                    }
                    if (matrix.map[x, y] != null)
                    {
                        GameObject go = null;
                        if (matrix.map[x, y] == exit_0 || matrix.map[x, y] == exit_1 || matrix.map[x, y] == exit_2 || matrix.map[x, y] == exit_3)
                        {
                            GameObject warp = Instantiate(matrix.map[x, y], new Vector3(x, y, 0), Quaternion.identity);
                            if (GetRoomIndex(x, y) < rooms)
                            {
                                go = Instantiate(floor_1, new Vector3(x, y, 0), Quaternion.identity);
                                warp.GetComponent<Exit>().isNormal = true;
                            }
                            warp.transform.SetParent(itemParent);
                            RoomObjInsert(x, y, warp);
                            BuildSpawn.Add(warp);
                        }
                        else
                        {
                            if (matrix.map[x, y] == start) continue;
                            if (matrix.map[x, y] == floor_2) continue;
                            if (matrix.map[x, y] == floor_1)
                            {
                                fList.Add(new Vector2Int(x, y));
                                continue;
                            }
                            if (matrix.map[x, y] == wall_mid)
                            {
                                fList2.Add(new Vector2Int(x, y));
                                continue;
                            }
                            go = Instantiate(matrix.map[x, y], new Vector3(x, y, 0), Quaternion.identity);
                            for (int i = 0; i < _MapPiece.Length; i++)
                            {
                                if (matrix.map[x, y] == _MapPiece[i].obj && _MapPiece[i].mapType == "bossstage")
                                {
                                    List<MonstersData> StageMonster = new List<MonstersData>();
                                    for (int j = 0; j < SheetManager.Instance.MonsterDB.dataArray.Length; j++)
                                    {
                                        if (SheetManager.Instance.MonsterDB.dataArray[j].Type.Equals("boss"))
                                        {
                                            for (int k = 0; k < SheetManager.Instance.MonsterDB.dataArray[j].Targetstage.Length; k++)
                                            {
                                                if (SheetManager.Instance.MonsterDB.dataArray[j].Targetstage[k].Equals(StageName))
                                                {
                                                    StageMonster.Add(SheetManager.Instance.MonsterDB.dataArray[j]);
                                                    break;
                                                }
                                            }
                                        }
                                    }
                                    break;
                                }
                            }
                            for (int i = 0; i < _MapPiece.Length; i++)
                            {
                                if (matrix.map[x, y] == _MapPiece[i].obj)
                                {
                                    GameObject newObj = new GameObject();
                                    SpriteRenderer sr = newObj.AddComponent<SpriteRenderer>();
                                    sr.sprite = _MapPiece[i]._minimapSprite;
                                    newObj.layer = 18;
                                    newObj.transform.position = go.transform.position;
                                    newObj.name = _MapPiece[i].id;
                                    FixedRoomMiniMap.Add(newObj);
                                    newObj.AddComponent<PolygonCollider2D>();
                                    newObj.transform.SetParent(go.transform);
                                    if (_MapPiece[i].id.Equals("boss_waiting_2")) newObj.transform.localPosition = new Vector3(0, 1, 0);
                                    break;
                                }
                            }
                        }
                        if (matrix.map[x, y] == wall_left || matrix.map[x, y] == wall_right || matrix.map[x, y] == wall_mid)
                        {
                            SpriteRenderer sr = go.transform.GetChild(0).GetComponent<SpriteRenderer>();
                            if (sr != null)
                            {
                                if (matrix.map[x, y - 1] != floor_1)
                                {
                                    sr.color = new Color32(0, 0, 0, 128);
                                }
                            }
                        }
                        if (go != null)
                        {
                            RoomObjInsert(x, y, go);
                            go.transform.SetParent(itemParent);
                        }
                    }
                }
            }
            bool[] fCheck = new bool[fList.Count];
            while (true)
            {
                int cRan = Random.Range(0, fCheck.Length);
                while (true)
                {
                    if (fCheck[cRan]) cRan = Random.Range(0, fCheck.Length);
                    if (!fCheck[cRan]) break;
                }
                int x = fList[cRan].x;
                int y = fList[cRan].y;
                GameObject go = Instantiate(matrix.map[x, y], new Vector3(x, y), Quaternion.identity);
                RoomObjInsert(x, y, go);
                go.transform.SetParent(itemParent);
                fCheck[cRan] = true;
                bool c = false;
                for (int i = 0; i < fCheck.Length; i++)
                {
                    if (!fCheck[i]) c = true;
                }
                if (!c) break;
            }
            bool[] fCheck2 = new bool[fList2.Count];
            while (true)
            {
                int cRan = Random.Range(0, fCheck2.Length);
                while (true)
                {
                    if (fCheck2[cRan]) cRan = Random.Range(0, fCheck2.Length);
                    if (!fCheck2[cRan]) break;
                }
                int x = fList2[cRan].x;
                int y = fList2[cRan].y;
                GameObject go = Instantiate(matrix.map[x, y], new Vector3(x, y), Quaternion.identity);
                RoomObjInsert(x, y, go);
                go.transform.SetParent(itemParent);
                fCheck2[cRan] = true;
                bool c = false;
                for (int i = 0; i < fCheck2.Length; i++)
                {
                    if (!fCheck2[i]) c = true;
                }
                if (!c) break;
            }

            for (int i = rooms; i < RoomInfo.Count; i++)
            {
                if (RoomInfo[i].EntranceWarps == null) continue;
                for (int j = 0; j < RoomInfo[i].EntranceWarps.Length; j++)
                {
                    Vector2 pos = RoomInfo[i].EntranceWarps[j];
                    int mask = 1 << 19;
                    RaycastHit2D hit = Physics2D.BoxCast(pos, new Vector2(0.5f, 0.5f), 0, Vector2.zero, 0, mask);
                    if (hit)
                    {
                        if (hit.collider.GetComponent<Exit>() && RoomInfo[i].rooMType.Equals("startstage"))
                        {
                            hit.collider.GetComponent<Exit>().isLock = false;
                            hit.collider.GetComponent<Exit>().roomNum = i;
                        }
                        else if (hit.collider.GetComponent<Exit>())
                        {
                            hit.collider.GetComponent<Exit>().roomNum = i;
                        }
                    }
                }
                for (int j = 0; j < RoomInfo[i].ExitWarps.Length; j++)
                {
                    Vector2 pos = RoomInfo[i].ExitWarps[j];
                    int mask = 1 << 19;
                    RaycastHit2D hit = Physics2D.BoxCast(pos, new Vector2(0.5f, 0.5f), 0, Vector2.zero, 0, mask);
                    if (hit)
                    {
                        if (hit.collider.GetComponent<Exit>() && RoomInfo[i].rooMType.Equals("startstage"))
                        {
                            hit.collider.GetComponent<Exit>().isLock = false;
                            hit.collider.GetComponent<Exit>().roomNum = i;
                        }
                        else if (hit.collider.GetComponent<Exit>())
                        {
                            hit.collider.GetComponent<Exit>().roomNum = i;
                        }
                    }
                }
            }
            for (int i = rooms; i < RoomInfo.Count; i++)
            {
                if (RoomInfo[i].rooMType.Equals("startstage"))
                {
                    Roominfos info = RoomInfo[i];
                    info.isClear = true;
                    RoomInfo[i] = info;
                    RoomInsert(i);
                    continue;
                }
                RoomClose(i);
            }
        }
        #endregion Initialization
        #region Generation
        private void Rooms()
        {
            try
            {
                int attemps = 0;

                for (int i = 0; i < rooms; i++)
                {
                    int X, Y, horizontal, vertical;
                    do
                    {
                        X = Random.Range(80, matrix.rows - 80);
                        Y = Random.Range(80, matrix.columns - 80);
                        horizontal = Random.Range(roomWidth.m_Min, roomWidth.m_Max);
                        vertical = Random.Range(roomHeight.m_Min, roomHeight.m_Min);

                        attemps++;
                        if (attemps > 1000)
                        {
                            LauncError("Insufficient space!", "Rooms");
                            break;
                        }

                    } while (!Avaiable(X, Y, horizontal, vertical));
                    for (int x = X; x < X + horizontal; x++) for (int y = Y; y < Y + vertical; y++) matrix.map[x, y] = floor_1;

                    Roominfos info = new Roominfos
                    {
                        width = horizontal,
                        height = vertical,
                        x = X,
                        y = Y,
                        roomNum = i,
                        EntranceWarps = null,
                        ExitWarps = null,
                        isExitCheck = false,
                        isClear = false,
                        rooMType = "Normal",
                        doorObj = null,
                        roomID = "normalstage",
                        roomObjs = new System.Collections.Generic.List<GameObject>()
                    };
                    RoomInfo.Add(info);
                    switch (Random.Range(0, 4))
                    {
                        case 0: matrix.map[Random.Range(X, X + horizontal - 1), Y] = exit; break;
                        case 1: matrix.map[Random.Range(X, X + horizontal - 1), Y + vertical - 1] = exit; break;
                        case 2: matrix.map[X, Random.Range(Y, Y + vertical - 1)] = exit; break;
                        case 3: matrix.map[X + horizontal - 1, Random.Range(Y, Y + vertical - 1)] = exit; break;
                    }
                }
                SavePoints();
            }
            catch (System.Exception e)
            {
                string msg = e.Message;
                Create();
            }
        }
        private void FixedPrefabsRooms()
        {
            try
            {
                int hiddenNumber = GetCurrentHiddenMap();
                int roomCount = rooms;
                for (int i = 0; i < stageDatas.Length; i++)
                {
                    if (stageDatas[i].Type2.Equals("normalstage")) continue;
                    if (stageDatas[i].Type2.Equals("cheststage")) continue;
                    if (stageDatas[i].Type2.Equals("hiddenstage") /*&& hiddenNumber != i*/) continue;

                    string roomID = string.Empty;
                    if (stageDatas[i].Type2.Equals("bossstage") && !isStageBoss)
                        roomID = "d01_normalend_0" + (stageRan[StageLevel] + 1);
                    else if (stageDatas[i].Type2.Equals("bossstage") && isStageBoss)
                        roomID = "d01_bossstage_0" + (stageBossRan[StageLevel] + 1);
                    else
                        roomID = (stageDatas[i].Roomid.Length == 0 ? string.Empty : (stageDatas[i].Roomid.Length == 1 ? stageDatas[i].Roomid[0] : stageDatas[i].Roomid[Random.Range(0, stageDatas[i].Roomid.Length)]));
                    int roomIndex = System.Array.FindIndex(_MapPiece, item => item.id.Equals(roomID));
                    if (roomIndex == -1) continue;

                    int attemps = 0;
                    MapPiece piece = _MapPiece[roomIndex];

                    int X, Y, horizontal, vertical;
                    do
                    {
                        X = Random.Range(4, matrix.rows - 4);
                        Y = Random.Range(4, matrix.columns - 4);
                        horizontal = piece.x;
                        vertical = piece.y;

                        attemps++;
                        if (attemps > 1000)
                        {
                            LauncError("Insufficient space!", "FixedPrefabsRooms");
                            break;
                        }

                    } while (!Avaiable(X, Y, horizontal, vertical));
                    for (int x = X; x < X + horizontal; x++) for (int y = Y; y < Y + vertical; y++) matrix.map[x, y] = floor_2;
                    Roominfos info = new Roominfos
                    {
                        width = horizontal,
                        height = vertical,
                        x = X,
                        y = Y,
                        roomNum = roomCount,
                        ExitWarps = new Vector2[4],
                        EntranceWarps = new Vector2[4],
                        isExitCheck = false,
                        isClear = false,
                        rooMType = stageDatas[i].Type2,
                        roomID = roomID,
                        doorObj = new System.Collections.Generic.List<GameObject>(),
                        roomObjs = new System.Collections.Generic.List<GameObject>()
                    };
                    roomCount++;
                    int centerX = (X + (X + horizontal)) / 2;
                    int centerY = (Y + (Y + vertical)) / 2;
                    matrix.map[centerX, centerY] = piece.obj;
                    int doorIndex = Random.Range(0, piece.doorPos.Length);
                    int x2 = centerX + Mathf.FloorToInt(piece.doorPos[doorIndex].x);
                    int y2 = centerY + Mathf.FloorToInt(piece.doorPos[doorIndex].y);
                    matrix.map[x2 + 0, y2 + 0] = exit_0;
                    matrix.map[x2 + 1, y2 + 0] = exit_1;
                    matrix.map[x2 + 0, y2 - 1] = exit_2;
                    matrix.map[x2 + 1, y2 - 1] = exit_3;
                    info.EntranceWarps[0] = new Vector2(x2 + 0, y2 + 0);
                    info.EntranceWarps[1] = new Vector2(x2 + 1, y2 + 0);
                    info.EntranceWarps[2] = new Vector2(x2 + 0, y2 - 1);
                    info.EntranceWarps[3] = new Vector2(x2 + 1, y2 - 1);
                    if (stageDatas[i].Type2.Equals("bossstage") && isStageBoss)
                    {
                        BossWaitingRoomMake(ref roomCount, new Vector2(centerX, Y));
                        waitingExit = new Vector2(x2 + 1, y2 + 0);
                        InsertExit(ref info, new Vector2(X, Y), true);
                    }
                    else
                    {
                        InsertExit(ref info, new Vector2(X, Y), false);
                    }
                    RoomInfo.Add(info);
                }
            }
            catch (System.Exception e)
            {
                string msg = e.Message;
                Create();
            }
        }
        private void BossWaitingRoomMake(ref int roomCount, Vector2 _centerPos)
        {
            int roomIndex = System.Array.FindIndex(_MapPiece, item => item.id.Equals("boss_waiting_" + (stageBossRan[StageLevel] + 1)));
            int attemps = 0;
            MapPiece piece = _MapPiece[roomIndex];

            int X, Y, horizontal, vertical;
            do
            {
                X = (int)_centerPos.x;
                Y = (int)_centerPos.y - Mathf.FloorToInt(piece.y * 1.5f);
                horizontal = piece.x;
                vertical = piece.y;

                attemps++;
                if (attemps > 1000)
                {
                    LauncError("Insufficient space!", "BossWaitingRoomMake");
                    break;
                }

            } while (!Avaiable(X, Y, horizontal, vertical));
            for (int x = X; x < X + horizontal; x++) for (int y = Y; y < Y + vertical; y++) matrix.map[x, y] = floor_2;
            Roominfos info = new Roominfos
            {
                width = horizontal,
                height = vertical,
                x = X,
                y = Y,
                roomNum = roomCount,
                ExitWarps = null,
                EntranceWarps = null,
                isExitCheck = false,
                isClear = false,
                rooMType = "boss_waiting",
                roomID = "boss_waiting",
                doorObj = new System.Collections.Generic.List<GameObject>(),
                roomObjs = new System.Collections.Generic.List<GameObject>()
            };
            roomCount++;
            int centerX = (X + (X + horizontal)) / 2;
            int centerY = (Y + (Y + vertical)) / 2;
            waitingRoomCenter = new Vector2(centerX, centerY);
            matrix.map[centerX, centerY] = piece.obj;
            RoomInfo.Add(info);
        }
        private void FixedTypeRooms()
        {
            try
            {
                int attemps = 0;
                int roomCount = rooms;
                /// End Room
                {
                    int ran = 0;
                    int X, Y, horizontal, vertical;
                    do
                    {
                        X = Random.Range(2, matrix.rows - 2);
                        Y = Random.Range(2, matrix.columns - 2);
                        horizontal = Random.Range((ran == 0 ? endRoomWidth.m_Min : endRoomHeight.m_Min), (ran == 0 ? endRoomWidth.m_Max : endRoomHeight.m_Min));
                        vertical = Random.Range((ran == 0 ? endRoomHeight.m_Min : endRoomWidth.m_Min), (ran == 0 ? endRoomHeight.m_Max : endRoomWidth.m_Min));

                        attemps++;
                        if (attemps > 1000)
                        {
                            LauncError("Insufficient space!", "FixedTypeRooms");
                            break;
                        }

                    } while (!Avaiable(X, Y, horizontal, vertical));
                    for (int x = X; x < X + horizontal; x++) for (int y = Y; y < Y + vertical; y++) matrix.map[x, y] = floor_1;

                    Roominfos info = new Roominfos
                    {
                        width = horizontal,
                        height = vertical,
                        x = X,
                        y = Y,
                        roomNum = roomCount,
                        ExitWarps = new Vector2[4],
                        EntranceWarps = new Vector2[4],
                        isExitCheck = false,
                        isClear = false,
                        rooMType = "End",
                        doorObj = new System.Collections.Generic.List<GameObject>(),
                        roomObjs = new System.Collections.Generic.List<GameObject>()
                    };
                    roomCount++;

                    int centerX = (X + (X + horizontal)) / 2;
                    int centerY = (Y + (Y + vertical)) / 2;
                    int mapCenterX = width / 2;
                    int mapCenterY = height / 2;

                    Direction d = Direction.None;
                    if (centerX > mapCenterX && centerY > mapCenterY) d = Direction.RightUp;
                    else if (centerX > mapCenterX && centerY <= mapCenterY) d = Direction.RightDown;
                    else if (centerX <= mapCenterX && centerY > mapCenterY) d = Direction.LeftUp;
                    else if (centerX <= mapCenterX && centerY <= mapCenterY) d = Direction.LeftDown;
                    int cutDir = CenterCut(X, Y, horizontal, vertical, 0.7f, ran, d, out Vector2 current, ref info, true);
                    InsertEntrance(current, d, ref info);
                    RoomInfo.Add(info);
                }

                /// Box Room
                for (int i = 0; i < BoxRoomCount; i++)
                {
                    int ran = 0;
                    int X, Y, horizontal, vertical;
                    do
                    {
                        X = Random.Range(2, matrix.rows - 2);
                        Y = Random.Range(2, matrix.columns - 2);
                        horizontal = Random.Range((i == 0 ? BoxRoomWidth.m_Min + 10 : BoxRoomWidth.m_Min), (i == 0 ? BoxRoomWidth.m_Max + 10 : BoxRoomWidth.m_Max));
                        vertical = Random.Range((i == 0 ? BoxRoomHeight.m_Min : BoxRoomHeight.m_Min), (ran == 0 ? BoxRoomHeight.m_Max : BoxRoomHeight.m_Max));

                        attemps++;
                        if (attemps > 1000)
                        {
                            LauncError("Insufficient space!", "FixedTypeRooms");
                            break;
                        }

                    } while (!Avaiable(X, Y, horizontal, vertical));
                    for (int x = X; x < X + horizontal; x++) for (int y = Y; y < Y + vertical; y++) matrix.map[x, y] = floor_1;

                    Roominfos info = new Roominfos
                    {
                        width = horizontal,
                        height = vertical,
                        x = X,
                        y = Y,
                        roomNum = roomCount,
                        ExitWarps = new Vector2[4],
                        EntranceWarps = new Vector2[4],
                        isExitCheck = false,
                        isClear = false,
                        rooMType = (i == 0 ? "Shop" : "Box"),
                        doorObj = new System.Collections.Generic.List<GameObject>(),
                        roomObjs = new System.Collections.Generic.List<GameObject>()
                    };
                    roomCount++;
                    int centerX = (X + (X + horizontal)) / 2;
                    int centerY = (Y + (Y + vertical)) / 2;

                    int mapCenterX = width / 2;
                    int mapCenterY = height / 2;

                    Direction d = Direction.None;
                    if (centerX > mapCenterX && centerY > mapCenterY) d = Direction.RightUp;
                    else if (centerX > mapCenterX && centerY <= mapCenterY) d = Direction.RightDown;
                    else if (centerX <= mapCenterX && centerY > mapCenterY) d = Direction.LeftUp;
                    else if (centerX <= mapCenterX && centerY <= mapCenterY) d = Direction.LeftDown;

                    int cutDir = CenterCut(X, Y, horizontal, vertical, 0.4f, ran, d, out Vector2 current, ref info, true);
                    InsertEntrance(current, d, ref info);
                    if (i == 0)
                    {
                        ShopSpawn(ref info);
                    }
                    RoomInfo.Add(info);
                }

                /// Start Room
                {
                    int X, Y, horizontal, vertical;
                    do
                    {
                        X = Random.Range(2, matrix.rows - 2);
                        Y = Random.Range(2, matrix.columns - 2);
                        horizontal = Random.Range(startRoomWidth.m_Min, startRoomWidth.m_Max);
                        vertical = Random.Range(startRoomHeight.m_Min, startRoomHeight.m_Min);

                        attemps++;
                        if (attemps > 1000)
                        {
                            LauncError("Insufficient space!", "FixedTypeRooms");
                            break;
                        }

                    } while (!Avaiable(X, Y, horizontal, vertical));
                    for (int x = X; x < X + horizontal; x++) for (int y = Y; y < Y + vertical; y++) matrix.map[x, y] = floor_1;

                    Roominfos info = new Roominfos
                    {
                        width = horizontal,
                        height = vertical,
                        x = X,
                        y = Y,
                        roomNum = roomCount,
                        ExitWarps = new Vector2[4],
                        EntranceWarps = new Vector2[4],
                        isExitCheck = false,
                        isClear = true,
                        rooMType = "Start",
                        doorObj = null,
                        roomObjs = new System.Collections.Generic.List<GameObject>()
                    };
                    roomCount++;
                    int centerX = (X + (X + horizontal)) / 2;
                    int centerY = (Y + (Y + vertical)) / 2;
                    Vector2 ranPos = new Vector2(centerX, centerY);
                    InsertEntrance(ranPos, Direction.None, ref info);
                    RoomInfo.Add(info);
                }
            }
            catch (System.Exception e)
            {
                string msg = e.Message;
                Create();
            }
        }
        private int GetCurrentHiddenMap()
        {
            System.Collections.Generic.List<int> hiddenList = new System.Collections.Generic.List<int>();
            System.Collections.Generic.List<int> hiddenList2 = new System.Collections.Generic.List<int>();
            for (int i = 0; i < stageDatas.Length; i++)
            {
                if (stageDatas[i].Type2.Equals("hiddenstage"))
                {
                    if (SaveAndLoadManager.instance.LoadNPC("reinforce") && stageDatas[i].Roomid[0].Equals("d01_reinforce_01")) continue;
                    else if (SaveAndLoadManager.instance.LoadNPC("mr_dark") && stageDatas[i].Roomid[0].Equals("d01_mr_dark_01")) continue;
                    else if (SaveAndLoadManager.instance.LoadNPC("hellDog") && stageDatas[i].Roomid[0].Equals("d01_hell_dog_01")) continue;
                    hiddenList.Add(i);
                    hiddenList2.Add((int)(stageDatas[i].Percentage * 100));
                }
            }

            int[] per = new int[100 * hiddenList.Count];
            for (int i = 0; i < per.Length; i++) per[i] = -1;

            for (int i = 0; i < hiddenList.Count; i++)
            {
                for (int j = 0; j < hiddenList2[i]; j++)
                {
                    per[(i * 100) + j] = hiddenList[i];
                }
            }
            if (per.Length == 0) return -1;
            return per[Random.Range(0, per.Length)];
        }
        private void Connect()
        {
            try
            {
                int x, y;
                Vector2 current, target;
                Vector2 direction = Vector2.zero;

                for (int web = 0; web < webCount; web++)
                {
                    for (int i = 0; i < rooms - 1; i++)
                    {
                        current = pointsList[i];
                        target = pointsList[i + 1];

                        do
                        {
                            if (target.x > current.x) direction = Vector2.right;
                            else if (target.x < current.x) direction = Vector2.left;
                            else if (target.y > current.y) direction = Vector2.up;
                            else if (target.y < current.y) direction = Vector2.down;
                            else direction = Vector2.zero;

                            x = (int)current.x + (int)direction.x;
                            y = (int)current.y + (int)direction.y;
                            if (Find(x, y, null))
                            {
                                if (direction == Vector2.right || direction == Vector2.left)
                                {
                                    matrix.map[x, y + 1] = floor_1;
                                    matrix.map[x, y - 1] = floor_1;
                                }
                                if (direction == Vector2.up || direction == Vector2.down)
                                {
                                    matrix.map[x + 1, y] = floor_1;
                                    matrix.map[x - 1, y] = floor_1;
                                }
                                matrix.map[x, y] = floor_1;
                            }
                            current = new Vector2(x, y);
                        } while (direction != Vector2.zero);
                    }

                    Vector2[] tmpList = pointsList;
                    for (int i = 0; i < tmpList.Length; i++)
                    {
                        pointsList[Random.Range(0, pointsList.Length)] = tmpList[i];
                    }
                }
            }
            catch (System.Exception e)
            {
                LauncError(e.Message, "Connect");
            }
        }
        private void Smooth()
        {
            for (int i = 0; i < 3; i++)
            {
                for (int x = 0; x < matrix.rows; x++)
                {
                    for (int y = 0; y < matrix.columns; y++)
                    {
                        if (matrix.map[x, y] == null)
                        {
                            if (Find(x + 1, y, floor_1) && Find(x - 1, y, floor_1)
                                && Find(x, y + 1, floor_1) && Find(x, y - 1, floor_1))
                                matrix.map[x, y] = floor_1;

                            if (Find(x + 1, y, floor_1) && Find(x - 1, y, floor_1))
                                matrix.map[x, y] = floor_1;

                            if (Find(x, y + 1, floor_1) && Find(x, y - 1, floor_1))
                                matrix.map[x, y] = floor_1;

                            if (Find(x, y + 1, null) && Find(x, y + 2, floor_1) && Find(x, y - 1, floor_1))
                            {
                                matrix.map[x, y + 1] = floor_1;
                                matrix.map[x, y] = floor_1;
                            }
                        }
                    }
                }
            }
        }
        private void Walls()
        {
            for (int x = 0; x < matrix.rows; x++)
            {
                for (int y = 0; y < matrix.columns; y++)
                {
                    if (matrix.map[x, y] == floor_1)
                    {
                        if (Find(x, y + 1, null)) matrix.map[x, y + 1] = wall_mid;
                        if (Find(x, y - 1, null)) matrix.map[x, y - 1] = wall_mid;
                    }
                }
            }
        }
        private void Structure()
        {
            for (int x = 0; x < matrix.rows; x++)
            {
                for (int y = 0; y < matrix.columns; y++)
                {
                    if (matrix.map[x, y] == wall_mid)
                    {
                        if (Find(x - 1, y, floor_1)
                            && Find(x - 1, y - 1, floor_1)
                            && Find(x, y - 1, floor_1))
                            matrix.map[x, y] = wall_left;
                        if (Find(x + 1, y, floor_1)
                            && Find(x + 1, y - 1, floor_1)
                            && Find(x, y - 1, floor_1))
                            matrix.map[x, y] = wall_right;

                        if (Find(x - 1, y, null)
                            && Find(x - 1, y - 1, null)
                            && Find(x, y - 1, null))
                        {
                            GameObject go = Instantiate(wall_side_front_left, new Vector3(x - 1, y, 0), Quaternion.identity);
                            RoomObjInsert(x, y, go);
                            board.map[x, y] = wall_side_front_left;
                            go.transform.SetParent(itemParent);
                            if (Find(x - 1, y, null))
                            {
                                go = Instantiate(wall_corner_bottom_2, new Vector3(x - 1, y + 1, 0), Quaternion.identity);
                                RoomObjInsert(x, y, go);
                                go.transform.SetParent(itemParent);
                            }
                        }
                        if (Find(x + 1, y, null)
                            && Find(x + 1, y - 1, null)
                            && Find(x, y - 1, null))
                        {
                            GameObject go = Instantiate(wall_side_front_right, new Vector3(x + 1, y, 0), Quaternion.identity);
                            RoomObjInsert(x, y, go);
                            board.map[x, y] = wall_side_front_right;
                            go.transform.SetParent(itemParent);
                            if (Find(x + 1, y, null))
                            {
                                go = Instantiate(wall_corner_bottom_1, new Vector3(x + 1, y + 1, 0), Quaternion.identity);
                                RoomObjInsert(x, y, go);
                                go.transform.SetParent(itemParent);
                            }
                        }
                    }
                }
            }
            for (int x = 0; x < matrix.rows; x++)
            {
                for (int y = 0; y < matrix.columns; y++)
                {
                    if (matrix.map[x, y] == wall_mid)
                    {
                        if (Find(x - 1, y, null) && !Find(x, y - 1, null))
                        {
                            GameObject go = Instantiate(wall_side_mid_left, new Vector3(x - 1, y, 0), Quaternion.identity);
                            RoomObjInsert(x, y, go);
                            go.transform.SetParent(itemParent);
                        }
                        if (Find(x + 1, y, null) && !Find(x, y - 1, null))
                        {
                            GameObject go = Instantiate(wall_side_mid_right, new Vector3(x + 1, y, 0), Quaternion.identity);
                            RoomObjInsert(x, y, go);
                            go.transform.SetParent(itemParent);
                        }

                        if (Find(x, y - 1, null) && (Find(x - 1, y - 1, floor_1) || Find(x - 1, y - 1, wall_mid)))
                        {
                            GameObject go = Instantiate(wall_side_mid_right, new Vector3(x, y, 0), Quaternion.identity);
                            RoomObjInsert(x, y, go);
                            go.transform.SetParent(itemParent);
                        }
                        if (Find(x, y - 1, null) && (Find(x + 1, y - 1, floor_1) || Find(x + 1, y - 1, wall_mid)))
                        {
                            GameObject go = Instantiate(wall_side_mid_left, new Vector3(x, y, 0), Quaternion.identity);
                            RoomObjInsert(x, y, go);
                            go.transform.SetParent(itemParent);
                        }

                        if (!Find(x - 1, y, floor_1) && !Find(x + 1, y, floor_1))
                        {
                            GameObject go = Instantiate(wall_top_mid, new Vector3(x, y + 1, 0), Quaternion.identity);
                            RoomObjInsert(x, y, go);
                            go.transform.SetParent(itemParent);
                        }
                        else if (Find(x - 1, y, floor_1))
                        {
                            GameObject go = Instantiate(wall_corner_top_left, new Vector3(x, y + 1, 0), Quaternion.identity);
                            RoomObjInsert(x, y, go);
                            go.transform.SetParent(itemParent);
                        }
                        else if (Find(x + 1, y, floor_1))
                        {
                            GameObject go = Instantiate(wall_corner_top_right, new Vector3(x, y + 1, 0), Quaternion.identity);
                            RoomObjInsert(x, y, go);
                            go.transform.SetParent(itemParent);
                        }
                    }
                    else if (matrix.map[x, y] == wall_left)
                    {
                        if (Find(x + 1, y, null))
                        {
                            GameObject go_ = Instantiate(wall_side_mid_right, new Vector3(x + 1, y, 0), Quaternion.identity);
                            RoomObjInsert(x, y, go_);
                            go_.transform.SetParent(itemParent);
                        }

                        GameObject go = Instantiate(wall_corner_bottom_left, new Vector3(x, y + 1, 0), Quaternion.identity);
                        RoomObjInsert(x, y, go);
                        go.transform.SetParent(itemParent);
                    }
                    else if (matrix.map[x, y] == wall_right)
                    {
                        if (Find(x - 1, y, null))
                        {
                            GameObject go_ = Instantiate(wall_side_mid_left, new Vector3(x - 1, y, 0), Quaternion.identity);
                            RoomObjInsert(x, y, go_);
                            go_.transform.SetParent(itemParent);
                        }

                        GameObject go = Instantiate(wall_corner_bottom_right, new Vector3(x, y + 1, 0), Quaternion.identity);
                        RoomObjInsert(x, y, go);
                        go.transform.SetParent(itemParent);
                    }
                    else if (matrix.map[x, y] == floor_1)
                    {
                        if (Find(x - 1, y, null))
                        {
                            GameObject go = Instantiate(wall_side_mid_left, new Vector3(x - 1, y, 0), Quaternion.identity);
                            RoomObjInsert(x, y, go);
                            go.transform.SetParent(itemParent);
                        }
                        if (Find(x + 1, y, null))
                        {
                            GameObject go = Instantiate(wall_side_mid_right, new Vector3(x + 1, y, 0), Quaternion.identity);
                            RoomObjInsert(x, y, go);
                            go.transform.SetParent(itemParent);
                        }

                        if (Find(x - 1, y + 2, null) && (Find(x, y + 1, wall_mid) || Find(x, y + 1, wall_left) || Find(x, y + 1, wall_right)))
                        {
                            GameObject go = Instantiate(wall_side_top_left, new Vector3(x - 1, y + 2, 0), Quaternion.identity);
                            RoomObjInsert(x, y, go);
                            go.transform.SetParent(itemParent);
                        }
                        if (Find(x + 1, y + 2, null) && (Find(x, y + 1, wall_mid) || Find(x, y + 1, wall_left) || Find(x, y + 1, wall_right)))
                        {
                            GameObject go = Instantiate(wall_side_top_right, new Vector3(x + 1, y + 2, 0), Quaternion.identity);
                            RoomObjInsert(x, y, go);
                            go.transform.SetParent(itemParent);
                        }
                    }
                }
            }
        }
        private void Columns()
        {
            for (int x = matrix.rows - 1; x > 0; x--)
            {
                for (int y = 0; y < matrix.columns; y++)
                {

                    if (matrix.map[x, y] == wall_mid)
                    {
                        if (!Find(x - 1, y, wall_column_mid) && !Find(x + 1, y, wall_column_mid) && Find(x, y - 1, floor_1))
                        {
                            bool check = false;
                            for (int i = 0; i < RoomInfo.Count; i++)
                            {
                                if (RoomInfo[i].x <= x && RoomInfo[i].x + RoomInfo[i].width >= x && RoomInfo[i].y <= y && RoomInfo[i].y + RoomInfo[i].height >= y)
                                {
                                    if (RoomInfo[i].rooMType.Equals("Box"))
                                    {
                                        check = true;
                                        break;
                                    }
                                    else break;
                                }
                            }
                            if (check) continue;
                            int ran = Random.Range(0, 100);
                            if (ran >= 20 && ran <= 60) continue;
                            matrix.map[x, y + 1] = wall_column_top;
                            matrix.map[x, y] = wall_column_mid;
                            matrix.map[x, y - 1] = wall_column_base;
                        }
                    }
                }
            }
        }
        #endregion Generation
        #region Extras
        private int CenterCut(int X, int Y, int horizontal, int vertical, float per, int type, Direction dir, out Vector2 exitPos, ref Roominfos _info, bool isBox = false)
        {
            exitPos = Vector2.zero;
            if (isBox) /// 아래 자르기
            {
                for (int i = X; i < X + horizontal; i++)
                {
                    if (i != X + (horizontal / 2))
                    {
                        matrix.map[i, (Y + vertical) - (Mathf.FloorToInt(vertical * per))] = null;
                        matrix.map[i, (Y + vertical) - (Mathf.FloorToInt(vertical * per)) + 1] = null;
                        matrix.map[i, (Y + vertical) - (Mathf.FloorToInt(vertical * per)) - 1] = null;
                    }
                    else
                    {
                        GameObject obj = Instantiate(door_horizontal, new Vector3(i, (Y + vertical) - (Mathf.FloorToInt(vertical * per)) + 2 + 0.25f, 0), Quaternion.identity, itemParent);
                        obj.transform.SetParent(itemParent);
                        BuildSpawn.Add(obj);
                        _info.doorObj.Add(obj);
                        _info.roomObjs.Add(obj);
                        obj = Instantiate(door_horizontal, new Vector3(i, (Y + vertical) - (Mathf.FloorToInt(vertical * per)) + 0 + 0.25f, 0), Quaternion.identity, itemParent);
                        obj.transform.SetParent(itemParent);
                        BuildSpawn.Add(obj);
                        _info.doorObj.Add(obj);
                        _info.roomObjs.Add(obj);
                        exitPos = new Vector2(i, Y);
                    }
                }
                return (int)Direction.Down;
            }
            else if ((dir == Direction.LeftDown || dir == Direction.RightDown) && type == 0)  /// 위 자르기
            {
                for (int i = X; i < X + horizontal; i++)
                {
                    if (i != X + (horizontal / 2))
                    {
                        matrix.map[i, Y + (Mathf.FloorToInt(vertical * per))] = null;
                        matrix.map[i, Y + (Mathf.FloorToInt(vertical * per)) - 1] = null;
                        matrix.map[i, Y + (Mathf.FloorToInt(vertical * per)) + 1] = null;
                    }
                    else
                    {
                        GameObject obj = Instantiate(door_horizontal, new Vector3(i, Y + (Mathf.FloorToInt(vertical * per)) + 2 + 0.25f, 0), Quaternion.identity, itemParent);
                        obj.transform.SetParent(itemParent);
                        BuildSpawn.Add(obj);
                        _info.doorObj.Add(obj);
                        _info.roomObjs.Add(obj);
                        obj = Instantiate(door_horizontal, new Vector3(i, Y + (Mathf.FloorToInt(vertical * per)) + 0 + 0.25f, 0), Quaternion.identity, itemParent);
                        obj.transform.SetParent(itemParent);
                        BuildSpawn.Add(obj);
                        _info.doorObj.Add(obj);
                        _info.roomObjs.Add(obj);
                        exitPos = new Vector2(i, Y + vertical);
                    }
                }

                return (int)Direction.Up;
            }
            else if ((dir == Direction.LeftUp || dir == Direction.RightUp) && type == 0) /// 아래 자르기
            {
                for (int i = X; i < X + horizontal; i++)
                {
                    if (i != X + (horizontal / 2))
                    {
                        matrix.map[i, (Y + vertical) - (Mathf.FloorToInt(vertical * per))] = null;
                        matrix.map[i, (Y + vertical) - (Mathf.FloorToInt(vertical * per)) + 1] = null;
                        matrix.map[i, (Y + vertical) - (Mathf.FloorToInt(vertical * per)) - 1] = null;
                    }
                    else
                    {
                        GameObject obj = Instantiate(door_horizontal, new Vector3(i, (Y + vertical) - (Mathf.FloorToInt(vertical * per)) + 2 + 0.25f, 0), Quaternion.identity, itemParent);
                        obj.transform.SetParent(itemParent);
                        BuildSpawn.Add(obj);
                        _info.doorObj.Add(obj);
                        _info.roomObjs.Add(obj);
                        obj = Instantiate(door_horizontal, new Vector3(i, (Y + vertical) - (Mathf.FloorToInt(vertical * per)) + 0 + 0.25f, 0), Quaternion.identity, itemParent);
                        obj.transform.SetParent(itemParent);
                        BuildSpawn.Add(obj);
                        _info.doorObj.Add(obj);
                        _info.roomObjs.Add(obj);
                        exitPos = new Vector2(i, Y);
                    }
                }
                return (int)Direction.Down;
            }
            else if ((dir == Direction.LeftUp || dir == Direction.LeftDown) && type == 1) /// 오른쪽 자르기
            {
                for (int i = Y; i < Y + vertical; i++)
                {
                    if (i != Y + (vertical / 2))
                    {
                        matrix.map[X + (Mathf.FloorToInt(horizontal * per)), i] = null;
                        matrix.map[X + (Mathf.FloorToInt(horizontal * per)) - 1, i] = null;
                        matrix.map[X + (Mathf.FloorToInt(horizontal * per)) + 1, i] = null;
                    }
                    else
                    {
                        exitPos = new Vector2(X + horizontal, i);
                    }
                }
                return (int)Direction.Right;
            }
            else if ((dir == Direction.RightUp || dir == Direction.RightDown) && type == 1) /// 왼쪽 자르기
            {
                for (int i = Y; i < Y + vertical; i++)
                {
                    if (i != Y + (vertical / 2))
                    {
                        matrix.map[(X + horizontal) - (Mathf.FloorToInt(horizontal * per)), i] = null;
                        matrix.map[(X + horizontal) - (Mathf.FloorToInt(horizontal * per)) + 1, i] = null;
                        matrix.map[(X + horizontal) - (Mathf.FloorToInt(horizontal * per)) - 1, i] = null;
                    }
                    else
                    {
                        exitPos = new Vector2(X, i);
                    }
                }
                return (int)Direction.Left;
            }
            return 0;
        }
        private void Fountains()
        {
            for (int x = 0; x < matrix.rows; x++)
            {
                for (int y = 0; y < matrix.columns; y++)
                {
                    if (matrix.map[x, y] == wall_mid)
                    {
                        if (Random.Range(0, 60) == 0 && Find(x, y - 1, floor_1))
                        {
                            matrix.map[x, y] = fountain_blue;
                        }
                        else if (Random.Range(0, 150) == 0 && Find(x, y - 1, floor_1))
                        {
                            matrix.map[x, y] = fountain_red;
                        }
                    }
                }
            }
        }
        private void InsertEntrance(Vector2 exitPos, Direction d, ref Roominfos info)
        {
            Vector2 dir;
            if (d == Direction.Right) dir = Vector2.right;
            else if (d == Direction.Left) dir = Vector2.up;
            else if (d == Direction.Up) dir = Vector2.left;
            else if (d == Direction.Down) dir = Vector2.down;
            else dir = Vector2.zero;

            int x = 0; int y = 0;
            int attemps = 0;
            do
            {
                x = Random.Range((int)exitPos.x - 10, (int)exitPos.x + 10);
                y = Random.Range((int)exitPos.y + 3, (int)exitPos.y + 15);
                attemps++;
                if (attemps > 1000)
                {
                    LauncError("Insufficient space!", "InsertEntrance");
                    break;
                }
            } while (
            !(matrix.map[x, y] == floor_1 && matrix.map[x + 1, y] == floor_1 && matrix.map[x, y - 1] == floor_1 && matrix.map[x + 1, y - 1] == floor_1
                && matrix.map[x - 1, y] == floor_1 && matrix.map[x - 1, y - 1] == floor_1 && matrix.map[x, y - 2] == floor_1 && matrix.map[x + 1, y - 2] == floor_1
                && matrix.map[x + 2, y] == floor_1 && matrix.map[x + 2, y - 1] == floor_1 && matrix.map[x, y + 1] == floor_1 && matrix.map[x + 1, y + 1] == floor_1
            ) ||
            !(matrix.map[x, y] == floor_1 && matrix.map[x - 1, y] == floor_1 && matrix.map[x, y - 1] == floor_1 && matrix.map[x - 1, y - 1] == floor_1
                && matrix.map[x - 2, y] == floor_1 && matrix.map[x - 2, y - 1] == floor_1 && matrix.map[x - 1, y - 2] == floor_1 && matrix.map[x, y - 2] == floor_1
                && matrix.map[x + 1, y] == floor_1 && matrix.map[x + 1, y - 1] == floor_1 && matrix.map[x - 1, y + 1] == floor_1 && matrix.map[x, y + 1] == floor_1
            ) ||
            !(matrix.map[x, y] == floor_1 && matrix.map[x + 1, y] == floor_1 && matrix.map[x, y + 1] == floor_1 && matrix.map[x + 1, y + 1] == floor_1
                && matrix.map[x - 1, y] == floor_1 && matrix.map[x - 1, y + 1] == floor_1 && matrix.map[x, y - 1] == floor_1 && matrix.map[x + 1, y - 1] == floor_1
                && matrix.map[x + 2, y - 1] == floor_1 && matrix.map[x + 2, y] == floor_1 && matrix.map[x, y + 2] == floor_1 && matrix.map[x + 1, y + 2] == floor_1
            ) ||
            !(matrix.map[x, y] == floor_1 && matrix.map[x - 1, y] == floor_1 && matrix.map[x, y + 1] == floor_1 && matrix.map[x - 1, y + 1] == floor_1)
                && matrix.map[x - 2, y] == floor_1 && matrix.map[x - 2, y + 1] == floor_1 && matrix.map[x, y - 1] == floor_1 && matrix.map[x - 1, y - 1] == floor_1
                && matrix.map[x + 1, y - 1] == floor_1 && matrix.map[x + 1, y] == floor_1 && matrix.map[x, y + 2] == floor_1 && matrix.map[x - 1, y + 2] == floor_1
            );

            if (matrix.map[x, y] == floor_1 && matrix.map[x + 1, y] == floor_1 && matrix.map[x, y - 1] == floor_1 && matrix.map[x + 1, y - 1] == floor_1)
            {
                matrix.map[x + 0, y + 0] = exit_0;
                matrix.map[x + 1, y + 0] = exit_1;
                matrix.map[x + 0, y - 1] = exit_2;
                matrix.map[x + 1, y - 1] = exit_3;
                info.EntranceWarps[0] = new Vector2(x + 0, y + 0);
                info.EntranceWarps[1] = new Vector2(x + 1, y + 0);
                info.EntranceWarps[2] = new Vector2(x + 0, y - 1);
                info.EntranceWarps[3] = new Vector2(x + 1, y - 1);
            }
            else if (matrix.map[x, y] == floor_1 && matrix.map[x - 1, y] == floor_1 && matrix.map[x, y - 1] == floor_1 && matrix.map[x - 1, y - 1] == floor_1)
            {
                matrix.map[x - 1, y + 0] = exit_0;
                matrix.map[x + 0, y + 0] = exit_1;
                matrix.map[x - 1, y - 1] = exit_2;
                matrix.map[x + 0, y - 1] = exit_3;
                info.EntranceWarps[0] = new Vector2(x - 1, y + 0);
                info.EntranceWarps[1] = new Vector2(x + 0, y + 0);
                info.EntranceWarps[2] = new Vector2(x - 1, y - 1);
                info.EntranceWarps[3] = new Vector2(x + 0, y - 1);
            }
            else if (matrix.map[x, y] == floor_1 && matrix.map[x + 1, y] == floor_1 && matrix.map[x, y + 1] == floor_1 && matrix.map[x + 1, y + 1] == floor_1)
            {
                matrix.map[x + 0, y + 1] = exit_0;
                matrix.map[x + 1, y + 1] = exit_1;
                matrix.map[x + 0, y + 0] = exit_2;
                matrix.map[x + 1, y + 0] = exit_3;
                info.EntranceWarps[0] = new Vector2(x + 0, y + 1);
                info.EntranceWarps[1] = new Vector2(x + 1, y + 1);
                info.EntranceWarps[2] = new Vector2(x + 0, y + 0);
                info.EntranceWarps[3] = new Vector2(x + 1, y + 0);
            }
            else if (matrix.map[x, y] == floor_1 && matrix.map[x - 1, y] == floor_1 && matrix.map[x, y + 1] == floor_1 && matrix.map[x - 1, y + 1] == floor_1)
            {
                matrix.map[x - 1, y + 1] = exit_0;
                matrix.map[x + 0, y + 1] = exit_1;
                matrix.map[x - 1, y + 0] = exit_2;
                matrix.map[x + 0, y + 0] = exit_3;
                info.EntranceWarps[0] = new Vector2(x - 1, y + 1);
                info.EntranceWarps[1] = new Vector2(x + 0, y + 1);
                info.EntranceWarps[2] = new Vector2(x - 1, y + 0);
                info.EntranceWarps[3] = new Vector2(x + 0, y + 0);
            }
            InsertExit(ref info, exitPos);
        }
        private void InsertExit(ref Roominfos info, Vector2 current, bool _isBoss = false)
        {
            int minIndex = -1;
            float minDepth = float.MaxValue;
            for (int i = 0; i < rooms; i++)
            {
                if (RoomInfo[i].isExitCheck) continue;
                int centerX = (RoomInfo[i].x + (RoomInfo[i].x + RoomInfo[i].width)) / 2;
                int centerY = (RoomInfo[i].y + (RoomInfo[i].y + RoomInfo[i].height)) / 2;
                Vector2 roomCenter = new Vector2(centerX, centerY);
                float dis = Vector3.Distance(current, roomCenter);

                if (dis < minDepth)
                {
                    minDepth = dis;
                    minIndex = i;
                }
            }
            Roominfos _info = RoomInfo[minIndex];
            _info.isExitCheck = true;
            RoomInfo[minIndex] = _info;

            int x = 0; int y = 0;
            int attemps = 0;
            do
            {
                x = Random.Range(RoomInfo[minIndex].x, RoomInfo[minIndex].x + RoomInfo[minIndex].width);
                y = Random.Range(RoomInfo[minIndex].y, RoomInfo[minIndex].y + RoomInfo[minIndex].height);
                attemps++;
                if (attemps > 1000)
                {
                    LauncError("Insufficient space!", "InsertExit");
                    break;
                }
            } while (
            !(matrix.map[x, y] == floor_1 && matrix.map[x + 1, y] == floor_1 && matrix.map[x, y - 1] == floor_1 && matrix.map[x + 1, y - 1] == floor_1
                && matrix.map[x - 1, y] == floor_1 && matrix.map[x - 1, y - 1] == floor_1 && matrix.map[x, y - 2] == floor_1 && matrix.map[x + 1, y - 2] == floor_1
                && matrix.map[x + 2, y] == floor_1 && matrix.map[x + 2, y - 1] == floor_1 && matrix.map[x, y + 1] == floor_1 && matrix.map[x + 1, y + 1] == floor_1
            ) ||
            !(matrix.map[x, y] == floor_1 && matrix.map[x - 1, y] == floor_1 && matrix.map[x, y - 1] == floor_1 && matrix.map[x - 1, y - 1] == floor_1
                && matrix.map[x - 2, y] == floor_1 && matrix.map[x - 2, y - 1] == floor_1 && matrix.map[x - 1, y - 2] == floor_1 && matrix.map[x, y - 2] == floor_1
                && matrix.map[x + 1, y] == floor_1 && matrix.map[x + 1, y - 1] == floor_1 && matrix.map[x - 1, y + 1] == floor_1 && matrix.map[x, y + 1] == floor_1
            ) ||
            !(matrix.map[x, y] == floor_1 && matrix.map[x + 1, y] == floor_1 && matrix.map[x, y + 1] == floor_1 && matrix.map[x + 1, y + 1] == floor_1
                && matrix.map[x - 1, y] == floor_1 && matrix.map[x - 1, y + 1] == floor_1 && matrix.map[x, y - 1] == floor_1 && matrix.map[x + 1, y - 1] == floor_1
                && matrix.map[x + 2, y - 1] == floor_1 && matrix.map[x + 2, y] == floor_1 && matrix.map[x, y + 2] == floor_1 && matrix.map[x + 1, y + 2] == floor_1
            ) ||
            !(matrix.map[x, y] == floor_1 && matrix.map[x - 1, y] == floor_1 && matrix.map[x, y + 1] == floor_1 && matrix.map[x - 1, y + 1] == floor_1)
                && matrix.map[x - 2, y] == floor_1 && matrix.map[x - 2, y + 1] == floor_1 && matrix.map[x, y - 1] == floor_1 && matrix.map[x - 1, y - 1] == floor_1
                && matrix.map[x + 1, y - 1] == floor_1 && matrix.map[x + 1, y] == floor_1 && matrix.map[x, y + 2] == floor_1 && matrix.map[x - 1, y + 2] == floor_1
            );

            if (matrix.map[x, y] == floor_1 && matrix.map[x + 1, y] == floor_1 && matrix.map[x, y - 1] == floor_1 && matrix.map[x + 1, y - 1] == floor_1)
            {
                matrix.map[x + 0, y + 0] = exit_0;
                matrix.map[x + 1, y + 0] = exit_1;
                matrix.map[x + 0, y - 1] = exit_2;
                matrix.map[x + 1, y - 1] = exit_3;
                info.ExitWarps[0] = new Vector2(x + 0, y + 0);
                info.ExitWarps[1] = new Vector2(x + 1, y + 0);
                info.ExitWarps[2] = new Vector2(x + 0, y - 1);
                info.ExitWarps[3] = new Vector2(x + 1, y - 1);
            }
            else if (matrix.map[x, y] == floor_1 && matrix.map[x - 1, y] == floor_1 && matrix.map[x, y - 1] == floor_1 && matrix.map[x - 1, y - 1] == floor_1)
            {
                matrix.map[x - 1, y + 0] = exit_0;
                matrix.map[x + 0, y + 0] = exit_1;
                matrix.map[x - 1, y - 1] = exit_2;
                matrix.map[x + 0, y - 1] = exit_3;
                info.ExitWarps[0] = new Vector2(x - 1, y + 0);
                info.ExitWarps[1] = new Vector2(x + 0, y + 0);
                info.ExitWarps[2] = new Vector2(x - 1, y - 1);
                info.ExitWarps[3] = new Vector2(x + 0, y - 1);
            }
            else if (matrix.map[x, y] == floor_1 && matrix.map[x + 1, y] == floor_1 && matrix.map[x, y + 1] == floor_1 && matrix.map[x + 1, y + 1] == floor_1)
            {
                matrix.map[x + 0, y + 1] = exit_0;
                matrix.map[x + 1, y + 1] = exit_1;
                matrix.map[x + 0, y + 0] = exit_2;
                matrix.map[x + 1, y + 0] = exit_3;
                info.ExitWarps[0] = new Vector2(x + 0, y + 1);
                info.ExitWarps[1] = new Vector2(x + 1, y + 1);
                info.ExitWarps[2] = new Vector2(x + 0, y + 0);
                info.ExitWarps[3] = new Vector2(x + 1, y + 0);
            }
            else if (matrix.map[x, y] == floor_1 && matrix.map[x - 1, y] == floor_1 && matrix.map[x, y + 1] == floor_1 && matrix.map[x - 1, y + 1] == floor_1)
            {
                matrix.map[x - 1, y + 1] = exit_0;
                matrix.map[x + 0, y + 1] = exit_1;
                matrix.map[x - 1, y + 0] = exit_2;
                matrix.map[x + 0, y + 0] = exit_3;
                info.ExitWarps[0] = new Vector2(x - 1, y + 1);
                info.ExitWarps[1] = new Vector2(x + 0, y + 1);
                info.ExitWarps[2] = new Vector2(x - 1, y + 0);
                info.ExitWarps[3] = new Vector2(x + 0, y + 0);
            }
            if (_isBoss) waitingEntrance = info.ExitWarps[1];
        }
        private void Chests()
        {
            try
            {
                for (int i = 0; i < rooms; i++)
                {
                    for (int x = RoomInfo[i].x; x < RoomInfo[i].x + RoomInfo[i].width; x++)
                    {
                        for (int y = RoomInfo[i].y + RoomInfo[i].height - 5; y < RoomInfo[i].y + RoomInfo[i].height; y++)
                        {
                            if (matrix.map[x, y] == floor_1 && board.map[x, y] != chestPrefab && board.map[x, y] != chestPrefab2 &&
                                (Find(x, y + 1, wall_mid)) &&
                                (Find(x - 1, y + 1, wall_mid) || Find(x - 1, y + 1, wall_left) || Find(x - 1, y + 1, wall_right) || Find(x - 1, y + 1, wall_column_mid)) &&
                                (Find(x + 1, y + 1, wall_mid) || Find(x + 1, y + 1, wall_left) || Find(x + 1, y + 1, wall_right) || Find(x - 1, y + 1, wall_column_mid)) &&
                                matrix.map[x, y - 2] == floor_1 &&
                                matrix.map[x + 1, y - 2] == floor_1 &&
                                matrix.map[x + 2, y - 2] == floor_1 &&
                                matrix.map[x - 1, y - 2] == floor_1 &&
                                matrix.map[x, y - 1] == floor_1 &&
                                matrix.map[x + 1, y - 1] == floor_1 &&
                                matrix.map[x + 2, y - 1] == floor_1 &&
                                matrix.map[x - 1, y - 1] == floor_1 &&
                                matrix.map[x - 2, y - 1] == floor_1 &&
                                (
                                board.map[x - 1, y] != chestPrefab &&
                                board.map[x - 2, y] != chestPrefab &&
                                board.map[x - 3, y] != chestPrefab &&
                                board.map[x - 4, y] != chestPrefab &&
                                board.map[x + 1, y] != chestPrefab &&
                                board.map[x + 2, y] != chestPrefab &&
                                board.map[x + 3, y] != chestPrefab &&
                                board.map[x + 4, y] != chestPrefab
                                ) &&
                                (
                                board.map[x - 1, y] != chestPrefab2 &&
                                board.map[x - 2, y] != chestPrefab2 &&
                                board.map[x - 3, y] != chestPrefab2 &&
                                board.map[x - 4, y] != chestPrefab2 &&
                                board.map[x + 1, y] != chestPrefab2 &&
                                board.map[x + 2, y] != chestPrefab2 &&
                                board.map[x + 3, y] != chestPrefab2 &&
                                board.map[x + 4, y] != chestPrefab2
                                )
                               )
                            {
                                //if (chestsCount < maxChests)
                                //{
                                //    ChestPosition[chestsCount + chestsCount2] = new Vector2(x, y);
                                //    board.map[x, y] = chestPrefab;
                                //    chestsCount++;
                                //}
                                //else 
                                if (chestsCount2 < nowChests2)
                                {
                                    ChestPosition[chestsCount2] = new Vector2(x, y);
                                    board.map[x, y] = chestPrefab2;
                                    chestsCount2++;
                                }
                                else if (chestsCount < nowChests)
                                {
                                    List<MonstersData> StageMonster = new List<MonstersData>();
                                    for (int j = 0; j < SheetManager.Instance.MonsterDB.dataArray.Length; j++)
                                    {
                                        if (!SheetManager.Instance.MonsterDB.dataArray[j].Type.Equals("normal")) continue;
                                        for (int k = 0; k < SheetManager.Instance.MonsterDB.dataArray[j].Targetstage.Length; k++)
                                        {
                                            if (SheetManager.Instance.MonsterDB.dataArray[j].Targetstage[k].Equals("Box") &&
                                                    StageLevel == 0 ? !SheetManager.Instance.MonsterDB.dataArray[j].ID.Contains("_t") :
                                                    SheetManager.Instance.MonsterDB.dataArray[j].ID.Contains("_t" + (StageLevel + 1)))
                                            {
                                                StageMonster.Add(SheetManager.Instance.MonsterDB.dataArray[j]);
                                                break;
                                            }
                                        }
                                    }
                                    MonstersData mobData = StageMonster[Random.Range(0, StageMonster.Count)];
                                    StageData sData = System.Array.Find(SheetManager.Instance.StageDB.dataArray, item => item.Type2.Equals("normalstage") && item.Type1.Equals(StageName));
                                    GameObject obj = ObjManager.Call().GetObject("Monster");
                                    if (obj == null)
                                    {
                                        LauncError("Insufficient space!", "Chests");
                                        return;
                                    }
                                    obj.transform.position = new Vector3(x, y, 0);
                                    obj.SetActive(true);
                                    obj.GetComponent<MonsterArggro>().InitSetting(HeroObj, mobData, Random.Range(10, 15), false, mobData.Animtype, false);
                                    board.map[x, y] = chestPrefab;
                                    chestsCount++;
                                }
                            }
                        }
                    }
                }
                for (int i = 0; i < rooms; i++)
                {
                    for (int x = RoomInfo[i].x; x < RoomInfo[i].x + RoomInfo[i].width; x++)
                    {
                        for (int y = RoomInfo[i].y + RoomInfo[i].height - 5; y < RoomInfo[i].y + RoomInfo[i].height; y++)
                        {
                            if (matrix.map[x, y] == floor_1 && board.map[x, y] != chestPrefab && board.map[x, y] != chestPrefab2 &&
                                (Find(x, y + 1, wall_mid)) && (Find(x - 1, y + 1, wall_mid) || Find(x - 1, y + 1, wall_left) || Find(x - 1, y + 1, wall_right) || Find(x - 1, y + 2, wall_column_mid)) &&
                                (Find(x + 1, y + 1, wall_mid) || Find(x + 1, y + 1, wall_left) || Find(x + 1, y + 1, wall_right) || Find(x - 1, y + 2, wall_column_mid)) &&
                                matrix.map[x, y - 2] == floor_1 &&
                                matrix.map[x + 1, y - 2] == floor_1 &&
                                matrix.map[x + 2, y - 2] == floor_1 &&
                                matrix.map[x - 1, y - 2] == floor_1 &&
                                matrix.map[x - 2, y - 2] == floor_1 &&
                                (
                                board.map[x - 1, y] != chestPrefab &&
                                board.map[x - 2, y] != chestPrefab &&
                                board.map[x - 3, y] != chestPrefab &&
                                board.map[x - 4, y] != chestPrefab &&
                                board.map[x + 1, y] != chestPrefab &&
                                board.map[x + 2, y] != chestPrefab &&
                                board.map[x + 3, y] != chestPrefab &&
                                board.map[x + 4, y] != chestPrefab
                                ) &&
                                (
                                board.map[x - 1, y] != chestPrefab2 &&
                                board.map[x - 2, y] != chestPrefab2 &&
                                board.map[x - 3, y] != chestPrefab2 &&
                                board.map[x - 4, y] != chestPrefab2 &&
                                board.map[x + 1, y] != chestPrefab2 &&
                                board.map[x + 2, y] != chestPrefab2 &&
                                board.map[x + 3, y] != chestPrefab2 &&
                                board.map[x + 4, y] != chestPrefab2
                                ) &&
                                (board.map[x - 1, y] != healthPrefab && board.map[x + 1, y] != healthPrefab) &&
                                (board.map[x - 2, y] != healthPrefab && board.map[x + 2, y] != healthPrefab) &&
                                (board.map[x - 3, y] != healthPrefab && board.map[x + 3, y] != healthPrefab) &&
                                (board.map[x - 4, y] != healthPrefab && board.map[x + 4, y] != healthPrefab) &&
                                (board.map[x - 5, y] != healthPrefab && board.map[x + 5, y] != healthPrefab)
                                )
                            {
                                if (healthCount < maxHealth)
                                {
                                    HealthPosition[healthCount] = new Vector2(x, y);
                                    board.map[x, y] = healthPrefab;
                                    healthCount++;
                                }
                            }
                        }
                    }
                }

                if (healthCount < 1) LauncError("Lack is healthtower!!!", "Lack is healthtower!!!");
            }
            catch (System.Exception e)
            {
                LauncError(e.Message, "Lack is healthtower!!!");
            }
        }
        private void SpawnHero()
        {
            if (hero == null) return;
            Roominfos info = RoomInfo.Find(item => item.rooMType.Equals("startstage"));
            int centerX = (info.x + (info.x + info.width)) / 2;
            int centerY = (info.y + (info.y + info.height)) / 2;
            GameObject h = Instantiate(hero, new Vector3(centerX, centerY - 0.25F, 0), Quaternion.identity);
            HeroObj = h;
            h.name = "Hero";
            Camera[] cameras = FindObjectsOfType<Camera>();
            for (int i = 0; i < cameras.Length; i++)
                cameras[i].target = h.transform;
        }
        public void SpawnHero(GameObject obj, ref Vector2 target)
        {
            int x = 0; int y = 0;
            do
            {
                x = Random.Range(RoomInfo[RoomInfo.Count - 1].x, RoomInfo[RoomInfo.Count - 1].x + RoomInfo[RoomInfo.Count - 1].width);
                y = Random.Range(RoomInfo[RoomInfo.Count - 1].y, RoomInfo[RoomInfo.Count - 1].y + RoomInfo[RoomInfo.Count - 1].height);
            } while (matrix.map[x, y] != floor_1
            || !Find(x + 1, y, floor_1) || !Find(x - 1, y, floor_1)
            || !Find(x, y + 1, floor_1) || !Find(x, y - 1, floor_1)
            || !Find(x + 1, y + 1, floor_1) || !Find(x - 1, y - 1, floor_1)
            || !Find(x + 1, y - 1, floor_1) || !Find(x - 1, y + 1, floor_1)
            || board.map[x, y] != null);
            board.map[x, y - 1] = hero;
            target = obj.transform.position = new Vector3(x, y - 0.25F, 0);
        }
        private void SpawnMonster()
        {
            try
            {
                int mobCount = Random.Range(MonsterSpawnCount.m_Min, MonsterSpawnCount.m_Max);
                int uniqueCount = 0;
                for (int i = 0; i < mobCount; i++)
                {
                    int attemps = 0;
                    int x = 0; int y = 0;
                    do
                    {
                        x = Random.Range(RoomInfo[Random.Range(0, rooms)].x, RoomInfo[Random.Range(0, rooms)].x + RoomInfo[Random.Range(0, rooms)].width);
                        y = Random.Range(RoomInfo[Random.Range(0, rooms)].y, RoomInfo[Random.Range(0, rooms)].y + RoomInfo[Random.Range(0, rooms)].height);

                        attemps++;
                        if (attemps > 1000)
                        {
                            LauncError("Insufficient space!", "SpawnMonster");
                            break;
                        }

                    } while (matrix.map[x, y] != floor_1
                    || !Find(x + 1, y, floor_1) || !Find(x - 1, y, floor_1)
                    || !Find(x, y + 1, floor_1) || !Find(x, y - 1, floor_1)
                    || !Find(x + 1, y + 1, floor_1) || !Find(x - 1, y - 1, floor_1)
                    || !Find(x + 1, y - 1, floor_1) || !Find(x - 1, y + 1, floor_1)
                    || !Find(x + 1, y - 2, floor_1) || !Find(x - 1, y - 2, floor_1) || !Find(x, y - 2, floor_1)
                    || board.map[x, y] != null);

                    List<MonstersData> StageMonster = new List<MonstersData>();
                    for (int j = 0; j < SheetManager.Instance.MonsterDB.dataArray.Length; j++)
                    {
                        if (!SheetManager.Instance.MonsterDB.dataArray[j].Type.Equals("normal")) continue;
                        for (int k = 0; k < SheetManager.Instance.MonsterDB.dataArray[j].Targetstage.Length; k++)
                        {
                            if (SheetManager.Instance.MonsterDB.dataArray[j].Targetstage[k].Equals(StageName) &&
                                (StageLevel == 0 ? !SheetManager.Instance.MonsterDB.dataArray[j].ID.Contains("_t") :
                                                    SheetManager.Instance.MonsterDB.dataArray[j].ID.Contains("_t" + (StageLevel + 1))))
                            {
                                StageMonster.Add(SheetManager.Instance.MonsterDB.dataArray[j]);
                                break;
                            }
                        }
                    }
                    MonstersData mobData = StageMonster[Random.Range(0, StageMonster.Count)];
                    StageData sData = System.Array.Find(SheetManager.Instance.StageDB.dataArray, item => item.Type2.Equals("normalstage") && item.Type1.Equals(StageName));
                    GameObject obj = ObjManager.Call().GetObject("Monster");
                    if (obj == null)
                    {
                        LauncError("Insufficient space!", "SpawnMonster");
                        return;
                    }
                    board.map[x, y - 1] = obj;
                    obj.transform.position = new Vector3(x, y, 0);
                    obj.SetActive(true);
                    obj.GetComponent<MonsterArggro>().InitSetting(HeroObj, mobData, Random.Range(10, 15), false, mobData.Animtype, (uniqueCount < 3 ? true : false));
                    uniqueCount++;
                    if (mobData.ID.Contains("gslime"))
                    {
                        for (int k = 0; k < 2; k++)
                        {
                            GameObject _obj = ObjManager.Call().GetObject("Monster");
                            _obj.transform.position = new Vector3(x, y, 0);
                            _obj.SetActive(true);
                            _obj.GetComponent<MonsterArggro>().InitSetting(HeroObj, mobData, Random.Range(10, 15), false, mobData.Animtype);
                        }
                    }
                }

                if (!isGoblinSpawn && GetPercentege(0.2f) != -1 && SteamStatsAndAchievements.instance != null &&
                    (SteamStatsAndAchievements.instance.GettingStat("stat_total_playtime") >= 60 || SteamStatsAndAchievements.instance.GettingStat("stat_hunting_unique_count") >= 1))
                {
                    isGoblinSpawn = true;

                    int attemps = 0;
                    int x = 0; int y = 0;
                    do
                    {
                        x = Random.Range(RoomInfo[Random.Range(0, rooms)].x, RoomInfo[Random.Range(0, rooms)].x + RoomInfo[Random.Range(0, rooms)].width);
                        y = Random.Range(RoomInfo[Random.Range(0, rooms)].y, RoomInfo[Random.Range(0, rooms)].y + RoomInfo[Random.Range(0, rooms)].height);

                        attemps++;
                        if (attemps > 1000)
                        {
                            LauncError("Insufficient space!", "SpawnMonster");
                            break;
                        }

                    } while (matrix.map[x, y] != floor_1
                    || !Find(x + 1, y, floor_1) || !Find(x - 1, y, floor_1)
                    || !Find(x, y + 1, floor_1) || !Find(x, y - 1, floor_1)
                    || !Find(x + 1, y + 1, floor_1) || !Find(x - 1, y - 1, floor_1)
                    || !Find(x + 1, y - 1, floor_1) || !Find(x - 1, y + 1, floor_1)
                    || !Find(x + 1, y - 2, floor_1) || !Find(x - 1, y - 2, floor_1) || !Find(x, y - 2, floor_1)
                    || board.map[x, y] != null);
                    GameObject obj = Instantiate(GolineSpawnObj);
                    board.map[x, y - 1] = obj;
                    obj.transform.position = new Vector3(x, y, 0);
                    obj.SetActive(true);
                }

            }
            catch (System.Exception e)
            {
                LauncError("Insufficient space!", "SpawnMonster");
                return;
            }
        }
        public void SpawnGoblin(Vector3 pos)
        {
            StartCoroutine(SpawnGoblinCoroutine(pos));
        }
        IEnumerator SpawnGoblinCoroutine(Vector3 pos)
        {
            GameObject portalObj = Instantiate(GoblinPortalObj);
            portalObj.transform.localEulerAngles = new Vector3(-40, 180, 180);
            portalObj.transform.position = new Vector3(pos.x, pos.y - 0.5f, 0);
            portalObj.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
            Destroy(portalObj, 3f);
            yield return new WaitForSeconds(2f);
            MonstersData mobData = System.Array.Find(SheetManager.Instance.MonsterDB.dataArray, item => item.ID.Equals("goblin"));
            GameObject obj = ObjManager.Call().GetObject("Monster");
            if (obj != null)
            {
                obj.transform.position = pos;
                obj.GetComponent<MonsterArggro>().InitSetting(HeroObj, mobData, 300, false, mobData.Animtype, false);
                obj.SetActive(true);
            }
        }
        private void ShopSpawn(ref Roominfos info)
        {
            int centerX = (info.x + (info.x + info.width)) / 2;
            int topY = info.y + info.height;

            GameObject obj = Instantiate(shop, new Vector3(centerX, topY - 5, 0), Quaternion.identity, itemParent);
            obj.transform.SetParent(itemParent);
            BuildSpawn.Add(obj);
            info.roomObjs.Add(obj);
        }
        public void MonsterClear()
        {
            for (int i = 0; i < SpawnMonsters.Count; i++)
            {
                if (SpawnMonsters[i].GetComponent<MonsterArggro>())
                {
                    if (SpawnMonsters[i].GetComponent<MonsterArggro>()._monster)
                    {
                        if (SpawnMonsters[i].GetComponent<MonsterArggro>()._monster.hpBar)
                            SpawnMonsters[i].GetComponent<MonsterArggro>()._monster.hpBar.SetActive(false);
                    }
                }
                SpawnMonsters[i].SetActive(false);
            }
            for (int i = 0; i < WarpSpawnMonsters.Count; i++)
            {
                if (WarpSpawnMonsters[i].GetComponent<MonsterArggro>())
                {
                    if (WarpSpawnMonsters[i].GetComponent<MonsterArggro>()._monster)
                    {
                        if (WarpSpawnMonsters[i].GetComponent<MonsterArggro>()._monster.hpBar)
                            WarpSpawnMonsters[i].GetComponent<MonsterArggro>()._monster.hpBar.SetActive(false);
                    }
                }
                WarpSpawnMonsters[i].SetActive(false);
            }
        }
        public IEnumerator MapGuide()
        {
            GameObject.Find("Canvas").transform.Find("MapGuide").gameObject.SetActive(true);
            GameObject.Find("Canvas").transform.Find("MapGuide").GetComponent<Text>().text = (StageLevel + 1) + "-" + (nowStageStep + 1) + LocalizeManager.GetLocalize("stage_title_" + stageRan[StageLevel]);
            float a = 0.0f;
            while (true)
            {
                GameObject.Find("Canvas").transform.Find("MapGuide").GetComponent<Text>().color = new Color(1, 1, 1, a);
                a += Time.deltaTime * 5;
                yield return new WaitForSeconds(Time.deltaTime);
                if (a >= 1.0f) break;
            }
            GameObject.Find("Canvas").transform.Find("MapGuide").GetComponent<Text>().color = new Color(1, 1, 1, 1);
            yield return new WaitForSeconds(3.0f);
            while (true)
            {
                GameObject.Find("Canvas").transform.Find("MapGuide").GetComponent<Text>().color = new Color(1, 1, 1, a);
                a -= Time.deltaTime * 5;
                yield return new WaitForSeconds(Time.deltaTime);
                if (a <= 0.0f) break;
            }
            GameObject.Find("Canvas").transform.Find("MapGuide").GetComponent<Text>().color = new Color(1, 1, 1, 0);
            GameObject.Find("Canvas").transform.Find("MapGuide").gameObject.SetActive(false);
        }
        #endregion Extras
        #region Tools
        public static bool JoyStickKeyInputCheck(KeyCode _joyKey, bool _isDown)
        {
            if (instance.joystricks.Count > 0 && (instance.joystricks[0].Contains("DS4") || instance.joystricks[0].Contains("PC") || instance.joystricks[0].Contains("Wireless")))
            {
                float joyCrossX = Input.GetAxisRaw("PS4_crossX");
                float joyCrossY = Input.GetAxisRaw("PS4_crossY");
                if (MemorialManager.instance != null && MemorialManager.instance.isOpen)
                {
                    joyCrossX = 0;
                    joyCrossY = 0;
                }
                if (_joyKey == KeyCode.Joystick6Button0)
                {
                    if (joyCrossY > 0) return true;
                }
                else if (_joyKey == KeyCode.Joystick6Button1)
                {
                    if (joyCrossY < 0) return true;
                }
                else if (_joyKey == KeyCode.Joystick6Button3)
                {
                    if (joyCrossX > 0) return true;
                }
                else if (_joyKey == KeyCode.Joystick6Button4)
                {
                    if (joyCrossX < 0) return true;
                }
                else if (_joyKey == KeyCode.Joystick7Button0)
                {
                    return _isDown ? Input.GetKeyDown(KeyCode.JoystickButton7) : Input.GetKey(KeyCode.JoystickButton7);
                }
                else if (_joyKey == KeyCode.Joystick7Button1)
                {
                    return _isDown ? Input.GetKeyDown(KeyCode.JoystickButton6) : Input.GetKey(KeyCode.JoystickButton6);
                }
                else if (_joyKey == KeyCode.JoystickButton8)
                {
                    return _isDown ? Input.GetKeyDown(KeyCode.JoystickButton10) : Input.GetKey(KeyCode.JoystickButton10);
                }
                else if (_joyKey == KeyCode.JoystickButton9)
                {
                    return _isDown ? Input.GetKeyDown(KeyCode.JoystickButton11) : Input.GetKey(KeyCode.JoystickButton11);
                }
                else
                {
                    return _isDown ? Input.GetKeyDown(_joyKey) : Input.GetKey(_joyKey);
                }
            }
            else
            {
                float joyCrossX = Input.GetAxisRaw("Horizontal_cross");
                float joyCrossY = Input.GetAxisRaw("Vertical_cross");
                float JoyStrickTrigger = Input.GetAxisRaw("JoyStrickTrigger");
                if (MemorialManager.instance != null && MemorialManager.instance.isOpen)
                {
                    joyCrossX = 0;
                    joyCrossY = 0;
                }
                if (_joyKey == KeyCode.Joystick6Button0)
                {
                    if (joyCrossY > 0) return true;
                }
                else if (_joyKey == KeyCode.Joystick6Button1)
                {
                    if (joyCrossY < 0) return true;
                }
                else if (_joyKey == KeyCode.Joystick6Button3)
                {
                    if (joyCrossX > 0) return true;
                }
                else if (_joyKey == KeyCode.Joystick6Button4)
                {
                    if (joyCrossX < 0) return true;
                }
                else if (_joyKey == KeyCode.Joystick7Button0)
                {
                    if (JoyStrickTrigger > 0) return true;
                }
                else if (_joyKey == KeyCode.Joystick7Button1)
                {
                    if (JoyStrickTrigger < 0) return true;
                }
                else
                {
                    return _isDown ? Input.GetKeyDown(_joyKey) : Input.GetKey(_joyKey);
                }
            }
            return false;
        }
        public static bool KeyboardInputCheck(KeyCode _keyCode, bool _isDown)
        {
            if (_isDown) return Input.GetKeyDown(_keyCode);
            else return Input.GetKey(_keyCode);
        }
        public static Sprite GetJoyStrickKeySprite(KeyCode _joyKey)
        {
            if (_joyKey == KeyCode.None) return Resources.Load<Sprite>("NoneSprite");

            if (!instance.isJoystrick)
            {
                return System.Array.Find(instance.joyKeySprites, item => item.name.Equals("keyboard_" + _joyKey.ToString()));
            }


            Sprite _sprite = null;

            if (instance.joystricks[0].Contains("DS4") || instance.joystricks[0].Contains("PC") || instance.joystricks[0].Contains("Wireless"))
            {
                if (_joyKey == KeyCode.Joystick6Button0)
                {
                    _sprite = System.Array.Find(instance.joyKeySprites, item => item.name.Equals("up"));
                }
                else if (_joyKey == KeyCode.Joystick6Button1)
                {
                    _sprite = System.Array.Find(instance.joyKeySprites, item => item.name.Equals("down"));
                }
                else if (_joyKey == KeyCode.Joystick6Button3)
                {
                    _sprite = System.Array.Find(instance.joyKeySprites, item => item.name.Equals("right"));
                }
                else if (_joyKey == KeyCode.Joystick6Button4)
                {
                    _sprite = System.Array.Find(instance.joyKeySprites, item => item.name.Equals("left"));
                }
                else if (_joyKey == KeyCode.Joystick7Button0)
                {
                    _sprite = System.Array.Find(instance.joyKeySprites, item => item.name.Equals("r2"));
                }
                else if (_joyKey == KeyCode.Joystick7Button1)
                {
                    _sprite = System.Array.Find(instance.joyKeySprites, item => item.name.Equals("L2"));
                }
                else if (_joyKey == KeyCode.Joystick8Button0)
                {
                    _sprite = System.Array.Find(instance.joyKeySprites, item => item.name.Equals("arrow"));
                }
                else
                {
                    if (_joyKey == KeyCode.JoystickButton0) _sprite = System.Array.Find(instance.joyKeySprites, item => item.name.Equals("Square"));
                    else if (_joyKey == KeyCode.JoystickButton1) _sprite = System.Array.Find(instance.joyKeySprites, item => item.name.Equals("x"));
                    else if (_joyKey == KeyCode.JoystickButton2) _sprite = System.Array.Find(instance.joyKeySprites, item => item.name.Equals("O"));
                    else if (_joyKey == KeyCode.JoystickButton3) _sprite = System.Array.Find(instance.joyKeySprites, item => item.name.Equals("Triangle"));
                    else if (_joyKey == KeyCode.JoystickButton4) _sprite = System.Array.Find(instance.joyKeySprites, item => item.name.Equals("L1"));
                    else if (_joyKey == KeyCode.JoystickButton5) _sprite = System.Array.Find(instance.joyKeySprites, item => item.name.Equals("r1"));
                    else if (_joyKey == KeyCode.JoystickButton8) _sprite = System.Array.Find(instance.joyKeySprites, item => item.name.Equals("ls"));
                    else if (_joyKey == KeyCode.JoystickButton9) _sprite = System.Array.Find(instance.joyKeySprites, item => item.name.Equals("rs"));
                }
            }
            else
            {
                if (_joyKey == KeyCode.Joystick6Button0)
                {
                    _sprite = System.Array.Find(instance.joyKeySprites, item => item.name.Equals("up"));
                }
                else if (_joyKey == KeyCode.Joystick6Button1)
                {
                    _sprite = System.Array.Find(instance.joyKeySprites, item => item.name.Equals("down"));
                }
                else if (_joyKey == KeyCode.Joystick6Button3)
                {
                    _sprite = System.Array.Find(instance.joyKeySprites, item => item.name.Equals("right"));
                }
                else if (_joyKey == KeyCode.Joystick6Button4)
                {
                    _sprite = System.Array.Find(instance.joyKeySprites, item => item.name.Equals("left"));
                }
                else if (_joyKey == KeyCode.Joystick7Button0)
                {
                    _sprite = System.Array.Find(instance.joyKeySprites, item => item.name.Equals("rt"));
                }
                else if (_joyKey == KeyCode.Joystick7Button1)
                {
                    _sprite = System.Array.Find(instance.joyKeySprites, item => item.name.Equals("lt"));
                }
                else if (_joyKey == KeyCode.Joystick8Button0)
                {
                    _sprite = System.Array.Find(instance.joyKeySprites, item => item.name.Equals("arrow"));
                }
                else
                {
                    if (_joyKey == KeyCode.JoystickButton0) _sprite = System.Array.Find(instance.joyKeySprites, item => item.name.Equals("a"));
                    else if (_joyKey == KeyCode.JoystickButton1) _sprite = System.Array.Find(instance.joyKeySprites, item => item.name.Equals("b"));
                    else if (_joyKey == KeyCode.JoystickButton2) _sprite = System.Array.Find(instance.joyKeySprites, item => item.name.Equals("x"));
                    else if (_joyKey == KeyCode.JoystickButton3) _sprite = System.Array.Find(instance.joyKeySprites, item => item.name.Equals("y"));
                    else if (_joyKey == KeyCode.JoystickButton4) _sprite = System.Array.Find(instance.joyKeySprites, item => item.name.Equals("lb"));
                    else if (_joyKey == KeyCode.JoystickButton5) _sprite = System.Array.Find(instance.joyKeySprites, item => item.name.Equals("rb"));
                    else if (_joyKey == KeyCode.JoystickButton8) _sprite = System.Array.Find(instance.joyKeySprites, item => item.name.Equals("ls"));
                    else if (_joyKey == KeyCode.JoystickButton9) _sprite = System.Array.Find(instance.joyKeySprites, item => item.name.Equals("rs"));
                }
            }
            return _sprite;
        }
        public void RoomInsert(int roomIndex, bool wait = false, bool wait2 = false)
        {
            if (wait)
            {
                if (isStageBoss) roomIndex = RoomInfo.FindIndex(item => item.roomID.Equals("boss_waiting"));
                else roomIndex = RoomInfo.FindIndex(item => item.roomID.Equals("d01_normalend_0" + (stageRan[StageLevel] + 1)));
            }
            if (wait2)
            {
                roomIndex = RoomInfo.FindIndex(item => item.rooMType.Equals("bossstage"));
            }
            int x = RoomInfo[roomIndex].x;
            int y = RoomInfo[roomIndex].y;
            int w = RoomInfo[roomIndex].width;
            int h = RoomInfo[roomIndex].height;
            RoomOpen(roomIndex);
        }
        public void RoomExit()
        {
            for (int i = rooms; i < RoomInfo.Count; i++)
            {
                RoomClose(RoomInfo[i].roomNum);
            }
        }
        private bool InBounds(int x, int y)
        {
            return !(x > matrix.rows - 1 || x < 0 || y > matrix.columns - 1 || y < 0);
        }
        public bool Find(int x, int y, GameObject tile)
        {
            return InBounds(x, y) && matrix.map[x, y] == tile;
        }
        private bool Avaiable(int X, int Y, int width, int height)
        {
            for (int x = X; x < X + width; x++)
            {
                for (int y = Y; y < Y + height; y++)
                {
                    if (!Find(x, y, null)) return false;

                    #region Around
                    if (!Find(X - 1, y, null)) return false;
                    if (!Find(x, Y - 1, null)) return false;
                    if (!Find(X + width, y, null)) return false;
                    if (!Find(x, Y + height, null)) return false;
                    if (!Find(X + width, Y + height, null)) return false;
                    if (!Find(X - 1, Y - 1, null)) return false;
                    if (!Find(X + width, Y - 1, null)) return false;
                    if (!Find(X - 1, Y + height, null)) return false;

                    if (!Find(X - 2, y, null)) return false;
                    if (!Find(x, Y - 2, null)) return false;
                    if (!Find(X + width + 1, y, null)) return false;
                    if (!Find(x, Y + height + 1, null)) return false;
                    if (!Find(X + width + 1, Y + height + 1, null)) return false;
                    if (!Find(X - 2, Y - 2, null)) return false;
                    if (!Find(X + width + 1, Y - 2, null)) return false;
                    if (!Find(X - 2, Y + height + 1, null)) return false;

                    if (!Find(X - 3, y, null)) return false;
                    if (!Find(x, Y - 3, null)) return false;
                    if (!Find(X + width + 2, y, null)) return false;
                    if (!Find(x, Y + height + 2, null)) return false;
                    if (!Find(X + width + 2, Y + height + 2, null)) return false;
                    if (!Find(X - 3, Y - 3, null)) return false;
                    if (!Find(X + width + 2, Y - 3, null)) return false;
                    if (!Find(X - 3, Y + height + 2, null)) return false;
                    #endregion
                }
            }
            return true;
        }
        private void SavePoints()
        {
            pointsList = new Vector2[rooms/* + 2*/];
            RemovePoints();

            switch (mode)
            {
                case GenerationMode.Horizontal:
                    for (int y = 0; y < matrix.columns; y++)
                    {
                        for (int x = 0; x < matrix.rows; x++)
                        {
                            SetPoints(x, y);
                        }
                    }
                    break;
                case GenerationMode.Vertical:
                    for (int x = 0; x < matrix.rows; x++)
                    {
                        for (int y = 0; y < matrix.columns; y++)
                        {
                            SetPoints(x, y);
                        }
                    }
                    break;
            }
        }
        private void SetPoints(int x, int y)
        {
            if (matrix.map[x, y] == exit)
            {
                for (int i = 0; i < pointsList.Length; i++)
                {
                    if (pointsList[i] == Vector2.zero)
                    {
                        pointsList[i] = new Vector2(x, y);
                        break;
                    }
                }
                matrix.map[x, y] = floor_1;
            }
        }
        private void RemovePoints()
        {
            for (int i = 0; i < pointsList.Length; i++)
            {
                pointsList[i] = Vector2.zero;
            }
        }
        private void LauncError(string message, string _functionName)
        {
            Create();
        }
        private void RoomObjInsert(int x, int y, GameObject obj)
        {
            int roomIndex = RoomInfo.FindIndex(item => item.x <= x + 1 && (item.x + item.width) >= x - 1 && item.y <= y + 1 && (item.y + item.height) >= y - 1);
            if (roomIndex != -1)
            {
                RoomInfo[roomIndex].roomObjs.Add(obj);
            }
        }
        public void RoomOpen(int roomNum)
        {
            for (int i = 0; i < RoomInfo[roomNum].roomObjs.Count; i++)
            {
                if (RoomInfo[roomNum].roomObjs[i] != null)
                    RoomInfo[roomNum].roomObjs[i].SetActive(true);

                GameObject f0 = RoomInfo[roomNum].roomObjs[i].transform.Find("FogDark")?.gameObject;
                GameObject f1 = RoomInfo[roomNum].roomObjs[i].transform.Find("FogDark2")?.gameObject;
                GameObject f2 = RoomInfo[roomNum].roomObjs[i].transform.Find("FogDark3")?.gameObject;
                GameObject f3 = RoomInfo[roomNum].roomObjs[i].transform.Find("FogDark4")?.gameObject;
                if (f0) f0.SetActive(true);
                if (f1) f1.SetActive(true);
                if (f2) f2.SetActive(true);
                if (f3) f3.SetActive(true);

                GameObject minimapObj = FixedRoomMiniMap.Find(item => item.name.Equals(RoomInfo[roomNum].roomID));
                if (minimapObj) minimapObj.SetActive(true);
                GameObject portalObj = TeleportList.Find(item => item.name.Equals(RoomInfo[roomNum].roomID));
                if (portalObj) portalObj.GetComponent<PortalControl>().isOpen = true;
            }

            if (RoomInfo[roomNum].roomID.Equals("d01_reinforce_01"))
            {
                Dialogue.Instance.CommnetSetting("reinforce_npc_incarceration_Entrance", 0, null);
            }
        }
        public void FogOff(int roomNum)
        {
            for (int i = 0; i < RoomInfo[roomNum].roomObjs.Count; i++)
            {
                GameObject f0 = RoomInfo[roomNum].roomObjs[i].transform.Find("FogDark")?.gameObject;
                GameObject f1 = RoomInfo[roomNum].roomObjs[i].transform.Find("FogDark2")?.gameObject;
                GameObject f2 = RoomInfo[roomNum].roomObjs[i].transform.Find("FogDark3")?.gameObject;
                GameObject f3 = RoomInfo[roomNum].roomObjs[i].transform.Find("FogDark4")?.gameObject;
                if (f0) f0.SetActive(false);
                if (f1) f1.SetActive(false);
                if (f2) f2.SetActive(false);
                if (f3) f3.SetActive(false);
            }
        }
        private void RoomClose(int roomNum)
        {
            int roomIndex = RoomInfo.FindIndex(item => item.roomNum == roomNum);
            if (roomIndex != -1)
            {
                for (int i = 0; i < RoomInfo[roomIndex].roomObjs.Count; i++)
                {
                    if (RoomInfo[roomIndex].roomObjs[i] != null)
                        RoomInfo[roomIndex].roomObjs[i].SetActive(false);
                }
            }
        }
        public bool RoomCheck(int x, int y)
        {
            return RoomInfo.FindIndex(item => item.x <= x && (item.x + item.width) >= x && item.y <= y && (item.y + item.height) >= y) != -1;
        }
        public bool RoomCheck(int roomNum, bool _isActive)
        {
            int roomIndex = RoomInfo.FindIndex(item => item.roomNum == roomNum);
            if (roomIndex != -1)
            {
                if (RoomInfo[roomIndex].roomObjs.Count > 0 && RoomInfo[roomIndex].roomObjs[0].activeSelf == _isActive) return true;

                for (int i = 0; i < RoomInfo[roomIndex].roomObjs.Count; i++)
                {
                    if (RoomInfo[roomIndex].roomObjs[i] != null)
                        RoomInfo[roomIndex].roomObjs[i].SetActive(_isActive);
                }

                return true;
            }

            return false;
        }
        public void WorldMapOnOff(bool isPortal = false)
        {
            if (isPortal && isWorldMap) return;
            isWorldMap = !isWorldMap;
            worldMap.SetActive(isWorldMap);
            miniMap.SetActive(!isWorldMap);
            Time.timeScale = (isWorldMap ? 0 : 1);
            if (isWorldMap)
            {
                if (StageName.Equals("Tutorial") || StageName.Equals("ajit_mapTitle"))
                    mapName.text = LocalizeManager.GetLocalize("stage_" + StageName);
                else
                    mapName.text = LocalizeManager.GetLocalize("stage_title_" + stageRan[StageLevel]);
                StartCoroutine(UiOff());
            }
            else
            {
                StartCoroutine(UiOn());
            }
            if (!isPortal)
            {
                for (int i = 0; i < LineObjs.Count; i++)
                {
                    LineObjs[i].SetActive(isWorldMap);
                }
            }
            isOpenPortal = isPortal;
        }
        public IEnumerator UiOff()
        {
            Image[] AllImages = GameObject.Find("Canvas").transform.Find("UI").GetComponentsInChildren<Image>(true);
            Text[] AllTexts = GameObject.Find("Canvas").transform.Find("UI").GetComponentsInChildren<Text>(true);
            float a = 1.0f;
            while (a >= 0.0f)
            {
                for (int i = 0; i < AllImages.Length; i++)
                {
                    AllImages[i].color = new Color(AllImages[i].color.r, AllImages[i].color.g, AllImages[i].color.b, a);
                }
                for (int i = 0; i < AllTexts.Length; i++)
                {
                    AllTexts[i].color = new Color(AllTexts[i].color.r, AllTexts[i].color.g, AllTexts[i].color.b, a);
                }
                a -= Time.unscaledDeltaTime / UIfadeTime;
                yield return new WaitForSecondsRealtime(Time.unscaledDeltaTime);
            }
            for (int i = 0; i < AllImages.Length; i++)
            {
                AllImages[i].color = new Color(AllImages[i].color.r, AllImages[i].color.g, AllImages[i].color.b, 0);
            }
            for (int i = 0; i < AllTexts.Length; i++)
            {
                AllTexts[i].color = new Color(AllTexts[i].color.r, AllTexts[i].color.g, AllTexts[i].color.b, 0);
            }
        }
        public IEnumerator UiOn()
        {
            Image[] AllImages = GameObject.Find("Canvas").transform.Find("UI").GetComponentsInChildren<Image>();
            Text[] AllTexts = GameObject.Find("Canvas").transform.Find("UI").GetComponentsInChildren<Text>(true);
            float a = 0.0f;
            while (a <= 1.0f)
            {
                for (int i = 0; i < AllImages.Length; i++)
                {
                    AllImages[i].color = new Color(AllImages[i].color.r, AllImages[i].color.g, AllImages[i].color.b, a);
                }
                for (int i = 0; i < AllTexts.Length; i++)
                {
                    AllTexts[i].color = new Color(AllTexts[i].color.r, AllTexts[i].color.g, AllTexts[i].color.b, a);
                }
                a += Time.unscaledDeltaTime / UIfadeTime;
                yield return new WaitForSecondsRealtime(Time.unscaledDeltaTime);
            }
            for (int i = 0; i < AllImages.Length; i++)
            {
                AllImages[i].color = new Color(AllImages[i].color.r, AllImages[i].color.g, AllImages[i].color.b, 1);
            }
            for (int i = 0; i < AllTexts.Length; i++)
            {
                AllTexts[i].color = new Color(AllTexts[i].color.r, AllTexts[i].color.g, AllTexts[i].color.b, 1);
            }
        }
        public int GetRoomIndex(int x, int y)
        {
            return RoomInfo.FindIndex(item => item.x <= x && (item.x + item.width) >= x && item.y <= y && (item.y + item.height) >= y);
        }
        private int GetPercentege(float _per)
        {
            int[] per = new int[100];
            for (int i = 0; i < per.Length; i++) per[i] = -1;

            int max = Mathf.FloorToInt(_per * 100);
            int cur = 0;
            while (true)
            {
                if (cur >= max) break;
                int t = Random.Range(0, 100);
                if (per[t] == -1)
                {
                    per[t] = 1;
                    cur++;
                }
            }
            return per[Random.Range(0, 100)];
        }
        public IEnumerator FadeInOut()
        {
            RectTransform rt = titleSloganObj.GetComponent<RectTransform>();
            rt.anchoredPosition3D = new Vector3(0, 350.0f, 0);
            titleSloganObj.SetActive(true);
            float y = 350.0f;
            while (true)
            {
                y -= Time.deltaTime * 750.0f;
                rt.anchoredPosition3D = new Vector3(0, y, 0);
                if (y < -100.0f) break;
                yield return new WaitForSeconds(Time.deltaTime);
            }
            yield return new WaitForSeconds(0.8f);
            y = -100.0f;
            while (true)
            {
                y += Time.deltaTime * 750.0f;
                rt.anchoredPosition3D = new Vector3(0, y, 0);
                if (y > 330) break;
                yield return new WaitForSeconds(Time.deltaTime);
            }
            titleSloganObj.SetActive(false);
        }
        public void OpenQuestion_Restart()
        {
            isQuit = false;
            QuestionText.text = LocalizeManager.GetLocalize("question_restart");
            QuestionPanels.SetActive(true);
            QuestionYes.onClick.RemoveAllListeners();
            QuestionYes.onClick.AddListener(() =>
            {
                SoundManager.instance.StartAudio(new string[1] { "Click_0" }, VOLUME_TYPE.EFFECT);
                isPause = !isPause;
                GameObject.Find("Canvas").transform.Find("UI").gameObject.SetActive(!isPause);
                SettingCanvas.SetActive(isPause);
                Time.timeScale = (isPause ? 0 : 1);
                settingTrigger = (isPause ? 1 : 0);
                QuestionPanels.SetActive(false);
                PDG.Player.instance.isCameraOff = false;
                GoAjit();
            });
            StageLevel = 0;
            isRebuildDevice = false;
            QuestionNo.onClick.RemoveAllListeners();
            QuestionNo.onClick.AddListener(() =>
            {
                PDG.Player.instance.isCameraOff = false;
                QuestionPanels.SetActive(false);
            });
        }
        public void OpenQuestion_Quit()
        {
            isQuit = true;
            QuestionText.text = LocalizeManager.GetLocalize("question_quit");
            QuestionPanels.SetActive(true);
            QuestionYes.onClick.RemoveAllListeners();
            QuestionYes.onClick.AddListener(() =>
            {
                SoundManager.instance.StartAudio(new string[1] { "Click_0" }, VOLUME_TYPE.EFFECT);
                PDG.Player.instance.isCameraOff = false;
                Application.Quit();
            });
            QuestionNo.onClick.RemoveAllListeners();
            QuestionNo.onClick.AddListener(() =>
            {
                QuestionPanels.SetActive(false);
                PDG.Player.instance.isCameraOff = false;
            });
        }
        public static void DropMaterial(string type, string id, Vector3 dropPos)
        {
            int r = Random.Range(0, 200);
            if (!type.Equals("unique") && !type.Equals("subboss") && r < (50 - instance.triggerKeyDropAmount)) return;
            Sprite[] s = Resources.LoadAll<Sprite>("Item/Material/" + type);
            s = System.Array.FindAll(s, item => item.name.Contains(id));
            if (s.Length > 1) id = s[Random.Range(0, s.Length)].name;
            GameObject obj = ObjManager.Call().GetObject("Item");
            if (obj != null)
            {
                Item _item = obj.GetComponent<Item>();
                _item._Item = new ItemInfo
                {
                    Data = new ItemsData
                    {
                        Type1 = "Material",
                        Type2 = type,
                        ID = id
                    }
                };
                _item.dir = Vector3.zero;
                _item.rPow = 0.0f;
                _item.itemCount = 1;
                obj.transform.position = dropPos;
                obj.SetActive(true);
            }
        }
        public Vector3 GetFloorPosition()
        {
            int attemps = 0;
            int x = 0; int y = 0;
            int x2 = Mathf.FloorToInt(Player.instance.transform.position.x);
            int y2 = Mathf.FloorToInt(Player.instance.transform.position.y);

            do
            {
                while (true)
                {
                    x = Random.Range(40, matrix.rows - 40);
                    y = Random.Range(40, matrix.columns - 40);
                    if (((x < (x2 - 20)) || (x > (x2 + 20))) && ((y < (y2 - 20)) || (y > (y2 + 20))))
                    {
                        break;
                    }
                }

            } while (matrix.map[x, y] != floor_1
            || !Find(x + 1, y, floor_1) || !Find(x - 1, y, floor_1)
            || !Find(x, y + 1, floor_1) || !Find(x, y - 1, floor_1)
            || !Find(x + 1, y + 1, floor_1) || !Find(x - 1, y - 1, floor_1)
            || !Find(x + 1, y - 1, floor_1) || !Find(x - 1, y + 1, floor_1)
            || !Find(x + 1, y - 2, floor_1) || !Find(x - 1, y - 2, floor_1) || !Find(x, y - 2, floor_1)
            || board.map[x, y] != null);

            return new Vector3(x, y, 0);
        }
        public IEnumerator BossTextClose()
        {
            GameObject.Find("Canvas").transform.Find("TutorialText2").GetComponent<Text>().text = LocalizeManager.GetLocalize("elite_timeover");
            yield return new WaitForSeconds(10.0f);
            GameObject.Find("Canvas").transform.Find("TutorialText2").GetComponent<Text>().text = string.Empty;
        }
        #endregion Tools
        #region -- Option Setting Method --
        public void SelectBarChange(Image _image)
        {
            for (int i = 0; i < SettingSelectImages.Length; i++)
            {
                if (SettingSelectImages[i] == _image)
                {
                    SettingSelectImages[i].enabled = true;
                }
                else
                {
                    SettingSelectImages[i].enabled = false;
                }
            }
        }
        public void OptionMenuClick(int _type)
        {
            SoundManager.instance.StartAudio(new string[1] { "menu_click" }, VOLUME_TYPE.EFFECT);
            settingTrigger = _type;
            if (_type == 3)
            {
                nowSettingSelectImageNum = optionOcount;
                SelectBarChange(SettingSelectImages[nowSettingSelectImageNum]);
                GamePlayCanvas.SetActive(true);
            }
            else if (_type == 4)
            {
                nowSettingSelectImageNum = optionOcount + optionGcount;
                SelectBarChange(SettingSelectImages[nowSettingSelectImageNum]);
                VideoCanvas.SetActive(true);
            }
            else if (_type == 5)
            {
                nowSettingSelectImageNum = optionOcount + optionGcount + optionVcount;
                SelectBarChange(SettingSelectImages[nowSettingSelectImageNum]);
                AudioCanvas.SetActive(true);
            }
            else if (_type == 6)
            {
                nowSettingSelectImageNum = optionOcount + optionGcount + optionVcount + optionAcount;
                SelectBarChange(SettingSelectImages[nowSettingSelectImageNum]);
                LanguageCanvas.SetActive(true);
            }
            else if (_type == 7)
            {
                nowSettingSelectImageNum = optionOcount + optionGcount + optionVcount + optionAcount + optionLcount;
                SelectBarChange(SettingSelectImages[nowSettingSelectImageNum]);
                KeyChangeGuideText.text = "";
                KeySettingCanvas.SetActive(true);
            }
        }
        public void ScreenModeChange(bool _fullScreen)
        {
            SoundManager.instance.StartAudio(new string[1] { "menu_click" }, VOLUME_TYPE.EFFECT);
            isFullScreenMode = _fullScreen;
            if (_fullScreen)
            {
                full_Option.sprite = _checkSprite;
                window_Option.sprite = _unCheckSprite;
            }
            else
            {
                full_Option.sprite = _unCheckSprite;
                window_Option.sprite = _checkSprite;
            }
            Screen.SetResolution(1600, 900, isFullScreenMode, 60);
            SaveAndLoadManager.instance.SaveVolume(new VolumeSave { _type = 5, _value = (isFullScreenMode ? 1 : 0) });
        }
        public void VsyncModeChange(bool _isVsyncMode)
        {
            SoundManager.instance.StartAudio(new string[1] { "menu_click" }, VOLUME_TYPE.EFFECT);
            isVsyncMode = _isVsyncMode;
            vSync_Option.sprite = (isVsyncMode ? _checkSprite : _unCheckSprite);
            if (isVsyncMode)
            {
                QualitySettings.vSyncCount = 1;
                ui_FreamText.text = LocalizeManager.GetLocalize("ui_fream") + "\n" + LocalizeManager.GetLocalize("ui_vsync_use");
            }
            else
            {
                QualitySettings.vSyncCount = 0;
                ui_FreamText.text = LocalizeManager.GetLocalize("ui_fream") + "\n(" + Application.targetFrameRate + "FPS)";
            }
            SaveAndLoadManager.instance.SaveVolume(new VolumeSave { _type = 7, _value = (isVsyncMode ? 1 : 0) });
        }
        public void VsyncModeChange()
        {
            SoundManager.instance.StartAudio(new string[1] { "menu_click" }, VOLUME_TYPE.EFFECT);
            isVsyncMode = !isVsyncMode;
            vSync_Option.sprite = (isVsyncMode ? _checkSprite : _unCheckSprite);
            if (isVsyncMode)
            {
                QualitySettings.vSyncCount = 1;
                ui_FreamText.text = LocalizeManager.GetLocalize("ui_fream") + "\n" + LocalizeManager.GetLocalize("ui_vsync_use");
            }
            else
            {
                QualitySettings.vSyncCount = 0;
                ui_FreamText.text = LocalizeManager.GetLocalize("ui_fream") + "\n(" + Application.targetFrameRate + "FPS)";
            }
            SaveAndLoadManager.instance.SaveVolume(new VolumeSave { _type = 7, _value = (isVsyncMode ? 1 : 0) });
        }
        public void CrossHairChange(bool prev)
        {
            if (prev)
            {
                nowCrossHairNum--;
                if (nowCrossHairNum < 0) nowCrossHairNum = crossHairSprites.Length - 1;
            }
            else
            {
                nowCrossHairNum++;
                if (nowCrossHairNum >= crossHairSprites.Length) nowCrossHairNum = 0;
            }
            crossHairImage.sprite = crossHairSprites[nowCrossHairNum];
            crossHairImage_UI.sprite = crossHairSprites[nowCrossHairNum];
            SaveAndLoadManager.instance.SaveItem(new ItemSave { _id = "UI_CrossHair", _nCount = nowCrossHairNum }, true);
        }
        public void OnOffPause()
        {
            isEscPause = !isEscPause;
            SaveAndLoadManager.instance.SaveItem(new ItemSave { _id = "ESC_PAUSE", _nCount = System.Convert.ToInt32(isEscPause) }, true);
            if (isEscPause) UI_EscPause.sprite = _checkSprite;
            else UI_EscPause.sprite = _unCheckSprite;
        }
        private void OnApplicationFocus(bool focus)
        {
            if (isEscPause)
            {
                if (!focus && (Player.instance != null && !Player.instance.isDie) && !Inventorys.Instance.isGuid && (Dialogue.Instance == null ? true : !Dialogue.Instance.isCommnet) &&
                    (UnLock.Instance == null ? true : !UnLock.Instance.isOpen) &&
                    (BodyRemodelingManager.instance == null ? true : !BodyRemodelingManager.instance.isOpen) && !QuestionPanel.activeSelf && !QuestionPanels.activeSelf)
                {
                    SoundManager.instance.StartAudio(new string[1] { "Pause" }, VOLUME_TYPE.EFFECT);
                    isPause = true;
                    GameObject.Find("Canvas").transform.Find("UI").gameObject.SetActive(!isPause);
                    SettingCanvas.SetActive(isPause);
                    Time.timeScale = (isPause ? 0 : 1);
                    if (settingTrigger == 0) settingTrigger = (isPause ? 1 : 0);
                    SoundManager.instance.AllMute(true);
                    QuestionPanel.SetActive(false);
                    if (StateUpgradeController.instance.isOn) StateUpgradeController.instance.CloseUpgrade();
                }
                else
                {
                    SoundManager.instance.AllMute(false);
                }
            }
        }
        public bool isTestScreenShot = false;
        private void FixedUpdate()
        {
            if (isBossRushStart) fBossRushTime += Time.fixedDeltaTime;
            if (isTestScreenShot)
            {
                isTestScreenShot = false;
                ScreenShot();
            }
        }
        public void ScreenShot()
        {
            string path = Application.persistentDataPath;
            path += "/pic1.png";
            ScreenCapture.CaptureScreenshot(path);
        }
        public void TumblbugButton(int _type)
        {
            if (_type == 0) Application.OpenURL("https://store.steampowered.com/app/1231670/Chunker/");   // 스팀
            else if (_type == 1) Application.OpenURL("https://discord.gg/VQcZ8D3");                       // 디스코드
        }
        public void CameraShakerLevelChange(int _level)
        {
            if (_level < 0) _level = 4;
            if (_level > 4) _level = 0;
            nCameraShakerLevel = _level;
            for (int i = 0; i < CameraShakerLevelImage.Length; i++)
            {
                if (i == nCameraShakerLevel) CameraShakerLevelImage[i].sprite = _checkSprite;
                else CameraShakerLevelImage[i].sprite = _unCheckSprite;
            }

            SaveAndLoadManager.instance.SaveItem(new ItemSave { _id = "OPTION_CAMERA_SHAKER", _nCount = nCameraShakerLevel }, true);
        }
        public void BrightnessChange(Scrollbar _slider)
        {
            float v = _slider.value;
            v = (v - 0.5f) * 2 * 100;
            pGrading.brightness.value = v;
            int vv = Mathf.FloorToInt(v);
            SaveAndLoadManager.instance.SaveItem(new ItemSave { _id = "UI_BRIGHT", _nCount = vv }, true);
        }
        public void BrightnessChange(Scrollbar _slider, float _value)
        {
            if (_value < 0.0f) _value = 0;
            if (_value > 1.0f) _value = 1;
            float v = _slider.value = _value;
            v = (v - 0.5f) * 2 * 100;
            pGrading.brightness.value = v;
            int vv = Mathf.FloorToInt(v);
            SaveAndLoadManager.instance.SaveItem(new ItemSave { _id = "UI_BRIGHT", _nCount = vv }, true);
        }
        public void FreamChange(Scrollbar _slider)
        {
            float v = _slider.value;
            v = (90 * v) + 30;
            int vv = Mathf.FloorToInt(v);
            Application.targetFrameRate = vv;
            if (isVsyncMode)
            {
                ui_FreamText.text = LocalizeManager.GetLocalize("ui_fream") + "\n" + LocalizeManager.GetLocalize("ui_vsync_use");
            }
            else
            {
                ui_FreamText.text = LocalizeManager.GetLocalize("ui_fream") + "\n(" + vv + "FPS)";
            }
            SaveAndLoadManager.instance.SaveItem(new ItemSave { _id = "FREAM_RATE", _nCount = vv }, true);
        }
        public void FreamChange(Scrollbar _slider, float _value)
        {
            if (_value < 0.0f) _value = 0;
            if (_value > 1.0f) _value = 1;
            float v = _slider.value = _value;
            v = (90 * v) + 30;
            int vv = Mathf.FloorToInt(v);
            Application.targetFrameRate = vv;
            if (isVsyncMode)
            {
                ui_FreamText.text = LocalizeManager.GetLocalize("ui_fream") + "\n" + LocalizeManager.GetLocalize("ui_vsync_use");
            }
            else
            {
                ui_FreamText.text = LocalizeManager.GetLocalize("ui_fream") + "\n(" + vv + "FPS)";
            }
            SaveAndLoadManager.instance.SaveItem(new ItemSave { _id = "FREAM_RATE", _nCount = vv }, true);
        }
        public void ContrastChange(Scrollbar _slider)
        {
            float v = _slider.value;
            v = (v - 0.5f) * 2 * 100;
            pGrading.contrast.value = v;
            int vv = Mathf.FloorToInt(v);
            SaveAndLoadManager.instance.SaveItem(new ItemSave { _id = "UI_CONTRAST", _nCount = vv }, true);
        }
        public void ContrastChange(Scrollbar _slider, float _value)
        {
            if (_value < 0.0f) _value = 0;
            if (_value > 1.0f) _value = 1;
            float v = _slider.value = _value;
            v = (v - 0.5f) * 2 * 100;
            pGrading.contrast.value = v;
            int vv = Mathf.FloorToInt(v);
            SaveAndLoadManager.instance.SaveItem(new ItemSave { _id = "UI_CONTRAST", _nCount = vv }, true);
        }
        [Header("ValueChange")]
        [SerializeField] private Text[] vChangeTexts;
        [SerializeField] private Image[] vChangeImages;
        [SerializeField] private RawImage[] vChangeImages2;
        [SerializeField] private Text[] buffText;
        public void ValueChangePointDown()
        {
            for (int i = 0; i < vChangeTexts.Length; i++) vChangeTexts[i].color = new Color(0, 0, 0, 0);
            for (int i = 0; i < vChangeImages.Length; i++)
            {
                if (vChangeImages[i].name.Contains("ItemSlot_Pause"))
                {
                    vChangeImages[i].gameObject.SetActive(false);
                }
                else
                {
                    vChangeImages[i].color = new Color(0, 0, 0, 0);
                }
            }
            for (int i = 0; i < vChangeImages2.Length; i++)
            {
                vChangeImages2[i].color = new Color(0, 0, 0, 0);
            }
        }
        public void ValueChangePointUp()
        {
            for (int i = 0; i < vChangeTexts.Length; i++)
            {
                vChangeTexts[i].color = new Color(1, 1, 1, 1);
                if (vChangeTexts[i].name.Equals("Version") || vChangeTexts[i].name.Equals("tumblbug"))
                {
                    vChangeTexts[i].color = new Color32(236, 216, 135, 255);
                }
            }
            for (int i = 0; i < vChangeImages.Length; i++)
            {

                if (vChangeImages[i].name.Equals("Up") || vChangeImages[i].name.Equals("Down"))
                {
                    vChangeImages[i].color = new Color32(0, 0, 0, 255);
                }
                else if (vChangeImages[i].name.Contains("ItemSlot_Pause"))
                {
                    vChangeImages[i].gameObject.SetActive(true);
                }
                else
                {
                    vChangeImages[i].color = new Color(1, 1, 1, 1);
                }
            }
            for (int i = 0; i < vChangeImages2.Length; i++)
            {
                vChangeImages2[i].color = new Color(1, 1, 1, 1);
            }
        }
        public void SettingSafe()
        {
            try
            {
                GameObject obja = SettingCanvas.transform.Find("Default").Find("MaterialSlot").gameObject;
                if (obja)
                {
                    int cc = 0;
                    Image[] images = obja.transform.GetComponentsInChildren<Image>(true);

                    Sprite coinSprite = Resources.Load<Sprite>("Item/Coin/Skull/glowing_skull");
                    images[cc].color = new Color32(255, 255, 255, 255);
                    images[cc].sprite = coinSprite;
                    images[cc].transform.Find("Text").GetComponent<Text>().text = Inventorys.Instance.glowindskull.ToString();
                    images[cc].transform.localScale = Vector3.one;
                    images[cc].GetComponent<Button>().onClick.RemoveAllListeners();
                    images[cc].GetComponent<Button>().onClick.AddListener(() => { Inventorys.Instance.SelectedItem("glowing_skull", coinSprite); });
                    cc++;
                    for (int i = 0; i < materialTypes.Length; i++)
                    {
                        Sprite[] materialSprites = Resources.LoadAll<Sprite>("Item/Material/" + materialTypes[i]);
                        for (int j = 0; j < materialSprites.Length; j++)
                        {
                            bool tt = MaterialList.ContainsKey(materialTypes[i] + "_" + materialSprites[j].name);
                            if (tt)
                            {
                                Sprite s = Resources.Load<Sprite>("Item/Material/" + materialTypes[i] + "/" + materialSprites[j].name);
                                images[cc].color = new Color32(255, 255, 255, 255);
                                images[cc].sprite = s;
                                images[cc].transform.Find("Text").GetComponent<Text>().text = MaterialList[materialTypes[i] + "_" + materialSprites[j].name].ToString();
                                images[cc].transform.localScale = Vector3.one;
                                images[cc].GetComponent<Button>().onClick.RemoveAllListeners();
                                string n = materialTypes[i] + "_" + materialSprites[j].name;
                                images[cc].GetComponent<Button>().onClick.AddListener(() => { Inventorys.Instance.SelectedItem(n, s); });
                                cc++;
                            }
                            if (cc >= images.Length) break;
                        }
                    }

                    if (isGetWarpPart)
                    {
                        if (cc >= images.Length) return;
                        Sprite s = Resources.Load<Sprite>("Item/Material/warp/CaseParts_" + warpType);
                        images[cc].color = new Color32(255, 255, 255, 255);
                        images[cc].sprite = s;
                        images[cc].transform.Find("Text").GetComponent<Text>().text = "1";
                        images[cc].transform.localScale = Vector3.one;
                        images[cc].GetComponent<Button>().onClick.RemoveAllListeners();
                        images[cc].GetComponent<Button>().onClick.AddListener(() => { Inventorys.Instance.SelectedItem("warp_CaseParts_" + warpType, s); });
                        cc++;
                    }

                    for (int i = cc; i < images.Length; i++)
                    {
                        images[i].color = new Color32(255, 255, 255, 0);
                        images[i].transform.Find("Text").GetComponent<Text>().text = string.Empty;
                        images[i].GetComponent<Button>().onClick.RemoveAllListeners();
                    }
                }
                GameObject obj22 = SettingCanvas.transform.Find("Default").Find("BuffContent").gameObject;
                Text objText = obj22.GetComponent<Text>();
                string strBuff = string.Empty;
                if (Player.instance.buffTowerBuffTime_0 > 0)
                {
                    strBuff += LocalizeManager.GetLocalize("bufftower_buff_0");
                }
                if (Player.instance.buffTowerBuffTime_1 > 0)
                {
                    if (!strBuff.Equals(string.Empty)) strBuff += "\n";
                    strBuff += LocalizeManager.GetLocalize("bufftower_buff_1");
                }
                if (Player.instance.buffTowerBuffTime_2 > 0)
                {
                    if (!strBuff.Equals(string.Empty)) strBuff += "\n";
                    strBuff += LocalizeManager.GetLocalize("bufftower_buff_2");
                }
                if (Player.instance.buffTowerBuffTime_3 > 0)
                {
                    if (!strBuff.Equals(string.Empty)) strBuff += "\n";
                    strBuff += LocalizeManager.GetLocalize("bufftower_buff_3");
                }

                if (PlayerBuffController.instance != null && PlayerBuffController.buffList.Count != 0)
                {
                    for (int i = 0; i < PlayerBuffController.buffList.Count; i++)
                    {
                        if (!strBuff.Equals(string.Empty)) strBuff += "\n";
                        strBuff += LocalizeManager.GetLocalize(PlayerBuffController.buffList[i] + "_cont");
                    }
                }
                objText.text = strBuff;

                string strBuff2 = string.Empty;
                int index_L = System.Array.FindIndex(SheetManager.Instance.ItemsDB.dataArray, item => item.ID.Equals(Player.instance.nowWeaponName));
                int index_R = System.Array.FindIndex(SheetManager.Instance.ItemsDB.dataArray, item => item.ID.Equals(Player.instance.nowRightWeaponName));
                int index_Q = System.Array.FindIndex(SheetManager.Instance.ItemsDB.dataArray, item => item.ID.Equals(Player.instance.qSkillName));
                if (index_L != -1)
                {
                    if (SheetManager.Instance.ItemsDB.dataArray[index_L].Attacktype.Equals("L"))
                        strBuff2 = Player.instance.damageMin + "-" + Player.instance.damageMax;
                    else
                        strBuff2 = Player.instance.damageNearMin + "-" + Player.instance.damageNearMax;
                    buffText[0].text = strBuff2;
                }
                else
                    buffText[0].text = string.Empty;
                if (index_R != -1)
                {
                    if (SheetManager.Instance.ItemsDB.dataArray[index_Q].ID.Contains("cell_shield"))
                    {
                        buffText[1].text = "-";
                    }
                    else
                    {
                        if (SheetManager.Instance.ItemsDB.dataArray[index_R].Attacktype.Equals("L"))
                            strBuff2 = Player.instance.damageRightMin + "-" + Player.instance.damageRightMax;
                        else
                            strBuff2 = Player.instance.damageNearRightMin + "-" + Player.instance.damageNearRightMax;
                        buffText[1].text = strBuff2;
                    }
                }
                else
                    buffText[1].text = string.Empty;
                if (index_Q != -1)
                {
                    if (SheetManager.Instance.ItemsDB.dataArray[index_Q].ID.Contains("q_change"))
                    {
                        buffText[2].text = "-";
                    }
                    else
                    {
                        if (SheetManager.Instance.ItemsDB.dataArray[index_Q].Attacktype.Equals("L"))
                            strBuff2 = Player.instance.damageQMin + "-" + Player.instance.damageQMax;
                        else
                            strBuff2 = Player.instance.damageNearQMin + "-" + Player.instance.damageNearQMax;
                        buffText[2].text = strBuff2;
                    }
                }
                else
                    buffText[2].text = string.Empty;
            }
            catch (System.Exception e)
            {
                //TextOpenController.instance.ShowText(e.Message, Color.white, 24, PDG.Player.instance.gameObject, 5.0f);
            }
        }
        public void OpenPause()
        {
            SoundManager.instance.StartAudio(new string[1] { "Pause" }, VOLUME_TYPE.EFFECT);
            isPause = !isPause;
            GameObject.Find("Canvas").transform.Find("UI").gameObject.SetActive(!isPause);
            Time.timeScale = (isPause ? 0 : 1);
            settingTrigger = (isPause ? 1 : 0);
            if (isPause)
            {
                SettingSafe();
            }
            else
            {
                OptionCanvas.SetActive(false);
                GamePlayCanvas.SetActive(false);
                VideoCanvas.SetActive(false);
                AudioCanvas.SetActive(false);
                LanguageCanvas.SetActive(false);
                KeySettingCanvas.SetActive(false);
            }
            SettingCanvas.SetActive(isPause);
        }
        public IEnumerator StartQuestionEvent(GameObject entranceQuestionPanel)
        {
            RectTransform rt = entranceQuestionPanel.GetComponent<RectTransform>();
            float y = 500.0f;
            while (true)
            {
                rt.anchoredPosition3D = new Vector3(0, y, 0);
                y -= Time.deltaTime * 1500.0f;
                if (y <= 50.0f) break;
                yield return new WaitForSeconds(Time.deltaTime);
            }
            y = 50.0f;
            rt.anchoredPosition3D = new Vector3(0, y, 0);
            while (true)
            {
                rt.anchoredPosition3D = new Vector3(0, y, 0);
                y += Time.deltaTime * (1500.0f / 4);
                if (y >= 70.0f) break;
                yield return new WaitForSeconds(Time.deltaTime);
            }
            y = 70.0f;
            rt.anchoredPosition3D = new Vector3(0, y, 0);
            while (true)
            {
                rt.anchoredPosition3D = new Vector3(0, y, 0);
                y -= Time.deltaTime * (1500.0f / 4);
                if (y <= 50.0f) break;
                yield return new WaitForSeconds(Time.deltaTime);
            }
            y = 50.0f;
            rt.anchoredPosition3D = new Vector3(0, y, 0);
            while (true)
            {
                rt.anchoredPosition3D = new Vector3(0, y, 0);
                y += Time.deltaTime * (1500.0f / 4);
                if (y >= 60.0f) break;
                yield return new WaitForSeconds(Time.deltaTime);
            }
            y = 60.0f;
            rt.anchoredPosition3D = new Vector3(0, y, 0);
            while (true)
            {
                rt.anchoredPosition3D = new Vector3(0, y, 0);
                y -= Time.deltaTime * (1500.0f / 4);
                if (y <= 50.0f) break;
                yield return new WaitForSeconds(Time.deltaTime);
            }
            y = 50.0f;
        }
        public void KeySetting(int _type)
        {
            nowKeySettingNum = _type;
            isChangeKey = true;

            AnyKeyObj.transform.Find("GameObject").Find("Image").GetComponent<SetUiSprite>()._joyKey = (JoystickKeySet)_type;
            if (isJoystrick)
                AnyKeyObj.transform.Find("GameObject").Find("Text").GetComponent<Text>().text = LocalizeManager.GetLocalize("ui_keysetting_" + ((JoystickKeySet)_type).ToString());
            else
                AnyKeyObj.transform.Find("GameObject").Find("Text").GetComponent<Text>().text = LocalizeManager.GetLocalize("keybaord_setting_" + _type);
            AnyKeyObj.SetActive(true);
        }
        private bool CheckingKeyChange(KeyCode _code)
        {
            if (!isJoystrick)
            {
                if (_code == KeyCode.Mouse0) return true;
                if (_code == KeyCode.Mouse1) return true;
                if (_code == KeyCode.BackQuote) return true;
                if (_code == KeyCode.Alpha0) return true;
                if (_code == KeyCode.Alpha1) return true;
                if (_code == KeyCode.Alpha2) return true;
                if (_code == KeyCode.Alpha3) return true;
                if (_code == KeyCode.Alpha4) return true;
                if (_code == KeyCode.Alpha5) return true;
                if (_code == KeyCode.Alpha6) return true;
                if (_code == KeyCode.Alpha7) return true;
                if (_code == KeyCode.Alpha8) return true;
                if (_code == KeyCode.Alpha9) return true;
                if (_code == KeyCode.Minus) return true;
                if (_code == KeyCode.Equals) return true;
                if (_code == KeyCode.Tab) return true;
                if (_code == KeyCode.CapsLock) return true;
                if (_code == KeyCode.A) return true;
                if (_code == KeyCode.B) return true;
                if (_code == KeyCode.C) return true;
                if (_code == KeyCode.D) return true;
                if (_code == KeyCode.E) return true;
                if (_code == KeyCode.F) return true;
                if (_code == KeyCode.G) return true;
                if (_code == KeyCode.H) return true;
                if (_code == KeyCode.I) return true;
                if (_code == KeyCode.J) return true;
                if (_code == KeyCode.K) return true;
                if (_code == KeyCode.L) return true;
                if (_code == KeyCode.M) return true;
                if (_code == KeyCode.N) return true;
                if (_code == KeyCode.O) return true;
                if (_code == KeyCode.P) return true;
                if (_code == KeyCode.Q) return true;
                if (_code == KeyCode.R) return true;
                if (_code == KeyCode.S) return true;
                if (_code == KeyCode.T) return true;
                if (_code == KeyCode.U) return true;
                if (_code == KeyCode.V) return true;
                if (_code == KeyCode.W) return true;
                if (_code == KeyCode.X) return true;
                if (_code == KeyCode.Y) return true;
                if (_code == KeyCode.Z) return true;
                if (_code == KeyCode.LeftBracket) return true;
                if (_code == KeyCode.RightBracket) return true;
                if (_code == KeyCode.Backslash) return true;
                if (_code == KeyCode.Semicolon) return true;
                if (_code == KeyCode.Quote) return true;
                if (_code == KeyCode.Comma) return true;
                if (_code == KeyCode.Period) return true;
                if (_code == KeyCode.Slash) return true;
                if (_code == KeyCode.Space) return true;

                return false;
            }
            else
            {
                if (joystricks[0].Contains("DS4") || joystricks[0].Contains("PC") || joystricks[0].Contains("Wireless"))
                {
                    if (_code == KeyCode.JoystickButton0) return true;
                    if (_code == KeyCode.JoystickButton1) return true;
                    if (_code == KeyCode.JoystickButton2) return true;
                    if (_code == KeyCode.JoystickButton3) return true;
                    if (_code == KeyCode.JoystickButton4) return true;
                    if (_code == KeyCode.JoystickButton5) return true;
                    if (_code == KeyCode.JoystickButton6) return true;
                    if (_code == KeyCode.JoystickButton7) return true;
                    if (_code == KeyCode.JoystickButton10) return true;
                    if (_code == KeyCode.JoystickButton11) return true;
                }
                else
                {
                    if (_code == KeyCode.JoystickButton0) return true;
                    if (_code == KeyCode.JoystickButton1) return true;
                    if (_code == KeyCode.JoystickButton2) return true;
                    if (_code == KeyCode.JoystickButton3) return true;
                    if (_code == KeyCode.JoystickButton4) return true;
                    if (_code == KeyCode.JoystickButton5) return true;
                    if (_code == KeyCode.JoystickButton8) return true;
                    if (_code == KeyCode.JoystickButton9) return true;
                }
            }
            return false;
        }
        public GameObject KeyResetObj;
        public void KeyBoardReSetBtn()
        {
            KeyResetObj.SetActive(true);
        }
        public void ReSetClose()
        {
            KeyResetObj.SetActive(false);
        }
        public void ReSetActive()
        {
            FPSDisplay.instance.ReSetKeyBoard();
            KeyResetObj.SetActive(false);
        }
        #endregion
        public IEnumerator DamageTextOpen(DamageText _damageText, float _damage)
        {
            while (true)
            {
                if (DamageCoroutine == null)
                {
                    DamageCoroutine = StartCoroutine(_damageText.DamageSetting(_damage));
                    yield return new WaitForFixedUpdate();
                    DamageCoroutine = null;
                    break;
                }
                yield return null;
            }
        }
        #endregion Methods

        private void OnGUI()
        {
            if (!isSaveMap)
                return;

            GUILayout.BeginArea(new Rect(10, 40, 215, 9999));

            if (GUILayout.Button("Save"))
            {
                SaveAndLoadManager.instance.MapSave();
            }
            if (GUILayout.Button("Load"))
            {
                SaveMap[] a = SaveAndLoadManager.instance.LoadMap(2);
                for (int i = 0; i < a.Length; i++)
                {
                    Debug.Log(a[i].id);
                }
            }
            GUILayout.EndArea();
        }
        
        private void GetKeyDownCheck(System.Action<bool> _keyDown)
        {
            if (Input.GetKeyDown(KeyCode.Mouse0) ||
                Input.GetKeyDown(KeyCode.Mouse1) ||
                Input.GetKeyDown(KeyCode.BackQuote) ||
                Input.GetKeyDown(KeyCode.BackQuote) ||
                Input.GetKeyDown(KeyCode.Alpha0) ||
                Input.GetKeyDown(KeyCode.Alpha1) ||
                Input.GetKeyDown(KeyCode.Alpha2) ||
                Input.GetKeyDown(KeyCode.Alpha3) ||
                Input.GetKeyDown(KeyCode.Alpha4) ||
                Input.GetKeyDown(KeyCode.Alpha5) ||
                Input.GetKeyDown(KeyCode.Alpha6) ||
                Input.GetKeyDown(KeyCode.Alpha7) ||
                Input.GetKeyDown(KeyCode.Alpha8) ||
                Input.GetKeyDown(KeyCode.Alpha9) ||
                Input.GetKeyDown(KeyCode.Minus) ||
                Input.GetKeyDown(KeyCode.Equals) ||
                Input.GetKeyDown(KeyCode.Tab) ||
                Input.GetKeyDown(KeyCode.CapsLock) ||
                Input.GetKeyDown(KeyCode.A) ||
                Input.GetKeyDown(KeyCode.B) ||
                Input.GetKeyDown(KeyCode.C) ||
                Input.GetKeyDown(KeyCode.D) ||
                Input.GetKeyDown(KeyCode.E) ||
                Input.GetKeyDown(KeyCode.F) ||
                Input.GetKeyDown(KeyCode.G) ||
                Input.GetKeyDown(KeyCode.H) ||
                Input.GetKeyDown(KeyCode.I) ||
                Input.GetKeyDown(KeyCode.J) ||
                Input.GetKeyDown(KeyCode.K) ||
                Input.GetKeyDown(KeyCode.L) ||
                Input.GetKeyDown(KeyCode.M) ||
                Input.GetKeyDown(KeyCode.N) ||
                Input.GetKeyDown(KeyCode.O) ||
                Input.GetKeyDown(KeyCode.P) ||
                Input.GetKeyDown(KeyCode.Q) ||
                Input.GetKeyDown(KeyCode.R) ||
                Input.GetKeyDown(KeyCode.S) ||
                Input.GetKeyDown(KeyCode.T) ||
                Input.GetKeyDown(KeyCode.U) ||
                Input.GetKeyDown(KeyCode.V) ||
                Input.GetKeyDown(KeyCode.W) ||
                Input.GetKeyDown(KeyCode.X) ||
                Input.GetKeyDown(KeyCode.Y) ||
                Input.GetKeyDown(KeyCode.Z) ||
                Input.GetKeyDown(KeyCode.LeftBracket) ||
                Input.GetKeyDown(KeyCode.RightBracket) ||
                Input.GetKeyDown(KeyCode.Backslash) ||
                Input.GetKeyDown(KeyCode.Semicolon) ||
                Input.GetKeyDown(KeyCode.Quote) ||
                Input.GetKeyDown(KeyCode.Comma) ||
                Input.GetKeyDown(KeyCode.Period) ||
                Input.GetKeyDown(KeyCode.Slash) ||
                Input.GetKeyDown(KeyCode.UpArrow) ||
                Input.GetKeyDown(KeyCode.DownArrow) ||
                Input.GetKeyDown(KeyCode.RightArrow) ||
                Input.GetKeyDown(KeyCode.LeftArrow) ||
                Input.GetKeyDown(KeyCode.Space))
            {
                _keyDown(true);
            }
            else
            {
                if (joystricks.Count == 0) return;
                if (joystricks[0].Contains("DS4") || joystricks[0].Contains("PC") || joystricks[0].Contains("Wireless"))
                {
                    float joyCrossX = Input.GetAxisRaw("PS4_crossX");
                    float joyCrossY = Input.GetAxisRaw("PS4_crossY");

                    if (Input.GetKeyDown(KeyCode.JoystickButton0) ||
                        Input.GetKeyDown(KeyCode.JoystickButton1) ||
                        Input.GetKeyDown(KeyCode.JoystickButton2) ||
                        Input.GetKeyDown(KeyCode.JoystickButton3) ||
                        Input.GetKeyDown(KeyCode.JoystickButton4) ||
                        Input.GetKeyDown(KeyCode.JoystickButton5) ||
                        Input.GetKeyDown(KeyCode.JoystickButton6) ||
                        Input.GetKeyDown(KeyCode.JoystickButton7) ||
                        Input.GetKeyDown(KeyCode.JoystickButton10) ||
                        Input.GetKeyDown(KeyCode.JoystickButton11) ||
                        joyCrossY > 0 || joyCrossY < 0 ||
                        joyCrossX > 0 || joyCrossX < 0
                        )
                    {
                        _keyDown(false);
                    }
                }
                else
                {
                    float joyCrossX = Input.GetAxisRaw("Horizontal_joy");
                    float joyCrossY = Input.GetAxisRaw("Vertical_joy");

                    if (Input.GetKeyDown(KeyCode.JoystickButton0) ||
                        Input.GetKeyDown(KeyCode.JoystickButton1) ||
                        Input.GetKeyDown(KeyCode.JoystickButton2) ||
                        Input.GetKeyDown(KeyCode.JoystickButton3) ||
                        Input.GetKeyDown(KeyCode.JoystickButton4) ||
                        Input.GetKeyDown(KeyCode.JoystickButton5) ||
                        Input.GetKeyDown(KeyCode.JoystickButton8) ||
                        Input.GetKeyDown(KeyCode.JoystickButton9) ||
                        joyCrossY > 0 || joyCrossY < 0 ||
                        joyCrossX > 0 || joyCrossX < 0
                        )
                    {
                        _keyDown(false);
                    }
                }
            }
        }
    }
}