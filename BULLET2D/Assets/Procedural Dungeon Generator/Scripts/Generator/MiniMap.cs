﻿using PDG;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MiniMap : MonoBehaviour
{
    GameObject _hero;
    SpriteRenderer sr;
    [SerializeField] private bool isMonster = false;
    private void Start()
    {
        sr = GetComponent<SpriteRenderer>();
    }

    private void Update()
    {
        if (_hero == null) _hero = Dungeon.instance.HeroObj;
        if (_hero == null) return;

        if (isMonster && !Player.instance.isScouter) sr.enabled = false;

        float dis = Vector3.Distance(_hero.transform.position, transform.position);
        if (isMonster && dis <= 80 && Player.instance.isScouter) sr.enabled = true;
        else if (isMonster && dis > 80 && Player.instance.isScouter) sr.enabled = false;
        if (dis > 8.0f) return;
        if (sr.enabled) return;
        if(!isMonster)
        {
            sr.enabled = true;
            enabled = false;
        }

    }
}
