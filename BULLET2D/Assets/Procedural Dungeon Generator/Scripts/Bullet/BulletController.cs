﻿using PDG;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletController : MonoBehaviour
{
    public string bulletType;

    private Rigidbody2D rigid;
    private SpriteRenderer sr;

    private bool isAnim = false;
    [Header("Attack")]
    public Vector3 dir;
    public float fSpeed;
    public float fAngle;
    public float fRange = 10.0f;
    //public GameObject fireOwnerObj;
    public Vector3 firePosition;
    public GameObject harpoon_tentaclesObj;
    public GameObject fanatic_dagger_hit_effect_1;

    public bool isPenetrate = false;
    AudioSource _audio;
    [SerializeField] private AudioClip _balkanClip;
    [SerializeField] private AudioClip _defaultClip;
    [Header("Monster")]
    public bool isMonster = false;
    public string monsterName = string.Empty;
    public bool isMiniGame = false;

    [SerializeField] private GameObject destroyObj;
    [SerializeField] private GameObject livingDestroyObj;
    [SerializeField] private GameObject secret_lasergunDestroyObj;
    [SerializeField] private GameObject secret_watergunDestroyObj;
    bool isRotate = false;

    public float hitTime = 0.25f;
    [SerializeField] private List<GameObject> objList = new List<GameObject>();
    [SerializeField] private List<float> timeList = new List<float>();
    private float rotSpeed = 30;
    Vector2 size = new Vector2(0.5f, 0.3f);
    public bool isRightAttack = false;
    private int bounsCount = 0;
    float bounsDelayTime = 0.15f;
    public bool isSign = false;
    private void Awake()
    {
        rigid = GetComponent<Rigidbody2D>();
        sr = GetComponent<SpriteRenderer>();
        _audio = GetComponent<AudioSource>();
    }
    private void OnEnable()
    {
        bounsCount = 0;
        StartCoroutine(Hoxy());
    }
    IEnumerator Hoxy()
    {
        yield return new WaitForSeconds(3.0f);
        if (bulletType.Contains("harpoon_tentacles"))
        {
            GameObject obj2 = Instantiate(harpoon_tentaclesObj);
            obj2.transform.position = transform.position;
            obj2.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
            Destroy(obj2, 1.5f);
        }
        else if (bulletType.Contains("elastic_cell_weapon"))
        {
            GameObject obj2 = Instantiate(destroyObj);
            obj2.transform.position = transform.position;
            obj2.transform.localScale = Vector3.one;
            Destroy(obj2, 1.5f);
        }
        else
        {
            GameObject obj = ObjManager.Call().GetObject("Bullet_Destroy");
            if (obj != null)
            {
                float dis = Vector3.Distance(PDG.Player.instance.transform.position, transform.position);
                if (dis > 5) _audio.volume = 0;
                float tVolume =  (SoundManager.isMute || (PDG.Dungeon.instance == null ? false : PDG.Dungeon.instance.isPause) || (PDG.Player.instance == null ? false : PDG.Player.instance.isDie)) ? 0 : SoundManager.EffectVolume;
                float t = tVolume / 5;
                _audio.volume = tVolume - (dis * t);
                if (_audio.clip != null) _audio.Play();
                obj.transform.position = transform.position;
                obj.transform.localEulerAngles = new Vector3(0, 0, fAngle);
                obj.SetActive(true);
            }
        }
        gameObject.SetActive(false);
    }
    public void BulletSetting()
    {
        bounsCount = 0;
        sr.enabled = true;
        if (transform.Find("secret_lasergun")) transform.Find("secret_lasergun").gameObject.SetActive(false);
        if (transform.Find("secret_watergun")) transform.Find("secret_watergun").gameObject.SetActive(false);
        sr.sprite = Resources.Load<Sprite>("Bullet/" + bulletType);
        if (bulletType.Equals("balkan_bullet"))
        {
            if (GetComponent<SpriteOutline>()) GetComponent<SpriteOutline>().color = Color.blue;
            if (GetComponent<SpriteOutline>()) GetComponent<SpriteOutline>().Regenerate();
            _audio.clip = _balkanClip;
        }
        else
        {
            if (GetComponent<SpriteOutline>()) GetComponent<SpriteOutline>().color = Color.red;
            if (GetComponent<SpriteOutline>()) GetComponent<SpriteOutline>().Regenerate();
            _audio.clip = _defaultClip;
        }
        isMiniGame = false;
        objList.Clear();
        timeList.Clear();
        hitTime = 0.25f;
    }
    public void BulletFire(bool _isSign = false)
    {
        isSign = _isSign;
        bounsCount = 0;
        if (bulletType.Contains("fanatic_dagger"))
        {
            bulletType = Player.instance.nowWeaponName;
            isRotate = true;
            if (Player.instance.nowWeaponName.Equals("fanatic_dagger_1"))
            {
                isPenetrate = true;
                size = new Vector2(3.5f, 3.5f);
            }
            else
            {
                isPenetrate = false;
                size = new Vector2(0.3f, 0.3f);
            }
        }
        else
        {
            size = new Vector2(0.5f, 0.3f);
            isRotate = false;
        }
        if (sr) sr.sprite = Resources.Load<Sprite>("Bullet/" + bulletType);
        if (!isSign)
            rigid.AddForce(dir * fSpeed, ForceMode2D.Impulse);
        else
        {
            StartCoroutine(SpecialFire());
        }
        transform.localEulerAngles = new Vector3(0, 0, fAngle);
        if (bulletType.Contains("living_arrow"))
        {
            if (transform.Find("secret_lasergun")) transform.Find("secret_lasergun").gameObject.SetActive(false);
            if (transform.Find("secret_watergun")) transform.Find("secret_watergun").gameObject.SetActive(false);
            if (sr) sr.enabled = true;
            if (GetComponent<SpriteOutline>()) GetComponent<SpriteOutline>().Clear();
            _audio.clip = null;
        }
        else if (bulletType.Contains("secret_lasergun"))
        {
            if (transform.Find("secret_lasergun")) transform.Find("secret_lasergun").gameObject.SetActive(true);
            if (transform.Find("secret_watergun")) transform.Find("secret_watergun").gameObject.SetActive(false);
            if (GetComponent<SpriteOutline>()) GetComponent<SpriteOutline>().Clear();
            _audio.clip = null;
            if (sr) sr.enabled = false;
        }
        else if (bulletType.Contains("secret_watergun"))
        {
            if (transform.Find("secret_lasergun")) transform.Find("secret_lasergun").gameObject.SetActive(false);
            if (transform.Find("secret_watergun")) transform.Find("secret_watergun").gameObject.SetActive(true);
            if (GetComponent<SpriteOutline>()) GetComponent<SpriteOutline>().Clear();
            _audio.clip = null;
            if (sr) sr.enabled = false;
        }
        else if (bulletType.Equals("balkan_bullet"))
        {
            if (transform.Find("secret_lasergun")) transform.Find("secret_lasergun").gameObject.SetActive(false);
            if (transform.Find("secret_watergun")) transform.Find("secret_watergun").gameObject.SetActive(false);
            if (sr) sr.enabled = true;
            if (GetComponent<SpriteOutline>()) GetComponent<SpriteOutline>().color = Color.blue;
            if (GetComponent<SpriteOutline>()) GetComponent<SpriteOutline>().Regenerate();
            _audio.clip = _balkanClip;
        }
        else if(Player.instance.nowWeaponName.Contains("start_gun") && !isMonster)
        {
            if (transform.Find("secret_lasergun")) transform.Find("secret_lasergun").gameObject.SetActive(false);
            if (transform.Find("secret_watergun")) transform.Find("secret_watergun").gameObject.SetActive(false);
            if (sr) sr.enabled = true;
            if (GetComponent<SpriteOutline>()) GetComponent<SpriteOutline>().color = Color.green;
            if (GetComponent<SpriteOutline>()) GetComponent<SpriteOutline>().Regenerate();
            _audio.clip = _balkanClip;
        }
        else
        {
            if (transform.Find("secret_lasergun")) transform.Find("secret_lasergun").gameObject.SetActive(false);
            if (transform.Find("secret_watergun")) transform.Find("secret_watergun").gameObject.SetActive(false);
            if (sr) sr.enabled = true;
            if (GetComponent<SpriteOutline>()) GetComponent<SpriteOutline>().color = Color.red;
            if (GetComponent<SpriteOutline>()) GetComponent<SpriteOutline>().Regenerate();
            _audio.clip = _defaultClip;
        }

        isMiniGame = false;
        objList.Clear();
        timeList.Clear();

        if (isPenetrate)
        {
            if (sr)
                sr.sortingLayerName = "Fog";
        }
        else
        {
            if (sr)
                sr.sortingLayerName = "Attack";
        }

        if (isMonster) 
        {
            hitTime = 0.25f;
        }
        else
        {
            hitTime = Player.instance.tickTime;
        }
    }
    IEnumerator SpecialFire()
    {
        int signCount = 7;
        int t = 0;
        float tt = 0.0f;
        Vector3 endPosition = transform.position + (dir * fRange);
        Vector3 minusPosition = endPosition - transform.position;
        Vector3[] endPositions = new Vector3[signCount];
        for (int i = 0; i < endPositions.Length; i++)
            endPositions[i] = transform.position + ((minusPosition / signCount) * (i + 1));
        Vector3 startPosition = transform.position;

        float angle = GetAngle(transform.position, transform.position + dir);

        while (true)
        {
            Vector3 centerPosition = startPosition + ((endPositions[t] - startPosition) / 2);
            Vector3 centerPositionTemp = GetPosition(centerPosition, (t % 2 == 0 ? angle - 90 : angle + 90));
            Vector3 centerPositionTemp2 = centerPositionTemp - centerPosition;
            centerPositionTemp2.Normalize();
            centerPosition += centerPositionTemp2 * 3.0f;

            Vector3 lerp_0 = Vector3.Lerp(startPosition, centerPosition, tt);
            Vector3 lerp_1 = Vector3.Lerp(centerPosition, endPositions[t], tt);
            transform.position = Vector3.Lerp(lerp_0, lerp_1, tt);
            tt += Time.fixedDeltaTime;
            if (tt >= 1.0f)
            {
                startPosition = endPositions[t];
                tt = 0.0f;
                t++;
                if (tt > signCount) break;
            }
            yield return new WaitForFixedUpdate();
        }
    }
    private void Update()
    {
        if(bounsDelayTime > 0)
        {
            bounsDelayTime -= Time.deltaTime;
            return;
        }
        if(isRotate)
        {
            float z = transform.localEulerAngles.z;
            
            if (dir.x < 0)
                z -= rotSpeed;
            else
                z += rotSpeed;
            transform.localEulerAngles = new Vector3(0, 0, z);
        }
        for (int i = 0; i < objList.Count; i++)
        {
            if (objList[i] == null)
            {
                objList.RemoveAt(i);
                timeList.RemoveAt(i);
                i--;
                continue;
            }
            else if (objList[i].GetComponent<MOB.MonsterArggro>())
            {
                if (objList[i].GetComponent<MOB.MonsterArggro>()._monster.isDie)
                {
                    objList.RemoveAt(i);
                    timeList.RemoveAt(i);
                    i--;
                    continue;
                }
            }
            else if (objList[i].GetComponent<MOB.Monster>())
            {
                if (objList[i].GetComponent<MOB.Monster>().isDie)
                {
                    objList.RemoveAt(i);
                    timeList.RemoveAt(i);
                    i--;
                    continue;
                }
            }
            timeList[i] += Time.deltaTime;
        }
        if (Vector3.Distance(firePosition, transform.position) > fRange)
        {
            if (bulletType.Contains("harpoon_tentacles"))
            {
                GameObject obj2 = Instantiate(harpoon_tentaclesObj);
                obj2.transform.position = transform.position;
                obj2.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
                Destroy(obj2, 1.5f);
            }
            else if (bulletType.Contains("elastic_cell_weapon"))
            {
                GameObject obj2 = Instantiate(destroyObj);
                obj2.transform.position = transform.position;
                obj2.transform.localScale = new Vector3(0.3f, 0.3f, 0.3f);
                Destroy(obj2, 1.5f);
            }
            else if (bulletType.Contains("living_arrow"))
            {
                GameObject obj2 = Instantiate(destroyObj);
                obj2.transform.position = transform.position;
                obj2.transform.localScale = new Vector3(0.3f, 0.3f, 0.3f);
                Destroy(obj2, 1.5f);
            }
            else if (bulletType.Contains("secret_lasergun"))
            {
                GameObject obj2 = Instantiate(secret_lasergunDestroyObj);
                obj2.transform.position = transform.position;
                obj2.transform.localScale = new Vector3(0.3f, 0.3f, 0.3f);
                Destroy(obj2, 1.5f);
            }
            else if (bulletType.Contains("secret_watergun"))
            {
                GameObject obj2 = Instantiate(secret_watergunDestroyObj);
                obj2.transform.position = transform.position;
                obj2.transform.localScale = new Vector3(1, 1, 1);
                Destroy(obj2, 1.5f);
            }
            else
            {
                GameObject obj = ObjManager.Call().GetObject("Bullet_Destroy");
                if (obj != null)
                {
                    float dis = Vector3.Distance(Player.instance.transform.position, transform.position);
                    if (dis > 5) _audio.volume = 0;
                    float tVolume =  (SoundManager.isMute || (PDG.Dungeon.instance == null ? false : PDG.Dungeon.instance.isPause) || (PDG.Player.instance == null ? false : PDG.Player.instance.isDie)) ? 0 : SoundManager.EffectVolume;
                    float t = tVolume / 5;
                    _audio.volume = tVolume - (dis * t);
                    if (_audio.clip != null) _audio.Play();
                    obj.transform.position = transform.position;
                    obj.transform.localEulerAngles = new Vector3(0, 0, fAngle);
                    obj.SetActive(true);
                }
            }
            gameObject.SetActive(false);
        }

        int mask = (isMonster ? (1 << 0 | 1 << 27) | 1 << 9 : 1 << 12 | 1 << 24) | (isPenetrate ? 1 << 23 : 1 << 23 | 1 << 8);
        RaycastHit2D[] hits = Physics2D.CapsuleCastAll(transform.position, size, CapsuleDirection2D.Horizontal, transform.eulerAngles.z, Vector2.zero, 0, mask);
        if (hits.Length > 0)
        {
            for (int i = 0; i < hits.Length; i++)
            {
                Collider2D collision = hits[i].collider;
                GameObject obj333 = collision.gameObject;
                int index = objList.FindIndex(item => item.Equals(obj333));
                if (index != -1)
                {
                    if (hitTime > timeList[index]) return;
                    else timeList[index] = 0.0f;
                }
                else
                {
                    objList.Add(obj333);
                    timeList.Add(0.0f);
                }

                if ((collision.gameObject.layer.Equals(0) || collision.gameObject.layer.Equals(27)) && isMonster)
                {
                    if (collision.name.Equals("Shield"))
                    {
                        Player.instance.right_defence_gauge -= 1.0f;
                        Inventorys.Instance.GuageShaker(1);
                        CameraShaker._instance.StartShake(0.2f, 0.02f, 0.2f);
                    }
                    if (collision.name.Contains("dna_mo_blade"))
                    {
                        Player.instance.left_defence_gauge -= 1.0f;
                        Inventorys.Instance.GuageShaker(0);
                        CameraShaker._instance.StartShake(0.2f, 0.02f, 0.2f);
                    }

                    if (collision.GetComponent<BossRoomObject>())
                    {
                        collision.GetComponent<BossRoomObject>().SetDamage();
                    }

                    if (bulletType.Equals("fanatic_dagger"))
                    {
                        GameObject obj2 = ObjManager.Call().GetObject("Hit_sprite7");
                        obj2.transform.position = transform.position;
                        obj2.transform.localScale = Vector3.one;
                        obj2.GetComponent<SwordHitEffectController>()._effctName = "fanatic_dagger_hit_" + Random.Range(0, 2);
                        obj2.GetComponent<SwordHitEffectController>().animSpeed = 1.0f;
                        obj2.GetComponent<Animator>().Rebind();
                        obj2.SetActive(true);
                    }
                    else if (bulletType.Equals("fanatic_dagger_1"))
                    {
                        GameObject obj222 = Instantiate(fanatic_dagger_hit_effect_1);
                        obj222.transform.position = transform.position;
                        Destroy(obj222, 2f);
                    }
                    else if (bulletType.Contains("harpoon_tentacles"))
                    {
                        GameObject obj2 = Instantiate(harpoon_tentaclesObj);
                        obj2.transform.position = transform.position;
                        obj2.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
                        Destroy(obj2, 1.5f);
                    }
                    else if (bulletType.Contains("elastic_cell_weapon"))
                    {
                        GameObject obj2 = Instantiate(destroyObj);
                        obj2.transform.position = transform.position;
                        obj2.transform.localScale = Vector3.one;
                        Destroy(obj2, 1.5f);
                    }
                    else
                    {
                        GameObject objs = ObjManager.Call().GetObject("Bullet_Hit");
                        objs.transform.position = transform.position;
                        objs.GetComponent<SwordHitEffectController>()._isMonster = isMonster;
                        objs.GetComponent<SwordHitEffectController>()._effctName = "pistol_bullet";
                        objs.GetComponent<Animator>().Rebind();
                        objs.transform.localEulerAngles = new Vector3(0, 0, 0);
                        objs.SetActive(true);
                    }
                    gameObject.SetActive(false);
                }

                GameObject obj = null;
                if (collision.gameObject.layer.Equals(23) && !isMonster)
                {
                    if (collision.transform.parent && collision.transform.parent.GetComponent<TentacleController>())
                    {
                        collision.transform.parent.GetComponent<TentacleController>().SetDamage(1);

                        if (bulletType.Equals("fanatic_dagger"))
                        {
                            GameObject obj2 = ObjManager.Call().GetObject("Hit_sprite7");
                            obj2.transform.position = transform.position;
                            obj2.transform.localScale = Vector3.one;
                            obj2.GetComponent<SwordHitEffectController>()._effctName = "fanatic_dagger_hit_" + Random.Range(0, 2);
                            obj2.GetComponent<SwordHitEffectController>().animSpeed = 1.0f;
                            obj2.GetComponent<Animator>().Rebind();
                            obj2.SetActive(true);
                        }
                        else if (bulletType.Equals("fanatic_dagger_1"))
                        {
                            GameObject obj222 = Instantiate(fanatic_dagger_hit_effect_1);
                            obj222.transform.position = transform.position;
                            Destroy(obj222, 2f);
                        }
                        else if (bulletType.Contains("harpoon_tentacles"))
                        {
                            GameObject obj2 = Instantiate(harpoon_tentaclesObj);
                            obj2.transform.position = transform.position;
                            obj2.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
                            Destroy(obj2, 1.5f);
                        }
                        else if (bulletType.Contains("elastic_cell_weapon"))
                        {
                            GameObject obj2 = Instantiate(destroyObj);
                            obj2.transform.position = transform.position;
                            obj2.transform.localScale = Vector3.one;
                            Destroy(obj2, 1.5f);
                        }
                        else
                        {
                            obj = ObjManager.Call().GetObject("Bullet_Hit");
                            obj.GetComponent<SwordHitEffectController>()._isMonster = isMonster;
                        }
                    }
                    if (collision.gameObject.GetComponent<ChestHit>())
                    {
                        collision.gameObject.GetComponent<ChestHit>().SetDamage(1);

                        if (bulletType.Equals("fanatic_dagger"))
                        {
                            GameObject obj2 = ObjManager.Call().GetObject("Hit_sprite7");
                            obj2.transform.position = transform.position;
                            obj2.transform.localScale = Vector3.one;
                            obj2.GetComponent<SwordHitEffectController>()._effctName = "fanatic_dagger_hit_" + Random.Range(0, 2);
                            obj2.GetComponent<SwordHitEffectController>().animSpeed = 1.0f;
                            obj2.GetComponent<Animator>().Rebind();
                            obj2.SetActive(true);
                        }
                        else if (bulletType.Equals("fanatic_dagger_1"))
                        {
                            GameObject obj222 = Instantiate(fanatic_dagger_hit_effect_1);
                            obj222.transform.position = transform.position;
                            Destroy(obj222, 2f);
                        }
                        else if (bulletType.Contains("harpoon_tentacles"))
                        {
                            GameObject obj2 = Instantiate(harpoon_tentaclesObj);
                            obj2.transform.position = transform.position;
                            obj2.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
                            Destroy(obj2, 1.5f);
                        }
                        else if (bulletType.Contains("elastic_cell_weapon"))
                        {
                            GameObject obj2 = Instantiate(destroyObj);
                            obj2.transform.position = transform.position;
                            obj2.transform.localScale = Vector3.one;
                            Destroy(obj2, 1.5f);
                        }
                        else
                        {
                            obj = ObjManager.Call().GetObject("Bullet_Hit");
                            obj.GetComponent<SwordHitEffectController>()._isMonster = isMonster;
                        }
                    }
                    else if (collision.gameObject.GetComponent<ObjectTrap>())
                    {
                        ObjectTrap objtrap = collision.gameObject.GetComponent<ObjectTrap>();
                        SoundManager.instance.StartAudio(new string[1] { "Object/target_" + Random.Range(0, 2) }, VOLUME_TYPE.EFFECT);
                        if (objtrap.animator != null) objtrap.animator.SetTrigger("Attack");
                        if (objtrap.isTarget)
                        {
                            objtrap.targetCount++;
                            objtrap.StartCoroutine(objtrap.PanelOpen(true));
                        }
                        else
                        {
                            objtrap.StartCoroutine(objtrap.PanelOpen(false));
                        }

                        Inventorys.Instance.miniGameBulletUnCount++;
                        GameObject obj22 = ObjManager.Call().GetObject("Bullet_Hit");
                        obj22.GetComponent<SwordHitEffectController>()._isMonster = isMonster;
                        obj22.transform.position = transform.position;
                        obj22.GetComponent<SwordHitEffectController>()._effctName = "pistol_bullet";
                        obj22.GetComponent<Animator>().Rebind();
                        obj22.SetActive(true);
                        gameObject.SetActive(false);
                    }
                    else if (collision.gameObject.GetComponent<DrumHit>())
                    {
                        collision.gameObject.GetComponent<DrumHit>().SetDamage(1);

                        if (bulletType.Equals("fanatic_dagger"))
                        {
                            GameObject obj2 = ObjManager.Call().GetObject("Hit_sprite7");
                            obj2.transform.position = transform.position;
                            obj2.transform.localScale = Vector3.one;
                            obj2.GetComponent<SwordHitEffectController>()._effctName = "fanatic_dagger_hit_" + Random.Range(0, 2);
                            obj2.GetComponent<SwordHitEffectController>().animSpeed = 1.0f;
                            obj2.GetComponent<Animator>().Rebind();
                            obj2.SetActive(true);
                        }
                        else if (bulletType.Equals("fanatic_dagger_1"))
                        {
                            GameObject obj222 = Instantiate(fanatic_dagger_hit_effect_1);
                            obj222.transform.position = transform.position;
                            Destroy(obj222, 2f);
                        }
                        else
                        {
                            obj = ObjManager.Call().GetObject("Bullet_Hit");
                            obj.GetComponent<SwordHitEffectController>()._isMonster = isMonster;
                        }
                    }
                    else if (collision.gameObject.GetComponent<DestroyObjects>())
                    {
                        collision.gameObject.GetComponent<DestroyObjects>().DestroyObject(dir);


                        if (bulletType.Equals("fanatic_dagger"))
                        {
                            GameObject obj2 = ObjManager.Call().GetObject("Hit_sprite7");
                            obj2.transform.position = transform.position;
                            obj2.transform.localScale = Vector3.one;
                            obj2.GetComponent<SwordHitEffectController>()._effctName = "fanatic_dagger_hit_" + Random.Range(0, 2);
                            obj2.GetComponent<SwordHitEffectController>().animSpeed = 1.0f;
                            obj2.GetComponent<Animator>().Rebind();
                            obj2.SetActive(true);
                        }
                        else if (bulletType.Equals("fanatic_dagger_1"))
                        {
                            GameObject obj222 = Instantiate(fanatic_dagger_hit_effect_1);
                            obj222.transform.position = transform.position;
                            Destroy(obj222, 2f);
                        }
                        else if (bulletType.Contains("harpoon_tentacles"))
                        {
                            GameObject obj2 = Instantiate(harpoon_tentaclesObj);
                            obj2.transform.position = transform.position;
                            obj2.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
                            Destroy(obj2, 1.5f);
                        }
                        else if (bulletType.Contains("elastic_cell_weapon"))
                        {
                            GameObject obj2 = Instantiate(destroyObj);
                            obj2.transform.position = transform.position;
                            obj2.transform.localScale = Vector3.one;
                            Destroy(obj2, 1.5f);
                        }
                        else
                        {
                            obj = ObjManager.Call().GetObject("Bullet_Hit");
                            obj.GetComponent<SwordHitEffectController>()._isMonster = isMonster;
                        }
                    }
                }
                else if (collision.gameObject.layer.Equals(12) && !isMonster)
                {

                    if (bulletType.Equals("fanatic_dagger"))
                    {
                        GameObject obj2 = ObjManager.Call().GetObject("Hit_sprite7");
                        obj2.transform.position = transform.position;
                        obj2.transform.localScale = Vector3.one;
                        obj2.GetComponent<SwordHitEffectController>()._effctName = "fanatic_dagger_hit_" + Random.Range(0, 2);
                        obj2.GetComponent<SwordHitEffectController>().animSpeed = 1.0f;
                        obj2.GetComponent<Animator>().Rebind();
                        obj2.SetActive(true);
                    }
                    else if (bulletType.Equals("fanatic_dagger_1"))
                    {
                        GameObject obj222 = Instantiate(fanatic_dagger_hit_effect_1);
                        obj222.transform.position = transform.position;
                        Destroy(obj222, 2f);
                    }
                    else if (bulletType.Contains("harpoon_tentacles"))
                    {
                        GameObject obj2 = Instantiate(harpoon_tentaclesObj);
                        obj2.transform.position = transform.position;
                        obj2.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
                        Destroy(obj2, 1.5f);
                    }
                    else if (bulletType.Contains("elastic_cell_weapon"))
                    {
                        GameObject obj2 = Instantiate(destroyObj);
                        obj2.transform.position = transform.position;
                        obj2.transform.localScale = Vector3.one;
                        Destroy(obj2, 1.5f);
                    }
                    else if (bulletType.Contains("living_arrow"))
                    {
                        GameObject obj2 = Instantiate(livingDestroyObj);
                        obj2.transform.position = transform.position;
                        obj2.transform.localScale = new Vector3(0.3f, 0.3f, 0.3f);
                        Destroy(obj2, 1.5f);
                    }
                    else if (bulletType.Contains("secret_lasergun"))
                    {
                        GameObject obj2 = Instantiate(secret_lasergunDestroyObj);
                        obj2.transform.position = transform.position;
                        obj2.transform.localScale = new Vector3(0.3f, 0.3f, 0.3f);
                        Destroy(obj2, 1.5f);
                    }
                    else if (bulletType.Contains("secret_watergun"))
                    {
                        GameObject obj2 = Instantiate(secret_watergunDestroyObj);
                        obj2.transform.position = transform.position;
                        obj2.transform.localScale = new Vector3(1, 1, 1);
                        Destroy(obj2, 1.5f);
                    }
                    else
                    {
                        obj = ObjManager.Call().GetObject("Bullet_Hit");
                        obj.GetComponent<SwordHitEffectController>()._isMonster = isMonster;
                    }
                    if (collision.GetComponent<MOB.Monster>())
                    {
                        if (isRightAttack)
                        {
                            collision.GetComponent<MOB.Monster>().SetDamage((Player.instance.isRightNear ? Player.instance.damageRightNear : Player.instance.damageRight), -dir, 1, Player.instance.right_elemetanl, Player.instance.right_elemnetalTime);
                            if (Player.instance.isRightNear && Player.instance.isCriticalRight)
                            {
                                GameObject criEffect = ObjManager.Call().GetObject("CriticalHitEffect");
                                criEffect.transform.position = collision.transform.position;
                                criEffect.SetActive(true);
                            }
                        }
                        else
                        {
                            collision.GetComponent<MOB.Monster>().SetDamage((Player.instance.isLeftNear ? Player.instance.damageNear : Player.instance.damage), -dir, 1, PDG.Player.instance.left_elemetanl, PDG.Player.instance.left_elemnetalTime);
                            if (Player.instance.isLeftNear && Player.instance.isCritical)
                            {
                                GameObject criEffect = ObjManager.Call().GetObject("CriticalHitEffect");
                                criEffect.transform.position = collision.transform.position;
                                criEffect.SetActive(true);
                            }
                        }

                    }
                    if (PDG.Player.instance.isStrategic_02) PDG.Player.instance.isStrategic_02 = false;
                }
                else if (collision.gameObject.layer.Equals(24) && !isMonster)
                {

                    if (bulletType.Equals("fanatic_dagger"))
                    {
                        GameObject obj2 = ObjManager.Call().GetObject("Hit_sprite7");
                        obj2.transform.position = transform.position;
                        obj2.transform.localScale = Vector3.one;
                        obj2.GetComponent<SwordHitEffectController>()._effctName = "fanatic_dagger_hit_" + Random.Range(0, 2);
                        obj2.GetComponent<SwordHitEffectController>().animSpeed = 1.0f;
                        obj2.GetComponent<Animator>().Rebind();
                        obj2.SetActive(true);
                    }
                    else if (bulletType.Equals("fanatic_dagger_1"))
                    {
                        GameObject obj222 = Instantiate(fanatic_dagger_hit_effect_1);
                        obj222.transform.position = transform.position;
                        Destroy(obj222, 2f);
                    }
                    else if (bulletType.Contains("harpoon_tentacles"))
                    {
                        GameObject obj2 = Instantiate(harpoon_tentaclesObj);
                        obj2.transform.position = transform.position;
                        obj2.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
                        Destroy(obj2, 1.5f);
                    }
                    else if (bulletType.Contains("elastic_cell_weapon"))
                    {
                        GameObject obj2 = Instantiate(destroyObj);
                        obj2.transform.position = transform.position;
                        obj2.transform.localScale = Vector3.one;
                        Destroy(obj2, 1.5f);
                    }
                    else if (bulletType.Contains("living_arrow"))
                    {
                        GameObject obj2 = Instantiate(livingDestroyObj);
                        obj2.transform.position = transform.position;
                        obj2.transform.localScale = new Vector3(0.3f, 0.3f, 0.3f);
                        Destroy(obj2, 1.5f);
                    }
                    else if (bulletType.Contains("secret_lasergun"))
                    {
                        GameObject obj2 = Instantiate(secret_lasergunDestroyObj);
                        obj2.transform.position = transform.position;
                        obj2.transform.localScale = new Vector3(0.3f, 0.3f, 0.3f);
                        Destroy(obj2, 1.5f);
                    }
                    else if (bulletType.Contains("secret_watergun"))
                    {
                        GameObject obj2 = Instantiate(secret_watergunDestroyObj);
                        obj2.transform.position = transform.position;
                        obj2.transform.localScale = new Vector3(1, 1, 1);
                        Destroy(obj2, 1.5f);
                    }
                    else
                    {
                        obj = ObjManager.Call().GetObject("Bullet_Hit");
                        obj.GetComponent<SwordHitEffectController>()._isMonster = isMonster;
                    }
                    if (isRightAttack)
                    {
                        if (collision.GetComponent<MOB.BossMonster_Six_Apear>()) collision.GetComponent<MOB.BossMonster_Six_Apear>().SetDamage(Player.instance.damageRight);
                        if (collision.GetComponent<Boss_STG>()) collision.GetComponent<Boss_STG>().SetDamage(Player.instance.isRightNear ? Player.instance.damageRightNear : Player.instance.damageRight, hits[i].point);
                        if (collision.GetComponent<Boss_NPC>()) collision.GetComponent<Boss_NPC>().SetDamage(Player.instance.isRightNear ? Player.instance.damageRightNear : Player.instance.damageRight, hits[i].point);
                        if (collision.GetComponent<Boss_Golem>()) collision.GetComponent<Boss_Golem>().SetDamage(Player.instance.isRightNear ? Player.instance.damageRightNear : Player.instance.damageRight, hits[i].point);
                        if (collision.GetComponent<Boss_Stone>()) collision.GetComponent<Boss_Stone>().SetDamage(Player.instance.isRightNear ? Player.instance.damageRightNear : Player.instance.damageRight, hits[i].point, false);
                    }
                    else
                    {
                        if (collision.GetComponent<MOB.BossMonster_Six_Apear>()) collision.GetComponent<MOB.BossMonster_Six_Apear>().SetDamage(Player.instance.damage);
                        if (collision.GetComponent<Boss_STG>()) collision.GetComponent<Boss_STG>().SetDamage(Player.instance.isLeftNear ? Player.instance.damageNear : Player.instance.damage, hits[i].point);
                        if (collision.GetComponent<Boss_NPC>()) collision.GetComponent<Boss_NPC>().SetDamage(Player.instance.isLeftNear ? Player.instance.damageNear : Player.instance.damage, hits[i].point);
                        if (collision.GetComponent<Boss_Golem>()) collision.GetComponent<Boss_Golem>().SetDamage(Player.instance.isLeftNear ? Player.instance.damageNear : Player.instance.damage, hits[i].point);
                        if (collision.GetComponent<Boss_Stone>()) collision.GetComponent<Boss_Stone>().SetDamage(Player.instance.isLeftNear ? Player.instance.damageNear : Player.instance.damage, hits[i].point, false);
                    }
                    if (PDG.Player.instance.isStrategic_02) PDG.Player.instance.isStrategic_02 = false;
                    if (PDG.Player.instance.isLeftNear && PDG.Player.instance.isCritical)
                    {
                        GameObject criEffect = ObjManager.Call().GetObject("CriticalHitEffect");
                        criEffect.transform.position = hits[i].point;
                        criEffect.SetActive(true);
                    }
                }
                if (collision.gameObject.layer.Equals(9) && isMonster)
                {

                    if (bulletType.Equals("fanatic_dagger"))
                    {
                        GameObject obj2 = ObjManager.Call().GetObject("Hit_sprite7");
                        obj2.transform.position = transform.position;
                        obj2.transform.localScale = Vector3.one;
                        obj2.GetComponent<SwordHitEffectController>()._effctName = "fanatic_dagger_hit_" + Random.Range(0, 2);
                        obj2.GetComponent<SwordHitEffectController>().animSpeed = 1.0f;
                        obj2.GetComponent<Animator>().Rebind();
                        obj2.SetActive(true);
                    }
                    else if (bulletType.Equals("fanatic_dagger_1"))
                    {
                        GameObject obj222 = Instantiate(fanatic_dagger_hit_effect_1);
                        obj222.transform.position = transform.position;
                        Destroy(obj222, 2f);
                    }
                    else if (bulletType.Contains("harpoon_tentacles"))
                    {
                        GameObject obj2 = Instantiate(harpoon_tentaclesObj);
                        obj2.transform.position = transform.position;
                        obj2.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
                        Destroy(obj2, 1.5f);
                    }
                    else
                    {
                        obj = ObjManager.Call().GetObject("Bullet_Hit");
                        obj.GetComponent<SwordHitEffectController>()._isMonster = isMonster;
                    }


                    if (monsterName.Equals("octopus_king") || monsterName.Equals("boss_stg") || monsterName.Equals("boss_npc") || monsterName.Equals("boss_golem") || monsterName.Equals("boss_stone"))
                    {
                        if (collision.gameObject.GetComponent<PDG.Player>())
                            collision.gameObject.GetComponent<PDG.Player>().SetDamage(1, LocalizeManager.GetLocalize("monster_" + monsterName), DEATH_TYPE.DEATH_BOSS, monsterName);
                    }
                    else if (monsterName.Equals("piko") || monsterName.Equals("slime_bright") || monsterName.Equals("golem_ice"))
                    {
                        if (collision.gameObject.GetComponent<PDG.Player>())
                            collision.gameObject.GetComponent<PDG.Player>().SetDamage(1, LocalizeManager.GetLocalize("monster_" + monsterName), DEATH_TYPE.DEATH_SUB_BOSS, monsterName);
                    }
                    else
                    {
                        if (collision.gameObject.GetComponent<PDG.Player>())
                            collision.gameObject.GetComponent<PDG.Player>().SetDamage(1, LocalizeManager.GetLocalize("monster_" + monsterName), DEATH_TYPE.DEATH_NORMAL, monsterName);
                    }
                }
                if (collision.gameObject.layer.Equals(8))
                {
                    if (collision.transform.parent && collision.transform.parent.GetComponent<BossRoomObject>())
                    {
                        collision.transform.parent.GetComponent<BossRoomObject>().SetDamage();
                    }
                    if (PDG.Player.instance.isMiniGame) Inventorys.Instance.miniGameBulletUnCount++;

                    if (bulletType.Equals("fanatic_dagger"))
                    {
                        GameObject obj2 = ObjManager.Call().GetObject("Hit_sprite7");
                        obj2.transform.position = transform.position;
                        obj2.transform.localScale = Vector3.one;
                        obj2.GetComponent<SwordHitEffectController>()._effctName = "fanatic_dagger_hit_" + Random.Range(0, 2);
                        obj2.GetComponent<SwordHitEffectController>().animSpeed = 1.0f;
                        obj2.GetComponent<Animator>().Rebind();
                        obj2.SetActive(true);
                    }
                    else if (bulletType.Contains("harpoon_tentacles"))
                    {
                        GameObject obj2 = Instantiate(harpoon_tentaclesObj);
                        obj2.transform.position = transform.position;
                        obj2.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
                        Destroy(obj2, 1.5f);
                    }
                    else if (bulletType.Contains("elastic_cell_weapon"))
                    {
                        GameObject obj2 = Instantiate(destroyObj);
                        obj2.transform.position = transform.position;
                        obj2.transform.localScale = Vector3.one;
                        Destroy(obj2, 1.5f);
                    }
                    else if (bulletType.Contains("living_arrow"))
                    {
                        GameObject obj2 = Instantiate(livingDestroyObj);
                        obj2.transform.position = transform.position;
                        obj2.transform.localScale = new Vector3(0.3f, 0.3f, 0.3f);
                        Destroy(obj2, 1.5f);
                    }
                    else if (bulletType.Contains("secret_lasergun"))
                    {
                        GameObject obj2 = Instantiate(secret_lasergunDestroyObj);
                        obj2.transform.position = transform.position;
                        obj2.transform.localScale = new Vector3(0.3f, 0.3f, 0.3f);
                        Destroy(obj2, 1.5f);
                    }
                    else if (bulletType.Contains("secret_watergun"))
                    {
                        GameObject obj2 = Instantiate(secret_watergunDestroyObj);
                        obj2.transform.position = transform.position;
                        obj2.transform.localScale = new Vector3(1, 1, 1);
                        Destroy(obj2, 1.5f);
                    }
                    else
                    {
                        obj = ObjManager.Call().GetObject("Bullet_Hit");
                        obj.GetComponent<SwordHitEffectController>()._isMonster = isMonster;
                    }
                }
                if (obj != null)
                {
                    obj.transform.position = transform.position;
                    obj.GetComponent<SwordHitEffectController>()._effctName = (bulletType.Equals("penetrate_bullet") ? bulletType + "_" + Random.Range(0, 3) : bulletType);
                    obj.GetComponent<Animator>().Rebind();
                    obj.transform.localEulerAngles = new Vector3(0, 0, GetDirectionAngle(collision));
                    obj.SetActive(true);
                }
            }
            if (!isPenetrate && (!Player.instance.isBulletBounce || isMonster))
            {
                gameObject.SetActive(false);
            }
            else if (Player.instance.isBulletBounce && !isPenetrate && !bulletType.Contains("living_arrow") && !bulletType.Contains("secret_lasergun"))
            {
                if (bounsCount >= PDG.Player.instance.BounsCount) gameObject.SetActive(false);
                bounsDelayTime = 0.15f;
                Vector3 dir = hits[0].collider.transform.position - transform.position;
                dir.Normalize();
                float angle = GetReflectAngle(hits[0].point, hits[0].collider.transform.position, dir);
                rigid.velocity = Vector2.zero;
                dir = GetPosition(transform.position, angle) - transform.position;
                dir.Normalize();
                rigid.AddForce(dir * fSpeed, ForceMode2D.Impulse);
                transform.localEulerAngles = new Vector3(0, 0, angle);
                bounsCount++;
            }
            else if (Player.instance.isLaserBounce && !isPenetrate && bulletType.Contains("secret_lasergun"))
            {
                if (bounsCount >= PDG.Player.instance.BounsLaserCount) gameObject.SetActive(false);
                bounsDelayTime = 0.15f;
                Vector3 dir = hits[0].collider.transform.position - transform.position;
                dir.Normalize();
                float angle = GetReflectAngle(hits[0].point, hits[0].collider.transform.position, dir);
                rigid.velocity = Vector2.zero;
                dir = GetPosition(transform.position, angle) - transform.position;
                dir.Normalize();
                rigid.AddForce(dir * fSpeed, ForceMode2D.Impulse);
                transform.localEulerAngles = new Vector3(0, 0, angle);
                bounsCount++;
            }
        }
    }
    #region -- Tool --
    int GetDirectionAngle(Collider2D collision)
    {
        float angle = GetAngle(collision.transform.position, transform.position);
        while (angle > 360.0f) angle -= 360.0f;
        while (angle < 0) angle += 360.0f;

        if (angle >= 45.0f && angle < 135.0f)
        {
            // 위쪽
            return 270;
        }
        else if (angle >= 135.0f && angle < 225.0f)
        {
            // 왼쪽
            return 0;
        }
        else if (angle > 225.0f && angle < 315.0f)
        {
            //아래쪽
            return 90;
        }
        else
        {
            //오른쪽
            return 180;
        }
    }
    float GetAngle(Vector3 vStart, Vector3 vEnd)
    {
        Vector3 v = vEnd - vStart;

        return Mathf.Atan2(v.y, v.x) * Mathf.Rad2Deg;
    }
    Vector3 GetPosition(Vector3 vStart, float angle)
    {
        float x = Mathf.Cos(Mathf.Deg2Rad * angle) + vStart.x;
        float y = Mathf.Sin(Mathf.Deg2Rad * angle) + vStart.y;

        return new Vector3(x, y, 0);
    }
    float GetReflectAngle(Vector3 _hitPoint, Vector3 _hitPosition, Vector3 _dir)
    {
        Vector3 _dir2 = _hitPoint - _hitPosition;
        _dir2.Normalize();

        if (_dir.x == 0 && _dir.y > 0)       // 위 
        {
            return 270.0f;
        }
        else if (_dir.x == 0 && _dir.y < 0)  // 아래
        {
            return 90.0f;
        }
        else if (_dir.x > 0 && _dir.y == 0)  // 오른쪽
        {
            return 180.0f;
        }
        else if (_dir.x < 0 && _dir.y == 0)  // 왼쪽
        {
            return 0.0f;
        }
        else if (_dir.x > 0 && _dir.y > 0)  // 오른쪽 위 
        {
            if (_dir2.x > 0)
            {
                float zz = GetAngle(_hitPoint, transform.position);
                float f = zz - 180.0f; // 입사각
                return 360.0f - f; // 반사각
            }
            else if (_dir2.y > 0)
            {
                float zz = GetAngle(_hitPoint, transform.position);
                float f = 270.0f - zz; // 입사각
                return 90.0f + f; // 반사각
            }
            else
            {
                float hitAngle = GetAngle(_hitPosition, _hitPoint);
                if (hitAngle > 225.0f)
                {
                    float zz = GetAngle(_hitPoint, transform.position);
                    float f = zz - 180.0f; // 입사각
                    return 360.0f - f; // 반사각
                }
                else if (hitAngle < 225.0f)
                {
                    float zz = GetAngle(_hitPoint, transform.position);
                    float f = 270.0f - zz; // 입사각
                    return 90.0f + f; // 반사각
                }
                else
                {
                    return 225.0f;
                }

            }
        }
        else if (_dir.x < 0 && _dir.y > 0)   // 왼쪽 위
        {
            if (_dir2.x < 0)
            {
                float zz = GetAngle(_hitPoint, transform.position);
                float f = 360.0f - zz; // 입사각
                return 180.0f + f; // 반사각
            }
            else if (_dir2.y > 0)
            {
                float zz = GetAngle(_hitPoint, transform.position);
                float f = zz - 270.0f; // 입사각
                return f; // 반사각
            }
            else
            {
                float hitAngle = GetAngle(_hitPosition, _hitPoint);
                if (hitAngle > 315.0f)
                {
                    float zz = GetAngle(_hitPoint, transform.position);
                    float f = zz - 270.0f; // 입사각
                    return f; // 반사각
                }
                else if (hitAngle < 315.0f)
                {
                    float zz = GetAngle(_hitPoint, transform.position);
                    float f = 360.0f - zz; // 입사각
                    return 180.0f + f; // 반사각
                }
                else
                {
                    return 315.0f;
                }

            }
        }
        else if (_dir.x < 0 && _dir.y < 0)   // 왼쪽 아래
        {
            if (_dir2.x < 0)
            {
                float zz = GetAngle(_hitPoint, transform.position);
                float f = zz; // 입사각
                return 180.0f - f; // 반사각
            }
            else if (_dir2.y < 0)
            {
                float zz = GetAngle(_hitPoint, transform.position);
                float f = 90.0f - zz; // 입사각
                return 270.0f + f; // 반사각
            }
            else
            {
                float hitAngle = GetAngle(_hitPosition, _hitPoint);
                if (hitAngle > 45.0f)
                {
                    float zz = GetAngle(_hitPoint, transform.position);
                    float f = zz; // 입사각
                    return 180.0f - f; // 반사각
                }
                else if (hitAngle < 45.0f)
                {
                    float zz = GetAngle(_hitPoint, transform.position);
                    float f = 90.0f - zz; // 입사각
                    return 270.0f + f; // 반사각
                }
                else
                {
                    return 45.0f;
                }

            }
        }
        else if (_dir.x > 0 && _dir.y < 0)   // 오른쪽 아래
        {
            if (_dir2.x > 0)
            {
                float zz = GetAngle(_hitPoint, transform.position);
                float f = 180.0f - zz; // 입사각
                return f; // 반사각
            }
            else if (_dir2.y < 0)
            {
                float zz = GetAngle(_hitPoint, transform.position);
                float f = zz - 90.0f; // 입사각
                return 270.0f - f; // 반사각
            }
            else
            {
                float hitAngle = GetAngle(_hitPosition, _hitPoint);
                if (hitAngle > 135.0f)
                {
                    float zz = GetAngle(_hitPoint, transform.position);
                    float f = zz - 90.0f; // 입사각
                    return 270.0f - f; // 반사각
                }
                else if (hitAngle < 135.0f)
                {
                    float zz = GetAngle(_hitPoint, transform.position);
                    float f = 180.0f - zz; // 입사각
                    return f; // 반사각
                }
                else
                {
                    return 135.0f;
                }
            }
        }
        else
        {
            return 0.0f;
        }
    }
    #endregion

}
