﻿using MoreMountains.Feedbacks;
using PDG;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class SwordHitEffectController : MonoBehaviour
{

    [SerializeField] private Animator animator;
    [SerializeField] private SpriteRenderer sr;
    [SerializeField] private string _spritePath;
    private Sprite[] _sprites;
    public string _effctName;
    public bool isAttack = false;
    public float animSpeed = 1.0f;
    public bool isPenetrate = false;
    public float nRange;
    public bool isPlayerTarget = false;
    public float attackRadius = 0;
    public Vector3 sPos = Vector3.zero;

    public bool _isMonster = false;
    AudioSource _audio;
    AudioClip _clip;

    [SerializeField] private ParticleSystem particle;
    [SerializeField] private Shader matShader;
    [SerializeField] private Sprite sprite;
    public float hitTime = 0.25f;
    [SerializeField] private Dictionary<GameObject, float> objDic = new Dictionary<GameObject, float>();

    [SerializeField] private GameObject penetrateBulletObj;
    [SerializeField] private GameObject penetrateBulletObj2;
    [SerializeField] private GameObject hitEffect;
    private void Awake()
    {
        sr = GetComponent<SpriteRenderer>();
        animator = GetComponent<Animator>();
        _audio = GetComponent<AudioSource>();
    }
    private void Start()
    {
        Load();
        if(_effctName.Equals("pistol_bullet") && !_isMonster)
        {
            if (transform.Find("BulletExplosionGreen"))
            {
                transform.Find("BulletExplosionGreen").gameObject.SetActive(true);
                sr.enabled = false;
            }
        }
        else
        {
            if (transform.Find("BulletExplosionGreen")) transform.Find("BulletExplosionGreen").gameObject.SetActive(false);
            sr.enabled = true;
        }
        objDic.Clear();
    }
    void Load()
    {
        var spritePath = "Effect/Sword/Hit/" + _effctName;
        if (!spritePath.Equals(_spritePath))
        {
            _spritePath = spritePath;
            _sprites = Resources.LoadAll<Sprite>(_spritePath);
        }
    }
    void LateUpdate()
    {
        if (animator == null) return;
        if (sr == null || sr.sprite == null)
            return;
        if (_sprites == null) return;
        if (_sprites.Length == 0) return;
        Load();
        var name = sr.sprite.name;
        var sprite = System.Array.Find(_sprites, item => item.name == name);
        if (sprite) sr.sprite = sprite;
    }
    private void OnEnable()
    {
        sr.enabled = true;
        if (animSpeed != 1.0f)
            animator.SetFloat("animSpeed", animSpeed);
        else
            animator.SetFloat("animSpeed", 1.0f);

        if (_effctName.Equals("pistol_bullet") && !_isMonster)
        {
            if (transform.Find("BulletExplosionGreen"))
            {
                transform.Find("BulletExplosionGreen").gameObject.SetActive(true);
                sr.enabled = false;
            }
        }
        else
        {
            if (transform.Find("BulletExplosionGreen")) transform.Find("BulletExplosionGreen").gameObject.SetActive(false);
            sr.enabled = true;
        }

        if (!isPenetrate) nRange = 0;
        isPlayerTarget = false;
        if(Player.instance != null && Player.instance.nowWeaponName.Contains("fanatic_dagger"))
        {
            _clip = Resources.Load<AudioClip>("Sound/Weapon/" + Player.instance.nowWeaponName + "_hit");
            if (_clip != null && _audio != null)
            {
                _audio.volume =  (SoundManager.isMute || (PDG.Dungeon.instance == null ? false : PDG.Dungeon.instance.isPause) || (PDG.Player.instance == null ? false : PDG.Player.instance.isDie)) ? 0 : SoundManager.EffectVolume;
                _audio.PlayOneShot(_clip);
            }
        }
        StartCoroutine(Hoxy());
        if (penetrateBulletObj)
        {
            if (_effctName.Contains("penetrate_bullet")) penetrateBulletObj.SetActive(true);
            else penetrateBulletObj.SetActive(false);
        }
        if (penetrateBulletObj2 && Player.instance != null)
        {
            if (Player.instance.nowWeaponName.Contains("balkan"))
            {
                sr.enabled = false;
                penetrateBulletObj2.SetActive(true);
            }
            else penetrateBulletObj2.SetActive(false);
        }
        if (Player.instance != null && Player.instance.nowWeaponName.Contains("giant_scythe") && !isAttack && !_effctName.Contains("rushing_ice"))
        {
            sr.enabled = false;
            if (transform.Find("SwordImpactRed")) transform.Find("SwordImpactRed").gameObject.SetActive(true);
        }
        else
        {
            if (transform.Find("SwordImpactRed")) transform.Find("SwordImpactRed").gameObject.SetActive(false);
        }
        objDic.Clear();
    }
    IEnumerator Hoxy()
    {
        yield return new WaitForSeconds(1.0f);
        if (!isPenetrate)
        {
            if (name.Contains("Bullet_Hit"))
                StartCoroutine(TimeOut());
            else
                gameObject.SetActive(false);
        }
        else
        {
            while (true)
            {
                float dis = Vector3.Distance(PDG.Player.instance.transform.position, transform.position);
                if (dis > nRange)
                {
                    if (name.Contains("Bullet_Hit"))
                        StartCoroutine(TimeOut());
                    else
                        gameObject.SetActive(false);
                    break;
                }
                yield return null;
            }
        }
    }
    public void AnimationEnd()
    {
        if (!isPenetrate)
        {
            StopAllCoroutines();
            if (name.Contains("Bullet_Hit"))
                StartCoroutine(TimeOut());
            else
                gameObject.SetActive(false);
        }
    }
    private void Update()
    {
        try
        {
            foreach (KeyValuePair<GameObject, float> items in objDic)
            {
                float t = items.Value;
                t += Time.deltaTime;
                objDic[items.Key] = t;
            }
        }
        catch (System.Exception e)
        {

        }

        if (isPenetrate)
        {
            float dis = Vector3.Distance(sPos, transform.position);
            if (dis > nRange)
            {
                if (name.Contains("Bullet_Hit"))
                    StartCoroutine(TimeOut());
                else
                    gameObject.SetActive(false);
            }
        }
        if(isPlayerTarget)
        {
            transform.position = PDG.Player.instance.transform.position;
        }
        if(isAttack)
        {
            RaycastHit2D[] hits = Physics2D.CircleCastAll(transform.position, attackRadius, Vector2.zero, 0, 1 << 12 | 1 << 23 | 1 << 24);
            if (hits.Length > 0)
            {
                for (int i = 0; i < hits.Length; i++)
                {
                    GameObject obj = hits[i].collider.gameObject;
                    if (objDic.ContainsKey(obj)) 
                    {
                        if (hitTime > objDic[obj]) continue;
                        else objDic[obj] = 0.0f;
                    }
                    else
                    {
                        objDic.Add(obj, 0.0f);
                    }

                    if (obj.layer.Equals(12) && isPenetrate)
                    {
                        if (!obj.GetComponent<MOB.Monster>().isHit && !obj.GetComponent<MOB.Monster>().isInvincibility)
                        {
                            Vector3 dir2 = obj.transform.position - PDG.Player.instance.transform.position;
                            dir2.Normalize();
                            if (_effctName.Contains("giant_blade_"))
                                obj.GetComponent<MOB.Monster>().SetDamage(Player.instance.damage, -dir2, 2, Player.instance.left_elemetanl, Player.instance.left_elemnetalTime);
                            else
                                obj.GetComponent<MOB.Monster>().SetDamage((Player.instance.isLeftNear ? Player.instance.damageNear : Player.instance.damage), -dir2, 2, Player.instance.left_elemetanl, Player.instance.left_elemnetalTime);
                            if (Player.instance.isStrategic_02) Player.instance.isStrategic_02 = false;
                            if (hitEffect && _effctName.Contains("giant_blade_"))
                            {
                                GameObject obj2 = Instantiate(hitEffect);
                                obj2.transform.position = hits[i].point;
                                Destroy(obj2, 2f);
                            }
                            else
                            {
                                GameObject obj2 = ObjManager.Call().GetObject("Hit_sprite7");
                                obj2.transform.position = obj.transform.position;
                                obj2.transform.localScale = Vector3.one;
                                obj2.transform.localEulerAngles = Vector2.zero;
                                obj2.GetComponent<SwordHitEffectController>()._effctName = "giant_blade_hit_" + Random.Range(0, 2);
                                obj2.GetComponent<SwordHitEffectController>().animSpeed = 1.0f;
                                obj2.GetComponent<Animator>().Rebind();
                                obj2.SetActive(true);
                            }
                            if (PDG.Player.instance.isLeftNear && PDG.Player.instance.isCritical && !_effctName.Contains("giant_blade_"))
                            {
                                GameObject criEffect = ObjManager.Call().GetObject("CriticalHitEffect");
                                criEffect.transform.position = hits[i].point;
                                criEffect.SetActive(true);
                            }
                        }
                    }
                    else if (obj.layer.Equals(12))
                    {
                        if (!obj.GetComponent<MOB.Monster>()) return;
                        if (!obj.GetComponent<MOB.Monster>().isHit && !obj.GetComponent<MOB.Monster>().isInvincibility)
                        {
                            Vector3 dir2 = obj.transform.position - Player.instance.transform.position;
                            dir2.Normalize();
                            obj.GetComponent<MOB.Monster>().SetDamage((Player.instance.isLeftNear ? Player.instance.damageNear : Player.instance.damage), -dir2, 2, Player.instance.left_elemetanl, Player.instance.left_elemnetalTime);
                            if (Player.instance.isStrategic_02) Player.instance.isStrategic_02 = false;
                            GameObject obj2 = ObjManager.Call().GetObject("Sword_Hit");
                            obj2.transform.position = obj.transform.position;
                            obj2.GetComponent<Animator>().Rebind();
                            obj2.SetActive(true);

                            GameObject obj3 = ObjManager.Call().GetObject("Hit_sprite4");
                            obj3.transform.position = obj.transform.position;
                            obj3.transform.localScale = Vector3.one;
                            obj3.transform.localEulerAngles = Vector2.zero;
                            obj3.GetComponent<SwordHitEffectController>()._effctName = "Hit_" + Random.Range(3, 6);
                            obj3.GetComponent<SwordHitEffectController>().animSpeed = 1.0f;
                            obj3.GetComponent<SwordHitEffectController>().isPenetrate = false;
                            obj3.GetComponent<SwordHitEffectController>().isAttack = false;
                            obj3.GetComponent<Animator>().Rebind();
                            obj3.SetActive(true);
                        }
                        if (PDG.Player.instance.isLeftNear && PDG.Player.instance.isCritical)
                        {
                            GameObject criEffect = ObjManager.Call().GetObject("CriticalHitEffect");
                            criEffect.transform.position = hits[i].point;
                            criEffect.SetActive(true);
                        }
                    }
                    else if (obj.layer.Equals(23) && isPenetrate)
                    {
                        if (obj.transform.parent && obj.transform.parent.GetComponent<TentacleController>() && !obj.transform.parent.GetComponent<TentacleController>().isHit) obj.transform.parent.GetComponent<TentacleController>().SetDamage(1);
                        if (obj.GetComponent<ChestHit>() && !obj.GetComponent<ChestHit>().isHit)
                        {
                            obj.GetComponent<ChestHit>().SetDamage(1);
                            GameObject obj3 = ObjManager.Call().GetObject("Sword_Hit");
                            obj3.transform.position = obj.transform.position;
                            obj3.GetComponent<Animator>().Rebind();
                            obj3.SetActive(true);

                            GameObject obj2 = ObjManager.Call().GetObject("Hit_sprite4");
                            obj2.transform.position = obj.transform.position;
                            obj2.transform.localScale = Vector3.one;
                            obj2.transform.localEulerAngles = Vector2.zero;
                            obj2.GetComponent<SwordHitEffectController>()._effctName = "Hit_" + Random.Range(3, 6);
                            obj2.GetComponent<SwordHitEffectController>().animSpeed = 1.0f;
                            obj2.GetComponent<SwordHitEffectController>().isPenetrate = false;
                            obj2.GetComponent<SwordHitEffectController>().isAttack = false;
                            obj2.GetComponent<Animator>().Rebind();
                            obj2.SetActive(true);
                        }
                        else if(obj.GetComponent<DrumHit>() && !obj.GetComponent<DrumHit>().isHit)
                        {
                            obj.GetComponent<DrumHit>().SetDamage(1);
                            GameObject obj3 = ObjManager.Call().GetObject("Sword_Hit");
                            obj3.transform.position = obj.transform.position;
                            obj3.GetComponent<Animator>().Rebind();
                            obj3.SetActive(true);

                            GameObject obj2 = ObjManager.Call().GetObject("Hit_sprite4");
                            obj2.transform.position = obj.transform.position;
                            obj2.transform.localScale = Vector3.one;
                            obj2.transform.localEulerAngles = Vector2.zero;
                            obj2.GetComponent<SwordHitEffectController>()._effctName = "Hit_" + Random.Range(3, 6);
                            obj2.GetComponent<SwordHitEffectController>().animSpeed = 1.0f;
                            obj2.GetComponent<SwordHitEffectController>().isPenetrate = false;
                            obj2.GetComponent<SwordHitEffectController>().isAttack = false;
                            obj2.GetComponent<Animator>().Rebind();
                            obj2.SetActive(true);
                        }
                    }
                    else if (obj.layer.Equals(23))
                    {
                        if (obj.transform.parent && obj.transform.parent.GetComponent<TentacleController>() && !obj.transform.parent.GetComponent<TentacleController>().isHit) obj.transform.parent.GetComponent<TentacleController>().SetDamage(1);
                        if (obj.GetComponent<ChestHit>() && !obj.GetComponent<ChestHit>().isHit && !obj.GetComponent<ChestHit>().isOpenBefore)
                        {
                            obj.GetComponent<ChestHit>().SetDamage(1);
                            GameObject obj3 = ObjManager.Call().GetObject("Sword_Hit");
                            obj3.transform.position = obj.transform.position;
                            obj3.GetComponent<Animator>().Rebind();
                            obj3.SetActive(true);

                            GameObject obj2 = ObjManager.Call().GetObject("Hit_sprite4");
                            obj2.transform.position = obj.transform.position;
                            obj2.transform.localScale = Vector3.one;
                            obj2.transform.localEulerAngles = Vector2.zero;
                            obj2.GetComponent<SwordHitEffectController>()._effctName = "Hit_" + Random.Range(3, 6);
                            obj2.GetComponent<SwordHitEffectController>().animSpeed = 1.0f;
                            obj2.GetComponent<SwordHitEffectController>().isPenetrate = false;
                            obj2.GetComponent<SwordHitEffectController>().isAttack = false;
                            obj2.GetComponent<Animator>().Rebind();
                            obj2.SetActive(true);
                        }
                        else if (obj.GetComponent<DrumHit>() && !obj.GetComponent<DrumHit>().isHit)
                        {
                            obj.GetComponent<DrumHit>().SetDamage(1);
                            GameObject obj3 = ObjManager.Call().GetObject("Sword_Hit");
                            obj3.transform.position = obj.transform.position;
                            obj3.GetComponent<Animator>().Rebind();
                            obj3.SetActive(true);

                            GameObject obj2 = ObjManager.Call().GetObject("Hit_sprite4");
                            obj2.transform.position = obj.transform.position;
                            obj2.transform.localScale = Vector3.one;
                            obj2.transform.localEulerAngles = Vector2.zero;
                            obj2.GetComponent<SwordHitEffectController>()._effctName = "Hit_" + Random.Range(3, 6);
                            obj2.GetComponent<SwordHitEffectController>().animSpeed = 1.0f;
                            obj2.GetComponent<SwordHitEffectController>().isPenetrate = false;
                            obj2.GetComponent<SwordHitEffectController>().isAttack = false;
                            obj2.GetComponent<Animator>().Rebind();
                            obj2.SetActive(true);
                        }
                    }
                    else if (obj.layer.Equals(24) && isPenetrate)
                    {
                        if (obj.GetComponent<MOB.BossMonster_Six_Apear>() && !obj.GetComponent<MOB.BossMonster_Six_Apear>().isHit)
                        {
                            obj.GetComponent<MOB.BossMonster_Six_Apear>().SetDamage(PDG.Player.instance.damage);
                            if (Player.instance.isStrategic_02) Player.instance.isStrategic_02 = false;

                            if (hitEffect && _effctName.Contains("giant_blade_"))
                            {
                                GameObject obj2 = Instantiate(hitEffect);
                                obj2.transform.position = hits[i].point;
                                Destroy(obj2, 2f);
                            }
                            else
                            {
                                GameObject obj2 = ObjManager.Call().GetObject("Hit_sprite7");
                                obj2.transform.position = obj.transform.position;
                                obj2.transform.localScale = Vector3.one;
                                obj2.transform.localEulerAngles = Vector2.zero;
                                obj2.GetComponent<SwordHitEffectController>()._effctName = "giant_blade_hit_" + Random.Range(0, 2);
                                obj2.GetComponent<SwordHitEffectController>().animSpeed = 1.0f;
                                obj2.GetComponent<Animator>().Rebind();
                                obj2.SetActive(true);
                            }
                            if (PDG.Player.instance.isLeftNear && PDG.Player.instance.isCritical)
                            {
                                GameObject criEffect = ObjManager.Call().GetObject("CriticalHitEffect");
                                criEffect.transform.position = hits[i].point;
                                criEffect.SetActive(true);
                            }
                        }
                        if (obj.GetComponent<Boss_STG>() && !obj.GetComponent<Boss_STG>().isHit)
                        {
                            if (_effctName.Contains("giant_blade_"))
                                obj.GetComponent<Boss_STG>().SetDamage(Player.instance.damage, hits[i].point);
                            else
                                obj.GetComponent<Boss_STG>().SetDamage(Player.instance.isLeftNear ? Player.instance.damageNear : Player.instance.damage, hits[i].point);
                            if (Player.instance.isStrategic_02) Player.instance.isStrategic_02 = false;

                            if (hitEffect && _effctName.Contains("giant_blade_"))
                            {
                                GameObject obj2 = Instantiate(hitEffect);
                                obj2.transform.position = hits[i].point;
                                Destroy(obj2, 2f);
                            }
                            else
                            {
                                GameObject obj2 = ObjManager.Call().GetObject("Hit_sprite7");
                                obj2.transform.position = obj.transform.position;
                                obj2.transform.localScale = Vector3.one;
                                obj2.transform.localEulerAngles = Vector2.zero;
                                obj2.GetComponent<SwordHitEffectController>()._effctName = "giant_blade_hit_" + Random.Range(0, 2);
                                obj2.GetComponent<SwordHitEffectController>().animSpeed = 1.0f;
                                obj2.GetComponent<Animator>().Rebind();
                                obj2.SetActive(true);
                            }
                            if (PDG.Player.instance.isLeftNear && PDG.Player.instance.isCritical && !_effctName.Contains("giant_blade_"))
                            {
                                GameObject criEffect = ObjManager.Call().GetObject("CriticalHitEffect");
                                criEffect.transform.position = hits[i].point;
                                criEffect.SetActive(true);
                            }
                        }
                        if (obj.GetComponent<Boss_NPC>() && !obj.GetComponent<Boss_NPC>().isHit)
                        {
                            if (_effctName.Contains("giant_blade_"))
                                obj.GetComponent<Boss_NPC>().SetDamage(Player.instance.damage, hits[i].point);
                            else
                                obj.GetComponent<Boss_NPC>().SetDamage(Player.instance.isLeftNear ? Player.instance.damageNear : Player.instance.damage, hits[i].point);
                            if (Player.instance.isStrategic_02) Player.instance.isStrategic_02 = false;

                            if (hitEffect && _effctName.Contains("giant_blade_"))
                            {
                                GameObject obj2 = Instantiate(hitEffect);
                                obj2.transform.position = hits[i].point;
                                Destroy(obj2, 2f);
                            }
                            else
                            {
                                GameObject obj2 = ObjManager.Call().GetObject("Hit_sprite7");
                                obj2.transform.position = obj.transform.position;
                                obj2.transform.localScale = Vector3.one;
                                obj2.transform.localEulerAngles = Vector2.zero;
                                obj2.GetComponent<SwordHitEffectController>()._effctName = "giant_blade_hit_" + Random.Range(0, 2);
                                obj2.GetComponent<SwordHitEffectController>().animSpeed = 1.0f;
                                obj2.GetComponent<Animator>().Rebind();
                                obj2.SetActive(true);
                            }
                            if (PDG.Player.instance.isLeftNear && PDG.Player.instance.isCritical && !_effctName.Contains("giant_blade_"))
                            {
                                GameObject criEffect = ObjManager.Call().GetObject("CriticalHitEffect");
                                criEffect.transform.position = hits[i].point;
                                criEffect.SetActive(true);
                            }
                        }
                        if (obj.GetComponent<Boss_Golem>() && !obj.GetComponent<Boss_Golem>().isHit)
                        {
                            if (_effctName.Contains("giant_blade_"))
                                obj.GetComponent<Boss_Golem>().SetDamage(Player.instance.damage, hits[i].point);
                            else
                                obj.GetComponent<Boss_Golem>().SetDamage(Player.instance.isLeftNear ? Player.instance.damageNear : Player.instance.damage, hits[i].point);
                            if (Player.instance.isStrategic_02) Player.instance.isStrategic_02 = false;

                            if (hitEffect && _effctName.Contains("giant_blade_"))
                            {
                                GameObject obj2 = Instantiate(hitEffect);
                                obj2.transform.position = hits[i].point;
                                Destroy(obj2, 2f);
                            }
                            else
                            {
                                GameObject obj2 = ObjManager.Call().GetObject("Hit_sprite7");
                                obj2.transform.position = obj.transform.position;
                                obj2.transform.localScale = Vector3.one;
                                obj2.transform.localEulerAngles = Vector2.zero;
                                obj2.GetComponent<SwordHitEffectController>()._effctName = "giant_blade_hit_" + Random.Range(0, 2);
                                obj2.GetComponent<SwordHitEffectController>().animSpeed = 1.0f;
                                obj2.GetComponent<Animator>().Rebind();
                                obj2.SetActive(true);
                            }
                            if (PDG.Player.instance.isLeftNear && PDG.Player.instance.isCritical && !_effctName.Contains("giant_blade_"))
                            {
                                GameObject criEffect = ObjManager.Call().GetObject("CriticalHitEffect");
                                criEffect.transform.position = hits[i].point;
                                criEffect.SetActive(true);
                            }
                        }
                        if (obj.GetComponent<Boss_Stone>() && !obj.GetComponent<Boss_Stone>().isHit)
                        {
                            if (_effctName.Contains("giant_blade_"))
                                obj.GetComponent<Boss_Stone>().SetDamage(Player.instance.damage, hits[i].point, false);
                            else
                                obj.GetComponent<Boss_Stone>().SetDamage(Player.instance.isLeftNear ? Player.instance.damageNear : Player.instance.damage, hits[i].point, false);
                            if (Player.instance.isStrategic_02) Player.instance.isStrategic_02 = false;

                            if (hitEffect && _effctName.Contains("giant_blade_"))
                            {
                                GameObject obj2 = Instantiate(hitEffect);
                                obj2.transform.position = hits[i].point;
                                Destroy(obj2, 2f);
                            }
                            else
                            {
                                GameObject obj2 = ObjManager.Call().GetObject("Hit_sprite7");
                                obj2.transform.position = obj.transform.position;
                                obj2.transform.localScale = Vector3.one;
                                obj2.transform.localEulerAngles = Vector2.zero;
                                obj2.GetComponent<SwordHitEffectController>()._effctName = "giant_blade_hit_" + Random.Range(0, 2);
                                obj2.GetComponent<SwordHitEffectController>().animSpeed = 1.0f;
                                obj2.GetComponent<Animator>().Rebind();
                                obj2.SetActive(true);
                            }
                            if (PDG.Player.instance.isLeftNear && PDG.Player.instance.isCritical && !_effctName.Contains("giant_blade_"))
                            {
                                GameObject criEffect = ObjManager.Call().GetObject("CriticalHitEffect");
                                criEffect.transform.position = hits[i].point;
                                criEffect.SetActive(true);
                            }
                        }
                    }
                    else if (obj.layer.Equals(24))
                    {
                        if (obj.GetComponent<MOB.BossMonster_Six_Apear>()) obj.GetComponent<MOB.BossMonster_Six_Apear>().SetDamage(PDG.Player.instance.damage);
                        if (obj.GetComponent<Boss_STG>()) obj.GetComponent<Boss_STG>().SetDamage(Player.instance.isLeftNear ? Player.instance.damageNear : Player.instance.damage, hits[i].point);
                        if (obj.GetComponent<Boss_NPC>()) obj.GetComponent<Boss_NPC>().SetDamage(Player.instance.isLeftNear ? Player.instance.damageNear : Player.instance.damage, hits[i].point);
                        if (obj.GetComponent<Boss_Golem>()) obj.GetComponent<Boss_Golem>().SetDamage(Player.instance.isLeftNear ? Player.instance.damageNear : Player.instance.damage, hits[i].point);
                        if (obj.GetComponent<Boss_Stone>()) obj.GetComponent<Boss_Stone>().SetDamage(Player.instance.isLeftNear ? Player.instance.damageNear : Player.instance.damage, hits[i].point, false);

                        if (Player.instance.isStrategic_02) Player.instance.isStrategic_02 = false;
                        GameObject obj2 = ObjManager.Call().GetObject("Sword_Hit");
                        obj2.transform.position = obj.transform.position;
                        obj2.GetComponent<Animator>().Rebind();
                        obj2.SetActive(true);

                        GameObject obj3 = ObjManager.Call().GetObject("Hit_sprite4");
                        obj3.transform.position = obj.transform.position;
                        obj3.transform.localScale = Vector3.one;
                        obj3.transform.localEulerAngles = Vector2.zero;
                        obj3.GetComponent<SwordHitEffectController>()._effctName = "Hit_" + Random.Range(3, 6);
                        obj3.GetComponent<SwordHitEffectController>().animSpeed = 1.0f;
                        obj3.GetComponent<SwordHitEffectController>().isPenetrate = false;
                        obj3.GetComponent<SwordHitEffectController>().isAttack = false;
                        obj3.GetComponent<Animator>().Rebind();
                        obj3.SetActive(true);

                        if (PDG.Player.instance.isLeftNear && PDG.Player.instance.isCritical)
                        {
                            GameObject criEffect = ObjManager.Call().GetObject("CriticalHitEffect");
                            criEffect.transform.position = hits[i].point;
                            criEffect.SetActive(true);
                        }
                    }
                }
            }
        }
    }
    IEnumerator TimeOut()
    {
        sr.enabled = false;
        yield return new WaitForSeconds(10.0f);
        gameObject.SetActive(false);
    }
}
