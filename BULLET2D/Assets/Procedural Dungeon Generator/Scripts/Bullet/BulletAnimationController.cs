﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletAnimationController : MonoBehaviour
{
    private SpriteRenderer sr;
    private Animator animator;
    private void Awake()
    {
        sr = GetComponent<SpriteRenderer>();
        animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    public void Change(RuntimeAnimatorController rControl)
    {
        if(rControl.name.Contains("startGun"))
        {
            sr.enabled = false;
        }
        else
        {
            sr.enabled = true;
        }
        animator.runtimeAnimatorController = rControl;
        animator.Rebind();
    }

    public void AnimationEnd()
    {
        gameObject.SetActive(false);
    }
}
