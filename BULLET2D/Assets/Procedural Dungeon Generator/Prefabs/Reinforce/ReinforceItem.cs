﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ReinforceItem : MonoBehaviour, IPointerClickHandler
{
    GameObject cont;
    ReinforceData rData;
    public bool isSelected = false;
    public void InitSetting(GameObject _cont, ReinforceData _rData)
    {
        cont = _cont;
        rData = _rData;
    }
    public void OnPointerClick(PointerEventData eventData)
    {
        if (eventData.button == PointerEventData.InputButton.Left)
            ReinforceController.instance.SelecteCont(cont);
        else if (eventData.button == PointerEventData.InputButton.Right)
        {
            if (isSelected) ReinforceController.instance.SelectRight(cont, rData);
        }
    }
}
