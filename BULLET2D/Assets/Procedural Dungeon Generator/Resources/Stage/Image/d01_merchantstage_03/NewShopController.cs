﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class NewShopController : MonoBehaviour
{
    public static NewShopController instance;

    [SerializeField] private GameObject controllerObj;
    [SerializeField] private SpriteRenderer controllerSR;
    [SerializeField] private bool isSelect = false;
    public bool isUsing = false;
    public bool isUse = false;
    private float delay = 0.0f;

    [SerializeField] private Vector3 vOffset;
    [SerializeField] private Vector3 vSize;

    [SerializeField] private GameObject timerObj;

    [SerializeField] private AudioSource audio;
    [SerializeField] private AudioSource audio2;
    [SerializeField] private AudioClip[] clips;

    private Animator animator;
    public bool isMake = false;
    public int nRequiredCount = 20;
    int maxCount = 6;
    int nowCount = 0;
    public List<string> itemList = new List<string>();
    public List<GameObject> itemList2 = new List<GameObject>();
    [SerializeField] private Transform[] ItemSettingPositions;
    bool isOpen = false;
    private void Awake()
    {
        if (instance == null) instance = this;
    }
    private void Start()
    {
        animator = GetComponent<Animator>();
        timerObj = GameObject.Find("Canvas").transform.Find("Timer").gameObject;
        clips = Resources.LoadAll<AudioClip>("Object/Dungeon_2_Computer");
    }

    private void Update()
    {
        delay -= Time.deltaTime;
        if (delay <= 0.0f) delay = 0.0f;


        RaycastHit2D hit = Physics2D.BoxCast(transform.position + vOffset, vSize, 0, Vector2.zero, 0, 1 << 9);
        if (hit && !isSelect && !isOpen && !PDG.Dungeon.instance.newObjectSelected && !isMake && nowCount < 1)
        {
            isSelect = true;
            PDG.Dungeon.instance.newObjectSelected = true;
            PDG.Dungeon.instance.newObjectSelectedObj = gameObject;
            controllerObj.GetComponent<SpriteRenderer>().sprite = PDG.Dungeon.instance.isJoystrick ? PDG.Dungeon.GetJoyStrickKeySprite(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_ACTIVE]) : 
                PDG.Dungeon.GetJoyStrickKeySprite(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ACTIVE]);
            controllerObj.SetActive(true);
            TextOpenController.instance.ShowText(LocalizeManager.GetLocalize("shop_title"), Color.white, 24, controllerObj, 2.0f, true);
        }
        else if (hit && !isSelect && isOpen && !isMake && (nowCount < 1 || PDG.Dungeon.instance.isTestMode2))
        {
            isSelect = true;
            controllerObj.SetActive(true);
            string coinText = LocalizeManager.GetLocalize("newshop_coin");
            coinText = coinText.Replace("##", nRequiredCount.ToString());
            string t = "" + coinText + "" + LocalizeManager.GetLocalize("newshop_content");
            TextOpenController.instance.ShowText(t, Color.white, 24, controllerObj, 2.0f, true);
        }
        else if (hit && isMake && (nowCount < 1 || PDG.Dungeon.instance.isTestMode2))
        {
            controllerObj.SetActive(false);
            string coinText = LocalizeManager.GetLocalize("newshop_waiting");
            TextOpenController.instance.ShowText(coinText, Color.white, 24, controllerObj, 2.0f, true);
        }
        else if (hit && (nowCount >= 1 && !PDG.Dungeon.instance.isTestMode2))
        {
            controllerObj.SetActive(false);
            string coinText = LocalizeManager.GetLocalize("newshop_overflow");
            TextOpenController.instance.ShowText(coinText, Color.white, 24, controllerObj, 2.0f, true);
        }
        else if (!hit)
        {
            isOpen = false;
            isSelect = false;
            isUse = false;
            if (PDG.Dungeon.instance.newObjectSelectedObj == gameObject)
            {
                PDG.Dungeon.instance.newObjectSelectedObj = null;
                PDG.Dungeon.instance.newObjectSelected = false;
            }
            controllerObj.SetActive(false);
            if (TextOpenController.instance.targetObj == controllerObj)
            {
                TextOpenController.instance.TextClose(Color.white);
                timerObj.SetActive(false);
            }
        }

        controllerSR.sprite = PDG.Dungeon.instance.isJoystrick ? PDG.Dungeon.GetJoyStrickKeySprite(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_ACTIVE]) :
            PDG.Dungeon.GetJoyStrickKeySprite(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ACTIVE]);
        if (timerObj.activeSelf && (Input.GetKeyDown(KeyCode.Escape) || PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_CANCLE], true)) && PDG.Dungeon.instance.closeCoolTime > 0.2f)
        {
            PDG.Dungeon.instance.closeCoolTime = 0.0f;
            isSelect = false;
            if (PDG.Dungeon.instance.newObjectSelectedObj == gameObject)
            {
                PDG.Dungeon.instance.newObjectSelectedObj = null;
                PDG.Dungeon.instance.newObjectSelected = false;
            }
            isOpen = false;
            isUse = false;
            if (audio2) audio2.Stop();
            animator.SetTrigger("makeEnd");
            controllerObj.SetActive(false);
            if (TextOpenController.instance.targetObj == controllerObj)
            {
                TextOpenController.instance.TextClose(Color.white);
                timerObj.SetActive(false);
            }
            PlayerCombat.instance.tCoroutine = null;
        }
        if (isSelect)
        {
            if ((PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ACTIVE], true) || PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_ACTIVE], true)) && !isOpen && delay <= 0.0f) 
            {
                isOpen = true;
                isSelect = false;
                controllerObj.SetActive(false);
                delay = 0.05f;
                SoundManager.instance.StartAudio(new string[1] { "Click_Maca" }, VOLUME_TYPE.EFFECT);
            }
            if ((PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ACTIVE], true) || PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_ACTIVE], true)) && isOpen && !isUse && delay <= 0.0f && nowCount < 1)
            {
                SoundManager.instance.StartAudio(new string[1] { "Click_Maca" }, VOLUME_TYPE.EFFECT);
                if (nRequiredCount <= Inventorys.Instance.glowindskull)
                {
                    isSelect = false;
                    controllerObj.SetActive(false);
                    animator.SetTrigger("make");
                    isUse = true;
                    PlayerCombat.instance.tCoroutine = StartCoroutine(Make());
                }
                else
                {
                    if (PDG.Dungeon.instance.newObjectSelectedObj == gameObject)
                    {
                        PDG.Dungeon.instance.newObjectSelectedObj = null;
                        PDG.Dungeon.instance.newObjectSelected = false;
                    }
                    TextOpenController.instance.ShowText(LocalizeManager.GetLocalize("d_lack_comment"), Color.white, 24, controllerObj, 2.0f, true);
                }
            }
        }
    }
    public void ReSetItem(ItemInfo item)
    {
        Inventorys.Instance.shopOpenItems.Add(new ItemInfo() { Data = Inventorys.Instance.CopyItem(item.Data) });

        for (int i = 0; i < itemList2.Count; i++)
        {
            if (itemList2[i] == null) continue;
            GameObject obj = ObjManager.Call().GetObject("GetItem");
            if (obj != null)
            {
                obj.transform.position = itemList2[i].transform.position;
                obj.SetActive(true);
            }
            Destroy(itemList2[i]);
        }
        itemList.Clear();
        itemList2.Clear();
        isMake = false;
        if (nowCount == 1) nRequiredCount = 35;
        else if (nowCount == 2) nRequiredCount = 50;
        else if (nowCount == 3) nRequiredCount = 70;
        else if (nowCount == 4) nRequiredCount = 100;

    }
    IEnumerator Make()
    {
        timerObj.SetActive(true);
        float t = 0.0f;
        while (isUse)
        {
            Vector3 p = controllerObj.transform.position;
            p.y -= 0.5f;
            timerObj.transform.position = Camera.main.WorldToScreenPoint(p);
            if (audio2 && !audio2.isPlaying)
            {
                audio2.clip = clips[Random.Range(0, clips.Length)];
                audio2.volume =  (SoundManager.isMute || (PDG.Dungeon.instance == null ? false : PDG.Dungeon.instance.isPause) || (PDG.Player.instance == null ? false : PDG.Player.instance.isDie)) ? 0 : SoundManager.EffectVolume;
                audio2.Play();
            }
            t += Time.deltaTime;
            int tInt = Mathf.CeilToInt(2.0f - t);
            timerObj.transform.Find("Text").GetComponent<Text>().text = tInt.ToString();
            timerObj.transform.Find("Image").GetComponent<Image>().fillAmount = t / 2.0f;
            timerObj.transform.Find("TextCont").GetComponent<Text>().text = LocalizeManager.GetLocalize("shop_ing");
            if (t >= 2.0f) break;
            yield return new WaitForSeconds(Time.deltaTime);
        }
        if(isUse)
        {
            animator.SetTrigger("makeEnd");
            SoundManager.instance.StartAudio(new string[1] { "ShopSpake" }, VOLUME_TYPE.AMBIENT);
            StartCoroutine(SpakeCoroutine());
            Inventorys.Instance.glowindskull -= nRequiredCount;
            SaveAndLoadManager.instance.SaveItem(new ItemSave
            {
                _id = "glowing_skull",
                _nCount = Inventorys.Instance.glowindskull
            }, true);

            PDG.Dungeon.instance.newObjectSelectedObj = null;
            PDG.Dungeon.instance.newObjectSelected = false;
            isMake = true;
            nowCount++;
        }
        isUse = false;
        timerObj.SetActive(false);
        PlayerCombat.instance.tCoroutine = null;
    }
    public void ShopSetting()
    {
        MerchantData[] mDatas = System.Array.FindAll(SheetManager.Instance.MerchantDB.dataArray, item => item.ID.Equals("d01_merchantstage_03"));
        for (int i = 0; i < mDatas.Length; i++)
        {
            string[] firstCheckString;
            string selectedItemID = string.Empty;
            bool first = false;
            if (!mDatas[i].Itemcategories1.Equals(string.Empty))
            {
                if (!mDatas[i].Itemcategories2.Equals(string.Empty))
                {
                    if (!mDatas[i].Itemcategories3.Equals(string.Empty)) firstCheckString = new string[3];
                    else firstCheckString = new string[2];
                }
                else firstCheckString = new string[1];
            }
            else firstCheckString = null;

            // 뒤에꺼 체크
            if (firstCheckString == null)
            {
                first = false;
                if (mDatas[i].Itemgrade.Length > 0)
                {
                    string grade = mDatas[i].Itemgrade[GetBoxGrade(mDatas[i].Gradeprobability)];

                    List<ItemsData> iDatas = System.Array.FindAll(SheetManager.Instance.ItemsDB.dataArray, item => item.Grade.ToUpper().Equals(grade.ToUpper())).ToList();
                    for (int j = 0; j < iDatas.Count; j++)
                    {
                        Sprite s = Resources.Load<Sprite>("Item/" + iDatas[j].Type1 + "/" + iDatas[j].Type2 + "/" + iDatas[j].ID);
                        if (s == null || !Inventorys.Instance.UnlockAndMaxCountCheck(iDatas[j].ID) || itemList.FindIndex(item => item.Equals(iDatas[j].ID)) != -1)
                        {
                            iDatas.RemoveAt(j);
                            j--;
                            continue;
                        }
                    }
                    if (iDatas.Count > 0)
                    {
                        selectedItemID = iDatas[Random.Range(0, iDatas.Count)].ID;
                    }
                    else
                    {
                        int iii = 0;
                        while (true)
                        {
                            grade = mDatas[i].Itemgrade[GetBoxGrade(mDatas[i].Gradeprobability)];
                            List<ItemsData> iDatas2 = System.Array.FindAll(SheetManager.Instance.ItemsDB.dataArray, item => item.Grade.ToUpper().Equals(grade.ToUpper())).ToList();
                            for (int j = 0; j < iDatas2.Count; j++)
                            {
                                Sprite s = Resources.Load<Sprite>("Item/" + iDatas2[j].Type1 + "/" + iDatas2[j].Type2 + "/" + iDatas2[j].ID);
                                if (s == null || !Inventorys.Instance.UnlockAndMaxCountCheck(iDatas2[j].ID) || itemList.FindIndex(item => item.Equals(iDatas2[j].ID)) != -1)
                                {
                                    iDatas2.RemoveAt(j);
                                    j--;
                                    continue;
                                }
                            }
                            if (iDatas2.Count > 0)
                            {
                                selectedItemID = iDatas2[Random.Range(0, iDatas2.Count)].ID;
                                break;
                            }
                            iii++;
                            if (iii > 10000) break;
                        }
                    }
                }
            }
            else
            {
                first = true;
                if (firstCheckString.Length == 1) firstCheckString[0] = mDatas[i].Itemcategories1;
                else if (firstCheckString.Length == 2)
                {
                    firstCheckString[0] = mDatas[i].Itemcategories1;
                    firstCheckString[1] = mDatas[i].Itemcategories2;
                }
                else
                {
                    firstCheckString[0] = mDatas[i].Itemcategories1;
                    firstCheckString[1] = mDatas[i].Itemcategories2;
                    firstCheckString[2] = mDatas[i].Itemcategories3;
                }
                selectedItemID = firstCheckString[Random.Range(0, firstCheckString.Length)];
            }
            if (selectedItemID == null) continue;

            if (!selectedItemID.Equals(string.Empty))
            {
                GameObject obj = ObjManager.Call().GetObject("Item");
                if (obj != null)
                {
                    Item _item = obj.GetComponent<Item>();
                    if (first)
                    {
                        List<ItemsData> iDatas = System.Array.FindAll(SheetManager.Instance.ItemsDB.dataArray, item => item.Type2.ToUpper().Equals(selectedItemID.ToUpper()) && !item.Grade.Equals("F") && !item.Grade.Equals("G")).ToList();
                        for (int j = 0; j < iDatas.Count; j++)
                        {
                            Sprite s = Resources.Load<Sprite>("Item/" + iDatas[j].Type1 + "/" + iDatas[j].Type2 + "/" + iDatas[j].ID);
                            if (s == null || !Inventorys.Instance.UnlockAndMaxCountCheck(iDatas[j].ID) || itemList.FindIndex(item => item.Equals(iDatas[j].ID)) != -1)
                            {
                                iDatas.RemoveAt(j);
                                j--;
                                continue;
                            }
                        }
                        if (iDatas.Count > 0)
                        {
                            selectedItemID = iDatas[Random.Range(0, iDatas.Count)].ID;
                        }
                        else
                        {
                            int iii = 0;
                            while (true)
                            {
                                string grade = mDatas[i].Itemgrade[GetBoxGrade(mDatas[i].Gradeprobability)];
                                List<ItemsData> iDatas2 = System.Array.FindAll(SheetManager.Instance.ItemsDB.dataArray, item => item.Grade.ToUpper().Equals(grade.ToUpper())).ToList();
                                for (int j = 0; j < iDatas2.Count; j++)
                                {
                                    Sprite s = Resources.Load<Sprite>("Item/" + iDatas2[j].Type1 + "/" + iDatas2[j].Type2 + "/" + iDatas2[j].ID);
                                    if (s == null || !Inventorys.Instance.UnlockAndMaxCountCheck(iDatas2[j].ID) || itemList.FindIndex(item => item.Equals(iDatas2[j].ID)) != -1)
                                    {
                                        iDatas2.RemoveAt(j);
                                        j--;
                                        continue;
                                    }
                                }
                                if (iDatas2.Count > 0)
                                {
                                    selectedItemID = iDatas2[Random.Range(0, iDatas2.Count)].ID;
                                    break;
                                }
                                iii++;
                                if (iii > 10000) break;
                            }
                        }
                    }
                    if (selectedItemID.Equals(string.Empty)) continue;

                    _item._Item = new ItemInfo
                    {
                        Data = Inventorys.Instance.CopyItem(System.Array.Find(SheetManager.Instance.ItemsDB.dataArray, item => item.ID.Equals(selectedItemID)))
                    };
                    _item.dir = Vector2.zero;
                    _item.itemCount = mDatas[i].Itemcount;
                    _item.rPow = 0;
                    obj.transform.position = ItemSettingPositions[i].position;
                    obj.SetActive(true);
                    _item.isShop = true;
                    itemList.Add(selectedItemID);
                    itemList2.Add(obj);
                }
            }
        }
    }
    private int GetBoxGrade(float[] percent)
    {
        int[] per = new int[percent.Length];
        for (int i = 0; i < percent.Length; i++)
        {
            per[i] = Mathf.FloorToInt(percent[i] * 1000);
        }

        int[] perTotal = new int[1000];
        int t = 0;
        for (int i = 0; i < per.Length; i++)
        {
            for (int j = 0; j < per[i]; j++)
            {
                perTotal[t] = i;
                t++;
            }
        }

        return perTotal[Random.Range(0, 1000)];
    }
    IEnumerator SpakeCoroutine()
    {
        float a = 0.0f;
        UnityEngine.UI.Image img = GameObject.Find("Canvas").transform.Find("Spake").GetComponent<UnityEngine.UI.Image>();
        while (true)
        {
            img.color = new Color(1, 1, 1, a);
            a += Time.deltaTime * 2.5f;
            if (a >= 1.0f) break;
            yield return null;
        }
        float dis = Vector3.Distance(PDG.Player.instance.transform.position, transform.position);
        if (dis > 10) audio.volume = 0;
        if ( (SoundManager.isMute || (PDG.Dungeon.instance == null ? false : PDG.Dungeon.instance.isPause) || (PDG.Player.instance == null ? false : PDG.Player.instance.isDie)))
        {
            audio.volume = 0;
        }
        else
        {
            float tVolume = SoundManager.AmbientVolume;
            float t = tVolume / 10;
            audio.volume = tVolume - (dis * t);
        }
        audio.Play();
        img.color = Color.white;
        ShopSetting();
        while (true)
        {
            img.color = new Color(1, 1, 1, a);
            a -= Time.deltaTime * 2.5f;
            if (a <= 0.0f) break;
            yield return null;
        }
        img.color = new Color(1, 1, 1, 0);
    }
}
