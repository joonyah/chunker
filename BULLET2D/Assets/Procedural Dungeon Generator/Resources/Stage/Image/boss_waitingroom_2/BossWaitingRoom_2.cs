﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossWaitingRoom_2 : MonoBehaviour
{
    [SerializeField] private bool isStartPos;
    bool isStart = false;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (!isStart && collision.gameObject.layer.Equals(9)) 
        {
            isStart = true;
            if (!isStartPos) 
                Dialogue.Instance.CommnetSetting("stage2_boss_intro", 0, null);
            else
                Dialogue.Instance.CommnetSetting("boss_waiting2_start", 0, null);
        }
    }
}
