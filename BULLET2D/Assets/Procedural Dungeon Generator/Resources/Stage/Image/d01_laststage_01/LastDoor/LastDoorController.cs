﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LastDoorController : MonoBehaviour
{
    private bool isOpen = false;
    private bool isSelect = false;
    private bool isWait = false;
    [SerializeField] private Vector2 offSet;
    [SerializeField] private Vector2 hitSize;
    [SerializeField] private string LocalObjName;
    [SerializeField] private string LocalObjName2;
    [SerializeField] private GameObject controllerObj;
    [SerializeField] private Animator animObj;
    [SerializeField] private SpriteRenderer controllerSR;
    // Start is called before the first frame update
    void Start()
    {
        
    }
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireCube(transform.position + (Vector3)offSet, hitSize);
    }
    // Update is called once per frame
    void Update()
    {
        RaycastHit2D hit = Physics2D.BoxCast(transform.position + (Vector3)offSet, hitSize, 0, Vector2.zero, 0, 1 << 9);
        if (hit && !isSelect && !isWait)
        {
            isSelect = true;
            if (!isOpen) TextOpenController.instance.ShowText(LocalizeManager.GetLocalize(LocalObjName), Color.white, 24, controllerObj, 2f, true);
            else TextOpenController.instance.ShowText(LocalizeManager.GetLocalize(LocalObjName2), Color.white, 24, controllerObj, 2f, true);
            controllerSR.sprite = PDG.Dungeon.instance.isJoystrick ? PDG.Dungeon.GetJoyStrickKeySprite(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_ACTIVE]) : PDG.Dungeon.GetJoyStrickKeySprite(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ACTIVE]);
            controllerObj.SetActive(true);
        }
        else if (!hit)
        {
            isSelect = false;
            controllerObj.SetActive(false);
            TextOpenController.instance.TextClose(Color.white);
        }
        if (isSelect)
        {
            if (PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ACTIVE], true) || PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_ACTIVE], true) && PDG.Dungeon.instance.closeCoolTime > 0.2f)
            {
                PDG.Dungeon.instance.closeCoolTime = 0.0f;
                SoundManager.instance.StartAudio(new string[1] { "Object/active_F" }, VOLUME_TYPE.EFFECT);
                isSelect = false;
                controllerObj.SetActive(false);
                if (!isOpen)
                {
                    isOpen = true;
                    isWait = true;
                    GetComponent<Animator>().SetTrigger("isOpen");
                }
                else
                {
                    StartCoroutine(EndingCredits());
                }
            }
        }
    }
    IEnumerator EndingCredits()
    {
        isWait = true;
        TextOpenController.instance.TextClose(Color.white);
        PDG.Player.instance.gameObject.SetActive(false);
        if (animObj) animObj.gameObject.SetActive(true);
        if (animObj) animObj.Rebind();
        yield return new WaitForSecondsRealtime(2.0f);
        SaveAndLoadManager.instance.GameDelete();
        SceneManager.LoadScene("EndingCredits");
    }
    public void OpenDoor()
    {
        isWait = false;
    }
}
