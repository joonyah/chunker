﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BigBulletController : MonoBehaviour
{
    public int _type;
    // Start is called before the first frame update
    [SerializeField] private UbhShotCtrl ranShoot;
    [SerializeField] private Rigidbody2D rigid;
    [SerializeField] private float forcePow = 300.0f;
    public Transform mapObj;
    public Vector3 mapSize;
    [SerializeField] private CircleCollider2D coll;
    [SerializeField] private GameObject hitEffect;
    [SerializeField] private bool isLaser;
    [SerializeField] private bool isGolem;
    [SerializeField] private GameObject shadowObj;
    public void StartBall()
    {
        if(_type == 0)
        {
            rigid.AddForce((PDG.Player.instance.transform.position - transform.position).normalized * forcePow * Time.deltaTime, ForceMode2D.Impulse);
            ranShoot.StartShotRoutine();
            Destroy(gameObject, 2.0f);
        }
        else
        {
            StartCoroutine(Jumping());
        }
    }
    IEnumerator Jumping()
    {
        shadowObj.gameObject.SetActive(true);
        int jumpingCount = 0;
        while (jumpingCount < (PDG.Dungeon.instance.StageLevel == 3 ? 3 : 5))
        {
            Vector3 startPos = transform.position;
            Vector3 endPos = new Vector3(mapObj.position.x + Random.Range(-mapSize.x, mapSize.x), mapObj.position.y + Random.Range(-mapSize.y, mapSize.y));

            Vector3 differencePos = endPos - startPos;
            Vector3 center1 = differencePos / 4;
            Vector3 center2 = center1 * 3;
            center1 += startPos;
            center2 += startPos;

            Vector3 temp1 = GetPosition(center1, 90);
            Vector3 temp1_1 = temp1 - center1;
            temp1_1.Normalize();
            center1 = center1 + (temp1_1 * 5);

            Vector3 temp2 = GetPosition(center2, 90);
            Vector3 temp2_1 = temp2 - center2;
            temp2_1.Normalize();
            center2 = center1 + (temp2_1 * 5);
            float t = 0.0f;
            coll.enabled = false;
            while (true)
            {
                Vector3 lerp1_0 = Vector3.Lerp(startPos, center1, t);
                Vector3 lerp1_1 = Vector3.Lerp(center1, center2, t);
                Vector3 lerp1_2 = Vector3.Lerp(center2, endPos, t);

                Vector3 lerp2_0 = Vector3.Lerp(lerp1_0, lerp1_1, t);
                Vector3 lerp2_1 = Vector3.Lerp(lerp1_1, lerp1_2, t);

                Vector3 lerp = Vector3.Lerp(lerp2_0, lerp2_1, t);
                transform.position = lerp;

                Vector3 lerp_shadow_0 = Vector3.Lerp(startPos, endPos, t);
                shadowObj.transform.position = lerp_shadow_0;
                t += Time.deltaTime * Random.Range(1, 2);
                if (t >= 1.0f)
                {
                    lerp1_0 = Vector3.Lerp(startPos, center1, 1);
                    lerp1_1 = Vector3.Lerp(center1, center2, 1);
                    lerp1_2 = Vector3.Lerp(center2, endPos, 1);
                    lerp2_0 = Vector3.Lerp(lerp1_0, lerp1_1, 1);
                    lerp2_1 = Vector3.Lerp(lerp1_1, lerp1_2, 1);
                    lerp = Vector3.Lerp(lerp2_0, lerp2_1, 1);
                    transform.position = lerp;
                    lerp_shadow_0 = Vector3.Lerp(startPos, endPos, 1);
                    shadowObj.transform.position = lerp_shadow_0;
                    break;
                }
                yield return new WaitForSeconds(Time.deltaTime);
            }
            coll.enabled = true;
            ranShoot.StartShotRoutine();
            yield return new WaitForSeconds(0.1f);
            ranShoot.StopShotRoutine();
            jumpingCount++;
        }
        shadowObj.gameObject.SetActive(false);
        Destroy(gameObject);
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.layer.Equals(9))
        {
            if(collision.gameObject.GetComponent<PDG.Player>())
            {
                if (hitEffect && !collision.gameObject.GetComponent<PDG.Player>().isHit && !collision.gameObject.GetComponent<PDG.Player>().isDie)
                {
                    GameObject obj = Instantiate(hitEffect);
                    obj.transform.position = collision.gameObject.transform.position;
                    obj.transform.eulerAngles = new Vector3(0, 0, 0);
                   Destroy(obj, 2f);
                }
                collision.gameObject.GetComponent<PDG.Player>().SetDamage(1, isLaser ? LocalizeManager.GetLocalize("killedby_laser") : 
                    (isGolem ? LocalizeManager.GetLocalize("killedby_golemAttack") : LocalizeManager.GetLocalize("killedby_big_ball")), DEATH_TYPE.DEATH_BOSS, "boss_golem");
            }
        }
    }
    #region -- Tool --
    public static float GetAngle(Vector3 vStart, Vector3 vEnd)
    {
        Vector3 v = vEnd - vStart;

        return Mathf.Atan2(v.y, v.x) * Mathf.Rad2Deg;
    }
    public static Vector3 GetPosition(Vector3 vStart, float angle)
    {
        float x = Mathf.Cos(Mathf.Deg2Rad * angle) + vStart.x;
        float y = Mathf.Sin(Mathf.Deg2Rad * angle) + vStart.y;

        return new Vector3(x, y, 0);
    }
    #endregion
}
