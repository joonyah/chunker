﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MummyFireController : MonoBehaviour
{
    [SerializeField] private GameObject drumBombObj;

    private void Update()
    {
        RaycastHit2D hit = Physics2D.CapsuleCast(transform.position + new Vector3(0, -0.03f, 0), new Vector2(0.55f, 0.35f), CapsuleDirection2D.Horizontal, 0, Vector2.zero, transform.eulerAngles.z, 1 << 0 | 1 << 27 | 1 << 9 | 1 << 8);
        if (hit)
        {

            if (hit.collider.gameObject.layer.Equals(0) || hit.collider.gameObject.layer.Equals(27))
            {
                if (hit.collider.name.Equals("Shield"))
                {
                    PDG.Player.instance.right_defence_gauge -= 1.0f;
                    Inventorys.Instance.GuageShaker(1);
                    CameraShaker._instance.StartShake(0.2f, 0.02f, 0.2f);
                }
                if (hit.collider.name.Contains("dna_mo_blade"))
                {
                    PDG.Player.instance.left_defence_gauge -= 1.0f;
                    Inventorys.Instance.GuageShaker(0);
                    CameraShaker._instance.StartShake(0.2f, 0.02f, 0.2f);
                }

                GameObject obj2 = ObjManager.Call().GetObject("Bullet_Hit");
                obj2.transform.position = transform.position;
                obj2.GetComponent<SwordHitEffectController>()._isMonster = true;
                obj2.GetComponent<SwordHitEffectController>()._effctName = "pistol_bullet";
                obj2.GetComponent<Animator>().Rebind();
                obj2.transform.localEulerAngles = new Vector3(0, 0, 0);
                obj2.SetActive(true);
                Destroy(gameObject);
                return;
            }
            if (hit.collider.gameObject.layer.Equals(9))
            {
                if (hit.collider.gameObject.GetComponent<PDG.Player>())
                    hit.collider.gameObject.GetComponent<PDG.Player>().SetDamage(1, LocalizeManager.GetLocalize("monster_mummy_white"), DEATH_TYPE.DEATH_NORMAL, "mummy_white", ELEMENTAL_TYPE.NONE);
            }

            GameObject obj = Instantiate(drumBombObj);
            obj.GetComponent<DrumBomb>().InitSetting(3, transform.position, drumBombObj, true, false, 0.25f, "mummy_white", true);
            Destroy(gameObject);
        }
    }
}
