﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class White_ant_bomb : MonoBehaviour
{
    [SerializeField] private GameObject destroyEffect;
    [SerializeField] private GameObject destroyEffect2;
    [SerializeField] private GameObject shadowObj;
    [SerializeField] private GameObject fireObj;
    [SerializeField] private bool isDestroy;
    string mobID;
    public void Throw(Vector3 startPos, Vector3 endPos, bool isPlayer, string _mobID)
    {
        mobID = _mobID;
        startPos.z = 0;
        endPos.z = 0;
        StartCoroutine(ThrowCoroutine(startPos, endPos, isPlayer));
    }
    IEnumerator ThrowCoroutine(Vector3 startPos, Vector3 endPos, bool isPlayer)
    {
        Vector3 differencePos = endPos - startPos;
        Vector3 center1 = differencePos / 4;
        Vector3 center2 = center1 * 3;
        center1 += startPos;
        center2 += startPos;

        Vector3 temp1 = GetPosition(center1, 90);
        Vector3 temp1_1 = temp1 - center1;
        temp1_1.Normalize();
        center1 = center1 + (temp1_1 * 5);

        Vector3 temp2 = GetPosition(center2, 90);
        Vector3 temp2_1 = temp2 - center2;
        temp2_1.Normalize();
        center2 = center1 + (temp2_1 * 5);

        float t = 0.0f;
        while (true)
        {
            Vector3 lerp1_0 = Vector3.Lerp(startPos, center1, t);
            Vector3 lerp1_1 = Vector3.Lerp(center1, center2, t);
            Vector3 lerp1_2 = Vector3.Lerp(center2, endPos, t);

            Vector3 lerp2_0 = Vector3.Lerp(lerp1_0, lerp1_1, t);
            Vector3 lerp2_1 = Vector3.Lerp(lerp1_1, lerp1_2, t);

            Vector3 lerp = Vector3.Lerp(lerp2_0, lerp2_1, t);

            transform.position = lerp;
            Vector3 lerp_shadow_0 = Vector3.Lerp(startPos, endPos, t);
            shadowObj.transform.position = lerp_shadow_0;
            t += Time.deltaTime;
            if (t >= 1.0f)
            {
                lerp1_0 = Vector3.Lerp(startPos, center1, 1);
                lerp1_1 = Vector3.Lerp(center1, center2, 1);
                lerp1_2 = Vector3.Lerp(center2, endPos, 1);

                lerp2_0 = Vector3.Lerp(lerp1_0, lerp1_1, 1);
                lerp2_1 = Vector3.Lerp(lerp1_1, lerp1_2, 1);

                lerp = Vector3.Lerp(lerp2_0, lerp2_1, 1);

                transform.position = lerp;
                lerp_shadow_0 = Vector3.Lerp(startPos, endPos, 1);
                shadowObj.transform.position = lerp_shadow_0;
                break;
            }
            yield return new WaitForSeconds(Time.deltaTime);
        }
        if(destroyEffect2)
        {
            GameObject bb2 = Instantiate(destroyEffect2);
            bb2.transform.position = transform.position;
            Destroy(bb2, 2.0f);
        }
        GetComponent<Animator>().SetTrigger("Bomb");
        if(destroyEffect)
        {
            GameObject obj2 = Instantiate(destroyEffect);
            if (obj2.GetComponent<DrumBomb>()) obj2.GetComponent<DrumBomb>().InitSetting(0, transform.position, destroyEffect, true, false, 0.25f, mobID);
        }
        if (isPlayer)
            SoundManager.instance.StartAudio(new string[1] { "Weapon/explosive_cell_bomb" }, VOLUME_TYPE.EFFECT);
        else
            SoundManager.instance.StartAudio(new string[1] { "Object/ant_poison" }, VOLUME_TYPE.EFFECT, gameObject);
        if(isPlayer && PDG.Player.instance.nowWeaponName.Equals("explosive_cell_1"))
        {
            GameObject obj = Instantiate(fireObj);
            obj.GetComponent<DrumBomb>().isPlayer = true;
            obj.GetComponent<DrumBomb>().InitSetting(1, transform.position, fireObj, false, false, 0.25f, mobID);
        }


        if (isDestroy)
        {
            BombEnd();
        }
    }
    public void BombEnd()
    {
        if (GetComponent<AudioSource>())
        {
            SoundManager.instance.StartAudio(new string[1] { "Monster/slime_bright_bomb" }, VOLUME_TYPE.EFFECT, gameObject);
        }
        Destroy(gameObject);
    }
    public void BombStart()
    {
        if (destroyEffect)
        {
            GameObject obj3 = Instantiate(destroyEffect);
            obj3.transform.position = transform.position;
            Destroy(obj3, 2f);
        }
        else
        {
            GameObject obj3 = ObjManager.Call().GetObject("Hit_sprite9");
            obj3.transform.position = transform.position;
            obj3.transform.localScale = Vector3.one;
            obj3.transform.localEulerAngles = new Vector3(0, 0, 0);
            obj3.GetComponent<SwordHitEffectController>()._effctName = "bomb";
            obj3.GetComponent<SwordHitEffectController>().animSpeed = 1.0f;
            obj3.GetComponent<Animator>().Rebind();
            obj3.SetActive(true);
        }

        RaycastHit2D[] hit = Physics2D.CircleCastAll(transform.position, 3, Vector2.zero, 0, 1 << 12 | 1 << 24 | 1 << 23);
        if (hit.Length != 0)
        {
            for (int i = hit.Length - 1; i >= 0; i--)
            {
                if (hit[i].collider.gameObject.layer.Equals(12))
                {

                    Vector3 dir2 = hit[i].collider.transform.position - PDG.Player.instance.transform.position;
                    dir2.Normalize();

                    hit[i].collider.GetComponent<MOB.Monster>().SetDamage((PDG.Player.instance.isRightNear ? PDG.Player.instance.damageRightNear : PDG.Player.instance.damageRight), -dir2, 2, ELEMENTAL_TYPE.NONE, 0.0f);
                    if (PDG.Player.instance.isRightNear && PDG.Player.instance.isCriticalRight)
                    {
                        GameObject criEffect = ObjManager.Call().GetObject("CriticalHitEffect");
                        criEffect.transform.position = hit[i].point;
                        criEffect.SetActive(true);
                    }
                }
                else if (hit[i].collider.gameObject.layer.Equals(23))
                {
                    if (hit[i].collider.transform.parent && hit[i].collider.transform.parent.GetComponent<TentacleController>())
                    {
                        hit[i].collider.transform.parent.GetComponent<TentacleController>().SetDamage(1);
                    }
                    Vector3 dir = hit[i].collider.gameObject.transform.position - PDG.Player.instance.transform.position;
                    dir.Normalize();
                    if (hit[i].collider.gameObject.GetComponent<DestroyObjects>())
                    {
                        hit[i].collider.gameObject.GetComponent<DestroyObjects>().DestroyObject(dir);
                    }
                    if (hit[i].collider.gameObject.GetComponent<ChestHit>() && !hit[i].collider.gameObject.GetComponent<ChestHit>().isOpenBefore)
                    {
                        hit[i].collider.gameObject.GetComponent<ChestHit>().SetDamage(1);
                    }
                    if (hit[i].collider.gameObject.GetComponent<DrumHit>())
                    {
                        hit[i].collider.gameObject.GetComponent<DrumHit>().SetDamage(1);
                    }
                }
                else if (hit[i].collider.gameObject.layer.Equals(24))
                {
                    if (hit[i].collider.GetComponent<MOB.BossMonster_Six_Apear>()) hit[i].collider.GetComponent<MOB.BossMonster_Six_Apear>().SetDamage(PDG.Player.instance.damageRight);
                    if (hit[i].collider.GetComponent<Boss_STG>()) hit[i].collider.GetComponent<Boss_STG>().SetDamage(PDG.Player.instance.isRightNear ? PDG.Player.instance.damageRightNear : PDG.Player.instance.damageRight, hit[i].point);
                    if (hit[i].collider.GetComponent<Boss_NPC>()) hit[i].collider.GetComponent<Boss_NPC>().SetDamage(PDG.Player.instance.isRightNear ? PDG.Player.instance.damageRightNear : PDG.Player.instance.damageRight, hit[i].point);
                    if (hit[i].collider.GetComponent<Boss_Golem>()) hit[i].collider.GetComponent<Boss_Golem>().SetDamage(PDG.Player.instance.isRightNear ? PDG.Player.instance.damageRightNear : PDG.Player.instance.damageRight, hit[i].point);
                    if (hit[i].collider.GetComponent<Boss_Stone>()) hit[i].collider.GetComponent<Boss_Stone>().SetDamage(PDG.Player.instance.isRightNear ? PDG.Player.instance.damageRightNear : PDG.Player.instance.damageRight, hit[i].point, false);
                    if (PDG.Player.instance.isRightNear && PDG.Player.instance.isCriticalRight)
                    {
                        GameObject criEffect = ObjManager.Call().GetObject("CriticalHitEffect");
                        criEffect.transform.position = hit[i].point;
                        criEffect.SetActive(true);
                    }
                }
            }
        }
    }
    #region -- Tool --
    public static float GetAngle(Vector3 vStart, Vector3 vEnd)
    {
        Vector3 v = vEnd - vStart;

        return Mathf.Atan2(v.y, v.x) * Mathf.Rad2Deg;
    }
    public static Vector3 GetPosition(Vector3 vStart, float angle)
    {
        float x = Mathf.Cos(Mathf.Deg2Rad * angle) + vStart.x;
        float y = Mathf.Sin(Mathf.Deg2Rad * angle) + vStart.y;

        return new Vector3(x, y, 0);
    }
    private int GetPercentege(float _per, string[] list)
    {
        int[] per = new int[Mathf.FloorToInt(100 * list.Length)];
        for (int i = 0; i < per.Length; i++) per[i] = -1;

        for (int i = 0; i < list.Length; i++)
        {
            for (int j = 0; j < Mathf.FloorToInt(_per * 100); j++)
            {
                per[(i * 100) + j] = i;
            }
        }
        if (per.Length == 0) return -1;
        return per[Random.Range(0, per.Length)];
    }
    #endregion
}
