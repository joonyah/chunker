﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OctopusBallController : MonoBehaviour
{
    [SerializeField] private float speed = 10.0f;
    [SerializeField] private AudioSource audio;
    [SerializeField] private AudioSource audio2;
    // Start is called before the first frame update
    private void Awake()
    {
        float dis = Vector3.Distance(PDG.Player.instance.transform.position, transform.position);
        if (dis > 5)
        {
            audio.volume = 0;
            audio2.volume = 0;
        }
        float tVolume =  (SoundManager.isMute || (PDG.Dungeon.instance == null ? false : PDG.Dungeon.instance.isPause) || (PDG.Player.instance == null ? false : PDG.Player.instance.isDie)) ? 0 : SoundManager.EffectVolume;
        float t = tVolume / 5;
        audio.volume = tVolume - (dis * t);
        audio2.volume = tVolume - (dis * t);
    }
    void Start()
    {
        StartCoroutine(AttackRolling());
    }
    IEnumerator AttackRolling()
    {
        float hitAngle = 0.0f;
        float time = 10.0f;
        Vector3 _dir = PDG.Player.instance.transform.position - transform.position;
        _dir.Normalize();
        int c = 0;
        float hithitTime = 0.0f;
        while (time > 0.0f && c < 5)
        {
            hithitTime -= Time.deltaTime;
            time -= Time.deltaTime;
            RaycastHit2D ray = Physics2D.Raycast(transform.position, _dir, 1, 1 << 8);
            if (ray && hithitTime < 0.0f)
            {
                audio2.Play();
                float f = GetReflectAngle(ray.point, ray.collider.transform.position, _dir);
                while (f < 0)
                {
                    f += 360.0f;
                    if (f > 0) break;
                }
                while (f >= 360.0f)
                {
                    f -= 360.0f;
                    if (f < 360.0f) break;
                }
                _dir = GetPosition(ray.point, f);
                _dir -= (Vector3)ray.point;
                _dir.Normalize();
                hithitTime = 0.1f;
                c++;
            }
            Move(_dir, speed, 1);
            yield return new WaitForSeconds(Time.deltaTime);
        }
        Destroy(gameObject);
    }
    private void Move(Vector2 axisDirection, float _speed, float _dis)
    {
        transform.Translate(axisDirection * _speed * Time.deltaTime);
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.layer.Equals(27))
        {
            if (collision.name.Equals("Shield"))
            {
                PDG.Player.instance.right_defence_gauge -= 1.0f;
                Inventorys.Instance.GuageShaker(1);
                CameraShaker._instance.StartShake(0.2f, 0.02f, 0.2f);
            }
            if (collision.name.Contains("dna_mo_blade"))
            {
                PDG.Player.instance.left_defence_gauge -= 1.0f;
                Inventorys.Instance.GuageShaker(0);
                CameraShaker._instance.StartShake(0.2f, 0.02f, 0.2f);
            }
            Destroy(gameObject);
        }
        else if (collision.gameObject.layer.Equals(9))
        {
            if (collision.gameObject.GetComponent<PDG.Player>())
            {
                collision.gameObject.GetComponent<PDG.Player>().SetDamage(1, LocalizeManager.GetLocalize("monster_octopus_ball"), DEATH_TYPE.DEATH_NORMAL, "octopus_ball");
            }
        }
    }
    private void Update()
    {
        float dis = Vector3.Distance(PDG.Player.instance.transform.position, transform.position);
        if (dis > 5)
        {
            audio.volume = 0;
            audio2.volume = 0;
        }
        float tVolume =  (SoundManager.isMute || (PDG.Dungeon.instance == null ? false : PDG.Dungeon.instance.isPause) || (PDG.Player.instance == null ? false : PDG.Player.instance.isDie)) ? 0 : SoundManager.EffectVolume;
        float t = tVolume / 5;
        audio.volume = tVolume - (dis * t);
        audio2.volume = tVolume - (dis * t);
    }
    #region -- Tool --
    public static float GetAngle(Vector3 vStart, Vector3 vEnd)
    {
        Vector3 v = vEnd - vStart;

        return Mathf.Atan2(v.y, v.x) * Mathf.Rad2Deg;
    }
    public static Vector3 GetPosition(Vector3 vStart, float angle)
    {
        float x = Mathf.Cos(Mathf.Deg2Rad * angle) + vStart.x;
        float y = Mathf.Sin(Mathf.Deg2Rad * angle) + vStart.y;

        return new Vector3(x, y, 0);
    }
    private int GetPercentege(float _per, string[] list)
    {
        int[] per = new int[Mathf.FloorToInt(100 * list.Length)];
        for (int i = 0; i < per.Length; i++) per[i] = -1;

        for (int i = 0; i < list.Length; i++)
        {
            for (int j = 0; j < Mathf.FloorToInt(_per * 100); j++)
            {
                per[(i * 100) + j] = i;
            }
        }
        if (per.Length == 0) return -1;
        return per[Random.Range(0, per.Length)];
    }
    float GetReflectAngle(Vector3 _hitPoint, Vector3 _hitPosition, Vector3 _dir)
    {
        Vector3 _dir2 = _hitPoint - _hitPosition;
        _dir2.Normalize();

        if (_dir.x == 0 && _dir.y > 0)       // 위 
        {
            return 270.0f;
        }
        else if (_dir.x == 0 && _dir.y < 0)  // 아래
        {
            return 90.0f;
        }
        else if (_dir.x > 0 && _dir.y == 0)  // 오른쪽
        {
            return 180.0f;
        }
        else if (_dir.x < 0 && _dir.y == 0)  // 왼쪽
        {
            return 0.0f;
        }
        else if (_dir.x > 0 && _dir.y > 0)  // 오른쪽 위 
        {
            if (_dir2.x > 0)
            {
                float zz = GetAngle(_hitPoint, transform.position);
                float f = zz - 180.0f; // 입사각
                return 360.0f - f; // 반사각
            }
            else if (_dir2.y > 0)
            {
                float zz = GetAngle(_hitPoint, transform.position);
                float f = 270.0f - zz; // 입사각
                return 90.0f + f; // 반사각
            }
            else
            {
                float hitAngle = GetAngle(_hitPosition, _hitPoint);
                if (hitAngle > 225.0f)
                {
                    float zz = GetAngle(_hitPoint, transform.position);
                    float f = zz - 180.0f; // 입사각
                    return 360.0f - f; // 반사각
                }
                else if (hitAngle < 225.0f)
                {
                    float zz = GetAngle(_hitPoint, transform.position);
                    float f = 270.0f - zz; // 입사각
                    return 90.0f + f; // 반사각
                }
                else
                {
                    return 225.0f;
                }

            }
        }
        else if (_dir.x < 0 && _dir.y > 0)   // 왼쪽 위
        {
            if (_dir2.x < 0)
            {
                float zz = GetAngle(_hitPoint, transform.position);
                float f = 360.0f - zz; // 입사각
                return 180.0f + f; // 반사각
            }
            else if (_dir2.y > 0)
            {
                float zz = GetAngle(_hitPoint, transform.position);
                float f = zz - 270.0f; // 입사각
                return f; // 반사각
            }
            else
            {
                float hitAngle = GetAngle(_hitPosition, _hitPoint);
                if (hitAngle > 315.0f)
                {
                    float zz = GetAngle(_hitPoint, transform.position);
                    float f = zz - 270.0f; // 입사각
                    return f; // 반사각
                }
                else if (hitAngle < 315.0f)
                {
                    float zz = GetAngle(_hitPoint, transform.position);
                    float f = 360.0f - zz; // 입사각
                    return 180.0f + f; // 반사각
                }
                else
                {
                    return 315.0f;
                }

            }
        }
        else if (_dir.x < 0 && _dir.y < 0)   // 왼쪽 아래
        {
            if (_dir2.x < 0)
            {
                float zz = GetAngle(_hitPoint, transform.position);
                float f = zz; // 입사각
                return 180.0f - f; // 반사각
            }
            else if (_dir2.y < 0)
            {
                float zz = GetAngle(_hitPoint, transform.position);
                float f = 90.0f - zz; // 입사각
                return 270.0f + f; // 반사각
            }
            else
            {
                float hitAngle = GetAngle(_hitPosition, _hitPoint);
                if (hitAngle > 45.0f)
                {
                    float zz = GetAngle(_hitPoint, transform.position);
                    float f = zz; // 입사각
                    return 180.0f - f; // 반사각
                }
                else if (hitAngle < 45.0f)
                {
                    float zz = GetAngle(_hitPoint, transform.position);
                    float f = 90.0f - zz; // 입사각
                    return 270.0f + f; // 반사각
                }
                else
                {
                    return 45.0f;
                }

            }
        }
        else if (_dir.x > 0 && _dir.y < 0)   // 오른쪽 아래
        {
            if (_dir2.x > 0)
            {
                float zz = GetAngle(_hitPoint, transform.position);
                float f = 180.0f - zz; // 입사각
                return f; // 반사각
            }
            else if (_dir2.y < 0)
            {
                float zz = GetAngle(_hitPoint, transform.position);
                float f = zz - 90.0f; // 입사각
                return 270.0f - f; // 반사각
            }
            else
            {
                float hitAngle = GetAngle(_hitPosition, _hitPoint);
                if (hitAngle > 135.0f)
                {
                    float zz = GetAngle(_hitPoint, transform.position);
                    float f = zz - 90.0f; // 입사각
                    return 270.0f - f; // 반사각
                }
                else if (hitAngle < 135.0f)
                {
                    float zz = GetAngle(_hitPoint, transform.position);
                    float f = 180.0f - zz; // 입사각
                    return f; // 반사각
                }
                else
                {
                    return 135.0f;
                }
            }
        }
        else
        {
            return 0.0f;
        }
    }
    #endregion
}
