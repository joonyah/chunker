﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SnowballController : MonoBehaviour
{
    [SerializeField] private Rigidbody2D[] fragments;
    [SerializeField] private GameObject fragmentsObj;
    [SerializeField] private AudioSource audio;
    bool isEnd = false;
    // Update is called once per frame
    void Update()
    {
        float dis = Vector3.Distance(PDG.Player.instance.transform.position, transform.position);
        if (dis > 5) audio.volume = 0;
        float tVolume =  (SoundManager.isMute || (PDG.Dungeon.instance == null ? false : PDG.Dungeon.instance.isPause) || (PDG.Player.instance == null ? false : PDG.Player.instance.isDie)) ? 0 : SoundManager.EffectVolume;
        float t = tVolume / 5;
        audio.volume = tVolume - (dis * t);
        RaycastHit2D hit = Physics2D.CircleCast(transform.position, 0.5f, Vector2.zero, 0, 1 << 8 | 1 << 9);
        if(hit && !isEnd)
        {
            if (hit.collider.GetComponent<PDG.Player>()) hit.collider.GetComponent<PDG.Player>().SetDamage(1, LocalizeManager.GetLocalize("killedby_snowball"), DEATH_TYPE.DEATH_SUB_BOSS, "golem_ice");
            isEnd = true;
            fragmentsObj.SetActive(true);
            for (int i = 0; i < fragments.Length; i++)
            {
                fragments[i].AddForce(new Vector2(Random.Range(-1.0f, 1.0f), Random.Range(-1.0f, 1.0f)), ForceMode2D.Impulse);
                fragments[i].GetComponent<FragmentsController>().isStart = true;
                GetComponent<SpriteRenderer>().enabled = false;
            }
            Destroy(gameObject, 2.0f);
        }
    }
}
