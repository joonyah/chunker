﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlizzardController : MonoBehaviour
{
    bool isDestroy = false;
    Animator animator;
    AudioSource audio;
    SpriteRenderer sr;
    public float y;
    float dis;
    float a = 0.0f;
    float dTime = 3.0f;
    private void Start()
    {
        audio = GetComponent<AudioSource>();
        animator = GetComponent<Animator>();
        sr = GetComponent<SpriteRenderer>();
        sr.color = new Color(1, 1, 1, 0);
    }
    private void Update()
    {
        dTime -= Time.deltaTime;
        if (dTime <= 0.0f) 
        {
            DestroyEnd();
        }
        a += Time.deltaTime * 4;
        if (a > 1.0f) a = 1.0f;
        sr.color = new Color(1, 1, 1, a);
        if (transform.position.y <= y && !isDestroy)
        {
            isDestroy = true;
            animator.SetTrigger("Destroy");
            GetComponent<Rigidbody2D>().velocity = Vector2.zero;

            dis = Vector3.Distance(PDG.Player.instance.transform.position, transform.position);
            if (dis > 5) audio.volume = 0;
            float tVolume =  (SoundManager.isMute || (PDG.Dungeon.instance == null ? false : PDG.Dungeon.instance.isPause) || (PDG.Player.instance == null ? false : PDG.Player.instance.isDie)) ? 0 : SoundManager.EffectVolume;
            float t = tVolume / 5;
            audio.volume = tVolume - (dis * t);
            audio.Play();
        }
        RaycastHit2D hit = Physics2D.CapsuleCast(transform.position + new Vector3(-0.1f, -0.6f, 0), new Vector2(0.3f, 0.75f), CapsuleDirection2D.Vertical, 0, Vector2.zero, 0, 1 << 9);
        if (hit && isDestroy && a > 0.5f)  
        {
            isDestroy = true;
            if (hit.collider.GetComponent<PDG.Player>()) hit.collider.GetComponent<PDG.Player>().SetDamage(1, LocalizeManager.GetLocalize("killedby_blizzard"), DEATH_TYPE.DEATH_SUB_BOSS, "golem_ice");
            animator.SetTrigger("Destroy");
            GetComponent<Rigidbody2D>().velocity = Vector2.zero;

            dis = Vector3.Distance(PDG.Player.instance.transform.position, transform.position);
            if (dis > 5) audio.volume = 0;
            float tVolume =  (SoundManager.isMute || (PDG.Dungeon.instance == null ? false : PDG.Dungeon.instance.isPause) || (PDG.Player.instance == null ? false : PDG.Player.instance.isDie)) ? 0 : SoundManager.EffectVolume;
            float t = tVolume / 5;
            audio.volume = tVolume - (dis * t);
            audio.Play();
        }
    }
    public void DestroyEnd()
    {
        Destroy(gameObject);
    }
}
