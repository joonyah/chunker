﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class Boss_Stone : MonoBehaviour
{
    public MonstersData mobData;
    public bool isHit = false;
    [SerializeField] private MOB.MonsterInfo mobInfo;
    [SerializeField] private bool isInvincibility = false;
    [SerializeField] private bool isDie = false;
    [SerializeField] private float hitTime = 0.0f;
    [SerializeField] private SpriteRenderer sr;
    [SerializeField] private Animator animator;

    [SerializeField] private bool isFire = false;
    [SerializeField] private bool isIce = false;
    [SerializeField] private bool isMoveStop = false;

    [Header("Color")]
    [SerializeField] private Color HitColor;
    [SerializeField] private Color HitColor2;
    [SerializeField] private Color HitColor3;
    [SerializeField] private Color HitColor4;
    [SerializeField] private Color IceModeColor;
    [SerializeField] private Color FireModeColor;
    Coroutine HitCoroutine;
    PDG.Camera _camera;
    bool isWait = true;
    [SerializeField] private Image spakeImage;

    [Header("Pattern 1")]
    [SerializeField] private GameObject LaserObj;
    [SerializeField] private GameObject[] StoneBulletObjs;

    [Header("Pattern 2")]
    [SerializeField] private float recoveryTime = 1.0f;

    [Header("Pattern 3")]
    [SerializeField] private GameObject longLaserObj;

    [Header("Pattern 4")]
    [SerializeField] private bool isAttack = false;
    [SerializeField] private LineRenderer leftLine;
    [SerializeField] private LineRenderer rightLine;
    [SerializeField] private LineRenderer leftLine2;
    [SerializeField] private LineRenderer rightLine2;
    [SerializeField] private GameObject LeftLineEndEffect;
    [SerializeField] private GameObject RightLineEndEffect;
    [SerializeField] private GameObject LeftLineEndEffect2;
    [SerializeField] private GameObject RightLineEndEffect2;

    [SerializeField] private GameObject[] nextHole;
    [SerializeField] private BossItemBoxController[] ItemBoxs;
    [SerializeField] private GameObject dieEffect_0;
    [SerializeField] private GameObject dieEffect_1;


    [SerializeField] private GameObject LaserUpObj;
    [SerializeField] private GameObject LaserDownObj;


    [SerializeField] private int maxpattern = 4;
    [SerializeField] private int pattern;
    [SerializeField] private float pTime_1 = 10.0f;
    [SerializeField] private float pTime_2 = 10.0f;
    [SerializeField] private float pTime_3 = 10.0f;
    [SerializeField] private float pTime_4 = 10.0f;
    [SerializeField] private GameObject subCollision;
    List<GameObject> objList = new List<GameObject>();
    float firModeFireTime = 0.0f;
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position + new Vector3(0, -12), 25);
    }
    // Start is called before the first frame update
    void Start()
    {
        StageData sData = System.Array.Find(SheetManager.Instance.StageDB.dataArray, item => item.Roomid[0].Equals("d01_bossstage_04"));
        mobData = System.Array.Find(SheetManager.Instance.MonsterDB.dataArray, item => item.ID.Equals("boss_stone"));
        mobInfo = GetComponent<MOB.MonsterInfo>();
        if (PDG.Dungeon.instance.StageLevel == 0)
            mobInfo.maxHp = mobInfo.curHp = Random.Range(sData.Health[0], sData.Health[1]);
        else if (PDG.Dungeon.instance.StageLevel == 1)
            mobInfo.maxHp = mobInfo.curHp = Random.Range(sData.Health[0], sData.Health[1]) * 2;
        else if (PDG.Dungeon.instance.StageLevel == 2)
            mobInfo.maxHp = mobInfo.curHp = Random.Range(sData.Health[0], sData.Health[1]) * 3;
        else if (PDG.Dungeon.instance.StageLevel == 3)
            mobInfo.maxHp = mobInfo.curHp = Random.Range(sData.Health[0], sData.Health[1]) * 5;

        float hp = mobInfo.curHp;
        if (SaveAndLoadManager.nDifficulty == 0) mobInfo.maxHp = mobInfo.curHp = (hp / 2);
        else if (SaveAndLoadManager.nDifficulty == 1) mobInfo.maxHp = mobInfo.curHp = hp * 0.8f;
        else if (SaveAndLoadManager.nDifficulty == 2) mobInfo.maxHp = mobInfo.curHp = (hp * 2);
        else if (SaveAndLoadManager.nDifficulty == 3) mobInfo.maxHp = mobInfo.curHp = (hp * 4);

        _camera = FindObjectOfType(typeof(PDG.Camera)) as PDG.Camera;
        spakeImage = GameObject.Find("Canvas").transform.Find("Spake").GetComponent<Image>();
        isInvincibility = true;
        StartCoroutine(PatternAction());
    }
    // Update is called once per frame
    void Update()
    {
        if (isWait)
        {
            RaycastHit2D hit = Physics2D.CircleCast(transform.position + new Vector3(0, -12), 25, Vector2.zero, 0, 1 << 9);
            if (hit)
            {
                SoundManager.instance.StartAudio(new string[1] { "BGM/Boss_3" }, VOLUME_TYPE.BGM, null, true);
                isWait = false;
            }
        }
        if (firModeFireTime > 0)
        {
            firModeFireTime -= Time.deltaTime;
        }

        if (isIce && HitCoroutine == null)
            sr.color = IceModeColor;
        else if (isFire && HitCoroutine == null)
            sr.color = FireModeColor;
        else if (!isFire && !isIce && HitCoroutine == null)
            sr.color = Color.white;

        if (hitTime > 0.0f)
        {
            hitTime -= Time.deltaTime;
        }
        else if (hitTime <= 0.0f)
        {
            isHit = false;
        }
        if (PDG.Dungeon.instance.StageLevel > 1)
        {
            if (LaserObj.activeSelf) 
            {
                LineRenderer[] laserRender = LaserObj.GetComponentsInChildren<LineRenderer>();
                for (int i = 0; i < laserRender.Length; i++) 
                {
                    for (int j = 0; j < laserRender[i].positionCount; j++)
                    {
                        Vector3 pos = laserRender[i].GetPosition(j);
                        if (pattern == 2)
                        {
                            pos.y -= Time.deltaTime * 3;

                            if (pos.y < LaserObj.transform.position.y - 10.0f)
                            {
                                pos.y = LaserObj.transform.position.y + 20.0f;
                            }
                        }
                        else
                        {
                            pos.y += Time.deltaTime * 3;

                            if (pos.y > LaserObj.transform.position.y + 20.0f)
                            {
                                pos.y = LaserObj.transform.position.y - 10.0f;
                            }
                        }
                        laserRender[i].SetPosition(j, pos);
                    }
                }
            }
        }

    }
    IEnumerator MoveCoroutine()
    {
        bool isLeft = false;
        bool isUpDown = false;
        while(!isDie)
        {
            if (!isMoveStop && !isDie)
            {
                Vector3 nPos = transform.localPosition;
                if (isLeft)
                {
                    if (nPos.x <= -8.0f) isLeft = false;
                    transform.Translate(Vector3.left * 10 * Time.deltaTime);
                }
                else
                {
                    if (nPos.x >= 8.0f) isLeft = true;
                    transform.Translate(Vector3.right * 10 * Time.deltaTime);
                }

                if (isUpDown)
                {
                    if (nPos.y < 15.75f) isUpDown = false;
                    transform.Translate(Vector3.down * 4 * Time.deltaTime);
                }
                else
                {
                    if (nPos.y > 16.75f) isUpDown = true;
                    transform.Translate(Vector3.up * 4 * Time.deltaTime);
                }
                if (Random.Range(0, 300) == 185) isLeft = !isLeft;
            }
            yield return new WaitForSeconds(Time.deltaTime);
        }
    }
    IEnumerator PatternAction()
    {
        while (isWait) yield return null;
        PDG.Player.instance.isCameraOff = true;
        CameraShaker._instance.isDontMoveCamera = true;
        _camera.target = transform.parent;
        float cSize = 7;
        while (!isDie)
        {
            cSize += Time.deltaTime * 4;
            Camera.main.orthographicSize = cSize;
            if (cSize >= 20)
            {
                cSize = 20;
                Camera.main.orthographicSize = cSize;
                break;
            }
            yield return new WaitForSeconds(Time.deltaTime / 4);
        }
        string tId = mobData.ID;
        if (tId.Contains("_t")) tId = tId.Substring(0, tId.Length - 3);
        BossHpController.instance.BossStart(LocalizeManager.GetLocalize("monster_" + tId), GetComponent<MOB.MonsterInfo>());
        yield return new WaitForSeconds(1.0f);
        while (!isDie)
        {
            cSize -= Time.deltaTime * 4;
            Camera.main.orthographicSize = cSize;
            if (cSize <= 12)
            {
                cSize = 12;
                Camera.main.orthographicSize = cSize;
                break;
            }
            yield return new WaitForSeconds(Time.deltaTime / 4);
        }
        _camera.target = PDG.Player.instance.transform;
        PDG.Player.instance.isCameraOff = false;
        CameraShaker._instance.isDontMoveCamera = false;
        yield return new WaitForSeconds(3.0f);
        isInvincibility = false;
        StateGroup.instance.isBattleTimeStart = true;

        pattern = Random.Range(0, maxpattern);

        float longLaserTime = 0.0f;
        float icebulletTime = 0.0f;
        StartCoroutine(MoveCoroutine());
        while(!isDie)
        {
            if (pattern == 0 && !isDie)
            {
                if (transform.Find("HealingFieldGreen").gameObject.activeSelf) transform.Find("HealingFieldGreen").gameObject.SetActive(false);
                pTime_1 -= Time.deltaTime;
                if (pTime_1 <= 0.0f)
                {
                    while (true)
                    {
                        pattern = Random.Range(0, maxpattern);
                        if (pattern != 0) break;
                    }
                    pTime_1 = 10.0f;
                    yield return StartCoroutine(Spake());
                }
                if (Random.Range(0, 200) % 4 == 0)
                {
                    GameObject obj =  Instantiate(StoneBulletObjs[Random.Range(0, StoneBulletObjs.Length)]);
                    obj.transform.position = transform.position;
                    Vector3 _pos = GetPosition(transform.position, Random.Range(190, 350));
                    Vector3 _dir = _pos - transform.position;
                    _dir.Normalize();
                    obj.GetComponent<Rigidbody2D>().AddForce(_dir * 10, ForceMode2D.Impulse);
                }
                if (!LaserObj.activeSelf) LaserObj.SetActive(true);
                if (isFire) isFire = false;
                if (isIce) isIce = false;
                if (animator.GetBool("Fire")) animator.SetBool("Fire", false);
                if (animator.GetBool("Ice")) animator.SetBool("Ice", false);
            }
            else if (pattern == 1 && !isDie)
            {
                isMoveStop = true;
                GameObject.Find("Canvas").transform.Find("TutorialText").gameObject.SetActive(true);
                GameObject.Find("Canvas").transform.Find("TutorialText").GetComponent<Text>().text = LocalizeManager.GetLocalize("boss_health_guide");
                if (!transform.Find("HealingFieldGreen").gameObject.activeSelf) transform.Find("HealingFieldGreen").gameObject.SetActive(true);
                recoveryTime -= Time.deltaTime;
                pTime_2 -= Time.deltaTime;
                if (recoveryTime <= 0.0f)
                {
                    mobInfo.curHp += ((mobInfo.maxHp / 100) * 2) + (((mobInfo.maxHp / 100) * 2) * (PDG.Dungeon.instance.StageLevel * 0.3f));
                    recoveryTime = 1.0f;
                }
                if (LaserObj.activeSelf) LaserObj.SetActive(false);
                if (!isFire)
                {
                    isFire = true;
                    isIce = false;
                    animator.SetBool("Ice", false);
                    animator.SetBool("Fire", true);
                }
                if (pTime_2 <= 0.0f)
                {
                    isMoveStop = false;
                    GameObject.Find("Canvas").transform.Find("TutorialText").gameObject.SetActive(false);
                    while (true)
                    {
                        pattern = Random.Range(0, maxpattern);
                        if (pattern != 1) break;
                    }
                    pTime_2 = 10.0f;
                    yield return StartCoroutine(Spake());
                }
            }
            else if (pattern == 2 && !isDie)
            {
                if (transform.Find("HealingFieldGreen").gameObject.activeSelf) transform.Find("HealingFieldGreen").gameObject.SetActive(false);
                if (LaserObj.activeSelf) LaserObj.SetActive(false);
                pTime_3 -= Time.deltaTime;
                longLaserTime -= Time.deltaTime;
                icebulletTime -= Time.deltaTime;
                if (!isIce && !isDie)
                {
                    isFire = false;
                    isIce = true;
                    animator.SetBool("Ice", true);
                    animator.SetBool("Fire", false);
                }
                if (icebulletTime <= 0.0f && !isDie)
                {
                    if (PDG.Dungeon.instance.StageLevel > 0)
                    {
                        icebulletTime = Random.Range(0.6f, 1.6f);
                        StartCoroutine(FirePiko());
                    }
                }
                if (longLaserTime <= 0.0f)
                {
                    longLaserTime = Random.Range(2.4f, 3.0f);
                    StartCoroutine(LongLaser());
                }
                if (pTime_3 <= 0.0f)
                {
                    while (true)
                    {
                        pattern = Random.Range(0, maxpattern);
                        if (pattern != 2) break;
                    }
                    pTime_3 = 10.0f;
                    longLaserTime = 0.0f;
                    icebulletTime = 0.0f;
                    yield return StartCoroutine(Spake());
                }
            }
            else if (pattern == 3)
            {
                isMoveStop = true;
                if (transform.Find("HealingFieldGreen").gameObject.activeSelf) transform.Find("HealingFieldGreen").gameObject.SetActive(false);
                if (PDG.Dungeon.instance.StageLevel > 1)
                {
                    if (!LaserObj.activeSelf) LaserObj.SetActive(true);
                }
                else
                {
                    if (LaserObj.activeSelf) LaserObj.SetActive(false);
                }
                if (isFire) isFire = false;
                if (isIce) isIce = false;
                if (animator.GetBool("Fire")) animator.SetBool("Fire", false);
                if (animator.GetBool("Ice")) animator.SetBool("Ice", false);
                if (!isAttack && !isDie)
                {
                    isAttack = true;
                    animator.SetTrigger("AttackTrigger");
                    yield return new WaitForSeconds(2.0f);

                    StartCoroutine(UpLaserCoroutine(0));
                    StartCoroutine(UpLaserCoroutine(1));
                    StartCoroutine(DownLaserCoroutine(0));
                    StartCoroutine(DownLaserCoroutine(1));

                    // 눈에서 빔
                    {
                        //Vector3 leftEnd = leftLine.transform.position;
                        //Vector3 rightEnd = rightLine.transform.position;
                        //Vector3 leftEnd2 = leftLine2.transform.position;
                        //Vector3 rightEnd2 = rightLine2.transform.position;
                        //
                        //Vector3 leftpPos = PDG.Player.instance.transform.position;
                        //Vector3 leftpPos2 = PDG.Player.instance.transform.position;
                        //float Leftt = 0.0f;
                        //float angle_right = Random.Range(190, 330);
                        //bool isRightControl = Random.Range(0, 100) % 2 == 0 ? true : false;
                        //
                        //float Leftt2 = 0.0f;
                        //float angle_right2 = Random.Range(190, 330);
                        //bool isRightControl2 = Random.Range(0, 100) % 2 == 0 ? true : false;
                        //while (isAttack && !isDie)
                        //{
                        //    pTime_4 -= Time.deltaTime;
                        //    if (pTime_4 <= 0.0f)
                        //    {
                        //        while (true)
                        //        {
                        //            pattern = Random.Range(0, maxpattern);
                        //            if (pattern != 3) break;
                        //        }
                        //        animator.SetTrigger("AttackEnd");
                        //        leftLine.gameObject.SetActive(false);
                        //        rightLine.gameObject.SetActive(false);
                        //        LeftLineEndEffect.SetActive(false);
                        //        RightLineEndEffect.SetActive(false);
                        //
                        //        leftLine2.gameObject.SetActive(false);
                        //        rightLine2.gameObject.SetActive(false);
                        //        LeftLineEndEffect2.SetActive(false);
                        //        RightLineEndEffect2.SetActive(false);
                        //        isAttack = false;
                        //        pTime_4 = 10.0f;
                        //        yield return StartCoroutine(Spake());
                        //        break;
                        //    }
                        //    leftLine.gameObject.SetActive(true);
                        //    rightLine.gameObject.SetActive(true);
                        //    LeftLineEndEffect.SetActive(true);
                        //    RightLineEndEffect.SetActive(true);
                        //
                        //    leftLine2.gameObject.SetActive(true);
                        //    rightLine2.gameObject.SetActive(true);
                        //    LeftLineEndEffect2.SetActive(true);
                        //    RightLineEndEffect2.SetActive(true);
                        //
                        //
                        //    Leftt += Time.deltaTime * Random.Range(2, 5);
                        //    if (Leftt > 1.0f)
                        //    {
                        //        Leftt = 0.0f;
                        //        leftEnd = leftpPos;
                        //        leftpPos = PDG.Player.instance.transform.position;
                        //    }
                        //
                        //    Leftt2 += Time.deltaTime * Random.Range(2, 5);
                        //    if (Leftt2 > 1.0f)
                        //    {
                        //        Leftt2 = 0.0f;
                        //        leftEnd2 = leftpPos2;
                        //        leftpPos2 = PDG.Player.instance.transform.position;
                        //    }
                        //
                        //    Vector3 leftEnds = Vector3.Lerp(leftEnd, leftpPos, Leftt);
                        //    Vector3 dir = leftEnds - leftLine.transform.position;
                        //    dir.Normalize();
                        //
                        //    Vector3 leftEnds2 = Vector3.Lerp(leftEnd2, leftpPos2, Leftt2);
                        //    Vector3 dir2 = leftEnds2 - leftLine2.transform.position;
                        //    dir2.Normalize();
                        //
                        //    RaycastHit2D hitLeft = Physics2D.BoxCast(leftLine.transform.position, new Vector2(0.5f, 0.1f), GetAngle(leftLine.transform.position, leftLine.transform.position + dir), dir,
                        //        Vector3.Distance(leftLine.transform.position, leftEnds), 1 << 8 | 1 << 9);
                        //    leftLine.positionCount = 2;
                        //    leftLine.SetPosition(0, leftLine.transform.position);
                        //    LeftLineEndEffect.transform.eulerAngles = new Vector3(0, 0, GetAngle(leftLine.transform.position, leftLine.GetPosition(1)) + 90);
                        //
                        //    RaycastHit2D hitLeft2 = Physics2D.BoxCast(leftLine2.transform.position, new Vector2(0.5f, 0.1f), GetAngle(leftLine2.transform.position, leftLine2.transform.position + dir2), dir2,
                        //        Vector3.Distance(leftLine2.transform.position, leftEnds2), 1 << 8 | 1 << 9);
                        //    leftLine2.positionCount = 2;
                        //    leftLine2.SetPosition(0, leftLine2.transform.position);
                        //    LeftLineEndEffect2.transform.eulerAngles = new Vector3(0, 0, GetAngle(leftLine2.transform.position, leftLine2.GetPosition(1)) + 90);
                        //
                        //    if (hitLeft)
                        //    {
                        //        leftLine.SetPosition(1, hitLeft.point);
                        //        LeftLineEndEffect.transform.position = hitLeft.point;
                        //    }
                        //    else
                        //    {
                        //        leftLine.SetPosition(1, leftEnds);
                        //        LeftLineEndEffect.transform.position = leftLine.GetPosition(1);
                        //    }
                        //
                        //    if (hitLeft2)
                        //    {
                        //        leftLine2.SetPosition(1, hitLeft2.point);
                        //        LeftLineEndEffect2.transform.position = hitLeft2.point;
                        //    }
                        //    else
                        //    {
                        //        leftLine2.SetPosition(1, leftEnds2);
                        //        LeftLineEndEffect2.transform.position = leftLine2.GetPosition(1);
                        //    }
                        //
                        //    if (!isRightControl)
                        //    {
                        //        angle_right += Time.deltaTime * Random.Range(32, 40);
                        //        if (angle_right > 330)
                        //        {
                        //            angle_right = 330;
                        //            isRightControl = true;
                        //        }
                        //    }
                        //    else
                        //    {
                        //        angle_right -= Time.deltaTime * Random.Range(32, 40);
                        //        if (angle_right < 190)
                        //        {
                        //            angle_right = 190;
                        //            isRightControl = false;
                        //        }
                        //    }
                        //
                        //    if (!isRightControl2)
                        //    {
                        //        angle_right2 += Time.deltaTime * Random.Range(32, 40);
                        //        if (angle_right2 > 330)
                        //        {
                        //            angle_right2 = 330;
                        //            isRightControl2 = true;
                        //        }
                        //    }
                        //    else
                        //    {
                        //        angle_right2 -= Time.deltaTime * Random.Range(32, 40);
                        //        if (angle_right2 < 190)
                        //        {
                        //            angle_right2 = 190;
                        //            isRightControl2 = false;
                        //        }
                        //    }
                        //    Vector3 pos = GetPosition(rightLine.transform.position, angle_right);
                        //    pos = pos - rightLine.transform.position;
                        //    pos.Normalize();
                        //
                        //    Vector3 pos2 = GetPosition(rightLine2.transform.position, angle_right2);
                        //    pos2 = pos2 - rightLine2.transform.position;
                        //    pos2.Normalize();
                        //    RaycastHit2D hit = Physics2D.BoxCast(rightLine.transform.position, new Vector2(0.5f, 0.1f), angle_right, pos, 40, 1 << 8 | 1 << 9);
                        //    RaycastHit2D hit2 = Physics2D.BoxCast(rightLine2.transform.position, new Vector2(0.5f, 0.1f), angle_right2, pos2, 40, 1 << 8 | 1 << 9);
                        //    if (hit)
                        //        rightEnd = hit.point;
                        //    else
                        //        rightEnd = rightLine.transform.position + pos * 40;
                        //
                        //    if (hit2)
                        //        rightEnd2 = hit2.point;
                        //    else
                        //        rightEnd2 = rightLine2.transform.position + pos2 * 40;
                        //
                        //    RightLineEndEffect.transform.eulerAngles = new Vector3(0, 0, angle_right + 90);
                        //    RightLineEndEffect.transform.position = rightEnd;
                        //    rightLine.positionCount = 2;
                        //    rightLine.SetPosition(0, rightLine.transform.position);
                        //    rightLine.SetPosition(1, rightEnd);
                        //
                        //
                        //    RightLineEndEffect2.transform.eulerAngles = new Vector3(0, 0, angle_right2 + 90);
                        //    RightLineEndEffect2.transform.position = rightEnd2;
                        //    rightLine2.positionCount = 2;
                        //    rightLine2.SetPosition(0, rightLine2.transform.position);
                        //    rightLine2.SetPosition(1, rightEnd2);
                        //    yield return new WaitForSeconds(Time.deltaTime);
                        //}
                    }
                }
                else
                {
                    if (pTime_4 <= 0.0f)
                    {
                        isMoveStop = false;
                        while (true)
                        {
                            pattern = Random.Range(0, maxpattern);
                            if (pattern != 3) break;
                        }
                        animator.SetTrigger("AttackEnd");
                        isAttack = false;
                        pTime_4 = 10.0f;
                        yield return StartCoroutine(Spake());
                    }
                    else
                        pTime_4 -= Time.deltaTime;
                }
            }
            yield return new WaitForSeconds(Time.deltaTime);
        }

    }
    IEnumerator LongLaser()
    {
        int fireCountUp = Random.Range(16, 32);
        for (int j = 0; j < 3; j++)
        {
            if (isDie) break;
            for (int i = 0; i < fireCountUp; i++)
            {
                if (isDie) break;
                float angle = (360.0f / 16.0f) * i;
                GameObject obj = Instantiate(longLaserObj);
                Rigidbody2D rigid = obj.GetComponent<Rigidbody2D>();
                obj.transform.eulerAngles = new Vector3(0, 0, angle);
                Vector3 dir = GetPosition(transform.position, angle) - transform.position;
                dir.Normalize();
                obj.transform.position = transform.position + (dir * 4);
                rigid.AddForce(dir * 10, ForceMode2D.Impulse);
            }
            if (isDie) break;
            yield return new WaitForSeconds(0.6f);
        }
    }
    IEnumerator FirePiko()
    {
        string tId = mobData.ID;
        if (tId.Contains("_t")) tId = tId.Substring(0, tId.Length - 3);

        Vector3[] dirs;
        List<GameObject> bulletList = new List<GameObject>();
        Vector3 nPos = transform.position;
        for (int i = 0; i < 8; i++)
        {
            float angle = (360.0f / 8.0f) * i;

            Vector3 pos = GetPosition(nPos, angle);
            Vector3 dir = pos - nPos;
            dir.Normalize();
            GameObject obj = ObjManager.Call().GetObject("Bullet");
            obj.GetComponent<BulletController>().firePosition = transform.position;
            obj.GetComponent<BulletController>().isMonster = true;
            obj.GetComponent<BulletController>().monsterName = tId;
            obj.GetComponent<BulletController>().bulletType = "Monster_Bullet";
            obj.GetComponent<BulletController>().fSpeed = mobData.Bulletspeed;
            obj.GetComponent<BulletController>().fAngle = 0;
            obj.GetComponent<BulletController>().dir = Vector2.zero;
            obj.GetComponent<BulletController>().fRange = 20;
            obj.GetComponent<BulletController>().isRightAttack = false;
            obj.transform.position = pos;
            obj.transform.localEulerAngles = Vector3.zero;
            obj.SetActive(true);
            obj.GetComponent<BulletController>().BulletSetting();
            bulletList.Add(obj);
            yield return new WaitForSeconds(0.05f);
        }
        dirs = new Vector3[bulletList.Count];
        for (int i = 0; i < bulletList.Count; i++)
        {
            float angle = (360.0f / 8.0f) * i;
            Vector3 pos = GetPosition(nPos, angle);
            Vector3 dir = pos - nPos;
            dir.Normalize();
            dirs[i] = dir;
        }
        float t = 0.0f;
        while (true)
        {
            for (int i = 0; i < bulletList.Count; i++)
            {
                bulletList[i].transform.Translate(dirs[i] * 10 * Time.deltaTime);
            }
            t += Time.deltaTime;
            if (t >= 0.2f) break;
            yield return new WaitForSeconds(Time.deltaTime);
        }
        yield return new WaitForSeconds(0.2f);
        Vector3 dir2 = PDG.Player.instance.transform.position - nPos;
        dir2.Normalize();
        for (int i = 0; i < bulletList.Count; i++)
        {
            if (!bulletList[i].activeSelf) continue;
            bulletList[i].GetComponent<BulletController>().dir = dir2;
            bulletList[i].GetComponent<BulletController>().fSpeed = 10;
            bulletList[i].GetComponent<BulletController>().BulletFire();
        }
    }
    IEnumerator UpLaserCoroutine(int type)
    {
        yield return new WaitForSeconds(Random.Range(0.2f, 0.5f));
        while (pTime_4 > 0 && !isDie && isAttack)
        {
            GameObject obj = Instantiate(LaserDownObj);
            obj.GetComponent<AttackBullet>().ownerObj = transform.parent.gameObject;
            obj.transform.position = type == 0 ? leftLine.transform.position : rightLine.transform.position;
            yield return new WaitForSeconds(0.5f);
        }
    }
    IEnumerator DownLaserCoroutine(int type)
    {
        yield return new WaitForSeconds(Random.Range(0.2f, 0.5f));
        while (pTime_4 > 0 && !isDie && isAttack)
        {
            GameObject obj = Instantiate(LaserDownObj);
            obj.GetComponent<AttackBullet>().ownerObj = transform.parent.gameObject;
            obj.transform.position = type == 0 ? leftLine2.transform.position : rightLine2.transform.position;
            yield return new WaitForSeconds(0.3f);
        }
    }
    public void SetDamage(float _damage, Vector3 _pos, bool isQ)
    {
        if (PDG.Dungeon.instance.isPause) return;
        if (isInvincibility) return;
        if (isDie) return;
        string tId = mobData.ID;
        if (tId.Contains("_t")) tId = tId.Substring(0, tId.Length - 3);
        if (isFire && firModeFireTime <= 0.0f)
        {
            firModeFireTime = 0.5f;
            for (int i = 0; i < 5; i++)
            {
                Vector3 s = PDG.Player.instance.transform.position;
                s = new Vector3(s.x + Random.Range(-5, 5), s.y + Random.Range(-5, 5));
                Vector3 fireDir = s - transform.position;
                fireDir.Normalize();

                GameObject obj = ObjManager.Call().GetObject("Bullet");
                obj.GetComponent<BulletController>().firePosition = transform.position;
                obj.GetComponent<BulletController>().isMonster = true;
                obj.GetComponent<BulletController>().monsterName = tId;
                obj.GetComponent<BulletController>().bulletType = "Monster_Bullet";
                obj.GetComponent<BulletController>().fSpeed = 6;
                obj.GetComponent<BulletController>().fAngle = 0;
                obj.GetComponent<BulletController>().dir = fireDir;
                obj.GetComponent<BulletController>().fRange = 50;
                obj.GetComponent<BulletController>().isRightAttack = false;
                obj.transform.position = transform.position;
                obj.SetActive(true);
                obj.GetComponent<BulletController>().BulletFire();
            }
        }
        if (pattern == 1 && isQ)
        {
            pTime_2 = 0.0f;
            recoveryTime = 1.0f;
        }

        GameObject damageObjOrigin = Resources.Load<GameObject>("DamageText");
        GameObject damageObj = Instantiate(damageObjOrigin);
        damageObj.transform.SetParent(GameObject.Find("Canvas").transform);
        damageObj.transform.SetSiblingIndex(16);
        damageObj.transform.localScale = Vector3.one;
        damageObj.GetComponent<DamageText>().InitSetting(_damage, gameObject);
        mobInfo.curHp -= _damage;
        AudioSource audio = GetComponent<AudioSource>();
        float dis = Vector3.Distance(PDG.Player.instance.transform.position, transform.position);
        if (dis > 10) audio.volume = 0;
        if ( (SoundManager.isMute || (PDG.Dungeon.instance == null ? false : PDG.Dungeon.instance.isPause) || (PDG.Player.instance == null ? false : PDG.Player.instance.isDie)))
        {
            audio.volume = 0;
        }
        else
        {
            float tVolume = SoundManager.EffectVolume;
            float t = tVolume / 10;
            audio.volume = tVolume - (dis * t);
        }
        GetComponent<AudioSource>().Play();
        GameObject obj2 = ObjManager.Call().GetObject("DamageHit_Boss3");
        if (obj2)
        {
            obj2.transform.position = _pos;
            obj2.SetActive(true);
        }

        if (!isHit)
        {
            isHit = true;
            hitTime = 0.25f;
        }
        if (HitCoroutine == null) HitCoroutine = StartCoroutine(HitEffect());

        if (mobInfo.curHp <= 0)
        {
            PDG.Dungeon.instance.isBossRushStart = false;
            StateGroup.instance.isClearTimeStart = false;
            StateGroup.instance.isBattleTimeStart = false;
            SaveAndLoadManager.instance.SaveCollection(new CollectionData { _id = tId, _isOpen = true, _isWeapon = false });
            SoundManager.instance.StopAllSound();
            leftLine.gameObject.SetActive(false);
            rightLine.gameObject.SetActive(false);
            LeftLineEndEffect.SetActive(false);
            RightLineEndEffect.SetActive(false);

            leftLine2.gameObject.SetActive(false);
            rightLine2.gameObject.SetActive(false);
            LeftLineEndEffect2.SetActive(false);
            RightLineEndEffect2.SetActive(false);

            UbhObjectPool.instance.ReleaseAllBullet();
            ResultPanelScript.instance.bossList.Add(3);
            ScreenShot.instance.Screenshot(gameObject);
            isDie = true;
            PDG.Player.instance.fInvincibleTime_offensive_01 = 10;
            GetComponent<PolygonCollider2D>().enabled = false;
            subCollision.SetActive(false);
            StartCoroutine(DieAction());
        }
    }
    IEnumerator HitEffect()
    {
        while (isHit && !isDie)
        {
            sr.color = HitColor;
            yield return new WaitForSeconds(0.06f);
            sr.color = HitColor2;
            yield return new WaitForSeconds(0.06f);
            sr.color = HitColor3;
            yield return new WaitForSeconds(0.06f);
            sr.color = HitColor4;
            yield return new WaitForSeconds(0.06f);
            sr.color = Color.white;
        }
        HitCoroutine = null;
    }
    IEnumerator DieAction()
    {
        if (GetComponent<PolygonCollider2D>()) Destroy(GetComponent<PolygonCollider2D>());
        LaserObj.SetActive(false);
        dieEffect_0.SetActive(true);
        dieEffect_1.SetActive(true);
        for (int i = 0; i < objList.Count; i++)
        {
            if(objList[i])
            {
                Destroy(objList[i]);
            }
        }
        SoundManager.instance.StartAudio(new string[1] { "Monster/boss_shaker" }, VOLUME_TYPE.EFFECT);
        yield return new WaitForSecondsRealtime(0.3f);
        Time.timeScale = 0.1f;
        yield return new WaitForSecondsRealtime(2.0f);
        Time.timeScale = 1.0f;
        CameraShaker._instance.StartShake(0.1f, Time.deltaTime, 2.5f, true);
        Image spake = GameObject.Find("Canvas").transform.Find("Spake").GetComponent<Image>();
        float a = 0.0f;
        while (true)
        {
            spake.color = new Color(1, 1, 1, a);
            a += Time.deltaTime;
            if (a >= 1.0f) break;
            yield return new WaitForSeconds(Time.deltaTime);
        }
        spake.color = new Color(1, 1, 1, 1);
        SoundManager.instance.StartAudio(new string[3] { "Boss/DistantBigExplosion2", "Boss/DistantBigExplosion3", "Boss/DistantBigExplosion4" }, VOLUME_TYPE.EFFECT);
        SoundManager.instance.StartAudio(new string[3] { "Boss/DistantBigExplosion2", "Boss/DistantBigExplosion3", "Boss/DistantBigExplosion4" }, VOLUME_TYPE.EFFECT);
        yield return new WaitForSeconds(1.0f);
        spake.color = new Color(1, 1, 1, 0);
        animator.SetTrigger("DieTrigger");
        PDG.Player.instance.isCameraOff = true;
        (FindObjectOfType(typeof(PDG.Camera)) as PDG.Camera).target = transform;
        StartCoroutine(EndEventCoroutine());
    }
    IEnumerator EndEventCoroutine()
    {
        string tId = mobData.ID;
        if (tId.Contains("_t")) tId = tId.Substring(0, tId.Length - 3);
        if (PDG.Dungeon.instance.isBossRush)
        {
            List<string> dropItems = new List<string>();
            int skullCount = 10 * (PDG.Dungeon.instance.StageLevel + 1);
            for (int i = 0; i < skullCount; i++) dropItems.Add("glowing_skull");
            dropItems.Add("recovery_syringe_2");
            dropItems.Add("recovery_syringe_2");
            dropItems.Add("recovery_syringe_2");
            for (int i = 0; i < ItemBoxs.Length; i++)
            {
                ItemBoxs[i].gameObject.SetActive(true);
                ItemBoxs[i]._id = tId;
                if (ItemBoxs[i].isMaterial)
                {
                    ItemBoxs[i].gameObject.SetActive(false);
                }
                else
                {
                    ItemBoxs[i].itemName = dropItems.ToArray();
                }
            }
        }
        else
        {
            int ata = 0;
            List<ItemsData> iDatas = null;
            while (true)
            {
                ChestPerData data = System.Array.Find(SheetManager.Instance.ChestPerDB.dataArray, item => item.Stagelevel.Equals(PDG.Dungeon.instance.StageLevel));
                string BoxGrade = data.Chest[SaveAndLoadManager.GetBoxGrade(data)];
                BoxGrade = BoxGrade.Substring(BoxGrade.Length - 1, 1);
                iDatas = System.Array.FindAll(SheetManager.Instance.ItemsDB.dataArray, item => item.Grade.ToUpper().Equals(BoxGrade.ToUpper())).ToList();
                for (int i = 0; i < iDatas.Count; i++)
                {
                    Sprite s = Resources.Load<Sprite>("Item/" + iDatas[i].Type1 + "/" + iDatas[i].Type2 + "/" + iDatas[i].ID);
                    if (s == null || !Inventorys.Instance.UnlockAndMaxCountCheck(iDatas[i].ID))
                    {
                        iDatas.RemoveAt(i);
                        i--;
                        continue;
                    }
                }
                ata++;
                if (ata >= 1000) break;
                if (iDatas.Count > 0) break;
            }
            List<string> dropItems = new List<string>();
            if (iDatas.Count > 0) dropItems.Add(iDatas[UnityEngine.Random.Range(0, iDatas.Count)].ID);
            int skullCount = UnityEngine.Random.Range(10, 20);
            if (SaveAndLoadManager.nDifficulty != 0)
            {
                skullCount *= (SaveAndLoadManager.nDifficulty * 2) - 1;
            }
            for (int i = 0; i < skullCount; i++) dropItems.Add("glowing_skull");
            //dropItems.Add("key_normal");
            dropItems.Add("muscle_enhancer_body");
            //int t = SaveAndLoadManager.instance.LoadItem("Boss_eyes_spirit");
            //if (t == -1) dropItems.Add("eyes_spirit");
            if (UnityEngine.Random.Range(0, 100) < 50) dropItems.Add("muscle_enhancer_body");

            yield return new WaitForSeconds(1.0f);
            for (int i = 0; i < ItemBoxs.Length; i++)
            {
                ItemBoxs[i].gameObject.SetActive(true);
                ItemBoxs[i]._id = tId;
                if (ItemBoxs[i].isMaterial)
                {
                    int ran = Random.Range(1, 4);
                    int ran2 = Random.Range(0, 3);
                    int ran3 = Random.Range(0, 3);
                    string[] ttt = new string[1 + ran];
                    for (int j = 0; j < 1 + ran; j++)
                    {
                        if (j == 0) ttt[j] = "boss_Boss_Stone";
                        else ttt[j] = "normal_" + (ran2 == 0 ? "Cells" + (ran3 == 0 ? "_0" : "_1") : (ran2 == 1 ? "Ore" : "Parts" + (ran3 == 0 ? "_0" : (ran3 == 1 ? "_1" : "_2"))));
                    }
                    ItemBoxs[i].itemName = ttt;
                }
                else
                {
                    ItemBoxs[i].itemName = dropItems.ToArray();
                }
            }
            yield return new WaitForSeconds(1.0f);

            if (SteamStatsAndAchievements.instance != null) SteamStatsAndAchievements.instance.SettingStat(tId + "_hunting");
            if (SteamStatsAndAchievements.instance != null) SteamStatsAndAchievements.instance.SettingStat("stat_stage_clear_" + PDG.Dungeon.instance.StageLevel);
            if (SteamStatsAndAchievements.instance != null) SteamStatsAndAchievements.instance.SettingStat("stat_hunting_boss_count");

            List<AchievementData> savedatas = new List<AchievementData>();
            AchievementsData aData_0 = System.Array.Find(SheetManager.Instance.AchievementsDB.dataArray, item => item.ID.Equals("huntermaster_stone"));
            savedatas.Add(new AchievementData { _id = aData_0.ID, _cur = 1, _max = aData_0.MAX, _clear = false });
            AchievementsData aData_1 = System.Array.Find(SheetManager.Instance.AchievementsDB.dataArray, item => item.ID.Equals("stage_clear_" + PDG.Dungeon.instance.StageLevel));
            savedatas.Add(new AchievementData { _id = aData_1.ID, _cur = 1, _max = aData_1.MAX, _clear = false });
            if (StateGroup.instance.nShotByBoss == 0)
            {
                AchievementsData aData = System.Array.Find(SheetManager.Instance.AchievementsDB.dataArray, item => item.ID.Equals("godcontrol"));
                savedatas.Add(new AchievementData { _id = aData.ID, _cur = 1, _max = aData.MAX, _clear = false });
            }
            if (StateGroup.instance.nShotByHunter == 0)
            {
                AchievementsData aData = System.Array.Find(SheetManager.Instance.AchievementsDB.dataArray, item => item.ID.Equals("nodamageclear"));
                savedatas.Add(new AchievementData { _id = aData.ID, _cur = 1, _max = aData.MAX, _clear = false });
            }
            if (PDG.Player.instance.nowWeaponName.Contains("tentacle_arm"))
            {
                AchievementsData aData = System.Array.Find(SheetManager.Instance.AchievementsDB.dataArray, item => item.ID.Equals("tantaclemaster"));
                savedatas.Add(new AchievementData { _id = aData.ID, _cur = 1, _max = aData.MAX, _clear = false });
            }
            if (StateGroup.instance.fClearTime <= 720)
            {
                AchievementsData aData = System.Array.Find(SheetManager.Instance.AchievementsDB.dataArray, item => item.ID.Equals("possible_0"));
                savedatas.Add(new AchievementData { _id = aData.ID, _cur = 1, _max = aData.MAX, _clear = false });
            }
            if (StateGroup.instance.fClearTime <= 480)
            {
                AchievementsData aData = System.Array.Find(SheetManager.Instance.AchievementsDB.dataArray, item => item.ID.Equals("possible_1"));
                savedatas.Add(new AchievementData { _id = aData.ID, _cur = 1, _max = aData.MAX, _clear = false });
            }
            if (StateGroup.instance.fClearTime <= 180)
            {
                AchievementsData aData = System.Array.Find(SheetManager.Instance.AchievementsDB.dataArray, item => item.ID.Equals("possible_2"));
                savedatas.Add(new AchievementData { _id = aData.ID, _cur = 1, _max = aData.MAX, _clear = false });
            }
            SaveAndLoadManager.instance.SaveAchievements(savedatas.ToArray());

            PDG.Dungeon.instance.isStageBoss = false;
            PDG.Dungeon.instance.StageLevel++;
            PDG.Dungeon.instance.nowStageStep = 0;
            SaveAndLoadManager.instance.GameSave(Inventorys.Instance.slots);
        }
        PDG.Player.instance.isCameraOff = false;
        (FindObjectOfType(typeof(PDG.Camera)) as PDG.Camera).target = PDG.Player.instance.transform;
        SoundManager.instance.StartAudio(new string[1] { "warp_open" }, VOLUME_TYPE.EFFECT);
        GameObject.Find("Canvas").transform.Find("TutorialText").gameObject.SetActive(true);
        GameObject.Find("Canvas").transform.Find("TutorialText").GetComponent<Text>().text = LocalizeManager.GetLocalize("boss_open_title");
        if (!PDG.Dungeon.instance.isTestMode2)
            GameObject.Find("Canvas").transform.Find("ExitArrow").gameObject.SetActive(true);
        for (int i = 0; i < nextHole.Length; i++)
        {
            nextHole[i].GetComponent<DungeonNextController>().isBossOpen = true;
            nextHole[i].GetComponent<DungeonNextController>().sr.sprite = nextHole[i].GetComponent<DungeonNextController>().endSprite;
        }
        if (SteamStatsAndAchievements.instance != null) SteamStatsAndAchievements.instance.UnlockAchievementToPublic("Boss_3_Clear");
        StateGroup.instance.OpenResult(tId);
    }
    public void ItemDrop(string[] itemName)
    {
        string tId = mobData.ID;
        if (tId.Contains("_t")) tId = tId.Substring(0, tId.Length - 3);
        UnlockListsData[] pikoUnlockDatas = System.Array.FindAll(SheetManager.Instance.UnlockListsDB.dataArray, item => item.NPCID.Equals(tId));
        for (int i = 0; i < pikoUnlockDatas.Length; i++)
        {
            if (SaveAndLoadManager.instance.LoadUnlock(pikoUnlockDatas[i].Itemid) < 1)
            {
                SaveAndLoadManager.instance.SaveUnlock(pikoUnlockDatas[i].Itemid, 1);
                ItemsData iData = System.Array.Find(SheetManager.Instance.ItemsDB.dataArray, item => item.ID.Equals(pikoUnlockDatas[i].Itemid));
                Inventorys.Instance.ShowGetItemUI("monster_unlockitem", Resources.Load<Sprite>("Item/" + iData.Type1 + "/" + iData.Type2 + "/" + iData.ID));
            }
        }

        Vector3 tPos = transform.parent.position;
        for (int i = 0; i < itemName.Length; i++)
        {
            float angle = -90.0f;
            angle += (i * (360.0f / itemName.Length));
            Vector3 pos = GetPosition(tPos, angle);
            Vector3 dir = pos - tPos;
            dir.Normalize();
            GameObject obj = ObjManager.Call().GetObject("Item");
            if (obj != null)
            {
                Item _item = obj.GetComponent<Item>();
                _item._Item = new ItemInfo
                {
                    Data = Inventorys.Instance.CopyItem(System.Array.Find(SheetManager.Instance.ItemsDB.dataArray, item => item.ID.Equals(itemName[i])))
                };
                _item.dir = dir;
                _item.itemCount = 1;
                _item.rPow = 1;
                float startDis2 = 2.0f;
                float angle2 = angle += 90.0f;
                if (angle2 <= 180) startDis2 -= (angle2 / 150.0f);
                else startDis2 -= ((180.0f / 150.0f) + ((180.0f - angle2) / 150.0f));

                obj.transform.position = pos + (dir * startDis2);
                obj.SetActive(true);
            }
        }
        PDG.Dungeon.DropMaterial(mobData.Type, mobData.Type2, transform.position);
    }
    IEnumerator Spake()
    {
        float a = 0.0f;
        while (true)
        {
            a += Time.fixedUnscaledDeltaTime * 4;
            spakeImage.color = new Color(1, 1, 1, a);
            if (a > 1.0f) break;
            yield return new WaitForSecondsRealtime(Time.fixedUnscaledDeltaTime);
        }
        spakeImage.color = new Color(1, 1, 1, 1);
        GameObject.Find("Camera").GetComponent<PDG.Camera>().isTestScreenClear = true;
        a = 1.0f;
        SoundManager.instance.StartAudio(new string[1] { "BossSpake" }, VOLUME_TYPE.EFFECT);
        GameObject.Find("Camera").GetComponent<PDG.Camera>().isTestScreenClear = true;
        while (true)
        {
            a -= Time.fixedUnscaledDeltaTime * 4;
            spakeImage.color = new Color(1, 1, 1, a);
            if (a <= 0.0f) break;
            yield return new WaitForSecondsRealtime(Time.fixedUnscaledDeltaTime);
        }
        spakeImage.color = new Color(1, 1, 1, 0);
    }
    #region -- Tool --
    public static float GetAngle(Vector3 vStart, Vector3 vEnd)
    {
        Vector3 v = vEnd - vStart;

        return Mathf.Atan2(v.y, v.x) * Mathf.Rad2Deg;
    }
    public static Vector3 GetPosition(Vector3 vStart, float angle)
    {
        float x = Mathf.Cos(Mathf.Deg2Rad * angle) + vStart.x;
        float y = Mathf.Sin(Mathf.Deg2Rad * angle) + vStart.y;

        return new Vector3(x, y, 0);
    }
    private int GetPercentege(float _per, string[] list)
    {
        int[] per = new int[Mathf.FloorToInt(100 * list.Length)];
        for (int i = 0; i < per.Length; i++) per[i] = -1;

        for (int i = 0; i < list.Length; i++)
        {
            for (int j = 0; j < Mathf.FloorToInt(_per * 100); j++)
            {
                per[(i * 100) + j] = i;
            }
        }
        if (per.Length == 0) return -1;
        return per[Random.Range(0, per.Length)];
    }
    #endregion
}
