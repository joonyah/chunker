﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossStoneLaserController : MonoBehaviour
{
    [SerializeField] private LineRenderer[] lineRenders;
    [SerializeField] private GameObject hitObj;
    // Start is called before the first frame update
    void Start()
    {
        Vector3 pos = transform.position;
        lineRenders[0].SetPosition(0, new Vector3(pos.x - 14, pos.y - 6.0f));
        lineRenders[0].SetPosition(1, new Vector3(pos.x + 14, pos.y - 6.0f));

        lineRenders[1].SetPosition(0, new Vector3(pos.x - 14, pos.y - 10.0f));
        lineRenders[1].SetPosition(1, new Vector3(pos.x + 14, pos.y - 10.0f));

        lineRenders[2].SetPosition(0, new Vector3(pos.x - 14, pos.y + 3.0f));
        lineRenders[2].SetPosition(1, new Vector3(pos.x + 14, pos.y + 3.0f));

        lineRenders[3].SetPosition(0, new Vector3(pos.x - 14, pos.y + 7.0f));
        lineRenders[3].SetPosition(1, new Vector3(pos.x + 14, pos.y + 7.0f));

        lineRenders[4].SetPosition(0, new Vector3(pos.x - 14, pos.y + 15.5f));
        lineRenders[4].SetPosition(1, new Vector3(pos.x + 14, pos.y + 15.5f));
        
        lineRenders[5].SetPosition(0, new Vector3(pos.x - 14, pos.y + 19.5f));
        lineRenders[5].SetPosition(1, new Vector3(pos.x + 14, pos.y + 19.5f));
    }

    // Update is called once per frame
    void Update()
    {
        for (int i = 0; i < lineRenders.Length; i++)
        {
            if (lineRenders[i] == null) continue;
            Vector3 dir = lineRenders[i].GetPosition(1) - lineRenders[i].GetPosition(0);
            dir.Normalize();
            float dis = Vector3.Distance(lineRenders[i].GetPosition(0), lineRenders[i].GetPosition(1));
            float angle = GetAngle(lineRenders[i].GetPosition(0), lineRenders[i].GetPosition(1));
            RaycastHit2D hit = Physics2D.BoxCast(lineRenders[i].GetPosition(0), new Vector2(1, 0.5f), angle, dir, dis, 1 << 9);
            if (hit)
            {
                if (hit.collider.GetComponent<PDG.Player>())
                {
                    if (hitObj)
                    {
                        GameObject obj = Instantiate(hitObj);
                        obj.transform.position = hit.point;
                        Destroy(obj, 2f);
                    }
                    hit.collider.GetComponent<PDG.Player>().SetDamage(1, LocalizeManager.GetLocalize("killedby_laser"), DEATH_TYPE.DEATH_BOSS, "boss_stone");
                }
            }
        }
    }
    #region -- Tool --
    public static float GetAngle(Vector3 vStart, Vector3 vEnd)
    {
        Vector3 v = vEnd - vStart;

        return Mathf.Atan2(v.y, v.x) * Mathf.Rad2Deg;
    }
    public static Vector3 GetPosition(Vector3 vStart, float angle)
    {
        float x = Mathf.Cos(Mathf.Deg2Rad * angle) + vStart.x;
        float y = Mathf.Sin(Mathf.Deg2Rad * angle) + vStart.y;

        return new Vector3(x, y, 0);
    }
    #endregion
}
