﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossStoneBulletController : MonoBehaviour
{
    [SerializeField] private GameObject destroyObj;
    float z;

    private void Start()
    {
        Destroy(gameObject, 2.0f);
    }
    // Update is called once per frame
    void Update()
    {
        z += Time.deltaTime * 1500;
        transform.eulerAngles = new Vector3(0, 0, z);

        RaycastHit2D hit = Physics2D.CircleCast(transform.position, 1, Vector2.zero, 0, 1 << 9 | 1 << 0 | 1 << 27);
        if (hit)
        {
            if (hit.collider.gameObject.layer.Equals(27))
            {
                if (hit.collider.name.Equals("Shield"))
                {
                    PDG.Player.instance.right_defence_gauge -= 1.0f;
                    Inventorys.Instance.GuageShaker(1);
                    CameraShaker._instance.StartShake(0.2f, 0.02f, 0.2f);
                }
                if (hit.collider.name.Contains("dna_mo_blade"))
                {
                    PDG.Player.instance.left_defence_gauge -= 1.0f;
                    Inventorys.Instance.GuageShaker(0);
                    CameraShaker._instance.StartShake(0.2f, 0.02f, 0.2f);
                }
                Destroy(gameObject);
            }
            if (hit.collider.GetComponent<PDG.Player>())
            {
                GameObject obj = Instantiate(destroyObj);
                obj.transform.position = transform.position;
                Destroy(obj, 2.0f);
                hit.collider.GetComponent<PDG.Player>().SetDamage(1, LocalizeManager.GetLocalize("killedby_stone"), DEATH_TYPE.DEATH_BOSS, "boss_stone");
                Destroy(gameObject);
            }
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        Destroy(gameObject);
    }
}
