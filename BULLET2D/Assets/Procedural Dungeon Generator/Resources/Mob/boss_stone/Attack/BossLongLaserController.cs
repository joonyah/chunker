﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossLongLaserController : MonoBehaviour
{
    [SerializeField] private GameObject destroyObj;
    private void Start()
    {
        Destroy(gameObject, 2.0f);
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.layer.Equals(9)) 
        {
            if (collision.gameObject.GetComponent<PDG.Player>())
            {
                if(destroyObj)
                {
                    GameObject obj = Instantiate(destroyObj);
                    obj.transform.position = transform.position;
                    Destroy(obj, 2f);
                }
                collision.gameObject.GetComponent<PDG.Player>().SetDamage(1, LocalizeManager.GetLocalize("killedby_laser"), DEATH_TYPE.DEATH_BOSS, "boss_stone");
            }
        }
    }
}
