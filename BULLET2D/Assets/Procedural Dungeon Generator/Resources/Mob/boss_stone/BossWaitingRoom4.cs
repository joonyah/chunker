﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossWaitingRoom4 : MonoBehaviour
{
    [SerializeField] private UnityEngine.UI.Image spakeImage;

    [SerializeField] private GameObject pObj;
    [SerializeField] private GameObject entranceObj;

    [SerializeField] private bool isStart = false;
    [SerializeField] private bool isSetting = false;
    [SerializeField] private Sprite _sprite;
    private void Start()
    {
        spakeImage = GameObject.Find("Canvas").transform.Find("Spake").GetComponent<UnityEngine.UI.Image>();
    }

    private void Update()
    {
        if (isStart && !Dialogue.Instance.isCommnet)
        {
            if (!isSetting)
            {
                StartCoroutine(IntroSetting());
                isSetting = true;
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.layer.Equals(9) && !isStart)
        {
            Dialogue.Instance.CommnetSetting("waitingroom4", 0, null);
            isStart = true;
        }
    }

    IEnumerator IntroSetting()
    {
        CameraShaker._instance.StartShake(0.1f, Time.deltaTime, 1.0f, true);
        yield return new WaitForSeconds(1.0f);
        yield return StartCoroutine(Spake());
        entranceObj.SetActive(true);
        gameObject.SetActive(false);
    }
    IEnumerator Spake()
    {
        float a = 0.0f;
        while (true)
        {
            a += Time.deltaTime;
            spakeImage.color = new Color(1, 1, 1, a);
            if (a > 1.0f) break;
            yield return new WaitForSeconds(Time.deltaTime);
        }
        spakeImage.color = new Color(1, 1, 1, 1);
        a = 1.0f;
        pObj.GetComponent<SpriteRenderer>().sprite = _sprite;
        SoundManager.instance.StartAudio(new string[1] { "BossWait_3_Entrance" }, VOLUME_TYPE.EFFECT);
        while (true)
        {
            a -= Time.deltaTime;
            spakeImage.color = new Color(1, 1, 1, a);
            if (a <= 0.0f) break;
            yield return new WaitForSeconds(Time.deltaTime);
        }
        spakeImage.color = new Color(1, 1, 1, 0);
    }
}
