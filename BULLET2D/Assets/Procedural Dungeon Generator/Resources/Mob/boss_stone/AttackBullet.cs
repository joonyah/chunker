﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackBullet : MonoBehaviour
{
    public int _type = 0;
    public GameObject ownerObj;
    [SerializeField] private GameObject destroyObj;
    [SerializeField] private GameObject targetObj;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(Attack());
    }
    IEnumerator Attack()
    {
        Vector3 startPos = transform.position;
        float t = 0.0f;
        if(_type == 1)
        {
            //Vector3 endPos = ownerObj.transform.position;
            //endPos = new Vector3(endPos.x + Random.Range(-7, 7), endPos.y + Random.Range(-12, 10));
            Vector3 endPos = PDG.Player.instance.transform.position;
            endPos = new Vector3(endPos.x + Random.Range(-5f, 5f), endPos.y + Random.Range(-5f, 5f));
            if (targetObj)
            {
                GameObject tObj = Instantiate(targetObj);
                tObj.transform.position = endPos;
                Destroy(tObj, 1f);
            }
            Vector3 differencePos = endPos - startPos;
            Vector3 center1 = differencePos / 4;
            Vector3 center2 = center1 * 3;
            center1 += startPos;
            center2 += startPos;

            Vector3 temp1 = GetPosition(center1, 90);
            Vector3 temp1_1 = temp1 - center1;
            temp1_1.Normalize();
            center1 = center1 + (temp1_1 * 5);

            Vector3 temp2 = GetPosition(center2, 90);
            Vector3 temp2_1 = temp2 - center2;
            temp2_1.Normalize();
            center2 = center1 + (temp2_1 * 5);
            while (true)
            {
                Vector3 lerp1_0 = Vector3.Lerp(startPos, center1, t);
                Vector3 lerp1_1 = Vector3.Lerp(center1, center2, t);
                Vector3 lerp1_2 = Vector3.Lerp(center2, endPos, t);

                Vector3 lerp2_0 = Vector3.Lerp(lerp1_0, lerp1_1, t);
                Vector3 lerp2_1 = Vector3.Lerp(lerp1_1, lerp1_2, t);

                Vector3 lerp = Vector3.Lerp(lerp2_0, lerp2_1, t);
                transform.position = lerp;
                t += Time.deltaTime;
                if (t >= 1.0f)
                {
                    lerp1_0 = Vector3.Lerp(startPos, center1, 1);
                    lerp1_1 = Vector3.Lerp(center1, center2, 1);
                    lerp1_2 = Vector3.Lerp(center2, endPos, 1);

                    lerp2_0 = Vector3.Lerp(lerp1_0, lerp1_1, 1);
                    lerp2_1 = Vector3.Lerp(lerp1_1, lerp1_2, 1);

                    lerp = Vector3.Lerp(lerp2_0, lerp2_1, 1);
                    transform.position = lerp;
                    break;
                }
                yield return new WaitForSeconds(Time.deltaTime / Random.Range(1, 3));
            }
            RaycastHit2D hit = Physics2D.CircleCast(transform.position, 1, Vector2.zero, 0, 1 << 9);
            if (hit)
            {
                hit.collider.GetComponent<PDG.Player>().SetDamage(1, LocalizeManager.GetLocalize("StoneLaserDown"), DEATH_TYPE.DEATH_BOSS, "boss_stone");
            }
        }
        else
        {
            Vector3 endPos = PDG.Player.instance.transform.position;
            endPos = new Vector3(endPos.x + Random.Range(-2, 2), endPos.y + Random.Range(-2, 2));

            while (true)
            {
                RaycastHit2D hit = Physics2D.CircleCast(transform.position, 0.3f, Vector2.zero, 0, 1 << 9);
                if (hit)
                {
                    hit.collider.GetComponent<PDG.Player>().SetDamage(1, LocalizeManager.GetLocalize("StoneLaserUp"), DEATH_TYPE.DEATH_BOSS, "boss_stone");
                    break;
                }
                Vector3 lerp = Vector3.Lerp(startPos, endPos, t);
                transform.position = lerp;
                t += Time.deltaTime;
                if (t >= 1.0f)
                {
                    lerp = Vector3.Lerp(startPos, endPos, 1);
                    transform.position = lerp;
                    break;
                }
                yield return new WaitForSeconds(Time.deltaTime / Random.Range(1, 3));
            }
            
        }
        if(destroyObj)
        {
            GameObject obj = Instantiate(destroyObj);
            obj.transform.position = transform.position;
            Destroy(obj, 2f);
        }
        Destroy(gameObject);
    }
    #region -- Tool --
    public static float GetAngle(Vector3 vStart, Vector3 vEnd)
    {
        Vector3 v = vEnd - vStart;

        return Mathf.Atan2(v.y, v.x) * Mathf.Rad2Deg;
    }
    public static Vector3 GetPosition(Vector3 vStart, float angle)
    {
        float x = Mathf.Cos(Mathf.Deg2Rad * angle) + vStart.x;
        float y = Mathf.Sin(Mathf.Deg2Rad * angle) + vStart.y;

        return new Vector3(x, y, 0);
    }
    #endregion
}
