﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossScytheController : MonoBehaviour
{
    public Transform parantTransform;
    public float fAroundSpeed = 50.0f;
    [SerializeField] private float fAroundSpeedOrigin = 50.0f;
    public float moveAngle = 0.0f;
    [SerializeField] private float moveAngleOrigin = 120.0f;
    public float dis = 2.0f;
    [SerializeField] private float disOrigin = 2.0f;
    [SerializeField] private float z = 0.0f;
    [SerializeField] private float fRotateSpeed = 20.0f;
    [SerializeField] private bool isDir = false;
    AudioSource audio;
    [SerializeField] private GameObject hitEffectObj;
    private void Start()
    {
        audio = GetComponent<AudioSource>();
    }
    // Start is called before the first frame update
    void OnEnable()
    {
        fAroundSpeed = fAroundSpeedOrigin;
        moveAngle = moveAngleOrigin;
        dis = disOrigin;
        z = 0.0f;
        fRotateSpeed = 3000.0f;
        isDir = Random.Range(0, 100) % 2 == 0 ? true : false;
        if (parantTransform == null && PDG.Player.instance != null) parantTransform = PDG.Player.instance.transform;
        StartCoroutine(Rotate());
    }

    IEnumerator Rotate()
    {
        while(true)
        {
            if(PDG.Player.instance != null)
            {
                float dis2 = Vector3.Distance(PDG.Player.instance.transform.position, transform.position);
                if (audio == null) audio = GetComponent<AudioSource>();
                if (dis2 > 10) audio.volume = 0;
                if ( (SoundManager.isMute || (PDG.Dungeon.instance == null ? false : PDG.Dungeon.instance.isPause) || (PDG.Player.instance == null ? false : PDG.Player.instance.isDie)))
                {
                    audio.volume = 0;
                }
                else
                {
                    float tVolume = SoundManager.AmbientVolume;
                    float t = tVolume / 10;
                    audio.volume = tVolume - (dis2 * t);
                }

                dis += Time.deltaTime;

                if (isDir) z += Time.deltaTime * fRotateSpeed;
                else z -= Time.deltaTime * fRotateSpeed;
                transform.localEulerAngles = new Vector3(0, 0, z);

                if (isDir) moveAngle += Time.deltaTime * fAroundSpeed;
                else moveAngle -= Time.deltaTime * fAroundSpeed;

                if (moveAngle >= 360.0f) moveAngle -= 360.0f;
                else if (moveAngle < 0) moveAngle += 360.0f;

                Vector3 anglePosition = GetPosition(parantTransform.position, moveAngle);
                Vector3 dir = anglePosition - parantTransform.position;
                dir.Normalize();
                transform.position = parantTransform.position + (dir * dis);

                if (Vector3.Distance(transform.position, parantTransform.position) > 20)
                {
                    gameObject.SetActive(false);
                }

                RaycastHit2D hit = Physics2D.CircleCast(transform.position, 1.25f, Vector2.zero, 0, 1 << 9);
                if (hit)
                {
                    if (hit.collider.GetComponent<PDG.Player>())
                    {
                        if (hitEffectObj && !hit.collider.GetComponent<PDG.Player>().isHit && !hit.collider.GetComponent<PDG.Player>().isDie)
                        {
                            GameObject obj = Instantiate(hitEffectObj);
                            obj.transform.position = transform.position;
                            Destroy(obj, 2f);
                        }
                        hit.collider.GetComponent<PDG.Player>().SetDamage(1, LocalizeManager.GetLocalize("monster_boss_npc"), DEATH_TYPE.DEATH_BOSS, "boss_npc");
                    }
                }

            }

            yield return new WaitForSeconds(Time.deltaTime);
        }
    }
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, 1.25f);
    }
    #region -- Tool --
    public static float GetAngle(Vector3 vStart, Vector3 vEnd)
    {
        Vector3 v = vEnd - vStart;

        return Mathf.Atan2(v.y, v.x) * Mathf.Rad2Deg;
    }
    public static Vector3 GetPosition(Vector3 vStart, float angle)
    {
        float x = Mathf.Cos(Mathf.Deg2Rad * angle) + vStart.x;
        float y = Mathf.Sin(Mathf.Deg2Rad * angle) + vStart.y;

        return new Vector3(x, y, 0);
    }
    #endregion
}
