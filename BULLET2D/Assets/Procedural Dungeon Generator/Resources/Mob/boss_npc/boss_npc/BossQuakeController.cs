﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossQuakeController : MonoBehaviour
{
    AudioSource audio;
    [SerializeField] private GameObject hitEffectObj;
    private void Start()
    {
        audio = GetComponent<AudioSource>();
    }
    public void EndEffect()
    {
        Destroy(gameObject);
    }
    private void OnTriggerStay2D(Collider2D collision)
    {
        if(collision.gameObject.layer.Equals(9))
        {
            if (collision.gameObject.GetComponent<PDG.Player>())
            {
                if(hitEffectObj && !collision.gameObject.GetComponent<PDG.Player>().isHit && !collision.GetComponent<PDG.Player>().isDie)
                {
                    GameObject obj = Instantiate(hitEffectObj);
                    obj.transform.position = collision.transform.position;
                    Destroy(obj, 2f);
                }
                collision.gameObject.GetComponent<PDG.Player>().SetDamage(1, LocalizeManager.GetLocalize("monster_boss_npc"), DEATH_TYPE.DEATH_BOSS, "boss_npc");
            }
        }
    }
    public void AudioPlay()
    {
        float dis = Vector3.Distance(PDG.Player.instance.transform.position, transform.position);
        if (dis > 10) audio.volume = 0;
        if ( (SoundManager.isMute || (PDG.Dungeon.instance == null ? false : PDG.Dungeon.instance.isPause) || (PDG.Player.instance == null ? false : PDG.Player.instance.isDie)))
        {
            audio.volume = 0;
        }
        else
        {
            float tVolume = SoundManager.AmbientVolume;
            float t = tVolume / 10;
            audio.volume = tVolume - (dis * t);
        }
        audio.Play();
    }
}
