﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SporesController : MonoBehaviour
{
    Vector3 dir;
    [SerializeField] private float moveSpeed = 50.0f;
    float time = 5.0f;
    float z = 0.0f;
    bool isLeft = false;
    [SerializeField] private float fRotateSpeed = 800.0f;
    public float bulletSpeed = 5.0f;
    public float fAttackRange = 10;
    [SerializeField] private Transform firePos;

    // Start is called before the first frame update
    void Start()
    {
        dir = new Vector3(Random.Range(-1, 1), Random.Range(-1, 1), 0);
        isLeft = Random.Range(0, 100) % 2 == 0 ? true : false;

        StartCoroutine(Fire());
    }

    // Update is called once per frame
    void Update()
    {
        if (isLeft) z += Time.deltaTime * fRotateSpeed;
        else z -= Time.deltaTime * fRotateSpeed;
        transform.localEulerAngles = new Vector3(0, 0, z);

        RaycastHit2D hit = Physics2D.CircleCast(transform.position, 0.3f, Vector2.zero, 0, 1 << 16 | 1 << 25);
        if (hit)
        {
            DirChange();
        }

        time -= Time.deltaTime;
        if (time <= 0.0f)
        {
            time = 5.0f;
            DirChange();
        }

        transform.Translate(dir * moveSpeed * Time.deltaTime);
        hit = Physics2D.CircleCast(transform.position, 0.3f, Vector2.zero, 0, 1 << 9);
        if (hit)
        {
            if (hit.collider.GetComponent<PDG.Player>())
            {
                hit.collider.GetComponent<PDG.Player>().SetDamage(1, LocalizeManager.GetLocalize("monster_boss_npc"), DEATH_TYPE.DEATH_BOSS, "boss_npc");
            }
        }
    }

    void DirChange()
    {
        dir = new Vector3(Random.Range(-1, 1), Random.Range(-1, 1), 0);
    }

    IEnumerator Fire()
    {
        while(true)
        {
            Vector3 dir2 = firePos.position - transform.position;
            dir2.Normalize();
            GameObject obj = ObjManager.Call().GetObject("Bullet");
            obj.GetComponent<BulletController>().isMonster = true;
            obj.GetComponent<BulletController>().firePosition = PDG.Player.instance.transform.position;
            obj.GetComponent<BulletController>().monsterName = "boss_npc";
            obj.GetComponent<BulletController>().bulletType = "Monster_Bullet3";
            obj.GetComponent<BulletController>().fSpeed = bulletSpeed;
            obj.GetComponent<BulletController>().fAngle = GetAngle(transform.position, transform.position + dir2);
            obj.GetComponent<BulletController>().fRange = fAttackRange;
            obj.GetComponent<BulletController>().isPenetrate = false;
            obj.GetComponent<BulletController>().dir = dir2;
            obj.GetComponent<BulletController>().isRightAttack = false;
            obj.transform.position = firePos.position;
            obj.SetActive(true);
            obj.GetComponent<BulletController>().BulletFire();
            yield return new WaitForSeconds(1.0f);
        }
    }
    #region -- Tool --
    public static float GetAngle(Vector3 vStart, Vector3 vEnd)
    {
        Vector3 v = vEnd - vStart;

        return Mathf.Atan2(v.y, v.x) * Mathf.Rad2Deg;
    }
    public static Vector3 GetPosition(Vector3 vStart, float angle)
    {
        float x = Mathf.Cos(Mathf.Deg2Rad * angle) + vStart.x;
        float y = Mathf.Sin(Mathf.Deg2Rad * angle) + vStart.y;

        return new Vector3(x, y, 0);
    }
    #endregion
}
