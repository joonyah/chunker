﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossEyesController : MonoBehaviour
{
    [SerializeField] private UbhShotCtrl ctl;
    private float timer = 3.0f;
    private float timer2 = 3.0f;
    private Animator animator;
    private bool isBomb = false;
    private SpriteRenderer sr;
    [SerializeField] private GameObject destroyObj;
    [SerializeField] private GameObject hitEffectObj;
    private void Start()
    {
        sr = GetComponent<SpriteRenderer>();
        animator = GetComponent<Animator>();
    }

    private void Update()
    {
        timer -= Time.deltaTime;
        if (timer <= 0.0f && !isBomb)
        {
            isBomb = true;
            animator.SetTrigger("attack");
            timer2 = 3.0f;
            timer = 4.5f;
        }
        else if (timer <= 0.0f && isBomb)
        {
            Destroy(gameObject);
        }

        if(isBomb)
        {
            timer2 -= Time.deltaTime;
            if (timer2 <= 0.0f)
            {
                if (destroyObj)
                {
                    GameObject obj = Instantiate(destroyObj);
                    obj.transform.position = transform.position;
                    Destroy(obj, 2f);
                }
                timer2 = 20.0f;
                GameObject obj3 = ObjManager.Call().GetObject("Hit_sprite9");
                obj3.transform.position = transform.position;
                obj3.transform.localScale = Vector3.one;
                obj3.transform.localEulerAngles = new Vector3(0, 0, 0);
                obj3.GetComponent<SwordHitEffectController>()._effctName = "bomb";
                obj3.GetComponent<SwordHitEffectController>().animSpeed = 1.0f;
                obj3.GetComponent<Animator>().Rebind();
                obj3.SetActive(true);
                sr.enabled = false;
                ctl._hitEffect = hitEffectObj;
                ctl._mobName = "boss_npc";
                ctl._bulletName = "Monster_Bullet2";
                ctl.StartShotRoutine();
            }
        }

        RaycastHit2D hit = Physics2D.CircleCast(transform.position, 0.5f, Vector2.zero, 0, 1 << 9);
        if (hit)
        {
            if (hit.collider.GetComponent<PDG.Player>())
            {
                if (hitEffectObj && !hit.collider.GetComponent<PDG.Player>().isHit && !hit.collider.GetComponent<PDG.Player>().isDie)
                {
                    GameObject obj = Instantiate(hitEffectObj);
                    obj.transform.position = transform.position;
                    Destroy(obj, 2f);
                }
                hit.collider.GetComponent<PDG.Player>().SetDamage(1, LocalizeManager.GetLocalize("monster_boss_npc"), DEATH_TYPE.DEATH_BOSS, "boss_npc");
            }
        }
    }
}
