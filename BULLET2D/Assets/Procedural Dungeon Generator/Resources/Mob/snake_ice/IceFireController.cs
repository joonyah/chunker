﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IceFireController : MonoBehaviour
{
    [SerializeField] private GameObject drumBombObj;

    [SerializeField] private AudioSource _audio;
    [SerializeField] private float changeTime = 2.0f;
    [SerializeField] private bool isTargeting = false;
    [SerializeField] private GameObject DestroyObj;
    [SerializeField] private SpriteOutline sOutline;
    [SerializeField] private bool isBoss = false;
    private void Start()
    {
        if (sOutline) sOutline.Regenerate();
    }
    private void OnEnable()
    {
        if (sOutline) sOutline.Regenerate();
    }

    private void Update()
    {
        if (isTargeting && changeTime > 0.0f)
        {
            changeTime -= Time.deltaTime;
        }
        if (isTargeting && changeTime <= 0.0f)
        {
            changeTime = 2.0f;
            Vector3 dir = PDG.Player.instance.transform.position - transform.position;
            dir.Normalize();
            GetComponent<Rigidbody2D>().velocity = Vector2.zero;
            GetComponent<Rigidbody2D>().AddForce(dir * 5, ForceMode2D.Impulse);
        }
        if (_audio)
        {
            float dis = Vector3.Distance(PDG.Player.instance.transform.position, transform.position);
            if (dis > 5) _audio.volume = 0;
            float tVolume =  (SoundManager.isMute || (PDG.Dungeon.instance == null ? false : PDG.Dungeon.instance.isPause) || (PDG.Player.instance == null ? false : PDG.Player.instance.isDie)) ? 0 : SoundManager.EffectVolume;
            float t = tVolume / 5;
            _audio.volume = tVolume - (dis * t);
        }

        RaycastHit2D hit = Physics2D.CircleCast(transform.position, 0.5f, Vector2.zero, 0, 1 << 9 | 1 << 8 | 1 << 27);
        if (hit)
        {
            if (hit.collider.gameObject.layer.Equals(9))
            {
                if (hit.collider.GetComponent<PDG.Player>())
                    hit.collider.GetComponent<PDG.Player>().SetDamage(1, LocalizeManager.GetLocalize("killedby_iceball"), isBoss ? DEATH_TYPE.DEATH_BOSS : DEATH_TYPE.DEATH_NORMAL, isBoss ? "boss_stone" : "snake_ice");
            }
            else if(hit.collider.gameObject.layer.Equals(27))
            {
                Destroy(gameObject);
            }
            GameObject obj = Instantiate(drumBombObj);
            obj.GetComponent<DrumBomb>().InitSetting(0, transform.position, drumBombObj, true, true, 0.25f, isBoss ? "boss_stone" : "snake_ice");
            if (DestroyObj)
            {
                GameObject obj2 = Instantiate(DestroyObj);
                obj2.transform.position = transform.position;
                Destroy(obj2, 2f);
            }
            Destroy(gameObject);
        }
    }
}
