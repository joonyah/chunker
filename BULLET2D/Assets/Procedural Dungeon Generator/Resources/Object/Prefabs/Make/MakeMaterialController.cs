﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MakeMaterialController : MonoBehaviour
{
    [SerializeField] private Image sImg;
    [SerializeField] private Text sText;
    public string _id;

    public void InitSetting(Sprite _sprite, int _count)
    {
        sImg.sprite = _sprite;
        sText.text = _count.ToString();

        int c = SaveAndLoadManager.instance.LoadItem(_id);
        int c2 = int.Parse(sText.text);
        if (c < c2)
        {
            sText.color = Color.red;
        }
        else
        {
            sText.color = Color.white;
        }
    }
    public void InitSetting()
    {
        int c = SaveAndLoadManager.instance.LoadItem(_id);
        int c2 = int.Parse(sText.text);
        if (c < c2)
        {
            sText.color = Color.red;
        }
        else
        {
            sText.color = Color.white;
        }
    }
}
