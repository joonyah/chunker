﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class MakeListController : MonoBehaviour
{
    public MakeData mData;
    public Image sImg;
    public Text sText;
    [SerializeField] private EventTrigger trigger;

    public GameObject EquipmentObj;
    public Text EquipmentText;
    public Image EquipmentImage;
    public int _count;
    private void Awake()
    {
        trigger = GetComponent<EventTrigger>();
    }

    public void InitSetting(int count, bool levelMax = false)
    {
        _count = count;
        string path = string.Empty;
        if (mData.Resulttype.Equals("Item"))
        {
            ItemsData iData = System.Array.Find(SheetManager.Instance.ItemsDB.dataArray, item => item.ID.Equals(mData.ID));
            path = "Item/" + iData.Type1 + "/" + iData.Type2 + "/" + iData.ID;
        }
        else if (mData.Resulttype.Equals("Special"))
        {
            path = "Object/Special/" + mData.ID + "/" + mData.ID;
        }
        else if (mData.Resulttype.Equals("Equipment"))
        {
            ItemsData iData = System.Array.Find(SheetManager.Instance.ItemsDB.dataArray, item => item.ID.Equals(mData.ID));
            path = "Item/" + iData.Type1 + "/" + iData.Type2 + "/" + iData.ID;
        }
        else if(mData.Resulttype.Equals("LevelUp"))
        {
            string str = string.Empty;
            if (levelMax)
            {
                str = mData.ID;
            }
            else if(mData.Unlock.Equals("NONE"))
            {
                str = mData.ID;
                int t = str.LastIndexOf('_');
                if (t != -1 && t == str.Length - 2)
                {
                    str = str.Substring(0, str.Length - 2);
                }
            }
            else
            {
                str = mData.Unlock;
            }
            ItemsData iData = System.Array.Find(SheetManager.Instance.ItemsDB.dataArray, item => item.ID.Equals(str));
            path = "Item/" + iData.Type1 + "/" + iData.Type2 + "/" + iData.ID;
        }
        else
        {
            string[] s = mData.ID.Split('_');
            string t = string.Empty;
            for (int i = 1; i < s.Length; i++)
            {
                if (i == 1) t += s[i];
                else t += "_" + s[i];
            }
            path = "Item/Material/" + s[0] + "/" + t;
        }
        sImg.sprite = Resources.Load<Sprite>(path);
        sText.text = LocalizeManager.GetLocalize(mData.ID);
        string tt = mData.Resulttype;
        EventTrigger.Entry entry = new EventTrigger.Entry();
        entry.eventID = EventTriggerType.PointerClick;
        entry.callback.AddListener((EvnetData) => { MakeController.instance.ListClickEvent(mData, this, tt, _count); });
        trigger.triggers.Add(entry);
    }
}
