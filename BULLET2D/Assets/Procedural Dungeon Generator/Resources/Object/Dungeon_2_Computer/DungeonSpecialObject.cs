﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class DungeonSpecialObject : MonoBehaviour
{
    [SerializeField] private GameObject controllerObj;
    [SerializeField] private SpriteRenderer controllerSR;
    [SerializeField] private bool isSelect = false;
    bool isOpen = false;
    public bool isUsing = false;
    public bool isUse = false;
    Animator animator;
    private float delay = 0.0f;

    [SerializeField] private Vector3 vOffset;
    [SerializeField] private Vector3 vSize;
    [SerializeField] private string firstCommnet;
    [SerializeField] private string secondCommnet;
    [SerializeField] private string thirdCommnet;
    [SerializeField] private string thirdCommnet2;
    [SerializeField] private Sprite noneSprite;
    [SerializeField] private GameObject timerObj;
    [SerializeField] private float fTime = 3.0f;

    [SerializeField] private AudioClip[] clips;
    [SerializeField] private AudioSource audio;

    [SerializeField] private bool isNotice = false;
    [SerializeField] private bool isEndingDoor = false;
    [SerializeField] private bool isEndingCredits = false;
    [SerializeField] private SpriteRenderer openSR;
    Image spakeImage;
    [SerializeField] private Animator animObj;
    [SerializeField] private bool isWarpObj;
    [SerializeField] private bool isWarpOpenObj;
    [SerializeField] private bool isRepair;

    [SerializeField] private bool isStartMap;
    [SerializeField] private bool isLastMap;
    [Header("Totem")]
    [SerializeField] private bool isTotem;
    [SerializeField] private string totemCase;
    [SerializeField] private TotemData tData;
    [SerializeField] private Sprite offItem;
    [SerializeField] private Sprite onItem;
    [SerializeField] private GameObject[] offObj;
    [SerializeField] private GameObject[] onObj;
    [SerializeField] private bool isTutorial;

    private void Awake()
    {
        audio = GetComponent<AudioSource>();
        animator = GetComponent<Animator>();
        clips = Resources.LoadAll<AudioClip>("Object/Dungeon_2_Computer");
    }
    // Start is called before the first frame update
    void Start()
    {
        spakeImage = GameObject.Find("Canvas").transform.Find("Spake").GetComponent<Image>();
        timerObj = GameObject.Find("Canvas").transform.Find("Timer").gameObject;
        if (animator) animator.enabled = false;
        noneSprite = Resources.Load<Sprite>("NoneSprite");

        if (isTotem)
        {
            int t = Random.Range(0, SheetManager.Instance.TotemDB.dataArray.Length);
            tData = SheetManager.Instance.TotemDB.dataArray[t];
            secondCommnet = "totem_" + tData.ID;
            thirdCommnet = "totem_result_" + tData.ID;
        }
    }

    void ResetTotem()
    {
        int t = Random.Range(0, SheetManager.Instance.TotemDB.dataArray.Length);
        tData = SheetManager.Instance.TotemDB.dataArray[t];
        secondCommnet = "totem_" + tData.ID;
        thirdCommnet = "totem_result_" + tData.ID;
        isOpen = false;
        isUsing = false;
        isUse = false;
        GetComponent<SpriteRenderer>().sprite = onItem;
        for (int i = 0; i < offObj.Length; i++) offObj[i].SetActive(true);
        for (int i = 0; i < onObj.Length; i++) onObj[i].SetActive(false);

        controllerObj.transform.localPosition = new Vector3(0, 2, 0);
    }
    // Update is called once per frame
    void Update()
    {
        delay -= Time.deltaTime;
        if (delay <= 0.0f) delay = 0.0f;

        RaycastHit2D hit = Physics2D.BoxCast(transform.position + vOffset, vSize, 0, Vector2.zero, 0, 1 << 9);
        if (hit && !isSelect && !isOpen && (isWarpOpenObj ? (!isUse/* || isRepair*/) : !isUse) && !isUsing &&
            (isEndingDoor ? (PDG.Dungeon.instance.isEndingDoorOpen ? openSR.enabled && !isEndingCredits : true) : true) && (isEndingDoor ? true : !PDG.Dungeon.instance.newObjectSelected)) 
        {
            isSelect = true;
            PDG.Dungeon.instance.newObjectSelected = true;
            PDG.Dungeon.instance.newObjectSelectedObj = gameObject;
            controllerObj.GetComponent<SpriteRenderer>().sprite = PDG.Dungeon.instance.isJoystrick ? PDG.Dungeon.GetJoyStrickKeySprite(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_ACTIVE]) : PDG.Dungeon.GetJoyStrickKeySprite(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ACTIVE]);
            controllerObj.SetActive(true);
            if (isEndingDoor && openSR.enabled)
                TextOpenController.instance.ShowText(LocalizeManager.GetLocalize(secondCommnet), Color.white, 24, controllerObj, 0.5f, true);
            else if (isWarpOpenObj && PDG.Dungeon.instance.isGetWarpPart && !isRepair)
                TextOpenController.instance.ShowText(LocalizeManager.GetLocalize(thirdCommnet2 + "_0"), Color.white, 24, controllerObj, 0.5f, true);
            else if (isWarpOpenObj && !PDG.Dungeon.instance.isGetWarpPart && isRepair && !PDG.Dungeon.instance.isOpenWarp)
                TextOpenController.instance.ShowText(LocalizeManager.GetLocalize(thirdCommnet2 + "_0"), Color.white, 24, controllerObj, 0.5f, true);
            else if (isWarpOpenObj && !PDG.Dungeon.instance.isGetWarpPart && isRepair && PDG.Dungeon.instance.isOpenWarp)
                TextOpenController.instance.ShowText(LocalizeManager.GetLocalize(thirdCommnet2 + "_1"), Color.white, 24, controllerObj, 0.5f, true);
            else
                TextOpenController.instance.ShowText(LocalizeManager.GetLocalize(firstCommnet), Color.white, 24, controllerObj, 0.5f, true);
        }
        else if (hit && !isSelect && isOpen && !isUse && !isUsing && !TextOpenController.instance.isOpen)
        {
            if (!isNotice && !isWarpOpenObj)
            {
                isSelect = true;
                controllerObj.SetActive(true);
                TextOpenController.instance.ShowText(LocalizeManager.GetLocalize(secondCommnet), Color.white, 24, controllerObj, 0.5f, true);
            }
            else
            {
                controllerObj.SetActive(false);
                TextOpenController.instance.ShowText(LocalizeManager.GetLocalize(secondCommnet), Color.white, 24, controllerObj, 0.5f, true);
            }
        }
        else if (!hit)
        {
            isSelect = false;
            if (PDG.Dungeon.instance.newObjectSelectedObj == gameObject)
            {
                PDG.Dungeon.instance.newObjectSelectedObj = null;
                PDG.Dungeon.instance.newObjectSelected = false;
            }
            isOpen = false;
            isUsing = false;
            if (audio) audio.Stop();
            if (animator && !isWarpOpenObj) animator.enabled = false;
            controllerObj.SetActive(false);
            if (TextOpenController.instance.targetObj == controllerObj)
            {
                TextOpenController.instance.TextClose(Color.white);
                timerObj.SetActive(false);
            }
        }

        if (timerObj.activeSelf && (Input.GetKeyDown(KeyCode.Escape) || PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_CANCLE], true)) && delay <= 0.0f)
        {
            delay = 0.1f;
            isSelect = false;
            if (PDG.Dungeon.instance.newObjectSelectedObj == gameObject)
            {
                PDG.Dungeon.instance.newObjectSelectedObj = null;
                PDG.Dungeon.instance.newObjectSelected = false;
            }
            isOpen = false;
            isUsing = false;
            if (audio) audio.Stop();
            if (animator) animator.enabled = false;
            controllerObj.SetActive(false);
            if (TextOpenController.instance.targetObj == controllerObj)
            {
                TextOpenController.instance.TextClose(Color.white);
                timerObj.SetActive(false);
            }
            PlayerCombat.instance.tCoroutine = null;
        }
        if (isSelect)
        {
            if ((PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ACTIVE], true) || PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_ACTIVE], true)) && !isOpen && delay <= 0.0f)
            {
                SoundManager.instance.StartAudio(new string[1] { "Object/active_F" }, VOLUME_TYPE.EFFECT);
                isSelect = false;
                if (isEndingDoor && !openSR.enabled)  StartCoroutine(Spake());
                else if (isEndingDoor && openSR.enabled)  StartCoroutine(EndingCredits());
                else if (isWarpOpenObj && PDG.Dungeon.instance.isGetWarpPart && !isRepair)
                {
                    SoundManager.instance.StartAudio(new string[1] { "Click_Maca" }, VOLUME_TYPE.EFFECT);
                    isUsing = true;
                    if (animator) animator.enabled = true;
                    delay = 0.1f;
                    PlayerCombat.instance.tCoroutine = StartCoroutine(TimerCoroutine());
                }
                else if (isWarpOpenObj && isRepair)
                {
                    SoundManager.instance.StartAudio(new string[1] { "Click_Maca" }, VOLUME_TYPE.EFFECT);
                    isUsing = true;
                    if (animator) animator.enabled = true;
                    delay = 0.1f;
                    PlayerCombat.instance.tCoroutine = StartCoroutine(TimerCoroutine());
                }
                else  isOpen = true;
                controllerObj.SetActive(false);
                delay = 0.1f;
            }
            if ((PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ACTIVE], true) || PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_ACTIVE], true)) && isOpen && delay <= 0.0f )
            {
                SoundManager.instance.StartAudio(new string[1] { "Object/active_F" }, VOLUME_TYPE.EFFECT);
                isSelect = false;
                isUsing = true;
                controllerObj.SetActive(false);
                if (animator) animator.enabled = true;
                delay = 0.1f;
                if (!isWarpOpenObj)
                    PlayerCombat.instance.tCoroutine = StartCoroutine(TimerCoroutine());
            }
        }
    }
    IEnumerator TimerCoroutine()
    {
        timerObj.SetActive(true);
        float t = 0.0f;
        while(isUsing)
        {
            Vector3 p = controllerObj.transform.position;
            p.y -= 0.5f;
            timerObj.transform.position = Camera.main.WorldToScreenPoint(p);

            if (audio && !audio.isPlaying)
            {
                if (!isWarpOpenObj) audio.clip = clips[Random.Range(0, clips.Length)];
                audio.volume =  (SoundManager.isMute || (PDG.Dungeon.instance == null ? false : PDG.Dungeon.instance.isPause) || (PDG.Player.instance == null ? false : PDG.Player.instance.isDie)) ? 0 : SoundManager.EffectVolume;
                audio.Play();
            }

            if (TextOpenController.instance.targetObj == controllerObj && isLastMap)
            {
                TextOpenController.instance.TextClose(Color.white);
            }
            t += Time.deltaTime;
            int tInt = Mathf.CeilToInt(fTime - t);
            timerObj.transform.Find("Text").GetComponent<Text>().text = tInt.ToString();
            timerObj.transform.Find("Image").GetComponent<Image>().fillAmount = t / fTime;
            if (isWarpObj)
                timerObj.transform.Find("TextCont").GetComponent<Text>().text = LocalizeManager.GetLocalize("repair_ing2");
            else if (isWarpOpenObj)
                timerObj.transform.Find("TextCont").GetComponent<Text>().text = LocalizeManager.GetLocalize("operating");
            else if (isTotem) 
                timerObj.transform.Find("TextCont").GetComponent<Text>().text = LocalizeManager.GetLocalize("totem_using");
            else
                timerObj.transform.Find("TextCont").GetComponent<Text>().text = LocalizeManager.GetLocalize("cabinet_ing");
            if (t >= fTime) break;
            yield return new WaitForSeconds(Time.deltaTime);
        }
        audio.Stop();
        if (isUsing) 
        {
            if (isWarpObj)
            {
                List<string> itemList2 = new List<string>();
                Vector3 pos = transform.position;
                pos.y -= 2.0f;
                Sprite s = Resources.Load<Sprite>("Item/Material/warp/CaseParts_" + (isTutorial ? 1 : PDG.Dungeon.instance.warpType));
                GameObject obj = ObjManager.Call().GetObject("Item");
                if (obj != null)
                {
                    Item _item = obj.GetComponent<Item>();
                    _item._Item = new ItemInfo
                    {
                        Data = new ItemsData
                        {
                            Type1 = "Material",
                            Type2 = "warp",
                            ID = "CaseParts_" + (isTutorial ? 1 : PDG.Dungeon.instance.warpType)
                        }
                    };
                    _item.dir = Vector3.zero;
                    _item.rPow = 0.0f;
                    _item.itemCount = 1;
                    obj.transform.position = pos;
                    obj.SetActive(true);
                }
                //Inventorys.Instance.DropItem("CaseParts_" + PDG.Dungeon.instance.warpType, 1, "warp", true);
                TextOpenController.instance.ShowText(LocalizeManager.GetLocalize(thirdCommnet), Color.white, 24, controllerObj, 0.5f, true);
                GetComponent<SpriteOutline>().Clear();
            }
            else if (isWarpOpenObj && !isRepair)
            {
                isSelect = false;
                if (PDG.Dungeon.instance.newObjectSelectedObj == gameObject)
                {
                    PDG.Dungeon.instance.newObjectSelectedObj = null;
                    PDG.Dungeon.instance.newObjectSelected = false;
                }
                isOpen = false;
                isUsing = false;
                isRepair = true;
                PDG.Dungeon.instance.isGetWarpPart = false;
                if (transform.Find("Particle")) transform.Find("Particle").gameObject.SetActive(false);
                SoundManager.instance.StartAudio(new string[1] { "Object/repair_clear" }, VOLUME_TYPE.EFFECT);
                //TextOpenController.instance.ShowText(LocalizeManager.GetLocalize("repair_clear"), Color.white, 24, controllerObj, 0.5f, true);
                if (controllerObj.GetComponent<AudioSource>()) controllerObj.GetComponent<AudioSource>().Play();
                if (animator) animator.SetTrigger("Open");
                PDG.Dungeon.instance.isOpenWarp = true;
                GetComponent<SpriteOutline>().Clear();
                if(isTutorial)
                {
                    Dialogue.Instance.CommnetSetting("tutorial_four_map_npc_2", 0, null);
                }
            }
            else if (isWarpOpenObj && isRepair)
            {
                isSelect = false;
                if (PDG.Dungeon.instance.newObjectSelectedObj == gameObject)
                {
                    PDG.Dungeon.instance.newObjectSelectedObj = null;
                    PDG.Dungeon.instance.newObjectSelected = false;
                }
                isOpen = false;
                isUsing = false;
                if (!PDG.Dungeon.instance.isOpenWarp)
                {
                    if (animator) animator.SetTrigger("Open");
                    PDG.Dungeon.instance.isOpenWarp = true;
                }
                else
                {
                    if (animator) animator.SetTrigger("Close");
                    PDG.Dungeon.instance.isOpenWarp = false;
                }
                if (PDG.Dungeon.instance.warpType == 0)
                    SoundManager.instance.StartAudio(new string[1] { "Object/lever" }, VOLUME_TYPE.EFFECT);
                GetComponent<SpriteOutline>().Clear();
            }
            else if (isLastMap)
            {
                SoundManager.instance.StartAudio(new string[1] { "last_box" }, VOLUME_TYPE.EFFECT);
                UnlockListsData[] UnlockDatas = System.Array.FindAll(SheetManager.Instance.UnlockListsDB.dataArray, item => item.NPCID.Equals("lastBox"));
                for (int i = 0; i < UnlockDatas.Length; i++)
                {
                    if (SaveAndLoadManager.instance.LoadUnlock(UnlockDatas[i].Itemid) < 1)
                    {
                        SaveAndLoadManager.instance.SaveUnlock(UnlockDatas[i].Itemid, 1);
                        ItemsData iData = System.Array.Find(SheetManager.Instance.ItemsDB.dataArray, item => item.ID.Equals(UnlockDatas[i].Itemid));
                        Inventorys.Instance.ShowGetItemUI("item_unlock_special", Resources.Load<Sprite>("Item/" + iData.Type1 + "/" + iData.Type2 + "/" + iData.ID));
                    }
                }
                //Inventorys.Instance.DropItem("dark_cells", 1, string.Empty, false);
            }
            else if (isTotem)
            {
                GetComponent<SpriteRenderer>().sprite = offItem;
                for (int i = 0; i < offObj.Length; i++) offObj[i].SetActive(false);
                for (int i = 0; i < onObj.Length; i++) onObj[i].SetActive(true);

                bool check = false;
                bool check2 = false;
                for (int i = 0; i < tData.Diminishtype.Length; i++)
                {
                    controllerObj.transform.position = new Vector3(controllerObj.transform.position.x, controllerObj.transform.position.y - 2.0f, 0);
                    string tStr = tData.Diminishtype[i];
                    if (tStr.Equals("Random"))
                    {
                        if (Inventorys.Instance.DeleteItem(tStr, tData.Diminishamount[i]))
                        {
                            check = true;
                            TextOpenController.instance.ShowText(LocalizeManager.GetLocalize(thirdCommnet), Color.white, 24, controllerObj, 2);
                        }
                        else
                        {
                            TextOpenController.instance.ShowText(LocalizeManager.GetLocalize("totem_nothing"), Color.white, 24, controllerObj, 2);
                        }
                    }
                    else if (tStr.Equals("Passive"))
                    {
                        if (Inventorys.Instance.DeleteItem(tStr, tData.Diminishamount[i]))
                        {
                            check = true;
                            TextOpenController.instance.ShowText(LocalizeManager.GetLocalize(thirdCommnet), Color.white, 24, controllerObj, 2);
                        }
                        else
                        {
                            TextOpenController.instance.ShowText(LocalizeManager.GetLocalize("totem_nothing"), Color.white, 24, controllerObj, 2);
                        }
                    }
                    else if (tStr.Equals("Health"))
                    {
                        if (Inventorys.Instance.DeleteItem(tStr, tData.Diminishamount[i]))
                        {
                            check = true;
                            check2 = true;
                            TextOpenController.instance.ShowText(LocalizeManager.GetLocalize(thirdCommnet), Color.white, 24, controllerObj, 2);
                        }
                        else
                        {
                            TextOpenController.instance.ShowText(LocalizeManager.GetLocalize("totem_nothing"), Color.white, 24, controllerObj, 2);
                        }
                    }
                    else if (tStr.Equals("Attack"))
                    {
                        if (Inventorys.Instance.DeleteItem(tStr, tData.Diminishamount[i]))
                        {
                            check = true;
                            check2 = true;
                            TextOpenController.instance.ShowText(LocalizeManager.GetLocalize(thirdCommnet), Color.white, 24, controllerObj, 0.5f, true);
                        }
                        else
                        {
                            TextOpenController.instance.ShowText(LocalizeManager.GetLocalize("totem_nothing"), Color.white, 24, controllerObj, 0.5f, true);
                        }
                    }
                    else if(tStr.Equals("None"))
                    {
                        check = true;
                        TextOpenController.instance.ShowText(LocalizeManager.GetLocalize(thirdCommnet), Color.white, 24, controllerObj, 3);
                    }
                }
                if (check2 && !check)
                {
                    TextOpenController.instance.ShowText(LocalizeManager.GetLocalize(thirdCommnet), Color.white, 24, controllerObj, 0.5f, true);
                }
                if (check)
                {
                    for (int i = 0; i < tData.Increasetype.Length; i++)
                    {
                        string tStr = tData.Increasetype[i];
                        if (tStr.Equals("Health"))
                        {
                            PDG.Player.instance.plusHp += tData.Increaseamount[i];
                            PDG.Dungeon.curseHealth -= tData.Increaseamount[i];
                            PDG.Player.instance.HpRecovery(0);
                        }
                        else if (tStr.Equals("Attack"))
                        {
                            PDG.Dungeon.curseAttack -= tData.Increaseamount[i];
                            PDG.Player.instance.damagePlus += tData.Increaseamount[i];
                            PDG.Player.instance.damagePlusNear += tData.Increaseamount[i];
                            PDG.Player.instance.damageQPlus += tData.Increaseamount[i];
                            PDG.Player.instance.damageQPlusNear += tData.Increaseamount[i];
                            PDG.Player.instance.damageRightPlus += tData.Increaseamount[i];
                            PDG.Player.instance.damageRightPlusNear += tData.Increaseamount[i];
                        }
                    }
                }

            }
            else
            {
                if (Random.Range(0, 10) > 5)
                {
                    List<string> itemList2 = new List<string>();
                    ItemsData[] datas = System.Array.FindAll(SheetManager.Instance.ItemsDB.dataArray, item => item.Type1.Equals("Weapon"));
                    for (int i = 0; i < datas.Length; i++)
                    {
                        Sprite s = Resources.Load<Sprite>("Item/" + datas[i].Type1 + "/" + datas[i].Type2 + "/" + datas[i].ID);
                        if (s == null) continue;
                        if (datas[i].Grade.Equals("F")) continue;
                        if (datas[i].Grade.Equals("G")) continue;
                        if (Inventorys.Instance.UnlockAndMaxCountCheck(datas[i].ID))
                        {
                            itemList2.Add(datas[i].ID);

                        }
                    }
                    string info = itemList2[Random.Range(0, itemList2.Count)];
                    string tempID = info;
                    int ttt = tempID.LastIndexOf('_');
                    if (ttt != -1 && ttt == tempID.Length - 2)
                        tempID = tempID.Substring(0, tempID.Length - 2);
                    SaveAndLoadManager.instance.SaveItem(new ItemSave { _id = "EquipmentSave_" + tempID, _nCount = 1 });
                    Inventorys.Instance.DropItem(info, 1, string.Empty, false);
                    TextOpenController.instance.ShowText(LocalizeManager.GetLocalize(thirdCommnet), Color.white, 24, controllerObj, 0.5f, true);
                    GetComponent<SpriteOutline>().Clear();
                }
                else
                {
                    TextOpenController.instance.ShowText(LocalizeManager.GetLocalize(thirdCommnet2), Color.white, 24, controllerObj, 0.5f, true);
                    GetComponent<SpriteOutline>().Clear();
                }
            }
            isUse = true;
            if (PDG.Dungeon.instance.newObjectSelectedObj == gameObject)
            {
                PDG.Dungeon.instance.newObjectSelectedObj = null;
                PDG.Dungeon.instance.newObjectSelected = false;
            }
            if (transform.Find("minimap")) transform.Find("minimap").gameObject.SetActive(false);
        }
        timerObj.SetActive(false);
        PlayerCombat.instance.tCoroutine = null;
        if (PDG.Dungeon.instance.isTestMode2 && isTotem)
        {
            yield return new WaitForSeconds(1.5f);
            ResetTotem();
        }
    }
    IEnumerator Spake()
    {
        PDG.Dungeon.instance.isEndingDoorOpen = true;
        float a = 0.0f;
        while (true)
        {
            a += Time.deltaTime * 4;
            spakeImage.color = new Color(1, 1, 1, a);
            if (a > 1.0f) break;
            yield return new WaitForSeconds(Time.deltaTime);
        }
        spakeImage.color = new Color(1, 1, 1, 1);
        a = 1.0f;
        openSR.enabled = true;
        SoundManager.instance.StartAudio(new string[1] { "Object/lastDoor" }, VOLUME_TYPE.EFFECT);
        while (true)
        {
            a -= Time.deltaTime * 4;
            spakeImage.color = new Color(1, 1, 1, a);
            if (a <= 0.0f) break;
            yield return new WaitForSeconds(Time.deltaTime);
        }
        spakeImage.color = new Color(1, 1, 1, 0);
    }
    IEnumerator EndingCredits()
    {
        isEndingCredits = true;
        TextOpenController.instance.TextClose(Color.white);
        PDG.Player.instance.gameObject.SetActive(false);
        if (animObj) animObj.gameObject.SetActive(true);
        if (animObj) animObj.Rebind();
        yield return new WaitForSecondsRealtime(2.0f);
        SaveAndLoadManager.instance.GameDelete();
        SceneManager.LoadScene("EndingCredits");
    }
}
