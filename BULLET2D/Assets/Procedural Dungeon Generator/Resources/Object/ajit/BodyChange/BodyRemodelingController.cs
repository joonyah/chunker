﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BodyRemodelingController : MonoBehaviour
{
    [SerializeField] private bool isSelect = false;
    public bool isOpen = false;
    [SerializeField] private GameObject controllerObj;
    [SerializeField] private SpriteRenderer controllerSR;
    GameObject panel;
    [SerializeField] private bool isBossRush = false;
    [SerializeField] private GameObject tutorialPrefabs;
    private GameObject tutorialObj;
    // Start is called before the first frame update
    void Start()
    {
        panel = GameObject.Find("Canvas").transform.Find("BodyRemodeling").gameObject;
        int t = SaveAndLoadManager.instance.LoadItem("TUTORIAL_BODY");
        if (t != 1)
        {
            tutorialObj = Instantiate(tutorialPrefabs);
            tutorialObj.transform.SetParent(GameObject.Find("Canvas").transform);
            tutorialObj.transform.localScale = Vector3.one;
            tutorialObj.transform.SetSiblingIndex(26);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (tutorialObj)
        {
            tutorialObj.transform.Find("Text").GetComponent<UnityEngine.UI.Text>().text = LocalizeManager.GetLocalize("tutorial_body");
            Vector3 pos = Camera.main.WorldToScreenPoint(controllerObj.transform.position);
            pos.y += 15;
            tutorialObj.transform.position = pos;
        }

        RaycastHit2D hit = Physics2D.BoxCast(transform.position + new Vector3(0, 0.5f), new Vector2(3.0f, 4), 0, Vector2.zero, 0, 1 << 9);
        if (hit && !isSelect && !isOpen && !PDG.Dungeon.instance.newObjectSelected)
        {
            isSelect = true;
            PDG.Dungeon.instance.newObjectSelected = true;
            PDG.Dungeon.instance.newObjectSelectedObj = gameObject;
            controllerObj.GetComponent<SpriteRenderer>().sprite = PDG.Dungeon.instance.isJoystrick ? PDG.Dungeon.GetJoyStrickKeySprite(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_ACTIVE]) : PDG.Dungeon.GetJoyStrickKeySprite(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ACTIVE]);
            controllerObj.SetActive(true);
            TextOpenController.instance.ShowText(LocalizeManager.GetLocalize("bodyremodeling"), Color.white, 24, controllerObj, 2.0f, true);
        }
        else if (!hit)
        {
            isSelect = false;
            if (PDG.Dungeon.instance.newObjectSelectedObj == gameObject)
            {
                PDG.Dungeon.instance.newObjectSelectedObj = null;
                PDG.Dungeon.instance.newObjectSelected = false;
            }
            if (TextOpenController.instance.targetObj == controllerObj)
            {
                TextOpenController.instance.TextClose(Color.white);
            }
            controllerObj.SetActive(false);
        }
        if (isSelect)
        {
            if ((PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ACTIVE], true) || PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_ACTIVE], true)) && PDG.Dungeon.instance.closeCoolTime > 0.2f)
            {
                PDG.Dungeon.instance.closeCoolTime = 0.0f;
                SoundManager.instance.StartAudio(new string[1] { "Object/active_F" }, VOLUME_TYPE.EFFECT);
                isSelect = false;
                if (PDG.Dungeon.instance.newObjectSelectedObj == gameObject)
                {
                    PDG.Dungeon.instance.newObjectSelectedObj = null;
                    PDG.Dungeon.instance.newObjectSelected = false;
                }
                isOpen = true;
                controllerObj.SetActive(false);
                SaveAndLoadManager.instance.SaveItem(new ItemSave { _id = "TUTORIAL_BODY", _nCount = 1 }, true);
                if (tutorialObj) Destroy(tutorialObj);
                StartCoroutine(OpenCanvas());
            }
        }
    }
    IEnumerator OpenCanvas()
    {
        BodyRemodelingManager.instance.isOpen = true;
        PDG.Player.instance.isCameraOff = true;
        StartCoroutine(PDG.Dungeon.instance.UiOff());
        GameObject.Find("Canvas").transform.Find("HpShieldPanel").gameObject.SetActive(false);
        GameObject.Find("Canvas").transform.Find("MiniMap_OutLine").gameObject.SetActive(false);
        float z = 8.0f;
        while (true)
        {
            z -= Time.deltaTime * 5;
            Camera.main.orthographicSize = z;

            if (z <= 3.0f)
            {
                Camera.main.orthographicSize = 3.0f;
                break;
            }

            yield return new WaitForSeconds(Time.deltaTime / 10);
        }
        BodyRemodelingManager.instance.Show(this, isBossRush);
    }
    public void Exit()
    {
        StartCoroutine(ExitCoroutine());
    }
    IEnumerator ExitCoroutine()
    {
        BodyRemodelingManager.instance.isOpen = false;
        BodyRemodelingManager.instance.Close();
        float z = 3.0f;
        while (true)
        {
            z += Time.deltaTime * 5;
            Camera.main.orthographicSize = z;
            if (z >= 8.0f)
            {
                Camera.main.orthographicSize = 8.0f;
                break;
            }

            yield return new WaitForSeconds(Time.deltaTime / 10);
        }
        PDG.Player.instance.isCameraOff = false;
        GameObject.Find("Canvas").transform.Find("HpShieldPanel").gameObject.SetActive(true);
        GameObject.Find("Canvas").transform.Find("MiniMap_OutLine").gameObject.SetActive(true);
        StartCoroutine(PDG.Dungeon.instance.UiOn());
        isOpen = false;
    }
}
