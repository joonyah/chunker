﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class BodyRemodelingManager : MonoBehaviour
{
    public static BodyRemodelingManager instance;
    [SerializeField] private GameObject obj;
    [SerializeField] private BodyRemodelingItem[] PickItems;
    [SerializeField] private bool[] PickItemsSelected;
    [SerializeField] private int nowPickCount = 3;
    [SerializeField] private GameObject pickCrossHair;
    [SerializeField] private bool isPick = false;
    [SerializeField] private BodyRemodelingItem pickItem = null;
    [SerializeField] private BodyRemodelingController controller;
    [SerializeField] private bool isChoice = false;

    [SerializeField] private Button prevBtn;
    [SerializeField] private Button nextBtn;

    [SerializeField] private Animator MonitorAnim;
    [SerializeField] private RuntimeAnimatorController[] anims;
    [SerializeField] private int nowAnimNum = 0;

    [SerializeField] private Text MonitorTextQ;
    [SerializeField] private int nowQType = 0;

    [SerializeField] private Text attackTypeText;
    [SerializeField] private Text healthText;
    [SerializeField] private Text attackText;
    [SerializeField] private Text specialText;
    [SerializeField] private string specialID = string.Empty;
    public bool isOpen = false;
    private int rotateNum = 0;
    private int rotateNum2 = 0;
    [SerializeField] private GameObject joystrickReset;
    [SerializeField] private GameObject bodyremodeling_select_text;
    [SerializeField] private GameObject bodyremodeling_select_text2;
    bool isBossRush = false;
    private void Awake()
    {
        if (instance == null) instance = this;
        PickItemsSelected = new bool[PickItems.Length];
    }
    // Start is called before the first frame update
    void Start()
    {
        ReSetPoint(false);
    }
    public void ReSetPoint(bool _isDie)
    {
        if (_isDie) isChoice = false;
        for (int i = 0; i < PickItems.Length; i++)
        {
            if (i < nowPickCount)
                PickItemsSelected[i] = true;
            else
                PickItemsSelected[i] = false;
        }
    }
    // Update is called once per frame
    void Update()
    {
        float x = Input.GetAxisRaw("Horizontal_joy");
        float y = Input.GetAxisRaw("Vertical_joy");


        if (pickItem == null)
        {
            if (joystrickReset.activeSelf && (!PDG.Dungeon.instance.isJoystrick && !isBossRush)) joystrickReset.SetActive(false);
            if (pickCrossHair.activeSelf) pickCrossHair.SetActive(false);
        }
        else if (!PDG.Dungeon.instance.isJoystrick)
        {
            if (joystrickReset.activeSelf && !isBossRush) joystrickReset.SetActive(false);
            if (!pickCrossHair.activeSelf) pickCrossHair.SetActive(true);
            pickCrossHair.transform.position = Input.mousePosition;
        }

        if (!joystrickReset.activeSelf && (PDG.Dungeon.instance.isJoystrick || isBossRush)) joystrickReset.SetActive(true);
        if (controller != null && controller.isOpen && (Input.GetKeyDown(KeyCode.R)) && PDG.Dungeon.instance.closeCoolTime > 0.2f)
        {
            PDG.Dungeon.instance.closeCoolTime = 0.0f;
            if (isChoice)
            {
                if (!bodyremodeling_select_text2.activeSelf)
                {
                    bodyremodeling_select_text2.SetActive(true);
                }
                else
                {
                    StartCoroutine(ReSetCoroutine());
                    SoundManager.instance.StartAudio(new string[1] { "Object/body_choice" }, VOLUME_TYPE.EFFECT);
                    isChoice = false;
                    bodyremodeling_select_text2.SetActive(false);
                }
            }
        }
        if (controller != null && controller.isOpen && (Input.GetKeyDown(KeyCode.Escape) || PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_CANCLE], true)) && PDG.Dungeon.instance.closeCoolTime > 0.2f) 
        {
            PDG.Dungeon.instance.closeCoolTime = 0.0f;
            if (bodyremodeling_select_text.activeSelf)
            {
                bodyremodeling_select_text.SetActive(false);
            }
            else
            {
                if (!isChoice) ReSetPoint(false);
                controller.Exit();
            }
        }
        if (controller != null && controller.isOpen && (Input.GetKeyDown(KeyCode.Space) || PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_CONFIRM], true)) && PDG.Dungeon.instance.closeCoolTime > 0.2f)
        {
            PDG.Dungeon.instance.closeCoolTime = 0.0f;
            if (!isChoice)
            {
                if (!bodyremodeling_select_text.activeSelf)
                {
                    if (isBossRush)
                        bodyremodeling_select_text.transform.Find("Text").GetComponent<SetLanguage>().InitSetting("bodyremodeling_select_text2");
                    else
                        bodyremodeling_select_text.transform.Find("Text").GetComponent<SetLanguage>().InitSetting("bodyremodeling_select_text");
                    bodyremodeling_select_text.SetActive(true);
                }
                else
                {
                    StartCoroutine(SpakeCoroutine());
                    SoundManager.instance.StartAudio(new string[1] { "Object/body_choice" }, VOLUME_TYPE.EFFECT);
                    isChoice = true;
                    bodyremodeling_select_text.SetActive(false);
                }
            }
        }
        if (controller != null && controller.isOpen && (PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ARROW_RIGHT], true) || Input.GetKeyDown(KeyCode.RightArrow) || (x > 0 && !isPick)) && !isChoice && PDG.Dungeon.instance.closeCoolTime > 0.2f)
        {
            PDG.Dungeon.instance.closeCoolTime = 0.0f;
            nextBtn.onClick.Invoke();
        }
        if (controller != null && controller.isOpen && (PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ARROW_LEFT], true) || Input.GetKeyDown(KeyCode.LeftArrow) || (x < 0 && !isPick)) && !isChoice && PDG.Dungeon.instance.closeCoolTime > 0.2f)
        {
            PDG.Dungeon.instance.closeCoolTime = 0.0f;
            prevBtn.onClick.Invoke();
        }
        if (PDG.Dungeon.instance.isJoystrick)
        {
            if (controller != null && controller.isOpen && PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_RECYCLE], true) && !isChoice && PDG.Dungeon.instance.closeCoolTime > 0.2f)
            {
                PDG.Dungeon.instance.closeCoolTime = 0.0f;
                if(isChoice && isBossRush)
                {
                    if (!bodyremodeling_select_text2.activeSelf)
                    {
                        bodyremodeling_select_text2.SetActive(true);
                    }
                    else
                    {
                        StartCoroutine(ReSetCoroutine());
                        SoundManager.instance.StartAudio(new string[1] { "Object/body_choice" }, VOLUME_TYPE.EFFECT);
                        isChoice = false;
                        bodyremodeling_select_text2.SetActive(false);
                    }
                }
                else
                {
                    if (!isChoice) ReSetPoint(false);
                    Show(controller, isBossRush);
                }
            }
            if (controller != null && controller.isOpen && !isPick && PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_ACTIVE], true) && !isChoice && PDG.Dungeon.instance.closeCoolTime > 0.2f)
            {
                PDG.Dungeon.instance.closeCoolTime = 0.0f;
                for (int i = 0; i < PickItemsSelected.Length; i++)
                {
                    if (PickItemsSelected[i])
                    {
                        if (i < 6)
                        {
                            PickItem(PickItems[i], PickItemsSelected[i]);
                            Vector3 pos = PickItems[i].transform.position;
                            pos.y += 3;
                            pickCrossHair.transform.position = pos;
                            rotateNum2 = i;
                            break;
                        }
                    }
                }
                if (!pickCrossHair.activeSelf) pickCrossHair.SetActive(true);
                rotateNum = 0;
            }
            if (controller != null && controller.isOpen && isPick && PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_ACTIVE], true) && !isChoice && PDG.Dungeon.instance.closeCoolTime > 0.2f)
            {
                PDG.Dungeon.instance.closeCoolTime = 0.0f;
                PickItem(PickItems[rotateNum2], PickItemsSelected[rotateNum2]);
                rotateNum = 0;
            }
            if (controller != null && controller.isOpen && (x < 0 && isPick) && !isChoice && PDG.Dungeon.instance.closeCoolTime > 0.2f)
            {
                PDG.Dungeon.instance.closeCoolTime = 0.0f;
                bool c = false;
                rotateNum--;
                if (rotateNum < 0) rotateNum = 2;
                while (true)
                {
                    for (int i = 0; i < PickItemsSelected.Length; i++)
                    {
                        if (rotateNum == 0 && i < 6 && !PickItemsSelected[i])
                        {
                            Vector3 pos = PickItems[i].transform.position;
                            pos.y += 3;
                            pickCrossHair.transform.position = pos;
                            rotateNum2 = i;
                            c = true;
                        }
                        else if (rotateNum == 1 && i < 12 && !PickItemsSelected[i])
                        {
                            Vector3 pos = PickItems[i].transform.position;
                            pos.y += 3;
                            pickCrossHair.transform.position = pos;
                            rotateNum2 = i;
                            c = true;
                        }
                        else if (rotateNum == 2 && !PickItemsSelected[i])
                        {
                            Vector3 pos = PickItems[i].transform.position;
                            pos.y += 3;
                            pickCrossHair.transform.position = pos;
                            rotateNum2 = i;
                            c = true;
                        }
                    }

                    if (c) break;
                    else
                    {
                        rotateNum--;
                        if (rotateNum < 0) rotateNum = 2;
                    }
                }
            }
            if (controller != null && controller.isOpen && (x > 0 && isPick) && !isChoice && PDG.Dungeon.instance.closeCoolTime > 0.2f)
            {
                PDG.Dungeon.instance.closeCoolTime = 0.0f;
                bool c = false;
                rotateNum++;
                if (rotateNum > 2) rotateNum = 0;
                while (true)
                {
                    for (int i = 0; i < PickItemsSelected.Length; i++)
                    {
                        if (rotateNum == 0 && i < 6 && !PickItemsSelected[i])
                        {
                            Vector3 pos = PickItems[i].transform.position;
                            pos.y += 3;
                            pickCrossHair.transform.position = pos;
                            rotateNum2 = i;
                            c = true;
                        }
                        else if (rotateNum == 1 && i < 12 && !PickItemsSelected[i])
                        {
                            Vector3 pos = PickItems[i].transform.position;
                            pos.y += 3;
                            pickCrossHair.transform.position = pos;
                            rotateNum2 = i;
                            c = true;
                        }
                        else if (rotateNum == 2 && !PickItemsSelected[i])
                        {
                            Vector3 pos = PickItems[i].transform.position;
                            pos.y += 3;
                            pickCrossHair.transform.position = pos;
                            rotateNum2 = i;
                            c = true;
                        }
                    }

                    if (c) break;
                    else
                    {
                        rotateNum++;
                        if (rotateNum > 2) rotateNum = 0;
                    }
                }
            }
        }
        if (controller != null && controller.isOpen)
        {
            int health = 0;
            int attack = 0;
            int special = 0;
            for (int i = 0; i < PickItemsSelected.Length; i++)
            {
                if (PickItemsSelected[i]) 
                {
                    if (i < 6) health++;
                    else if (i < 12) attack++;
                    else special++;
                }
            }
            attackTypeText.text = LocalizeManager.GetLocalize("bodyremodeling_attacktype") + LocalizeManager.GetLocalize("bodyremodeling_attacktype_" + nowAnimNum);
            healthText.text = LocalizeManager.GetLocalize("bodyremodeling_health") + health;
            attackText.text = LocalizeManager.GetLocalize("bodyremodeling_attack") + attack;
            if (isChoice)
                specialText.text = LocalizeManager.GetLocalize("bodyremodeling_special") + "\n\n<size=18>" + (special == 0 ? "-" : LocalizeManager.GetLocalize(specialID + "_cont")) + "</size>";
            else
                specialText.text = LocalizeManager.GetLocalize("bodyremodeling_special") + "\n\n<size=18>" + (special == 0 ? "-" : "???") + "</size>";
        }
    }
    public void Show(BodyRemodelingController _controller, bool _isBossRush)
    {
        isBossRush = _isBossRush;
        MonitorTextQ.text = LocalizeManager.GetLocalize("bodyremodeling_q_type_" + nowQType);
        SoundManager.instance.StartAudio(new string[1] { "Object/body_onoff" }, VOLUME_TYPE.EFFECT);
        isOpen = true;
        controller = _controller;
        for (int i = 0; i < PickItems.Length; i++)
        {
            PickItems[i].InitSetting(PickItemsSelected[i]);
        }
        GetComponent<Image>().enabled = true;
        obj.SetActive(true);
    }
    public void Close()
    {
        SoundManager.instance.StartAudio(new string[1] { "Object/body_onoff" }, VOLUME_TYPE.EFFECT);
        isOpen = false;
        pickItem = null;
        isPick = false;
        pickCrossHair.SetActive(false);
        GetComponent<Image>().enabled = false;
        obj.SetActive(false);
    }
    public void PickItem(BodyRemodelingItem _item, bool _isOpen)
    {
        if (isChoice) return;
        if (!_isOpen && !isPick) return;

        if (_isOpen && !isPick)
        {
            SoundManager.instance.StartAudio(new string[1] { "Object/body_pick" }, VOLUME_TYPE.EFFECT);
            pickItem = _item;
            pickItem.InitSetting(false);
            isPick = true;
        }

        if (!_isOpen && isPick)
        {
            SoundManager.instance.StartAudio(new string[1] { "Object/body_pick_down" }, VOLUME_TYPE.EFFECT);
            for (int i = 0; i < PickItems.Length; i++)
            {
                if (PickItems[i] == pickItem) PickItemsSelected[i] = false;
                if (PickItems[i] == _item) PickItemsSelected[i] = true;
            }
            _item.InitSetting(true);
            pickItem = null;
            isPick = false;
        }
    }
    IEnumerator SpakeCoroutine()
    {
        float a = 0.0f;
        Image img = GameObject.Find("Canvas").transform.Find("Spake").GetComponent<Image>();
        while (true)
        {
            img.color = new Color(1, 1, 1, a);
            a += Time.deltaTime * 2.5f;
            if (a >= 1.0f) break;
            yield return null;
        }

        int health = 0;
        int attack = 0;
        int special = 0;
        for (int i = 0; i < PickItemsSelected.Length; i++)
        {
            if (PickItemsSelected[i])
            {
                if (i < 6) health++;
                else if (i < 12) attack++;
                else special++;
            }
        }

        prevDamage = attack;
        prevHealth = health;
        if (special == 0) prevSpecial = false;
        else prevSpecial = true;

        PDG.Dungeon.curseAttack2 -= attack;
        PDG.Dungeon.curseHealth2 += (3 - health);

        PDG.Player.instance.damagePlus -= PDG.Dungeon.curseAttack;
        PDG.Player.instance.damagePlusNear -= PDG.Dungeon.curseAttack;
        PDG.Player.instance.damageQPlus -= PDG.Dungeon.curseAttack;
        PDG.Player.instance.damageQPlusNear -= PDG.Dungeon.curseAttack;
        PDG.Player.instance.damageRightPlus -= PDG.Dungeon.curseAttack;
        PDG.Player.instance.damageRightPlusNear -= PDG.Dungeon.curseAttack;

        PDG.Player.instance.damagePlus -= PDG.Dungeon.curseAttack2;
        PDG.Player.instance.damagePlusNear -= PDG.Dungeon.curseAttack2;
        PDG.Player.instance.damageQPlus -= PDG.Dungeon.curseAttack2;
        PDG.Player.instance.damageQPlusNear -= PDG.Dungeon.curseAttack2;
        PDG.Player.instance.damageRightPlus -= PDG.Dungeon.curseAttack2;
        PDG.Player.instance.damageRightPlusNear -= PDG.Dungeon.curseAttack2;

        PDG.Player.instance.plusHp -= PDG.Dungeon.curseHealth;
        PDG.Player.instance.plusHp -= PDG.Dungeon.curseHealth2;

        PDG.Player.instance.HpRecovery(0);
        if (special > 0)
        {
            ReinforceData[] useData = System.Array.FindAll(SheetManager.Instance.ReinforceDB.dataArray, item => item.Useflag);
            int ttt = Random.Range(0, useData.Length);
            specialID = useData[ttt].ID;
            PlayerBuffController.instance.AddBuff(specialID);
            SaveAndLoadManager.specialID = ttt;
        }

        if (nowAnimNum == 1)
        {
            // 아이템 레벨 체크
            int tLeft = SaveAndLoadManager.instance.LoadUnlock("start_gun_1");
            string startLeft = string.Empty;
            if (tLeft == 0) startLeft = "start_gun";
            else
            {
                tLeft = SaveAndLoadManager.instance.LoadUnlock("start_gun_2");
                if (tLeft == 0) startLeft = "start_gun_1";
                else startLeft = "start_gun_2";
            }
            PDG.Dungeon.instance.isStartGun = true;
            ItemsData iData = System.Array.Find(SheetManager.Instance.ItemsDB.dataArray, item => item.ID.Equals(startLeft));
            SaveAndLoadManager.instance.SaveCollection(new CollectionData { _id = iData.ID, _isOpen = true, _isWeapon = true });
            for (int i = 0; i < Inventorys.Instance.slots.Length; i++)
            {
                if (Inventorys.Instance.slots[i]._Item != null)
                {
                    if (Inventorys.Instance.slots[i]._Item.Data.Type3.Equals(iData.Type3))
                    {
                        ItemInfo newInfo = new ItemInfo
                        {
                            Data = Inventorys.Instance.CopyItem(iData)
                        };
                        ItemInfo info = Inventorys.Instance.slots[i]._Item;
                        Inventorys.Instance.DeleteBuff(Resources.Load<Sprite>("Item/" + info.Data.Type1 + "/" + info.Data.Type2 + "/" + info.Data.ID));
                        Inventorys.Instance.slots[i].itemCount = 1;
                        Inventorys.Instance.slots[i].SetItem(newInfo);
                        Inventorys.Instance.AddBuff(Resources.Load<Sprite>("Item/" + newInfo.Data.Type1 + "/" + newInfo.Data.Type2 + "/" + newInfo.Data.ID), newInfo.Data);
                        Inventorys.Instance.ShowGetItemUI(newInfo.Data.ID, Resources.Load<Sprite>("Item/" + newInfo.Data.Type1 + "/" + newInfo.Data.Type2 + "/" + newInfo.Data.ID));
                        Inventorys.Instance.SelectedItem(newInfo);
                        break;
                    }
                }
            }
            SoundManager.instance.StartAudio(new string[1] { "Object/coin" }, VOLUME_TYPE.EFFECT);

            string tempID = startLeft;
            int ttt = tempID.LastIndexOf('_');
            if (ttt != -1 && ttt == tempID.Length - 2)
                tempID = tempID.Substring(0, tempID.Length - 2);
            SaveAndLoadManager.instance.SaveItem(new ItemSave { _id = "EquipmentSave_" + tempID, _nCount = 1 });

            SaveAndLoadManager.instance.GameSave(Inventorys.Instance.slots);
            PDG.Player.instance.SettingItem(iData);
        }
        else
            PDG.Dungeon.instance.isStartGun = false;

        PDG.Dungeon.instance.nQChange = nowQType;
        if(nowQType != 0)
        {
            ItemsData iData = System.Array.Find(SheetManager.Instance.ItemsDB.dataArray, item => item.ID.Equals("q_change"));
            SaveAndLoadManager.instance.SaveCollection(new CollectionData { _id = iData.ID, _isOpen = true, _isWeapon = true });
            for (int i = 0; i < Inventorys.Instance.slots.Length; i++)
            {
                if (Inventorys.Instance.slots[i]._Item != null)
                {
                    if (Inventorys.Instance.slots[i]._Item.Data.Type3.Equals(iData.Type3))
                    {
                        ItemInfo newInfo = new ItemInfo
                        {
                            Data = Inventorys.Instance.CopyItem(iData)
                        };
                        ItemInfo info = Inventorys.Instance.slots[i]._Item;
                        Inventorys.Instance.DeleteBuff(Resources.Load<Sprite>("Item/" + info.Data.Type1 + "/" + info.Data.Type2 + "/" + info.Data.ID));
                        Inventorys.Instance.slots[i].itemCount = 1;
                        Inventorys.Instance.slots[i].SetItem(newInfo);
                        Inventorys.Instance.AddBuff(Resources.Load<Sprite>("Item/" + newInfo.Data.Type1 + "/" + newInfo.Data.Type2 + "/" + newInfo.Data.ID), newInfo.Data);
                        Inventorys.Instance.ShowGetItemUI(newInfo.Data.ID, Resources.Load<Sprite>("Item/" + newInfo.Data.Type1 + "/" + newInfo.Data.Type2 + "/" + newInfo.Data.ID));
                        Inventorys.Instance.SelectedItem(newInfo);
                        break;
                    }
                }
            }
            SoundManager.instance.StartAudio(new string[1] { "Object/coin" }, VOLUME_TYPE.EFFECT);
            SaveAndLoadManager.instance.GameSave(Inventorys.Instance.slots);
            PDG.Player.instance.SettingItem(iData);
        }

        img.color = Color.white;
        while (true)
        {
            img.color = new Color(1, 1, 1, a);
            a -= Time.deltaTime * 2.5f;
            if (a <= 0.0f) break;
            yield return null;
        }
        img.color = new Color(1, 1, 1, 0);
    }
    public void NextMonitor()
    {
        SoundManager.instance.StartAudio(new string[1] { "Click_Arrow" }, VOLUME_TYPE.EFFECT);
        if (isChoice) return;
        nowAnimNum++;
        if (nowAnimNum >= anims.Length) nowAnimNum = 0;
        MonitorAnim.runtimeAnimatorController = anims[nowAnimNum];
    }
    public void PrevMonitor()
    {
        SoundManager.instance.StartAudio(new string[1] { "Click_Arrow" }, VOLUME_TYPE.EFFECT);
        if (isChoice) return;
        nowAnimNum--;
        if (nowAnimNum < 0) nowAnimNum = anims.Length - 1;
        MonitorAnim.runtimeAnimatorController = anims[nowAnimNum];
    }
    public void NextMonitor2()
    {
        SoundManager.instance.StartAudio(new string[1] { "Click_Arrow" }, VOLUME_TYPE.EFFECT);
        if (isChoice) return;
        nowQType++;
        if (nowQType >= 5) nowQType = 0;
        MonitorTextQ.text = LocalizeManager.GetLocalize("bodyremodeling_q_type_" + nowQType);
    }
    public void PrevMonitor2()
    {
        SoundManager.instance.StartAudio(new string[1] { "Click_Arrow" }, VOLUME_TYPE.EFFECT);
        if (isChoice) return;
        nowQType--;
        if (nowQType < 0) nowQType = 4;
        MonitorTextQ.text = LocalizeManager.GetLocalize("bodyremodeling_q_type_" + nowQType);
    }

    int prevHealth = 0;
    int prevDamage = 0;
    bool prevSpecial = false;
    IEnumerator ReSetCoroutine()
    {
        float a = 0.0f;
        Image img = GameObject.Find("Canvas").transform.Find("Spake").GetComponent<Image>();
        while (true)
        {
            img.color = new Color(1, 1, 1, a);
            a += Time.deltaTime * 2.5f;
            if (a >= 1.0f) break;
            yield return null;
        }

        PDG.Dungeon.curseAttack2 += prevDamage;
        PDG.Dungeon.curseHealth2 -= (3 - prevHealth);

        PDG.Player.instance.damagePlus += prevDamage;
        PDG.Player.instance.damagePlusNear += prevDamage;
        PDG.Player.instance.damageQPlus += prevDamage;
        PDG.Player.instance.damageQPlusNear += prevDamage;
        PDG.Player.instance.damageRightPlus += prevDamage;
        PDG.Player.instance.damageRightPlusNear += prevDamage;

        PDG.Player.instance.plusHp += (3 - prevHealth);

        PDG.Player.instance.HpRecovery((3 - prevHealth));
        if (prevSpecial)
        {
            ReinforceData[] useData = System.Array.FindAll(SheetManager.Instance.ReinforceDB.dataArray, item => item.Useflag);
            specialID = useData[SaveAndLoadManager.specialID].ID;
            PlayerBuffController.instance.DeleteBuff(specialID);
            SaveAndLoadManager.specialID = -1;
        }

        PDG.Dungeon.instance.isStartGun = false;

        {
            // 아이템 레벨 체크
            int tLeft = SaveAndLoadManager.instance.LoadUnlock("tentacle_arm_1");
            string startLeft = string.Empty;
            if (tLeft == 0) startLeft = "tentacle_arm";
            else startLeft = "tentacle_arm_1";
            ItemsData iData = System.Array.Find(SheetManager.Instance.ItemsDB.dataArray, item => item.ID.Equals(startLeft));
            for (int i = 0; i < Inventorys.Instance.slots.Length; i++)
            {
                if (Inventorys.Instance.slots[i]._Item != null)
                {
                    if (Inventorys.Instance.slots[i]._Item.Data.Type3.Equals(iData.Type3))
                    {
                        ItemInfo newInfo = new ItemInfo
                        {
                            Data = Inventorys.Instance.CopyItem(iData)
                        };
                        ItemInfo info = Inventorys.Instance.slots[i]._Item;
                        Inventorys.Instance.DeleteBuff(Resources.Load<Sprite>("Item/" + info.Data.Type1 + "/" + info.Data.Type2 + "/" + info.Data.ID));
                        Inventorys.Instance.slots[i].itemCount = 1;
                        Inventorys.Instance.slots[i].SetItem(newInfo);
                        Inventorys.Instance.AddBuff(Resources.Load<Sprite>("Item/" + newInfo.Data.Type1 + "/" + newInfo.Data.Type2 + "/" + newInfo.Data.ID), newInfo.Data);
                        Inventorys.Instance.ShowGetItemUI(newInfo.Data.ID, Resources.Load<Sprite>("Item/" + newInfo.Data.Type1 + "/" + newInfo.Data.Type2 + "/" + newInfo.Data.ID));
                        Inventorys.Instance.SelectedItem(newInfo);
                        break;
                    }
                }
            }
            PDG.Player.instance.SettingItem(iData);
        }

        PDG.Dungeon.instance.nQChange = 0;
        {
            ItemsData iData = System.Array.Find(SheetManager.Instance.ItemsDB.dataArray, item => item.ID.Equals("spiny_cell_firing"));
            for (int i = 0; i < Inventorys.Instance.slots.Length; i++)
            {
                if (Inventorys.Instance.slots[i]._Item != null)
                {
                    if (Inventorys.Instance.slots[i]._Item.Data.Type3.Equals(iData.Type3))
                    {
                        ItemInfo newInfo = new ItemInfo
                        {
                            Data = Inventorys.Instance.CopyItem(iData)
                        };
                        ItemInfo info = Inventorys.Instance.slots[i]._Item;
                        Inventorys.Instance.DeleteBuff(Resources.Load<Sprite>("Item/" + info.Data.Type1 + "/" + info.Data.Type2 + "/" + info.Data.ID));
                        Inventorys.Instance.slots[i].itemCount = 1;
                        Inventorys.Instance.slots[i].SetItem(newInfo);
                        Inventorys.Instance.AddBuff(Resources.Load<Sprite>("Item/" + newInfo.Data.Type1 + "/" + newInfo.Data.Type2 + "/" + newInfo.Data.ID), newInfo.Data);
                        Inventorys.Instance.ShowGetItemUI(newInfo.Data.ID, Resources.Load<Sprite>("Item/" + newInfo.Data.Type1 + "/" + newInfo.Data.Type2 + "/" + newInfo.Data.ID));
                        Inventorys.Instance.SelectedItem(newInfo);
                        break;
                    }
                }
            }
            SoundManager.instance.StartAudio(new string[1] { "Object/coin" }, VOLUME_TYPE.EFFECT);
            PDG.Player.instance.SettingItem(iData);
        }
        img.color = Color.white;
        while (true)
        {
            img.color = new Color(1, 1, 1, a);
            a -= Time.deltaTime * 2.5f;
            if (a <= 0.0f) break;
            yield return null;
        }
        img.color = new Color(1, 1, 1, 0);
    }
}