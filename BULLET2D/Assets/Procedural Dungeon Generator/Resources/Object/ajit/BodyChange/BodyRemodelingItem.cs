﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class BodyRemodelingItem : MonoBehaviour
{
    [SerializeField] private EventTrigger ET;
    [SerializeField] private Image image;
    public bool isOpen = false;
    // Start is called before the first frame update
    void Start()
    {
    }
    public void InitSetting(bool _isOpen)
    {
        ET.triggers.Clear();
        isOpen = _isOpen;
        if (isOpen)
            image.color = new Color(1, 1, 1, 1);
        else
            image.color = new Color(1, 1, 1, 0);

        EventTrigger.Entry entry = new EventTrigger.Entry();
        entry.eventID = EventTriggerType.PointerClick;
        entry.callback.AddListener((eventData) => { BodyRemodelingManager.instance.PickItem(this, isOpen); });
        ET.triggers.Add(entry);
    }
}
