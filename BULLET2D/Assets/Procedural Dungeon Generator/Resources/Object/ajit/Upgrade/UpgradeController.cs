﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UpgradeController : MonoBehaviour
{
    private SpriteRenderer sr;
    public Animator animator;

    [SerializeField] private AudioClip[] clips;
    [SerializeField] private AudioSource audio;

    [SerializeField] private GameObject controllerObj;
    [SerializeField] private SpriteRenderer controllerSR;
    [SerializeField] private bool isSelect = false;
    [SerializeField] private Vector2 vSize;
    [SerializeField] private Vector2 vOffSet;
    [SerializeField] private string firstComment;
    [SerializeField] private string secondComment;
    [SerializeField] private float fTime = 3.0f;

    bool isRepair = false;
    bool isOpen = false;
    public bool isUsing = false;

    int material_0 = 200;
    int material_1 = 60;
    int material_2 = 50;
    int material_3 = 70;

    GameObject timerObj;

    [SerializeField] private GameObject tutorialPrefabs;
    private GameObject tutorialObj;

    private void Awake()
    {
        audio = GetComponent<AudioSource>();
        animator = GetComponent<Animator>();
        clips = Resources.LoadAll<AudioClip>("Object/Dungeon_2_Computer");
        sr = GetComponent<SpriteRenderer>();
    }
    // Start is called before the first frame update
    void Start()
    {
        timerObj = GameObject.Find("Canvas").transform.Find("Timer").gameObject;
        int t = SaveAndLoadManager.instance.LoadItem("UpgradeFlag");
        if (t == 1)
        {
            isRepair = true;
            animator.enabled = true;
        }

        int t2 = SaveAndLoadManager.instance.LoadItem("TUTORIAL_UPGRADE");
        if (t2 != 1)
        {
            tutorialObj = Instantiate(tutorialPrefabs);
            tutorialObj.transform.SetParent(GameObject.Find("Canvas").transform);
            tutorialObj.transform.localScale = Vector3.one;
            tutorialObj.transform.SetSiblingIndex(26);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (tutorialObj)
        {
            tutorialObj.transform.Find("Text").GetComponent<UnityEngine.UI.Text>().text = LocalizeManager.GetLocalize("tutorial_upgrade");
            Vector3 pos = Camera.main.WorldToScreenPoint(controllerObj.transform.position);
            pos.y += 45;
            tutorialObj.transform.position = pos;
        }

        RaycastHit2D hit = Physics2D.BoxCast(transform.position + (Vector3)vOffSet, vSize, 0, Vector2.zero, 0, 1 << 9);
        if (hit && !isSelect && !isOpen && !TextOpenController.instance.isOpen && !PDG.Dungeon.instance.newObjectSelected)
        {
            isSelect = true;
            PDG.Dungeon.instance.newObjectSelected = true;
            PDG.Dungeon.instance.newObjectSelectedObj = gameObject;
            controllerObj.SetActive(true);
            controllerObj.GetComponent<SpriteRenderer>().sprite = PDG.Dungeon.instance.isJoystrick ? PDG.Dungeon.GetJoyStrickKeySprite(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_ACTIVE]) : PDG.Dungeon.GetJoyStrickKeySprite(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ACTIVE]);
            TextOpenController.instance.ShowText(LocalizeManager.GetLocalize(!isRepair ? firstComment : secondComment), Color.white, 24, controllerObj, 2.0f, true);
        }
        if (hit && !isSelect && isOpen)
        {
            isSelect = true;
            controllerObj.SetActive(true);
            TextOpenController.instance.ShowText(LocalizeManager.GetLocalize("upgrade_repair_material"), Color.white, 24, controllerObj, 2.0f, true);
        }
        else if (!hit)
        {
            isSelect = false;
            if (PDG.Dungeon.instance.newObjectSelectedObj == gameObject)
            {
                PDG.Dungeon.instance.newObjectSelectedObj = null;
                PDG.Dungeon.instance.newObjectSelected = false;
            }
            isOpen = false;
            isUsing = false;
            if (audio) audio.Stop();
            controllerObj.SetActive(false);
            if (TextOpenController.instance.targetObj == controllerObj)
            {
                TextOpenController.instance.TextClose(Color.white);
                timerObj.SetActive(false);
            }
        }

        if (timerObj.activeSelf && (Input.GetKeyDown(KeyCode.Escape) || PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_CANCLE], true)) && PDG.Dungeon.instance.closeCoolTime > 0.2f)
        {
            PDG.Dungeon.instance.closeCoolTime = 0.0f;
            isSelect = false;
            if (PDG.Dungeon.instance.newObjectSelectedObj == gameObject)
            {
                PDG.Dungeon.instance.newObjectSelectedObj = null;
                PDG.Dungeon.instance.newObjectSelected = false;
            }
            isOpen = false;
            isUsing = false;
            if (audio) audio.Stop();
            controllerObj.SetActive(false);
            if (TextOpenController.instance.targetObj == controllerObj)
            {
                TextOpenController.instance.TextClose(Color.white);
                timerObj.SetActive(false);
            }
            PlayerCombat.instance.tCoroutine = null;
        }

        if (isSelect)
        {
            if ((PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ACTIVE], true) || PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_ACTIVE], true)) && PDG.Dungeon.instance.closeCoolTime > 0.2f)
            {
                PDG.Dungeon.instance.closeCoolTime = 0.0f;
                SoundManager.instance.StartAudio(new string[1] { "Object/active_F" }, VOLUME_TYPE.EFFECT);
                isSelect = false;
                controllerObj.SetActive(false);
                SaveAndLoadManager.instance.SaveItem(new ItemSave { _id = "TUTORIAL_UPGRADE", _nCount = 1 }, true);
                if (tutorialObj) Destroy(tutorialObj);
                if (!isRepair && !isOpen)
                {
                    isOpen = true;
                }
                else if (!isRepair && isOpen)
                {
                    bool checkMaterial = false;
                    int c1 = SaveAndLoadManager.instance.LoadItem("normal_Parts_2");
                    int c2 = SaveAndLoadManager.instance.LoadItem("normal_Parts_1");
                    int c3 = SaveAndLoadManager.instance.LoadItem("normal_Parts_0");
                    if (Inventorys.Instance.glowindskull < material_0)
                        checkMaterial = true;
                    if (material_1 > c1)
                        checkMaterial = true;
                    if (material_2 > c2)
                        checkMaterial = true;
                    if (material_3 > c3)
                        checkMaterial = true;
                    if (checkMaterial)
                    {

                        int c_0 = material_0 - Inventorys.Instance.glowindskull;
                        int c_1 = material_1 - (c1 == -1 ? 0 : c1);
                        int c_2 = material_2 - (c2 == -1 ? 0 : c2);
                        int c_3 = material_3 - (c3 == -1 ? 0 : c3);
                        string str = LocalizeManager.GetLocalize("d_lack_comment") + LocalizeManager.GetLocalize("d_lack_comment2");
                        if (c_0 > 0) str += "\n" + LocalizeManager.GetLocalize("glowing_skull") + " : " + c_0;
                        if (c_1 > 0) str += "\n" + LocalizeManager.GetLocalize("normal_Parts_2") + " : " + c_1;
                        if (c_2 > 0) str += "\n" + LocalizeManager.GetLocalize("normal_Parts_1") + " : " + c_2;
                        if (c_3 > 0) str += "\n" + LocalizeManager.GetLocalize("normal_Parts_0") + " : " + c_3;
                        str += "\n\n\n";
                        TextOpenController.instance.ShowText(str, Color.white, 24, controllerObj, 2.0f, true);
                        isOpen = false;
                        return;
                    }
                    isUsing = true;
                    PlayerCombat.instance.tCoroutine = StartCoroutine(TimerCoroutine());
                }
                else
                {
                    if (PDG.Dungeon.instance.newObjectSelectedObj == gameObject)
                    {
                        PDG.Dungeon.instance.newObjectSelectedObj = null;
                        PDG.Dungeon.instance.newObjectSelected = false;
                    }
                    MakeController.instance.ShowMake(string.Empty, "upgrade", gameObject);
                }
            }
        }
    }
    IEnumerator TimerCoroutine()
    {
        timerObj.SetActive(true);
        float t = 0.0f;
        while (isUsing)
        {
            Vector3 p = controllerObj.transform.position;
            p.y += 0.5f;
            timerObj.transform.position = Camera.main.WorldToScreenPoint(p);

            if (audio && !audio.isPlaying)
            {
                audio.clip = clips[Random.Range(0, clips.Length)];
                audio.volume =  (SoundManager.isMute || (PDG.Dungeon.instance == null ? false : PDG.Dungeon.instance.isPause) || (PDG.Player.instance == null ? false : PDG.Player.instance.isDie)) ? 0 : SoundManager.EffectVolume;
                audio.Play();
            }

            if (TextOpenController.instance.targetObj == controllerObj)
            {
                TextOpenController.instance.TextClose(Color.white);
            }
            t += Time.deltaTime;
            int tInt = Mathf.CeilToInt(fTime - t);
            timerObj.transform.Find("Text").GetComponent<Text>().text = tInt.ToString();
            timerObj.transform.Find("Image").GetComponent<Image>().fillAmount = t / fTime;
            timerObj.transform.Find("TextCont").GetComponent<Text>().text = LocalizeManager.GetLocalize("repair_ing");
            if (t >= fTime) break;
            yield return new WaitForSeconds(Time.deltaTime);
        }
        audio.Stop();
        if (isUsing)
        {

            Inventorys.Instance.glowindskull -= material_0;
            SaveAndLoadManager.instance.SaveItem(new ItemSave { _id = "glowing_skull", _nCount = Inventorys.Instance.glowindskull }, true);

            int c = SaveAndLoadManager.instance.LoadItem("normal_Parts_2");
            SaveAndLoadManager.instance.SaveItem(new ItemSave { _id = "normal_Parts_2", _nCount = (c - material_1) }, true);

            int c2 = SaveAndLoadManager.instance.LoadItem("normal_Parts_1");
            SaveAndLoadManager.instance.SaveItem(new ItemSave { _id = "normal_Parts_1", _nCount = (c2 - material_2) }, true);

            int c3 = SaveAndLoadManager.instance.LoadItem("normal_Parts_0");
            SaveAndLoadManager.instance.SaveItem(new ItemSave { _id = "normal_Parts_0", _nCount = (c3 - material_3) }, true);

            isRepair = true;
            animator.enabled = true;
            animator.Rebind();
            SaveAndLoadManager.instance.SaveItem(new ItemSave { _id = "UpgradeFlag", _nCount = 1 }, true);
            TextOpenController.instance.ShowText(LocalizeManager.GetLocalize("repair_clear"), Color.white, 24, controllerObj, 2.0f);
            
            SoundManager.instance.StartAudio(new string[1] { "Object/repair_complete" }, VOLUME_TYPE.EFFECT);
            if (PDG.Dungeon.instance.newObjectSelectedObj == gameObject)
            {
                PDG.Dungeon.instance.newObjectSelectedObj = null;
                PDG.Dungeon.instance.newObjectSelected = false;
            }
        }
        
        timerObj.SetActive(false);
        PlayerCombat.instance.tCoroutine = null;
    }
}
