﻿using PDG;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DroneBulletController : MonoBehaviour
{
    //public Vector3 endPos;
    public GameObject target;
    public Vector3 dir;
    private float zAngle;
    public GameObject DestroyObj;
    AudioSource audio;
    [SerializeField] private float hitTime = 0.25f;
    [SerializeField] private float hitTimeNow = 0.25f;
    // Start is called before the first frame update
    private void Start()
    {
        audio = GetComponent<AudioSource>();
    }

    public void BulletFire()
    {
        StartCoroutine(GoBullet());
    }

    void Update()
    {
        hitTimeNow += Time.deltaTime;
        RaycastHit2D hit = Physics2D.CircleCast(transform.position, 1.0f, Vector2.zero, 0, 1 << 12 | 1 << 24);
        if (hit && hitTime <= hitTimeNow)
        {
            hitTimeNow = 0.0f;
            if (hit.collider.gameObject.layer.Equals(24))
            {
                if (hit.collider.GetComponent<MOB.BossMonster_Six_Apear>()) hit.collider.GetComponent<MOB.BossMonster_Six_Apear>().SetDamage(Player.instance.damage);
                if (hit.collider.GetComponent<Boss_STG>()) hit.collider.GetComponent<Boss_STG>().SetDamage(Player.instance.isLeftNear ? Player.instance.damageNear : Player.instance.damage, hit.point);
                if (hit.collider.GetComponent<Boss_NPC>()) hit.collider.GetComponent<Boss_NPC>().SetDamage(Player.instance.isLeftNear ? Player.instance.damageNear : Player.instance.damage, hit.point);
                if (hit.collider.GetComponent<Boss_Golem>()) hit.collider.GetComponent<Boss_Golem>().SetDamage(Player.instance.isLeftNear ? Player.instance.damageNear : Player.instance.damage, hit.point);
                if (hit.collider.GetComponent<Boss_Stone>()) hit.collider.GetComponent<Boss_Stone>().SetDamage(Player.instance.isLeftNear ? Player.instance.damageNear : Player.instance.damage, hit.point, false);
                if (Player.instance.isStrategic_02) Player.instance.isStrategic_02 = false;

                if (PDG.Player.instance.isLeftNear && PDG.Player.instance.isCritical)
                {
                    GameObject criEffect = ObjManager.Call().GetObject("CriticalHitEffect");
                    criEffect.transform.position = hit.point;
                    criEffect.SetActive(true);
                }
            }
            else
            {
                if (hit.collider.GetComponent<MOB.Monster>())
                {
                    hit.collider.GetComponent<MOB.Monster>().SetDamage((Player.instance.isLeftNear ? Player.instance.damageNear : Player.instance.damage), -dir, 1, ELEMENTAL_TYPE.NONE, 0.0f);

                    if (PDG.Player.instance.isLeftNear && PDG.Player.instance.isCritical)
                    {
                        GameObject criEffect = ObjManager.Call().GetObject("CriticalHitEffect");
                        criEffect.transform.position = hit.point;
                        criEffect.SetActive(true);
                    }
                }
            }
            audio.Play();
            GameObject dObj = Instantiate(DestroyObj, null);
            dObj.transform.position = transform.position;
            Destroy(dObj, 2);
            Destroy(gameObject);
        }
    }
    IEnumerator GoBullet()
    {
        float range = Random.Range(3.0f, 7.0f);
        float ranAngle = Random.Range(70, 110);
        if (Random.Range(0, 2) % 2 == 0) ranAngle += 180.0f;

        Vector3 startPos = transform.position;
        Vector3 endPos = target.transform.position;
        Vector3 differencePos = endPos - startPos;
        Vector3 center1 = differencePos / 4;
        Vector3 center2 = center1 * 3;
        center1 += startPos;
        center2 += startPos;

        Vector3 temp1 = GetPosition(center1, ranAngle);
        Vector3 temp1_1 = temp1 - center1;
        temp1_1.Normalize();
        center1 = center1 + (temp1_1 * range);

        Vector3 temp2 = GetPosition(center2, ranAngle);
        Vector3 temp2_1 = temp2 - center2;
        temp2_1.Normalize();
        center2 = center1 + (temp2_1 * range);

        float t = 0.0f;
        while (true)
        {
            if (target != null) endPos = target.transform.position;

            Vector3 lerp1_0 = Vector3.Lerp(startPos, center1, t);
            Vector3 lerp1_1 = Vector3.Lerp(center1, center2, t);
            Vector3 lerp1_2 = Vector3.Lerp(center2, endPos, t);

            Vector3 lerp2_0 = Vector3.Lerp(lerp1_0, lerp1_1, t);
            Vector3 lerp2_1 = Vector3.Lerp(lerp1_1, lerp1_2, t);

            Vector3 lerp = Vector3.Lerp(lerp2_0, lerp2_1, t);
            dir = lerp - transform.position;
            dir.Normalize();
            zAngle = GetAngle(transform.position, transform.position + dir);
            transform.eulerAngles = Vector3.zero;
            transform.position = lerp;
            transform.eulerAngles = new Vector3(0, 0, zAngle);
            t += Time.deltaTime;
            if (t >= 1.0f) break;
            yield return new WaitForSeconds(Time.deltaTime);
        }
        audio.Play();
        GameObject dObj = Instantiate(DestroyObj, null);
        dObj.transform.position = transform.position;
        Destroy(dObj, 2);
        yield return new WaitForSeconds(0.2f);
        Destroy(gameObject);
    }
    #region -- Tool --
    public static float GetAngle(Vector3 vStart, Vector3 vEnd)
    {
        Vector3 v = vEnd - vStart;

        return Mathf.Atan2(v.y, v.x) * Mathf.Rad2Deg;
    }
    public static Vector3 GetPosition(Vector3 vStart, float angle)
    {
        float x = Mathf.Cos(Mathf.Deg2Rad * angle) + vStart.x;
        float y = Mathf.Sin(Mathf.Deg2Rad * angle) + vStart.y;

        return new Vector3(x, y, 0);
    }
    private int GetPercentege(float _per, string[] list)
    {
        int[] per = new int[Mathf.FloorToInt(100 * list.Length)];
        for (int i = 0; i < per.Length; i++) per[i] = -1;

        for (int i = 0; i < list.Length; i++)
        {
            for (int j = 0; j < Mathf.FloorToInt(_per * 100); j++)
            {
                per[(i * 100) + j] = i;
            }
        }
        if (per.Length == 0) return -1;
        return per[Random.Range(0, per.Length)];
    }
    #endregion
}
