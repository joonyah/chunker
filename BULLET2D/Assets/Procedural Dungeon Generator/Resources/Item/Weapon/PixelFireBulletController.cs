﻿using PDG;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PixelFireBulletController : MonoBehaviour
{
    [SerializeField] private GameObject bombObj;
    private Rigidbody2D rigid;
    private AudioSource audio;
    [SerializeField] private AudioSource audio2;
    [SerializeField] private AudioSource audio3;

    public Vector3 firePosition;
    public float fSpeed;
    public float fAngle;
    public float fRange;
    public Vector3 dir;

    public string bulletType = string.Empty;

    [SerializeField] private SpriteRenderer sr;
    [SerializeField] private Sprite[] sprites;
    [SerializeField] private ParticleSystem particle;
    [SerializeField] private GameObject DestroyObj;

    private void Awake()
    {
        rigid = GetComponent<Rigidbody2D>();
        audio = GetComponent<AudioSource>();
    }
    private void Start()
    {
        StartCoroutine(UpdateCoroutine());
    }
    private void OnEnable()
    {
        sr.sprite = Array.Find(sprites, item => item.name.Equals(bulletType + "_bullet"));


        ParticleSystem.MainModule pm = particle.main;
        ParticleSystemGradientMode mode = pm.startColor.mode;
        mode = ParticleSystemGradientMode.TwoColors;
        ParticleSystem.MinMaxGradient pc;
        if (bulletType.Contains("pixelfire_small"))
        {
            Color32 color1 = new Color32(255, 107, 0, 255);
            Color32 color2 = new Color32(255, 24, 0, 255);
            pc = new ParticleSystem.MinMaxGradient(color1, color2);
        }
        else
        {
            Color32 color1 = new Color32(0, 5, 255, 255);
            Color32 color2 = new Color32(0, 142, 255, 255);
            pc = new ParticleSystem.MinMaxGradient(color1, color2);
        }
        pm.startColor = pc;
        StartCoroutine(UpdateCoroutine());
    }
    IEnumerator UpdateCoroutine()
    {
        while (true)
        {
            RaycastHit2D hit = Physics2D.CircleCast(transform.position, 0.2f, Vector2.zero, 0, 1 << 8 | 1 << 24 | 1 << 12 | 1 << 23);

            if (Vector3.Distance(firePosition, transform.position) > fRange)
            {
                Bomb(hit);
            }
            if (hit)
            {
                Bomb(hit);
            }
            yield return new WaitForSeconds(Time.deltaTime / 10);
        }
    }

    void Bomb(RaycastHit2D hit)
    {
        Color _color = Color.white;
        if (bulletType.Contains("pixelfire_small"))
        {
            _color = new Color32(255, 156, 0, 255);
            GameObject obj2 = Instantiate(bombObj);
            obj2.GetComponent<DrumBomb>().InitSetting(1, transform.position, bombObj, false, false, Player.instance.tickTime, string.Empty);
            audio2.Play();
        }
        else
        {
            _color = new Color32(0, 133, 255, 255);
        }

        if (hit)
        {
            if (hit.collider.gameObject.layer.Equals(24))
            {
                if (hit.collider.GetComponent<MOB.BossMonster_Six_Apear>()) hit.collider.GetComponent<MOB.BossMonster_Six_Apear>().SetDamage(Player.instance.damage);
                if (hit.collider.GetComponent<Boss_STG>()) hit.collider.GetComponent<Boss_STG>().SetDamage(Player.instance.isLeftNear ? Player.instance.damageNear : Player.instance.damage, hit.point);
                if (hit.collider.GetComponent<Boss_NPC>()) hit.collider.GetComponent<Boss_NPC>().SetDamage(Player.instance.isLeftNear ? Player.instance.damageNear : Player.instance.damage, hit.point);
                if (hit.collider.GetComponent<Boss_Golem>()) hit.collider.GetComponent<Boss_Golem>().SetDamage(Player.instance.isLeftNear ? Player.instance.damageNear : Player.instance.damage, hit.point);
                if (hit.collider.GetComponent<Boss_Stone>()) hit.collider.GetComponent<Boss_Stone>().SetDamage(Player.instance.isLeftNear ? Player.instance.damageNear : Player.instance.damage, hit.point, false);
                if (Player.instance.isStrategic_02) Player.instance.isStrategic_02 = false;

                if (PDG.Player.instance.isLeftNear && PDG.Player.instance.isCritical)
                {
                    GameObject criEffect = ObjManager.Call().GetObject("CriticalHitEffect");
                    criEffect.transform.position = hit.point;
                    criEffect.SetActive(true);
                }
            }
            else if (hit.collider.gameObject.layer.Equals(23))
            {

                if (hit.collider.gameObject.name.Contains("puddle")) return;
                if (hit.collider.gameObject.name.Contains("grass_2")) return;
                if (hit.collider.gameObject.name.Contains("door")) return;

                if (hit.collider.transform.parent && hit.collider.transform.parent.GetComponent<TentacleController>())
                {
                    hit.collider.transform.parent.GetComponent<TentacleController>().SetDamage(1);
                }
                Vector3 dir = hit.collider.gameObject.transform.position - PDG.Player.instance.transform.position;
                dir.Normalize();
                if (hit.collider.gameObject.GetComponent<DestroyObjects>())
                {
                    hit.collider.gameObject.GetComponent<DestroyObjects>().DestroyObject(dir);
                }
                if (hit.collider.gameObject.GetComponent<ChestHit>() && !hit.collider.gameObject.GetComponent<ChestHit>().isOpenBefore)
                {
                    hit.collider.gameObject.GetComponent<ChestHit>().SetDamage(1);
                }
                if (hit.collider.gameObject.GetComponent<DrumHit>())
                {
                    hit.collider.gameObject.GetComponent<DrumHit>().SetDamage(1);
                }
            }
            else
            {
                if (hit.collider.GetComponent<MOB.Monster>())
                {
                    hit.collider.GetComponent<MOB.Monster>().SetDamage((Player.instance.isLeftNear ? Player.instance.damageNear : Player.instance.damage), -dir, 1, bulletType.Contains("pixelfire_small") ? ELEMENTAL_TYPE.NONE : ELEMENTAL_TYPE.FREEZE, 6.0f);

                    if (PDG.Player.instance.isLeftNear && PDG.Player.instance.isCritical)
                    {
                        GameObject criEffect = ObjManager.Call().GetObject("CriticalHitEffect");
                        criEffect.transform.position = hit.point;
                        criEffect.SetActive(true);
                    }
                }
            }
            if (audio3 && audio3.enabled && audio3.clip != null) audio3.Play();
        }
        GameObject obj = Instantiate(DestroyObj);
        ParticleSystem particle = obj.transform.GetChild(0).GetComponent<ParticleSystem>();
        ParticleSystem.MainModule pm1 = particle.main;
        ParticleSystem.MinMaxGradient pColor = new ParticleSystem.MinMaxGradient(_color);
        pm1.startColor = pColor;
        obj.transform.position = transform.position;
        Destroy(obj, 2.0f);

        gameObject.SetActive(false);
    }

    public void BulletFire()
    {
        rigid.AddForce(dir * fSpeed, ForceMode2D.Impulse);
    }
}
