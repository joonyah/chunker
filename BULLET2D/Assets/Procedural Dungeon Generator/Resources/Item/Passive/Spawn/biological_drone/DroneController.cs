﻿using PDG;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DroneController : MonoBehaviour
{
    public float moveAngle = 0.0f;
    public float dis = 2.0f;
    public float fAroundSpeed = 50.0f;
    GameObject target;
    Animator animator;
    private float fireDelay = 1.0f;
    private float fireDelayNow = 0.0f;

    [SerializeField] private float startDis = 0.0f;
    [SerializeField] private bool isStart;
    [SerializeField] private bool isAttack;
    [SerializeField] private bool isTarget;

    [SerializeField] private float destroyTime = 90.0f;
    bool isEnd = false;
    [SerializeField] private GameObject bulletObj;
    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
        AnimatorStateInfo _stateInfo = animator.GetCurrentAnimatorStateInfo(0);
        animator.Play(_stateInfo.fullPathHash, -1, Random.Range(0.0f, 1.0f));

        //Destroy(gameObject, 90.0f);
    }

    // Update is called once per frame
    void Update()
    {
        if(!Player.instance.isBiologicalDrone)
        {
            Destroy(gameObject);
        }

        fireDelayNow += Time.deltaTime;
        destroyTime -= Time.deltaTime;

        if (destroyTime <= 0.0f && !isEnd)
        {
            isEnd = true;
        }

        if (isEnd)
        {
            startDis -= Time.deltaTime * 2;
            Vector3 anglePosition = GetPosition(Player.instance.transform.position, moveAngle);
            Vector3 dir = anglePosition - Player.instance.transform.position;
            dir.Normalize();
            transform.position = Player.instance.transform.position + (dir * startDis);
            if (startDis <= 0.0f)
            {
                Destroy(gameObject);
            }
            return;
        }

        RaycastHit2D[] hit = Physics2D.CircleCastAll(transform.position, 6, Vector2.zero, 0, 1 << 12 | 1 << 24);
        if (hit.Length > 0 && !isTarget)
        {
            target = hit[Random.Range(0, hit.Length)].collider.gameObject;
            isTarget = true;
        }

        if (!isStart)
        {
            startDis += Time.deltaTime * 2;
            Vector3 anglePosition = GetPosition(Player.instance.transform.position, moveAngle);
            Vector3 dir = anglePosition - Player.instance.transform.position;
            dir.Normalize();
            transform.position = Player.instance.transform.position + (dir * startDis);
            if (startDis >= 2.0f)
            {
                isStart = true;
                StartCoroutine(MoveCoroutine());
            }
        }
        else if (isTarget)
        {
            if (target == null) isTarget = false;
            if (target != null && target.GetComponent<MOB.Monster>() && target.GetComponent<MOB.Monster>().mobInfo.curHp <= 0) isTarget = false;
            if (target != null && Vector3.Distance(transform.position, target.transform.position) > 4) isTarget = false;
            if (fireDelayNow > fireDelay)
            {
                fireDelayNow = 0.0f;
                animator.SetTrigger("AttackTrigger");
            }
        }
        //else if (!isAttack)
        //{
        //    moveAngle += Time.deltaTime * fAroundSpeed;
        //    if (moveAngle >= 360.0f) moveAngle -= 360.0f;
        //    Vector3 anglePosition = GetPosition(Player.instance.transform.position, moveAngle);
        //    Vector3 dir = anglePosition - Player.instance.transform.position;
        //    dir.Normalize();
        //    transform.position = Player.instance.transform.position + (dir * dis);
        //}
    }
    IEnumerator MoveCoroutine()
    {
        float f = Random.Range(0.1f, 1.0f);
        bool isUp = Random.Range(0, 2) % 2 == 0 ? true : false;
        while (true)
        {
            if (Vector3.Distance(Player.instance.transform.position, transform.position) > 3)
            {
                Vector3 anglePosition = GetPosition(Player.instance.transform.position, moveAngle);
                Vector3 dir = anglePosition - Player.instance.transform.position;
                dir.Normalize();

                Vector3 startPos = transform.position;
                Vector3 endPos = Player.instance.transform.position + (dir * dis);
                float t = 0.0f;
                while (true)
                {
                    Vector3 lerp = Vector3.Lerp(startPos, endPos, t);
                    transform.position = lerp;
                    t += Time.deltaTime * 3;
                    if (t >= 1.0f) break;
                    yield return new WaitForSeconds(Time.deltaTime);
                }
            }
            else
            {
                Vector3 pos = transform.position;
                if (isUp)
                {
                    f += Time.deltaTime * 4;
                    pos.y += Time.deltaTime;
                    if (f > 1.0f)
                    {
                        f = 0.0f;
                        isUp = !isUp;
                    }
                }
                else
                {
                    f += Time.deltaTime * 4;
                    pos.y -= Time.deltaTime ;
                    if (f > 1.0f)
                    {
                        f = 0.0f;
                        isUp = !isUp;
                    }
                }
                transform.position = pos;
            }
            yield return new WaitForSeconds(Time.deltaTime);
        }
    }
    public void Fire()
    {
        if (target != null)
        {
            GameObject obj = Instantiate(bulletObj);
            obj.transform.position = transform.position;
            obj.GetComponent<DroneBulletController>().target = target;
            obj.SetActive(true);
            obj.GetComponent<DroneBulletController>().BulletFire();
        }
    }
    #region -- Tool --
    public static float GetAngle(Vector3 vStart, Vector3 vEnd)
    {
        Vector3 v = vEnd - vStart;

        return Mathf.Atan2(v.y, v.x) * Mathf.Rad2Deg;
    }
    public static Vector3 GetPosition(Vector3 vStart, float angle)
    {
        float x = Mathf.Cos(Mathf.Deg2Rad * angle) + vStart.x;
        float y = Mathf.Sin(Mathf.Deg2Rad * angle) + vStart.y;

        return new Vector3(x, y, 0);
    }
    #endregion
}
