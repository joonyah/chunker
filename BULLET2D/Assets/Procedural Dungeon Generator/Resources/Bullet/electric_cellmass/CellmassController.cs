﻿using PDG;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CellmassController : MonoBehaviour
{
    public Vector3 firePosition;
    public float fSpeed;
    public float fAngle;
    public float fRange;
    public Vector3 dir;
    [SerializeField] private GameObject FireObj;
    public float fireDelay = 0.5f;
    [SerializeField] private float fireDelayNow = 0.5f;
    [SerializeField] private GameObject DestroyObj;
    Rigidbody2D rigid;
    AudioSource audio;
    public int maxAttackCount;
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, 8);
    }
    private void Awake()
    {
        audio = GetComponent<AudioSource>();
        rigid = GetComponent<Rigidbody2D>();
    }
    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        RaycastHit2D hitBack = Physics2D.CircleCast(transform.position, 0.5f, Vector2.zero, 0, 1 << 12 | 1 << 24 | 1 << 23 | 1 << 8);
        if (hitBack) Bomb();

        if (Vector3.Distance(firePosition, transform.position) > fRange)
        {
            Bomb();
        }

        fireDelayNow += Time.deltaTime;
        if (fireDelayNow > fireDelay)
        {
            fireDelayNow = 0.0f;
            RaycastHit2D[] hit = Physics2D.CircleCastAll(transform.position, 4, Vector2.zero, 0, 1 << 12 | 1 << 24 | 1 << 23);
            if (hit.Length > 0)
            {
                for (int i = 0; i < hit.Length && i < maxAttackCount; i++)
                {
                    if (hit[i].collider.transform.parent && hit[i].collider.transform.parent.GetComponent<TentacleController>())
                    {
                        GameObject obj = Instantiate(FireObj);
                        obj.transform.position = transform.position;
                        obj.GetComponent<CellmassBulletController>().target = hit[i].collider.gameObject;
                        obj.GetComponent<CellmassBulletController>().dir = (hit[i].collider.transform.position - transform.position).normalized;
                        obj.GetComponent<CellmassBulletController>().fSpeed = 10.0f;
                    }
                    else if (hit[i].collider.gameObject.layer.Equals(23)) continue;
                    else
                    {
                        GameObject obj = Instantiate(FireObj);
                        obj.transform.position = transform.position;
                        obj.GetComponent<CellmassBulletController>().target = hit[i].collider.gameObject;
                        obj.GetComponent<CellmassBulletController>().dir = (hit[i].collider.transform.position - transform.position).normalized;
                        obj.GetComponent<CellmassBulletController>().fSpeed = 10.0f;
                    }
                }
            }
        }
    }
    private void Bomb()
    {
        GameObject obj = Instantiate(DestroyObj);
        obj.transform.position = transform.position;
        Destroy(obj, 2.0f);
        RaycastHit2D[] hit = Physics2D.CircleCastAll(transform.position, 4, Vector2.zero, 0, 1 << 12 | 1 << 24 | 1 << 23);
        if (hit.Length > 0)
        {
            for (int i = 0; i < hit.Length; i++)
            {
                Collider2D collision = hit[i].collider;
                if (collision.gameObject.layer.Equals(12))
                {
                    if (collision.GetComponent<MOB.Monster>())
                    {
                        collision.GetComponent<MOB.Monster>().SetDamage((Player.instance.isLeftNear ? Player.instance.damageNear : Player.instance.damage), -dir, 1, PDG.Player.instance.left_elemetanl, PDG.Player.instance.left_elemnetalTime);
                        if (PDG.Player.instance.isLeftNear && PDG.Player.instance.isCritical)
                        {
                            GameObject criEffect = ObjManager.Call().GetObject("CriticalHitEffect");
                            criEffect.transform.position = hit[i].point;
                            criEffect.SetActive(true);
                        }
                    }
                    if (Player.instance.isStrategic_02) Player.instance.isStrategic_02 = false;
                }
                else if (collision.gameObject.layer.Equals(24))
                {
                    if (collision.GetComponent<MOB.BossMonster_Six_Apear>()) collision.GetComponent<MOB.BossMonster_Six_Apear>().SetDamage(Player.instance.damage);
                    if (collision.GetComponent<Boss_STG>()) collision.GetComponent<Boss_STG>().SetDamage(Player.instance.isLeftNear ? Player.instance.damageNear : Player.instance.damage, hit[i].point);
                    if (collision.GetComponent<Boss_NPC>()) collision.GetComponent<Boss_NPC>().SetDamage(Player.instance.isLeftNear ? Player.instance.damageNear : Player.instance.damage, hit[i].point);
                    if (collision.GetComponent<Boss_Golem>()) collision.GetComponent<Boss_Golem>().SetDamage(Player.instance.isLeftNear ? Player.instance.damageNear : Player.instance.damage, hit[i].point);
                    if (collision.GetComponent<Boss_Stone>()) collision.GetComponent<Boss_Stone>().SetDamage(Player.instance.isLeftNear ? Player.instance.damageNear : Player.instance.damage, hit[i].point, false);
                    if (Player.instance.isStrategic_02) Player.instance.isStrategic_02 = false;
                    if (PDG.Player.instance.isLeftNear && PDG.Player.instance.isCritical)
                    {
                        GameObject criEffect = ObjManager.Call().GetObject("CriticalHitEffect");
                        criEffect.transform.position = hit[i].point;
                        criEffect.SetActive(true);
                    }
                }
                else if (collision.gameObject.layer.Equals(23))
                {

                    if (collision.gameObject.name.Contains("puddle")) return;
                    if (collision.gameObject.name.Contains("grass_2")) return;
                    if (collision.gameObject.name.Contains("door")) return;

                    if (collision.transform.parent && collision.transform.parent.GetComponent<TentacleController>())
                    {
                        collision.transform.parent.GetComponent<TentacleController>().SetDamage(1);
                    }
                    Vector3 dir = collision.gameObject.transform.position - PDG.Player.instance.transform.position;
                    dir.Normalize();
                    if (collision.gameObject.GetComponent<DestroyObjects>())
                    {
                        collision.gameObject.GetComponent<DestroyObjects>().DestroyObject(dir);
                    }
                    if (collision.gameObject.GetComponent<ChestHit>() && !collision.gameObject.GetComponent<ChestHit>().isOpenBefore)
                    {
                        collision.gameObject.GetComponent<ChestHit>().SetDamage(1);
                    }
                    if (collision.gameObject.GetComponent<DrumHit>())
                    {
                        collision.gameObject.GetComponent<DrumHit>().SetDamage(1);
                    }
                }
            }
        }
        gameObject.SetActive(false);
    }
    public void BulletFire()
    {
        rigid.AddForce(dir * fSpeed, ForceMode2D.Impulse);
        //transform.localEulerAngles = new Vector3(0, 0, fAngle);
    }

}
