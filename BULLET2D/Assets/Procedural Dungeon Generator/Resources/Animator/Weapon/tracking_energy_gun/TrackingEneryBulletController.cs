﻿using PDG;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrackingEneryBulletController : MonoBehaviour
{
    public Vector3 firePosition;
    public float fSpeed;
    public float fAngle;
    public float fRange;
    public Vector3 dir;
    Rigidbody2D rigid;
    AudioSource audio;
    [SerializeField] private float hitTime = 0.25f;
    [SerializeField] private float hitTimeNow = 0.25f;
    [SerializeField] private GameObject DestroyObj;
    bool isTargeting = false;
    private void Awake()
    {
        audio = GetComponent<AudioSource>();
        rigid = GetComponent<Rigidbody2D>();
    }
    private void Update()
    {
        hitTimeNow += Time.deltaTime;
        RaycastHit2D hitBack = Physics2D.CircleCast(transform.position, 0.5f, Vector2.zero, 0, 1 << 8);
        if (hitBack)
        {
            GameObject obj = Instantiate(DestroyObj);
            obj.transform.position = transform.position;
            Destroy(obj, 2.0f);
            gameObject.SetActive(false);
        }
        if (Vector3.Distance(firePosition, transform.position) > fRange)
        {
            GameObject obj = Instantiate(DestroyObj);
            obj.transform.position = transform.position;
            Destroy(obj, 2.0f);
            gameObject.SetActive(false);
        }

        RaycastHit2D[] hit = Physics2D.CircleCastAll(transform.position, 3, Vector2.zero, 0, 1 << 12 | 1 << 24);
        if (hit.Length > 0)
        {
            dir = hit[0].collider.transform.position - transform.position;
            dir.Normalize();
            rigid.velocity = Vector2.zero;
            rigid.AddForce(dir * fSpeed, ForceMode2D.Impulse);
        }
        hit = Physics2D.CapsuleCastAll(transform.position, new Vector2(1, 0.5f), CapsuleDirection2D.Horizontal, fAngle, Vector2.zero, 0, 1 << 12 | 1 << 24 | 1 << 23 | 1 << 8);
        if (hit.Length > 0 && hitTime <= hitTimeNow)
        {
            hitTimeNow = 0.0f;
            for (int i = 0; i < hit.Length; i++)
            {
                Collider2D collision = hit[i].collider;
                if (collision.gameObject.layer.Equals(12))
                {
                    if (collision.GetComponent<MOB.Monster>()) collision.GetComponent<MOB.Monster>().SetDamage((Player.instance.isLeftNear ? Player.instance.damageNear : Player.instance.damage), -dir, 1, PDG.Player.instance.left_elemetanl, PDG.Player.instance.left_elemnetalTime);
                    if (PDG.Player.instance.isStrategic_02) PDG.Player.instance.isStrategic_02 = false;
                    if (PDG.Player.instance.isLeftNear && PDG.Player.instance.isCritical)
                    {
                        GameObject criEffect = ObjManager.Call().GetObject("CriticalHitEffect");
                        criEffect.transform.position = collision.transform.position;
                        criEffect.SetActive(true);
                    }
                }
                else if (collision.gameObject.layer.Equals(23))
                {

                    if (collision.gameObject.name.Contains("puddle")) return;
                    if (collision.gameObject.name.Contains("grass_2")) return;
                    if (collision.gameObject.name.Contains("door")) return;

                    if (collision.transform.parent && collision.transform.parent.GetComponent<TentacleController>())
                    {
                        collision.transform.parent.GetComponent<TentacleController>().SetDamage(1);
                    }
                    Vector3 dir = collision.gameObject.transform.position - PDG.Player.instance.transform.position;
                    dir.Normalize();
                    if (collision.gameObject.GetComponent<DestroyObjects>())
                    {
                        collision.gameObject.GetComponent<DestroyObjects>().DestroyObject(dir);
                    }
                    if (collision.gameObject.GetComponent<ChestHit>() && !collision.gameObject.GetComponent<ChestHit>().isOpenBefore)
                    {
                        collision.gameObject.GetComponent<ChestHit>().SetDamage(1);
                    }
                    if (collision.gameObject.GetComponent<DrumHit>())
                    {
                        collision.gameObject.GetComponent<DrumHit>().SetDamage(1);
                    }
                }
                else if (collision.gameObject.layer.Equals(24))
                {
                    if (collision.GetComponent<MOB.BossMonster_Six_Apear>()) collision.GetComponent<MOB.BossMonster_Six_Apear>().SetDamage(Player.instance.damage);
                    if (collision.GetComponent<Boss_STG>()) collision.GetComponent<Boss_STG>().SetDamage(Player.instance.isLeftNear ? Player.instance.damageNear : Player.instance.damage, hit[i].point);
                    if (collision.GetComponent<Boss_NPC>()) collision.GetComponent<Boss_NPC>().SetDamage(Player.instance.isLeftNear ? Player.instance.damageNear : Player.instance.damage, hit[i].point);
                    if (collision.GetComponent<Boss_Golem>()) collision.GetComponent<Boss_Golem>().SetDamage(Player.instance.isLeftNear ? Player.instance.damageNear : Player.instance.damage, hit[i].point);
                    if (collision.GetComponent<Boss_Stone>()) collision.GetComponent<Boss_Stone>().SetDamage(Player.instance.isLeftNear ? Player.instance.damageNear : Player.instance.damage, hit[i].point, false);
                    if (PDG.Player.instance.isStrategic_02) PDG.Player.instance.isStrategic_02 = false;
                    if (PDG.Player.instance.isLeftNear && PDG.Player.instance.isCritical)
                    {
                        GameObject criEffect = ObjManager.Call().GetObject("CriticalHitEffect");
                        criEffect.transform.position = collision.transform.position;
                        criEffect.SetActive(true);
                    }
                }
            }
            GameObject obj = Instantiate(DestroyObj);
            obj.transform.position = transform.position;
            Destroy(obj, 2.0f);
            gameObject.SetActive(false);

        }

        fAngle += 50 / 1.5f;
        if (fAngle >= 360) fAngle -= 360;
        transform.localEulerAngles = new Vector3(0, 0, fAngle);
    }
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, 6);
    }
    public void BulletFire()
    {
        rigid.AddForce(dir * fSpeed, ForceMode2D.Impulse);
    }
}
