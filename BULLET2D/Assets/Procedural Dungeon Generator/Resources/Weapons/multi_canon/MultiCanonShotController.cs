﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MultiCanonShotController : MonoBehaviour
{
    [SerializeField] private Transform[] ShotTransforms;
    [SerializeField] private GameObject shotObj;
    [SerializeField] private float delay = 0.15f;
    [SerializeField] private float[] delayNow;
    ItemsData iData;
    private void Start()
    {
        delayNow = new float[ShotTransforms.Length];
        iData = System.Array.Find(SheetManager.Instance.ItemsDB.dataArray, item => item.ID.Equals("multi_canon"));
        delay = iData.Shootdelay;
    }

    private void OnEnable()
    {
        delayNow = new float[ShotTransforms.Length];
        for (int i=0; i< delayNow.Length; i++)
        {
            delayNow[i] = Random.Range(0, delay);
        }
    }

    // Update is called once per frame
    void Update()
    {
        for (int i = 0; i < ShotTransforms.Length; i++)
        {
            delayNow[i] += Time.deltaTime;
            if (delayNow[i] >= delay) 
            {
                delayNow[i] = 0.0f;
                StartCoroutine(Fire(i));
            }
        }
    }

    IEnumerator Fire(int i)
    {
        for (int j = 0; j < iData.Shootcount + PDG.Player.instance.plusShootCount; j++)
        {
            for (int k = 0; k < iData.Multishoot + PDG.Player.instance.plusMultiShoot; k++)
            {
                float angleangle = 0.0f;
                if (iData.Multishoot + PDG.Player.instance.plusMultiShoot == 1) angleangle = 0.0f;
                else
                {
                    int r = Random.Range(5, 8);
                    int v = (iData.Multishoot + PDG.Player.instance.plusMultiShoot) / 2;
                    v *= r;
                    angleangle = -v + (r * ((iData.Multishoot + PDG.Player.instance.plusMultiShoot) % 2 == 0 ? k + 1 : k));
                }
                float angle = PDG.Player.GetAngle(PDG.Player.instance.transform.position, ShotTransforms[i].position);
                angle += (Random.Range(-iData.Aim, iData.Aim) + angleangle);
                Vector3 firePos = PDG.Player.GetPosition(ShotTransforms[i].position, angle);
                Vector3 dir = firePos - ShotTransforms[i].position;
                dir.Normalize();
                GameObject obj = Instantiate(shotObj);
                obj.transform.position = ShotTransforms[i].position;
                obj.GetComponent<PlayerAttackController>().firePosition = ShotTransforms[i].position;
                obj.GetComponent<PlayerAttackController>().destroyDistance = iData.Range;
                obj.GetComponent<PlayerAttackController>().isRight = true;
                obj.GetComponent<PlayerAttackController>().weaponName = iData.ID;
                obj.GetComponent<PlayerAttackController>().RigidBodyFire(dir, iData.Bulletspeed);
            }
            yield return new WaitForSeconds(iData.Bulletdelay);
        }
    }
}
