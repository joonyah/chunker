﻿using Pathfinding;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(Seeker))]
public class MercenaryController : MonoBehaviour
{
    #region -- Variabled --
    [SerializeField] private Animator animator;
    [SerializeField] private SpriteRenderer sr;
    [SerializeField] private Seeker seeker;
    [SerializeField] private BoxCollider2D bCol;
    [SerializeField] private Rigidbody2D rigid;
    [SerializeField] private SpriteOutline sOutline;
    [SerializeField] private GameObject targetObj;
    [SerializeField] private bool isTargeting;
    [SerializeField] private bool isAttack = false;
    [SerializeField] private bool isMoveStop = false;
    public Vector3 dir;
    private Sprite[] _sprites;
    private string _spritePath;
    private bool isHit = false;
    private float hitTime = 0.25f;
    #region -- AI Variabled --
    [Header("AI Movement")]
    //플레이어에게 갈 수 있는지 체크
    [SerializeField] private bool isPathPossible = false;
    //이동할 좌표를 바꿔야할 상태인지 체크
    [SerializeField] private bool isMoveChange = true;
    // 초마다 몇번의 길 업데이트를 할것인지
    [SerializeField] private float updateRate = 2f;
    [HideInInspector] public bool pathIsEnabled = false;
    // AI에서 다음 웨이 포인트까지 계속되는 웨이 포인트까지의 최대 거리
    [SerializeField] private float nextWaypointDistance = 1;
    // 우리가 현재 움직이고있는 웨이 포인트
    private int currentWaypoint = 0;
    // 계산 된 경로
    public Path path;
    #endregion
    #region -- Sheet Infomation -- 
    [Header("Sheet Information")]
    [SerializeField] private string _id = "fighter";
    [SerializeField] private string attackType = "fighter";
    [SerializeField] private float fAttackRange = 5;
    [SerializeField] private float fAttackDelayNow = 0.5f;
    [SerializeField] private float fAttackDelay = 0.5f;
    [SerializeField] private float fSpeed = 5;
    [SerializeField] private float fDamage = 5;
    [SerializeField] private bool isRangeAttack = false;

    [Header("Attack Information")]
    [SerializeField] private GameObject hitEffect;
    [SerializeField] private GameObject AttackHitEffect;
    [SerializeField] private float fighterRange = 2;

    #endregion
    #endregion
    #region -- Method --
    private void Awake()
    {
        animator = GetComponent<Animator>();
        sr = GetComponent<SpriteRenderer>();
        seeker = GetComponent<Seeker>();
        bCol = GetComponent<BoxCollider2D>();
        rigid = GetComponent<Rigidbody2D>();
        sOutline = GetComponent<SpriteOutline>();
    }
    void Start()
    {
        MercenaryData mData = System.Array.Find(SheetManager.Instance.MercenaryDB.dataArray, item => item.ID.Equals(_id));
        attackType = mData.Attacktype;
        fAttackRange = mData.Attackrange;
        fAttackDelay = fAttackDelayNow = mData.Attackdelay;
        fSpeed = mData.Speed;
        fDamage = mData.Damage;
        isRangeAttack = mData.Israngeattack;
        StartCoroutine(UpdatePath());
    }
    void Load()
    {
        var spritePath = "Mercenary/" + _id;
        if (!spritePath.Equals(_spritePath))
        {
            _spritePath = spritePath;
            _sprites = Resources.LoadAll<Sprite>(_spritePath);
        }
    }
    void LateUpdate()
    {
        if (sr == null || sr.sprite == null)  return;
        Load();
        var name = sr.sprite.name;
        var sprite = System.Array.Find(_sprites, item => item.name == name);
        if (sprite) sr.sprite = sprite;
    }
    void Update()
    {
        fAttackDelayNow += Time.deltaTime;

        if (targetObj)
        {
            if (Vector3.Distance(PDG.Player.instance.transform.position, transform.position) > 10)
            {
                isTargeting = false;
                targetObj = null;
                isMoveChange = true;
            }
            else if (targetObj.GetComponent<MOB.Monster>())
            {
                if (targetObj.GetComponent<MOB.Monster>().isDie) 
                {
                    isTargeting = false;
                    targetObj = null;
                    isMoveChange = true;
                }
            }
        }

        if (isTargeting)
        {
            if (isPathPossible)
            {
                if (path == null)
                {
                    return;
                }
                if (Vector3.Distance(targetObj.transform.position, transform.position) < fAttackRange)
                {
                    // 공격해 
                    if (fAttackDelayNow > fAttackDelay)
                    {
                        isMoveStop = true;
                        float angle = GetAngle(transform.position, targetObj.transform.position);
                        if ((angle >= 0 && angle < 22.5f)) animator.SetInteger("Direction", 0);
                        else if (angle >= 22.5f && angle < 67.5f) animator.SetInteger("Direction", 5);
                        else if (angle >= 67.5f && angle < 112.5f) animator.SetInteger("Direction", 2);
                        else if (angle >= 112.5f && angle < 157.5f) animator.SetInteger("Direction", 4);
                        else if (angle >= 157.5f && angle < 202.5f) animator.SetInteger("Direction", 1);
                        else if (angle >= 202.5f && angle < 247.5f) animator.SetInteger("Direction", 6);
                        else if (angle >= 247.5f && angle < 292.5f) animator.SetInteger("Direction", 3);
                        else if (angle >= 292.5f && angle < 337.5f) animator.SetInteger("Direction", 7);
                        else animator.SetInteger("Direction", 0);
                        animator.SetTrigger("Attack");
                        fAttackDelayNow = 0.0f;
                    }
                }
                else
                {
                    if (currentWaypoint >= path.vectorPath.Count)
                    {
                        if (pathIsEnabled)
                            return;
                        //경로의 끝에 도착
                        pathIsEnabled = true;
                        isMoveChange = true;
                        return;
                    }
                    pathIsEnabled = false;
                    if (isMoveStop) return;
                    // 다음 웨이 포인트 방향
                    dir = (path.vectorPath[currentWaypoint] - transform.position).normalized;
                    transform.eulerAngles = Vector3.zero;
                    Move(dir, fSpeed);
                    float dist = Vector3.Distance(transform.position, path.vectorPath[currentWaypoint]);
                    if (dist < nextWaypointDistance)
                    {
                        currentWaypoint++;
                        return;
                    }
                }
            }
        }
        else
        {
            if (Vector3.Distance(PDG.Player.instance.transform.position, transform.position) < 4)
            {
                RaycastHit2D[] hits = Physics2D.CircleCastAll(transform.position, 10, Vector2.zero, 0, 1 << 12 | 1 << 24);
                if (hits.Length > 0)
                {
                    int t = 0;
                    float dis = float.MaxValue;
                    for (int i = 0; i < hits.Length; i++)
                    {
                        if (hits[i].collider.GetComponent<MOB.Monster>())
                        {
                            if (hits[i].collider.GetComponent<MOB.Monster>().isDie) continue;
                            if (hits[i].collider.GetComponent<MOB.Monster>().isMiMicWait) continue;
                            if (hits[i].collider.GetComponent<MOB.Monster>().isGrab) continue;
                        }

                        float dis2 = Vector3.Distance(transform.position, hits[i].collider.transform.position);
                        if (dis2 < dis)
                        {
                            dis = dis2;
                            t = i;
                        }
                    }
                    targetObj = hits[t].collider.gameObject;
                    isTargeting = true;
                    isMoveChange = true;
                }
            }
            if (isPathPossible)
            {
                if (path == null)
                {
                    isMoveChange = true;
                    return;
                }
                if (Vector3.Distance(PDG.Player.instance.transform.position, transform.position) < 1.5f)
                {
                    if (animator.GetBool("isMove")) animator.SetBool("isMove", false);
                }
                else
                {
                    if (currentWaypoint >= path.vectorPath.Count)
                    {
                        if (pathIsEnabled)
                            return;
                        //경로의 끝에 도착
                        pathIsEnabled = true;
                        isMoveChange = true;
                        return;
                    }
                    pathIsEnabled = false;
                    if (isMoveStop) return;
                    // 다음 웨이 포인트 방향
                    dir = (path.vectorPath[currentWaypoint] - transform.position).normalized;
                    transform.eulerAngles = Vector3.zero;
                    Move(dir, fSpeed);
                    float dist = Vector3.Distance(transform.position, path.vectorPath[currentWaypoint]);
                    if (dist < nextWaypointDistance)
                    {
                        currentWaypoint++;
                        return;
                    }
                }
            }
        }

        if(!isHit)
        {
            RaycastHit2D hit = Physics2D.BoxCast(transform.position, new Vector2(0.8f, 0.8f), 0, Vector2.zero, 0, 1 << 12 | 1 << 24);
            if (hit)
            {
                if (hit.collider.tag.Equals("Enemy"))
                {
                    if (hit.collider.GetComponent<MOB.Monster>() && hit.collider.GetComponent<MOB.Monster>().isMiMicWait)
                    {
                        return;
                    }
                    isHit = true;
                    hitTime = 0.25f;
                    Vector3 backDir = transform.position - hit.collider.transform.position;
                    backDir.Normalize();
                    RaycastHit2D hit2 = Physics2D.Raycast(transform.position + backDir, backDir, 1, 1 << 8);
                    if (!hit2) transform.Translate(backDir * 20.0f * Time.deltaTime);

                    GameObject obj2 = Instantiate(hitEffect);
                    obj2.transform.position = transform.position;
                    Destroy(obj2, 2f);
                }
            }
        }
        else
        {
            hitTime -= Time.deltaTime;
            if (hitTime <= 0.0f) isHit = false;
        }

    }
    private void Move(Vector2 _dir, float _speed)
    {
        dir = _dir;
        if (_dir.x < 0) sr.flipX = true;
        else sr.flipX = false;

        animator.SetBool("isMove", true);
        transform.Translate(_dir * _speed * Time.deltaTime);
    }
    IEnumerator UpdatePath()
    {
        Vector3 pPos = transform.position;
        while (true)
        {
            Vector3 pos = transform.position;

            if (targetObj != null)
            {
                Vector3 newPos = targetObj.transform.position;
                Vector3 newPos2 = new Vector3(transform.position.x, transform.position.y, 0);
                Vector3 newPos3 = new Vector3(newPos.x, newPos.y, 0);
                //현재 자신의 노드와 목표지점의 노드를 비교하여 그곳까지 갈수있는지 확인
                GraphNode node1 = AstarPath.active.GetNearest(newPos2, NNConstraint.Default).node;
                GraphNode node2 = AstarPath.active.GetNearest(newPos3, NNConstraint.Default).node;
                //못간다
                if (!PathUtilities.IsPathPossible(node1, node2))
                {
                    path = null;
                    isPathPossible = false;
                }
                else
                    isPathPossible = true;

                if (isPathPossible)
                {
                    if (isMoveChange)
                    {
                        seeker.StartPath(transform.position, newPos, OnPathComplete);
                        isMoveChange = false;
                    }
                }
            }
            else
            {
                Vector3 newPos = PDG.Player.instance.transform.position;
                Vector3 newPos2 = new Vector3(transform.position.x, transform.position.y, 0);
                Vector3 newPos3 = new Vector3(newPos.x, newPos.y, 0);
                //현재 자신의 노드와 목표지점의 노드를 비교하여 그곳까지 갈수있는지 확인
                GraphNode node1 = AstarPath.active.GetNearest(newPos2, NNConstraint.Default).node;
                GraphNode node2 = AstarPath.active.GetNearest(newPos3, NNConstraint.Default).node;
                //못간다
                if (!PathUtilities.IsPathPossible(node1, node2))
                {
                    path = null;
                    isPathPossible = false;
                }
                else
                    isPathPossible = true;

                if (isPathPossible)
                {
                    if (isMoveChange)
                    {
                        seeker.StartPath(transform.position, newPos, OnPathComplete);
                        isMoveChange = false;
                    }
                }
            }
            yield return null;
        }
    }
    public void OnPathComplete(Path _path)
    {
        //길에 오류가 있는지 검사
        if (!_path.error)
        {
            path = _path;
            currentWaypoint = 0;
        }
        else
            isMoveChange = true;
    }
    public void Attack()
    {
        if (GetComponent<AudioSource>()) GetComponent<AudioSource>().Play();
        if (attackType.Equals("fighter"))
        {
            if (targetObj != null)
            {
                float angle = GetAngle(transform.position, targetObj.transform.position);
                if (isRangeAttack)
                {
                    RaycastHit2D[] hits = Physics2D.BoxCastAll(transform.position, new Vector2(2, 1), angle, dir, fighterRange, 1 << 12 | 1 << 24);
                    for (int i = 0; i < hits.Length; i++)
                    {
                        if (hits[i].collider.GetComponent<MOB.Monster>()) hits[i].collider.GetComponent<MOB.Monster>().SetDamage(fDamage, -dir, 1, ELEMENTAL_TYPE.NONE, 0.0f);
                        if (hits[i].collider.GetComponent<Boss_STG>()) hits[i].collider.GetComponent<Boss_STG>().SetDamage(fDamage, hits[i].point);
                        if (hits[i].collider.GetComponent<Boss_NPC>()) hits[i].collider.GetComponent<Boss_NPC>().SetDamage(fDamage, hits[i].point);
                        if (hits[i].collider.GetComponent<Boss_Golem>()) hits[i].collider.GetComponent<Boss_Golem>().SetDamage(fDamage, hits[i].point);
                        if (hits[i].collider.GetComponent<Boss_Stone>()) hits[i].collider.GetComponent<Boss_Stone>().SetDamage(fDamage, hits[i].point, false);

                        GameObject attackEffect = Instantiate(AttackHitEffect);
                        attackEffect.transform.position = hits[i].point;
                        Destroy(attackEffect, 2f);
                    }
                }
                else
                {
                    RaycastHit2D hit = Physics2D.BoxCast(transform.position, new Vector2(2, 1), angle, dir, fighterRange, 1 << 12 | 1 << 24);
                    if (hit)
                    {
                        if (hit.collider.GetComponent<MOB.Monster>()) hit.collider.GetComponent<MOB.Monster>().SetDamage(fDamage, -dir, 1, ELEMENTAL_TYPE.NONE, 0.0f);
                        if (hit.collider.GetComponent<Boss_STG>()) hit.collider.GetComponent<Boss_STG>().SetDamage(fDamage, hit.point);
                        if (hit.collider.GetComponent<Boss_NPC>()) hit.collider.GetComponent<Boss_NPC>().SetDamage(fDamage, hit.point);
                        if (hit.collider.GetComponent<Boss_Golem>()) hit.collider.GetComponent<Boss_Golem>().SetDamage(fDamage, hit.point);
                        if (hit.collider.GetComponent<Boss_Stone>()) hit.collider.GetComponent<Boss_Stone>().SetDamage(fDamage, hit.point, false);

                        GameObject attackEffect = Instantiate(AttackHitEffect);
                        attackEffect.transform.position = hit.point;
                        Destroy(attackEffect, 2f);

                    }
                }
            }
        }
        isMoveStop = false;
        if(Random.Range(0, 100) < 40)
        {
            isTargeting = false;
            targetObj = null;
            isMoveChange = true;
        }
    }
    private float GetAngle(Vector3 vStart, Vector3 vEnd)
    {
        Vector3 v = vEnd - vStart;

        return Mathf.Atan2(v.y, v.x) * Mathf.Rad2Deg;
    }
    private Vector3 GetPosition(Vector3 vStart, float angle)
    {
        float x = Mathf.Cos(Mathf.Deg2Rad * angle) + vStart.x;
        float y = Mathf.Sin(Mathf.Deg2Rad * angle) + vStart.y;

        return new Vector3(x, y, 0);
    }
    #endregion
}
