﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MercenaryAjitController : MonoBehaviour
{
    [SerializeField] private GameObject controllerObj;
    [SerializeField] private SpriteRenderer controllerSR;
    [SerializeField] private bool isSelect = false;
    [SerializeField] private Vector2 hitSize;
    [SerializeField] private Vector2 offSet;
    [SerializeField] private string firstComment;
    [SerializeField] private string secondComment;
    [SerializeField] private string thirdCommnet;
    [SerializeField] private bool isOpen = false;
    [SerializeField] private bool isUse = false;

    [Header("SlolText")]
    [SerializeField] private string[] SoloTexts;
    [SerializeField] private Text nowSoloText;
    [SerializeField] private Font _font;
    [SerializeField] private bool isTargeting = false;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(SoloText());
    }
    IEnumerator SoloText()
    {
        if (nowSoloText == null)
        {
            GameObject obj = new GameObject();
            Text nText = obj.AddComponent<Text>();
            nText.font = _font;
            nText.alignment = TextAnchor.MiddleCenter;
            nText.fontSize = 20;
            nText.horizontalOverflow = HorizontalWrapMode.Overflow;
            nowSoloText = nText;
            obj.transform.SetParent(GameObject.Find("Canvas").transform);
            obj.transform.SetSiblingIndex(15);
            obj.transform.localScale = Vector3.one;
        }

        while (!isUse)
        {
            if (!isSelect)
            {
                if (nowSoloText) nowSoloText.text = LocalizeManager.GetLocalize(SoloTexts[Random.Range(0, SoloTexts.Length)]);
                yield return new WaitForSeconds(2.0f);
                if (nowSoloText) nowSoloText.text = string.Empty;
            }
            else
            {
                if (nowSoloText) nowSoloText.text = string.Empty;
            }
            yield return new WaitForSeconds(Random.Range(2, 5));
        }
    }
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireCube(transform.position + (Vector3)offSet, hitSize);
    }
    // Update is called once per frame
    void Update()
    {
        if (isUse) return;
        if (nowSoloText != null)
        {
            nowSoloText.gameObject.transform.position = Camera.main.WorldToScreenPoint(controllerObj.transform.position);
        }
        RaycastHit2D hit = Physics2D.BoxCast(transform.position + (Vector3)offSet, hitSize, 0, Vector2.zero, 0, 1 << 9);
        RaycastHit2D hit3 = Physics2D.CircleCast(transform.position, 15, Vector2.zero, 0, 1 << 9);
        if (hit3 && isTargeting)
        {
            Vector3 dir = hit3.collider.transform.position - transform.position;
            dir.Normalize();
            if (dir.x < 0) GetComponent<SpriteRenderer>().flipX = true;
            else GetComponent<SpriteRenderer>().flipX = false;
        }

        if (hit && !isSelect && !PDG.Dungeon.instance.newObjectSelected)
        {
            isSelect = true;
            if (nowSoloText != null) nowSoloText.text = string.Empty;
            PDG.Dungeon.instance.newObjectSelected = true;
            PDG.Dungeon.instance.newObjectSelectedObj = gameObject;
            if (isOpen)
                TextOpenController.instance.ShowText(LocalizeManager.GetLocalize(secondComment), Color.white, 24, controllerObj, 2f, true);
            else
                TextOpenController.instance.ShowText(LocalizeManager.GetLocalize(firstComment), Color.white, 24, controllerObj, 2f, true);
            controllerSR.sprite = PDG.Dungeon.instance.isJoystrick ? PDG.Dungeon.GetJoyStrickKeySprite(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_ACTIVE]) : PDG.Dungeon.GetJoyStrickKeySprite(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ACTIVE]);
            controllerObj.SetActive(true);
        }
        else if (!hit)
        {
            isSelect = false;
            if (PDG.Dungeon.instance.newObjectSelectedObj == gameObject)
            {
                PDG.Dungeon.instance.newObjectSelectedObj = null;
                PDG.Dungeon.instance.newObjectSelected = false;
            }
            controllerObj.SetActive(false);
            if (TextOpenController.instance.targetObj == controllerObj) TextOpenController.instance.TextClose(Color.white);
        }
        if (isSelect)
        {
            if (PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ACTIVE], true) || PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_ACTIVE], true) && PDG.Dungeon.instance.closeCoolTime > 0.2f)
            {
                PDG.Dungeon.instance.closeCoolTime = 0.0f;
                SoundManager.instance.StartAudio(new string[1] { "Object/active_F" }, VOLUME_TYPE.EFFECT);
                isSelect = false;
                controllerObj.SetActive(false);
                if (!isOpen)
                {
                    isOpen = true;
                    if (PDG.Dungeon.instance.newObjectSelectedObj == gameObject)
                    {
                        PDG.Dungeon.instance.newObjectSelectedObj = null;
                        PDG.Dungeon.instance.newObjectSelected = false;
                    }
                }
                else
                {
                    int glowingCount = Inventorys.Instance.glowindskull;
                    if(glowingCount >= 15)
                    {
                        Inventorys.Instance.glowindskull -= 15;
                        SaveAndLoadManager.instance.SaveItem(new ItemSave
                        {
                            _id = "glowing_skull",
                            _nCount = Inventorys.Instance.glowindskull
                        }, true);
                        PDG.Dungeon.instance.isMercenaryAjit = true;
                        TextOpenController.instance.ShowText(LocalizeManager.GetLocalize(thirdCommnet), Color.white, 24, controllerObj, 2f, true);
                        isUse = true;
                        Destroy(nowSoloText);
                        if (PDG.Dungeon.instance.newObjectSelectedObj == gameObject)
                        {
                            PDG.Dungeon.instance.newObjectSelectedObj = null;
                            PDG.Dungeon.instance.newObjectSelected = false;
                        }
                    }
                    else
                    {
                        isOpen = false;
                        TextOpenController.instance.ShowText(LocalizeManager.GetLocalize(thirdCommnet + "2"), Color.white, 24, controllerObj, 2f, true);
                    }
                }
            }
        }
    }
}
