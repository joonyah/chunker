﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MercenaryManager : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        if (SaveAndLoadManager.instance.LoadNPC("Mercenary_fighter"))
        {
            transform.Find("Fighter").gameObject.SetActive(true);
        }
    }
}
