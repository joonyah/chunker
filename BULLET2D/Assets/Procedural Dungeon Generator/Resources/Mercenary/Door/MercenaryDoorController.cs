﻿using Pathfinding;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MercenaryDoorController : MonoBehaviour
{
    public string _id;
    [SerializeField] private GameObject controllerObj;
    [SerializeField] private GameObject controllerObj2;
    [SerializeField] private SpriteRenderer controllerSR;
    [SerializeField] private bool isSelect = false;
    [SerializeField] private bool isSelect2 = false;
    [SerializeField] private Vector2 hitSize;
    [SerializeField] private Vector2 hitSize2;
    [SerializeField] private Vector2 controllerPos;
    [SerializeField] private string _openText;
    [SerializeField] private string _openTextPrev;
    [SerializeField] private string _openTextNext;
    [SerializeField] private Image spakeImage;
    [SerializeField] private GameObject timerObj;
    [SerializeField] private bool isUsing = false;
    [SerializeField] private bool isUse = false;
    [SerializeField] private AudioSource audio;
    [SerializeField] private AudioClip[] clips;
    [SerializeField] private float fTime = 3.0f;
    [SerializeField] private GameObject OpenMercenary;
    [SerializeField] private Sprite openSprite;
    [SerializeField] private float prevTextTime = 1.0f;
    [SerializeField] private bool isSkip = false;
    [SerializeField] private Animator animator;
    [SerializeField] private Vector3 dir;
    [SerializeField] private Text nowSoloText;
    [SerializeField] private Font _font;
    #region -- AI Variabled --
    [SerializeField] private Seeker seeker;
    [Header("AI Movement")]
    //플레이어에게 갈 수 있는지 체크
    [SerializeField] private bool isPathPossible = false;
    //이동할 좌표를 바꿔야할 상태인지 체크
    [SerializeField] private bool isMoveChange = true;
    // 초마다 몇번의 길 업데이트를 할것인지
    [SerializeField] private float updateRate = 2f;
    [HideInInspector] public bool pathIsEnabled = false;
    // AI에서 다음 웨이 포인트까지 계속되는 웨이 포인트까지의 최대 거리
    [SerializeField] private float nextWaypointDistance = 1;
    // 우리가 현재 움직이고있는 웨이 포인트
    private int currentWaypoint = 0;
    // 계산 된 경로
    public Path path;
    #endregion
    void Start()
    {
        controllerObj.transform.localPosition = controllerPos;
        spakeImage = GameObject.Find("Canvas").transform.Find("Spake").GetComponent<Image>();
        timerObj = GameObject.Find("Canvas").transform.Find("Timer").gameObject;
        clips = Resources.LoadAll<AudioClip>("Object/Dungeon_2_Computer");


        if (nowSoloText == null)
        {
            GameObject obj = new GameObject();
            Text nText = obj.AddComponent<Text>();
            nText.font = _font;
            nText.alignment = TextAnchor.MiddleCenter;
            nText.fontSize = 20;
            nText.horizontalOverflow = HorizontalWrapMode.Overflow;
            nowSoloText = nText;
            obj.transform.SetParent(GameObject.Find("Canvas").transform);
            obj.transform.localScale = Vector3.one;
            obj.transform.SetSiblingIndex(15);
        }
    }
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireCube(OpenMercenary.transform.position, hitSize2);
    }
    IEnumerator TextOpen()
    {
        nowSoloText.text = LocalizeManager.GetLocalize(_openTextPrev + "_" + _id + "_" + Random.Range(0, 3));
        yield return new WaitForSeconds(1.0f);
        nowSoloText.text = string.Empty;
    }
    void Update()
    {
        if (isSkip)
        {
            if (isPathPossible)
            {
                if (path == null)
                {
                    return;
                }
                if (Vector3.Distance(PDG.Player.instance.transform.position, transform.position) < 1.5f)
                {
                    if (animator.GetBool("isMove")) animator.SetBool("isMove", false);
                }
                else
                {
                    if (currentWaypoint >= path.vectorPath.Count)
                    {
                        if (pathIsEnabled)
                            return;
                        //경로의 끝에 도착
                        OpenMercenary.SetActive(false);
                        return;
                    }
                    pathIsEnabled = false;
                    // 다음 웨이 포인트 방향
                    dir = (path.vectorPath[currentWaypoint] - OpenMercenary.transform.position).normalized;
                    OpenMercenary.transform.eulerAngles = Vector3.zero;
                    Move(dir, 10);
                    float dist = Vector3.Distance(OpenMercenary.transform.position, path.vectorPath[currentWaypoint]);
                    if (dist < nextWaypointDistance)
                    {
                        currentWaypoint++;
                        return;
                    }
                }
            }
            return;
        }

        if (nowSoloText != null)
        {
            nowSoloText.gameObject.transform.position = Camera.main.WorldToScreenPoint(controllerObj.transform.position);
        }
        else if (nowSoloText == null)
        {
            GameObject obj = new GameObject();
            Text nText = obj.AddComponent<Text>();
            nText.font = _font;
            nText.alignment = TextAnchor.MiddleCenter;
            nText.fontSize = 20;
            nText.horizontalOverflow = HorizontalWrapMode.Overflow;
            nowSoloText = nText;
            obj.transform.SetParent(GameObject.Find("Canvas").transform);
            obj.transform.localScale = Vector3.one;
            obj.transform.SetSiblingIndex(15);
        }
        prevTextTime -= Time.deltaTime;
        RaycastHit2D hitPrev = Physics2D.CircleCast(transform.position, 10, Vector2.zero, 0, 1 << 9);
        RaycastHit2D hit = Physics2D.BoxCast(transform.position, hitSize, 0, Vector2.zero, 0, 1 << 9);
        RaycastHit2D hit3 = Physics2D.BoxCast(OpenMercenary.transform.position, hitSize2, 0, Vector2.zero, 0, 1 << 9);
        if (hitPrev && !hit && !isUse)
        {
            if (prevTextTime <= 0.0f)
            {
                StartCoroutine(TextOpen());
                prevTextTime = 2.0f;
            }
        }
        if(isSelect2 && !hit3)
        {
            StartCoroutine(UpdatePath());
            TextOpenController.instance.ShowText(LocalizeManager.GetLocalize("mercenary_skip"), Color.white, 24, controllerObj2);
            isSelect2 = false;
            isSkip = true;
        }
        if (hit && !isSelect && !PDG.Dungeon.instance.newObjectSelected && !isUse && !isUsing)
        {
            isSelect = true;
            PDG.Dungeon.instance.newObjectSelected = true;
            PDG.Dungeon.instance.newObjectSelectedObj = gameObject;
            controllerSR.sprite = PDG.Dungeon.instance.isJoystrick ? PDG.Dungeon.GetJoyStrickKeySprite(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_ACTIVE]) : PDG.Dungeon.GetJoyStrickKeySprite(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ACTIVE]);
            TextOpenController.instance.ShowText(LocalizeManager.GetLocalize(_openText), Color.white, 24, controllerObj, 0.5f, true);
            controllerObj.SetActive(true);
        }
        else if (!hit)
        {
            isSelect = false;
            if (PDG.Dungeon.instance.newObjectSelectedObj == gameObject)
            {
                PDG.Dungeon.instance.newObjectSelectedObj = null;
                PDG.Dungeon.instance.newObjectSelected = false;
            }
            isUsing = false;
            controllerObj.SetActive(false);
            PlayerCombat.instance.tCoroutine = null;
            if (TextOpenController.instance.targetObj == controllerObj)
            {
                if(TextOpenController.instance._text.text == LocalizeManager.GetLocalize(_openText))
                    TextOpenController.instance.TextClose(Color.white);
            }
        }
        if (isSelect)
        {
            if (PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ACTIVE], true) || PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_ACTIVE], true) && PDG.Dungeon.instance.closeCoolTime > 0.2f && !isUsing)
            {
                PDG.Dungeon.instance.closeCoolTime = 0.0f;
                SoundManager.instance.StartAudio(new string[1] { "Object/active_F" }, VOLUME_TYPE.EFFECT);
                isSelect = false;
                if (PDG.Dungeon.instance.newObjectSelectedObj == gameObject)
                {
                    PDG.Dungeon.instance.newObjectSelectedObj = null;
                    PDG.Dungeon.instance.newObjectSelected = false;
                }
                controllerObj.SetActive(false);
                isUsing = true;
                PlayerCombat.instance.tCoroutine = StartCoroutine(TimerCoroutine());
            }
        }
        if (isSelect2)
        {
            if (PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ACTIVE], true) || PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_ACTIVE], true) && PDG.Dungeon.instance.closeCoolTime > 0.2f && !isUsing)
            {
                PDG.Dungeon.instance.closeCoolTime = 0.0f;
                SoundManager.instance.StartAudio(new string[1] { "Object/active_F" }, VOLUME_TYPE.EFFECT);
                isSelect2 = false;
                if (PDG.Dungeon.instance.newObjectSelectedObj == gameObject)
                {
                    PDG.Dungeon.instance.newObjectSelectedObj = null;
                    PDG.Dungeon.instance.newObjectSelected = false;
                }
                controllerObj2.SetActive(false);
                Vector3 pos = controllerObj2.transform.position;
                pos.y -= 1;
                GameObject obj = Instantiate(Resources.Load<GameObject>("Mercenary/Mercenary"));
                obj.transform.position = pos;
                OpenMercenary.SetActive(false);
                PDG.Dungeon.instance.isMercenaryAjit = true;
                TextOpenController.instance.TextClose(Color.white);
            }
        }
    }
    private void Move(Vector2 _dir, float _speed)
    {
        dir = _dir;
        if (_dir.x < 0) OpenMercenary.GetComponent<SpriteRenderer>().flipX = true;
        else OpenMercenary.GetComponent<SpriteRenderer>().flipX = false;

        animator.SetBool("isMove", true);
        OpenMercenary.transform.Translate(_dir * _speed * Time.deltaTime);
    }
    IEnumerator TimerCoroutine()
    {
        timerObj.SetActive(true);
        float t = 0.0f;
        while (isUsing)
        {
            Vector3 p = controllerObj.transform.position;
            p.y -= 0.5f;
            timerObj.transform.position = Camera.main.WorldToScreenPoint(p);

            if (audio && !audio.isPlaying)
            {
                audio.clip = clips[Random.Range(0, clips.Length)];
                audio.volume = (SoundManager.isMute || (PDG.Dungeon.instance == null ? false : PDG.Dungeon.instance.isPause) || (PDG.Player.instance == null ? false : PDG.Player.instance.isDie)) ? 0 : SoundManager.EffectVolume;
                audio.Play();
            }

            if (TextOpenController.instance.targetObj == controllerObj)
            {
                TextOpenController.instance.TextClose(Color.white);
            }
            t += Time.deltaTime;
            int tInt = Mathf.CeilToInt(fTime - t);
            timerObj.transform.Find("Text").GetComponent<Text>().text = tInt.ToString();
            timerObj.transform.Find("Image").GetComponent<Image>().fillAmount = t / fTime;
            timerObj.transform.Find("TextCont").GetComponent<Text>().text = LocalizeManager.GetLocalize("dooropening");
            if (t >= fTime) break;
            yield return new WaitForSeconds(Time.deltaTime);
        }
        audio.Stop();
        if (isUsing)
        {
            isUse = true;
            if (PDG.Dungeon.instance.newObjectSelectedObj == gameObject)
            {
                PDG.Dungeon.instance.newObjectSelectedObj = null;
                PDG.Dungeon.instance.newObjectSelected = false;
            }
            if (transform.Find("minimap")) transform.Find("minimap").gameObject.SetActive(false);
            StartCoroutine(Spake());
        }
        timerObj.SetActive(false);
        PlayerCombat.instance.tCoroutine = null;
    }
    IEnumerator Spake()
    {
        PDG.Dungeon.instance.isEndingDoorOpen = true;
        float a = 0.0f;
        while (true)
        {
            a += Time.deltaTime * 4;
            spakeImage.color = new Color(1, 1, 1, a);
            if (a > 1.0f) break;
            yield return new WaitForSeconds(Time.deltaTime);
        }
        spakeImage.color = new Color(1, 1, 1, 1);
        a = 1.0f;
        GetComponent<SpriteRenderer>().sprite = openSprite;
        SoundManager.instance.StartAudio(new string[1] { "Object/lastDoor" }, VOLUME_TYPE.EFFECT);
        OpenMercenary.SetActive(true);
        TextOpenController.instance.ShowText(LocalizeManager.GetLocalize(_openTextNext + "_" + _id), Color.white, 24, controllerObj2, 2.0f, true);
        isSelect2 = true;
        SaveAndLoadManager.instance.SaveNPC("Mercenary_" + _id);
        while (true)
        {
            a -= Time.deltaTime * 4;
            spakeImage.color = new Color(1, 1, 1, a);
            if (a <= 0.0f) break;
            yield return new WaitForSeconds(Time.deltaTime);
        }
        spakeImage.color = new Color(1, 1, 1, 0);
    }
    IEnumerator UpdatePath()
    {
        Vector3 newPos = PDG.Dungeon.instance.GetFloorPosition();
        while (true)
        {
            Vector3 newPos2 = new Vector3(OpenMercenary.transform.position.x, OpenMercenary.transform.position.y, 0);
            Vector3 newPos3 = new Vector3(newPos.x, newPos.y, 0);
            //현재 자신의 노드와 목표지점의 노드를 비교하여 그곳까지 갈수있는지 확인
            GraphNode node1 = AstarPath.active.GetNearest(newPos2, NNConstraint.Default).node;
            GraphNode node2 = AstarPath.active.GetNearest(newPos3, NNConstraint.Default).node;
            //못간다
            if (!PathUtilities.IsPathPossible(node1, node2))
            {
                path = null;
                isPathPossible = false;
            }
            else
                isPathPossible = true;

            if (isPathPossible)
            {
                if (isMoveChange)
                {
                    seeker.StartPath(OpenMercenary.transform.position, newPos, OnPathComplete);
                    isMoveChange = false;
                }
            }
            yield return null;
        }
    }
    public void OnPathComplete(Path _path)
    {
        //길에 오류가 있는지 검사
        if (!_path.error)
        {
            path = _path;
            currentWaypoint = 0;
        }
        else
            isMoveChange = true;
    }
}
