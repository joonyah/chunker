﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BatController : MonoBehaviour
{
    private Animator animator;
    private AudioSource audio;
    public bool isMove = false;
    public float range = 20;
    public SpriteRenderer[] renderers;
    bool isStart = false;

    private void Awake()
    {
        animator = GetComponent<Animator>();
        audio = GetComponent<AudioSource>();
        audio.volume = 0;
    }

    // Start is called before the first frame update
    void Start()
    {
        AnimatorStateInfo _stateInfo = animator.GetCurrentAnimatorStateInfo(0);
        animator.Play(_stateInfo.fullPathHash, -1, Random.Range(0.0f, 1.0f));
        animator.SetFloat("animSpeed", Random.Range(1.0f, 1.5f));

        if (isMove)
        {
            StartCoroutine(MOVE());
        }
    }
    // Update is called once per frame
    void Update()
    {
        if (PDG.Player.instance == null) return;

        if(!isStart)
        {
            for (int i = 0; i < renderers.Length; i++)
            {
                renderers[i].GetComponent<BatController>().isStart = isStart;
                renderers[i].enabled = isStart;
            }
            audio.volume = 0;
            return;
        }

        float dis = Vector3.Distance(PDG.Player.instance.transform.position, transform.position);
        if (dis > range) audio.volume = 0;
        if( (SoundManager.isMute || (PDG.Dungeon.instance == null ? false : PDG.Dungeon.instance.isPause) || (PDG.Player.instance == null ? false : PDG.Player.instance.isDie)))
        {
            audio.volume = 0;
        }
        else
        {
            float tVolume = SoundManager.AmbientVolume; //1
            float t = tVolume / range;                     // 0.1
            audio.volume = tVolume - (dis * t);
        }
    }

    IEnumerator MOVE()
    {
        float delay = 2.0f;
        float delayNow = 0.0f;
        Vector3 startPos = Vector3.zero;
        Vector3 endPos = Vector3.zero;
        Vector3 centerPos = Vector3.zero;
        float t = 0.0f;
        while (true)
        {
            if (PDG.Player.instance == null)
            {
                yield return new WaitForSeconds(Time.deltaTime);
                continue;
            }
            if (!isStart) delayNow += Time.deltaTime;
            if (delayNow > delay)
            {
                isStart = true;
                for (int i = 0; i < renderers.Length; i++)
                {
                    renderers[i].GetComponent<BatController>().isStart = isStart;
                    renderers[i].enabled = isStart;
                }
                delayNow = 0.0f;
                float angle = Random.Range(0, 360);
                float angle2 = angle + 180;
                Vector3 ranPos = GetPosition(PDG.Player.instance.transform.position, angle);
                Vector3 ranPos2 = GetPosition(PDG.Player.instance.transform.position, angle2);
                Vector3 dir = ranPos - PDG.Player.instance.transform.position;
                Vector3 dir2 = ranPos2 - PDG.Player.instance.transform.position;
                dir.Normalize();
                dir2.Normalize();
                startPos = PDG.Player.instance.transform.position + (dir * 20);
                endPos = PDG.Player.instance.transform.position + (dir2 * 20);


                Vector3 tempRan = endPos - startPos;
                tempRan /= 2;
                centerPos = startPos + tempRan;
                tempRan = GetPosition(centerPos, Random.Range(90, 270));
                Vector3 dir3 = tempRan - centerPos;
                dir3.Normalize();
                centerPos = centerPos + (dir3 * Random.Range(10, 20));
                t = 0.0f;
            }

            if(isStart)
            {
                Vector3 lerp1 = Vector3.Lerp(startPos, centerPos, t);
                Vector3 lerp2 = Vector3.Lerp(centerPos, endPos, t);
                transform.position = Vector3.Lerp(lerp1, lerp2, t);
                t += Time.deltaTime;

                if (Vector3.Distance(endPos, transform.position) < 2.0f)
                {
                    isStart = false;
                    delay = Random.Range(5.0f, 60.0f);
                }
            }
            yield return new WaitForSeconds(Time.deltaTime);
        }
    }
    public static float GetAngle(Vector3 vStart, Vector3 vEnd)
    {
        Vector3 v = vEnd - vStart;

        return Mathf.Atan2(v.y, v.x) * Mathf.Rad2Deg;
    }
    public static Vector3 GetPosition(Vector3 vStart, float angle)
    {
        float x = Mathf.Cos(Mathf.Deg2Rad * angle) + vStart.x;
        float y = Mathf.Sin(Mathf.Deg2Rad * angle) + vStart.y;

        return new Vector3(x, y, 0);
    }
}
