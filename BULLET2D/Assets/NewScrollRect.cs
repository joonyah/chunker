﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NewScrollRect : ScrollRect
{
    protected override void Start()
    {
        base.Start();
        onValueChanged.AddListener(ListenerMethod);
    }
    protected override void OnEnable()
    {
    }
    public void ListenerMethod(Vector2 value)
    {
        if (MemorialManager.instance != null)
        {
            if (MemorialManager.instance.isOpen)
            {
                if (MemorialManager.instance.popupObj.activeSelf)
                {
                    MemorialManager.instance.popupObj.SetActive(false);
                }
            }
        }

        content.anchorMin = new Vector2(0, 1);
        content.anchorMax = new Vector2(1, 1);
        float y = content.anchoredPosition3D.y;
        if (content.sizeDelta.y < 510)
        {
            if (y < 510)
            {
                Vector2 temp = new Vector2(0, -Mathf.Abs(y));
                content.offsetMin = temp;
            }
        }
        else if (y > content.sizeDelta.y)
        {
            Vector2 temp = new Vector2(0, -content.sizeDelta.y);
            content.offsetMin = temp;
            temp = new Vector3(0, temp.y + content.sizeDelta.y);
            content.offsetMax = temp;
        }
    }
}
