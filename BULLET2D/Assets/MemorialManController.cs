﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MemorialManController : MonoBehaviour
{
    [SerializeField] private GameObject controllerObj;
    [SerializeField] private SpriteRenderer controllerSR;
    [SerializeField] private bool isSelect = false;
    [SerializeField] private Vector2 hitSize;
    [SerializeField] private Vector2 offSet;
    [SerializeField] private string LocalObjName;

    [Header("SlolText")]
    [SerializeField] private string[] SoloTexts;
    [SerializeField] private Text nowSoloText;
    [SerializeField] private Font _font;
    [SerializeField] private bool isTargeting = false;
    [SerializeField] private GameObject rewardObj;

    [SerializeField] private GameObject tutorialPrefabs;
    private GameObject tutorialObj;
    // Start is called before the first frame update
    void Start()
    {
        CheckReward();
        StartCoroutine(SoloText());

        int t2 = SaveAndLoadManager.instance.LoadItem("TUTORIAL_MEMORIAL");
        if (t2 != 1)
        {
            tutorialObj = Instantiate(tutorialPrefabs);
            tutorialObj.transform.SetParent(GameObject.Find("Canvas").transform);
            tutorialObj.transform.localScale = Vector3.one;
            tutorialObj.transform.SetSiblingIndex(26);
        }

    }
    public void CheckReward()
    {
        bool c = false;
        AchievementsData[] aDatas = SheetManager.Instance.AchievementsDB.dataArray;
        for (int i = 0; i < aDatas.Length; i++)
        {
            AchievementData loadData = SaveAndLoadManager.instance.LoadAchievements(aDatas[i].ID);
            if (loadData._clear)
            {
                if (!loadData._reward && aDatas[i].REWARD)
                {
                    c = true;
                    break;
                }
            }
        }
        rewardObj.SetActive(c);
    }
    IEnumerator SoloText()
    {
        if (nowSoloText == null)
        {
            GameObject obj = new GameObject();
            Text nText = obj.AddComponent<Text>();
            nText.font = _font;
            nText.alignment = TextAnchor.MiddleCenter;
            nText.fontSize = 20;
            nText.horizontalOverflow = HorizontalWrapMode.Overflow;
            nowSoloText = nText;
            obj.transform.SetParent(GameObject.Find("Canvas").transform);
            obj.transform.localScale = Vector3.one;
            obj.transform.SetSiblingIndex(15);
        }
        while (true)
        {
            if(!isSelect)
            {
                nowSoloText.text = LocalizeManager.GetLocalize(SoloTexts[Random.Range(0, SoloTexts.Length)]);
                yield return new WaitForSeconds(2.0f);
                nowSoloText.text = string.Empty;
            }
            else
            {
                nowSoloText.text = string.Empty;
            }
            yield return new WaitForSeconds(Random.Range(2, 5));
        }
    }
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireCube(transform.position + (Vector3)offSet, hitSize);
    }
    // Update is called once per frame
    void Update()
    {
        if (tutorialObj)
        {
            tutorialObj.transform.Find("Text").GetComponent<UnityEngine.UI.Text>().text = LocalizeManager.GetLocalize("tutorial_memorial");
            Vector3 pos = Camera.main.WorldToScreenPoint(controllerObj.transform.position);
            pos.y += 55;
            tutorialObj.transform.position = pos;
        }


        if (nowSoloText != null)
        {
            nowSoloText.gameObject.transform.position = Camera.main.WorldToScreenPoint(controllerObj.transform.position);
        }
        RaycastHit2D hit = Physics2D.BoxCast(transform.position + (Vector3)offSet, hitSize, 0, Vector2.zero, 0, 1 << 9);
        RaycastHit2D hit3 = Physics2D.CircleCast(transform.position, 15, Vector2.zero, 0, 1 << 9);
        if (hit3 && isTargeting)
        {
            Vector3 dir = hit3.collider.transform.position - transform.position;
            dir.Normalize();
            if (dir.x < 0) GetComponent<SpriteRenderer>().flipX = true;
            else GetComponent<SpriteRenderer>().flipX = false;
        }
        if (hit && !isSelect && !PDG.Dungeon.instance.newObjectSelected)
        {
            isSelect = true;
            if (nowSoloText != null) nowSoloText.text = string.Empty;
            PDG.Dungeon.instance.newObjectSelected = true;
            PDG.Dungeon.instance.newObjectSelectedObj = gameObject;
            TextOpenController.instance.ShowText(LocalizeManager.GetLocalize(LocalObjName), Color.white, 24, controllerObj, 2f, true);
            controllerSR.sprite = PDG.Dungeon.instance.isJoystrick ? PDG.Dungeon.GetJoyStrickKeySprite(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_ACTIVE]) : PDG.Dungeon.GetJoyStrickKeySprite(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ACTIVE]);
            controllerObj.SetActive(true);
        }
        else if (!hit)
        {
            isSelect = false;
            if (PDG.Dungeon.instance.newObjectSelectedObj == gameObject)
            {
                PDG.Dungeon.instance.newObjectSelectedObj = null;
                PDG.Dungeon.instance.newObjectSelected = false;
            }
            controllerObj.SetActive(false);
            if (TextOpenController.instance.targetObj == controllerObj) TextOpenController.instance.TextClose(Color.white);
        }
        if (isSelect && !MemorialManager.instance.isOpen)
        {
            if (PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ACTIVE], true) || PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_ACTIVE], true) && PDG.Dungeon.instance.closeCoolTime > 0.2f)
            {
                SaveAndLoadManager.instance.SaveItem(new ItemSave { _id = "TUTORIAL_MEMORIAL", _nCount = 1 }, true);
                if (tutorialObj) Destroy(tutorialObj);
                PDG.Dungeon.instance.closeCoolTime = 0.0f;
                SoundManager.instance.StartAudio(new string[1] { "Object/active_F" }, VOLUME_TYPE.EFFECT);
                isSelect = false;
                if (PDG.Dungeon.instance.newObjectSelectedObj == gameObject)
                {
                    PDG.Dungeon.instance.newObjectSelectedObj = null;
                    PDG.Dungeon.instance.newObjectSelected = false;
                }
                controllerObj.SetActive(false);
                MemorialManager.instance.ShowMemorial();
            }
        }


    }
}
