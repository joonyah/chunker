﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MailController : MonoBehaviour
{
    public static MailController instance;
    [SerializeField] private Sprite mailSpeite;
    [SerializeField] private Sprite fSprite;
    public bool isRepair;
    [SerializeField] private GameObject controllerObj;
    [SerializeField] private SpriteRenderer controllerSR;
    [SerializeField] private bool isSelect = false;
    private bool isCheckingMail = false;
    [SerializeField] private bool isWaiting = false;
    private void Awake()
    {
        if (instance == null) instance = this;
    } 

    // Start is called before the first frame update
    void Start()
    {
        int t = SaveAndLoadManager.instance.LoadItem("RepairMode");
        if (t == 1) isRepair = true;
        else isRepair = false;


        string _date = System.DateTime.Now.ToString("yyyyMMdd");
        MailSaveData mData = SaveAndLoadManager.instance.LoadMail("mail_first");
        if (mData._id.Equals(string.Empty))
        {
            SaveAndLoadManager.instance.SaveMail(new MailSaveData
            {
                _id = "mail_first",
                date = _date,
                isCheck = false,
                isPush = true
            });
            isCheckingMail = true;
        }
        else if(mData.isPush && !mData.isCheck)
        {
            isCheckingMail = true;
        }

        MailSaveData mData2 = SaveAndLoadManager.instance.LoadMail("mail_loop_daily");
        if (mData2._id.Equals(string.Empty))
        {
            SaveAndLoadManager.instance.SaveMail(new MailSaveData
            {
                _id = "mail_loop_daily",
                date = _date,
                isCheck = false,
                isPush = true
            });
            isCheckingMail = true;
        }
        else if (!mData2.date.Equals(_date))
        {
            SaveAndLoadManager.instance.SaveMail(new MailSaveData
            {
                _id = "mail_loop_daily",
                date = _date,
                isCheck = false,
                isPush = true
            });
            isCheckingMail = true;
        }
        else if (mData2.isPush && !mData2.isCheck)
        {
            isCheckingMail = true;
        }
        controllerObj.SetActive(true);
        controllerSR.sprite = mailSpeite;
    }

    // Update is called once per frame
    void Update()
    {
        RaycastHit2D hit = Physics2D.BoxCast(transform.position, new Vector2(1.0f, 2.8f), 0, Vector2.zero, 0, 1 << 9);

        if (isRepair)
        {
            if (hit && !isSelect && !PDG.Dungeon.instance.newObjectSelected)
            {
                isSelect = true;
                PDG.Dungeon.instance.newObjectSelected = true;
                PDG.Dungeon.instance.newObjectSelectedObj = gameObject;
                controllerObj.SetActive(true);
                controllerObj.GetComponent<SpriteRenderer>().sprite = PDG.Dungeon.instance.isJoystrick ? PDG.Dungeon.GetJoyStrickKeySprite(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_ACTIVE]) : PDG.Dungeon.GetJoyStrickKeySprite(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ACTIVE]);
                TextOpenController.instance.ShowText(LocalizeManager.GetLocalize("ajit_mailbox"), Color.white, 24, controllerObj, 2.0f);
            }
            else if (!hit && !isCheckingMail)
            {
                isSelect = false;
                if (PDG.Dungeon.instance.newObjectSelectedObj == gameObject)
                {
                    PDG.Dungeon.instance.newObjectSelectedObj = null;
                    PDG.Dungeon.instance.newObjectSelected = false;
                }
                controllerObj.SetActive(false);
                if (TextOpenController.instance.targetObj == controllerObj) TextOpenController.instance.TextClose(Color.white);
            }
            else if (!hit && isCheckingMail)
            {
                isSelect = false;
                if (PDG.Dungeon.instance.newObjectSelectedObj == gameObject)
                {
                    PDG.Dungeon.instance.newObjectSelectedObj = null;
                    PDG.Dungeon.instance.newObjectSelected = false;
                }
                controllerObj.SetActive(true);
                controllerSR.sprite = mailSpeite;
            }
        }
        else
        {
            if (hit && !isSelect && !TextOpenController.instance.isOpen && !PDG.Dungeon.instance.newObjectSelected)
            {
                isSelect = true;
                PDG.Dungeon.instance.newObjectSelected = true;
                PDG.Dungeon.instance.newObjectSelectedObj = gameObject;
                controllerObj.SetActive(true);
                controllerObj.GetComponent<SpriteRenderer>().sprite = PDG.Dungeon.instance.isJoystrick ? PDG.Dungeon.GetJoyStrickKeySprite(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_ACTIVE]) : PDG.Dungeon.GetJoyStrickKeySprite(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ACTIVE]);
                TextOpenController.instance.ShowText(LocalizeManager.GetLocalize("ajit_repair_comment"), Color.white, 24, controllerObj, 2.0f);
            }
            else if (!hit) 
            {
                isSelect = false;
                if (PDG.Dungeon.instance.newObjectSelectedObj == gameObject)
                {
                    PDG.Dungeon.instance.newObjectSelectedObj = null;
                    PDG.Dungeon.instance.newObjectSelected = false;
                }
                controllerObj.SetActive(false);
                if (TextOpenController.instance.targetObj == controllerObj) TextOpenController.instance.TextClose(Color.white);
            }
        }

        if (isSelect)
        {
            if ((PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ACTIVE], true) || PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_ACTIVE], true)) && PDG.Dungeon.instance.closeCoolTime > 0.2f)
            {
                PDG.Dungeon.instance.closeCoolTime = 0.0f;
                SoundManager.instance.StartAudio(new string[1] { "Object/active_F" }, VOLUME_TYPE.EFFECT);
                isSelect = false;
                if (PDG.Dungeon.instance.newObjectSelectedObj == gameObject)
                {
                    PDG.Dungeon.instance.newObjectSelectedObj = null;
                    PDG.Dungeon.instance.newObjectSelected = false;
                }
                controllerObj.SetActive(false);
                if (!isWaiting)
                {
                    if (!isRepair) MakeController.instance.ShowMake("box_mail", "mail", gameObject);
                    else if (isCheckingMail) SandMailManager.instance.ShowSandMail();
                }
                else
                {
                    TextOpenController.instance.ShowText(LocalizeManager.GetLocalize("object_waiting"), Color.white, 24, controllerObj, 2.0f, true);
                }

            }
        }
    }
}
