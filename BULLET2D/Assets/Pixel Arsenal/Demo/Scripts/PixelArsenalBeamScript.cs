﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace PixelArsenal
{

    public class PixelArsenalBeamScript : MonoBehaviour
    {
        public static PixelArsenalBeamScript instance;
        [Header("Prefabs")]
        public GameObject[] beamLineRendererPrefab;
        public GameObject[] beamStartPrefab;
        public GameObject[] beamEndPrefab;

        private int currentBeam = 0;

        [Header("BeamStart")]
        public GameObject startObj;
        public bool isStart = false;
        public float fAttackDelay = 1.0f;
        public float fAttackDelayNow = 1.0f;

        private GameObject beamStart;
        private GameObject beamEnd;
        private GameObject beam;
        private LineRenderer line;

        [Header("Adjustable Variables")]
        public float beamEndOffset = 1f; //How far from the raycast hit point the end effect is positioned
        public float textureScrollSpeed = 8f; //How fast the texture scrolls along the beam
        public float textureLengthScale = 3; //Length of the beam texture

        [Header("Put Sliders here (Optional)")]
        public Slider endOffSetSlider; //Use UpdateEndOffset function on slider
        public Slider scrollSpeedSlider; //Use UpdateScrollSpeed function on slider

        [Header("Put UI Text object here to show beam name")]
        public Text textBeamName;

        private void Awake()
        {
            if (instance == null) instance = this;
        }

        // Use this for initialization
        void Start()
        {
            if (textBeamName)
                textBeamName.text = beamLineRendererPrefab[currentBeam].name;
            if (endOffSetSlider)
                endOffSetSlider.value = beamEndOffset;
            if (scrollSpeedSlider)
                scrollSpeedSlider.value = textureScrollSpeed;
        }

        void Update()
        {
            //    if (Input.GetMouseButtonDown(0))
            //    {
            //        beamStart = Instantiate(beamStartPrefab[currentBeam], new Vector3(0, 0, 0), Quaternion.identity) as GameObject;
            //        beamEnd = Instantiate(beamEndPrefab[currentBeam], new Vector3(0, 0, 0), Quaternion.identity) as GameObject;
            //        beam = Instantiate(beamLineRendererPrefab[currentBeam], new Vector3(0, 0, 0), Quaternion.identity) as GameObject;
            //        line = beam.GetComponent<LineRenderer>();
            //    }
            if (Input.GetMouseButtonUp(0) && isStart)
            {
                Destroy(beamStart);
                Destroy(beamEnd);
                Destroy(beam);
                isStart = false;
            }
            
            if (Input.GetMouseButton(0) && isStart)
            {
                Vector3 mPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                mPos.z = 0;
                Vector3 tdir = mPos - startObj.transform.position;
                ShootBeamInDir(startObj.transform.position, tdir, Vector3.Distance(startObj.transform.position, mPos));
            }

        }
        public void BeamStart(GameObject sObj)
        {
            startObj = sObj;
            beamStart = Instantiate(beamStartPrefab[currentBeam], new Vector3(0, 0, 0), Quaternion.identity) as GameObject;
            beamEnd = Instantiate(beamEndPrefab[currentBeam], new Vector3(0, 0, 0), Quaternion.identity) as GameObject;
            beam = Instantiate(beamLineRendererPrefab[currentBeam], new Vector3(0, 0, 0), Quaternion.identity) as GameObject;
            line = beam.GetComponent<LineRenderer>();
            isStart = true;
        }

        public void nextBeam() // Next beam
        {
            if (currentBeam < beamLineRendererPrefab.Length - 1)
                currentBeam++;
            else
                currentBeam = 0;

            if (textBeamName)
                textBeamName.text = beamLineRendererPrefab[currentBeam].name;
        }

        public void previousBeam() // Previous beam
        {
            if (currentBeam > -0)
                currentBeam--;
            else
                currentBeam = beamLineRendererPrefab.Length - 1;

            if (textBeamName)
                textBeamName.text = beamLineRendererPrefab[currentBeam].name;
        }

        public void UpdateEndOffset()
        {
            beamEndOffset = endOffSetSlider.value;
        }

        public void UpdateScrollSpeed()
        {
            textureScrollSpeed = scrollSpeedSlider.value;
        }

        void ShootBeamInDir(Vector3 start, Vector3 dir, float dis)
        {
#if UNITY_5_5_OR_NEWER
            line.positionCount = 2;
#else
		            line.SetVertexCount(2); 
#endif
            line.SetPosition(0, start);
            beamStart.transform.position = start;

            Vector3 end = Vector3.zero;
            int mask = 1 << 8 | 1 << 12;
            RaycastHit2D hit = Physics2D.Raycast(start, dir, dis, mask);
            if (hit)
                end = (Vector3)hit.point - (dir.normalized * beamEndOffset);
            else
                end = Camera.main.ScreenToWorldPoint(Input.mousePosition);//transform.position + (dir * 100);
            end.z = 0;
            beamEnd.transform.position = end;
            line.SetPosition(1, end);

            beamStart.transform.LookAt(beamEnd.transform.position);
            beamEnd.transform.LookAt(beamStart.transform.position);

            float distance = Vector3.Distance(start, end);
            line.sharedMaterial.mainTextureScale = new Vector2(distance / textureLengthScale, 1);
            line.sharedMaterial.mainTextureOffset -= new Vector2(Time.deltaTime * textureScrollSpeed, 0);

            // HIT 처리
            fAttackDelayNow += Time.deltaTime;
            if (hit.collider.gameObject.layer.Equals(12))
            {
                if(fAttackDelayNow >= fAttackDelay)
                {
                    fAttackDelayNow = 0.0f;
                    hit.collider.gameObject.GetComponent<MOB.Monster>().SetDamage(PDG.Player.instance.isLeftNear ? PDG.Player.instance.damageNear : PDG.Player.instance.damage, -dir, 1, PDG.Player.instance.left_elemetanl, PDG.Player.instance.left_elemnetalTime);

                    if (PDG.Player.instance.isLeftNear && PDG.Player.instance.isCritical)
                    {
                        GameObject criEffect = ObjManager.Call().GetObject("CriticalHitEffect");
                        criEffect.transform.position = hit.point;
                        criEffect.SetActive(true);
                    }
                }
            }
        }
    }
}