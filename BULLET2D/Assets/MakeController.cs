﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class MakeController : MonoBehaviour
{
    public static MakeController instance;
    [SerializeField] private Transform makePartsParent;
    [SerializeField] private GameObject makeObj;
    [SerializeField] private GameObject makePartsObj;

    [Header("CellRegeneration")]
    [SerializeField] private GameObject makeCellObj;
    [SerializeField] private Text UpgradeText;
    [SerializeField] private Text RegenerationText;
    [SerializeField] private Transform regenerationParant;
    [SerializeField] private GameObject LevelObj;
    [SerializeField] private GameObject UpgradeObj;
    [SerializeField] private GameObject ResultBtnObj;
    [SerializeField] private Button UpgradeBtn;
    [SerializeField] private Button RegenerationBtn;

    [Header("Maker Horizontal")]
    [SerializeField] private Transform makeHorizontalListTransform;
    [SerializeField] private Transform makeHorizontalMaterialsTransform;
    [SerializeField] private GameObject makeHorizontalObj;
    [SerializeField] private Image resultImg;
    [SerializeField] private Text resultText;
    [SerializeField] private GameObject listPrefabs;
    [SerializeField] private GameObject materialPrefabs;
    [SerializeField] private GameObject LoadingObj;
    [SerializeField] private GameObject LoadingMakeObj;
    [SerializeField] private GameObject ResultObj;
    [SerializeField] private Image resultImage2;
    [SerializeField] private GameObject timerObj;

    private string nowMakeID = string.Empty;
    private List<GameObject> partsObjList = new List<GameObject>();
    private List<MakeListController> makeList = new List<MakeListController>();
    private List<GameObject> materialList = new List<GameObject>();

    [SerializeField] private Button[] MakeBtn;
    [SerializeField] private GameObject npcObj;

    [SerializeField] private RuntimeAnimatorController helldogAim;
    [SerializeField] private RuntimeAnimatorController darkAim;
    [SerializeField] private RuntimeAnimatorController doctorAim;

    [SerializeField] private AudioSource makeAudio;
    [SerializeField] private AudioClip hellgodClip;
    [SerializeField] private AudioClip darkClip;
    [SerializeField] private AudioClip doctorClip;

    [SerializeField] private GameObject contentObj;
    [SerializeField] private Text contentText;
    [SerializeField] private Text lackText;
    [SerializeField] private Text resultText2;
    [SerializeField] private Text resultText3;

    [SerializeField] private Sprite scrollSprite;
    [SerializeField] private GameObject filterObj;
    List<MakeMaterialController> MaterialList = new List<MakeMaterialController>();

    [SerializeField] private Text BossRushSkullCountText;
    [SerializeField] private GameObject BossRUshSkullObj;

    public Dictionary<string, int> DicMakeCount = new Dictionary<string, int>();
    [SerializeField] private Material greyMaterial;
    [SerializeField] private Material defaultMaterial;

    Coroutine lackCoroutine;
    private bool isWaitng = false;
    private string npcName;
    public bool isOpen = false;
    public bool isRepair = false;
    bool isUse = false;
    MakeListController nowMakingData = null;
    private int joyNum = 0;
    int nowFilter = 0;
    private void Awake()
    {
        if (instance == null) instance = this;
    }
    private void Start()
    {
        timerObj = GameObject.Find("Canvas").transform.Find("Timer").gameObject;
    }
    private void Update()
    {
        if (isWaitng)
        {
            if (Input.anyKey && PDG.Dungeon.instance.closeCoolTime > 0.2f)
            {
                PDG.Dungeon.instance.closeCoolTime = 0.0f;
                isWaitng = false;
                return;
            }
        }

        if (isOpen && isUse && (Input.GetKeyDown(KeyCode.Escape) || PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_CANCLE], true)) && !LoadingObj.activeSelf && PDG.Dungeon.instance.closeCoolTime > 0.2f)
        {
            PDG.Dungeon.instance.closeCoolTime = 0.0f;
            isUse = false;
            nowMakingData.EquipmentObj.SetActive(false);
            nowMakingData = null;
        }
        if (isOpen && !isUse && (Input.GetKeyDown(KeyCode.Escape) || PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_CANCLE], true)) && !LoadingObj.activeSelf && PDG.Dungeon.instance.closeCoolTime > 0.2f) 
        {
            PDG.Dungeon.instance.closeCoolTime = 0.0f;
            isWaitng = false;
            Cancle();
        }
        else if (isOpen && (PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ACTIVE], true) || PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_CONFIRM], true)) && PDG.Dungeon.instance.closeCoolTime > 0.2f)
        {
            PDG.Dungeon.instance.closeCoolTime = 0.0f;
            MakeBtn[0].onClick.Invoke();
        }
        if (isOpen && (Input.GetAxis("Mouse ScrollWheel") * 100 != 0))
        {
            float max = -makeHorizontalListTransform.GetComponent<RectTransform>().sizeDelta.y;
            Vector3 min = makeHorizontalListTransform.GetComponent<RectTransform>().offsetMin;
            min.y -= Input.GetAxis("Mouse ScrollWheel") * 150;
            if (min.y > -510) min.y = -510;
            if (min.y < -makeHorizontalListTransform.GetComponent<RectTransform>().sizeDelta.y) min.y = -makeHorizontalListTransform.GetComponent<RectTransform>().sizeDelta.y;
            makeHorizontalListTransform.GetComponent<RectTransform>().offsetMin = min;
        }
        if(isOpen && PDG.Dungeon.instance.isJoystrick)
        {
            float joyX = Input.GetAxisRaw("Horizontal_joy");
            float joyY = Input.GetAxisRaw("Vertical_joy");
            if (joyY > 0 && PDG.Dungeon.instance.closeCoolTime > 0.2f)
            {
                PDG.Dungeon.instance.closeCoolTime = 0.0f;
                joyNum--;
                if (joyNum < 0) joyNum = 0;
                else ListClickEvent(makeList[joyNum].mData, makeList[joyNum], makeList[joyNum].mData.Resulttype);
                float y = makeList[joyNum].GetComponent<RectTransform>().anchoredPosition3D.y - 25.0f;
                if (Mathf.FloorToInt(y) >= -400)
                {
                    float max = -makeHorizontalListTransform.GetComponent<RectTransform>().sizeDelta.y;
                    Vector3 min = makeHorizontalListTransform.GetComponent<RectTransform>().offsetMin;
                    min.y -= 50;
                    if (min.y > -510) min.y = -510;
                    if (min.y < -makeHorizontalListTransform.GetComponent<RectTransform>().sizeDelta.y) min.y = -makeHorizontalListTransform.GetComponent<RectTransform>().sizeDelta.y;
                    makeHorizontalListTransform.GetComponent<RectTransform>().offsetMin = min;
                }
            }
            if (joyY < 0 && PDG.Dungeon.instance.closeCoolTime > 0.2f)
            {
                PDG.Dungeon.instance.closeCoolTime = 0.0f;
                joyNum++;
                if (joyNum >= makeList.Count) joyNum = makeList.Count - 1;
                else ListClickEvent(makeList[joyNum].mData, makeList[joyNum], makeList[joyNum].mData.Resulttype);
                float y = makeList[joyNum].GetComponent<RectTransform>().anchoredPosition3D.y - 25.0f;
                if (Mathf.FloorToInt(y) < -590)
                {
                    float max = -makeHorizontalListTransform.GetComponent<RectTransform>().sizeDelta.y;
                    Vector3 min = makeHorizontalListTransform.GetComponent<RectTransform>().offsetMin;
                    min.y += 50;
                    if (min.y > -510) min.y = -510;
                    if (min.y < -makeHorizontalListTransform.GetComponent<RectTransform>().sizeDelta.y) min.y = -makeHorizontalListTransform.GetComponent<RectTransform>().sizeDelta.y;
                    makeHorizontalListTransform.GetComponent<RectTransform>().offsetMin = min;
                }
            }
            if (npcName.Equals("equipment") || npcName.Equals("manufacture"))
            {
                if (joyX > 0 && PDG.Dungeon.instance.closeCoolTime > 0.2f)
                {
                    nowFilter++;
                    if (nowFilter > 7) nowFilter = 0;
                    ShowMakeToFilter(nowFilter);
                }
                if (joyX < 0 && PDG.Dungeon.instance.closeCoolTime > 0.2f)
                {
                    nowFilter--;
                    if (nowFilter < 0) nowFilter = 7;
                    ShowMakeToFilter(nowFilter);
                }
            }
        }
        if (contentObj.activeSelf)
        {
            contentObj.transform.position = Input.mousePosition;
        }
        if (BossRUshSkullObj.activeSelf)
        {
            BossRushSkullCountText.text = Inventorys.Instance.glowingSkullText.text;
        }
    }
    public void ShowMake(string _id, string _npcName, GameObject _npcObj, int level = 0, bool _isUpgrade = false, int _filter = 0)
    {
        string filterName = string.Empty;
        if (_filter == 0) filterName = "CT";
        else if (_filter == 1) filterName = "CG";
        else if (_filter == 2) filterName = "CB";
        else if (_filter == 3) filterName = "CD";
        else if (_filter == 4) filterName = "MG";
        else if (_filter == 5) filterName = "MB";
        else if (_filter == 6) filterName = "MD";
        else if (_filter == 7) filterName = "ETC";
        else if (_filter == 8) filterName = "Passive";
        else if (_filter == 9) filterName = "Fulid";
        BossRUshSkullObj.SetActive(false);
        PDG.Player.instance.isCameraOff = true;
        resultText3.gameObject.SetActive(false);
        SoundManager.instance.StartAudio(new string[1] { "Object/makeOpen" }, VOLUME_TYPE.EFFECT);
        isOpen = true;
        GetComponent<Image>().enabled = true;
        for (int i = 0; i < partsObjList.Count; i++)
        {
            GameObject obj = partsObjList[i];
            Destroy(obj);
        }
        for (int i = 0; i < makeList.Count; i++)
        {
            GameObject obj = makeList[i].gameObject;
            Destroy(obj);
        }
        for (int i = 0; i < materialList.Count; i++)
        {
            GameObject obj = materialList[i];
            Destroy(obj);
        }
        partsObjList.Clear();
        makeList.Clear();
        materialList.Clear();
        nowMakeID = _id;
        npcObj = _npcObj;
        npcName = _npcName;
        nowFilter = _filter;
        filterObj.SetActive(false);
        FilterImages[8].gameObject.SetActive(false);
        FilterImages[9].gameObject.SetActive(false);
        if (_npcName.Equals("mail"))
        {
            MakeData mData = System.Array.Find(SheetManager.Instance.MakeDB.dataArray, item => item.ID.Equals(_id) && item.Maker.Equals(_npcName));
            for (int i = 0; i < mData.Materials.Length; i++)
            {
                GameObject obj = Instantiate(makePartsObj);
                Sprite s = Resources.Load<Sprite>("Item/Material/" + mData.Materialstype[i] + "/" + mData.Materials[i]);
                obj.GetComponent<MakeItemController>().spriteImg.sprite = s;
                obj.GetComponent<MakeItemController>()._id = mData.Materialstype[i] + "_" + mData.Materials[i];
                obj.GetComponent<MakeItemController>().idText.text = LocalizeManager.GetLocalize(mData.Materialstype[i] + "_" + mData.Materials[i]);
                obj.GetComponent<MakeItemController>().countText.text = mData.Counts[i].ToString();
                obj.transform.SetParent(makePartsParent);
                obj.transform.localScale = Vector3.one;
                partsObjList.Add(obj);
            }
            makeObj.SetActive(true);
            for (int i = 0; i < MakeBtn.Length; i++)
            {
                MakeBtn[i].onClick.RemoveAllListeners();
                MakeBtn[i].onClick.AddListener(() => { Make(mData, null); });
            }
        }
        else if (_npcName.Equals("cell_regeneration"))
        {
            if (level == 0 || _isUpgrade)
            {
                MakeData mData = System.Array.Find(SheetManager.Instance.MakeDB.dataArray, item => item.ID.Equals(_npcName + "_level_" + level));
                for (int i = 0; i < mData.Materials.Length; i++)
                {
                    GameObject obj = Instantiate(makePartsObj);
                    Sprite s = Resources.Load<Sprite>("Item/Material/" + mData.Materialstype[i] + "/" + mData.Materials[i]);
                    obj.GetComponent<MakeItemController>().spriteImg.sprite = s;
                    obj.GetComponent<MakeItemController>()._id = mData.Materialstype[i] + "_" + mData.Materials[i];
                    obj.GetComponent<MakeItemController>().idText.text = LocalizeManager.GetLocalize(mData.Materialstype[i] + "_" + mData.Materials[i]);
                    obj.GetComponent<MakeItemController>().countText.text = mData.Counts[i].ToString();
                    obj.transform.SetParent(regenerationParant);
                    obj.transform.localScale = Vector3.one;
                    partsObjList.Add(obj);
                }
                LevelObj.SetActive(false);
                UpgradeObj.SetActive(true);
                makeCellObj.SetActive(true);
                for (int i = 0; i < MakeBtn.Length; i++)
                {
                    MakeBtn[i].onClick.RemoveAllListeners();
                    MakeBtn[i].onClick.AddListener(() => { Make(mData, null); });
                }
                ResultBtnObj.SetActive(true);
            }
            else
            {
                if (level > 4)
                {

                }
                else
                {
                    UpgradeBtn.onClick.RemoveAllListeners();
                    UpgradeBtn.onClick.AddListener(() => { ShowMake(_id, _npcName, _npcObj, level + 1, true); });
                    UpgradeText.text = (level + 1) + LocalizeManager.GetLocalize("ajit_cellregeneration_upgrade");
                }
                RegenerationBtn.onClick.RemoveAllListeners();
                RegenerationBtn.onClick.AddListener(() => { Regeneration(level); });
                RegenerationText.text = level + LocalizeManager.GetLocalize("ajit_cellregeneration_recovery");
                LevelObj.SetActive(true);
                UpgradeObj.SetActive(false);
                makeCellObj.SetActive(true);
                ResultBtnObj.SetActive(false);
            }
        }
        else if (_npcName.Equals("upgrade"))
        {
            resultImg.color = new Color32(0, 0, 0, 0);
            resultImg.transform.Find("Text").gameObject.SetActive(false);
            resultText.text = string.Empty;

            MakeData[] mDatas = System.Array.FindAll(SheetManager.Instance.MakeDB.dataArray, item => item.Maker.Equals(_npcName));
            for (int m = 0; m < mDatas.Length; m++)
            {
                if (!mDatas[m].Unlock.Equals("NONE"))
                {
                    int loadUnlock = SaveAndLoadManager.instance.LoadUnlock(mDatas[m].Unlock);
                    if (loadUnlock == 0) continue;
                }
                bool levelMax = false;
                string str = mDatas[m].ID;
                int tt = str.LastIndexOf('_');
                int tLevel = 0;
                if (tt != -1 && tt == str.Length - 2)
                {
                    tLevel = int.Parse(str.Substring(str.Length - 1, 1));
                    str = str.Substring(0, str.Length - 1);
                    tLevel++;
                    str += tLevel.ToString();
                    int tIndex = System.Array.FindIndex(SheetManager.Instance.ItemsDB.dataArray, item => item.ID.Equals(str));
                    if (tIndex != -1)
                    {
                        int loadUnlock = SaveAndLoadManager.instance.LoadUnlock(mDatas[m].ID);
                        if (loadUnlock >= 1) continue;
                    }
                    else
                    {
                        int loadUnlock = SaveAndLoadManager.instance.LoadUnlock(mDatas[m].ID);
                        if (loadUnlock >= 1)
                        {
                            levelMax = true;
                        }
                    }
                }

                GameObject obj = Instantiate(listPrefabs);
                MakeListController t = obj.GetComponent<MakeListController>();
                t.mData = mDatas[m];
                t.InitSetting(1, levelMax);
                obj.transform.SetParent(makeHorizontalListTransform);
                obj.transform.localScale = Vector3.one;
                if (levelMax)
                {
                    obj.transform.Find("Materials").Find("Text").GetComponent<SetLanguage>().InitSetting("ui_upgrade_clear");
                    obj.transform.Find("Materials").Find("Text").gameObject.SetActive(true);
                }
                else
                {
                    obj.transform.Find("Materials").Find("Text").gameObject.SetActive(false);
                    MakeData mData = System.Array.Find(SheetManager.Instance.MakeDB.dataArray, item => item.ID.Equals(mDatas[m].ID) && item.Maker.Equals(_npcName));
                    for (int i = 0; i < mData.Materials.Length; i++)
                    {
                        GameObject objMaterial = Instantiate(materialPrefabs);
                        Sprite s = Resources.Load<Sprite>("Item/Material/" + mData.Materialstype[i] + "/" + mData.Materials[i]);
                        MakeMaterialController t2 = objMaterial.GetComponent<MakeMaterialController>();
                        t2._id = mData.Materialstype[i] + "_" + mData.Materials[i];
                        t2.InitSetting(s, mData.Counts[i]);
                        objMaterial.transform.SetParent(obj.transform.Find("Materials"));
                        objMaterial.transform.localScale = Vector3.one;
                        MaterialList.Add(t2);
                    }
                }
                makeList.Add(t);
            }
            makeHorizontalObj.SetActive(true);
            for (int i = 0; i < MakeBtn.Length; i++)
            {
                MakeBtn[i].onClick.RemoveAllListeners();
            }
            StartCoroutine(RectReSize());
        }
        else if (_npcName.Equals("equipment"))
        {
            BossRUshSkullObj.SetActive(false);
            FilterImages[8].gameObject.SetActive(false);
            FilterImages[9].gameObject.SetActive(false);
            for (int i = 0; i < FilterImages.Length; i++)
            {
                if (i == _filter)
                    FilterImages[i].GetComponent<RectTransform>().sizeDelta = new Vector2(60, 60);
                else
                    FilterImages[i].GetComponent<RectTransform>().sizeDelta = new Vector2(48, 48);
            }
            filterObj.SetActive(true);
            resultImg.color = new Color32(0, 0, 0, 0);
            resultImg.transform.Find("Text").gameObject.SetActive(false);
            resultText.text = string.Empty;
            MakeData[] mDatas = System.Array.FindAll(SheetManager.Instance.MakeDB.dataArray, item => item.Maker.Equals(_npcName));
            for (int m = 0; m < mDatas.Length; m++)
            {
                int maxLevel = 1;
                string tempID = mDatas[m].ID;
                int ttt = tempID.LastIndexOf('_');
                int ttLevel = 0;
                if (ttt != -1 && ttt == tempID.Length - 2)
                {
                    ttLevel = int.Parse(tempID.Substring(tempID.Length - 1, 1));
                }
                if (ttLevel == 0)
                    tempID += "_" + maxLevel;
                else
                {
                    tempID = tempID.Substring(0, tempID.Length - 1);
                    tempID += maxLevel.ToString();
                }

                while (true)
                {
                    int tIndex = System.Array.FindIndex(SheetManager.Instance.ItemsDB.dataArray, item => item.ID.Equals(tempID));
                    if (tIndex == -1)
                    {
                        maxLevel--;
                        break;
                    }
                    else
                    {
                        maxLevel++;
                        tempID = mDatas[m].ID + "_" + maxLevel;
                    }
                }

                int nowLevel = maxLevel;
                if (maxLevel != 0)
                {
                    for (int i = maxLevel; i >= 1; i--)
                    {
                        tempID = mDatas[m].ID;
                        ttt = tempID.LastIndexOf('_');
                        ttLevel = 0;
                        if (ttt != -1 && ttt == tempID.Length - 2)
                        {
                            ttLevel = int.Parse(tempID.Substring(tempID.Length - 1, 1));
                        }
                        if (ttLevel == 0)
                            tempID += "_" + i;
                        else
                        {
                            tempID = tempID.Substring(0, tempID.Length - 1);
                            tempID += i.ToString();
                        }
                        int loadUnlock = SaveAndLoadManager.instance.LoadUnlock(tempID);
                        if (loadUnlock < 1) nowLevel = i - 1;
                    }
                    string str = mDatas[m].ID;
                    int tt = str.LastIndexOf('_');
                    int tLevel = 0;
                    if (tt != -1 && tt == str.Length - 2)
                    {
                        tLevel = int.Parse(str.Substring(str.Length - 1, 1));
                    }
                    if (nowLevel == 0 && tLevel != 0)
                    {
                        continue;
                    }
                    else if (nowLevel != 0 && tLevel == 0)
                    {
                        continue;
                    }
                }

                tempID = mDatas[m].ID;
                ttt = tempID.LastIndexOf('_');
                ttLevel = 0;
                if (ttt != -1 && ttt == tempID.Length - 2)
                    tempID = tempID.Substring(0, tempID.Length - 2);
                int getCount = SaveAndLoadManager.instance.LoadItem("EquipmentSave_" + tempID);
                if (getCount < 10) continue;

                ItemsData iData = System.Array.Find(SheetManager.Instance.ItemsDB.dataArray, item => item.ID.Equals(mDatas[m].ID));
                if (!iData.Type4.Equals(filterName)) continue;

                GameObject obj = Instantiate(listPrefabs);
                MakeListController t = obj.GetComponent<MakeListController>();
                t.mData = mDatas[m];
                t.InitSetting(1);
                obj.transform.SetParent(makeHorizontalListTransform);
                obj.transform.localScale = Vector3.one;
                makeList.Add(t);

                obj.transform.Find("Materials").Find("Text").gameObject.SetActive(false);
                MakeData mData = System.Array.Find(SheetManager.Instance.MakeDB.dataArray, item => item.ID.Equals(mDatas[m].ID) && item.Maker.Equals(_npcName));
                for (int i = 0; i < mData.Materials.Length; i++)
                {
                    GameObject objMaterial = Instantiate(materialPrefabs);
                    Sprite s = Resources.Load<Sprite>("Item/Material/" + mData.Materialstype[i] + "/" + mData.Materials[i]);
                    MakeMaterialController t2 = objMaterial.GetComponent<MakeMaterialController>();
                    t2._id = mData.Materialstype[i] + "_" + mData.Materials[i];
                    t2.InitSetting(s, mData.Counts[i]);
                    objMaterial.transform.SetParent(obj.transform.Find("Materials"));
                    objMaterial.transform.localScale = Vector3.one;
                    MaterialList.Add(t2);
                }
            }
            makeHorizontalObj.SetActive(true);
            for (int i = 0; i < MakeBtn.Length; i++)
            {
                MakeBtn[i].onClick.RemoveAllListeners();
            }
            StartCoroutine(RectReSize());
        }
        else if (_npcName.Equals("equipment2"))
        {
            BossRUshSkullObj.SetActive(true);
            FilterImages[8].gameObject.SetActive(true);
            FilterImages[9].gameObject.SetActive(true);
            for (int i = 0; i < FilterImages.Length; i++)
            {
                if (i == _filter)
                    FilterImages[i].GetComponent<RectTransform>().sizeDelta = new Vector2(60, 60);
                else
                    FilterImages[i].GetComponent<RectTransform>().sizeDelta = new Vector2(48, 48);
            }
            filterObj.SetActive(true);
            resultImg.color = new Color32(0, 0, 0, 0);
            resultImg.transform.Find("Text").gameObject.SetActive(false);
            resultText.text = string.Empty;
            MakeData[] mDatas = System.Array.FindAll(SheetManager.Instance.MakeDB.dataArray, item => item.Maker.Equals(_npcName));
            List<MakeData> fList = new List<MakeData>();
            for (int m = 0; m < mDatas.Length; m++)
            {
                if (mDatas[m].ID.Contains("fluid_"))
                {
                    if (filterName.Equals("Fulid"))
                        fList.Add(mDatas[m]);
                    continue;
                }

                int maxLevel = 1;
                string tempID = mDatas[m].ID;
                int ttt = tempID.LastIndexOf('_');
                int ttLevel = 0;
                if (ttt != -1 && ttt == tempID.Length - 2)
                {
                    ttLevel = int.Parse(tempID.Substring(tempID.Length - 1, 1));
                }
                if (ttLevel == 0)
                    tempID += "_" + maxLevel;
                else
                {
                    tempID = tempID.Substring(0, tempID.Length - 1);
                    tempID += maxLevel.ToString();
                }

                while (true)
                {
                    int tIndex = System.Array.FindIndex(SheetManager.Instance.ItemsDB.dataArray, item => item.ID.Equals(tempID));
                    if (tIndex == -1)
                    {
                        maxLevel--;
                        break;
                    }
                    else
                    {
                        maxLevel++;
                        tempID = mDatas[m].ID + "_" + maxLevel;
                    }
                }

                int nowLevel = maxLevel;
                if (maxLevel != 0)
                {
                    for (int i = maxLevel; i >= 1; i--)
                    {
                        tempID = mDatas[m].ID;
                        ttt = tempID.LastIndexOf('_');
                        ttLevel = 0;
                        if (ttt != -1 && ttt == tempID.Length - 2)
                        {
                            ttLevel = int.Parse(tempID.Substring(tempID.Length - 1, 1));
                        }
                        if (ttLevel == 0)
                            tempID += "_" + i;
                        else
                        {
                            tempID = tempID.Substring(0, tempID.Length - 1);
                            tempID += i.ToString();
                        }
                        int loadUnlock = SaveAndLoadManager.instance.LoadUnlock(tempID);
                        if (loadUnlock < 1) nowLevel = i - 1;
                    }
                    string str = mDatas[m].ID;
                    int tt = str.LastIndexOf('_');
                    int tLevel = 0;
                    if (tt != -1 && tt == str.Length - 2)
                    {
                        tLevel = int.Parse(str.Substring(str.Length - 1, 1));
                    }
                    if (nowLevel == 0 && tLevel != 0)
                    {
                        continue;
                    }
                    else if (nowLevel != 0 && tLevel == 0)
                    {
                        continue;
                    }
                }

                tempID = mDatas[m].ID;
                ttt = tempID.LastIndexOf('_');
                ttLevel = 0;
                if (ttt != -1 && ttt == tempID.Length - 2)
                    tempID = tempID.Substring(0, tempID.Length - 2);

                ItemsData iData = System.Array.Find(SheetManager.Instance.ItemsDB.dataArray, item => item.ID.Equals(mDatas[m].ID));
                if (!iData.Type4.Equals(filterName)) continue;

                GameObject obj = Instantiate(listPrefabs);
                MakeListController t = obj.GetComponent<MakeListController>();
                t.mData = mDatas[m];
                t.InitSetting(1);
                obj.transform.SetParent(makeHorizontalListTransform);
                obj.transform.localScale = Vector3.one;
                makeList.Add(t);

                obj.transform.Find("Materials").Find("Text").gameObject.SetActive(false);
                MakeData mData = System.Array.Find(SheetManager.Instance.MakeDB.dataArray, item => item.ID.Equals(mDatas[m].ID) && item.Maker.Equals(_npcName));
                for (int i = 0; i < mData.Materials.Length; i++)
                {
                    GameObject objMaterial = Instantiate(materialPrefabs);
                    Sprite s = Resources.Load<Sprite>("Item/Material/" + mData.Materialstype[i] + "/" + mData.Materials[i]);
                    MakeMaterialController t2 = objMaterial.GetComponent<MakeMaterialController>();
                    t2._id = mData.Materials[i];
                    t2.InitSetting(s, mData.Counts[i]);
                    objMaterial.transform.SetParent(obj.transform.Find("Materials"));
                    objMaterial.transform.localScale = Vector3.one;
                    MaterialList.Add(t2);
                }
            }
            for (int i = 0; i < fList.Count; i++)
            {
                GameObject obj = Instantiate(listPrefabs);
                MakeListController t = obj.GetComponent<MakeListController>();
                t.mData = fList[i];
                t.InitSetting(1);
                obj.transform.SetParent(makeHorizontalListTransform);
                obj.transform.localScale = Vector3.one;
                makeList.Add(t);
                obj.transform.Find("Materials").Find("Text").gameObject.SetActive(false);
                MakeData mData = System.Array.Find(SheetManager.Instance.MakeDB.dataArray, item => item.ID.Equals(fList[i].ID));
                for (int k = 0; k < mData.Materials.Length; k++)
                {
                    GameObject objMaterial = Instantiate(materialPrefabs);
                    Sprite s = Resources.Load<Sprite>("Item/Material/" + mData.Materialstype[k] + "/" + mData.Materials[k]);
                    MakeMaterialController t2 = objMaterial.GetComponent<MakeMaterialController>();
                    t2._id = mData.Materials[k];
                    t2.InitSetting(s, mData.Counts[k]);
                    objMaterial.transform.SetParent(obj.transform.Find("Materials"));
                    objMaterial.transform.localScale = Vector3.one;
                    MaterialList.Add(t2);
                }
            }
            makeHorizontalObj.SetActive(true);
            for (int i = 0; i < MakeBtn.Length; i++)
            {
                MakeBtn[i].onClick.RemoveAllListeners();
            }
            StartCoroutine(RectReSize());
        }
        else if (_npcName.Equals("manufacture"))
        {
            for (int i = 0; i < FilterImages.Length; i++)
            {
                if (i == _filter)
                    FilterImages[i].GetComponent<RectTransform>().sizeDelta = new Vector2(60, 60);
                else
                    FilterImages[i].GetComponent<RectTransform>().sizeDelta = new Vector2(48, 48);
            }
            filterObj.SetActive(true);
            resultImg.color = new Color32(0, 0, 0, 0);
            resultImg.transform.Find("Text").gameObject.SetActive(false);
            resultText.text = string.Empty;
            MakeData[] mDatas = System.Array.FindAll(SheetManager.Instance.MakeDB.dataArray, item => item.Maker.Equals(_npcName));

            for (int m = 0; m < mDatas.Length; m++)
            {
                int maxLevel = 1;
                string tempID = mDatas[m].ID;
                int ttt = tempID.LastIndexOf('_');
                int ttLevel = 0;
                if (ttt != -1 && ttt == tempID.Length - 2)
                {
                    ttLevel = int.Parse(tempID.Substring(tempID.Length - 1, 1));
                }
                if (ttLevel == 0)
                    tempID += "_" + maxLevel;
                else
                {
                    tempID = tempID.Substring(0, tempID.Length - 1);
                    tempID += maxLevel.ToString();
                }

                while (true)
                {
                    int tIndex = System.Array.FindIndex(SheetManager.Instance.ItemsDB.dataArray, item => item.ID.Equals(tempID));
                    if (tIndex == -1)
                    {
                        maxLevel--;
                        break;
                    }
                    else
                    {
                        maxLevel++;
                        tempID = mDatas[m].ID + "_" + maxLevel;
                    }
                }

                int nowLevel = maxLevel;
                if (maxLevel != 0)
                {
                    for (int i = maxLevel; i >= 1; i--)
                    {
                        tempID = mDatas[m].ID;
                        ttt = tempID.LastIndexOf('_');
                        ttLevel = 0;
                        if (ttt != -1 && ttt == tempID.Length - 2)
                        {
                            ttLevel = int.Parse(tempID.Substring(tempID.Length - 1, 1));
                        }
                        if (ttLevel == 0)
                            tempID += "_" + i;
                        else
                        {
                            tempID = tempID.Substring(0, tempID.Length - 1);
                            tempID += i.ToString();
                        }
                        int loadUnlock = SaveAndLoadManager.instance.LoadUnlock(tempID);
                        if (loadUnlock < 1) nowLevel = i - 1;
                    }
                    string str = mDatas[m].ID;
                    int tt = str.LastIndexOf('_');
                    int tLevel = 0;
                    if (tt != -1 && tt == str.Length - 2)
                    {
                        tLevel = int.Parse(str.Substring(str.Length - 1, 1));
                    }
                    if (nowLevel == 0 && tLevel != 0)
                    {
                        continue;
                    }
                    else if (nowLevel != 0 && tLevel == 0)
                    {
                        continue;
                    }
                }

                tempID = mDatas[m].ID;
                ttt = tempID.LastIndexOf('_');
                ttLevel = 0;
                if (ttt != -1 && ttt == tempID.Length - 2)
                    tempID = tempID.Substring(0, tempID.Length - 2);

                int getCount = SaveAndLoadManager.instance.LoadItem("EquipmentSave_" + tempID);
                if (getCount < 1 && !PDG.Dungeon.instance.isBossRush) continue;

                ItemsData iData = System.Array.Find(SheetManager.Instance.ItemsDB.dataArray, item => item.ID.Equals(mDatas[m].ID));
                if (!iData.Type4.Equals(filterName)) continue;

                GameObject obj = Instantiate(listPrefabs);
                MakeListController t = obj.GetComponent<MakeListController>();
                t.mData = mDatas[m];
                t.InitSetting(1);
                obj.transform.SetParent(makeHorizontalListTransform);
                obj.transform.localScale = Vector3.one;
                makeList.Add(t);

                obj.transform.Find("Materials").Find("Icon").gameObject.SetActive(true);
                MakingStateController stateController = obj.transform.Find("Materials").Find("Icon").GetComponent<MakingStateController>();
                stateController.InitSetting();
                ManufacturesData loadData = SaveAndLoadManager.instance.LoadManufacture(tempID, PDG.Dungeon.instance.isBossRush);
                if (!loadData.id.Equals(string.Empty) || !loadData.id.Equals(""))
                {
                    for (int i = 0; i < loadData.stateid.Count; i++)
                    {
                        if (loadData.stateid[i].ToUpper().Equals("Attack".ToUpper()))
                        {
                            stateController.StateTexts[0].color = Color.green;
                            stateController.StateTexts[0].text = "+" + loadData.statevalue[i];
                        }
                        else if (loadData.stateid[i].ToUpper().Equals("Critical".ToUpper()))
                        {
                            stateController.StateTexts[1].color = Color.green;
                            stateController.StateTexts[1].text = "+" + loadData.statevalue[i];
                        }
                        else if (loadData.stateid[i].ToUpper().Equals("CriticalDamage".ToUpper()))
                        {
                            stateController.StateTexts[2].color = Color.green;
                            stateController.StateTexts[2].text = "+" + loadData.statevalue[i];
                        }
                        else if (loadData.stateid[i].ToUpper().Equals("Range".ToUpper()))
                        {
                            stateController.StateTexts[3].color = Color.green;
                            stateController.StateTexts[3].text = "+" + loadData.statevalue[i];
                        }
                        else if (loadData.stateid[i].ToUpper().Equals("Aim".ToUpper()))
                        {
                            stateController.StateTexts[4].color = Color.green;
                            stateController.StateTexts[4].text = "+" + loadData.statevalue[i];
                        }
                        else if (loadData.stateid[i].ToUpper().Equals("MultiShoot".ToUpper()))
                        {
                            stateController.StateTexts[5].color = Color.green;
                            stateController.StateTexts[5].text = "+" + loadData.statevalue[i];
                        }
                        else if (loadData.stateid[i].ToUpper().Equals("ShootCount".ToUpper()))
                        {
                            stateController.StateTexts[6].color = Color.green;
                            stateController.StateTexts[6].text = "+" + loadData.statevalue[i];
                        }
                        else if (loadData.stateid[i].ToUpper().Equals("ShootDelay".ToUpper()))
                        {
                            stateController.StateTexts[7].color = Color.green;
                            stateController.StateTexts[7].text = "-" + (loadData.statevalue[i] / 100.0f);
                        }
                    }
                }

            }
            makeHorizontalObj.SetActive(true);
            for (int i = 0; i < MakeBtn.Length; i++)
            {
                MakeBtn[i].onClick.RemoveAllListeners();
            }
            StartCoroutine(RectReSize());
        }
        else
        {
            resultImg.color = new Color32(0, 0, 0, 0);
            resultImg.transform.Find("Text").gameObject.SetActive(false);
            resultText.text = string.Empty;
            int unlockCount = 0;
            MakeData[] mDatas = System.Array.FindAll(SheetManager.Instance.MakeDB.dataArray, item => item.Maker.Equals(_npcName));
            List<MakeData> fList = new List<MakeData>();
            for (int m = 0; m < mDatas.Length; m++)
            {
                if (!mDatas[m].Unlock.Equals("NONE"))
                {
                    int loadUnlock = SaveAndLoadManager.instance.LoadUnlock(mDatas[m].ID);
                    if (loadUnlock == 0) continue;
                }
                if(mDatas[m].ID.Contains("fluid_"))
                {
                    fList.Add(mDatas[m]);
                    continue;
                }
                GameObject obj = Instantiate(listPrefabs);
                MakeListController t = obj.GetComponent<MakeListController>();
                t.mData = mDatas[m];
                t.InitSetting(1);
                obj.transform.SetParent(makeHorizontalListTransform);
                obj.transform.localScale = Vector3.one;
                makeList.Add(t);

                int u = SaveAndLoadManager.instance.LoadUnlock(mDatas[m].ID);
                if (u != 0)
                {
                    unlockCount++;
                    obj.transform.Find("Materials").Find("Text").GetComponent<SetLanguage>().InitSetting("chest_unlock_text");
                    obj.transform.Find("Materials").Find("Text").gameObject.SetActive(true);
                }
                else
                {
                    obj.transform.Find("Materials").Find("Text").gameObject.SetActive(false);
                    MakeData mData = System.Array.Find(SheetManager.Instance.MakeDB.dataArray, item => item.ID.Equals(mDatas[m].ID));
                    for (int i = 0; i < mData.Materials.Length; i++)
                    {
                        GameObject objMaterial = Instantiate(materialPrefabs);
                        Sprite s = Resources.Load<Sprite>("Item/Material/" + mData.Materialstype[i] + "/" + mData.Materials[i]);
                        MakeMaterialController t2 = objMaterial.GetComponent<MakeMaterialController>();
                        t2._id = mData.Materialstype[i] + "_" + mData.Materials[i];
                        t2.InitSetting(s, u == 0 ? mData.Counts[i] : mData.Counts2[i]);
                        objMaterial.transform.SetParent(obj.transform.Find("Materials"));
                        objMaterial.transform.localScale = Vector3.one;
                        MaterialList.Add(t2);
                    }
                }
            }
            for (int j = 0; j < 2; j++)
            {
                for (int i = 0; i < fList.Count; i++)
                {
                    GameObject obj = Instantiate(listPrefabs);
                    MakeListController t = obj.GetComponent<MakeListController>();
                    t.mData = fList[i];
                    if (j > 0)
                        t.InitSetting(j * 5);
                    else
                        t.InitSetting(1);
                    obj.transform.SetParent(makeHorizontalListTransform);
                    obj.transform.localScale = Vector3.one;
                    makeList.Add(t);
                    if (j > 0)
                    {
                        obj.transform.Find("Image").Find("Text").GetComponent<Text>().text = (j * 5).ToString();
                        obj.transform.Find("Image").Find("Text").gameObject.SetActive(true);
                    }
                    obj.transform.Find("Materials").Find("Text").gameObject.SetActive(false);
                    MakeData mData = System.Array.Find(SheetManager.Instance.MakeDB.dataArray, item => item.ID.Equals(fList[i].ID));
                    for (int k = 0; k < mData.Materials.Length; k++)
                    {
                        GameObject objMaterial = Instantiate(materialPrefabs);
                        Sprite s = Resources.Load<Sprite>("Item/Material/" + mData.Materialstype[k] + "/" + mData.Materials[k]);
                        MakeMaterialController t2 = objMaterial.GetComponent<MakeMaterialController>();
                        t2._id = mData.Materialstype[k] + "_" + mData.Materials[k];
                        if (j == 0)
                            t2.InitSetting(s, mData.Counts[k]);
                        else
                            t2.InitSetting(s, mData.Counts[k] * (j * 5));
                        objMaterial.transform.SetParent(obj.transform.Find("Materials"));
                        objMaterial.transform.localScale = Vector3.one;
                        MaterialList.Add(t2);
                    }
                }
            }

            if (_npcName.Equals("hellfogee"))
            {
                AchievementsData aData_0 = System.Array.Find(SheetManager.Instance.AchievementsDB.dataArray, item => item.ID.Equals("soulmate"));
                SaveAndLoadManager.instance.SaveAchievements(new AchievementData[1] {
                      new AchievementData { _id = aData_0.ID, _cur = unlockCount, _max = mDatas.Length, _clear = false }
                    });

            }
            if (_npcName.Equals("doctor"))
            {
                AchievementsData aData_0 = System.Array.Find(SheetManager.Instance.AchievementsDB.dataArray, item => item.ID.Equals("researchaddiction"));
                SaveAndLoadManager.instance.SaveAchievements(new AchievementData[1] {
                      new AchievementData { _id = aData_0.ID, _cur = unlockCount, _max = mDatas.Length, _clear = false }
                    });

            }

            makeHorizontalObj.SetActive(true);
            for (int i = 0; i < MakeBtn.Length; i++)
            {
                MakeBtn[i].onClick.RemoveAllListeners();
            }
            StartCoroutine(RectReSize());
        }
    }
    bool isMake = false;
    IEnumerator RectReSize()
    {
        yield return null;
        RectTransform content = makeHorizontalListTransform.GetComponent<RectTransform>();
        content.anchorMin = new Vector2(0, 1);
        content.anchorMax = new Vector2(1, 1);
        Vector2 temp = new Vector2(0, -content.sizeDelta.y);
        content.offsetMin = temp;
        temp = new Vector3(0, temp.y + content.sizeDelta.y);
        content.offsetMax = temp;
        if (-content.sizeDelta.y < -510)
        {
            makeHorizontalObj.transform.Find("Scroll View").GetComponent<ScrollRect>().vertical = true;
        }
        else
        {
            makeHorizontalObj.transform.Find("Scroll View").GetComponent<ScrollRect>().vertical = false;
        }
    }
    public void Make(MakeData _mData, MakeListController _this, int makeCount = 1)
    {
        if (isMake) return;
        bool isMakeCountCheck = false;  
        for (int i = 0; i < DicMakeCount.Count; i++)
        {
            if (_mData.Makecount == 0) continue;
            if(DicMakeCount.ToList()[i].Key.Equals(_mData.ID))
            {
                int t = _mData.Makecount;
                if (t >= DicMakeCount.ToList()[i].Value) isMakeCountCheck = true;
            }
        }
        if (isMakeCountCheck) return;
        isMake = true;
        bool check = false;
        SoundManager.instance.StartAudio(new string[1] { "Object/active_F" }, VOLUME_TYPE.EFFECT);
        if(!npcName.Equals("manufacture"))
        {
            for (int i = 0; i < _mData.Materials.Length; i++)
            {

                int c = 0;
                if(npcName.Equals("equipment2"))
                    c = SaveAndLoadManager.instance.LoadItem(_mData.Materials[i]);
                else
                    c = SaveAndLoadManager.instance.LoadItem(_mData.Materialstype[i] + "_" + _mData.Materials[i]);
                int c2 = _mData.Counts[i];
                if (c < c2)
                {
                    if (lackCoroutine != null)
                    {
                        StopCoroutine(lackCoroutine);
                        lackText.color = new Color(1, 1, 1, 0);
                    }
                    lackCoroutine = StartCoroutine(LackTextOpen());
                    SoundManager.instance.StartAudio(new string[1] { "Object/MakeFail" }, VOLUME_TYPE.EFFECT);
                    check = true;
                    isMake = false;
                    return;
                }
            }
        }
        if (check) return;
        if (!npcName.Equals("equipment") && !npcName.Equals("manufacture"))
        {
            for (int i = 0; i < _mData.Materials.Length; i++)
            {
                int c = SaveAndLoadManager.instance.LoadItem(_mData.Materialstype[i] + "_" + _mData.Materials[i]);
                int c2 = _mData.Counts[i];

                int calc = c - c2;
                if (calc == 0)
                {
                    SaveAndLoadManager.instance.DeleteItem(_mData.Materialstype[i] + "_" + _mData.Materials[i]);
                }
                else
                {
                    SaveAndLoadManager.instance.SaveItem(new ItemSave { _id = _mData.Materialstype[i] + "_" + _mData.Materials[i], _nCount = calc }, true);
                }
            }
        }
        if (_mData.ID.Equals("box_mail"))
        {
            SaveAndLoadManager.instance.SaveItem(new ItemSave { _id = "RepairMode", _nCount = 1 }, true);
            MailController.instance.isRepair = true;
            Cancle();
            return;
        }
        if (_mData.ID.Contains("cell_regeneration_level_"))
        {
            StartCoroutine(RepairRegeneration());
            return;
        }

        string path = string.Empty;
        ItemsData iData;
        string _strSpecial = string.Empty;
        bool isLevel = false;
        if (_mData.Resulttype.Equals("Item"))
        {
            iData = System.Array.Find(SheetManager.Instance.ItemsDB.dataArray, item => item.ID.Equals(_mData.ID));
            path = "Item/" + iData.Type1 + "/" + iData.Type2 + "/" + iData.ID;
        }
        else if (_mData.Resulttype.Equals("LevelUp"))
        {
            iData = System.Array.Find(SheetManager.Instance.ItemsDB.dataArray, item => item.ID.Equals(_mData.ID));
            path = "Item/" + iData.Type1 + "/" + iData.Type2 + "/" + iData.ID;
            isLevel = true;
        }
        else if (_mData.Resulttype.Equals("Special"))
        {
            _strSpecial = _mData.ID;
            iData = null;
            path = "Object/Special/" + _mData.ID + "/" + _mData.ID;
        }
        else if (_mData.Resulttype.Equals("Equipment"))
        {
            iData = System.Array.Find(SheetManager.Instance.ItemsDB.dataArray, item => item.ID.Equals(_mData.ID));
            path = "Item/" + iData.Type1 + "/" + iData.Type2 + "/" + iData.ID;
        }
        else if (_mData.Resulttype.Equals("manufacture"))
        {
            iData = System.Array.Find(SheetManager.Instance.ItemsDB.dataArray, item => item.ID.Equals(_mData.ID));
            path = "Item/" + iData.Type1 + "/" + iData.Type2 + "/" + iData.ID;
        }
        else
        {
            string[] s = _mData.ID.Split('_');
            string t = string.Empty;
            for (int i = 1; i < s.Length; i++)
            {
                if (i == 1) t += s[i];
                else t += "_" + s[i];
            }
            path = "Item/Material/" + s[0] + "/" + t;
            iData = new ItemsData
            {
                Type1 = "Material",
                Type2 = s[0],
                ID = t
            };
        }
        StartCoroutine(Loading(Resources.Load<Sprite>(path), iData, isLevel, _strSpecial, _this, makeCount));
    }
    IEnumerator RepairRegeneration()
    {
        Cancle();
        timerObj.SetActive(true);
        isRepair = true;
        float time = 15.0f;
        float timeOrigin = 15.0f;
        while (true)
        {
            Vector3 p = npcObj.transform.position;
            p.y += 2.5f;
            timerObj.transform.position = Camera.main.WorldToScreenPoint(p);
            int tInt = Mathf.CeilToInt(time);
            timerObj.transform.Find("Text").GetComponent<Text>().text = tInt.ToString();
            timerObj.transform.Find("TextCont").GetComponent<Text>().text = LocalizeManager.GetLocalize("repair_ing");
            timerObj.transform.Find("Image").GetComponent<Image>().fillAmount = 1 - time / timeOrigin;
            time -= Time.deltaTime;
            if (time <= 0.0f) break;
            yield return new WaitForSeconds(Time.deltaTime);
        }
        TextOpenController.instance.ShowText(LocalizeManager.GetLocalize("repair_clear"), Color.white, 24, npcObj, 2.0f);
        timerObj.SetActive(false);
        int Level = SaveAndLoadManager.instance.LoadItem("CellRegenerationLevel");
        if (Level == -1) Level = 0;
        SaveAndLoadManager.instance.SaveItem(new ItemSave { _id = "CellRegenerationLevel", _nCount = Level + 1 }, true);
        isRepair = false;
    }
    IEnumerator Loading(Sprite _sprite, ItemsData makeItem, bool isLevel, string _strSpecial, MakeListController _this, int makeCount)
    {
        if(npcName.Equals("equipment"))
        {
            bool check = false;
            MakeData _mData = _this.mData;
            for (int i = 0; i < _mData.Materials.Length; i++)
            {
                int c = SaveAndLoadManager.instance.LoadItem(_mData.Materialstype[i] + "_" + _mData.Materials[i]);
                int c2 = _mData.Counts[i];
                if (c < c2)
                {
                    if (lackCoroutine != null)
                    {
                        StopCoroutine(lackCoroutine);
                        lackText.color = new Color(1, 1, 1, 0);
                    }
                    lackCoroutine = StartCoroutine(LackTextOpen());
                    SoundManager.instance.StartAudio(new string[1] { "Object/MakeFail" }, VOLUME_TYPE.EFFECT);
                    check = true;
                    break;
                }
            }
            if (!check)
            {
                nowMakingData = _this;
                _this.EquipmentObj.SetActive(true);
                float coolTime = _mData.Cooltime;
                isUse = true;
                while (isUse)
                {
                    coolTime -= Time.fixedDeltaTime;
                    _this.EquipmentText.text = (Mathf.CeilToInt(coolTime)).ToString() + "S";
                    _this.EquipmentImage.fillAmount = (_mData.Cooltime - coolTime) / _mData.Cooltime;
                    if (coolTime <= 0.0f) break;
                    yield return new WaitForFixedUpdate();
                }
                if (isUse)
                {
                    _this.EquipmentText.text = LocalizeManager.GetLocalize("ui_equipment_complete");
                    for (int i = 0; i < _mData.Materials.Length; i++)
                    {
                        int c = SaveAndLoadManager.instance.LoadItem(_mData.Materialstype[i] + "_" + _mData.Materials[i]);
                        int c2 = _mData.Counts[i];
                    
                        int calc = c - c2;
                        if (calc == 0)
                        {
                            SaveAndLoadManager.instance.DeleteItem(_mData.Materialstype[i] + "_" + _mData.Materials[i]);
                        }
                        else
                        {
                            SaveAndLoadManager.instance.SaveItem(new ItemSave { _id = _mData.Materialstype[i] + "_" + _mData.Materials[i], _nCount = calc }, true);
                        }
                    };
                    Vector3 pos = Vector3.zero;
                    if (npcObj.GetComponent<EquipmentUpgradeController>())
                    {
                        int nNum = npcObj.GetComponent<EquipmentUpgradeController>().nowItemNum;
                        if (nNum >= npcObj.GetComponent<EquipmentUpgradeController>().ItemPositions.Length)
                            nNum = 0;
                        pos = npcObj.GetComponent<EquipmentUpgradeController>().ItemPositions[nNum].position;
                        npcObj.GetComponent<EquipmentUpgradeController>().nowItemNum++;
                    }
                    else
                    {
                        pos = npcObj.transform.position;
                        pos.y -= 1.0f;
                    }
                    GameObject obj = ObjManager.Call().GetObject("Item");
                    if (obj != null)
                    {
                        Item _item = obj.GetComponent<Item>();
                        _item._Item = new ItemInfo
                        {
                            Data = Inventorys.Instance.CopyItem(makeItem)
                        };
                        _item.itemCount = makeCount;
                        _item.dir = Vector2.zero;
                        _item.rPow = 0;
                        obj.transform.position = pos;
                        obj.SetActive(true);
                    }

                    if (DicMakeCount.ContainsKey(makeItem.ID))
                        DicMakeCount[makeItem.ID] += makeCount;
                    else
                        DicMakeCount.Add(makeItem.ID, makeCount);

                    bool isMakeCountCheck = false;
                    for (int i = 0; i < DicMakeCount.Count; i++)
                    {
                        if (_mData.Makecount == 0) continue;
                        if (DicMakeCount.ToList()[i].Key.Equals(_mData.ID))
                        {
                            int t = _mData.Makecount;
                            if (t >= DicMakeCount.ToList()[i].Value) isMakeCountCheck = true;
                        }
                    }
                    if (isMakeCountCheck)
                    {
                        _this.sImg.material = greyMaterial;
                    }
                }
            }
        }
        else if (npcName.Equals("equipment2"))
        {
            bool check = false;
            MakeData _mData = _this.mData;
            for (int i = 0; i < _mData.Materials.Length; i++)
            {
                int c = SaveAndLoadManager.instance.LoadItem(_mData.Materials[i]);
                int c2 = _mData.Counts[i];
                if (c < c2)
                {
                    if (lackCoroutine != null)
                    {
                        StopCoroutine(lackCoroutine);
                        lackText.color = new Color(1, 1, 1, 0);
                    }
                    lackCoroutine = StartCoroutine(LackTextOpen());
                    SoundManager.instance.StartAudio(new string[1] { "Object/MakeFail" }, VOLUME_TYPE.EFFECT);
                    check = true;
                    break;
                }
            }
            if (!check)
            {
                nowMakingData = _this;
                _this.EquipmentObj.SetActive(true);
                float coolTime = _mData.Cooltime;
                isUse = true;
                while (isUse)
                {
                    coolTime -= Time.fixedDeltaTime;
                    _this.EquipmentText.text = (Mathf.CeilToInt(coolTime)).ToString() + "S";
                    _this.EquipmentImage.fillAmount = (_mData.Cooltime - coolTime) / _mData.Cooltime;
                    if (coolTime <= 0.0f) break;
                    yield return new WaitForFixedUpdate();
                }
                if (isUse)
                {
                    _this.EquipmentText.text = LocalizeManager.GetLocalize("ui_equipment_complete");
                    for (int i = 0; i < _mData.Materials.Length; i++)
                    {
                        int c = SaveAndLoadManager.instance.LoadItem(_mData.Materials[i]);
                        int c2 = _mData.Counts[i];
                        int calc = c - c2;
                        SaveAndLoadManager.instance.SaveItem(new ItemSave { _id = _mData.Materials[i], _nCount = calc }, true);
                        Inventorys.Instance.glowindskull2 = calc;
                    };
                    Vector3 pos = Vector3.zero;
                    if (npcObj.GetComponent<EquipmentUpgradeController>())
                    {
                        int nNum = npcObj.GetComponent<EquipmentUpgradeController>().nowItemNum;
                        if (nNum >= npcObj.GetComponent<EquipmentUpgradeController>().ItemPositions.Length)
                            nNum = 0;
                        pos = npcObj.GetComponent<EquipmentUpgradeController>().ItemPositions[nNum].position;
                        npcObj.GetComponent<EquipmentUpgradeController>().nowItemNum++;
                    }
                    else
                    {
                        pos = npcObj.transform.position;
                        pos.y -= 1.0f;
                    }
                    GameObject obj = ObjManager.Call().GetObject("Item");
                    if (obj != null)
                    {
                        Item _item = obj.GetComponent<Item>();
                        _item._Item = new ItemInfo
                        {
                            Data = Inventorys.Instance.CopyItem(makeItem)
                        };
                        _item.itemCount = makeCount;
                        _item.dir = Vector2.zero;
                        _item.rPow = 0;
                        _item.isBossRush = true;
                        obj.transform.position = pos;
                        obj.SetActive(true);
                        _item.isBossRush = true;

                        PDG.Dungeon.instance.BossRushItemList.Add(obj);
                    }

                    if (DicMakeCount.ContainsKey(makeItem.ID))
                        DicMakeCount[makeItem.ID] += makeCount;
                    else
                        DicMakeCount.Add(makeItem.ID, makeCount);

                    bool isMakeCountCheck = false;
                    for (int i = 0; i < DicMakeCount.Count; i++)
                    {
                        if (_mData.Makecount == 0) continue;
                        if (DicMakeCount.ToList()[i].Key.Equals(_mData.ID))
                        {
                            int t = _mData.Makecount;
                            if (t >= DicMakeCount.ToList()[i].Value) isMakeCountCheck = true;
                        }
                    }
                    if (isMakeCountCheck)
                    {
                        _this.sImg.material = greyMaterial;
                    }
                }
            }
        }
        else if (npcName.Equals("manufacture"))
        {
            EquipmentUpgradeManager.instance.ShowManufacture(makeItem);
            Cancle();
        }
        else
        {
            resultImage2.sprite = _sprite;
            ResultObj.SetActive(false);
            if (npcObj.name.Equals("doctor"))
            {
                makeAudio.clip = doctorClip;
                LoadingMakeObj.GetComponent<Animator>().runtimeAnimatorController = doctorAim;
            }
            else if (npcObj.name.Equals("helldog"))
            {
                makeAudio.clip = hellgodClip;
                LoadingMakeObj.GetComponent<Animator>().runtimeAnimatorController = helldogAim;
            }
            else if (npcObj.name.Equals("npc_mr_dark"))
            {
                makeAudio.clip = darkClip;
                LoadingMakeObj.GetComponent<Animator>().runtimeAnimatorController = darkAim;
            }
            LoadingMakeObj.SetActive(true);
            LoadingObj.SetActive(true);
            float f = 0.0f;
            makeAudio.volume = (SoundManager.isMute || (PDG.Dungeon.instance == null ? false : PDG.Dungeon.instance.isPause) || (PDG.Player.instance == null ? false : PDG.Player.instance.isDie)) ? 0 : SoundManager.EffectVolume;
            makeAudio.Play();
            while (true)
            {
                f += Time.deltaTime;
                yield return new WaitForSeconds(Time.deltaTime);
                if (f > 2.0f) break;
            }
            LoadingObj.GetComponent<AudioSource>().Stop();
            LoadingMakeObj.SetActive(false);
            ResultObj.SetActive(true);
            SoundManager.instance.StartAudio(new string[1] { "Object/makeClear" }, VOLUME_TYPE.EFFECT);
            resultText2.text = LocalizeManager.GetLocalize("complete");
            isWaitng = true;
            while (isWaitng)
            {
                yield return null;
            }
            LoadingObj.SetActive(false);
            if (isLevel)
            {
                SaveAndLoadManager.instance.SaveUnlock(makeItem.ID, 1);

                Inventorys.Instance.ShowGetItemUI("upgrade_ui_text", Resources.Load<Sprite>("Item/" + makeItem.Type1 + "/" + makeItem.Type2 + "/" + makeItem.ID));
                ShowMake(string.Empty, "upgrade", npcObj);

                string str = makeItem.ID;
                int tt = str.LastIndexOf('_');
                int tLevel = 0;
                if (tt != -1 && tt == str.Length - 2)
                {
                    tLevel = int.Parse(str.Substring(str.Length - 1, 1));
                    str = str.Substring(0, str.Length - 2);
                    int tIndex = System.Array.FindIndex(SheetManager.Instance.ItemsDB.dataArray, item => item.ID.Equals(str));
                    if (tIndex != -1)
                    {
                        ItemsData iData = SheetManager.Instance.ItemsDB.dataArray[tIndex];
                        for (int i = 0; i < Inventorys.Instance.slots.Length; i++)
                        {
                            if (Inventorys.Instance.slots[i]._Item.Data.ID.Contains(SheetManager.Instance.ItemsDB.dataArray[tIndex].ID))
                            {
                                Inventorys.Instance.ItemRetire(iData.ID);
                                Inventorys.Instance.AddItem(new ItemInfo { Data = Inventorys.Instance.CopyItem(makeItem) }, 1, false);
                                PDG.Player.instance.SettingItem(makeItem);
                                break;
                            }
                        }
                    }
                }
            }
            else
            {
                if (makeItem == null && !_strSpecial.Equals(string.Empty))
                {
                    SaveAndLoadManager.instance.SaveUnlock(_strSpecial, 1);
                    if (_strSpecial.Equals("recycle")) PDG.Dungeon.isRecycle = true;
                    if (_strSpecial.Equals("predators"))
                    {
                        PDG.Dungeon.isPredators = true;
                        if (Inventorys.Instance != null)
                            Inventorys.Instance.predatorObj.SetActive(true);
                        else
                            GameObject.Find("Canvas").transform.Find("UI").Find("SlotPanel").Find("Predator").gameObject.SetActive(true);
                    }
                }
                else if (makeItem.Type1.Equals("Potion"))
                {
                    Inventorys.Instance.AddItem(makeItem, makeCount, true, false);
                }
                else if (makeItem.Type1.Equals("Material"))
                {
                    Inventorys.Instance.AddItem(makeItem, makeCount, true, false);
                }
                else
                {
                    ItemInfo info = new ItemInfo { Data = Inventorys.Instance.CopyItem(makeItem) };
                    Inventorys.Instance.DiscardedItem.Add(info);
                    Inventorys.Instance.DiscardedItemString.Add(info.Data.ID);
                    Vector3 tPos = npcObj.transform.position;
                    tPos.y += 1.0f;

                    float angle = -90.0f;
                    Vector3 pos = PDG.Player.GetPosition(tPos, angle);
                    Vector3 dir = pos - tPos;
                    dir.Normalize();
                    GameObject obj = ObjManager.Call().GetObject("Item");
                    if (obj != null)
                    {
                        Item _item = obj.GetComponent<Item>();
                        _item._Item = new ItemInfo
                        {
                            Data = Inventorys.Instance.CopyItem(makeItem)
                        };
                        _item.itemCount = makeCount;

                        int u = 0;
                        UnlockListsData[] UnlockDatas = System.Array.FindAll(SheetManager.Instance.UnlockListsDB.dataArray, item => item.NPCID.Equals(npcName));
                        for (int i = 0; i < UnlockDatas.Length; i++)
                        {
                            if (!UnlockDatas[i].Itemid.Equals(makeItem.ID))
                            {
                                continue;
                            }
                            u = SaveAndLoadManager.instance.LoadUnlock(UnlockDatas[i].Itemid);
                            if (u < UnlockDatas[i].Unlockcount)
                            {
                                SaveAndLoadManager.instance.SaveUnlock(UnlockDatas[i].Itemid, (u + 1));
                                if ((u + 1) == UnlockDatas[i].Unlockcount)
                                {
                                    ItemsData iData = System.Array.Find(SheetManager.Instance.ItemsDB.dataArray, item => item.ID.Equals(UnlockDatas[i].Itemid));
                                    Inventorys.Instance.ShowGetItemUI("monster_unlockitem", Resources.Load<Sprite>("Item/" + makeItem.Type1 + "/" + makeItem.Type2 + "/" + makeItem.ID));
                                }
                            }
                        }
                        if (u != 0)
                        {
                            resultText2.text = LocalizeManager.GetLocalize("complete");
                            if (npcName.Equals("doctor"))
                            {
                                while (true)
                                {
                                    if (npcObj.GetComponent<DoctorController>().itemPositions[npcObj.GetComponent<DoctorController>().nowItem].gameObject.activeSelf)
                                    {
                                        npcObj.GetComponent<DoctorController>().nowItem++;
                                        if (npcObj.GetComponent<DoctorController>().nowItem >= npcObj.GetComponent<DoctorController>().itemPositions.Length) break;
                                    }
                                    else
                                        break;
                                }
                                if (npcObj.GetComponent<DoctorController>().nowItem < npcObj.GetComponent<DoctorController>().itemPositions.Length)
                                {
                                    obj.transform.position = npcObj.GetComponent<DoctorController>().itemPositions[npcObj.GetComponent<DoctorController>().nowItem].position;
                                    npcObj.GetComponent<DoctorController>().nowItem++;
                                    obj.SetActive(true);
                                }
                                else
                                {
                                    _item.dir = dir;
                                    _item.rPow = 1;
                                    float startDis2 = 2;
                                    float angle2 = angle += 90.0f;
                                    if (angle2 <= 180) startDis2 -= (angle2 / 150.0f);
                                    else startDis2 -= ((180.0f / 150.0f) + ((180.0f - angle2) / 150.0f));
                                    obj.transform.position = pos + (dir * startDis2);
                                    obj.SetActive(true);
                                }
                            }
                            if (npcName.Equals("hellfogee"))
                            {
                                if (npcObj.GetComponent<HellfogeeController>().nowItem < npcObj.GetComponent<HellfogeeController>().itemPositions2.Length)
                                {
                                    obj.transform.position = npcObj.GetComponent<HellfogeeController>().itemPositions2[npcObj.GetComponent<HellfogeeController>().nowItem].position;
                                    npcObj.GetComponent<HellfogeeController>().nowItem++;
                                    obj.SetActive(true);
                                }
                                else
                                {
                                    _item.dir = dir;
                                    _item.rPow = 1;
                                    float startDis2 = 2;
                                    float angle2 = angle += 90.0f;
                                    if (angle2 <= 180) startDis2 -= (angle2 / 150.0f);
                                    else startDis2 -= ((180.0f / 150.0f) + ((180.0f - angle2) / 150.0f));
                                    obj.transform.position = pos + (dir * startDis2);
                                    obj.SetActive(true);
                                }
                            }
                        }
                        else
                        {
                            resultText2.text = LocalizeManager.GetLocalize("complete_prev");
                        }
                    }
                }
            }
            if (_this != null)
            {
                string tt = _this.mData.Resulttype;
                ListClickEvent(_this.mData, _this, tt, makeCount);
            }
        }
        for (int i = 0; i < MaterialList.Count; i++)
        {
            if (MaterialList[i] == null)
            {
                MaterialList.RemoveAt(i);
                i--;
            }
            else
            {
                MaterialList[i].InitSetting();
            }
        }

        isMake = false;
    }
    public void Cancle()
    {
        if (npcObj.GetComponent<EquipmentUpgradeController>())
        {
            EquipmentUpgradeManager.instance.isSubOpen = false;
        }
        PDG.Player.instance.isCameraOff = false;
        SoundManager.instance.StartAudio(new string[1] { "Object/makeClose" }, VOLUME_TYPE.EFFECT);
        isOpen = false;
        isMake = false;
        GetComponent<Image>().enabled = false;
        nowMakeID = string.Empty;
        contentObj.SetActive(false);
        makeObj.SetActive(false);
        makeHorizontalObj.SetActive(false);
        makeCellObj.SetActive(false);
    }
    public void ListClickEvent(MakeData _mData, MakeListController _this, string rType, int makeCount = 1)
    {
        resultImg.transform.Find("Text").gameObject.SetActive(false);
        SoundManager.instance.StartAudio(new string[1] { "Click_0" }, VOLUME_TYPE.EFFECT);
        for (int i = 0; i < materialList.Count; i++)
        {
            GameObject obj = materialList[i];
            Destroy(obj);
        }
        materialList.Clear();
        _this.EquipmentObj.SetActive(false);
        for (int i = 0; i < makeList.Count; i++)
        {
            if (makeList[i] == _this)
            {
                makeList[i].GetComponent<Image>().color = new Color32(0, 0, 0, 50);
                makeList[i].sText.color = Color.white;
            }
            else
            {
                makeList[i].GetComponent<Image>().color = new Color32(0, 0, 0, 0);
                makeList[i].sText.color = new Color32(128, 128, 128, 255);
            }
        }
        bool levelMax = false;
        bool specialUnlock = false;
        string path = string.Empty;
        if (_mData.Resulttype.Equals("Item"))
        {
            ItemsData iData = System.Array.Find(SheetManager.Instance.ItemsDB.dataArray, item => item.ID.Equals(_mData.ID));
            path = "Item/" + iData.Type1 + "/" + iData.Type2 + "/" + iData.ID;
        }
        else if (_mData.Resulttype.Equals("Special"))
        {
            path = "Object/Special/" + _mData.ID + "/" + _mData.ID;
        }
        else if (_mData.Resulttype.Equals("Equipment"))
        {
            ItemsData iData = System.Array.Find(SheetManager.Instance.ItemsDB.dataArray, item => item.ID.Equals(_mData.ID));
            path = "Item/" + iData.Type1 + "/" + iData.Type2 + "/" + iData.ID;
        }
        else if (_mData.Resulttype.Equals("LevelUp"))
        {
            string str = _mData.ID;
            int tt = str.LastIndexOf('_');
            int tLevel = 0;
            if (tt != -1 && tt == str.Length - 2)
            {
                tLevel = int.Parse(str.Substring(str.Length - 1, 1));
                str = str.Substring(0, str.Length - 1);
                tLevel++;
                str += tLevel.ToString();
                int tIndex = System.Array.FindIndex(SheetManager.Instance.ItemsDB.dataArray, item => item.ID.Equals(str));
                if(tIndex == -1)
                {
                    int loadUnlock = SaveAndLoadManager.instance.LoadUnlock(_mData.ID);
                    if (loadUnlock >= 1)
                    {
                        levelMax = true;
                    }
                }
            }
            ItemsData iData = System.Array.Find(SheetManager.Instance.ItemsDB.dataArray, item => item.ID.Equals(_mData.ID));
            path = "Item/" + iData.Type1 + "/" + iData.Type2 + "/" + iData.ID;
        }
        else
        {
            string[] s = _mData.ID.Split('_');
            string t = string.Empty;
            for (int i = 1; i < s.Length; i++)
            {
                if (i == 1) t += s[i];
                else t += "_" + s[i];
            }
            path = "Item/Material/" + s[0] + "/" + t;
        }

        int u = SaveAndLoadManager.instance.LoadUnlock(_mData.ID);
        if (u == 0 && _mData.Resulttype.Equals("Item"))
        {
            resultImg.color = Color.white;
            resultImg.sprite = Resources.Load<Sprite>(path);
            resultText.text = LocalizeManager.GetLocalize(_mData.ID) + " " + LocalizeManager.GetLocalize("recipe") + "\n\n" + LocalizeManager.GetLocalize(_mData.ID + "_cont");
        }
        else if (u != 0 && _mData.Resulttype.Equals("Item"))
        {
            resultImg.color = Color.white;
            resultImg.sprite = Resources.Load<Sprite>(path);
            resultText.text = LocalizeManager.GetLocalize(_mData.ID) + "\n\n" + LocalizeManager.GetLocalize(_mData.ID + "_cont");
            specialUnlock = true;
        }
        else if (levelMax)
        {
            resultImg.color = new Color(1, 1, 1, 0);
            resultImg.sprite = Resources.Load<Sprite>(path);
            resultText.text = string.Empty;
        }
        else if (_mData.Resulttype.Equals("Special"))
        {
            if (u > 0) specialUnlock = true;
            resultImg.color = Color.white;
            resultImg.sprite = Resources.Load<Sprite>(path);
            resultText.text = LocalizeManager.GetLocalize(_mData.ID + "_cont");
        }
        else
        {

            if (makeCount != 1)
            {
                resultImg.transform.Find("Text").gameObject.SetActive(true);
                resultImg.transform.Find("Text").GetComponent<Text>().text = makeCount.ToString();
            }
            resultImg.color = Color.white;
            resultImg.sprite = Resources.Load<Sprite>(path);
            resultText.text = LocalizeManager.GetLocalize(_mData.ID) + "\n\n" + LocalizeManager.GetLocalize(_mData.ID + "_cont");
        }
        if (!levelMax)
        {
            if (specialUnlock)
            {
                resultText3.text = LocalizeManager.GetLocalize("special_unlock_max");
                resultText3.gameObject.SetActive(true);
                for (int i = 0; i < MakeBtn.Length; i++)
                {
                    MakeBtn[i].onClick.RemoveAllListeners();
                }
            }
            else
            {
                resultText3.gameObject.SetActive(false);
                MakeData mData = System.Array.Find(SheetManager.Instance.MakeDB.dataArray, item => item.ID.Equals(_mData.ID) && item.Maker.Equals(npcName));
                //for (int i = 0; i < mData.Materials.Length; i++)
                //{
                //    GameObject obj = Instantiate(materialPrefabs);
                //    Sprite s = Resources.Load<Sprite>("Item/Material/" + mData.Materialstype[i] + "/" + mData.Materials[i]);
                //    MakeMaterialController t = obj.GetComponent<MakeMaterialController>();
                //    t.InitSetting(s, u == 0 ? mData.Counts[i] : mData.Counts2[i]);
                //    t._id = mData.Materialstype[i] + "_" + mData.Materials[i];
                //    obj.transform.SetParent(makeHorizontalMaterialsTransform);
                //    obj.transform.localScale = Vector3.one;
                //    materialList.Add(obj);
                //}
                for (int i = 0; i < MakeBtn.Length; i++)
                {
                    MakeBtn[i].onClick.RemoveAllListeners();
                    MakeBtn[i].onClick.AddListener(() => { Make(mData, _this, makeCount); });
                }
            }
        }
        else
        {
            resultText3.text = LocalizeManager.GetLocalize("level_max_text");
            resultText3.gameObject.SetActive(true);
        }

    }
    private void Regeneration(int _level)
    {
        Cancle();
    }
    IEnumerator LackTextOpen()
    {
        float a = 0.0f;
        lackText.color = new Color(1, 1, 1, 0);
        while (true)
        {
            a += Time.deltaTime * 3;
            lackText.color = new Color(1, 1, 1, a);
            if (a >= 1.0f) break;
            yield return new WaitForSeconds(Time.deltaTime);
        }
        lackText.color = Color.white;
        yield return new WaitForSeconds(2.0f);
        while (true)
        {
            a -= Time.deltaTime * 3;
            lackText.color = new Color(1, 1, 1, a);
            if (a <= 0.0f) break;
            yield return new WaitForSeconds(Time.deltaTime);
        }
        lackText.color = new Color(1, 1, 1, 0);
    }
    [SerializeField] private Image[] FilterImages;
    public void ShowMakeToFilter(int _type)
    {
        if (isMake) return;
        ShowMake(nowMakeID, npcName, npcObj, 0, false, _type);
    }
}
