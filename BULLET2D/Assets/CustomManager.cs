﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CustomManager : MonoBehaviour
{
    public static CustomManager instance;
    [SerializeField] private Sprite fSprite;
    [SerializeField] private GameObject controllerObj;
    [SerializeField] private SpriteRenderer controllerSR;
    [SerializeField] private bool isSelect = false;
    public bool isOpen = false;
    [SerializeField] private SpriteRenderer centerSR;
    [SerializeField] private GameObject panel;
    [SerializeField] private int nowNum = 0;
    [SerializeField] private Sprite[] sprites;
    [SerializeField] private bool[] checks;
    GameObject CostumPanel;
    Button ExitBtn;
    Button SelectBtn;
    private void Awake()
    {
        if (instance == null) instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        checks = new bool[sprites.Length];
        checks[0] = true;
        for (int i = 1; i < sprites.Length; i++)
        {
            if (SaveAndLoadManager.instance.LoadUnlock(sprites[i].name + "_Custom") != 0) checks[i] = true;
        }

        CostumPanel = GameObject.Find("Canvas").transform.Find("CostumPanel").gameObject;
        ExitBtn = CostumPanel.transform.Find("Exit").GetComponent<Button>();
        SelectBtn = CostumPanel.transform.Find("Select").GetComponent<Button>();

        ExitBtn.onClick.RemoveAllListeners();
        ExitBtn.onClick.AddListener(() => { Exit(); });
        SelectBtn.onClick.RemoveAllListeners();
        SelectBtn.onClick.AddListener(() => { Select(); });
    }

    // Update is called once per frame
    void Update()
    {
        float x = Input.GetAxisRaw("Horizontal_joy");
        float y = Input.GetAxisRaw("Vertical_joy");

        if (isOpen)
        {
            if ((PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ACTIVE], true) || PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_CONFIRM], true)) && PDG.Dungeon.instance.closeCoolTime > 0.2f) 
            {
                PDG.Dungeon.instance.closeCoolTime = 0.0f;
                SoundManager.instance.StartAudio(new string[1] { "Object/active_F" }, VOLUME_TYPE.EFFECT);
                Select();
            }
            if ((Input.GetKeyDown(KeyCode.Escape) || PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_CANCLE], true)) && PDG.Dungeon.instance.closeCoolTime > 0.2f) 
            {
                PDG.Dungeon.instance.closeCoolTime = 0.0f;
                Exit();
            }
            if ((PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ARROW_LEFT], false) || Input.GetKeyDown(KeyCode.LeftArrow) || x < 0) && PDG.Dungeon.instance.closeCoolTime > 0.2f)
            {
                PDG.Dungeon.instance.closeCoolTime = 0.0f;
                SoundManager.instance.StartAudio(new string[1] { "Click_Arrow" }, VOLUME_TYPE.EFFECT);
                while (true)
                {
                    nowNum--;
                    if (nowNum < 0) nowNum = sprites.Length - 1;
                    if (checks[nowNum]) break;
                }
                centerSR.sprite = sprites[nowNum];
            }
            if ((PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ARROW_RIGHT], false) || Input.GetKeyDown(KeyCode.RightArrow) || x > 0) && PDG.Dungeon.instance.closeCoolTime > 0.2f)
            {
                PDG.Dungeon.instance.closeCoolTime = 0.0f;
                SoundManager.instance.StartAudio(new string[1] { "Click_Arrow" }, VOLUME_TYPE.EFFECT);
                while (true)
                {
                    nowNum++;
                    if (nowNum >= sprites.Length) nowNum = 0;
                    if (checks[nowNum]) break;
                }
                centerSR.sprite = sprites[nowNum];
            }
        }

        RaycastHit2D hit = Physics2D.BoxCast(transform.position, new Vector2(2.5f, 3.5f), 0, Vector2.zero, 0, 1 << 9);
        RaycastHit2D hit2 = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero, 0, 1 << 0);
        if (hit2 && (PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ATTACK], true) || PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_ATTACK], true)) && PDG.Dungeon.instance.closeCoolTime > 0.2f)
        {
            PDG.Dungeon.instance.closeCoolTime = 0.0f;
            if (hit2.collider.name.Equals("Prev"))
            {
                SoundManager.instance.StartAudio(new string[1] { "Click_Arrow" }, VOLUME_TYPE.EFFECT);
                while (true)
                {
                    nowNum--;
                    if (nowNum < 0) nowNum = sprites.Length - 1;
                    if (checks[nowNum]) break;
                }
            }
            else if (hit2.collider.name.Equals("Next"))
            {
                SoundManager.instance.StartAudio(new string[1] { "Click_Arrow" }, VOLUME_TYPE.EFFECT);
                while (true)
                {
                    nowNum++;
                    if (nowNum >= sprites.Length) nowNum = 0;
                    if (checks[nowNum]) break;
                }
            }
            centerSR.sprite = sprites[nowNum];
        }
        if (hit && !isSelect && !isOpen && !PDG.Dungeon.instance.newObjectSelected)
        {
            isSelect = true;
            PDG.Dungeon.instance.newObjectSelected = true;
            PDG.Dungeon.instance.newObjectSelectedObj = gameObject;
            controllerObj.SetActive(true);
            controllerObj.GetComponent<SpriteRenderer>().sprite = PDG.Dungeon.instance.isJoystrick ? PDG.Dungeon.GetJoyStrickKeySprite(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_ACTIVE]) : PDG.Dungeon.GetJoyStrickKeySprite(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ACTIVE]);
            TextOpenController.instance.ShowText(LocalizeManager.GetLocalize("monitor_title"), Color.white, 24, controllerObj, 2.0f, true);
        }
        else if (!hit)
        {
            isSelect = false;
            if (PDG.Dungeon.instance.newObjectSelectedObj == gameObject)
            {
                PDG.Dungeon.instance.newObjectSelectedObj = null;
                PDG.Dungeon.instance.newObjectSelected = false;
            }
            if (TextOpenController.instance.targetObj == controllerObj)
            {
                TextOpenController.instance.TextClose(Color.white);
            }
            controllerObj.SetActive(false);
        }
        if (isSelect)
        {
            if ((PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ACTIVE], true) || PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_ACTIVE], true)) && PDG.Dungeon.instance.closeCoolTime > 0.2f)
            {
                PDG.Dungeon.instance.closeCoolTime = 0.0f;
                SoundManager.instance.StartAudio(new string[1] { "Object/active_F" }, VOLUME_TYPE.EFFECT);
                isSelect = false;
                if (PDG.Dungeon.instance.newObjectSelectedObj == gameObject)
                {
                    PDG.Dungeon.instance.newObjectSelectedObj = null;
                    PDG.Dungeon.instance.newObjectSelected = false;
                }
                isOpen = true;
                controllerObj.SetActive(false);
                StartCoroutine(SelectCustom());
            }
        }
    }
    IEnumerator SelectCustom()
    {
        PDG.Player.instance.isCameraOff = true;
        StartCoroutine(PDG.Dungeon.instance.UiOff());
        GameObject.Find("Canvas").transform.Find("HpShieldPanel").gameObject.SetActive(false);
        GameObject.Find("Canvas").transform.Find("MiniMap_OutLine").gameObject.SetActive(false);
        float z = 8.0f;
        while (true)
        {
            z -= Time.deltaTime * 5;
            Camera.main.orthographicSize = z;

            if (z <= 3.0f)
            {
                Camera.main.orthographicSize = 3.0f;
                break;
            }

            yield return new WaitForSeconds(Time.deltaTime / 10);
        }
        CostumPanel.SetActive(true);
        panel.SetActive(true);
    }
    private void Exit()
    {
        StartCoroutine(ExitCoroutine());
    }
    IEnumerator ExitCoroutine()
    {
        panel.SetActive(false);
        CostumPanel.SetActive(false);
        float z = 3.0f;
        while (true)
        {
            z += Time.deltaTime * 5;
            Camera.main.orthographicSize = z;
            if (z >= 8.0f)
            {
                Camera.main.orthographicSize = 8.0f;
                break;
            }

            yield return new WaitForSeconds(Time.deltaTime / 10);
        }
        PDG.Player.instance.isCameraOff = false;
        GameObject.Find("Canvas").transform.Find("HpShieldPanel").gameObject.SetActive(true);
        GameObject.Find("Canvas").transform.Find("MiniMap_OutLine").gameObject.SetActive(true);
        StartCoroutine(PDG.Dungeon.instance.UiOn());
        isOpen = false;
    }
    private void Select()
    {
        SoundManager.instance.StartAudio(new string[1] { "helmatSelect" }, VOLUME_TYPE.EFFECT);
        if (nowNum == 0)
        {
            PDG.Dungeon.headType = string.Empty;
            PDG.Dungeon.isSelectHead = false;
            PDG.Player.instance.helmetObj.SetActive(false);
        }
        else
        {
            PDG.Dungeon.headType = sprites[nowNum].name;
            PDG.Dungeon.isSelectHead = true;
            PDG.Player.instance.helmetObj.SetActive(true);
        }
        Exit();
    }
}
