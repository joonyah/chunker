﻿using PDG;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemLoopController : MonoBehaviour
{
    [SerializeField] private GameObject matParticle;
    [SerializeField] private GameObject skullParticle;
    [SerializeField] private bool isTest = false;
    private void Update()
    {
        if(isTest)
        {
            isTest = false;
            GoItem(null, false);
        }
    }
    public void GoItem(Sprite _sprite, bool _isMaterial)
    {
        if (_sprite) GetComponent<SpriteRenderer>().sprite = _sprite;
        if (_isMaterial)
        {
            matParticle.SetActive(true);
            skullParticle.SetActive(false);
        }
        else
        {
            matParticle.SetActive(false);
            skullParticle.SetActive(true);
        }
        StartCoroutine(Looping());
    }

    IEnumerator Looping()
    {
        Vector3 startPos = transform.position;
        Vector3 endPos = UnityEngine.Camera.main.ScreenToWorldPoint(GameObject.Find("Canvas").transform.Find("UI").Find("Coin").Find("GlowingSkull").position);
        float t = 0.0f;
        while (true)
        {
            endPos = UnityEngine.Camera.main.ScreenToWorldPoint(GameObject.Find("Canvas").transform.Find("UI").Find("Coin").Find("GlowingSkull").position);
            Vector3 lerp = Vector3.Lerp(startPos, endPos, t);
            transform.position = lerp;
            t += Time.deltaTime * 2;
            if (t >= 1.0f) break;
            yield return new WaitForSeconds(Time.deltaTime);
        }
        GameObject.Find("Canvas").transform.Find("UI").Find("Coin").Find("GlowingSkull").GetComponent<Animator>().SetTrigger("GetItem");
        SoundManager.instance.StartAudio(new string[1] { "Object/coin" }, VOLUME_TYPE.EFFECT);
        gameObject.SetActive(false);
    }
    #region -- Tool --
    public static float GetAngle(Vector3 vStart, Vector3 vEnd)
    {
        Vector3 v = vEnd - vStart;

        return Mathf.Atan2(v.y, v.x) * Mathf.Rad2Deg;
    }
    public static Vector3 GetPosition(Vector3 vStart, float angle)
    {
        float x = Mathf.Cos(Mathf.Deg2Rad * angle) + vStart.x;
        float y = Mathf.Sin(Mathf.Deg2Rad * angle) + vStart.y;

        return new Vector3(x, y, 0);
    }
    private int GetPercentege(float _per, string[] list)
    {
        int[] per = new int[Mathf.FloorToInt(100 * list.Length)];
        for (int i = 0; i < per.Length; i++) per[i] = -1;

        for (int i = 0; i < list.Length; i++)
        {
            for (int j = 0; j < Mathf.FloorToInt(_per * 100); j++)
            {
                per[(i * 100) + j] = i;
            }
        }
        if (per.Length == 0) return -1;
        return per[Random.Range(0, per.Length)];
    }
    #endregion
}
