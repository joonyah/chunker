﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossWaitingRoom2 : MonoBehaviour
{
    [SerializeField] private Sprite[] MazeMaps;
    [SerializeField] private GameObject[] MazeMapsCollision;
    // Start is called before the first frame update
    void Start()
    {
        int t = Random.Range(0, MazeMaps.Length);
        GetComponent<SpriteRenderer>().sprite = MazeMaps[t];
        for (int i = 0; i < MazeMapsCollision.Length; i++)
        {
            if (i == t)
                MazeMapsCollision[i].SetActive(true);
            else
                MazeMapsCollision[i].SetActive(false);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
