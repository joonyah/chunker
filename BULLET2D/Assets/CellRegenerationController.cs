﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CellRegenerationController : MonoBehaviour
{
    [SerializeField] private Sprite fSprite;
    [SerializeField] private GameObject controllerObj;
    [SerializeField] private SpriteRenderer controllerSR;
    [SerializeField] private bool isSelect = false;
    [SerializeField] private int Level = 0;
    [SerializeField] private bool isWaiting = false;
    // Start is called before the first frame update
    void Start()
    {
        Level = SaveAndLoadManager.instance.LoadItem("CellRegenerationLevel");
        if (Level == -1) Level = 0;
        StartCoroutine(LevelCheck());
    }

    IEnumerator LevelCheck()
    {
        while(true)
        {
            Level = SaveAndLoadManager.instance.LoadItem("CellRegenerationLevel");
            if (Level == -1) Level = 0;
            yield return new WaitForSeconds(1.0f);
        }
    }

    // Update is called once per frame
    void Update()
    {

        RaycastHit2D hit = Physics2D.BoxCast(transform.position, new Vector2(4.5f, 5.5f), 0, Vector2.zero, 0, 1 << 9);

        if (hit && !isSelect && !MakeController.instance.isRepair && !TextOpenController.instance.isOpen && !PDG.Dungeon.instance.newObjectSelected)
        {
            isSelect = true;
            PDG.Dungeon.instance.newObjectSelected = true;
            PDG.Dungeon.instance.newObjectSelectedObj = gameObject;
            controllerObj.SetActive(true);
            controllerObj.GetComponent<SpriteRenderer>().sprite = PDG.Dungeon.instance.isJoystrick ? PDG.Dungeon.GetJoyStrickKeySprite(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_ACTIVE]) : PDG.Dungeon.GetJoyStrickKeySprite(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ACTIVE]);
            if (Level == 0) TextOpenController.instance.ShowText(LocalizeManager.GetLocalize("ajit_repair_comment"), Color.white, 24, controllerObj, 2.0f);
        }
        else if (!hit)
        {
            isSelect = false;
            if (PDG.Dungeon.instance.newObjectSelectedObj == gameObject)
            {
                PDG.Dungeon.instance.newObjectSelectedObj = null;
                PDG.Dungeon.instance.newObjectSelected = false;
            }
            controllerObj.SetActive(false);
            if (TextOpenController.instance.targetObj == controllerObj) TextOpenController.instance.TextClose(Color.white);
        }

        if (isSelect)
        {
            if ((PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ACTIVE], true) || PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_ACTIVE], true)) && !MakeController.instance.isRepair && PDG.Dungeon.instance.closeCoolTime > 0.2f) 
            {
                PDG.Dungeon.instance.closeCoolTime = 0.0f;
                SoundManager.instance.StartAudio(new string[1] { "Object/active_F" }, VOLUME_TYPE.EFFECT);
                isSelect = false;
                if (PDG.Dungeon.instance.newObjectSelectedObj == gameObject)
                {
                    PDG.Dungeon.instance.newObjectSelectedObj = null;
                    PDG.Dungeon.instance.newObjectSelected = false;
                }
                controllerObj.SetActive(false);
                if (!isWaiting)
                {
                    MakeController.instance.ShowMake(string.Empty, "cell_regeneration", gameObject, Level);
                }
                else
                {
                    TextOpenController.instance.ShowText(LocalizeManager.GetLocalize("object_waiting"), Color.white, 24, controllerObj, 2.0f, true);
                };
            }
        }
    }
}
