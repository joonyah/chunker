using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using System.Text;

using GDataDB;
using GDataDB.Linq;

using UnityQuickSheet;

///
/// !!! Machine generated code !!!
///
[CustomEditor(typeof(Achievements))]
public class AchievementsEditor : BaseGoogleEditor<Achievements>
{	    
    public override bool Load()
    {        
        Achievements targetData = target as Achievements;
        
        var client = new DatabaseClient("", "");
        string error = string.Empty;
        var db = client.GetDatabase(targetData.SheetName, ref error);	
        var table = db.GetTable<AchievementsData>(targetData.WorksheetName) ?? db.CreateTable<AchievementsData>(targetData.WorksheetName);
        
        List<AchievementsData> myDataList = new List<AchievementsData>();
        
        var all = table.FindAll();
        foreach(var elem in all)
        {
            AchievementsData data = new AchievementsData();
            
            data = Cloner.DeepCopy<AchievementsData>(elem.Element);
            myDataList.Add(data);
        }
                
        targetData.dataArray = myDataList.ToArray();
        
        EditorUtility.SetDirty(targetData);
        AssetDatabase.SaveAssets();
        
        return true;
    }
}
