using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using System.Text;

using GDataDB;
using GDataDB.Linq;

using UnityQuickSheet;

///
/// !!! Machine generated code !!!
///
[CustomEditor(typeof(Reinforce))]
public class ReinforceEditor : BaseGoogleEditor<Reinforce>
{	    
    public override bool Load()
    {        
        Reinforce targetData = target as Reinforce;
        
        var client = new DatabaseClient("", "");
        string error = string.Empty;
        var db = client.GetDatabase(targetData.SheetName, ref error);	
        var table = db.GetTable<ReinforceData>(targetData.WorksheetName) ?? db.CreateTable<ReinforceData>(targetData.WorksheetName);
        
        List<ReinforceData> myDataList = new List<ReinforceData>();
        
        var all = table.FindAll();
        foreach(var elem in all)
        {
            ReinforceData data = new ReinforceData();
            
            data = Cloner.DeepCopy<ReinforceData>(elem.Element);
            myDataList.Add(data);
        }
                
        targetData.dataArray = myDataList.ToArray();
        
        EditorUtility.SetDirty(targetData);
        AssetDatabase.SaveAssets();
        
        return true;
    }
}
