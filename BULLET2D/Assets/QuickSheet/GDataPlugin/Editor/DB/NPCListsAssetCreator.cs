using UnityEngine;
using UnityEditor;
using System.IO;
using UnityQuickSheet;

///
/// !!! Machine generated code !!!
/// 
public partial class GoogleDataAssetUtility
{
    [MenuItem("Assets/Create/Google/NPCLists")]
    public static void CreateNPCListsAssetFile()
    {
        NPCLists asset = CustomAssetUtility.CreateAsset<NPCLists>();
        asset.SheetName = "PJ_2nd_DBlist2";
        asset.WorksheetName = "NPCLists";
        EditorUtility.SetDirty(asset);        
    }
    
}