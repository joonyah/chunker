using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using System.Text;

using GDataDB;
using GDataDB.Linq;

using UnityQuickSheet;

///
/// !!! Machine generated code !!!
///
[CustomEditor(typeof(Mail))]
public class MailEditor : BaseGoogleEditor<Mail>
{	    
    public override bool Load()
    {        
        Mail targetData = target as Mail;
        
        var client = new DatabaseClient("", "");
        string error = string.Empty;
        var db = client.GetDatabase(targetData.SheetName, ref error);	
        var table = db.GetTable<MailData>(targetData.WorksheetName) ?? db.CreateTable<MailData>(targetData.WorksheetName);
        
        List<MailData> myDataList = new List<MailData>();
        
        var all = table.FindAll();
        foreach(var elem in all)
        {
            MailData data = new MailData();
            
            data = Cloner.DeepCopy<MailData>(elem.Element);
            myDataList.Add(data);
        }
                
        targetData.dataArray = myDataList.ToArray();
        
        EditorUtility.SetDirty(targetData);
        AssetDatabase.SaveAssets();
        
        return true;
    }
}
