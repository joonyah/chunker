using UnityEngine;
using UnityEditor;
using System.IO;
using UnityQuickSheet;

///
/// !!! Machine generated code !!!
/// 
public partial class GoogleDataAssetUtility
{
    [MenuItem("Assets/Create/Google/Localize")]
    public static void CreateLocalizeAssetFile()
    {
        Localize asset = CustomAssetUtility.CreateAsset<Localize>();
        asset.SheetName = "PJ_2nd_DBlist2";
        asset.WorksheetName = "Localize";
        EditorUtility.SetDirty(asset);        
    }
    
}