using UnityEngine;
using UnityEditor;
using System.IO;
using UnityQuickSheet;

///
/// !!! Machine generated code !!!
/// 
public partial class GoogleDataAssetUtility
{
    [MenuItem("Assets/Create/Google/Mail")]
    public static void CreateMailAssetFile()
    {
        Mail asset = CustomAssetUtility.CreateAsset<Mail>();
        asset.SheetName = "PJ_2nd_DBlist";
        asset.WorksheetName = "Mail";
        EditorUtility.SetDirty(asset);        
    }
    
}