using UnityEngine;
using UnityEditor;
using System.IO;
using UnityQuickSheet;

///
/// !!! Machine generated code !!!
/// 
public partial class GoogleDataAssetUtility
{
    [MenuItem("Assets/Create/Google/Make")]
    public static void CreateMakeAssetFile()
    {
        Make asset = CustomAssetUtility.CreateAsset<Make>();
        asset.SheetName = "PJ_2nd_DBlist";
        asset.WorksheetName = "Make";
        EditorUtility.SetDirty(asset);        
    }
    
}