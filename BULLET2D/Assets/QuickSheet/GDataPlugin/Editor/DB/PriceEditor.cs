using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using System.Text;

using GDataDB;
using GDataDB.Linq;

using UnityQuickSheet;

///
/// !!! Machine generated code !!!
///
[CustomEditor(typeof(Price))]
public class PriceEditor : BaseGoogleEditor<Price>
{	    
    public override bool Load()
    {        
        Price targetData = target as Price;
        
        var client = new DatabaseClient("", "");
        string error = string.Empty;
        var db = client.GetDatabase(targetData.SheetName, ref error);	
        var table = db.GetTable<PriceData>(targetData.WorksheetName) ?? db.CreateTable<PriceData>(targetData.WorksheetName);
        
        List<PriceData> myDataList = new List<PriceData>();
        
        var all = table.FindAll();
        foreach(var elem in all)
        {
            PriceData data = new PriceData();
            
            data = Cloner.DeepCopy<PriceData>(elem.Element);
            myDataList.Add(data);
        }
                
        targetData.dataArray = myDataList.ToArray();
        
        EditorUtility.SetDirty(targetData);
        AssetDatabase.SaveAssets();
        
        return true;
    }
}
