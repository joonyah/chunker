using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using System.Text;

using GDataDB;
using GDataDB.Linq;

using UnityQuickSheet;

///
/// !!! Machine generated code !!!
///
[CustomEditor(typeof(Stage))]
public class StageEditor : BaseGoogleEditor<Stage>
{	    
    public override bool Load()
    {        
        Stage targetData = target as Stage;
        
        var client = new DatabaseClient("", "");
        string error = string.Empty;
        var db = client.GetDatabase(targetData.SheetName, ref error);	
        var table = db.GetTable<StageData>(targetData.WorksheetName) ?? db.CreateTable<StageData>(targetData.WorksheetName);
        
        List<StageData> myDataList = new List<StageData>();
        
        var all = table.FindAll();
        foreach(var elem in all)
        {
            StageData data = new StageData();
            
            data = Cloner.DeepCopy<StageData>(elem.Element);
            myDataList.Add(data);
        }
                
        targetData.dataArray = myDataList.ToArray();
        
        EditorUtility.SetDirty(targetData);
        AssetDatabase.SaveAssets();
        
        return true;
    }
}
