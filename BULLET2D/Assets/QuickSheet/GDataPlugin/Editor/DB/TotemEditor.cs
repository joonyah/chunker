using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using System.Text;

using GDataDB;
using GDataDB.Linq;

using UnityQuickSheet;

///
/// !!! Machine generated code !!!
///
[CustomEditor(typeof(Totem))]
public class TotemEditor : BaseGoogleEditor<Totem>
{	    
    public override bool Load()
    {        
        Totem targetData = target as Totem;
        
        var client = new DatabaseClient("", "");
        string error = string.Empty;
        var db = client.GetDatabase(targetData.SheetName, ref error);	
        var table = db.GetTable<TotemData>(targetData.WorksheetName) ?? db.CreateTable<TotemData>(targetData.WorksheetName);
        
        List<TotemData> myDataList = new List<TotemData>();
        
        var all = table.FindAll();
        foreach(var elem in all)
        {
            TotemData data = new TotemData();
            
            data = Cloner.DeepCopy<TotemData>(elem.Element);
            myDataList.Add(data);
        }
                
        targetData.dataArray = myDataList.ToArray();
        
        EditorUtility.SetDirty(targetData);
        AssetDatabase.SaveAssets();
        
        return true;
    }
}
