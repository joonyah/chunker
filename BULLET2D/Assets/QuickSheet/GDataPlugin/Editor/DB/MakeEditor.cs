using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using System.Text;

using GDataDB;
using GDataDB.Linq;

using UnityQuickSheet;

///
/// !!! Machine generated code !!!
///
[CustomEditor(typeof(Make))]
public class MakeEditor : BaseGoogleEditor<Make>
{	    
    public override bool Load()
    {        
        Make targetData = target as Make;
        
        var client = new DatabaseClient("", "");
        string error = string.Empty;
        var db = client.GetDatabase(targetData.SheetName, ref error);	
        var table = db.GetTable<MakeData>(targetData.WorksheetName) ?? db.CreateTable<MakeData>(targetData.WorksheetName);
        
        List<MakeData> myDataList = new List<MakeData>();
        
        var all = table.FindAll();
        foreach(var elem in all)
        {
            MakeData data = new MakeData();
            
            data = Cloner.DeepCopy<MakeData>(elem.Element);
            myDataList.Add(data);
        }
                
        targetData.dataArray = myDataList.ToArray();
        
        EditorUtility.SetDirty(targetData);
        AssetDatabase.SaveAssets();
        
        return true;
    }
}
