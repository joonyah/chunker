using UnityEngine;
using UnityEditor;
using System.IO;
using UnityQuickSheet;

///
/// !!! Machine generated code !!!
/// 
public partial class GoogleDataAssetUtility
{
    [MenuItem("Assets/Create/Google/Manufacture")]
    public static void CreateManufactureAssetFile()
    {
        Manufacture asset = CustomAssetUtility.CreateAsset<Manufacture>();
        asset.SheetName = "PJ_2nd_DBlist";
        asset.WorksheetName = "Manufacture";
        EditorUtility.SetDirty(asset);        
    }
    
}