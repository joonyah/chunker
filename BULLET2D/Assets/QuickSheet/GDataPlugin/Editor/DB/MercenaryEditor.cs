using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using System.Text;

using GDataDB;
using GDataDB.Linq;

using UnityQuickSheet;

///
/// !!! Machine generated code !!!
///
[CustomEditor(typeof(Mercenary))]
public class MercenaryEditor : BaseGoogleEditor<Mercenary>
{	    
    public override bool Load()
    {        
        Mercenary targetData = target as Mercenary;
        
        var client = new DatabaseClient("", "");
        string error = string.Empty;
        var db = client.GetDatabase(targetData.SheetName, ref error);	
        var table = db.GetTable<MercenaryData>(targetData.WorksheetName) ?? db.CreateTable<MercenaryData>(targetData.WorksheetName);
        
        List<MercenaryData> myDataList = new List<MercenaryData>();
        
        var all = table.FindAll();
        foreach(var elem in all)
        {
            MercenaryData data = new MercenaryData();
            
            data = Cloner.DeepCopy<MercenaryData>(elem.Element);
            myDataList.Add(data);
        }
                
        targetData.dataArray = myDataList.ToArray();
        
        EditorUtility.SetDirty(targetData);
        AssetDatabase.SaveAssets();
        
        return true;
    }
}
