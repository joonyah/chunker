using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using System.Text;

using GDataDB;
using GDataDB.Linq;

using UnityQuickSheet;

///
/// !!! Machine generated code !!!
///
[CustomEditor(typeof(Localize))]
public class LocalizeEditor : BaseGoogleEditor<Localize>
{	    
    public override bool Load()
    {        
        Localize targetData = target as Localize;
        
        var client = new DatabaseClient("", "");
        string error = string.Empty;
        var db = client.GetDatabase(targetData.SheetName, ref error);	
        var table = db.GetTable<LocalizeData>(targetData.WorksheetName) ?? db.CreateTable<LocalizeData>(targetData.WorksheetName);
        
        List<LocalizeData> myDataList = new List<LocalizeData>();
        
        var all = table.FindAll();
        foreach(var elem in all)
        {
            LocalizeData data = new LocalizeData();
            
            data = Cloner.DeepCopy<LocalizeData>(elem.Element);
            myDataList.Add(data);
        }
                
        targetData.dataArray = myDataList.ToArray();
        
        EditorUtility.SetDirty(targetData);
        AssetDatabase.SaveAssets();
        
        return true;
    }
}
