using UnityEngine;
using UnityEditor;
using System.IO;
using UnityQuickSheet;

///
/// !!! Machine generated code !!!
/// 
public partial class GoogleDataAssetUtility
{
    [MenuItem("Assets/Create/Google/Stats")]
    public static void CreateStatsAssetFile()
    {
        Stats asset = CustomAssetUtility.CreateAsset<Stats>();
        asset.SheetName = "PJ_2nd_DBlist";
        asset.WorksheetName = "Stats";
        EditorUtility.SetDirty(asset);        
    }
    
}