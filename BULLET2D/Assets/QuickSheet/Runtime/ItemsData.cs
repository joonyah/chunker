using UnityEngine;
using System.Collections;

///
/// !!! Machine generated code !!!
/// !!! DO NOT CHANGE Tabs to Spaces !!!
///
[System.Serializable]
public class ItemsData
{
  [SerializeField]
  string id;
  public string ID { get {return id; } set { id = value;} }
  
  [SerializeField]
  string name;
  public string Name { get {return name; } set { name = value;} }
  
  [SerializeField]
  string type1;
  public string Type1 { get {return type1; } set { type1 = value;} }
  
  [SerializeField]
  string type2;
  public string Type2 { get {return type2; } set { type2 = value;} }
  
  [SerializeField]
  string type3;
  public string Type3 { get {return type3; } set { type3 = value;} }
  
  [SerializeField]
  string type4;
  public string Type4 { get {return type4; } set { type4 = value;} }
  
  [SerializeField]
  string cont;
  public string Cont { get {return cont; } set { cont = value;} }
  
  [SerializeField]
  int maxdropcount;
  public int Maxdropcount { get {return maxdropcount; } set { maxdropcount = value;} }
  
  [SerializeField]
  string grade;
  public string Grade { get {return grade; } set { grade = value;} }
  
  [SerializeField]
  bool needunlock;
  public bool Needunlock { get {return needunlock; } set { needunlock = value;} }
  
  [SerializeField]
  string attacktype;
  public string Attacktype { get {return attacktype; } set { attacktype = value;} }
  
  [SerializeField]
  bool isfixed;
  public bool Isfixed { get {return isfixed; } set { isfixed = value;} }
  
  [SerializeField]
  string idskill;
  public string Idskill { get {return idskill; } set { idskill = value;} }
  
  [SerializeField]
  int[] damage = new int[0];
  public int[] Damage { get {return damage; } set { damage = value;} }
  
  [SerializeField]
  int[] damagenear = new int[0];
  public int[] Damagenear { get {return damagenear; } set { damagenear = value;} }
  
  [SerializeField]
  float critical;
  public float Critical { get {return critical; } set { critical = value;} }
  
  [SerializeField]
  float criticaldamage;
  public float Criticaldamage { get {return criticaldamage; } set { criticaldamage = value;} }
  
  [SerializeField]
  float bulletsize;
  public float Bulletsize { get {return bulletsize; } set { bulletsize = value;} }
  
  [SerializeField]
  float force;
  public float Force { get {return force; } set { force = value;} }
  
  [SerializeField]
  float shootdelay;
  public float Shootdelay { get {return shootdelay; } set { shootdelay = value;} }
  
  [SerializeField]
  float bulletspeed;
  public float Bulletspeed { get {return bulletspeed; } set { bulletspeed = value;} }
  
  [SerializeField]
  float range;
  public float Range { get {return range; } set { range = value;} }
  
  [SerializeField]
  float aim;
  public float Aim { get {return aim; } set { aim = value;} }
  
  [SerializeField]
  bool auto;
  public bool Auto { get {return auto; } set { auto = value;} }
  
  [SerializeField]
  bool autoaim;
  public bool Autoaim { get {return autoaim; } set { autoaim = value;} }
  
  [SerializeField]
  int shootcount;
  public int Shootcount { get {return shootcount; } set { shootcount = value;} }
  
  [SerializeField]
  float bulletdelay;
  public float Bulletdelay { get {return bulletdelay; } set { bulletdelay = value;} }
  
  [SerializeField]
  int multishoot;
  public int Multishoot { get {return multishoot; } set { multishoot = value;} }
  
  [SerializeField]
  string elemental;
  public string Elemental { get {return elemental; } set { elemental = value;} }
  
  [SerializeField]
  float elementaltime;
  public float Elementaltime { get {return elementaltime; } set { elementaltime = value;} }
  
  [SerializeField]
  int[] armspeed = new int[0];
  public int[] Armspeed { get {return armspeed; } set { armspeed = value;} }
  
  [SerializeField]
  bool armwave;
  public bool Armwave { get {return armwave; } set { armwave = value;} }
  
  [SerializeField]
  float[] buffamount = new float[0];
  public float[] Buffamount { get {return buffamount; } set { buffamount = value;} }
  
  [SerializeField]
  bool cooltimeopen;
  public bool Cooltimeopen { get {return cooltimeopen; } set { cooltimeopen = value;} }
  
  [SerializeField]
  float ticktime;
  public float Ticktime { get {return ticktime; } set { ticktime = value;} }
  
  [SerializeField]
  int maxattackcount;
  public int Maxattackcount { get {return maxattackcount; } set { maxattackcount = value;} }
  
  [SerializeField]
  string type;
  public string Type { get {return type; } set { type = value;} }
  
}