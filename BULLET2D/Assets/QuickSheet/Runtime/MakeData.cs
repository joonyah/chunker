using UnityEngine;
using System.Collections;

///
/// !!! Machine generated code !!!
/// !!! DO NOT CHANGE Tabs to Spaces !!!
///
[System.Serializable]
public class MakeData
{
  [SerializeField]
  string id;
  public string ID { get {return id; } set { id = value;} }
  
  [SerializeField]
  string resulttype;
  public string Resulttype { get {return resulttype; } set { resulttype = value;} }
  
  [SerializeField]
  string maker;
  public string Maker { get {return maker; } set { maker = value;} }
  
  [SerializeField]
  string[] materialstype = new string[0];
  public string[] Materialstype { get {return materialstype; } set { materialstype = value;} }
  
  [SerializeField]
  string[] materials = new string[0];
  public string[] Materials { get {return materials; } set { materials = value;} }
  
  [SerializeField]
  int[] counts = new int[0];
  public int[] Counts { get {return counts; } set { counts = value;} }
  
  [SerializeField]
  int[] counts2 = new int[0];
  public int[] Counts2 { get {return counts2; } set { counts2 = value;} }
  
  [SerializeField]
  string unlock;
  public string Unlock { get {return unlock; } set { unlock = value;} }
  
  [SerializeField]
  float cooltime;
  public float Cooltime { get {return cooltime; } set { cooltime = value;} }
  
  [SerializeField]
  int makecount;
  public int Makecount { get {return makecount; } set { makecount = value;} }
  
}