using UnityEngine;
using System.Collections;

///
/// !!! Machine generated code !!!
/// !!! DO NOT CHANGE Tabs to Spaces !!!
///
[System.Serializable]
public class AchievementsData
{
  [SerializeField]
  string id;
  public string ID { get {return id; } set { id = value;} }
  
  [SerializeField]
  string group;
  public string GROUP { get {return group; } set { group = value;} }
  
  [SerializeField]
  bool reward;
  public bool REWARD { get {return reward; } set { reward = value;} }
  
  [SerializeField]
  int score;
  public int SCORE { get {return score; } set { score = value;} }
  
  [SerializeField]
  int max;
  public int MAX { get {return max; } set { max = value;} }
  
  [SerializeField]
  string type;
  public string TYPE { get {return type; } set { type = value;} }
  
}