using UnityEngine;
using System.Collections;

///
/// !!! Machine generated code !!!
/// !!! DO NOT CHANGE Tabs to Spaces !!!
///
[System.Serializable]
public class EnhanceData
{
  [SerializeField]
  string id;
  public string ID { get {return id; } set { id = value;} }
  
  [SerializeField]
  bool attack;
  public bool Attack { get {return attack; } set { attack = value;} }
  
  [SerializeField]
  bool critical;
  public bool Critical { get {return critical; } set { critical = value;} }
  
  [SerializeField]
  bool criticaldamage;
  public bool Criticaldamage { get {return criticaldamage; } set { criticaldamage = value;} }
  
  [SerializeField]
  bool range;
  public bool Range { get {return range; } set { range = value;} }
  
  [SerializeField]
  bool shootcount;
  public bool Shootcount { get {return shootcount; } set { shootcount = value;} }
  
  [SerializeField]
  bool multishoot;
  public bool Multishoot { get {return multishoot; } set { multishoot = value;} }
  
  [SerializeField]
  bool shootdelay;
  public bool Shootdelay { get {return shootdelay; } set { shootdelay = value;} }
  
  [SerializeField]
  bool aim;
  public bool Aim { get {return aim; } set { aim = value;} }
  
}