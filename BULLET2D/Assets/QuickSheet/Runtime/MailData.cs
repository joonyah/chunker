using UnityEngine;
using System.Collections;

///
/// !!! Machine generated code !!!
/// !!! DO NOT CHANGE Tabs to Spaces !!!
///
[System.Serializable]
public class MailData
{
  [SerializeField]
  string id;
  public string ID { get {return id; } set { id = value;} }
  
  [SerializeField]
  string from;
  public string From { get {return from; } set { from = value;} }
  
  [SerializeField]
  string text;
  public string Text { get {return text; } set { text = value;} }
  
  [SerializeField]
  int term;
  public int Term { get {return term; } set { term = value;} }
  
  [SerializeField]
  string[] gift_1 = new string[0];
  public string[] Gift_1 { get {return gift_1; } set { gift_1 = value;} }
  
  [SerializeField]
  string[] gift_2 = new string[0];
  public string[] Gift_2 { get {return gift_2; } set { gift_2 = value;} }
  
  [SerializeField]
  int loop;
  public int Loop { get {return loop; } set { loop = value;} }
  
}