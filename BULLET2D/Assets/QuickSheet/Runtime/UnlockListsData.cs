using UnityEngine;
using System.Collections;

///
/// !!! Machine generated code !!!
/// !!! DO NOT CHANGE Tabs to Spaces !!!
///
[System.Serializable]
public class UnlockListsData
{
  [SerializeField]
  int seq;
  public int SEQ { get {return seq; } set { seq = value;} }
  
  [SerializeField]
  string npcid;
  public string NPCID { get {return npcid; } set { npcid = value;} }
  
  [SerializeField]
  string itemid;
  public string Itemid { get {return itemid; } set { itemid = value;} }
  
  [SerializeField]
  string cont;
  public string Cont { get {return cont; } set { cont = value;} }
  
  [SerializeField]
  int unlockcount;
  public int Unlockcount { get {return unlockcount; } set { unlockcount = value;} }
  
  [SerializeField]
  string type;
  public string Type { get {return type; } set { type = value;} }
  
}