using UnityEngine;
using System.Collections;

///
/// !!! Machine generated code !!!
/// !!! DO NOT CHANGE Tabs to Spaces !!!
///
[System.Serializable]
public class MerchantData
{
  [SerializeField]
  int seq;
  public int SEQ { get {return seq; } set { seq = value;} }
  
  [SerializeField]
  string id;
  public string ID { get {return id; } set { id = value;} }
  
  [SerializeField]
  int itemcount;
  public int Itemcount { get {return itemcount; } set { itemcount = value;} }
  
  [SerializeField]
  string itemcategories1;
  public string Itemcategories1 { get {return itemcategories1; } set { itemcategories1 = value;} }
  
  [SerializeField]
  string itemcategories2;
  public string Itemcategories2 { get {return itemcategories2; } set { itemcategories2 = value;} }
  
  [SerializeField]
  string itemcategories3;
  public string Itemcategories3 { get {return itemcategories3; } set { itemcategories3 = value;} }
  
  [SerializeField]
  string[] itemgrade = new string[0];
  public string[] Itemgrade { get {return itemgrade; } set { itemgrade = value;} }
  
  [SerializeField]
  float[] gradeprobability = new float[0];
  public float[] Gradeprobability { get {return gradeprobability; } set { gradeprobability = value;} }
  
}