using UnityEngine;
using System.Collections;

///
/// !!! Machine generated code !!!
/// !!! DO NOT CHANGE Tabs to Spaces !!!
///
[System.Serializable]
public class NPCListsData
{
  [SerializeField]
  int seq;
  public int SEQ { get {return seq; } set { seq = value;} }
  
  [SerializeField]
  string id;
  public string ID { get {return id; } set { id = value;} }
  
  [SerializeField]
  string name;
  public string Name { get {return name; } set { name = value;} }
  
  [SerializeField]
  string namechns;
  public string Namechns { get {return namechns; } set { namechns = value;} }
  
  [SerializeField]
  string namechnt;
  public string Namechnt { get {return namechnt; } set { namechnt = value;} }
  
  [SerializeField]
  string nameja;
  public string Nameja { get {return nameja; } set { nameja = value;} }
  
  [SerializeField]
  string nameeng;
  public string Nameeng { get {return nameeng; } set { nameeng = value;} }
  
  [SerializeField]
  string nameru;
  public string Nameru { get {return nameru; } set { nameru = value;} }
  
  [SerializeField]
  string dialogue;
  public string Dialogue { get {return dialogue; } set { dialogue = value;} }
  
  [SerializeField]
  string dialoguechns;
  public string Dialoguechns { get {return dialoguechns; } set { dialoguechns = value;} }
  
  [SerializeField]
  string dialoguechnt;
  public string Dialoguechnt { get {return dialoguechnt; } set { dialoguechnt = value;} }
  
  [SerializeField]
  string dialogueja;
  public string Dialogueja { get {return dialogueja; } set { dialogueja = value;} }
  
  [SerializeField]
  string dialogueeng;
  public string Dialogueeng { get {return dialogueeng; } set { dialogueeng = value;} }
  
  [SerializeField]
  string dialogueru;
  public string Dialogueru { get {return dialogueru; } set { dialogueru = value;} }
  
  [SerializeField]
  bool chioce;
  public bool Chioce { get {return chioce; } set { chioce = value;} }
  
  [SerializeField]
  string dialogue2;
  public string Dialogue2 { get {return dialogue2; } set { dialogue2 = value;} }
  
  [SerializeField]
  string dialogue2chns;
  public string Dialogue2chns { get {return dialogue2chns; } set { dialogue2chns = value;} }
  
  [SerializeField]
  string dialogue2chnt;
  public string Dialogue2chnt { get {return dialogue2chnt; } set { dialogue2chnt = value;} }
  
  [SerializeField]
  string dialogue2ja;
  public string Dialogue2ja { get {return dialogue2ja; } set { dialogue2ja = value;} }
  
  [SerializeField]
  string dialogue2eng;
  public string Dialogue2eng { get {return dialogue2eng; } set { dialogue2eng = value;} }
  
  [SerializeField]
  string dialogue2ru;
  public string Dialogue2ru { get {return dialogue2ru; } set { dialogue2ru = value;} }
  
  [SerializeField]
  string changenpc;
  public string Changenpc { get {return changenpc; } set { changenpc = value;} }
  
  [SerializeField]
  string changenpcchns;
  public string Changenpcchns { get {return changenpcchns; } set { changenpcchns = value;} }
  
  [SerializeField]
  string changenpcchnt;
  public string Changenpcchnt { get {return changenpcchnt; } set { changenpcchnt = value;} }
  
  [SerializeField]
  string changenpcja;
  public string Changenpcja { get {return changenpcja; } set { changenpcja = value;} }
  
  [SerializeField]
  string changenpceng;
  public string Changenpceng { get {return changenpceng; } set { changenpceng = value;} }
  
  [SerializeField]
  string changenpcru;
  public string Changenpcru { get {return changenpcru; } set { changenpcru = value;} }
  
  [SerializeField]
  string result;
  public string Result { get {return result; } set { result = value;} }
  
  [SerializeField]
  string item;
  public string Item { get {return item; } set { item = value;} }
  
}