using UnityEngine;
using System.Collections;

///
/// !!! Machine generated code !!!
/// !!! DO NOT CHANGE Tabs to Spaces !!!
///
[System.Serializable]
public class MonstersData
{
  [SerializeField]
  string seq;
  public string SEQ { get {return seq; } set { seq = value;} }
  
  [SerializeField]
  string id;
  public string ID { get {return id; } set { id = value;} }
  
  [SerializeField]
  string name;
  public string Name { get {return name; } set { name = value;} }
  
  [SerializeField]
  string[] targetstage = new string[0];
  public string[] Targetstage { get {return targetstage; } set { targetstage = value;} }
  
  [SerializeField]
  string type;
  public string Type { get {return type; } set { type = value;} }
  
  [SerializeField]
  string type2;
  public string Type2 { get {return type2; } set { type2 = value;} }
  
  [SerializeField]
  string animtype;
  public string Animtype { get {return animtype; } set { animtype = value;} }
  
  [SerializeField]
  float movespeed;
  public float Movespeed { get {return movespeed; } set { movespeed = value;} }
  
  [SerializeField]
  string elementalresist;
  public string Elementalresist { get {return elementalresist; } set { elementalresist = value;} }
  
  [SerializeField]
  float bulletsize;
  public float Bulletsize { get {return bulletsize; } set { bulletsize = value;} }
  
  [SerializeField]
  float force;
  public float Force { get {return force; } set { force = value;} }
  
  [SerializeField]
  float shootdelay;
  public float Shootdelay { get {return shootdelay; } set { shootdelay = value;} }
  
  [SerializeField]
  float bulletspeed;
  public float Bulletspeed { get {return bulletspeed; } set { bulletspeed = value;} }
  
  [SerializeField]
  float bulletrange;
  public float Bulletrange { get {return bulletrange; } set { bulletrange = value;} }
  
  [SerializeField]
  float range;
  public float Range { get {return range; } set { range = value;} }
  
  [SerializeField]
  float aim;
  public float Aim { get {return aim; } set { aim = value;} }
  
  [SerializeField]
  bool auto;
  public bool Auto { get {return auto; } set { auto = value;} }
  
  [SerializeField]
  bool autoaim;
  public bool Autoaim { get {return autoaim; } set { autoaim = value;} }
  
  [SerializeField]
  int shootcount;
  public int Shootcount { get {return shootcount; } set { shootcount = value;} }
  
  [SerializeField]
  float bulletdelay;
  public float Bulletdelay { get {return bulletdelay; } set { bulletdelay = value;} }
  
  [SerializeField]
  int multishoot;
  public int Multishoot { get {return multishoot; } set { multishoot = value;} }
  
  [SerializeField]
  bool elemental;
  public bool Elemental { get {return elemental; } set { elemental = value;} }
  
  [SerializeField]
  float elementaltime;
  public float Elementaltime { get {return elementaltime; } set { elementaltime = value;} }
  
}