using UnityEngine;
using System.Collections;

///
/// !!! Machine generated code !!!
/// !!! DO NOT CHANGE Tabs to Spaces !!!
///
[System.Serializable]
public class LocalizeData
{
  [SerializeField]
  string id;
  public string ID { get {return id; } set { id = value;} }
  
  [SerializeField]
  string kor;
  public string KOR { get {return kor; } set { kor = value;} }
  
  [SerializeField]
  string chns;
  public string CHNS { get {return chns; } set { chns = value;} }
  
  [SerializeField]
  string chnt;
  public string CHNT { get {return chnt; } set { chnt = value;} }
  
  [SerializeField]
  string ja;
  public string JA { get {return ja; } set { ja = value;} }
  
  [SerializeField]
  string eng;
  public string ENG { get {return eng; } set { eng = value;} }
  
  [SerializeField]
  string ru;
  public string RU { get {return ru; } set { ru = value;} }
  
}