using UnityEngine;
using System.Collections;

///
/// !!! Machine generated code !!!
/// !!! DO NOT CHANGE Tabs to Spaces !!!
///
[System.Serializable]
public class ManufactureData
{
  [SerializeField]
  string id;
  public string ID { get {return id; } set { id = value;} }
  
  [SerializeField]
  int stepby;
  public int Stepby { get {return stepby; } set { stepby = value;} }
  
  [SerializeField]
  int amount;
  public int Amount { get {return amount; } set { amount = value;} }
  
  [SerializeField]
  float percentage;
  public float Percentage { get {return percentage; } set { percentage = value;} }
  
}