using UnityEngine;
using System.Collections;

///
/// !!! Machine generated code !!!
/// !!! DO NOT CHANGE Tabs to Spaces !!!
///
[System.Serializable]
public class MercenaryData
{
  [SerializeField]
  string id;
  public string ID { get {return id; } set { id = value;} }
  
  [SerializeField]
  string attacktype;
  public string Attacktype { get {return attacktype; } set { attacktype = value;} }
  
  [SerializeField]
  float attackrange;
  public float Attackrange { get {return attackrange; } set { attackrange = value;} }
  
  [SerializeField]
  float attackdelay;
  public float Attackdelay { get {return attackdelay; } set { attackdelay = value;} }
  
  [SerializeField]
  float speed;
  public float Speed { get {return speed; } set { speed = value;} }
  
  [SerializeField]
  float damage;
  public float Damage { get {return damage; } set { damage = value;} }
  
  [SerializeField]
  bool israngeattack;
  public bool Israngeattack { get {return israngeattack; } set { israngeattack = value;} }
  
}