using UnityEngine;
using System.Collections;

///
/// !!! Machine generated code !!!
/// !!! DO NOT CHANGE Tabs to Spaces !!!
///
[System.Serializable]
public class ReinforceData
{
  [SerializeField]
  string categoriesid;
  public string Categoriesid { get {return categoriesid; } set { categoriesid = value;} }
  
  [SerializeField]
  string categoriestext;
  public string Categoriestext { get {return categoriestext; } set { categoriestext = value;} }
  
  [SerializeField]
  string id;
  public string ID { get {return id; } set { id = value;} }
  
  [SerializeField]
  string maincont;
  public string Maincont { get {return maincont; } set { maincont = value;} }
  
  [SerializeField]
  string cont;
  public string Cont { get {return cont; } set { cont = value;} }
  
  [SerializeField]
  bool useflag;
  public bool Useflag { get {return useflag; } set { useflag = value;} }
  
}