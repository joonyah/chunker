﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CollectionManager : MonoBehaviour
{
    public static CollectionManager instance;
    [SerializeField] private CollectionController controller;
    [SerializeField] private GameObject openObj;
    [SerializeField] private GameObject itemPrefabs;
    [SerializeField] private GameObject leftList;
    [SerializeField] private GameObject rightList;
    private List<CollectionItem> itemList = new List<CollectionItem>();
    public bool isOpen = false;

    [SerializeField] private Text titleText;
    [SerializeField] private Text leftDataCollText;
    [SerializeField] private Text rightDataCollText;
    [SerializeField] private Text leftTitleText;
    [SerializeField] private Text rightTitleText;

    [Header("Click")]
    [SerializeField] private Text _idText;
    [SerializeField] private Text _contText;
    [SerializeField] private Image _cImage;

    [SerializeField] private Material grayMaterial;
    int nowSelectNum = 0;
    private void Awake()
    {
        if (instance == null) instance = this;
    }
    void Update()
    {
        if (isOpen)
        {
            float x = Input.GetAxisRaw("Horizontal_joy");
            float y = Input.GetAxisRaw("Vertical_joy");

            if ((Input.GetKeyDown(KeyCode.Escape) || PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_CANCLE], true)) && PDG.Dungeon.instance.closeCoolTime > 0.2f)
            {
                PDG.Dungeon.instance.closeCoolTime = 0.0f;
                Close();
            }
            if (PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_ACTIVE], true) && PDG.Dungeon.instance.closeCoolTime > 0.2f)
            {
                PDG.Dungeon.instance.closeCoolTime = 0.0f;
                itemList[nowSelectNum]._btn.onClick.Invoke();
            }
            if ((PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ARROW_RIGHT], false) || Input.GetKeyDown(KeyCode.RightArrow) || x > 0) && PDG.Dungeon.instance.closeCoolTime > 0.2f)
            {
                PDG.Dungeon.instance.closeCoolTime = 0.0f;
                nowSelectNum++;
                if (nowSelectNum >= itemList.Count) nowSelectNum = 0;
                itemList[nowSelectNum]._btn.onClick.Invoke();
            }
            if ((PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ARROW_DOWN], false) || Input.GetKeyDown(KeyCode.DownArrow) || y < 0) && PDG.Dungeon.instance.closeCoolTime > 0.2f)
            {
                PDG.Dungeon.instance.closeCoolTime = 0.0f;
                nowSelectNum += 6;
                if (nowSelectNum >= itemList.Count) nowSelectNum = 0;
                itemList[nowSelectNum]._btn.onClick.Invoke();
            }
            if ((PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ARROW_UP], false) || Input.GetKeyDown(KeyCode.UpArrow) || y > 0) && PDG.Dungeon.instance.closeCoolTime > 0.2f)
            {
                PDG.Dungeon.instance.closeCoolTime = 0.0f;
                nowSelectNum -= 6;
                if (nowSelectNum < 0) nowSelectNum = itemList.Count - 1;
                itemList[nowSelectNum]._btn.onClick.Invoke();
            }
            if ((PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ARROW_LEFT], false) || Input.GetKeyDown(KeyCode.LeftArrow) || x < 0) && PDG.Dungeon.instance.closeCoolTime > 0.2f)
            {
                PDG.Dungeon.instance.closeCoolTime = 0.0f;
                nowSelectNum--;
                if (nowSelectNum < 0) nowSelectNum = itemList.Count - 1;
                itemList[nowSelectNum]._btn.onClick.Invoke();
            }
        }
    }
    public void ShowCollection(CollectionController _controller)
    {
        nowSelectNum = 0;
        for (int i = 0; i < itemList.Count; i++)
        {
            Destroy(itemList[i].gameObject);
        }
        itemList.Clear();
        GetComponent<Image>().enabled = true;
        controller = _controller;
        bool isWeapon = _controller.isWeapon;
        CollectionData[] datas = SaveAndLoadManager.instance.LoadCollection();
        int leftCount = 0;
        int leftGetCount = 0;
        int rightCount = 0;
        int rightGetCount = 0;
        if (isWeapon)
        {
            titleText.text = LocalizeManager.GetLocalize("collection_weapon_name");
            leftTitleText.text = LocalizeManager.GetLocalize("collection_left_weapon");
            rightTitleText.text = LocalizeManager.GetLocalize("collection_right_weapon");
            ItemsData[] iDatas = Array.FindAll(SheetManager.Instance.ItemsDB.dataArray, item => item.Type1.Equals("Weapon") && !item.Grade.Equals("G"));
            for (int i = 0; i < iDatas.Length; i++)
            {
                bool isGet = false;
                if (datas != null)
                {
                    for (int j = 0; j < datas.Length; j++)
                    {
                        if (!datas[j]._isWeapon) continue;
                        if (datas[j]._id.Equals(iDatas[i].ID))
                        {
                            isGet = true;
                            break;
                        }
                    }
                }
                bool isBiology = false;
                if (iDatas[i].Type.Equals("Biology")) isBiology = true;
                GameObject obj = Instantiate(itemPrefabs);
                if (isBiology)
                {
                    obj.transform.SetParent(leftList.transform);
                    leftCount++;
                    if (isGet) leftGetCount++;
                }
                else
                {
                    obj.transform.SetParent(rightList.transform);
                    rightCount++;
                    if (isGet) rightGetCount++;
                }
                obj.transform.localScale = Vector3.one;
                CollectionItem item = obj.GetComponent<CollectionItem>();
                item.InitSetting(iDatas[i].ID, true, isGet, isBiology);
                itemList.Add(item);
            }
        }
        else
        {
            titleText.text = LocalizeManager.GetLocalize("collection_monster_name");
            leftTitleText.text = LocalizeManager.GetLocalize("collection_left_monster");
            rightTitleText.text = LocalizeManager.GetLocalize("collection_right_monster");
            MonstersData[] mDatas = Array.FindAll(SheetManager.Instance.MonsterDB.dataArray, item => !item.Targetstage[0].Equals("Dungeon_00") && !item.ID.Contains("_t"));
            for (int i = 0; i < mDatas.Length; i++)
            {
                bool isGet = false;
                if (datas != null)
                {
                    for (int j = 0; j < datas.Length; j++)
                    {
                        if (datas[j]._isWeapon) continue;
                        if (datas[j]._id.Equals(mDatas[i].ID))
                        {
                            isGet = true;
                            break;
                        }
                    }
                }
                bool isNormal = false;
                if (mDatas[i].Type.Equals("normal")) isNormal = true;
                GameObject obj = Instantiate(itemPrefabs);
                if (isNormal)
                {
                    obj.transform.SetParent(leftList.transform);
                    leftCount++;
                    if (isGet) leftGetCount++;
                }
                else
                {
                    obj.transform.SetParent(rightList.transform);
                    rightCount++;
                    if (isGet) rightGetCount++;
                }
                obj.transform.localScale = Vector3.one;
                CollectionItem item = obj.GetComponent<CollectionItem>();
                item.InitSetting(mDatas[i].ID, false, isGet, isNormal);
                itemList.Add(item);
            }
        }
        isOpen = true;

        float leftPer = (float)leftGetCount / (float)leftCount;
        leftPer *= 100;

        float rightPer = (float)rightGetCount / (float)rightCount;
        rightPer *= 100;
        leftDataCollText.text = LocalizeManager.GetLocalize("datacollection") + leftPer.ToString("F0") + "%";
        rightDataCollText.text = LocalizeManager.GetLocalize("datacollection") + rightPer.ToString("F0") + "%";

        List<CollectionItem> leftListItem = new List<CollectionItem>();
        List<CollectionItem> rightListItem = new List<CollectionItem>();
        for (int i = 0; i < itemList.Count; i++)
        {
            if (itemList[i].isLeft) leftListItem.Add(itemList[i]);
            else rightListItem.Add(itemList[i]);
        }
        itemList.Clear();
        for (int i = 0; i < leftListItem.Count; i++) itemList.Add(leftListItem[i]);
        for (int i = 0; i < rightListItem.Count; i++) itemList.Add(rightListItem[i]);
        leftListItem.Clear();
        rightListItem.Clear();
        openObj.SetActive(true);
    }
    void Close()
    {
        isOpen = false;
        GetComponent<Image>().enabled = false;
        PDG.Dungeon.instance.newObjectSelected = false;
        PDG.Dungeon.instance.newObjectSelectedObj = null;
        _cImage.color = new Color(0, 0, 0, 0);
        _idText.text = string.Empty;
        _contText.text = string.Empty;
        openObj.SetActive(false);
    }
    public void ClickEvent(string _id, bool _isWeapon, bool _isGet, Sprite _sprite)
    {
        for (int i = 0; i < itemList.Count; i++)
        {
            if(itemList[i]._image.sprite == _sprite)
            {
                itemList[i]._image.GetComponent<RectTransform>().sizeDelta = new Vector2(96, 96);
            }
            else
            {
                itemList[i]._image.GetComponent<RectTransform>().sizeDelta = new Vector2(60, 60);
            }
        }
        _cImage.color = Color.white;
        _cImage.sprite = _sprite;
        if (!_isGet) _cImage.material = grayMaterial;
        else _cImage.material = null;
        if(_isWeapon && _isGet)
        {
            _idText.text = LocalizeManager.GetLocalize(_id);
            _contText.text = LocalizeManager.GetLocalize(_id + "_cont");
        }
        else if (!_isWeapon && _isGet)
        {
            _idText.text = LocalizeManager.GetLocalize("monster_" + _id);
            _contText.text = LocalizeManager.GetLocalize("monster_" + _id + "_cont");
        }
        else
        {
            _idText.text = LocalizeManager.GetLocalize("notCollection_name");
            _contText.text = LocalizeManager.GetLocalize("notCollection_cont");
        }
    }
}
