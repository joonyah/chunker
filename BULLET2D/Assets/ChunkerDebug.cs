﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChunkerDebug : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        if(SteamStatsAndAchievements.instance != null)
        {
            if(!SteamStatsAndAchievements.instance.isGUI)
                gameObject.SetActive(false);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (SteamStatsAndAchievements.instance != null)
        {
            if (!SteamStatsAndAchievements.instance.isGUI)
                gameObject.SetActive(false);
        }
    }

    public void ItemSetting(InputField _inputField)
    {
        string value = _inputField.text;
        if (value.Equals(string.Empty)) return;
        int t = System.Array.FindIndex(SheetManager.Instance.ItemsDB.dataArray, item => item.ID.Equals(value));
        if (t == -1) return;
        GameObject obj = ObjManager.Call().GetObject("Item");
        if (obj != null)
        {
            ItemsData iData = System.Array.Find(SheetManager.Instance.ItemsDB.dataArray, item => item.ID.Equals(value));
            Item _item = obj.GetComponent<Item>();
            _item._Item = new ItemInfo
            {
                Data = Inventorys.Instance.CopyItem(iData)
            };
            if (!_item._Item.Data.ID.Equals("glowing_skull"))
            {
                Inventorys.Instance.DiscardedItem.Add(_item._Item);
                Inventorys.Instance.DiscardedItemString.Add(_item._Item.Data.ID);
            }
            _item.dir = Vector3.zero;
            _item.itemCount = 1;
            _item.rPow = 1;
            obj.transform.position = PDG.Player.instance.transform.position;
            obj.SetActive(true);
        }
        _inputField.text = string.Empty;
    }

    public void StageSetting(InputField _inputField)
    {
        string value = _inputField.text;
        int t = int.Parse(value);
        if (t >= 4) _inputField.text = string.Empty;
        PDG.Dungeon.instance.StageLevel = t;
    }
}
