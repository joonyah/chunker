using PDG;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SciFiBeamStatic : MonoBehaviour
{

    [Header("Prefabs")]
    public GameObject beamLineRendererPrefab; //Put a prefab with a line renderer onto here.
    public GameObject beamStartPrefab; //This is a prefab that is put at the start of the beam.
    public GameObject beamEndPrefab; //Prefab put at end of beam.

    private GameObject beamStart;
    private GameObject beamEnd;
    private GameObject beam;
    private LineRenderer line;

    [Header("Beam Options")]
    public bool alwaysOn = true; //Enable this to spawn the beam when script is loaded.
    public bool beamCollides = true; //Beam stops at colliders
    public float beamLength = 100; //Ingame beam length
    public float beamEndOffset = 0f; //How far from the raycast hit point the end effect is positioned
    public float textureScrollSpeed = 0f; //How fast the texture scrolls along the beam, can be negative or positive.
    public float textureLengthScale = 1f;   //Set this to the horizontal length of your texture relative to the vertical. 
                                            //Example: if texture is 200 pixels in height and 600 in length, set this to 3
    public Vector3 dir;
    [SerializeField] private GameObject HitEffect;
    [SerializeField] private GameObject HitEffect2;
    [SerializeField] private Vector3 hitEffectSize = new Vector3(0.3f, 0.3f, 0.3f);

    private int bounsCount = 0;
    float bounsDelayTime = 0.15f;

    [SerializeField] private List<GameObject> objList = new List<GameObject>();
    [SerializeField] private List<float> timeList = new List<float>();
    public float hitTime = 0.0f;

    void Start()
    {
        
    }

    private void OnEnable()
    {
        if (Player.instance != null)
        {
            beamLength = Player.instance.bulletRangeRight;
            hitTime = Player.instance.tickTimeR;
        }
        bounsCount = 0;
        if (alwaysOn) //When the object this script is attached to is enabled, spawn the beam.
            SpawnBeam();
        objList.Clear();
        timeList.Clear();
    }
    private void OnDisable() //If the object this script is attached to is disabled, remove the beam.
    {
        RemoveBeam();
    }
    void FixedUpdate()
    {
        for (int i = 0; i < objList.Count; i++)
        {
            if (objList[i] == null)
            {
                objList.RemoveAt(i);
                timeList.RemoveAt(i);
                i--;
                continue;
            }
            else if (objList[i].GetComponent<MOB.MonsterArggro>())
            {
                if (objList[i].GetComponent<MOB.MonsterArggro>()._monster.isDie)
                {
                    objList.RemoveAt(i);
                    timeList.RemoveAt(i);
                    i--;
                    continue;
                }
            }
            else if (objList[i].GetComponent<MOB.Monster>())
            {
                if (objList[i].GetComponent<MOB.Monster>().isDie)
                {
                    objList.RemoveAt(i);
                    timeList.RemoveAt(i);
                    i--;
                    continue;
                }
            }
            timeList[i] += Time.fixedDeltaTime;
        }

        if (beam) //Updates the beam
        {
            int c = 0;
            line.SetPosition(c, transform.position);
            c++;
            Vector3 end;
            RaycastHit2D hit = Physics2D.Raycast(transform.position, dir, beamLength, 1 << 12 | 1 << 24 | 1 << 23 | 1 << 8);
            float damage = Player.instance.isRightNear ? Player.instance.damageRightNear : Player.instance.damageRight;
            Vector3 _dir = dir;
            while (true)
            {
                hit = Physics2D.Raycast(transform.position, _dir, beamLength, 1 << 12 | 1 << 24 | 1 << 23 | 1 << 8);
                if (hit && beamCollides)
                {
                    line.SetPosition(c, hit.point);
                    end = hit.point;
                    SetHit(hit, damage);
                    float angle = GetReflectAngle(hit.point, hit.collider.transform.position, _dir);
                    _dir = GetPosition(hit.point, angle) - (Vector3)hit.point;
                    _dir.Normalize();
                    c++;
                    int max = 0;
                    if (Player.instance.isLaserBounce) max = (Player.instance.BounsLaserCount + 1);
                    else max = 1;
                    if (c > max) 
                    {
                        c--;
                        break;
                    }
                }
                else
                {
                    end = transform.position + (_dir * beamLength);
                    if (c < (Player.instance.BounsLaserCount + 1)) 
                    {
                        for (int i = c; c < (Player.instance.BounsLaserCount + 1); c++) 
                        {
                            line.SetPosition(i, end);
                        }
                    }
                    break;
                }
            }
            try
            {
                if (c >= line.positionCount) c = line.positionCount - 1;
                line.SetPosition(c, end);
            }
            catch(System.Exception e)
            {
                Debug.Log(c);
                Debug.LogError(e.Message);
            }

            if (beamStart)
            {
                beamStart.transform.position = transform.position;
                beamStart.transform.LookAt(end);
            }
            if (beamEnd)
            {
                beamEnd.transform.position = end;
                beamEnd.transform.LookAt(beamStart.transform.position);
            }

            float distance = Vector3.Distance(transform.position, end);
            line.material.mainTextureScale = new Vector2(distance / textureLengthScale, 1); //This sets the scale of the texture so it doesn't look stretched
            line.material.mainTextureOffset -= new Vector2(Time.deltaTime * textureScrollSpeed, 0); //This scrolls the texture along the beam if not set to 0
        }
    }
    void SetHit(RaycastHit2D hit, float damage)
    {
        GameObject obj33 = hit.collider.gameObject;
        int index  = objList.FindIndex(item => item.Equals(obj33));
        if (index != -1)
        {
            if (hitTime > timeList[index]) return;
            else timeList[index] = 0.0f;
        }
        else
        {
            objList.Add(obj33);
            timeList.Add(0.0f);
        }

        if (hit.collider.gameObject.layer.Equals(12))
        {
            Vector3 dir = hit.collider.transform.position - transform.position;
            dir.Normalize();

            if (hit.collider.GetComponent<MOB.Monster>())
            {
                hit.collider.GetComponent<MOB.Monster>().SetDamage(damage, -dir, 1, ELEMENTAL_TYPE.NONE, 0.0f);
                if (HitEffect)
                {
                    GameObject obj = Instantiate(HitEffect);
                    obj.transform.position = hit.point;
                    obj.transform.localScale = hitEffectSize;
                    Destroy(obj, 2f);
                }
                if (HitEffect2)
                {
                    GameObject obj = Instantiate(HitEffect2);
                    obj.transform.position = hit.point;
                    obj.transform.localScale = hitEffectSize;
                    Destroy(obj, 2f);
                }
            }
        }
        else if (hit.collider.gameObject.layer.Equals(24))
        {
            if (hit.collider.GetComponent<MOB.BossMonster_Six_Apear>()) hit.collider.GetComponent<MOB.BossMonster_Six_Apear>().SetDamage(damage);
            if (hit.collider.GetComponent<Boss_STG>()) hit.collider.GetComponent<Boss_STG>().SetDamage(damage, hit.point);
            if (hit.collider.GetComponent<Boss_NPC>()) hit.collider.GetComponent<Boss_NPC>().SetDamage(damage, hit.point);
            if (hit.collider.GetComponent<Boss_Golem>()) hit.collider.GetComponent<Boss_Golem>().SetDamage(damage, hit.point);
            if (hit.collider.GetComponent<Boss_Stone>()) hit.collider.GetComponent<Boss_Stone>().SetDamage(damage, hit.point, false);
            if (Player.instance.isStrategic_02) Player.instance.isStrategic_02 = false;
        }
        else if (hit.collider.gameObject.layer.Equals(23))
        {
            if (hit.collider.transform.parent && hit.collider.transform.parent.GetComponent<TentacleController>())
            {
                hit.collider.transform.parent.GetComponent<TentacleController>().SetDamage(1);
                if (HitEffect)
                {
                    GameObject obj = Instantiate(HitEffect);
                    obj.transform.position = hit.point;
                    obj.transform.localScale = hitEffectSize;
                    Destroy(obj, 2f);
                }
                if (HitEffect2)
                {
                    GameObject obj = Instantiate(HitEffect2);
                    obj.transform.position = hit.point;
                    obj.transform.localScale = hitEffectSize;
                    Destroy(obj, 2f);
                }
            }
            Vector3 dir = hit.collider.gameObject.transform.position - PDG.Player.instance.transform.position;
            dir.Normalize();
            if (hit.collider.gameObject.GetComponent<DestroyObjects>())
            {
                hit.collider.gameObject.GetComponent<DestroyObjects>().DestroyObject(dir);
                if (HitEffect)
                {
                    GameObject obj = Instantiate(HitEffect);
                    obj.transform.position = hit.point;
                    obj.transform.localScale = hitEffectSize;
                    Destroy(obj, 2f);
                }
                if (HitEffect2)
                {
                    GameObject obj = Instantiate(HitEffect2);
                    obj.transform.position = hit.point;
                    obj.transform.localScale = hitEffectSize;
                    Destroy(obj, 2f);
                }
            }
            if (hit.collider.gameObject.GetComponent<ChestHit>() && !hit.collider.gameObject.GetComponent<ChestHit>().isOpenBefore)
            {
                hit.collider.gameObject.GetComponent<ChestHit>().SetDamage(1);
                if (HitEffect)
                {
                    GameObject obj = Instantiate(HitEffect);
                    obj.transform.position = hit.point;
                    obj.transform.localScale = hitEffectSize;
                    Destroy(obj, 2f);
                }
                if (HitEffect2)
                {
                    GameObject obj = Instantiate(HitEffect2);
                    obj.transform.position = hit.point;
                    obj.transform.localScale = hitEffectSize;
                    Destroy(obj, 2f);
                }
            }
            if (hit.collider.gameObject.GetComponent<DrumHit>())
            {
                hit.collider.gameObject.GetComponent<DrumHit>().SetDamage(1);
                if (HitEffect)
                {
                    GameObject obj = Instantiate(HitEffect);
                    obj.transform.position = hit.point;
                    obj.transform.localScale = hitEffectSize;
                    Destroy(obj, 2f);
                }
                if (HitEffect2)
                {
                    GameObject obj = Instantiate(HitEffect2);
                    obj.transform.position = hit.point;
                    obj.transform.localScale = hitEffectSize;
                    Destroy(obj, 2f);
                }
            }
        }
    }
    public void SpawnBeam() //This function spawns the prefab with linerenderer
    {
        if (beamLineRendererPrefab)
        {
            if (beamStartPrefab)
                beamStart = Instantiate(beamStartPrefab);
            if (beamEndPrefab)
                beamEnd = Instantiate(beamEndPrefab);
            beam = Instantiate(beamLineRendererPrefab);
            beam.transform.position = transform.position;
            beam.transform.parent = transform;
            beam.transform.rotation = transform.rotation;
            line = beam.GetComponent<LineRenderer>();
            line.useWorldSpace = true;
            float width = Random.Range(0.1f, 2f);
            line.startWidth = width;
            line.endWidth = width;
#if UNITY_5_5_OR_NEWER
            if (Player.instance != null && Player.instance.isLaserBounce)
            {
                line.positionCount = 2 + Player.instance.BounsLaserCount;
            }
            else
                line.positionCount = 2;
			#else
			line.SetVertexCount(2); 
			#endif
        }
        else  print("Add a hecking prefab with a line renderer to the SciFiBeamStatic script on " + gameObject.name + "! Heck!");
        StartCoroutine(DisabledObj());
    }
    IEnumerator DisabledObj()
    {
        yield return new WaitForSeconds(0.15f);
        gameObject.SetActive(false);
    }
    public void RemoveBeam() //This function removes the prefab with linerenderer
    {
        if (beam)
            Destroy(beam);
        if (beamStart)
            Destroy(beamStart);
        if (beamEnd)
            Destroy(beamEnd);
    }

    #region -- Tool --
    float GetAngle(Vector3 vStart, Vector3 vEnd)
    {
        Vector3 v = vEnd - vStart;

        return Mathf.Atan2(v.y, v.x) * Mathf.Rad2Deg;
    }
    Vector3 GetPosition(Vector3 vStart, float angle)
    {
        float x = Mathf.Cos(Mathf.Deg2Rad * angle) + vStart.x;
        float y = Mathf.Sin(Mathf.Deg2Rad * angle) + vStart.y;

        return new Vector3(x, y, 0);
    }
    float GetReflectAngle(Vector3 _hitPoint, Vector3 _hitPosition, Vector3 _dir)
    {
        Vector3 _dir2 = _hitPoint - _hitPosition;
        _dir2.Normalize();

        if (_dir.x == 0 && _dir.y > 0)       // 위 
        {
            return 270.0f;
        }
        else if (_dir.x == 0 && _dir.y < 0)  // 아래
        {
            return 90.0f;
        }
        else if (_dir.x > 0 && _dir.y == 0)  // 오른쪽
        {
            return 180.0f;
        }
        else if (_dir.x < 0 && _dir.y == 0)  // 왼쪽
        {
            return 0.0f;
        }
        else if (_dir.x > 0 && _dir.y > 0)  // 오른쪽 위 
        {
            if (_dir2.x > 0)
            {
                float zz = GetAngle(_hitPoint, transform.position);
                float f = zz - 180.0f; // 입사각
                return 360.0f - f; // 반사각
            }
            else if (_dir2.y > 0)
            {
                float zz = GetAngle(_hitPoint, transform.position);
                float f = 270.0f - zz; // 입사각
                return 90.0f + f; // 반사각
            }
            else
            {
                float hitAngle = GetAngle(_hitPosition, _hitPoint);
                if (hitAngle > 225.0f)
                {
                    float zz = GetAngle(_hitPoint, transform.position);
                    float f = zz - 180.0f; // 입사각
                    return 360.0f - f; // 반사각
                }
                else if (hitAngle < 225.0f)
                {
                    float zz = GetAngle(_hitPoint, transform.position);
                    float f = 270.0f - zz; // 입사각
                    return 90.0f + f; // 반사각
                }
                else
                {
                    return 225.0f;
                }

            }
        }
        else if (_dir.x < 0 && _dir.y > 0)   // 왼쪽 위
        {
            if (_dir2.x < 0)
            {
                float zz = GetAngle(_hitPoint, transform.position);
                float f = 360.0f - zz; // 입사각
                return 180.0f + f; // 반사각
            }
            else if (_dir2.y > 0)
            {
                float zz = GetAngle(_hitPoint, transform.position);
                float f = zz - 270.0f; // 입사각
                return f; // 반사각
            }
            else
            {
                float hitAngle = GetAngle(_hitPosition, _hitPoint);
                if (hitAngle > 315.0f)
                {
                    float zz = GetAngle(_hitPoint, transform.position);
                    float f = zz - 270.0f; // 입사각
                    return f; // 반사각
                }
                else if (hitAngle < 315.0f)
                {
                    float zz = GetAngle(_hitPoint, transform.position);
                    float f = 360.0f - zz; // 입사각
                    return 180.0f + f; // 반사각
                }
                else
                {
                    return 315.0f;
                }

            }
        }
        else if (_dir.x < 0 && _dir.y < 0)   // 왼쪽 아래
        {
            if (_dir2.x < 0)
            {
                float zz = GetAngle(_hitPoint, transform.position);
                float f = zz; // 입사각
                return 180.0f - f; // 반사각
            }
            else if (_dir2.y < 0)
            {
                float zz = GetAngle(_hitPoint, transform.position);
                float f = 90.0f - zz; // 입사각
                return 270.0f + f; // 반사각
            }
            else
            {
                float hitAngle = GetAngle(_hitPosition, _hitPoint);
                if (hitAngle > 45.0f)
                {
                    float zz = GetAngle(_hitPoint, transform.position);
                    float f = zz; // 입사각
                    return 180.0f - f; // 반사각
                }
                else if (hitAngle < 45.0f)
                {
                    float zz = GetAngle(_hitPoint, transform.position);
                    float f = 90.0f - zz; // 입사각
                    return 270.0f + f; // 반사각
                }
                else
                {
                    return 45.0f;
                }

            }
        }
        else if (_dir.x > 0 && _dir.y < 0)   // 오른쪽 아래
        {
            if (_dir2.x > 0)
            {
                float zz = GetAngle(_hitPoint, transform.position);
                float f = 180.0f - zz; // 입사각
                return f; // 반사각
            }
            else if (_dir2.y < 0)
            {
                float zz = GetAngle(_hitPoint, transform.position);
                float f = zz - 90.0f; // 입사각
                return 270.0f - f; // 반사각
            }
            else
            {
                float hitAngle = GetAngle(_hitPosition, _hitPoint);
                if (hitAngle > 135.0f)
                {
                    float zz = GetAngle(_hitPoint, transform.position);
                    float f = zz - 90.0f; // 입사각
                    return 270.0f - f; // 반사각
                }
                else if (hitAngle < 135.0f)
                {
                    float zz = GetAngle(_hitPoint, transform.position);
                    float f = 180.0f - zz; // 입사각
                    return f; // 반사각
                }
                else
                {
                    return 135.0f;
                }
            }
        }
        else
        {
            return 0.0f;
        }
    }
    #endregion
}