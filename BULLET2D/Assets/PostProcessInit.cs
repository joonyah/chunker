﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

public class PostProcessInit : MonoBehaviour
{
    public PostProcessResources postProcessResources;
    // Start is called before the first frame update
    void Start()
    {
        PostProcessLayer postProcessLayer = Camera.main.gameObject.AddComponent<PostProcessLayer>();
        postProcessLayer.Init(postProcessResources);
        postProcessLayer.volumeTrigger = Camera.main.transform;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
