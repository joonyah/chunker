﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class MemorialManager : MonoBehaviour
{
    public static MemorialManager instance;
    public bool isOpen = false;
    [SerializeField] private GameObject openObj;
    [SerializeField] private Image[] SelecteBars;
    [SerializeField] private int nowSelectNum = 0;

    [Header("Group")]
    [SerializeField] private GameObject NormalObj;
    [SerializeField] private GameObject AdventureObj;
    [SerializeField] private GameObject HunterObj;
    [SerializeField] private GameObject DeathObj;
    [SerializeField] private GameObject AchievementsObj;
    [SerializeField] private Transform HunterParant;
    [SerializeField] private GameObject listPrefabs;
    [SerializeField] private GameObject joystrickAddBtn;
    [SerializeField] private GameObject joystrickAddBtn2;

    [SerializeField] private float subScrollValue = 1.0f;


    [Header("Achievements")]
    [SerializeField] private Transform AchievementsParant;
    [SerializeField] private GameObject AchievementsPrefabs;
    [SerializeField] private GameObject AchievementsPrefabs2;
    [SerializeField] private GameObject AchievementsPrefabs3;
    [SerializeField] private Text AchievementsTotal;
    [SerializeField] private List<GameObject> LineList = new List<GameObject>();
    [SerializeField] private List<GameObject> AchievementsList = new List<GameObject>();
    [SerializeField] private List<GameObject> SubAchievementsList = new List<GameObject>();
    [SerializeField] private Sprite checkOff;
    [SerializeField] private Sprite checkOn;
    [SerializeField] private Material DontClearMaterial;
    [SerializeField] private int nowAchievements = 0;
    [SerializeField] private int nowAchievementsY = 0;
    [SerializeField] private bool isJoystickAchievementsCheck = false;
    public GameObject popupObj;
    [SerializeField] private Font _font;
    [SerializeField] private GraphicRaycaster gr;

    private void Awake()
    {
        if (instance == null) instance = this;
    }
    private void Start()
    {
        MonstersData[] mobDatas = System.Array.FindAll(SheetManager.Instance.MonsterDB.dataArray, item => !item.Targetstage[0].Equals("Dungeon_00") && !item.Targetstage[0].Equals("DungeonSpecial") && !item.ID.Contains("_t"));
        for (int i = 0; i < mobDatas.Length; i++)
        {
            GameObject line = Instantiate(listPrefabs);
            line.transform.SetParent(HunterParant);
            line.transform.localScale = Vector3.one;
            line.transform.Find("LeftText").GetComponent<Text>().text = "-----------------------------------------------------------------------------";

            // name
            GameObject name = Instantiate(listPrefabs);
            name.transform.SetParent(HunterParant);
            name.transform.localScale = Vector3.one;
            name.transform.Find("LeftText").GetComponent<SetLanguage>().InitSetting("monster_" + mobDatas[i].ID);

            // hunting
            GameObject hunting = Instantiate(listPrefabs);
            hunting.transform.SetParent(HunterParant);
            hunting.transform.localScale = Vector3.one;
            hunting.transform.Find("LeftText").GetComponent<SetLanguage>().InitSetting("memorial_hunting_count");
            hunting.transform.Find("RightText").GetComponent<SetSteamStat>().InitSetting(mobDatas[i].ID + "_hunting");

            // death
            GameObject death = Instantiate(listPrefabs);
            death.transform.SetParent(HunterParant);
            death.transform.localScale = Vector3.one;
            death.transform.Find("LeftText").GetComponent<SetLanguage>().InitSetting("memorial_hunting_death");
            death.transform.Find("RightText").GetComponent<SetSteamStat>().InitSetting(mobDatas[i].ID + "_death");

        }
        GameObject lastLine = Instantiate(listPrefabs);
        lastLine.transform.SetParent(HunterParant);
        lastLine.transform.localScale = Vector3.one;
        lastLine.transform.Find("LeftText").GetComponent<Text>().text = "-----------------------------------------------------------------------------";

        StartCoroutine(ScrollViewLock());
    }
    IEnumerator ScrollViewLock()
    {
        while(true)
        {
            RectTransform content;
            if (nowSelectNum == 2) content = HunterParant.GetComponent<RectTransform>();
            else content = AchievementsParant.GetComponent<RectTransform>();
            float max = -content.sizeDelta.y;
            Vector3 min = content.offsetMin;
            if (min.y < max) min.y = max;
            if (min.y > -510) min.y = -510;
            content.offsetMin = min;
            yield return null;
        }
    }
    void Update()
    {
        if (PDG.Dungeon.instance.isJoystrick)
        {
            joystrickAddBtn.SetActive(true);
            joystrickAddBtn2.SetActive(true);
        }
        else
        {
            joystrickAddBtn.SetActive(false);
            joystrickAddBtn2.SetActive(false);
        }

        if (isOpen) 
        {
            float x = Input.GetAxisRaw("Horizontal_joy");
            float y = Input.GetAxisRaw("Vertical_joy");


            if (PDG.Dungeon.instance.isJoystrick)
            {
                float x2 = (PDG.Dungeon.instance.joystricks[0].Contains("PC") || PDG.Dungeon.instance.joystricks[0].Contains("DS4") || PDG.Dungeon.instance.joystricks[0].Contains("Wireless")) ? Input.GetAxisRaw("PS4_subX") : Input.GetAxisRaw("Horizontal_sub");
                float y2 = (PDG.Dungeon.instance.joystricks[0].Contains("PC") || PDG.Dungeon.instance.joystricks[0].Contains("DS4") || PDG.Dungeon.instance.joystricks[0].Contains("Wireless")) ? Input.GetAxisRaw("PS4_subY") : Input.GetAxisRaw("Vertical_sub");

                if (AchievementsObj.activeSelf)
                {
                    if (x2 > 0 && PDG.Dungeon.instance.closeCoolTime > 0.2f)
                    {
                        popupObj.SetActive(false);
                        isJoystickAchievementsCheck = false;
                        nowAchievements++;
                        if (nowAchievements >= SubAchievementsList.Count) nowAchievements = SubAchievementsList.Count - 1;
                        PDG.Dungeon.instance.closeCoolTime = 0.0f;
                        for (int i = 0; i < AchievementsList.Count; i++)
                        {
                            if (SubAchievementsList[nowAchievements].transform.parent.gameObject == AchievementsList[i])
                            {
                                nowAchievementsY = i;
                                break;
                            }
                        }
                    }
                    else if (x2 < 0 && PDG.Dungeon.instance.closeCoolTime > 0.2f)
                    {
                        popupObj.SetActive(false);
                        isJoystickAchievementsCheck = false;
                        nowAchievements--;
                        if (nowAchievements < 0) nowAchievements = 0;
                        PDG.Dungeon.instance.closeCoolTime = 0.0f;
                        for (int i = 0; i < AchievementsList.Count; i++)
                        {
                            if (SubAchievementsList[nowAchievements].transform.parent.gameObject == AchievementsList[i])
                            {
                                nowAchievementsY = i;
                                break;
                            }
                        }
                    }
                    else if (y2 < 0 && PDG.Dungeon.instance.closeCoolTime > 0.2f)
                    {
                        popupObj.SetActive(false);
                        isJoystickAchievementsCheck = false;
                        nowAchievementsY++;
                        if (nowAchievementsY >= AchievementsList.Count) nowAchievementsY = AchievementsList.Count - 1;
                        PDG.Dungeon.instance.closeCoolTime = 0.0f;
                        for (int i = 0; i < SubAchievementsList.Count; i++)
                        {
                            if (AchievementsList[nowAchievementsY].transform.Find("Reward").GetChild(0).gameObject == SubAchievementsList[i])
                            {
                                nowAchievements = i;
                                break;
                            }
                        }
                    }
                    else if (y2 > 0 && PDG.Dungeon.instance.closeCoolTime > 0.2f)
                    {
                        popupObj.SetActive(false);
                        isJoystickAchievementsCheck = false;
                        nowAchievementsY--;
                        if (nowAchievementsY < 0) nowAchievementsY = 0;
                        PDG.Dungeon.instance.closeCoolTime = 0.0f;
                        for (int i = 0; i < SubAchievementsList.Count; i++)
                        {
                            if (AchievementsList[nowAchievementsY].transform.Find("Reward").GetChild(0).gameObject == SubAchievementsList[i])
                            {
                                nowAchievements = i;
                                break;
                            }
                        }
                    }
                    for (int i = 0; i < SubAchievementsList.Count; i++)
                    {
                        if (nowAchievements == i)
                        {
                            if (SubAchievementsList[i].GetComponent<Image>())
                                SubAchievementsList[i].GetComponent<Image>().color = new Color32(100, 100, 100, 255);
                            else
                                SubAchievementsList[i].transform.Find("Image").GetComponent<Image>().color = new Color32(100, 100, 100, 255);
                        }
                        else
                        {
                            if (SubAchievementsList[i].GetComponent<Image>())
                                SubAchievementsList[i].GetComponent<Image>().color = new Color32(255, 255, 255, 255);
                            else
                                SubAchievementsList[i].transform.Find("Image").GetComponent<Image>().color = new Color32(255, 255, 255, 255);
                        }
                    }
                    float posY = SubAchievementsList[nowAchievements].transform.position.y;
                    if (posY < 200)
                    {
                        float t = (posY * -1) + 400;
                        RectTransform content = AchievementsParant.GetComponent<RectTransform>();
                        content.anchorMin = new Vector2(0, 1);
                        content.anchorMax = new Vector2(1, 1);
                        Vector2 temp = content.offsetMin;
                        temp.y += t;
                        content.offsetMin = temp;
                        posY += t;
                        popupObj.transform.position = new Vector3(popupObj.transform.position.x, posY, 0);
                    }
                    else if(posY > 600)
                    {
                        float t = posY - 400;
                        RectTransform content = AchievementsParant.GetComponent<RectTransform>();
                        content.anchorMin = new Vector2(0, 1);
                        content.anchorMax = new Vector2(1, 1);
                        Vector2 temp = content.offsetMin;
                        temp.y -= t;
                        content.offsetMin = temp;
                        posY -= t;
                        popupObj.transform.position = new Vector3(popupObj.transform.position.x, posY, 0);
                    }
                }
                else if (y2 < 0)
                {
                    subScrollValue -= 0.01f;
                    if (subScrollValue < 0) subScrollValue = 0;
                    RectTransform content;
                    if (nowSelectNum == 2) content = HunterParant.GetComponent<RectTransform>();
                    else content = AchievementsParant.GetComponent<RectTransform>();
                    float height = content.sizeDelta.y;
                    height -= 510;
                    float temp2 = height * subScrollValue;
                    temp2 += 510;
                    Vector2 temp = new Vector2(0, -temp2);
                    content.offsetMin = temp;
                }
                else if (y2 > 0)
                {
                    subScrollValue += 0.01f;
                    if (subScrollValue > 1) subScrollValue = 1;
                    RectTransform content;
                    if (nowSelectNum == 2) content = HunterParant.GetComponent<RectTransform>();
                    else content = AchievementsParant.GetComponent<RectTransform>();
                    float height = content.sizeDelta.y;
                    height -= 510;
                    float temp2 = height * subScrollValue;
                    temp2 += 510;
                    Vector2 temp = new Vector2(0, -temp2);
                    content.offsetMin = temp;
                }

            }

            if (Input.GetKeyDown(KeyCode.Escape) || PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_CANCLE], true) && PDG.Dungeon.instance.closeCoolTime > 0.2f)
            {
                PDG.Dungeon.instance.closeCoolTime = 0.0f;
                if (isJoystickAchievementsCheck)
                {
                    isJoystickAchievementsCheck = false;
                    popupObj.SetActive(false);
                }
                else
                    Close();
            }
            if ((PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_CONFIRM], true)|| PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ACTIVE], true)) && PDG.Dungeon.instance.closeCoolTime > 0.2f)
            {
                PDG.Dungeon.instance.closeCoolTime = 0.0f;
                if (AchievementsObj.activeSelf)
                {
                    if (isJoystickAchievementsCheck)
                        popupObj.transform.Find("Reward").Find("Reward").GetComponent<Button>().onClick.Invoke();
                    else
                    {
                        isJoystickAchievementsCheck = true;
                        SubAchievementsList[nowAchievements].GetComponent<Button>().onClick.Invoke();
                    }
                }
                else
                    SelecteBarPointerClick(nowSelectNum);
            }
            if ((PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ARROW_DOWN],true)  || Input.GetKeyDown(KeyCode.DownArrow) || y < 0) && PDG.Dungeon.instance.closeCoolTime > 0.2f)
            {
                PDG.Dungeon.instance.closeCoolTime = 0.0f;
                nowSelectNum++;
                if (nowSelectNum >= SelecteBars.Length) nowSelectNum = 0;
                SelecteBarPointerEnter(SelecteBars[nowSelectNum]);
            }
            if ((PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ARROW_UP], true) || Input.GetKeyDown(KeyCode.UpArrow) || y > 0) && PDG.Dungeon.instance.closeCoolTime > 0.2f)
            {
                PDG.Dungeon.instance.closeCoolTime = 0.0f;
                nowSelectNum--;
                if (nowSelectNum < 0) nowSelectNum = SelecteBars.Length - 1;
                SelecteBarPointerEnter(SelecteBars[nowSelectNum]);
            }
            if (popupObj.activeSelf && PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ATTACK], true))
            {
                var ped = new PointerEventData(null);
                ped.position = Input.mousePosition;
                List<RaycastResult> results = new List<RaycastResult>();
                gr.Raycast(ped, results);

                if (results.Count > 0)
                {
                    if(!results[0].gameObject.name.Equals("Reward") && !results[0].gameObject.name.Equals("MouseClickPopup") && !results[0].gameObject.name.Contains("Achievements_Reward"))
                    {
                        popupObj.SetActive(false);
                    }
                }
            }
        }
    }
    public void ShowMemorial()
    {
        GetComponent<Image>().enabled = true;
        isOpen = true;
        nowSelectNum = 0;
        SelecteBarPointerEnter(SelecteBars[nowSelectNum]);
        openObj.SetActive(true);
    }
    IEnumerator RectReSize()
    {
        yield return null;
        RectTransform content;
        if (nowSelectNum == 2) content = HunterParant.GetComponent<RectTransform>();
        else content = AchievementsParant.GetComponent<RectTransform>();
        content.anchorMin = new Vector2(0, 1);
        content.anchorMax = new Vector2(1, 1);
        Vector2 temp = new Vector2(0, -content.sizeDelta.y);
        content.offsetMin = temp;
        temp = new Vector3(0, temp.y + content.sizeDelta.y);
        content.offsetMax = temp;
        if (-content.sizeDelta.y < -510)
        {
            HunterObj.transform.Find("Scroll View").GetComponent<ScrollRect>().vertical = true;
            AchievementsObj.transform.Find("Scroll View").GetComponent<ScrollRect>().vertical = true;
        }
        else
        {
            HunterObj.transform.Find("Scroll View").GetComponent<ScrollRect>().vertical = false;
            AchievementsObj.transform.Find("Scroll View").GetComponent<ScrollRect>().vertical = false;
        }
        subScrollValue = 1.0f;
    }
    void Close()
    {
        GetComponent<Image>().enabled = false;
        isOpen = false;
        NormalObj.SetActive(false);
        AdventureObj.SetActive(false);
        HunterObj.SetActive(false);
        DeathObj.SetActive(false);
        AchievementsObj.SetActive(false);
        openObj.SetActive(false);
    }
    public void SelecteBarPointerEnter(Image _selecteBar)
    {
        for(int i=0; i < SelecteBars.Length; i++)
        {
            if (SelecteBars[i] == _selecteBar) SelecteBars[i].enabled = true;
            else SelecteBars[i].enabled = false;
        }
    }
    public void SelecteBarPointerClick(int _type)
    {
        nowSelectNum = _type;
        if (_type == 0)
        {
            NormalObj.SetActive(true);
            AdventureObj.SetActive(false);
            HunterObj.SetActive(false);
            DeathObj.SetActive(false);
            AchievementsObj.SetActive(false);
        }
        else if (_type == 1)
        {
            NormalObj.SetActive(false);
            AdventureObj.SetActive(true);
            HunterObj.SetActive(false);
            DeathObj.SetActive(false);
            AchievementsObj.SetActive(false);
        }
        else if (_type == 2)
        {
            NormalObj.SetActive(false);
            AdventureObj.SetActive(false);
            HunterObj.SetActive(true);
            DeathObj.SetActive(false);
            AchievementsObj.SetActive(false);
            StartCoroutine(RectReSize());
        }
        else if (_type == 3)
        {
            NormalObj.SetActive(false);
            AdventureObj.SetActive(false);
            HunterObj.SetActive(false);
            DeathObj.SetActive(true);
            AchievementsObj.SetActive(false);
        }
        else if (_type == 4)
        {
            nowAchievements = 0;
            popupObj.SetActive(false);
            for (int i = 0; i < AchievementsList.Count; i++)
            {
                Destroy(AchievementsList[i]);
            }
            for (int i = 0; i < LineList.Count; i++)
            {
                Destroy(LineList[i]);
            }
            LineList.Clear();
            AchievementsList.Clear();
            SubAchievementsList.Clear();
            NormalObj.SetActive(false);
            AdventureObj.SetActive(false);
            HunterObj.SetActive(false);
            DeathObj.SetActive(false);
            string totalScore = LocalizeManager.GetLocalize("memorial_achievements_score");
            AchievementsData[] aDatas = SheetManager.Instance.AchievementsDB.dataArray;
            string category = string.Empty;
            bool categoryClear = false;
            int c = -1;
            int maxScore = 0;
            int curScore = 0;
            bool lastClear = false;
            for (int i = 0; i < aDatas.Length; i++)
            {
                maxScore += aDatas[i].SCORE;
                if (!category.Equals(aDatas[i].GROUP))
                {
                    if (i != 0)
                    {
                        GameObject lineObj = new GameObject();
                        lineObj.name = "LineObj";
                        Text text1 = lineObj.AddComponent<Text>();
                        text1.alignment = TextAnchor.MiddleCenter;
                        text1.fontSize = 24;
                        text1.font = _font;
                        text1.color = new Color32(0, 79, 19, 255);
                        text1.verticalOverflow = VerticalWrapMode.Overflow;
                        text1.horizontalOverflow = HorizontalWrapMode.Overflow;
                        text1.text = "\n\n\n---------------------------------------------------------------------";
                        text1.raycastTarget = false;
                        lineObj.transform.SetParent(AchievementsParant);
                        lineObj.transform.localScale = Vector3.one;
                        LineList.Add(lineObj);
                    }
                    lastClear = false;

                    category = aDatas[i].GROUP;
                    GameObject newObj = Instantiate(AchievementsPrefabs);
                    newObj.transform.SetParent(AchievementsParant);
                    newObj.transform.localScale = Vector3.one;

                    newObj.transform.Find("TitleText").GetComponent<Text>().text = LocalizeManager.GetLocalize(aDatas[i].ID);
                    newObj.transform.Find("ContText").GetComponent<Text>().text = LocalizeManager.GetLocalize(aDatas[i].ID + "_cont");
                    AchievementData loadData = SaveAndLoadManager.instance.LoadAchievements(aDatas[i].ID);

                    if (loadData._id.Equals(string.Empty))
                    {
                        int max = 0;
                        if (aDatas[i].ID.Contains("endlesschallenges_")) max = aDatas[i].MAX;
                        else if (loadData._id.Contains("soulmate") || loadData._id.Contains("researchaddiction")) max = loadData._max;
                        else max = aDatas[i].MAX;
                        newObj.transform.Find("Graph").Find("Graph").GetComponent<Image>().fillAmount = 0;
                        newObj.transform.Find("Graph").Find("Text").GetComponent<Text>().text = "0/" + max;
                        newObj.transform.Find("Graph").Find("Text").GetComponent<Text>().text += "\n" + LocalizeManager.GetLocalize(aDatas[i].ID + "_cont2");
                    }
                    else if (!loadData._clear)
                    {
                        int max = 0;
                        int cur = 0;
                        if (aDatas[i].ID.Contains("endlesschallenges_"))
                        {
                            cur = loadData._cur / 60;
                            max = aDatas[i].MAX;
                        }
                        else
                        {
                            cur = loadData._cur;
                            if (loadData._id.Contains("soulmate") || loadData._id.Contains("researchaddiction"))
                                max = loadData._max;
                            else
                                max = aDatas[i].MAX;
                        }

                        newObj.transform.Find("Graph").Find("Graph").GetComponent<Image>().fillAmount = (float)loadData._cur / (float)loadData._max;
                        newObj.transform.Find("Graph").Find("Text").GetComponent<Text>().text = cur + "/" + max;
                        newObj.transform.Find("Graph").Find("Text").GetComponent<Text>().text += "\n" + LocalizeManager.GetLocalize(aDatas[i].ID + "_cont2");
                    }
                    else
                    {
                        categoryClear = true;
                        int max = 0;
                        int cur = 0;
                        if (aDatas[i].ID.Contains("endlesschallenges_"))
                        {
                            cur = loadData._cur / 60;
                            max = aDatas[i].MAX;
                        }
                        else
                        {
                            cur = loadData._cur;
                            if (loadData._id.Contains("soulmate") || loadData._id.Contains("researchaddiction"))
                                max = loadData._max;
                            else
                                max = aDatas[i].MAX;
                        }

                        curScore += aDatas[i].SCORE;
                        newObj.transform.Find("Graph").Find("Graph").GetComponent<Image>().fillAmount = 1;
                        newObj.transform.Find("Graph").Find("Text").GetComponent<Text>().text = cur + "/" + max;
                        newObj.transform.Find("Graph").Find("Text").GetComponent<Text>().text += "\n" + LocalizeManager.GetLocalize(aDatas[i].ID + "_cont2");
                    }
                    AchievementsList.Add(newObj);
                    if (aDatas[i].TYPE.Equals("SINGLE"))
                    {
                        HorizontalLayoutGroup layoutgroup = newObj.transform.Find("Reward").gameObject.AddComponent<HorizontalLayoutGroup>();
                        layoutgroup.GetComponent<HorizontalLayoutGroup>().padding = new RectOffset(0, 0, 130, 0);
                        layoutgroup.GetComponent<HorizontalLayoutGroup>().spacing = 20;
                        layoutgroup.GetComponent<HorizontalLayoutGroup>().childAlignment = TextAnchor.MiddleLeft;
                        layoutgroup.GetComponent<HorizontalLayoutGroup>().childControlWidth = false;
                        layoutgroup.GetComponent<HorizontalLayoutGroup>().childControlHeight = false;
                        layoutgroup.GetComponent<HorizontalLayoutGroup>().childScaleWidth = true;
                        layoutgroup.GetComponent<HorizontalLayoutGroup>().childScaleHeight = true;
                        layoutgroup.GetComponent<HorizontalLayoutGroup>().childForceExpandWidth = false;
                        layoutgroup.GetComponent<HorizontalLayoutGroup>().childForceExpandHeight = true;
                        GameObject newObj2 = Instantiate(AchievementsPrefabs2);
                        newObj2.transform.SetParent(newObj.transform.Find("Reward"));
                        newObj2.transform.localScale = Vector3.one;
                        newObj2.GetComponent<Image>().sprite = Resources.Load<Sprite>("Achievements/" + aDatas[i].ID);
                        string _id = aDatas[i].ID;
                        newObj2.GetComponent<Button>().onClick.AddListener(() => { AchievementsClickEvent(_id, newObj2); });
                        if (!loadData._clear) newObj2.GetComponent<Image>().material = DontClearMaterial;
                        else
                        {
                            if (aDatas[i].REWARD && !loadData._reward)
                            {
                                newObj2.transform.Find("Gift").gameObject.SetActive(true);
                            }
                        }
                        SubAchievementsList.Add(newObj2);
                    }
                    else
                    {
                        GridLayoutGroup layoutgroup = newObj.transform.Find("Reward").gameObject.AddComponent<GridLayoutGroup>();
                        layoutgroup.cellSize = new Vector2(350, 100);
                        layoutgroup.spacing = new Vector2(50, -40);
                        GameObject newObj2 = Instantiate(AchievementsPrefabs3);
                        newObj2.transform.SetParent(newObj.transform.Find("Reward"));
                        newObj2.transform.localScale = Vector3.one;
                        newObj2.transform.Find("Text").GetComponent<Text>().text = LocalizeManager.GetLocalize(aDatas[i].ID + "_cont2");
                        string _id = aDatas[i].ID;
                        newObj2.GetComponent<Button>().onClick.AddListener(() => { AchievementsClickEvent(_id, newObj2); });
                        if (loadData._clear)
                        {
                            newObj2.transform.Find("Image").GetComponent<Image>().sprite = checkOn;

                            if (aDatas[i].REWARD && !loadData._reward)
                            {
                                newObj2.transform.Find("Image").Find("Gift").gameObject.SetActive(true);
                            }
                        }
                        else newObj2.transform.Find("Image").GetComponent<Image>().sprite = checkOff;
                        SubAchievementsList.Add(newObj2);
                    }
                    c++;
                }
                else
                {
                    AchievementData loadData = SaveAndLoadManager.instance.LoadAchievements(aDatas[i].ID);
                    if (aDatas[i].TYPE.Equals("SINGLE"))
                    {
                        GameObject newObj = Instantiate(AchievementsPrefabs2);
                        newObj.transform.SetParent(AchievementsList[c].transform.Find("Reward"));
                        newObj.transform.localScale = Vector3.one;
                        newObj.GetComponent<Image>().sprite = Resources.Load<Sprite>("Achievements/" + aDatas[i].ID);
                        string _id = aDatas[i].ID;
                        newObj.GetComponent<Button>().onClick.AddListener(() => { AchievementsClickEvent(_id, newObj); });
                        if (!loadData._clear) newObj.GetComponent<Image>().material = DontClearMaterial;
                        else
                        {
                            if (aDatas[i].REWARD && !loadData._reward)
                            {
                                newObj.transform.Find("Gift").gameObject.SetActive(true);
                            }
                        }
                        SubAchievementsList.Add(newObj);
                    }
                    else
                    {
                        GameObject newObj = Instantiate(AchievementsPrefabs3);
                        newObj.transform.SetParent(AchievementsList[c].transform.Find("Reward"));
                        newObj.transform.localScale = Vector3.one;
                        newObj.transform.Find("Text").GetComponent<Text>().text = LocalizeManager.GetLocalize(aDatas[i].ID + "_cont2");
                        string _id = aDatas[i].ID;
                        newObj.GetComponent<Button>().onClick.AddListener(() => { AchievementsClickEvent(_id, newObj); });
                        if (loadData._clear)
                        {
                            newObj.transform.Find("Image").GetComponent<Image>().sprite = checkOn;

                            if (aDatas[i].REWARD && !loadData._reward)
                            {
                                newObj.transform.Find("Image").Find("Gift").gameObject.SetActive(true);
                            }
                        }
                        else newObj.transform.Find("Image").GetComponent<Image>().sprite = checkOff;
                        SubAchievementsList.Add(newObj);
                    }
                    if (!categoryClear) continue;
                    if (loadData._id.Equals(string.Empty))
                    {
                    }
                    else if (!loadData._clear)
                    {
                        int max = 0;
                        int cur = 0;
                        if (aDatas[i].ID.Contains("endlesschallenges_"))
                        {
                            cur = loadData._cur / 60;
                            max = aDatas[i].MAX;
                        }
                        else
                        {
                            cur = loadData._cur;
                            if (loadData._id.Contains("soulmate") || loadData._id.Contains("researchaddiction"))
                                max = loadData._max;
                            else
                                max = aDatas[i].MAX;
                        }
                        if (lastClear)
                        {
                            lastClear = false;
                            AchievementsList[c].transform.Find("Graph").Find("Graph").GetComponent<Image>().fillAmount = (float)cur / (float)max;
                            AchievementsList[c].transform.Find("Graph").Find("Text").GetComponent<Text>().text = cur + "/" + max;
                            AchievementsList[c].transform.Find("Graph").Find("Text").GetComponent<Text>().text += "\n" + LocalizeManager.GetLocalize(aDatas[i].ID + "_cont2");
                        }
                    }
                    else
                    {
                        if (!lastClear) lastClear = true;
                        int max = 0;
                        int cur = 0;
                        if (aDatas[i].ID.Contains("endlesschallenges_"))
                        {
                            cur = loadData._cur / 60;
                            max = aDatas[i].MAX;
                        }
                        else
                        {
                            cur = loadData._cur;
                            if (loadData._id.Contains("soulmate") || loadData._id.Contains("researchaddiction"))
                                max = loadData._max;
                            else
                                max = aDatas[i].MAX;
                        }
                        curScore += aDatas[i].SCORE;
                        AchievementsList[c].transform.Find("Graph").Find("Graph").GetComponent<Image>().fillAmount = 1;
                        AchievementsList[c].transform.Find("Graph").Find("Text").GetComponent<Text>().text = cur + "/" + max;
                        AchievementsList[c].transform.Find("Graph").Find("Text").GetComponent<Text>().text += "\n" + LocalizeManager.GetLocalize(aDatas[i].ID + "_cont2");
                    }
                }
            }

            GameObject lineObj2 = new GameObject();
            lineObj2.name = "LineObj";
            Text text2 = lineObj2.AddComponent<Text>();
            text2.alignment = TextAnchor.MiddleCenter;
            text2.fontSize = 24;
            text2.font = _font;
            text2.color = new Color32(0, 79, 19, 255);
            text2.verticalOverflow = VerticalWrapMode.Overflow;
            text2.horizontalOverflow = HorizontalWrapMode.Overflow;
            text2.text = "\n\n\n---------------------------------------------------------------------";
            text2.raycastTarget = false;
            lineObj2.transform.SetParent(AchievementsParant);
            lineObj2.transform.localScale = Vector3.one;
            LineList.Add(lineObj2);
            AchievementsTotal.text = LocalizeManager.GetLocalize("memorial_achievements_score") + " (" + curScore + "/" + maxScore + ")";
            AchievementsObj.SetActive(true);
            StartCoroutine(RectReSize());
        }
    }
    GameObject clickObj = null;
    string clickId = string.Empty;
    public void AchievementsClickEvent(string _id, GameObject _obj)
    {
        clickObj = _obj;
        clickId = _id;
        AchievementsData aData = System.Array.Find(SheetManager.Instance.AchievementsDB.dataArray, item => item.ID.Equals(_id));
        popupObj.transform.Find("Image").GetComponent<Image>().sprite = Resources.Load<Sprite>("Achievements/" + _id);
        popupObj.transform.Find("Score").GetComponent<Text>().text = aData.SCORE.ToString();
        popupObj.transform.Find("Title").GetComponent<Text>().text = LocalizeManager.GetLocalize(aData.ID);
        popupObj.transform.Find("Cont").GetComponent<Text>().text = LocalizeManager.GetLocalize(aData.ID + "_cont");
        AchievementData loadData = SaveAndLoadManager.instance.LoadAchievements(_id);
        if (loadData._clear) 
        {
            popupObj.GetComponent<Image>().material = null;
            popupObj.transform.Find("Image").GetComponent<Image>().material = null;
            popupObj.transform.Find("Graph").Find("Graph").GetComponent<Image>().material = null;
            string year = loadData._date.Substring(2, 2);
            string month = loadData._date.Substring(4, 2);
            string day = loadData._date.Substring(6, 2);
            string hour = loadData._date.Substring(8, 2);
            string minute = loadData._date.Substring(10, 2);
            string second = loadData._date.Substring(12, 2);
            popupObj.transform.Find("Clear").GetComponent<Text>().text = year + "." + month + "." + day + " " + hour + ":" + minute + ":" + second;
        }
        else
        {
            popupObj.GetComponent<Image>().material = DontClearMaterial;
            popupObj.transform.Find("Image").GetComponent<Image>().material = DontClearMaterial;
            popupObj.transform.Find("Graph").Find("Graph").GetComponent<Image>().material = DontClearMaterial;
            popupObj.transform.Find("Clear").GetComponent<Text>().text = LocalizeManager.GetLocalize("dontclear");
        }

        int cur = loadData._cur;
        int max = aData.MAX;
        if(loadData._id.Contains("endlesschallenges"))
            cur /= 60;
        else if (loadData._id.Contains("soulmate") || loadData._id.Contains("researchaddiction"))
            max = loadData._max;
        popupObj.transform.Find("Graph").Find("Graph").GetComponent<Image>().fillAmount = (float)cur / (float)max;
        popupObj.transform.Find("Graph").Find("Text").GetComponent<Text>().text = cur + "/" + max;
        popupObj.transform.Find("Reward").Find("Reward").GetComponent<Button>().onClick.RemoveAllListeners();
        if (aData.REWARD)
        {
            popupObj.transform.Find("Reward").GetComponent<Text>().text = LocalizeManager.GetLocalize(_id + "_reward");
            if (loadData._clear)
            {
                if (!loadData._reward)
                {
                    popupObj.transform.Find("Reward").Find("Reward").gameObject.SetActive(true);
                    string dddd = _id;
                    GameObject obj = popupObj.transform.Find("Reward").Find("Reward").gameObject;
                    popupObj.transform.Find("Reward").Find("Reward").GetComponent<Button>().onClick.AddListener(() => { Reward(dddd, obj); });
                }
                else
                    popupObj.transform.Find("Reward").Find("Reward").gameObject.SetActive(false);
            }
            else
                popupObj.transform.Find("Reward").Find("Reward").gameObject.SetActive(false);
        }
        else
        {
            popupObj.transform.Find("Reward").GetComponent<Text>().text = "";
            popupObj.transform.Find("Reward").Find("Reward").gameObject.SetActive(false);
        }
        popupObj.transform.position = new Vector3(popupObj.transform.position.x, _obj.transform.position.y + 30, 0);
        popupObj.SetActive(true);
    }
    public void Reward(string _id, GameObject _obj)
    {
        int plusCount = 0;
        if (_id.Equals("endlesschallenges_1"))
        {
            plusCount = 100;
        }
        else if (_id.Equals("thiefmaster_0"))
        {
            plusCount = 50;
        }
        else if (_id.Equals("openedbox_0"))
        {
            plusCount = 50;
        }
        else if (_id.Equals("openedbox_1"))
        {
            plusCount = 100;
        }
        else if (_id.Equals("openedbox_2"))
        {
            plusCount = 100;
        }
        else if (_id.Equals("openedbox_3"))
        {
            plusCount = 100;
        }
        else if (_id.Equals("openedbox_4"))
        {
            plusCount = 500;
        }
        else if (_id.Equals("openedbox_5"))
        {
            plusCount = 1000;
        }
        else if (_id.Equals("stage_clear_0"))
        {
            plusCount = 50;
        }

        SoundManager.instance.StartAudio(new string[1] { "Object/coin" }, VOLUME_TYPE.EFFECT);
        StateGroup.instance.nMutantCell += plusCount;
        Inventorys.Instance.glowindskull += plusCount;
        ResultPanelScript.instance.skullCount += plusCount;
        if (SteamStatsAndAchievements.instance != null) SteamStatsAndAchievements.instance.SettingStat("stat_glowing_total_count", plusCount);
        if (SteamStatsAndAchievements.instance != null) SteamStatsAndAchievements.instance.SettingStat("stage_glowing_max_count", Inventorys.Instance.glowindskull, true);
        SaveAndLoadManager.instance.SaveItem(new ItemSave
        {
            _id = "glowing_skull",
            _nCount = plusCount
        });

        AchievementData loadData = SaveAndLoadManager.instance.LoadAchievements(_id);
        loadData._reward = true;
        SaveAndLoadManager.instance.SaveAchievements(new AchievementData[1] { loadData }, true);
        _obj.SetActive(false);
        (FindObjectOfType(typeof(MemorialManController)) as MemorialManController).CheckReward();
        AchievementsClickEvent(clickId, clickObj);
    }
}
