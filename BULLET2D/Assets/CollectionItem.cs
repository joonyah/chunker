﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CollectionItem : MonoBehaviour
{
    public Button _btn;
    public Image _image;
    public bool isLeft = false;
    [SerializeField] private Material _grayMaterial;
    public void InitSetting(string _id, bool _isWeapon, bool _isGet, bool _isLeft)
    {
        isLeft = _isLeft;
        _btn.onClick.RemoveAllListeners();
        Sprite _sprite = null;
        if (_isWeapon)
        {
            ItemsData iData = System.Array.Find(SheetManager.Instance.ItemsDB.dataArray, item => item.ID.Equals(_id));
            _sprite = Resources.Load<Sprite>("Item/" + iData.Type1 + "/" + iData.Type2 + "/" + iData.ID);
        }
        else
        {
            MonstersData mData = System.Array.Find(SheetManager.Instance.MonsterDB.dataArray, item => item.ID.Equals(_id));
            _sprite = Resources.Load<Sprite>("Mob/" + mData.ID + "/" + mData.ID);
        }
        if (_sprite != null)
        {
            _image.sprite = _sprite;
            if (!_isGet)
            {
                ColorBlock c = new ColorBlock
                {
                    normalColor = new Color(1, 1, 1, 0.5f),
                    highlightedColor = new Color32(245, 245, 245, 255),
                    pressedColor = new Color32(200, 200, 200, 255),
                    selectedColor = new Color32(245, 245, 245, 255),
                    disabledColor = new Color32(200, 200, 200, 128),
                    colorMultiplier = 1f,
                    fadeDuration = 0.1f
                };
                _btn.colors = c;
                _image.color = new Color(1, 1, 1, 0.5f);
                _image.material = _grayMaterial;
            }
            else
            {
                ColorBlock c = new ColorBlock
                {
                    normalColor = new Color(1, 1, 1, 1),
                    highlightedColor = new Color32(245, 245, 245, 255),
                    pressedColor = new Color32(200, 200, 200, 255),
                    selectedColor = new Color32(245, 245, 245, 255),
                    disabledColor = new Color32(200, 200, 200, 128),
                    colorMultiplier = 1f,
                    fadeDuration = 0.1f
                };
                _btn.colors = c;
                _image.color = new Color(1, 1, 1, 1);
                _image.material = null;
            }
        }
        _image.GetComponent<RectTransform>().sizeDelta = new Vector2(60, 60);
        string id = _id;
        bool isGet = _isGet;
        bool isWeapon = _isWeapon;
        _btn.onClick.AddListener(() => { CollectionManager.instance.ClickEvent(id, isWeapon, isGet, _sprite); });
    }
}
