﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EquipmentUpgradeManager : MonoBehaviour
{
    public static EquipmentUpgradeManager instance;
    [SerializeField] private GameObject openObj;
    public bool isOpen = false;
    GameObject controllerObj = null;
    public bool isSubOpen = false;
    bool isManufacture = false;
    [SerializeField] private GameObject ManufactureObj;
    string[] FluidArray;
    string nowItemID = string.Empty;
    int nowFluid = 0;
    bool isInjection = false;
    bool isBossRush = false;
    [SerializeField] private Animator monitorAnim;
    [SerializeField] private Animator[] slotAnim;
    [SerializeField] private GameObject guageObj;
    [SerializeField] private GameObject guageObjOff;
    [SerializeField] private Image spakeImage;
    [SerializeField] private Image guageImage;
    bool isUse = false;
    bool isEnhance = false;
    ItemsData nowItemData;
    [SerializeField] private Text[] fludeCountTexts;
    private void Awake()
    {
        if (instance == null) instance = this;
    }
    void Start()
    {
        spakeImage = GameObject.Find("Canvas").transform.Find("Spake").GetComponent<Image>();
    }
    void Update()
    {
        if (isOpen && !isSubOpen) 
        {
            if (!isManufacture && !isUse)
            {
                if (Input.GetKeyDown(KeyCode.A) || PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_SLIDE], true) && PDG.Dungeon.instance.closeCoolTime > 0.2f)
                {
                    isSubOpen = true;
                    MakeController.instance.ShowMake(string.Empty, "equipment" + (isBossRush ? "2" : string.Empty), controllerObj);
                }
                if (Input.GetKeyDown(KeyCode.D) || PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_SKILL], true) && PDG.Dungeon.instance.closeCoolTime > 0.2f)
                {
                    isSubOpen = true;
                    MakeController.instance.ShowMake(string.Empty, "manufacture", controllerObj);
                }
            }
            else if (!isUse)
            {
                float x = Input.GetAxisRaw("Horizontal_joy");
                float y = Input.GetAxisRaw("Vertical_joy");
                if ((PDG.Dungeon.KeyboardInputCheck(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ACTIVE], true) || PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_CONFIRM], true)) && PDG.Dungeon.instance.closeCoolTime > 0.2f)
                {
                    PDG.Dungeon.instance.closeCoolTime = 0.0f;
                    StartInjection(FluidArray[nowFluid]);
                }
                if ((PDG.Dungeon.KeyboardInputCheck(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ARROW_LEFT], true) || x < 0) && PDG.Dungeon.instance.closeCoolTime > 0.2f)
                {
                    nowFluid--;
                    if (FluidArray.Length > 0)
                    {
                        if (nowFluid < 0) nowFluid = FluidArray.Length - 1;
                        SettingManufacture(FluidArray[nowFluid]);
                    }
                }
                if ((PDG.Dungeon.KeyboardInputCheck(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ARROW_RIGHT], true) || x > 0) && PDG.Dungeon.instance.closeCoolTime > 0.2f)
                {
                    nowFluid++;
                    if (FluidArray.Length > 0)
                    {
                        if (nowFluid >= FluidArray.Length) nowFluid = 0;
                        SettingManufacture(FluidArray[nowFluid]);
                    }
                }
            }
            if (!isManufacture && !isUse && (Input.GetKeyDown(KeyCode.Escape) || PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_CANCLE], true)) && PDG.Dungeon.instance.closeCoolTime > 0.2f)
            {
                PDG.Dungeon.instance.closeCoolTime = 0.0f;
                Close();
            }
            if (isManufacture && !isUse && (Input.GetKeyDown(KeyCode.Escape) || PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_CANCLE], true)) && PDG.Dungeon.instance.closeCoolTime > 0.2f)
            {
                PDG.Dungeon.instance.closeCoolTime = 0.0f;
                isManufacture = false;
                isSubOpen = true;
                MakeController.instance.ShowMake(string.Empty, "manufacture", controllerObj);
                ManufactureObj.SetActive(false);
            }
            if (isUse && (Input.GetKeyDown(KeyCode.Escape) || PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_CANCLE], true)) && PDG.Dungeon.instance.closeCoolTime > 0.2f)
            {
                PDG.Dungeon.instance.closeCoolTime = 0.0f;
                isUse = false;
            }
        }
    }
    public void Show(GameObject _obj, bool _isBossRush)
    {
        isBossRush = _isBossRush;
        controllerObj = _obj;
        isOpen = true;
        openObj.SetActive(isOpen);
    }
    void Close()
    {
        isOpen = false;
        openObj.SetActive(isOpen);
    }
    public void ShowManufacture(ItemsData makeItem, string fulidID = "")
    {

        int c = 0;
        {
               c = SaveAndLoadManager.instance.LoadItem("fluid_attack" + (isBossRush ? "2" : string.Empty));
            fludeCountTexts[0].color = Color.white;
            if (c == -1)
                fludeCountTexts[0].text = "0";
            else if (c == 0)
                fludeCountTexts[0].text = "0";
            else
            {
                fludeCountTexts[0].color = Color.green;
                fludeCountTexts[0].text = c.ToString();
            }
        }
        {
            c = SaveAndLoadManager.instance.LoadItem("fluid_aim" + (isBossRush ? "2" : string.Empty));

            fludeCountTexts[1].color = Color.white;
            if (c == -1)
                fludeCountTexts[1].text = "0";
            else if (c == 0)
                fludeCountTexts[1].text = "0";
            else
            {
                fludeCountTexts[1].color = Color.green;
                fludeCountTexts[1].text = c.ToString();
            }
        }
        {
            c = SaveAndLoadManager.instance.LoadItem("fluid_range" + (isBossRush ? "2" : string.Empty));

            fludeCountTexts[2].color = Color.white;
            if (c == -1)
                fludeCountTexts[2].text = "0";
            else if (c == 0)
                fludeCountTexts[2].text = "0";
            else
            {
                fludeCountTexts[2].color = Color.green;
                fludeCountTexts[2].text = c.ToString();
            }
        }
        {
            c = SaveAndLoadManager.instance.LoadItem("fluid_critical" + (isBossRush ? "2" : string.Empty));

            fludeCountTexts[3].color = Color.white;
            if (c == -1)
                fludeCountTexts[3].text = "0";
            else if (c == 0)
                fludeCountTexts[3].text = "0";
            else
            {
                fludeCountTexts[3].color = Color.green;
                fludeCountTexts[3].text = c.ToString();
            }
        }
        {
            c = SaveAndLoadManager.instance.LoadItem("fluid_multiShoot" + (isBossRush ? "2" : string.Empty));

            fludeCountTexts[4].color = Color.white;
            if (c == -1)
                fludeCountTexts[4].text = "0";
            else if (c == 0)
                fludeCountTexts[4].text = "0";
            else
            {
                fludeCountTexts[4].color = Color.green;
                fludeCountTexts[4].text = c.ToString();
            }
        }
        {
            c = SaveAndLoadManager.instance.LoadItem("fluid_shootCount" + (isBossRush ? "2" : string.Empty));

            fludeCountTexts[5].color = Color.white;
            if (c == -1)
                fludeCountTexts[5].text = "0";
            else if (c == 0)
                fludeCountTexts[5].text = "0";
            else
            {
                fludeCountTexts[5].color = Color.green;
                fludeCountTexts[5].text = c.ToString();
            }
        }
        {
            c = SaveAndLoadManager.instance.LoadItem("fluid_shootDelay" + (isBossRush ? "2" : string.Empty));

            fludeCountTexts[6].color = Color.white;
            if (c == -1)
                fludeCountTexts[6].text = "0";
            else if (c == 0)
                fludeCountTexts[6].text = "0";
            else
            {
                fludeCountTexts[6].color = Color.green;
                fludeCountTexts[6].text = c.ToString();
            }
        }
        {
            c = SaveAndLoadManager.instance.LoadItem("fluid_criticalDamage" + (isBossRush ? "2" : string.Empty));

            fludeCountTexts[7].color = Color.white;
            if (c == -1)
                fludeCountTexts[7].text = "0";
            else if (c == 0)
                fludeCountTexts[7].text = "0";
            else
            {
                fludeCountTexts[7].color = Color.green;
                fludeCountTexts[7].text = c.ToString();
            }
        }

        string tempID = makeItem.ID;
        int ttt = tempID.LastIndexOf('_');
        if (ttt != -1 && ttt == tempID.Length - 2)
        {
            tempID = tempID.Substring(0, tempID.Length - 2);
        }
        isManufacture = true;
        string path = "Item/" + makeItem.Type1 + "/" + makeItem.Type2 + "/" + makeItem.ID;
        ManufactureObj.transform.Find("Item").GetComponent<Image>().sprite = Resources.Load<Sprite>(path);
        nowItemID = tempID;
        nowItemData = makeItem;
        isInjection = false;
        if (fulidID.Equals(""))
        {
            nowFluid = 0;
            SettingManufacture();
        }
        else
            SettingManufacture(fulidID);
        ManufactureObj.SetActive(true);
    }
    void SettingManufacture(string fulidID = "")
    {
        ManufacturesData loadData = SaveAndLoadManager.instance.LoadManufacture(nowItemID, PDG.Dungeon.instance.isBossRush);
        MakingStateController stateController = ManufactureObj.transform.Find("Icon").GetComponent<MakingStateController>();
        stateController.InitSetting();
        if (!loadData.id.Equals(string.Empty) || !loadData.id.Equals(""))
        {
            for (int i = 0; i < loadData.stateid.Count; i++)
            {
                if (loadData.stateid[i].ToUpper().Equals("Attack".ToUpper()))
                {
                    stateController.StateTexts[0].color = Color.green;
                    stateController.StateTexts[0].text = "+" + loadData.statevalue[i];
                }
                else if (loadData.stateid[i].ToUpper().Equals("Critical".ToUpper()))
                {
                    stateController.StateTexts[1].color = Color.green;
                    stateController.StateTexts[1].text = "+" + loadData.statevalue[i];
                }
                else if (loadData.stateid[i].ToUpper().Equals("CriticalDamage".ToUpper()))
                {
                    stateController.StateTexts[2].color = Color.green;
                    stateController.StateTexts[2].text = "+" + loadData.statevalue[i];
                }
                else if (loadData.stateid[i].ToUpper().Equals("Range".ToUpper()))
                {
                    stateController.StateTexts[3].color = Color.green;
                    stateController.StateTexts[3].text = "+" + loadData.statevalue[i];
                }
                else if (loadData.stateid[i].ToUpper().Equals("Aim".ToUpper()))
                {
                    stateController.StateTexts[4].color = Color.green;
                    stateController.StateTexts[4].text = "+" + loadData.statevalue[i];
                }
                else if (loadData.stateid[i].ToUpper().Equals("MultiShoot".ToUpper()))
                {
                    stateController.StateTexts[5].color = Color.green;
                    stateController.StateTexts[5].text = "+" + loadData.statevalue[i];
                }
                else if (loadData.stateid[i].ToUpper().Equals("ShootCount".ToUpper()))
                {
                    stateController.StateTexts[6].color = Color.green;
                    stateController.StateTexts[6].text = "+" + loadData.statevalue[i];
                }
                else if (loadData.stateid[i].ToUpper().Equals("ShootDelay".ToUpper()))
                {
                    stateController.StateTexts[7].color = Color.green;
                    stateController.StateTexts[7].text = "-" + (loadData.statevalue[i] / 100.0f);
                }
            }
        }
        Sprite[] materialSprites = Resources.LoadAll<Sprite>("Item/Material/fluid");
        bool checkMaterial = false;
        string fluidName = string.Empty;
        if (fulidID.Equals(""))
        {
            List<string> tempList = new List<string>();
            for (int i = 0; i < materialSprites.Length; i++)
            {
                if(isBossRush)
                {
                    if (!materialSprites[i].name.Contains("2")) continue;
                }
                else
                {
                    if (materialSprites[i].name.Contains("2")) continue;
                }
                int c = SaveAndLoadManager.instance.LoadItem("fluid_" + materialSprites[i].name);
                if (c > 0)
                {
                    ManufactureObj.transform.Find("Slot").transform.Find("Slot_0").Find("Text").GetComponent<Text>().text = LocalizeManager.GetLocalize("fluid_" + materialSprites[i].name);
                    ManufactureObj.transform.Find("Slot").transform.Find("Slot_0").gameObject.SetActive(true);
                    checkMaterial = true;
                    fluidName = materialSprites[i].name;
                    tempList.Add(fluidName);
                }
            }
            FluidArray = tempList.ToArray();
            nowFluid = FluidArray.Length - 1;
        }
        else
        {
            int c = SaveAndLoadManager.instance.LoadItem("fluid_" + fulidID);
            ManufactureObj.transform.Find("Slot").transform.Find("Slot_0").Find("Text").GetComponent<Text>().text = LocalizeManager.GetLocalize("fluid_" + fulidID);
            ManufactureObj.transform.Find("Slot").transform.Find("Slot_0").gameObject.SetActive(true);
            fluidName = fulidID;
            if (c > 0)
                checkMaterial = true;
            else
                checkMaterial = false;
        }
        if (!checkMaterial)
        {
            ManufactureObj.transform.Find("Slot").transform.Find("Slot_0").gameObject.SetActive(false);
            ManufactureObj.transform.Find("Slot").transform.Find("Slot_1").gameObject.SetActive(false);
            ManufactureObj.transform.Find("Slot").transform.Find("Slot_2").gameObject.SetActive(false);
            ManufactureObj.transform.Find("Slot").transform.Find("Text").gameObject.SetActive(true);
            ManufactureObj.transform.Find("TextGroup").gameObject.SetActive(false);
        }
        else
        {

            ManufactureObj.transform.Find("Slot").transform.Find("Text").gameObject.SetActive(false);
            ManufactureObj.transform.Find("TextGroup").Find("fluid").GetComponent<Text>().text = LocalizeManager.GetLocalize("fluid_" + fluidName);
            string fluidName2 = fluidName;
            int tt = fluidName.IndexOf("2");
            if (tt > 0) fluidName2 = fluidName2.Substring(0, fluidName.Length - 1);
            EnhanceData eData = System.Array.Find(SheetManager.Instance.EnhanceDB.dataArray, item => item.ID.Equals(nowItemID));
            if (fluidName2.ToUpper().Equals("Attack".ToUpper()) && !eData.Attack)
            {
                isEnhance = false;
                ManufactureObj.transform.Find("TextGroup").gameObject.SetActive(true);
                ManufactureObj.transform.Find("TextGroup").Find("per").gameObject.SetActive(false);
                ManufactureObj.transform.Find("TextGroup").Find("warning").gameObject.SetActive(false);
                ManufactureObj.transform.Find("TextGroup").Find("warning2").gameObject.SetActive(true);
                return;
            }
            else if (fluidName2.ToUpper().Equals("Critical".ToUpper()) && !eData.Critical)
            {
                isEnhance = false;
                ManufactureObj.transform.Find("TextGroup").gameObject.SetActive(true);
                ManufactureObj.transform.Find("TextGroup").Find("per").gameObject.SetActive(false);
                ManufactureObj.transform.Find("TextGroup").Find("warning").gameObject.SetActive(false);
                ManufactureObj.transform.Find("TextGroup").Find("warning2").gameObject.SetActive(true);
                return;
            }
            else if (fluidName2.ToUpper().Equals("CriticalDamage".ToUpper()) && !eData.Criticaldamage)
            {
                isEnhance = false;
                ManufactureObj.transform.Find("TextGroup").gameObject.SetActive(true);
                ManufactureObj.transform.Find("TextGroup").Find("per").gameObject.SetActive(false);
                ManufactureObj.transform.Find("TextGroup").Find("warning").gameObject.SetActive(false);
                ManufactureObj.transform.Find("TextGroup").Find("warning2").gameObject.SetActive(true);
                return;
            }
            else if (fluidName2.ToUpper().Equals("Range".ToUpper()) && !eData.Range)
            {
                isEnhance = false;
                ManufactureObj.transform.Find("TextGroup").gameObject.SetActive(true);
                ManufactureObj.transform.Find("TextGroup").Find("per").gameObject.SetActive(false);
                ManufactureObj.transform.Find("TextGroup").Find("warning").gameObject.SetActive(false);
                ManufactureObj.transform.Find("TextGroup").Find("warning2").gameObject.SetActive(true);
                return;
            }
            else if (fluidName2.ToUpper().Equals("ShootCount".ToUpper()) && !eData.Shootcount)
            {
                isEnhance = false;
                ManufactureObj.transform.Find("TextGroup").gameObject.SetActive(true);
                ManufactureObj.transform.Find("TextGroup").Find("per").gameObject.SetActive(false);
                ManufactureObj.transform.Find("TextGroup").Find("warning").gameObject.SetActive(false);
                ManufactureObj.transform.Find("TextGroup").Find("warning2").gameObject.SetActive(true);
                return;
            }
            else if (fluidName2.ToUpper().Equals("MultiShoot".ToUpper()) && !eData.Multishoot)
            {
                isEnhance = false;
                ManufactureObj.transform.Find("TextGroup").gameObject.SetActive(true);
                ManufactureObj.transform.Find("TextGroup").Find("per").gameObject.SetActive(false);
                ManufactureObj.transform.Find("TextGroup").Find("warning").gameObject.SetActive(false);
                ManufactureObj.transform.Find("TextGroup").Find("warning2").gameObject.SetActive(true);
                return;
            }
            else if (fluidName2.ToUpper().Equals("ShootDelay".ToUpper()) && !eData.Shootdelay)
            {
                isEnhance = false;
                ManufactureObj.transform.Find("TextGroup").gameObject.SetActive(true);
                ManufactureObj.transform.Find("TextGroup").Find("per").gameObject.SetActive(false);
                ManufactureObj.transform.Find("TextGroup").Find("warning").gameObject.SetActive(false);
                ManufactureObj.transform.Find("TextGroup").Find("warning2").gameObject.SetActive(true);
                return;
            }
            else if (fluidName2.ToUpper().Equals("Aim".ToUpper()) && !eData.Aim)
            {
                isEnhance = false;
                ManufactureObj.transform.Find("TextGroup").gameObject.SetActive(true);
                ManufactureObj.transform.Find("TextGroup").Find("per").gameObject.SetActive(false);
                ManufactureObj.transform.Find("TextGroup").Find("warning").gameObject.SetActive(false);
                ManufactureObj.transform.Find("TextGroup").Find("warning2").gameObject.SetActive(true);
                return;
            }
            else
            {
                isEnhance = true;
                ManufactureObj.transform.Find("TextGroup").Find("per").gameObject.SetActive(true);
                ManufactureObj.transform.Find("TextGroup").Find("warning").gameObject.SetActive(true);
                ManufactureObj.transform.Find("TextGroup").Find("warning2").gameObject.SetActive(false);
            }
            int fluidStep = 0;
            if (loadData.stateid != null && loadData.stateid.Count > 0)
            {
                for (int i = 0; i < loadData.stateid.Count; i++)
                {
                    if (loadData.stateid[i].ToUpper().Equals(fluidName2.ToUpper()))
                    {
                        fluidStep = loadData.statestep[i];
                    }
                }
            }
            int mDataIndex = System.Array.FindIndex(SheetManager.Instance.ManufactureDB.dataArray, item => item.ID.ToUpper().Equals(fluidName2.ToUpper()) && item.Stepby == (fluidStep + 1));
            if (mDataIndex != -1)
            {
                ManufactureData mData = SheetManager.Instance.ManufactureDB.dataArray[mDataIndex];
                ManufactureObj.transform.Find("TextGroup").Find("per").GetComponent<Text>().text = LocalizeManager.GetLocalize("ui_manufacture_material_probability")
                                                                                                    + "<color=#FFFF00>" + Mathf.FloorToInt(mData.Percentage * 100) + "%</color>";
                ManufactureObj.transform.Find("TextGroup").Find("warning").gameObject.SetActive(true);

                if (mData.ID.Equals("Attack"))
                {
                    stateController.StateTexts[0].text += "<color=#FFFF00> +" + mData.Amount + "</color>";
                }
                else if (mData.ID.Equals("Critical"))
                {
                    stateController.StateTexts[1].text += "<color=#FFFF00> +" + mData.Amount + "</color>";
                }
                else if (mData.ID.Equals("CriticalDamage"))
                {
                    stateController.StateTexts[2].text += "<color=#FFFF00> +" + mData.Amount + "</color>";
                }
                else if (mData.ID.Equals("Range"))
                {
                    stateController.StateTexts[3].text += "<color=#FFFF00> +" + mData.Amount + "</color>";
                }
                else if (mData.ID.Equals("Aim"))
                {
                    stateController.StateTexts[4].text += "<color=#FFFF00> +" + mData.Amount + "</color>";
                }
                else if (mData.ID.Equals("MultiShoot"))
                {
                    stateController.StateTexts[5].text += "<color=#FFFF00> +" + mData.Amount + "</color>";
                }
                else if (mData.ID.Equals("ShootCount"))
                {
                    stateController.StateTexts[6].text += "<color=#FFFF00> +" + mData.Amount + "</color>";
                }
                else if (mData.ID.Equals("ShootDelay"))
                {
                    stateController.StateTexts[7].text += "<color=#FFFF00> -" + (mData.Amount / 100.0f) + "</color>";
                }
            }
            else
            {
                ManufactureObj.transform.Find("TextGroup").Find("per").GetComponent<Text>().text = "<color=#FF0000>" + LocalizeManager.GetLocalize("ui_manufacture_material_max") + "</color>";
                ManufactureObj.transform.Find("TextGroup").Find("warning").gameObject.SetActive(false);
            }
            ManufactureObj.transform.Find("TextGroup").gameObject.SetActive(true);
        }
    }
    void StartInjection(string _fluidName)
    {
        if (!isEnhance) return;

        string fluidName2 = _fluidName;
        int tt = fluidName2.IndexOf("2");
        if (tt > 0) fluidName2 = fluidName2.Substring(0, fluidName2.Length - 1);

        int c = SaveAndLoadManager.instance.LoadItem("fluid_" + _fluidName);
        if (c > 0)
        {
            ManufacturesData loadData = SaveAndLoadManager.instance.LoadManufacture(nowItemID, PDG.Dungeon.instance.isBossRush);
            int fluidStep = 0;
            if (loadData.stateid != null && loadData.stateid.Count > 0)
            {
                for (int i = 0; i < loadData.stateid.Count; i++)
                {
                    if (loadData.stateid[i].ToUpper().Equals(fluidName2.ToUpper()))
                    {
                        fluidStep = loadData.statestep[i];
                    }
                }
            }
            int mDataIndex = System.Array.FindIndex(SheetManager.Instance.ManufactureDB.dataArray, item => item.ID.ToUpper().Equals(fluidName2.ToUpper()) && item.Stepby == (fluidStep + 1));
            if (mDataIndex != -1)
            {
                if (isInjection) return;
                isInjection = true;
                StartCoroutine(Injection(_fluidName));
            }
        }
    }
    IEnumerator Injection(string _fluidName)
    {
        isUse = true;
        float curTime = 0f;
        float maxTime = 3.0f;
        monitorAnim.enabled = true;
        slotAnim[0].enabled = true;
        slotAnim[1].enabled = true;
        slotAnim[2].enabled = true;
        guageObj.SetActive(true);
        guageObjOff.SetActive(false);
        while (isUse)
        {
            curTime += Time.deltaTime;
            guageImage.fillAmount = curTime / maxTime;
            if (curTime >= maxTime) break;
            yield return new WaitForFixedUpdate();
        }

        if (isUse)
        {
            yield return StartCoroutine(Spake());
            int c = SaveAndLoadManager.instance.LoadItem("fluid_" + _fluidName);
            int c2 = 1;
            int calc = c - c2;
            if (calc == 0)
                SaveAndLoadManager.instance.DeleteItem("fluid_" + _fluidName);
            else
                SaveAndLoadManager.instance.SaveItem(new ItemSave { _id = "fluid_" + _fluidName, _nCount = calc }, true);
            ManufacturesData loadData = SaveAndLoadManager.instance.LoadManufacture(nowItemID, PDG.Dungeon.instance.isBossRush);
            int fluidStep = 0;
            if (loadData.stateid != null && loadData.stateid.Count > 0)
            {
                for (int i = 0; i < loadData.stateid.Count; i++)
                {
                    if (loadData.stateid[i].ToUpper().Equals(_fluidName.ToUpper()))
                    {
                        fluidStep = loadData.statestep[i];
                        break;
                    }
                }
            }
            string fluidName2 = _fluidName;
            int tt = fluidName2.IndexOf("2");
            if (tt > 0) fluidName2 = fluidName2.Substring(0, fluidName2.Length - 1);
            int mDataIndex = System.Array.FindIndex(SheetManager.Instance.ManufactureDB.dataArray, item => item.ID.ToUpper().Equals(fluidName2.ToUpper()) && item.Stepby == (fluidStep + 1));
            if (mDataIndex != -1)
            {
                ManufactureData mData = SheetManager.Instance.ManufactureDB.dataArray[mDataIndex];
                int success = 0;
                if (isBossRush)
                    success = 1;
                else
                    success = GetPercentege(mData.Percentage);
                if (success == 1)
                {
                    SaveAndLoadManager.instance.SaveManufacture(nowItemID, (fluidStep + 1), fluidName2, mData.Amount, isBossRush);
                    SoundManager.instance.StartAudio(new string[1] { "ManufactureSuccess" }, VOLUME_TYPE.EFFECT);
                    ManufactureObj.transform.Find("TextGroup").Find("Result").gameObject.SetActive(true);
                    ManufactureObj.transform.Find("TextGroup").Find("Result").GetComponent<Text>().text = "<color=#00FF00>" + LocalizeManager.GetLocalize("ui_manufacture_material_success") + "</color>";
                    guageObj.SetActive(false);

                    ItemsData iData = System.Array.Find(SheetManager.Instance.ItemsDB.dataArray, item => item.ID.Equals(PDG.Player.instance.nowWeaponName));
                    PDG.Player.instance.SettingItem(iData);

                    ItemsData iData2 = System.Array.Find(SheetManager.Instance.ItemsDB.dataArray, item => item.ID.Equals(PDG.Player.instance.nowRightWeaponName));
                    PDG.Player.instance.SettingItem(iData2);

                    ItemsData iData3 = System.Array.Find(SheetManager.Instance.ItemsDB.dataArray, item => item.ID.Equals(PDG.Player.instance.qSkillName));
                    PDG.Player.instance.SettingItem(iData3);
                }
                else
                {
                    SoundManager.instance.StartAudio(new string[1] { "ManufactureFail" }, VOLUME_TYPE.EFFECT);
                    ManufactureObj.transform.Find("TextGroup").Find("Result").gameObject.SetActive(true);
                    ManufactureObj.transform.Find("TextGroup").Find("Result").GetComponent<Text>().text = "<color=#FF0000>" + LocalizeManager.GetLocalize("ui_manufacture_material_fail") + "</color>";
                    guageObj.SetActive(false);
                }
            }
        }
        bool checkMaterial = false;
        int c3 = SaveAndLoadManager.instance.LoadItem("fluid_" + _fluidName);
        if (c3 > 0) checkMaterial = true;
        if (!checkMaterial)
        {
            ManufactureObj.transform.Find("Slot").transform.Find("Slot_0").gameObject.SetActive(false);
            ManufactureObj.transform.Find("Slot").transform.Find("Slot_1").gameObject.SetActive(false);
            ManufactureObj.transform.Find("Slot").transform.Find("Slot_2").gameObject.SetActive(false);
            ManufactureObj.transform.Find("Slot").transform.Find("Text").gameObject.SetActive(true);
        }
        yield return new WaitForSeconds(1.5f);
        isUse = false;
        guageObj.SetActive(false);
        ManufactureObj.transform.Find("TextGroup").Find("Result").gameObject.SetActive(false);
        guageObjOff.SetActive(true);
        monitorAnim.enabled = false;
        slotAnim[0].enabled = false;
        slotAnim[1].enabled = false;
        slotAnim[2].enabled = false;
        slotAnim[0].GetComponent<Image>().sprite = defaultSlotSprite;
        slotAnim[1].GetComponent<Image>().sprite = defaultSlotSprite;
        slotAnim[2].GetComponent<Image>().sprite = defaultSlotSprite;
        ShowManufacture(nowItemData, _fluidName);
    }
    [SerializeField] private Sprite defaultSlotSprite;
    private int GetPercentege(float _per)
    {
        int[] per = new int[10000];
        for (int i = 0; i < per.Length; i++) per[i] = 0;
        int inPer = Mathf.FloorToInt(_per * 10000);
        int tCount = 0;
        while (true)
        {
            int r = Random.Range(0, 10000);
            if (per[r] != 1)
            {
                per[r] = 1;
                tCount++;
            }
            if (tCount >= inPer) break;
        }
        return per[Random.Range(0, 10000)];
    }
    IEnumerator Spake()
    {
        float a = 0.0f;
        while (true)
        {
            a += Time.deltaTime * 4;
            spakeImage.color = new Color(1, 1, 1, a);
            if (a > 1.0f) break;
            yield return new WaitForSeconds(Time.deltaTime);
        }
        spakeImage.color = new Color(1, 1, 1, 1);
        GameObject.Find("Camera").GetComponent<PDG.Camera>().isTestScreenClear = true;
        a = 1.0f;
        GameObject.Find("Camera").GetComponent<PDG.Camera>().isTestScreenClear = true;
        while (true)
        {
            a -= Time.deltaTime * 4;
            spakeImage.color = new Color(1, 1, 1, a);
            if (a <= 0.0f) break;
            yield return new WaitForSeconds(Time.deltaTime);
        }
        spakeImage.color = new Color(1, 1, 1, 0);
    }
}
