﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DifficultyController : MonoBehaviour
{
    [SerializeField] private Sprite fSprite;
    [SerializeField] private GameObject controllerObj;
    [SerializeField] private SpriteRenderer controllerSR;
    [SerializeField] private bool isSelect = false;
    [SerializeField] private Vector3 offSet;
    [SerializeField] private Vector3 size;

    [SerializeField] private GameObject tutorialPrefabs;
    private GameObject tutorialObj;

    [SerializeField] private int nowDifficulty = 0;
    // Start is called before the first frame update
    void Start()
    {
        int t2 = SaveAndLoadManager.instance.LoadItem("TUTORIAL_DIFFICULTY");
        if (t2 != 1)
        {
            tutorialObj = Instantiate(tutorialPrefabs);
            tutorialObj.transform.SetParent(GameObject.Find("Canvas").transform);
            tutorialObj.transform.localScale = Vector3.one;
            tutorialObj.transform.SetSiblingIndex(26);
        }
        Setting();
    }
    public void Setting()
    {
        nowDifficulty = SaveAndLoadManager.nDifficulty;
        GetComponent<Animator>().SetBool("easy", false);
        GetComponent<Animator>().SetBool("normal", false);
        GetComponent<Animator>().SetBool("hard", false);
        GetComponent<Animator>().SetBool("hell", false);
        if (nowDifficulty == 0) GetComponent<Animator>().SetBool("easy", true);
        else if (nowDifficulty == 1) GetComponent<Animator>().SetBool("normal", true);
        else if (nowDifficulty == 2) GetComponent<Animator>().SetBool("hard", true);
        else if (nowDifficulty == 3) GetComponent<Animator>().SetBool("hell", true);
    }
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireCube(transform.position + offSet, size);
    }
    // Update is called once per frame
    void Update()
    {
        if (tutorialObj)
        {
            tutorialObj.transform.Find("Text").GetComponent<UnityEngine.UI.Text>().text = LocalizeManager.GetLocalize("tutorial_difficulty");
            Vector3 pos = Camera.main.WorldToScreenPoint(controllerObj.transform.position);
            pos.y += 15;
            tutorialObj.transform.position = pos;
        }

        RaycastHit2D hit = Physics2D.BoxCast(transform.position + offSet, size, 0, Vector2.zero, 0, 1 << 9);

        if (hit && !isSelect)
        {
            isSelect = true;
            controllerObj.SetActive(true);
            controllerObj.GetComponent<SpriteRenderer>().sprite = PDG.Dungeon.instance.isJoystrick ? PDG.Dungeon.GetJoyStrickKeySprite(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_ACTIVE]) : PDG.Dungeon.GetJoyStrickKeySprite(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ACTIVE]);
            TextOpenController.instance.ShowText(LocalizeManager.GetLocalize("ajit_difficulty"), Color.white, 24, controllerObj, 2.0f, true);
        }
        else if (!hit)
        {
            isSelect = false;
            controllerObj.SetActive(false);
            if (TextOpenController.instance.targetObj == controllerObj) TextOpenController.instance.TextClose(Color.white);
        }

        if (isSelect)
        {
            if ((PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ACTIVE], true) || PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_ACTIVE], true)) && PDG.Dungeon.instance.closeCoolTime > 0.2f)
            {
                SaveAndLoadManager.instance.SaveItem(new ItemSave { _id = "TUTORIAL_DIFFICULTY", _nCount = 1 }, true);
                if (tutorialObj) Destroy(tutorialObj);
                PDG.Dungeon.instance.closeCoolTime = 0.0f;
                SoundManager.instance.StartAudio(new string[1] { "Click_Safe" }, VOLUME_TYPE.EFFECT);
                isSelect = false;
                controllerObj.SetActive(false);
                DifficultyManager.instance.ShowDifficulty(this);
            }
        }
    }
}
