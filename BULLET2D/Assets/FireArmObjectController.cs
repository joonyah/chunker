﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FireArmObjectController : MonoBehaviour
{
    [SerializeField] private GameObject controllerObj;
    [SerializeField] private SpriteRenderer controllerSR;
    [SerializeField] private bool isSelect = false;
    bool isOpen = false;
    private float delay = 0.0f;

    [SerializeField] private Vector3 vOffset;
    [SerializeField] private Vector3 vSize;
    [SerializeField] private string firstCommnet;
    [SerializeField] private string secondCommnet;
    [SerializeField] private AudioClip[] clips;
    [SerializeField] private AudioSource audio;
    [SerializeField] private Sprite noneSprite;

    private void Awake()
    {
        audio = GetComponent<AudioSource>();
        clips = Resources.LoadAll<AudioClip>("Object/Dungeon_2_Computer");
    }
    // Start is called before the first frame update
    void Start()
    {
        noneSprite = Resources.Load<Sprite>("NoneSprite");
    }

    // Update is called once per frame
    void Update()
    {
        delay -= Time.deltaTime;
        if (delay <= 0.0f) delay = 0.0f;

        RaycastHit2D hit = Physics2D.BoxCast(transform.position + vOffset, vSize, 0, Vector2.zero, 0, 1 << 9);
        if (hit && !isSelect && !isOpen && !TextOpenController.instance.isOpen && !PDG.Dungeon.instance.newObjectSelected) 
        {
            isSelect = true;
            PDG.Dungeon.instance.newObjectSelected = true;
            PDG.Dungeon.instance.newObjectSelectedObj = gameObject;
            controllerObj.SetActive(true);
            TextOpenController.instance.ShowText(LocalizeManager.GetLocalize(firstCommnet), Color.white, 24, controllerObj, 0.5f, true);
        }
        else if (hit && !isSelect && isOpen && !TextOpenController.instance.isOpen)
        {
            isSelect = true;
            controllerObj.SetActive(true);
            controllerObj.GetComponent<SpriteRenderer>().sprite = PDG.Dungeon.instance.isJoystrick ? PDG.Dungeon.GetJoyStrickKeySprite(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_ACTIVE]) : PDG.Dungeon.GetJoyStrickKeySprite(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ACTIVE]);
            TextOpenController.instance.ShowText(LocalizeManager.GetLocalize(secondCommnet), Color.white, 24, controllerObj, 0.5f, true);
        }
        else if (!hit)
        {
            if (PDG.Dungeon.instance.newObjectSelectedObj == gameObject)
            {
                PDG.Dungeon.instance.newObjectSelectedObj = null;
                PDG.Dungeon.instance.newObjectSelected = false;
            }
            isSelect = false;
            isOpen = false;
            audio.Stop();
            controllerObj.SetActive(false);
            if (TextOpenController.instance.targetObj == controllerObj) TextOpenController.instance.TextClose(Color.white);
        }

        if (isSelect)
        {
            if ((PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ACTIVE], true) || PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_ACTIVE], true)) && !isOpen && delay <= 0.0f)
            {
                SoundManager.instance.StartAudio(new string[1] { "Object/active_F" }, VOLUME_TYPE.EFFECT);
                isOpen = true;
                isSelect = false;
                controllerObj.SetActive(false);
                delay = 0.1f;
            }
            if ((PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ACTIVE], true) || PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_ACTIVE], true)) && isOpen && delay <= 0.0f)
            {
                SoundManager.instance.StartAudio(new string[1] { "Object/active_F" }, VOLUME_TYPE.EFFECT);
                isSelect = false;
                if (PDG.Dungeon.instance.newObjectSelectedObj == gameObject)
                {
                    PDG.Dungeon.instance.newObjectSelectedObj = null;
                    PDG.Dungeon.instance.newObjectSelected = false;
                }
                controllerObj.SetActive(false);
                delay = 0.1f;
                if (TextOpenController.instance.targetObj == controllerObj) TextOpenController.instance.TextClose(Color.white);
                ItemsData iData = System.Array.Find(SheetManager.Instance.ItemsDB.dataArray, item => item.ID.Equals("fireArm"));
                PDG.Player.instance.SettingItem(iData);
                SoundManager.instance.StartAudio(new string[1] { "Object/firearm_open" }, VOLUME_TYPE.EFFECT);
                Destroy(gameObject);
            }
        }
    }
}
