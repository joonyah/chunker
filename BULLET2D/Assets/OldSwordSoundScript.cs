﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OldSwordSoundScript : MonoBehaviour
{
    private void OnEnable()
    {
        SoundManager.instance.StartAudio(new string[1] { "Weapon/old_sword_hit" }, VOLUME_TYPE.EFFECT);
    }
}
