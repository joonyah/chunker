﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SignController : MonoBehaviour
{
    [SerializeField] private string signName;
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(transform.position, 2);
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        RaycastHit2D hit = Physics2D.CircleCast(transform.position, 2, Vector2.zero, 0, 1 << 9);

        if (hit && !TextOpenController.instance.isOpen)
        {
            TextOpenController.instance.ShowText(LocalizeManager.GetLocalize(signName), Color.white, 24, gameObject, 0.5f, true);
        }
        else if (!hit)
        {
            if (TextOpenController.instance.targetObj == gameObject)
            {
                TextOpenController.instance.TextClose(Color.white);
            }
        }
    }
}
