﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DamageText : MonoBehaviour
{
    [SerializeField] private Text outlineText;
    [SerializeField] private Text damageText;
    GameObject targetObj = null;
    // Start is called before the first frame update
    void Start()
    {
    }
    public void InitSetting(float _damage, GameObject _obj)
    {
        outlineText.text = string.Empty;
        damageText.text = string.Empty;
        targetObj = _obj;
        PDG.Dungeon.instance.StartCoroutine(PDG.Dungeon.instance.DamageTextOpen(this, _damage));
    }
    public IEnumerator DamageSetting(float _damage)
    {
        int t = Mathf.FloorToInt(_damage);
        outlineText.text = t.ToString();
        damageText.text = t.ToString();
        float alhpa = 1.0f;
        int fontSize = 12;
        float yAmount = 25.0f;
        if (targetObj)
        {
            Vector3 pos = Camera.main.WorldToScreenPoint(targetObj.transform.position);
            transform.position = Camera.main.WorldToScreenPoint(targetObj.transform.position) + new Vector3(0, yAmount, 0);
        }
        while (targetObj != null) 
        {
            outlineText.color = new Color(1, 1, 1, alhpa);
            damageText.color = new Color(1, 0, 0, alhpa);
            outlineText.fontSize = fontSize;
            damageText.fontSize = fontSize;
            fontSize++;
            yAmount += Time.fixedDeltaTime * 150;
            alhpa -= Time.fixedDeltaTime * 2;
            transform.position = Camera.main.WorldToScreenPoint(targetObj.transform.position) + new Vector3(0, yAmount, 0);
            if (alhpa <= 0.0f) break;
            yield return new WaitForFixedUpdate();
        }
        Destroy(gameObject);
    }
}
