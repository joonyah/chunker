﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestBossOpener : MonoBehaviour
{
    [SerializeField] private int bossType = 0;
    [SerializeField] private GameObject controllerObj;
    bool isSelect;
    [SerializeField] private GameObject AjitPrefabs;
    [SerializeField] private GameObject[] BossPrefabs;

    GameObject spawnObj = null;
    // Start is called before the first frame update
    void Start()
    {
        if (!PDG.Dungeon.instance.isTestMode2)
        {
            gameObject.SetActive(false);
        }
    }

    // Update is called once per frame
    void Update()
    {
        RaycastHit2D hit = Physics2D.BoxCast(transform.position, new Vector2(2.0f, 3.0f), 0, Vector2.zero, 0, 1 << 9);
        if (hit && !isSelect && !PDG.Dungeon.instance.newObjectSelected)
        {
            isSelect = true;
            PDG.Dungeon.instance.newObjectSelected = true;
            PDG.Dungeon.instance.newObjectSelectedObj = gameObject;
            controllerObj.SetActive(true);
            controllerObj.GetComponent<SpriteRenderer>().sprite = PDG.Dungeon.instance.isJoystrick ? PDG.Dungeon.GetJoyStrickKeySprite(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_ACTIVE]) : PDG.Dungeon.GetJoyStrickKeySprite(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ACTIVE]);
            TextOpenController.instance.ShowText(LocalizeManager.GetLocalize("warp_door_type_opner_t2_0_0"), Color.white, 24, controllerObj, 2.0f, true);
        }
        else if (!hit)
        {
            isSelect = false;
            if (PDG.Dungeon.instance.newObjectSelectedObj == gameObject)
            {
                PDG.Dungeon.instance.newObjectSelectedObj = null;
                PDG.Dungeon.instance.newObjectSelected = false;
            }
            if (TextOpenController.instance.targetObj == controllerObj)
            {
                TextOpenController.instance.TextClose(Color.white);
            }
            controllerObj.SetActive(false);
        }

        if(PDG.Dungeon.instance.isTestModeReset)
        {
            if (spawnObj) 
            {
                Destroy(spawnObj);
                spawnObj = null;

                for (int i = 0; i < PDG.Dungeon.instance.TestBoxs.Count; i++)
                    PDG.Dungeon.instance.TestBoxs[i].ResetBox();
                PDG.Dungeon.instance.isTestModeReset = false;
            }
        }
        if (isSelect)
        {
            if (Input.GetKeyDown(KeyCode.F))
            {
                if (bossType == -1)
                {
                    PDG.Dungeon.instance.newObjectSelectedObj = null;
                    PDG.Dungeon.instance.newObjectSelected = false;
                    PDG.Dungeon.instance.fBossRushTime = 0.0f;
                    GameObject obj = Instantiate(AjitPrefabs);
                    obj.transform.position = new Vector3(22.5f, 22.5f, 0);
                    PDG.Player.instance.transform.position = new Vector3(22.5f, 22.5f, 0);
                    SaveAndLoadManager.instance.SaveItem(new ItemSave { _id = "glowing_skull2", _nCount = 200 }, true);
                    SaveAndLoadManager.instance.DeleteItemBossRush();
                    Inventorys.Instance.glowindskull2 = 200;
                   
                    PDG.Dungeon.instance.isBossRush = true;
                    PDG.Dungeon.instance.StartCoroutine(PDG.Dungeon.instance.BossRush());
                    Destroy(transform.parent.gameObject);
                }
                else
                {
                    GameObject obj = Instantiate(BossPrefabs[bossType]);
                    obj.transform.position = new Vector3(100, 100, 0);
                    PDG.Player.instance.transform.position = new Vector3(100, 100 - (bossType == 1 ? 7 : 10), 0);
                    spawnObj = obj;
                }
            }
        }
    }
}
