﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DifficultyManager : MonoBehaviour
{
    public static DifficultyManager instance;
    public bool isOpen = false;
    [SerializeField] private GameObject openObj;
    DifficultyController ControllerObj;
    [SerializeField] private Image centerImage;
    [SerializeField] private Text centerText;
    [SerializeField] private Text leftInfo_firstText;
    [SerializeField] private Text leftInfo_secondText;
    [SerializeField] private Text leftInfo_thirdText;
    [SerializeField] private Image leftInfo_firstImage;
    [SerializeField] private Image achievementImage;
    [SerializeField] private Sprite[] achievementImages;
    [SerializeField] private Sprite[] leftFirstImages;
    [SerializeField] private Sprite[] centerImages;
    private int entranceNum = -1;
    bool isSpark = false;
    private void Awake()
    {
        if (instance == null) instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (isOpen)
        {
            float x = Input.GetAxisRaw("Horizontal_joy");
            if (Input.GetKeyDown(KeyCode.Escape) || PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_CANCLE], true) && PDG.Dungeon.instance.closeCoolTime > 0.1f)
            {
                PDG.Dungeon.instance.closeCoolTime = 0.0f;
                CloseDifficulty();
            }
            if (Input.GetKeyDown(KeyCode.Space) || PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_CONFIRM], true) && PDG.Dungeon.instance.closeCoolTime > 0.1f)
            {
                PDG.Dungeon.instance.closeCoolTime = 0.0f;
                SelectDifficulty();
            }
            if ((PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ARROW_LEFT], true) || Input.GetKeyDown(KeyCode.LeftArrow) || x < 0) && PDG.Dungeon.instance.closeCoolTime > 0.1f)
            {
                PDG.Dungeon.instance.closeCoolTime = 0.0f;
                SaveAndLoadManager.nDifficulty--;
                if (SaveAndLoadManager.nDifficulty < 0) SaveAndLoadManager.nDifficulty = 3;
            }
            else if ((PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ARROW_RIGHT], true) || Input.GetKeyDown(KeyCode.RightArrow) || x > 0) && PDG.Dungeon.instance.closeCoolTime > 0.1f)
            {
                PDG.Dungeon.instance.closeCoolTime = 0.0f;
                SaveAndLoadManager.nDifficulty++;
                if (SaveAndLoadManager.nDifficulty > 3) SaveAndLoadManager.nDifficulty = 0;
            }

            centerImage.sprite = centerImages[SaveAndLoadManager.nDifficulty];
            leftInfo_firstImage.sprite = leftFirstImages[SaveAndLoadManager.nDifficulty];
            achievementImage.sprite = achievementImages[SaveAndLoadManager.nDifficulty];
            centerText.text = LocalizeManager.GetLocalize("ui_difficulty_" + SaveAndLoadManager.nDifficulty);
            leftInfo_firstText.text = LocalizeManager.GetLocalize("ui_difficulty_infofirst_" + SaveAndLoadManager.nDifficulty);
            leftInfo_secondText.text = LocalizeManager.GetLocalize("ui_difficulty_infosecond_" + SaveAndLoadManager.nDifficulty);
            leftInfo_thirdText.text = LocalizeManager.GetLocalize("ui_difficulty_infothird_" + SaveAndLoadManager.nDifficulty);
        }
    }

    public void ShowDifficulty(DifficultyController _controllerObj)
    {
        entranceNum = SaveAndLoadManager.nDifficulty;
        ControllerObj = _controllerObj;
        isOpen = true;
        openObj.SetActive(true);
    }
    public void CloseDifficulty()
    {
        SaveAndLoadManager.nDifficulty = entranceNum;
        isOpen = false;
        ControllerObj.Setting();
        openObj.SetActive(false);
    }

    public void SelectDifficulty()
    {
        if (!isSpark)
        {
            isSpark = true;
            StartCoroutine(SpakeCoroutine());
        }
    }
    IEnumerator SpakeCoroutine()
    {
        float a = 0.0f;
        Image img = GameObject.Find("Canvas").transform.Find("Spake").GetComponent<Image>();
        while (true)
        {
            img.color = new Color(1, 1, 1, a);
            a += Time.deltaTime * 2.5f;
            if (a >= 1.0f) break;
            yield return null;
        }
        SoundManager.instance.StartAudio(new string[1] { "Object/DifficultyChange" }, VOLUME_TYPE.EFFECT);
        img.color = Color.white;
        while (true)
        {
            img.color = new Color(1, 1, 1, a);
            a -= Time.deltaTime * 2.5f;
            if (a <= 0.0f) break;
            yield return null;
        }
        img.color = new Color(1, 1, 1, 0);
        isSpark = false;
        entranceNum = SaveAndLoadManager.nDifficulty;
        CloseDifficulty();
    }
}
