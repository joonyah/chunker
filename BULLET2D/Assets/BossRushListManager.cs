﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BossRushListManager : MonoBehaviour
{
    public static BossRushListManager instance;
    public bool isOpen = false;
    [SerializeField] private GameObject listObj;
    [SerializeField] private GameObject openObj;
    [SerializeField] private Text subText;
    [SerializeField] private Transform ContentTransform;

    int nowDiff = 0;
    List<GameObject> objList = new List<GameObject>();
    private void Awake()
    {
        if (instance == null) instance = this;
    }
    private void Update()
    {
        float x = Input.GetAxisRaw("Horizontal_joy");
        if (isOpen && (Input.GetAxis("Mouse ScrollWheel") * 100 != 0))
        {
            float max = -ContentTransform.GetComponent<RectTransform>().sizeDelta.y;
            Vector3 min = ContentTransform.GetComponent<RectTransform>().offsetMin;
            min.y -= Input.GetAxis("Mouse ScrollWheel") * 150;
            if (min.y > -510) min.y = -510;
            if (min.y < -ContentTransform.GetComponent<RectTransform>().sizeDelta.y) min.y = -ContentTransform.GetComponent<RectTransform>().sizeDelta.y;
            ContentTransform.GetComponent<RectTransform>().offsetMin = min;
        }
        if (isOpen && (Input.GetKeyDown(KeyCode.Escape) || PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_CANCLE], true)) && PDG.Dungeon.instance.closeCoolTime > 0.2f)
        {
            isOpen = false;
            openObj.SetActive(false);
        }
        if (isOpen && (PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ARROW_LEFT], false) || Input.GetKeyDown(KeyCode.LeftArrow) || x < 0) && PDG.Dungeon.instance.closeCoolTime > 0.2f)
        {
            PDG.Dungeon.instance.closeCoolTime = 0.0f;
            nowDiff--;
            if (nowDiff < 0) nowDiff = 3;
            Show();
        }
        if (isOpen && (PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ARROW_RIGHT], false) || Input.GetKeyDown(KeyCode.RightArrow) || x > 0) && PDG.Dungeon.instance.closeCoolTime > 0.2f)
        {
            PDG.Dungeon.instance.closeCoolTime = 0.0f;
            nowDiff++;
            if (nowDiff >= 4) nowDiff = 0;
            Show();
        }
    }
    public void Show()
    {
        subText.text = LocalizeManager.GetLocalize("login_diff_" + nowDiff);
        isOpen = true;
        for (int i = 0; i < objList.Count; i++)
        {
            Destroy(objList[i]);
            objList.RemoveAt(i);
            i--;
        }
        RushData[] data = SaveAndLoadManager.instance.LoadRushData();
        if (data != null)
        {
            for (int i = 0; i < data.Length; i++)
            {
                if (data[i].diff != nowDiff) continue;
                GameObject obj = Instantiate(listObj);
                obj.transform.SetParent(ContentTransform);
                obj.transform.localScale = Vector3.one;
                Text noText = obj.transform.Find("NO").GetComponent<Text>();
                Text DifficultyText = obj.transform.Find("Difficulty").GetComponent<Text>();
                Text TimeText = obj.transform.Find("Time").GetComponent<Text>();
                Text DateText = obj.transform.Find("Date").GetComponent<Text>();
                Text WeaponText = obj.transform.Find("Weapon").GetComponent<Text>();
                Text CellText = obj.transform.Find("Cell").GetComponent<Text>();

                noText.text = (i + 1).ToString();
                DifficultyText.text = LocalizeManager.GetLocalize("login_diff_" + data[i].diff);
                TimeText.text = FloatToTime(Mathf.FloorToInt(data[i].time));
                DateText.text = data[i].date.Substring(4, 2) + "." + data[i].date.Substring(6, 2) + "." + data[i].date.Substring(0, 4);
                WeaponText.text = LocalizeManager.GetLocalize(data[i].weapon);
                CellText.text = (200 - data[i].usesell).ToString();
                objList.Add(obj);
            }
        }
        openObj.SetActive(true);
        StartCoroutine(RectReSize());
    }
    IEnumerator RectReSize()
    {
        yield return null;
        RectTransform content = ContentTransform.GetComponent<RectTransform>();
        content.anchorMin = new Vector2(0, 1);
        content.anchorMax = new Vector2(1, 1);
        Vector2 temp = new Vector2(0, -content.sizeDelta.y);
        content.offsetMin = temp;
        temp = new Vector3(0, temp.y + content.sizeDelta.y);
        content.offsetMax = temp;
        if (-content.sizeDelta.y < -510)
        {
            openObj.transform.Find("Scroll View").GetComponent<ScrollRect>().vertical = true;
        }
        else
        {
            openObj.transform.Find("Scroll View").GetComponent<ScrollRect>().vertical = false;
        }
    }
    private string FloatToTime(int _time)
    {
        if (_time == 0) return "0S";

        int day = 0;
        int hour = 0;
        int minute = 0;
        int secend = 0;
        int newTime = _time;

        while (newTime >= 86400)
        {
            newTime -= 86400;
            day++;
        }

        while (newTime >= 3600)
        {
            newTime -= 3600;
            hour++;
        }

        while (newTime >= 60)
        {
            newTime -= 60;
            minute++;
        }

        secend = newTime;

        return (day > 0 ? day + "D " : string.Empty) + (hour > 0 ? hour + "H " : string.Empty) + (minute > 0 ? minute + "M " : string.Empty) + (secend > 0 ? secend + "S" : string.Empty);
    }
}
