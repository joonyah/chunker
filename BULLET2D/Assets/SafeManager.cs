﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Security.AccessControl;
using UnityEngine;
using UnityEngine.UI;

public class SafeManager : MonoBehaviour
{
    public static SafeManager instance;
    [SerializeField] private Transform safePartsParent;
    [SerializeField] private GameObject safeObj;
    [SerializeField] private GameObject safePartsObj;
    private List<GameObject> partsObjList = new List<GameObject>();
    public bool isOpen = false;
    private void Awake()
    {
        if (instance == null) instance = this;
    }
    private void Update()
    {
        if (isOpen && (Input.GetKeyDown(KeyCode.Escape) || PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_CANCLE], true)) && PDG.Dungeon.instance.closeCoolTime > 0.2f) 
        {
            PDG.Dungeon.instance.closeCoolTime = 0.0f;
            Cancle();
        }

        //float max = -safePartsParent.GetComponent<RectTransform>().sizeDelta.y;
        //Vector3 min = safePartsParent.GetComponent<RectTransform>().offsetMin;
        //min.y -= Input.GetAxis("Mouse ScrollWheel") * 150;
        //if (min.y > -580) min.y = -580;
        //if (min.y < -safePartsParent.GetComponent<RectTransform>().sizeDelta.y) min.y = -safePartsParent.GetComponent<RectTransform>().sizeDelta.y;
        //safePartsParent.GetComponent<RectTransform>().offsetMin = min;
    }
    public void ShowMake()
    {
        for (int i = 0; i < partsObjList.Count; i++)
        {
            GameObject obj = partsObjList[i];
            Destroy(obj);
        }
        partsObjList.Clear();
        try
        {
            for (int i = 0; i < PDG.Dungeon.instance.materialTypes.Length; i++)
            {
                Sprite[] materialSprites = Resources.LoadAll<Sprite>("Item/Material/" + PDG.Dungeon.instance.materialTypes[i]);
                for (int j = 0; j < materialSprites.Length; j++)
                {
                    int c = SaveAndLoadManager.instance.LoadItem(PDG.Dungeon.instance.materialTypes[i] + "_" + materialSprites[j].name);
                    if (c > 0)
                    {
                        GameObject obj = Instantiate(safePartsObj);
                        Sprite s = Resources.Load<Sprite>("Item/Material/" + PDG.Dungeon.instance.materialTypes[i] + "/" + materialSprites[j].name);
                        obj.GetComponent<MakeItemController>().spriteImg.sprite = s;
                        obj.GetComponent<MakeItemController>()._id = PDG.Dungeon.instance.materialTypes[i] + "_" + materialSprites[j].name;
                        obj.GetComponent<MakeItemController>().idText.text = LocalizeManager.GetLocalize(PDG.Dungeon.instance.materialTypes[i] + "_" + materialSprites[j].name);
                        obj.GetComponent<MakeItemController>().countText.text = c.ToString();
                        obj.transform.SetParent(safePartsParent);
                        obj.transform.localScale = Vector3.one;
                        partsObjList.Add(obj);
                    }
                }
            }
            isOpen = true;
            //StartCoroutine(RectReSize());
            safeObj.SetActive(true);
        }
        catch(Exception e)
        {
            TextOpenController.instance.ShowText(e.Message, Color.white, 24, PDG.Player.instance.gameObject, 5.0f);
        }
    }
    IEnumerator RectReSize()
    {
        yield return null;
        RectTransform content = safePartsParent.GetComponent<RectTransform>();
        content.anchorMin = new Vector2(0, 1);
        content.anchorMax = new Vector2(1, 1);
        Vector2 temp = new Vector2(0, -content.sizeDelta.y);
        content.offsetMin = temp;
        temp = new Vector3(0, temp.y + content.sizeDelta.y);
        content.offsetMax = temp;
        if (-content.sizeDelta.y < -510)
        {
            safeObj.transform.Find("Parts").Find("Scroll View").GetComponent<ScrollRect>().vertical = true;
        }
        else
        {
            safeObj.transform.Find("Parts").Find("Scroll View").GetComponent<ScrollRect>().vertical = false;
        }
    }
    public void Cancle()
    {
        isOpen = false;
        safeObj.SetActive(false);
    }
}

