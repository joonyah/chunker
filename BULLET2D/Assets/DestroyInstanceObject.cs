﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyInstanceObject : MonoBehaviour
{
    [SerializeField] private bool isEndEffect;
    [SerializeField] private bool isDestroy;
    [SerializeField] private float destroyTime;

    private void OnEnable()
    {
        if(isEndEffect)
        {
            StartCoroutine(Close());
        }
    }

    IEnumerator Close()
    {
        yield return new WaitForSeconds(destroyTime);
        if (isDestroy) gameObject.SetActive(false);
        else Destroy(gameObject);
    }
}
