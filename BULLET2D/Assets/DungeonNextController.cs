﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DungeonNextController : MonoBehaviour
{
    [SerializeField] private Sprite fSprite;
    [SerializeField] private GameObject controllerObj;
    [SerializeField] private SpriteRenderer controllerSR;
    [SerializeField] private bool isSelect = false;

    [SerializeField] private Sprite defaultSprite;
    public Sprite endSprite;

    public GameObject animObj;
    public GameObject entranceQuestionPanel;
    public Button YesBtn;
    public Button NoBtn;
    public Text entranceQuestionText;
    public GameObject fadeObj;
    private float fadeTime = 0.1f;
    private float moveSpeed = 1500.0f;
    private Animator WarpExitAnim;
    LocalizeData lData;

    public SpriteRenderer sr;
    public Animator animator;
    [SerializeField] private bool isOpen = false;

    public bool isBossOpen = false;
    [SerializeField] private bool isBoss = false;

    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
        sr = GetComponent<SpriteRenderer>();

        if (animator) animator.enabled = false;
        sr.sprite = defaultSprite;

        if (entranceQuestionPanel == null) entranceQuestionPanel = GameObject.Find("Canvas").transform.Find("QuestionPanel").gameObject;
        if (entranceQuestionText == null) entranceQuestionText = entranceQuestionPanel.transform.Find("Text").GetComponent<Text>();
        if (YesBtn == null) YesBtn = entranceQuestionPanel.transform.Find("BtnPanel").Find("YesBtn").GetComponent<Button>();
        if (NoBtn == null) NoBtn = entranceQuestionPanel.transform.Find("BtnPanel").Find("NoBtn").GetComponent<Button>();
        if (fadeObj == null) fadeObj = GameObject.Find("Canvas").transform.Find("fadeObj").gameObject;
        lData = System.Array.Find(SheetManager.Instance.LocalDB.dataArray, item => item.ID.Equals("next_stage"));
    }

    // Update is called once per frame
    void Update()
    {
        RaycastHit2D hit = Physics2D.BoxCast(transform.position + new Vector3(-0.2f, -0.4f), new Vector2(4.0f, 4.0f), 0, Vector2.zero, 0, 1 << 9);

        if (hit && !isSelect && (isBoss ? isBossOpen : true) && !isOpen)
        {
            if(isBossOpen)
            {
                if (GameObject.Find("Canvas").transform.Find("TutorialText").gameObject.activeSelf)
                    GameObject.Find("Canvas").transform.Find("TutorialText").gameObject.SetActive(false);
            }
            isSelect = true;
            controllerObj.SetActive(true);
            controllerObj.GetComponent<SpriteRenderer>().sprite = PDG.Dungeon.instance.isJoystrick ? PDG.Dungeon.GetJoyStrickKeySprite(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_ACTIVE]) : PDG.Dungeon.GetJoyStrickKeySprite(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ACTIVE]);
        }
        else if (!hit && !isOpen)
        {
            isSelect = false;
            controllerObj.SetActive(false);
            if (animator) animator.enabled = false;
            if (!isBoss) sr.sprite = defaultSprite;
        }

        if (entranceQuestionPanel.activeSelf)
        {
            if ((Input.GetKeyDown(KeyCode.Escape) || PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_CANCLE], true)) && PDG.Dungeon.instance.closeCoolTime > 0.2f)
            {
                PDG.Dungeon.instance.closeCoolTime = 0.0f;
                PDG.Player.instance.isCameraOff = false;
                entranceQuestionPanel.SetActive(false);
            }
        }
        if (entranceQuestionPanel.activeSelf && isSelect)
        {
            if ((PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ACTIVE], true) || PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_CONFIRM], true)) && PDG.Dungeon.instance.closeCoolTime > 0.2f)
            {
                PDG.Dungeon.instance.closeCoolTime = 0.0f;
                SoundManager.instance.StartAudio(new string[1] { "Object/active_F" }, VOLUME_TYPE.EFFECT);
                PDG.Player.instance.isCameraOff = false;
                Warp();
            }
        }
        else if (isSelect)
        {
            if ((PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ACTIVE], true) || PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_ACTIVE], true)) && PDG.Dungeon.instance.closeCoolTime > 0.2f)
            {
                PDG.Dungeon.instance.closeCoolTime = 0.0f;
                SoundManager.instance.StartAudio(new string[1] { "Object/active_F" }, VOLUME_TYPE.EFFECT);
                isSelect = false;
                controllerObj.SetActive(false);
                if (animator) animator.enabled = true;
                if (animator) animator.Rebind();

                PDG.Dungeon.instance.newObjectSelectedObj = null;
                PDG.Dungeon.instance.newObjectSelected = false;
                PDG.Player.instance.isCameraOff = false;
                Warp();
            }
        }
    }
    public void Question()
    {
        if (!entranceQuestionPanel.activeSelf)
        {
            if (LocalizeManager.lang == LANGUAGE_SHIFTRICK.ENG)
                entranceQuestionText.text = lData.ENG;
            else if (LocalizeManager.lang == LANGUAGE_SHIFTRICK.KOR)
                entranceQuestionText.text = lData.KOR;
            else if (LocalizeManager.lang == LANGUAGE_SHIFTRICK.CHN_S)
                entranceQuestionText.text = lData.CHNS;
            else if (LocalizeManager.lang == LANGUAGE_SHIFTRICK.CHN_T)
                entranceQuestionText.text = lData.CHNT;
            else if (LocalizeManager.lang == LANGUAGE_SHIFTRICK.JA)
                entranceQuestionText.text = lData.JA;
            else if (LocalizeManager.lang == LANGUAGE_SHIFTRICK.RU)
                entranceQuestionText.text = lData.RU;
            entranceQuestionPanel.SetActive(true);
            StartCoroutine(StartQuestionEvent());
            YesBtn.onClick.RemoveAllListeners();
            YesBtn.onClick.AddListener(() => {
                PDG.Player.instance.isCameraOff = false;
                Warp();
            });
            NoBtn.onClick.RemoveAllListeners();
            NoBtn.onClick.AddListener(() => {
                PDG.Player.instance.isCameraOff = false;
                entranceQuestionPanel.SetActive(false);
            });
        }
    }
    private void Warp()
    {
        isOpen = true;
        isSelect = false;
        StartCoroutine(WarpStart());
    }
    IEnumerator WarpStart()
    {
        if (animator) animator.enabled = false;
        entranceQuestionPanel.SetActive(false);
        yield return new WaitForSeconds(Time.deltaTime);
        PDG.Dungeon.isStageClear = true;
        if (!PDG.Dungeon.instance.isStageBoss && !isBoss)
        {
            PDG.Dungeon.instance.nowStageStep++;
            if (PDG.Dungeon.instance.stageCount <= PDG.Dungeon.instance.nowStageStep)
                PDG.Dungeon.instance.isStageBoss = true;
        }
        if (GameObject.Find("Canvas").transform.Find("ExitArrow")) GameObject.Find("Canvas").transform.Find("ExitArrow").gameObject.SetActive(false);
        sr.sprite = endSprite;

        PDG.Dungeon.instance.newObjectSelectedObj = null;
        PDG.Dungeon.instance.newObjectSelected = false;

        if (PDG.Dungeon.instance.isBossRush)
        {
            Camera.main.orthographicSize = 7;
            PDG.Dungeon.instance.StageLevel++;
            if(PDG.Dungeon.instance.StageLevel >= 4)
            {
                int t = SaveAndLoadManager.instance.LoadItem("glowing_skull2");
                string wName = PDG.Player.instance.nowWeaponName;
                SaveAndLoadManager.instance.BossRushSave(wName, t);
                PDG.Dungeon.instance.isTestModeReset = true;
                PDG.Dungeon.instance.GoAjit2(true);
            }
            else
            {
                PDG.Dungeon.instance.isBossRushStart = true;
                string mapId = "d01_bossstage_0" + (PDG.Dungeon.instance.stageBossRan[PDG.Dungeon.instance.StageLevel] + 1);
                PDG.MapPiece piece = System.Array.Find(PDG.Dungeon.instance._MapPiece, item => item.id.Equals(mapId));
                Vector3 pos = transform.parent.position;
                pos.x += 50.0f;
                pos.y += 50.0f;
                GameObject oobj = Instantiate(piece.obj, null);
                oobj.transform.position = pos;
                pos.y += piece.doorPos[0].y;
                PDG.Player.instance.transform.position = pos;
                entranceQuestionPanel.SetActive(false);
                Destroy(transform.parent.gameObject);
            }
        }
        else if (PDG.Dungeon.instance.isDemo && !PDG.Dungeon.instance.isStageBoss) 
        {
            if (!SaveAndLoadManager.instance.GameDelete()) Debug.LogError("DELETE FAIL");
            PDG.Dungeon.instance.GoTumblbug();
        }
        else if (PDG.Dungeon.instance.StageLevel == 4)
        {
            if (!SaveAndLoadManager.instance.GameDelete()) Debug.LogError("DELETE FAIL");
            PDG.Dungeon.instance.GoEnding();
        }
        else
        {
            SaveAndLoadManager.instance.GameSave(Inventorys.Instance.slots);
            PDG.Dungeon.instance.StateCreate("", null, animObj);
        }

    }
    IEnumerator FadeInOut()
    {
        fadeObj.SetActive(true);
        float a = 0.0f;
        while (a <= 1.0f)
        {
            fadeObj.GetComponent<Image>().color = new Color(0, 0, 0, a);
            a += Time.deltaTime / fadeTime;
            yield return new WaitForSeconds(Time.deltaTime);
        }
        fadeObj.GetComponent<Image>().color = new Color(0, 0, 0, 1);
        yield return new WaitForSeconds(1.0f);
        while (a >= 0.0f)
        {
            fadeObj.GetComponent<Image>().color = new Color(0, 0, 0, a);
            a -= Time.deltaTime / fadeTime;
            yield return new WaitForSeconds(Time.deltaTime);
        }
        fadeObj.GetComponent<Image>().color = new Color(0, 0, 0, 0);
        fadeObj.SetActive(false);
    }
    IEnumerator StartQuestionEvent()
    {
        RectTransform rt = entranceQuestionPanel.GetComponent<RectTransform>();
        float y = 500.0f;
        while (true)
        {
            rt.anchoredPosition3D = new Vector3(0, y, 0);
            y -= Time.deltaTime * moveSpeed;
            if (y <= 50.0f) break;
            yield return new WaitForSeconds(Time.deltaTime);
        }
        y = 50.0f;
        rt.anchoredPosition3D = new Vector3(0, y, 0);
        while (true)
        {
            rt.anchoredPosition3D = new Vector3(0, y, 0);
            y += Time.deltaTime * (moveSpeed / 4);
            if (y >= 70.0f) break;
            yield return new WaitForSeconds(Time.deltaTime);
        }
        y = 70.0f;
        rt.anchoredPosition3D = new Vector3(0, y, 0);
        while (true)
        {
            rt.anchoredPosition3D = new Vector3(0, y, 0);
            y -= Time.deltaTime * (moveSpeed / 4);
            if (y <= 50.0f) break;
            yield return new WaitForSeconds(Time.deltaTime);
        }
        y = 50.0f;
        rt.anchoredPosition3D = new Vector3(0, y, 0);
        while (true)
        {
            rt.anchoredPosition3D = new Vector3(0, y, 0);
            y += Time.deltaTime * (moveSpeed / 4);
            if (y >= 60.0f) break;
            yield return new WaitForSeconds(Time.deltaTime);
        }
        y = 60.0f;
        rt.anchoredPosition3D = new Vector3(0, y, 0);
        while (true)
        {
            rt.anchoredPosition3D = new Vector3(0, y, 0);
            y -= Time.deltaTime * (moveSpeed / 4);
            if (y <= 50.0f) break;
            yield return new WaitForSeconds(Time.deltaTime);
        }
        y = 50.0f;
        rt.anchoredPosition3D = new Vector3(0, y, 0);
    }
    IEnumerator EndQuestionEvent()
    {
        RectTransform rt = entranceQuestionPanel.GetComponent<RectTransform>();
        float y = 50.0f;
        rt.anchoredPosition3D = new Vector3(0, y, 0);
        while (true)
        {
            rt.anchoredPosition3D = new Vector3(0, y, 0);
            y += Time.deltaTime * (moveSpeed / 2);
            if (y >= 500.0f) break;
            yield return new WaitForSeconds(Time.deltaTime);
        }
        y = 500.0f;
        rt.anchoredPosition3D = new Vector3(0, y, 0);
        entranceQuestionPanel.SetActive(false);
    }
}
