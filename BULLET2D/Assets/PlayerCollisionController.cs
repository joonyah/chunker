﻿using MOB;
using PDG;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class PlayerCollisionController : MonoBehaviour
{
    ArmController armController;
    [SerializeField] private GameObject hitEffect;

    public float hitTime = 0.25f;
    [SerializeField] private List<GameObject> objList = new List<GameObject>();
    [SerializeField] private List<float> timeList = new List<float>();
    public CapsuleDirection2D CapsuleDirection = CapsuleDirection2D.Horizontal;
    public Vector2 offSet;
    public Vector2 size;
    [SerializeField] private GameObject criticalHitEffect;
    [SerializeField] private bool isPublicList = false;
    private void Start()
    {
        armController = Player.instance._ArmController;
    }

    private void OnEnable()
    {
    }

    private void Update()
    {
        if (!isPublicList)
        {
            for (int i = 0; i < objList.Count; i++)
            {
                if (objList[i] == null)
                {
                    objList.RemoveAt(i);
                    timeList.RemoveAt(i);
                    i--;
                    continue;
                }
                else if (objList[i].GetComponent<MonsterArggro>())
                {
                    if (objList[i].GetComponent<MonsterArggro>()._monster.isDie)
                    {
                        objList.RemoveAt(i);
                        timeList.RemoveAt(i);
                        i--;
                        continue;
                    }
                }
                else if (objList[i].GetComponent<Monster>())
                {
                    if (objList[i].GetComponent<Monster>().isDie)
                    {
                        objList.RemoveAt(i);
                        timeList.RemoveAt(i);
                        i--;
                        continue;
                    }
                }
                timeList[i] += Time.fixedDeltaTime;
            }
        }

        RaycastHit2D[] hits = Physics2D.CapsuleCastAll(transform.position, size, CapsuleDirection, gun_simulation.rot, Vector2.zero, 0, 1 << 12 | 1 << 23 | 1 << 24);
        if (hits.Length > 0)
        {
            for (int i = 0; i < hits.Length; i++)
            {
                GameObject obj = hits[i].collider.gameObject;
                int index = -1;
                if (isPublicList) index = Dungeon.instance.publicListObj.FindIndex(item => item.Equals(obj));
                else index = objList.FindIndex(item => item.Equals(obj));
                if (index != -1)
                {
                    if (isPublicList)
                    {
                        if (hitTime > Dungeon.instance.publicListTime[index]) continue;
                        else Dungeon.instance.publicListTime[index] = 0.0f;
                    }
                    else
                    {
                        if (hitTime > timeList[index]) continue;
                        else timeList[index] = 0.0f;
                    }
                }
                else
                {
                    if (isPublicList)
                    {
                        Dungeon.instance.publicListObj.Add(obj);
                        Dungeon.instance.publicListTime.Add(0.0f);
                    }
                    else
                    {
                        objList.Add(obj);
                        timeList.Add(0.0f);
                    }
                }
                if (obj.layer.Equals(23))
                {
                    if (obj.name.Contains("puddle")) return;
                    if (obj.name.Contains("grass_2")) return;
                    if (obj.name.Contains("door")) return;
                    Vector3 dir = obj.transform.position - Player.instance.transform.position;
                    dir.Normalize();
                    if (obj.transform.parent && obj.transform.parent.GetComponent<TentacleController>())
                    {
                        obj.transform.parent.GetComponent<TentacleController>().SetDamage(1);

                        if (hitEffect)
                        {
                            GameObject obj2 = Instantiate(hitEffect);
                            obj2.transform.position = obj.transform.position;
                            Destroy(obj2, 2f);
                        }
                    }
                    if (obj.GetComponent<DestroyObjects>())
                    {
                        obj.GetComponent<DestroyObjects>().DestroyObject(dir);

                        if (hitEffect)
                        {
                            GameObject obj2 = Instantiate(hitEffect);
                            obj2.transform.position = obj.transform.position;
                            Destroy(obj2, 2f);
                        }
                    }
                    if (obj.GetComponent<ChestHit>() && !obj.GetComponent<ChestHit>().isOpenBefore)
                    {
                        obj.GetComponent<ChestHit>().SetDamage(1);

                        if (hitEffect)
                        {
                            GameObject obj2 = Instantiate(hitEffect);
                            obj2.transform.position = obj.transform.position;
                            Destroy(obj2, 2f);
                        }
                    }
                    if (obj.GetComponent<DrumHit>())
                    {
                        obj.GetComponent<DrumHit>().SetDamage(1);

                        if (hitEffect)
                        {
                            GameObject obj2 = Instantiate(hitEffect);
                            obj2.transform.position = obj.transform.position;
                            Destroy(obj2, 2f);
                        }
                    }
                }
                if (obj.layer.Equals(24))
                {
                    if (obj.GetComponent<BossMonster_Six_Apear>()) obj.GetComponent<BossMonster_Six_Apear>().SetDamage(Player.instance.damageNear);
                    if (obj.GetComponent<Boss_STG>()) obj.GetComponent<Boss_STG>().SetDamage(Player.instance.isLeftNear ? Player.instance.damageNear : Player.instance.damage, hits[i].point);
                    if (obj.GetComponent<Boss_NPC>()) obj.GetComponent<Boss_NPC>().SetDamage(Player.instance.isLeftNear ? Player.instance.damageNear : Player.instance.damage, hits[i].point);
                    if (obj.GetComponent<Boss_Golem>()) obj.GetComponent<Boss_Golem>().SetDamage(Player.instance.isLeftNear ? Player.instance.damageNear : Player.instance.damage, hits[i].point);
                    if (obj.GetComponent<Boss_Stone>()) obj.GetComponent<Boss_Stone>().SetDamage(Player.instance.isLeftNear ? Player.instance.damageNear : Player.instance.damage, hits[i].point, false);
                    if (Player.instance.isStrategic_02) Player.instance.isStrategic_02 = false;

                    if (hitEffect && !Player.instance.isCritical)
                    {
                        GameObject hitObj = Instantiate(hitEffect);
                        hitObj.transform.position = hits[i].point;
                        Destroy(hitObj, 2f);
                    }
                    if (Player.instance.isLeftNear && Player.instance.isCritical)
                    {
                        GameObject criEffect = ObjManager.Call().GetObject("CriticalHitEffect");
                        criEffect.transform.position = hits[i].point;
                        criEffect.SetActive(true);
                    }
                }
                if (obj.layer.Equals(12))
                {
                    Monster mob = obj.GetComponent<Monster>();

                    Vector3 dir = mob.transform.position - Player.instance.transform.position;
                    dir.Normalize();
                    mob.SetDamage((Player.instance.isLeftNear ? Player.instance.damageNear : Player.instance.damage), -dir, 1, Player.instance.left_elemetanl, Player.instance.left_elemnetalTime);
                    if (Player.instance.isStrategic_02) Player.instance.isStrategic_02 = false;

                    if (hitEffect && !Player.instance.isCritical)
                    {
                        GameObject hitObj = Instantiate(hitEffect);
                        hitObj.transform.position = hits[i].point;
                        Destroy(hitObj, 2f);
                    }
                    if (Player.instance.isLeftNear && Player.instance.isCritical)
                    {
                        GameObject criEffect = ObjManager.Call().GetObject("CriticalHitEffect");
                        criEffect.transform.position = hits[i].point;
                        criEffect.SetActive(true);
                    }
                }
            }
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.layer.Equals(8)) return;
    }
}
