﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MrDarkController : MonoBehaviour
{
    public string mobType;
    string _spritePath;
    Sprite[] _sprites;
    private SpriteRenderer sr;
    public Animator animator;

    [SerializeField] private Sprite fSprite;
    [SerializeField] private GameObject controllerObj;
    [SerializeField] private SpriteRenderer controllerSR;
    [SerializeField] private bool isSelect = false;
    bool isChecking = false;
    bool isFireEyes = false;
    [SerializeField] private Sprite helldogSpriite;

    [SerializeField] private Vector3 controller2Pos;
    [SerializeField] private GameObject controllerObj2;
    [SerializeField] private bool isSelect2 = false;
    [SerializeField] private string[] _openText;
    [SerializeField] private string npcName;

    [Header("SlolText")]
    [SerializeField] private string[] SoloTexts;
    [SerializeField] private Text nowSoloText;
    [SerializeField] private Font _font;
    [SerializeField] private bool isTargeting = false;

    private void Awake()
    {
        animator = GetComponent<Animator>();
        sr = GetComponent<SpriteRenderer>();
    }
    // Start is called before the first frame update
    void Start()
    {
        Load();

        int t = SaveAndLoadManager.instance.LoadItem("MrDarkFlag");
        if (t == 1) isFireEyes = true;
        StartCoroutine(SoloText());
    }
    IEnumerator SoloText()
    {
        if (nowSoloText == null)
        {
            GameObject obj = new GameObject();
            Text nText = obj.AddComponent<Text>();
            nText.font = _font;
            nText.alignment = TextAnchor.MiddleCenter;
            nText.fontSize = 20;
            nText.horizontalOverflow = HorizontalWrapMode.Overflow;
            nowSoloText = nText;
            obj.transform.SetParent(GameObject.Find("Canvas").transform);
            obj.transform.localScale = Vector3.one;
            obj.transform.SetSiblingIndex(15);
        }

        while (true)
        {
            if (!isSelect)
            {
                nowSoloText.text = LocalizeManager.GetLocalize(SoloTexts[Random.Range(0, SoloTexts.Length)]);
                yield return new WaitForSeconds(2.0f);
                nowSoloText.text = string.Empty;
            }
            else
            {
                nowSoloText.text = string.Empty;
            }
            yield return new WaitForSeconds(Random.Range(2, 5));
        }
    }

    // Update is called once per frame
    void Update()
    {

        if (nowSoloText != null)
        {
            nowSoloText.gameObject.transform.position = Camera.main.WorldToScreenPoint(controllerObj.transform.position);
        }

        RaycastHit2D hit = Physics2D.BoxCast(transform.position + new Vector3(-0.1f, -0.5f), new Vector2(2.5f, 3.5f), 0, Vector2.zero, 0, 1 << 9);
        RaycastHit2D hit2 = Physics2D.BoxCast(transform.position + controller2Pos, new Vector2(1.2f, 1.2f), 0, Vector2.zero, 0, 1 << 9);
        RaycastHit2D hit3 = Physics2D.CircleCast(transform.position, 15, Vector2.zero, 0, 1 << 9);
        if (hit3 && isTargeting)
        {
            Vector3 dir = hit3.collider.transform.position - transform.position;
            dir.Normalize();
            if (dir.x < 0) GetComponent<SpriteRenderer>().flipX = true;
            else GetComponent<SpriteRenderer>().flipX = false;
        }

        if (hit2 && !isSelect2 && !PDG.Dungeon.instance.newObjectSelected)
        {
            isSelect2 = true;
            if (nowSoloText != null) nowSoloText.text = string.Empty;
            PDG.Dungeon.instance.newObjectSelected = true;
            PDG.Dungeon.instance.newObjectSelectedObj = gameObject;
            controllerObj2.GetComponent<SpriteRenderer>().sprite = PDG.Dungeon.instance.isJoystrick ? PDG.Dungeon.GetJoyStrickKeySprite(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_ACTIVE]) : PDG.Dungeon.GetJoyStrickKeySprite(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ACTIVE]);
            controllerObj2.SetActive(true);
        }
        else if (!hit2)
        {
            isSelect2 = false;
            if (PDG.Dungeon.instance.newObjectSelectedObj == gameObject)
            {
                PDG.Dungeon.instance.newObjectSelectedObj = null;
                PDG.Dungeon.instance.newObjectSelected = false;
            }
            controllerObj2.SetActive(false);
        }

        if (isSelect2)
        {
            if ((PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ACTIVE], true) || PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_ACTIVE], true)) && PDG.Dungeon.instance.closeCoolTime > 0.2f)
            {
                PDG.Dungeon.instance.closeCoolTime = 0.0f;
                SoundManager.instance.StartAudio(new string[1] { "Object/active_F" }, VOLUME_TYPE.EFFECT);
                isSelect2 = false;
                if (PDG.Dungeon.instance.newObjectSelectedObj == gameObject)
                {
                    PDG.Dungeon.instance.newObjectSelectedObj = null;
                    PDG.Dungeon.instance.newObjectSelected = false;
                }
                controllerObj2.SetActive(false);
                TextOpenController.instance.ShowText(LocalizeManager.GetLocalize(_openText[Random.Range(0, _openText.Length)]), Color.white, 24, controllerObj, 2.0f);
            }
        }

        if (hit && !isSelect && !TextOpenController.instance.isOpen && !PDG.Dungeon.instance.newObjectSelected)
        {
            isSelect = true;
            if (nowSoloText != null) nowSoloText.text = string.Empty;
            PDG.Dungeon.instance.newObjectSelected = true;
            PDG.Dungeon.instance.newObjectSelectedObj = gameObject;
            controllerObj.SetActive(true);
            controllerObj.GetComponent<SpriteRenderer>().sprite = PDG.Dungeon.instance.isJoystrick ? PDG.Dungeon.GetJoyStrickKeySprite(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_ACTIVE]) : PDG.Dungeon.GetJoyStrickKeySprite(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ACTIVE]);
            animator.SetBool("isSelected", true);
            TextOpenController.instance.ShowText(LocalizeManager.GetLocalize(npcName), Color.white, 24, controllerObj, 2.0f, true);
        }
        else if (!hit)
        {
            isSelect = false;
            if (PDG.Dungeon.instance.newObjectSelectedObj == gameObject)
            {
                PDG.Dungeon.instance.newObjectSelectedObj = null;
                PDG.Dungeon.instance.newObjectSelected = false;
            }
            controllerObj.SetActive(false);
            animator.SetBool("isSelected", false);
            if (TextOpenController.instance.targetObj == controllerObj && !hit2)
            {
                TextOpenController.instance.TextClose(Color.white);
            }
        }
        if (isSelect)
        {
            if ((PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ACTIVE], true) || PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_ACTIVE], true)) && PDG.Dungeon.instance.closeCoolTime > 0.2f)
            {
                PDG.Dungeon.instance.closeCoolTime = 0.0f;
                SoundManager.instance.StartAudio(new string[1] { "Object/active_F" }, VOLUME_TYPE.EFFECT);
                isSelect = false;
                if (PDG.Dungeon.instance.newObjectSelectedObj == gameObject)
                {
                    PDG.Dungeon.instance.newObjectSelectedObj = null;
                    PDG.Dungeon.instance.newObjectSelected = false;
                }
                controllerObj.SetActive(false);
                animator.SetBool("isSelected", false);
                if (!isFireEyes)
                {
                    int t = SaveAndLoadManager.instance.LoadItem("Boss_eyes_spirit");
                    if (t > 0)
                    {
                        TextOpenController.instance.ShowText(LocalizeManager.GetLocalize("ajit_dark_stone_comment"), Color.white, 24, controllerObj, 2.0f);
                        Inventorys.Instance.ShowGetItemUI("mrdark_unlock", helldogSpriite);
                        SaveAndLoadManager.instance.SaveItem(new ItemSave { _id = "MrDarkFlag", _nCount = 1 }, true);
                        isFireEyes = true;
                    }
                    else
                    {
                        TextOpenController.instance.ShowText(LocalizeManager.GetLocalize("ajit_dark_stone_not_comment"), Color.white, 24, controllerObj, 2.0f);
                    }
                }
                else
                {
                    MakeController.instance.ShowMake(string.Empty, "mrdark", gameObject);
                }
            }
        }
    }
    void LateUpdate()
    {
        if (sr == null || sr.sprite == null)
            return;
        Load();
        var name = sr.sprite.name;
        var sprite = System.Array.Find(_sprites, item => item.name == name);
        if (sprite) sr.sprite = sprite;
    }
    void Load()
    {
        var spritePath = "NPC/" + mobType;
        if (!spritePath.Equals(_spritePath))
        {
            _spritePath = spritePath;
            _sprites = Resources.LoadAll<Sprite>(_spritePath);
        }
    }
}
