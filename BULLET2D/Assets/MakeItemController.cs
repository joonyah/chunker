﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MakeItemController : MonoBehaviour
{
    public Image spriteImg;
    public Text idText;
    public Text countText;
    public string _id;

    private void Update()
    {
        int c = SaveAndLoadManager.instance.LoadItem(_id);
        int c2 = int.Parse(countText.text);
        if (c < c2)
        {
            countText.color = Color.red;
        }
    }
}
