﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VolatileColorChange : MonoBehaviour
{
    [SerializeField] private ParticleSystem particle;
    [SerializeField] private ParticleSystem.MainModule pMain;
    [SerializeField] private ParticleSystem.MinMaxGradient color;
    // Start is called before the first frame update
    void Start()
    {
        pMain = particle.main;   
    }

    // Update is called once per frame
    void Update()
    {
        Color color_1 = new Color32((byte)Random.Range(0, 255), (byte)Random.Range(0, 255), (byte)Random.Range(0, 255), 255);
        Color color_2 = new Color32((byte)Random.Range(0, 255), (byte)Random.Range(0, 255), (byte)Random.Range(0, 255), 255);
        color = new ParticleSystem.MinMaxGradient(color_1, color_2);
        pMain.startColor = color;
    }
}
