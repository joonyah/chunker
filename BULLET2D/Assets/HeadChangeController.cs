﻿using PDG;
using UnityEngine;

public class HeadChangeController : MonoBehaviour
{
    string _spritePath;
    Sprite[] _sprites;
    private SpriteRenderer sr;
    Animator animator;
    private void Awake()
    {
        sr = GetComponent<SpriteRenderer>();
    }
    // Start is called before the first frame update
    void Start()
    {
        Load();
    }
    void LateUpdate()
    {
        if (sr == null || sr.sprite == null)
            return;
        Load();
        var name = sr.sprite.name;
        var sprite = System.Array.Find(_sprites, item => item.name == name);
        if (sprite) sr.sprite = sprite;
    }
    void Load()
    {
        var spritePath = "Head/" + Dungeon.headType;
        if (!spritePath.Equals(_spritePath))
        {
            _spritePath = spritePath;
            _sprites = Resources.LoadAll<Sprite>(_spritePath);
        }
    }
}
