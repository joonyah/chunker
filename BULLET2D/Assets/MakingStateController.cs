﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MakingStateController : MonoBehaviour
{
    public Text[] StateTexts;
    // Start is called before the first frame update
    void Start()
    {
    }
    public void InitSetting()
    {
        for (int i = 0; i < StateTexts.Length; i++)
        {
            StateTexts[i].color = Color.white;
            StateTexts[i].text = "+0";
        }
    }
}
