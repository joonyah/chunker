﻿using PDG;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SetUiSprite : MonoBehaviour
{
    Image _image;
    [SerializeField] private Sprite noJoySPrite;
    public JoystickKeySet _joyKey;
    [SerializeField] private bool isNoSize = false;
    [SerializeField] private bool isNotKeyboardChange = false;
    Login _login;
    // Start is called before the first frame update
    void Start()
    {
        _image = GetComponent<Image>();
        _login = FindObjectOfType(typeof(Login)) as Login;
    }

    // Update is called once per frame
    void Update()
    {
        if (Dungeon.instance == null)
        {
            if (_login == null) return;

            if (_login.isJoystrick)
            {
                _image.sprite = _login.GetJoyStrickKeySprite(FPSDisplay.instance.JoyStickKey[_joyKey]);
                if (!isNoSize) _image.GetComponent<RectTransform>().sizeDelta = new Vector2(_image.sprite.texture.width * 3, _image.sprite.texture.height * 3);
                if (noJoySPrite.name.Equals("arrow"))
                {
                    _image.GetComponent<RectTransform>().anchoredPosition3D = new Vector3(-165, 0, 0);
                }
            }
            else
            {
                if (isNotKeyboardChange) _image.sprite = noJoySPrite;
                else if (_joyKey == JoystickKeySet.KEY_CANCLE) _image.sprite = noJoySPrite;
                else if (_joyKey == JoystickKeySet.KEY_CONFIRM) _image.sprite = _login.GetJoyStrickKeySprite(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ACTIVE]);
                else if (_joyKey == JoystickKeySet.KEY_ARROW) _image.sprite = noJoySPrite;
                else _image.sprite = _login.GetJoyStrickKeySprite(FPSDisplay.instance.KeyboardKey[_joyKey]);
                if (!isNoSize) _image.GetComponent<RectTransform>().sizeDelta = new Vector2(_image.sprite.texture.width * 3, _image.sprite.texture.height * 3);
                if (noJoySPrite.name.Equals("arrow"))
                {
                    _image.GetComponent<RectTransform>().anchoredPosition3D = new Vector3(-210, 20, 0);
                }
            }
        }
        else
        {
            if (Dungeon.instance.isJoystrick)
            {
                _image.sprite = Dungeon.GetJoyStrickKeySprite(FPSDisplay.instance.JoyStickKey[_joyKey]);
                if (!isNoSize) _image.GetComponent<RectTransform>().sizeDelta = new Vector2(_image.sprite.texture.width * 3, _image.sprite.texture.height * 3);
                if(noJoySPrite != null && noJoySPrite.name.Equals("arrow"))
                {
                    _image.GetComponent<RectTransform>().anchoredPosition3D = new Vector3(-165, 0, 0);
                }
            }
            else
            {
                if (isNotKeyboardChange) _image.sprite = noJoySPrite;
                else if(_joyKey == JoystickKeySet.KEY_ACTIVE && noJoySPrite.name.Equals("ui_mouse_scroll")) ;
                else if (_joyKey == JoystickKeySet.KEY_CANCLE) _image.sprite = noJoySPrite;
                else if (_joyKey == JoystickKeySet.KEY_CONFIRM) _image.sprite = Dungeon.GetJoyStrickKeySprite(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ACTIVE]);
                else if (_joyKey == JoystickKeySet.KEY_ARROW) _image.sprite = noJoySPrite;
                else _image.sprite = Dungeon.GetJoyStrickKeySprite(FPSDisplay.instance.KeyboardKey[_joyKey]);
                if (!isNoSize) _image.GetComponent<RectTransform>().sizeDelta = new Vector2(_image.sprite.texture.width * 3, _image.sprite.texture.height * 3);
                if (noJoySPrite != null && noJoySPrite.name.Equals("arrow"))
                {
                    _image.GetComponent<RectTransform>().anchoredPosition3D = new Vector3(-210, 20, 0);
                }
            }
        }
    }
}
