﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BossRushResetController : MonoBehaviour
{
    [SerializeField] private GameObject controllerObj;
    [SerializeField] private SpriteRenderer controllerSR;
    [SerializeField] private bool isSelect = false;
    [SerializeField] private bool isUse = false;
    [SerializeField] private bool isOpen = false;
    [SerializeField] private Vector3 offSet;
    [SerializeField] private Vector3 size;
    // Start is called before the first frame update
    void Start()
    {
    }
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireCube(transform.position + offSet, size);
    }
    // Update is called once per frame
    void Update()
    {
        RaycastHit2D hit = Physics2D.BoxCast(transform.position + offSet, size, 0, Vector2.zero, 0, 1 << 9);
        if (hit && !isSelect && !isOpen && !isUse)
        {
            isSelect = true;
            controllerObj.SetActive(true);
            controllerObj.GetComponent<SpriteRenderer>().sprite = PDG.Dungeon.instance.isJoystrick ? PDG.Dungeon.GetJoyStrickKeySprite(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_ACTIVE]) : PDG.Dungeon.GetJoyStrickKeySprite(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ACTIVE]);
            TextOpenController.instance.ShowText(LocalizeManager.GetLocalize("ajit2_reset_title"), Color.white, 24, controllerObj, 2.0f, true);
        }
        if (hit && !isSelect && isOpen && !isUse)
        {
            isSelect = true;
            controllerObj.SetActive(true);
            controllerObj.GetComponent<SpriteRenderer>().sprite = PDG.Dungeon.instance.isJoystrick ? PDG.Dungeon.GetJoyStrickKeySprite(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_ACTIVE]) : PDG.Dungeon.GetJoyStrickKeySprite(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ACTIVE]);
            TextOpenController.instance.ShowText(LocalizeManager.GetLocalize("ajit2_reset_title2"), Color.white, 24, controllerObj, 2.0f, true);
        }
        else if (!hit)
        {
            isSelect = false;
            isOpen = false;
            isUse = false;
            controllerObj.SetActive(false);
            if (TextOpenController.instance.targetObj == controllerObj) TextOpenController.instance.TextClose(Color.white);
        }

        if (isSelect)
        {
            if ((PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.KeyboardKey[JoystickKeySet.KEY_ACTIVE], true) || PDG.Dungeon.JoyStickKeyInputCheck(FPSDisplay.instance.JoyStickKey[JoystickKeySet.KEY_ACTIVE], true)) && PDG.Dungeon.instance.closeCoolTime > 0.2f)
            {
                PDG.Dungeon.instance.closeCoolTime = 0.0f;
                SoundManager.instance.StartAudio(new string[1] { "Click_Safe" }, VOLUME_TYPE.EFFECT);
                isSelect = false;
                controllerObj.SetActive(false);
                if (!isOpen)
                {
                    isOpen = true;
                }
                else
                {
                    isUse = true;
                    StartCoroutine(SpakeCoroutine());
                }
            }
        }
    }
    IEnumerator SpakeCoroutine()
    {
        float a = 0.0f;
        Image img = GameObject.Find("Canvas").transform.Find("Spake").GetComponent<Image>();
        while (isUse)
        {
            img.color = new Color(1, 1, 1, a);
            a += Time.deltaTime * 2.0f;
            if (a >= 1.0f) break;
            yield return null;
        }
        SoundManager.instance.StartAudio(new string[1] { "Object/DifficultyChange" }, VOLUME_TYPE.EFFECT);
        img.color = Color.white;
        while (isUse)
        {
            img.color = new Color(1, 1, 1, a);
            a -= Time.deltaTime * 2.0f;
            if (a <= 0.0f) break;
            yield return null;
        }
        img.color = new Color(1, 1, 1, 0);
        if (isUse)
        {
            isUse = false;
            isSelect = false;
            isOpen = false;
            controllerObj.SetActive(false);
            Inventorys.Instance.InventoryReset(true);
            SaveAndLoadManager.instance.SaveItem(new ItemSave { _id = "glowing_skull2", _nCount = 200 }, true);
            SaveAndLoadManager.instance.DeleteItemBossRush();
            SaveAndLoadManager.instance.DeleteManufacture(1);
            Inventorys.Instance.glowindskull2 = 200;
            TextOpenController.instance.ShowText(LocalizeManager.GetLocalize("ajit2_reset_title3"), Color.white, 24, controllerObj, 2.0f, true);

            // 시작 아이템 레벨 체크

            string startLeft = string.Empty;
            int tLeft = SaveAndLoadManager.instance.LoadUnlock("tentacle_arm_1");
            if (tLeft == 0) startLeft = "tentacle_arm";
            else startLeft = "tentacle_arm_1";

            int tRight = SaveAndLoadManager.instance.LoadUnlock("catching_tentacle_1");
            string startRight = string.Empty;
            if (tRight == 0) startRight = "catching_tentacle";
            else startRight = "catching_tentacle_1";

            string startQ = string.Empty;
            int tQ = SaveAndLoadManager.instance.LoadUnlock("spiny_cell_firing_1");
            if (tQ == 0) startQ = "spiny_cell_firing";
            else startQ = "spiny_cell_firing_1";

            ItemsData Item_left = System.Array.Find(SheetManager.Instance.ItemsDB.dataArray, item => item.ID.Equals(startLeft));
            ItemsData Item_right = System.Array.Find(SheetManager.Instance.ItemsDB.dataArray, item => item.ID.Equals(startRight));
            ItemsData Item_Q = System.Array.Find(SheetManager.Instance.ItemsDB.dataArray, item => item.ID.Equals(startQ));
            ItemsData Item_SPACE = System.Array.Find(SheetManager.Instance.ItemsDB.dataArray, item => item.ID.Equals("rolling"));
            Inventorys.Instance.AddItem(Item_left, 1, false, true);
            Inventorys.Instance.AddItem(Item_right, 1, false, true);
            Inventorys.Instance.AddItem(Item_Q, 1, false, true);
            Inventorys.Instance.AddItem(Item_SPACE, 1, false, true);
            PDG.Player.instance.SettingItem(Item_left);
            PDG.Player.instance.SettingItem(Item_right);
            PDG.Player.instance.SettingItem(Item_Q);
            PDG.Player.instance.SettingItem(Item_SPACE);
            MakeController.instance.DicMakeCount.Clear();
            if (PDG.Player.instance._pet) Destroy(PDG.Player.instance._pet.gameObject);
            PDG.Player.instance.isBiologicalDrone = false;
            for (int i = 0; i < PDG.Dungeon.instance.BossRushItemList.Count; i++)
            {
                PDG.Dungeon.instance.BossRushItemList[i].SetActive(false);
            }
            PDG.Dungeon.instance.BossRushItemList.Clear();
        }
    }
}